#!/usr/bin/env python3

'''
    Passando come parametri in ingresso al programma i dati relativi ad un firmware viene
    generato il nome dell'eseguibile stampandolo su stdout.
    
    esempio:
    ./format_code.py --boardcode AVC --firmwarecode 15
    ./format_code.py --boardcode AVC --firmwarecode 15 --year 2016 --month 1 --day 1
    ./format_code.py --boardcode AVC --firmwarecode 15 --year 2016 --month 2 --day 29
    
'''


import os
import sys
import datetime
import argparse


def format_year(time):
    base = time.year
    base -= 2010
    if base >= 8:
        base += 1
    return chr(ord('A')+base)


def format_month(time):
    base = time.month
    retVal = 0

    if base < 10:
        return chr(ord('0')+base)
    elif base == 10:
        return 'A'
    elif base == 11:
        return 'B'
    elif base == 12:
        return 'C'
    else:
        return '0'


def format_day(time):
    base = time.day
    if base < 10:
        return chr(ord('0')+base)
    elif base < 18:
        return chr(ord('A')+base-10)
    elif base < 23:
        return chr(ord('B')+base-10)
    elif base <= 31:
        return chr(ord('C')+base-10)
    else:
        return 0


def format_string(date):
    year = format_year(date)
    month = format_month(date)
    day = format_day(date)
    st = "%c" % (year)  # , month, day, t, int(n))

    print(st),


if __name__ == "__main__":
    date = datetime.datetime.now()
    format_string(date)
