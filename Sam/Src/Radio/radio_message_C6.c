#include "radio_message_C6.h"
#include "event_list.h"
#include "logger.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

void message_C6_process(struct ALPINE_MESSAGE_C6 *msg){
	union EVENT ev;
	(void)ev;
	ev.as_phone_info.generic.event = eEVENT_NAVIGATION;
	(void)msg;
}

/**
 * @}
 */
