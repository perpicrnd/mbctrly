#include "radio_message_30.h"
#include <stdbool.h>
#include "logger.h"

#include "FreeRTOS.h"
#include "task.h"

bool message_30_transmit_header(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t total_frames, uint8_t this_menu, const char *str) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xC8);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, this_menu);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

bool message_30_transmit_row_two_column(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, enum MESSAGE_30_ROW_TYPE field_type, uint8_t attributes, const char *str, const char *str1) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xCA);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, index);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, field_type);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, attributes);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_string(&msg->msg, strlen(str1), str1);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

bool message_30_transmit_row_checkbox(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t value0) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xCA);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, index);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, MESSAGE_30_ROW_TYPE_CHECKBOX);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, attributes);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_string(&msg->msg, strlen(str1), str1);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, value0);
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

bool message_30_transmit_row_optionbox(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, uint8_t value) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xCA);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, index);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, MESSAGE_30_ROW_TYPE_OPTIONBOX);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, attributes);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, value);
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

bool message_30_transmit_textnumeric(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t current, uint8_t min, uint8_t max) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xCA);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, index);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, MESSAGE_30_ROW_TYPE_TEXTNUMERIC);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, attributes);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_string(&msg->msg, strlen(str1), str1);
    message_raw_add_uint8(&msg->msg, '\0');

    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, min);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, max);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, current);
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

bool message_30_transmit_textnumeric_hidden(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t current, uint8_t min, uint8_t max) {
    message_raw_reset(&msg->msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xCA);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x30);
    message_raw_add_uint8(&msg->msg, index);
    message_raw_add_uint8(&msg->msg, 0x00);  //will be changed later
    message_raw_add_uint8(&msg->msg, MESSAGE_30_ROW_TYPE_TEXTNUMERICHIDDEN);
    message_raw_add_uint8(&msg->msg, total_frames);
    message_raw_add_uint8(&msg->msg, attributes);
    message_raw_add_string(&msg->msg, strlen(str), str);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_string(&msg->msg, strlen(str1), str1);
    message_raw_add_uint8(&msg->msg, '\0');

    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, min);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, max);
    message_raw_add_uint8(&msg->msg, '\0');
    message_raw_add_uint8(&msg->msg, current);
    message_raw_update_message_len(&msg->msg);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}
