#include "radio_message_B7.h"
#include "event_list.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

static bool radio_handle_clock_format = false;


bool radio_get_handles_clock_format(void){
	return radio_handle_clock_format;
}


void message_B7_process(struct ALPINE_MESSAGE_B7 *msg) {
	if (msg->msg.data[COMMAND_POS + 3] == 0x00){
		radio_handle_clock_format = true;
		LOG_UART_NORMAL("%s cambio formato ora\r\n", __PRETTY_FUNCTION__);
		event_time_format_changed_emit(msg->msg.data[COMMAND_POS + 4], msg->msg.data[COMMAND_POS + 5]);
	}
}

/**
 * @}
 */
