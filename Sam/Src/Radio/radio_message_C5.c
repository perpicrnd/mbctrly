#include "radio_message.h"
#include "event_list.h"
#include "logger.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

void message_C5_process(struct ALPINE_MESSAGE_C5 *msg){
	union EVENT ev;
	ev.as_generic.event = eEVENT_MEDIA_TEXT;
	ev.as_media_text.string_type = msg->msg.data[6];
	ev.as_media_text.string_id = msg->msg.data[7];
	bool end_of_string=false;
	for(uint8_t i=0; i<MEDIA_TEXT_STRING_LENGTH && end_of_string==false; i++){
		ev.as_media_text.string[i] = msg->msg.data[8+i];
		if (msg->msg.data[8+i] == '\0'){
			end_of_string = true;
		}
		if (i == MEDIA_TEXT_STRING_LENGTH-1){
			LOG_DEF_ERROR("Buffer media text string full\r\n");
			//Metto i puntini di terminazione, non voglio usare stringhe troppo lunghe.
			ev.as_media_text.string[MEDIA_TEXT_STRING_LENGTH-1] = '\0';
			ev.as_media_text.string[MEDIA_TEXT_STRING_LENGTH-2] = '.';
			ev.as_media_text.string[MEDIA_TEXT_STRING_LENGTH-3] = '.';
			ev.as_media_text.string[MEDIA_TEXT_STRING_LENGTH-4] = '.';
		}
	}
	event_emit(&ev);
}

/**
 * @}
 */
