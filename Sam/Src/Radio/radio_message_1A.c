#include "radio_message_1A.h"
#include "event_list.h"

bool  message_1A_transmit(struct ALPINE_MESSAGE_1A *msg, uint8_t counter, const struct HVAC_INFO * const info){
	message_raw_reset(&msg->msg);
	message_raw_add_uint8(&msg->msg, 0xC7);
	message_raw_add_uint8(&msg->msg, 0xC8);
	message_raw_add_uint8(&msg->msg, counter);
	message_raw_add_uint8(&msg->msg, 0x1A);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, 0x00);//Counter della lunghezza.

	message_raw_add_uint8(&msg->msg, info->left_temperature);
	message_raw_add_uint8(&msg->msg, info->right_temperature);
	message_raw_add_uint8(&msg->msg, info->fan_speed);
	message_raw_add_uint8(&msg->msg, info->fan_direction);
	message_raw_add_uint8(&msg->msg, info->seat_heating.as_uint8   );
	message_raw_add_uint8(&msg->msg, info->indicators.as_uint8);
	message_raw_add_uint8(&msg->msg, info->profile.as_uint8);
	message_raw_add_uint8(&msg->msg, info->features.as_uint8);
    message_raw_update_message_len(&msg->msg);
	message_raw_crc_add(&msg->msg);
	message_raw_transmit(&msg->msg);
	return true;
}