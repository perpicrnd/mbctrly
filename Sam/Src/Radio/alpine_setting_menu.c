#include "alpine_menu.h"
#include "alpine_menu_priv.h"
#include "alpine_task.h"
#include "buttons.h"
#include "controller.h"
#include "ignition.h"
#include "logger.h"
#include "menu_manager.h"
#include "menu_walking.h"
#include "vehicle.h"

static menu_manager_t alpine_settings_menu_manager;

typedef enum {
    INTERFACE_MENU_VERS = 0x10,
} INTERFACE_MENU;

enum INTERFACE_MENU_ROWS {
    INTERFACE_MENU_ROWS_IGNITION = 1,
    INTERFACE_MENU_ROWS_IGNITION_DELAY,
    INTERFACE_MENU_ROWS_REVERSE,
    INTERFACE_MENU_ROWS_REVERSE_ENTER,
    INTERFACE_MENU_ROWS_REVERSE_EXIT,
    INTERFACE_MENU_LED_COLOR,
    INTERFACE_MENU_PREFLIGHT,
    INTERFACE_MENU_BUTTON_POS,
    INTERFACE_MENU_STEERING_POS,
    INTERFACE_MENU_FACTORY,
};

extern char *interface_settings_str[LANGUAGE_DEFINITION_COUNT];

static const char *ignition_logic[] = {
    "Zündungslogik",       //LANGUAGE_DEFINITION_GERMAN,
    "Ignition Logic",      //LANGUAGE_DEFINITION_ENGLISH,
    "Ignition Logic",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Logique d'allumage",  //LANGUAGE_DEFINITION_FRENCH,
    "Logica spegnimento",  //LANGUAGE_DEFINITION_ITALIAN,
    "Modo encendido",      //LANGUAGE_DEFINITION_SPANISH,
    "Modo ligado",         //LANGUAGE_DEFINITION_PORTUGUESE,
    "Ignition Logic",      //LANGUAGE_DEFINITION_POLISH,
    "Ignition Logic",      //LANGUAGE_DEFINITION_CZEC,
    "Ignition Logic",      //LANGUAGE_DEFINITION_SWEDISH,
    "Ignition Logic",      //LANGUAGE_DEFINITION_DUTCH,
    "Ignition Logic",      //LANGUAGE_DEFINITION_JAPANESE,
    "Ignition Logic",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Ignition Logic",      //LANGUAGE_DEFINITION_KOREAN,
    "Ignition Logic",      //LANGUAGE_DEFINITION_TURKISH,
    "Ignition Logic",      //LANGUAGE_DEFINITION_CHINESE
    "Ignition Logic",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Ignition Logic",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Ignition Logic",      //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *ignition_logic_door[] = {
    "Tür",                  //LANGUAGE_DEFINITION_GERMAN,
    "Door",                 //LANGUAGE_DEFINITION_ENGLISH,
    "Door",                 //LANGUAGE_DEFINITION_ENGLISH US,
    "Portes",               //LANGUAGE_DEFINITION_FRENCH,
    "Porta",                //LANGUAGE_DEFINITION_ITALIAN,
    "Cierre centralizado",  //LANGUAGE_DEFINITION_SPANISH,
    "Fecho central",        //LANGUAGE_DEFINITION_PORTUGUESE,
    "Door",                 //LANGUAGE_DEFINITION_POLISH,
    "Door",                 //LANGUAGE_DEFINITION_CZEC,
    "Door",                 //LANGUAGE_DEFINITION_SWEDISH,
    "Door",                 //LANGUAGE_DEFINITION_DUTCH,
    "Door",                 //LANGUAGE_DEFINITION_JAPANESE,
    "Door",                 //LANGUAGE_DEFINITION_RUSSIAN,
    "Door",                 //LANGUAGE_DEFINITION_KOREAN,
    "Door",                 //LANGUAGE_DEFINITION_TURKISH,
    "Door",                 //LANGUAGE_DEFINITION_CHINESE
    "Door",                 //LANGUAGE_DEFINITION_NORWEGIAN
    "Door",                 //LANGUAGE_DEFINITION_SLOVAKIAN
    "Door",                 //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *ignition_logic_key[] = {
    "Schlüssel",          //LANGUAGE_DEFINITION_GERMAN,
    "Key",                //LANGUAGE_DEFINITION_ENGLISH,
    "Key",                //LANGUAGE_DEFINITION_ENGLISH US,
    "Deverouillage",      //LANGUAGE_DEFINITION_FRENCH,
    "Chiave",             //LANGUAGE_DEFINITION_ITALIAN,
    "Llave de contacto",  //LANGUAGE_DEFINITION_SPANISH,
    "Posião de chave",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Key",                //LANGUAGE_DEFINITION_POLISH,
    "Key",                //LANGUAGE_DEFINITION_CZEC,
    "Key",                //LANGUAGE_DEFINITION_SWEDISH,
    "Key",                //LANGUAGE_DEFINITION_DUTCH,
    "Key",                //LANGUAGE_DEFINITION_JAPANESE,
    "Key",                //LANGUAGE_DEFINITION_RUSSIAN,
    "Key",                //LANGUAGE_DEFINITION_KOREAN,
    "Key",                //LANGUAGE_DEFINITION_TURKISH,
    "Key",                //LANGUAGE_DEFINITION_CHINESE
    "Key",                //LANGUAGE_DEFINITION_NORWEGIAN
    "Key",                //LANGUAGE_DEFINITION_SLOVAKIAN
    "Key",                //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *delay_minutes[] = {
    "Verzögerung Zündung",        //LANGUAGE_DEFINITION_GERMAN,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_ENGLISH,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_ENGLISH US,
    "Délai d'extinction",         //LANGUAGE_DEFINITION_FRENCH,
    "Mantenimento sottochiave",   //LANGUAGE_DEFINITION_ITALIAN,
    "Retardo de apagado (Min.)",  //LANGUAGE_DEFINITION_SPANISH,
    "Atrasar desligar (Min.)",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_POLISH,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_CZEC,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_SWEDISH,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_DUTCH,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_JAPANESE,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_RUSSIAN,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_KOREAN,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_TURKISH,
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_CHINESE
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_NORWEGIAN
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_SLOVAKIAN
    "Ign. HOLD Minutes",          //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *min_str[] = {
    "min",     //LANGUAGE_DEFINITION_GERMAN,
    "min(s)",  //LANGUAGE_DEFINITION_ENGLISH,
    "min(s)",  //LANGUAGE_DEFINITION_ENGLISH US,
    "min(s)",  //LANGUAGE_DEFINITION_FRENCH,
    "min",     //LANGUAGE_DEFINITION_ITALIAN,
    "min(s)",  //LANGUAGE_DEFINITION_SPANISH,
    "min(s)",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "min(s)",  //LANGUAGE_DEFINITION_POLISH,
    "min(s)",  //LANGUAGE_DEFINITION_CZEC,
    "min(s)",  //LANGUAGE_DEFINITION_SWEDISH,
    "min(s)",  //LANGUAGE_DEFINITION_DUTCH,
    "min(s)",  //LANGUAGE_DEFINITION_JAPANESE,
    "min(s)",  //LANGUAGE_DEFINITION_RUSSIAN,
    "min(s)",  //LANGUAGE_DEFINITION_KOREAN,
    "min(s)",  //LANGUAGE_DEFINITION_TURKISH,
    "min(s)",  //LANGUAGE_DEFINITION_CHINESE
    "min(s)",  //LANGUAGE_DEFINITION_NORWEGIAN
    "min(s)",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "min(s)",  //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *sec_str[] = {
    "sek",     //LANGUAGE_DEFINITION_GERMAN,
    "sec(s)",  //LANGUAGE_DEFINITION_ENGLISH,
    "sec(s)",  //LANGUAGE_DEFINITION_ENGLISH US,
    "sec(s)",  //LANGUAGE_DEFINITION_FRENCH,
    "sec",     //LANGUAGE_DEFINITION_ITALIAN,
    "seg(s)",  //LANGUAGE_DEFINITION_SPANISH,
    "seg(s)",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "sec(s)",  //LANGUAGE_DEFINITION_POLISH,
    "sec(s)",  //LANGUAGE_DEFINITION_CZEC,
    "sec(s)",  //LANGUAGE_DEFINITION_SWEDISH,
    "sec(s)",  //LANGUAGE_DEFINITION_DUTCH,
    "sec(s)",  //LANGUAGE_DEFINITION_JAPANESE,
    "sec(s)",  //LANGUAGE_DEFINITION_RUSSIAN,
    "sec(s)",  //LANGUAGE_DEFINITION_KOREAN,
    "sec(s)",  //LANGUAGE_DEFINITION_TURKISH,
    "sec(s)",  //LANGUAGE_DEFINITION_CHINESE
    "sec(s)",  //LANGUAGE_DEFINITION_NORWEGIAN
    "sec(s)",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "sec(s)",  //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *reverse_logic[] = {
    "Rückwärtsganglogik",       //LANGUAGE_DEFINITION_GERMAN,
    "Reverse Logic",            //LANGUAGE_DEFINITION_ENGLISH,
    "Reverse Logic",            //LANGUAGE_DEFINITION_ENGLISH US,
    "Logique d'info de recul",  //LANGUAGE_DEFINITION_FRENCH,
    "Logica Retro",             //LANGUAGE_DEFINITION_ITALIAN,
    "Modo marcha atrás",        //LANGUAGE_DEFINITION_SPANISH,
    "Modo marcha a trás",       //LANGUAGE_DEFINITION_PORTUGUESE,
    "Reverse Logic",            //LANGUAGE_DEFINITION_POLISH,
    "Reverse Logic",            //LANGUAGE_DEFINITION_CZEC,
    "Reverse Logic",            //LANGUAGE_DEFINITION_SWEDISH,
    "Reverse Logic",            //LANGUAGE_DEFINITION_DUTCH,
    "Reverse Logic",            //LANGUAGE_DEFINITION_JAPANESE,
    "Reverse Logic",            //LANGUAGE_DEFINITION_RUSSIAN,
    "Reverse Logic",            //LANGUAGE_DEFINITION_KOREAN,
    "Reverse Logic",            //LANGUAGE_DEFINITION_TURKISH,
    "Reverse Logic",            //LANGUAGE_DEFINITION_CHINESE
    "Reverse Logic",            //LANGUAGE_DEFINITION_NORWEGIAN
    "Reverse Logic",            //LANGUAGE_DEFINITION_SLOVAKIAN
    "Reverse Logic",            //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *natural[] = {
    "Sofort",    //LANGUAGE_DEFINITION_GERMAN,
    "Natural",   //LANGUAGE_DEFINITION_ENGLISH,
    "Natural",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Normal",    //LANGUAGE_DEFINITION_FRENCH,
    "Naturale",  //LANGUAGE_DEFINITION_ITALIAN,
    "Normal",    //LANGUAGE_DEFINITION_SPANISH,
    "Normal",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Natural",   //LANGUAGE_DEFINITION_POLISH,
    "Natural",   //LANGUAGE_DEFINITION_CZEC,
    "Natural",   //LANGUAGE_DEFINITION_SWEDISH,
    "Natural",   //LANGUAGE_DEFINITION_DUTCH,
    "Natural",   //LANGUAGE_DEFINITION_JAPANESE,
    "Natural",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Natural",   //LANGUAGE_DEFINITION_KOREAN,
    "Natural",   //LANGUAGE_DEFINITION_TURKISH,
    "Natural",   //LANGUAGE_DEFINITION_CHINESE
    "Natural",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Natural",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Natural",   //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *delayed[] = {
    "Verzögert",  //LANGUAGE_DEFINITION_GERMAN,
    "Delayed",    //LANGUAGE_DEFINITION_ENGLISH,
    "Delayed",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Délai",      //LANGUAGE_DEFINITION_FRENCH,
    "Ritardata",  //LANGUAGE_DEFINITION_ITALIAN,
    "Retardado",  //LANGUAGE_DEFINITION_SPANISH,
    "Atrasado",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Delayed",    //LANGUAGE_DEFINITION_POLISH,
    "Delayed",    //LANGUAGE_DEFINITION_CZEC,
    "Delayed",    //LANGUAGE_DEFINITION_SWEDISH,
    "Delayed",    //LANGUAGE_DEFINITION_DUTCH,
    "Delayed",    //LANGUAGE_DEFINITION_JAPANESE,
    "Delayed",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Delayed",    //LANGUAGE_DEFINITION_KOREAN,
    "Delayed",    //LANGUAGE_DEFINITION_TURKISH,
    "Delayed",    //LANGUAGE_DEFINITION_CHINESE
    "Delayed",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Delayed",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Delayed",    //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *on_delay_seconds[] = {
    "Verzögerung beim Einschalten",  //LANGUAGE_DEFINITION_GERMAN,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_ENGLISH,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_ENGLISH US,
    "Délai ON en seconds",           //LANGUAGE_DEFINITION_FRENCH,
    "Retro Ritardo Attivazione",     //LANGUAGE_DEFINITION_ITALIAN,
    "Retardo en marcha atrás",       //LANGUAGE_DEFINITION_SPANISH,
    "Atraso em marcha a trás",       //LANGUAGE_DEFINITION_PORTUGUESE,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_POLISH,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_CZEC,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_SWEDISH,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_DUTCH,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_JAPANESE,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_RUSSIAN,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_KOREAN,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_TURKISH,
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_CHINESE
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_NORWEGIAN
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_SLOVAKIAN
    "Rev. ON HOLD Seconds",          //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *off_delay_seconds[] = {
    "Verzögerung beim Ausschalten",  //LANGUAGE_DEFINITION_GERMAN,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_ENGLISH,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_ENGLISH US,
    "Délai OFF en seconds",          //LANGUAGE_DEFINITION_FRENCH,
    "Retro Ritardo Disattivazione",  //LANGUAGE_DEFINITION_ITALIAN,
    "Retardo OFF en marcha atrás",   //LANGUAGE_DEFINITION_SPANISH,
    "Atraso OFF marcha a trás",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_POLISH,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_CZEC,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_SWEDISH,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_DUTCH,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_JAPANESE,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_RUSSIAN,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_KOREAN,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_TURKISH,
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_CHINESE
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_NORWEGIAN
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_SLOVAKIAN
    "Rev. OFF HOLD Seconds",         //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *led_color[] = {
    "Tastenbeleuchtung",        //LANGUAGE_DEFINITION_GERMAN,
    "Button Illumination",      //LANGUAGE_DEFINITION_ENGLISH,
    "Button Illumination",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Couleur retro-éclairage",  //LANGUAGE_DEFINITION_FRENCH,
    "Colore Pulsanti",          //LANGUAGE_DEFINITION_ITALIAN,
    "Iluminación botonera",     //LANGUAGE_DEFINITION_SPANISH,
    "Tecla de iluminação",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Button Illumination",      //LANGUAGE_DEFINITION_POLISH,
    "Button Illumination",      //LANGUAGE_DEFINITION_CZEC,
    "Button Illumination",      //LANGUAGE_DEFINITION_SWEDISH,
    "Button Illumination",      //LANGUAGE_DEFINITION_DUTCH,
    "Button Illumination",      //LANGUAGE_DEFINITION_JAPANESE,
    "Button Illumination",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Button Illumination",      //LANGUAGE_DEFINITION_KOREAN,
    "Button Illumination",      //LANGUAGE_DEFINITION_TURKISH,
    "Button Illumination",      //LANGUAGE_DEFINITION_CHINESE
    "Button Illumination",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Button Illumination",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Button Illumination",      //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *red[] = {
    "Rot",       //LANGUAGE_DEFINITION_GERMAN,
    "Red",       //LANGUAGE_DEFINITION_ENGLISH,
    "Red",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Rouge",     //LANGUAGE_DEFINITION_FRENCH,
    "Rosso",     //LANGUAGE_DEFINITION_ITALIAN,
    "Rojo",      //LANGUAGE_DEFINITION_SPANISH,
    "Vermelho",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Red",       //LANGUAGE_DEFINITION_POLISH,
    "Red",       //LANGUAGE_DEFINITION_CZEC,
    "Red",       //LANGUAGE_DEFINITION_SWEDISH,
    "Red",       //LANGUAGE_DEFINITION_DUTCH,
    "Red",       //LANGUAGE_DEFINITION_JAPANESE,
    "Red",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Red",       //LANGUAGE_DEFINITION_KOREAN,
    "Red",       //LANGUAGE_DEFINITION_TURKISH,
    "Red",       //LANGUAGE_DEFINITION_CHINESE
    "Red",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Red",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Red",       //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_red[] = {
    "Rot (gedimmt)",   //LANGUAGE_DEFINITION_GERMAN,
    "Dim Red",         //LANGUAGE_DEFINITION_ENGLISH,
    "Dim Red",         //LANGUAGE_DEFINITION_ENGLISH US,
    "Rouge sombre",    //LANGUAGE_DEFINITION_FRENCH,
    "Rosso tenue",     //LANGUAGE_DEFINITION_ITALIAN,
    "Rojo tenue",      //LANGUAGE_DEFINITION_SPANISH,
    "Vermelho tenue",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim Red",         //LANGUAGE_DEFINITION_POLISH,
    "Dim Red",         //LANGUAGE_DEFINITION_CZEC,
    "Dim Red",         //LANGUAGE_DEFINITION_SWEDISH,
    "Dim Red",         //LANGUAGE_DEFINITION_DUTCH,
    "Dim Red",         //LANGUAGE_DEFINITION_JAPANESE,
    "Dim Red",         //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim Red",         //LANGUAGE_DEFINITION_KOREAN,
    "Dim Red",         //LANGUAGE_DEFINITION_TURKISH,
    "Dim Red",         //LANGUAGE_DEFINITION_CHINESE
    "Dim Red",         //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim Red",         //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim Red",         //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *green[] = {
    "Grün",   //LANGUAGE_DEFINITION_GERMAN,
    "Green",  //LANGUAGE_DEFINITION_ENGLISH,
    "Green",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Vert",   //LANGUAGE_DEFINITION_FRENCH,
    "Verde",  //LANGUAGE_DEFINITION_ITALIAN,
    "Verde",  //LANGUAGE_DEFINITION_SPANISH,
    "Verde",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Green",  //LANGUAGE_DEFINITION_POLISH,
    "Green",  //LANGUAGE_DEFINITION_CZEC,
    "Green",  //LANGUAGE_DEFINITION_SWEDISH,
    "Green",  //LANGUAGE_DEFINITION_DUTCH,
    "Green",  //LANGUAGE_DEFINITION_JAPANESE,
    "Green",  //LANGUAGE_DEFINITION_RUSSIAN,
    "Green",  //LANGUAGE_DEFINITION_KOREAN,
    "Green",  //LANGUAGE_DEFINITION_TURKISH,
    "Green",  //LANGUAGE_DEFINITION_CHINESE
    "Green",  //LANGUAGE_DEFINITION_NORWEGIAN
    "Green",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "Green",  //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_green[] = {
    "Grün (gedimmt)",  //LANGUAGE_DEFINITION_GERMAN,
    "Dim Green",       //LANGUAGE_DEFINITION_ENGLISH,
    "Dim Green",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Vert sombre",     //LANGUAGE_DEFINITION_FRENCH,
    "Verde Tenue",     //LANGUAGE_DEFINITION_ITALIAN,
    "Verde tenue",     //LANGUAGE_DEFINITION_SPANISH,
    "Verde tenue",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim Green",       //LANGUAGE_DEFINITION_POLISH,
    "Dim Green",       //LANGUAGE_DEFINITION_CZEC,
    "Dim Green",       //LANGUAGE_DEFINITION_SWEDISH,
    "Dim Green",       //LANGUAGE_DEFINITION_DUTCH,
    "Dim Green",       //LANGUAGE_DEFINITION_JAPANESE,
    "Dim Green",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim Green",       //LANGUAGE_DEFINITION_KOREAN,
    "Dim Green",       //LANGUAGE_DEFINITION_TURKISH,
    "Dim Green",       //LANGUAGE_DEFINITION_CHINESE
    "Dim Green",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim Green",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim Green",       //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *blue[] = {
    "Blau",  //LANGUAGE_DEFINITION_GERMAN,
    "Blue",  //LANGUAGE_DEFINITION_ENGLISH,
    "Blue",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Bleu",  //LANGUAGE_DEFINITION_FRENCH,
    "Blu",   //LANGUAGE_DEFINITION_ITALIAN,
    "Azul",  //LANGUAGE_DEFINITION_SPANISH,
    "Azul",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Blue",  //LANGUAGE_DEFINITION_POLISH,
    "Blue",  //LANGUAGE_DEFINITION_CZEC,
    "Blue",  //LANGUAGE_DEFINITION_SWEDISH,
    "Blue",  //LANGUAGE_DEFINITION_DUTCH,
    "Blue",  //LANGUAGE_DEFINITION_JAPANESE,
    "Blue",  //LANGUAGE_DEFINITION_RUSSIAN,
    "Blue",  //LANGUAGE_DEFINITION_KOREAN,
    "Blue",  //LANGUAGE_DEFINITION_TURKISH,
    "Blue",  //LANGUAGE_DEFINITION_CHINESE
    "Blue",  //LANGUAGE_DEFINITION_NORWEGIAN
    "Blue",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "Blue",  //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_blue[] = {
    "Blau (gedimmt)",  //LANGUAGE_DEFINITION_GERMAN,
    "Dim Blue",        //LANGUAGE_DEFINITION_ENGLISH,
    "Dim Blue",        //LANGUAGE_DEFINITION_ENGLISH US,
    "Bleu sombre",     //LANGUAGE_DEFINITION_FRENCH,
    "Blu Tenue",       //LANGUAGE_DEFINITION_ITALIAN,
    "Azul tenue",      //LANGUAGE_DEFINITION_SPANISH,
    "Azul tenue",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim Blue",        //LANGUAGE_DEFINITION_POLISH,
    "Dim Blue",        //LANGUAGE_DEFINITION_CZEC,
    "Dim Blue",        //LANGUAGE_DEFINITION_SWEDISH,
    "Dim Blue",        //LANGUAGE_DEFINITION_DUTCH,
    "Dim Blue",        //LANGUAGE_DEFINITION_JAPANESE,
    "Dim Blue",        //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim Blue",        //LANGUAGE_DEFINITION_KOREAN,
    "Dim Blue",        //LANGUAGE_DEFINITION_TURKISH,
    "Dim Blue",        //LANGUAGE_DEFINITION_CHINESE
    "Dim Blue",        //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim Blue",        //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim Blue",        //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *orange[] = {
    "Orange",     //LANGUAGE_DEFINITION_GERMAN,
    "Orange",     //LANGUAGE_DEFINITION_ENGLISH,
    "Orange",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Orange",     //LANGUAGE_DEFINITION_FRENCH,
    "Arancione",  //LANGUAGE_DEFINITION_ITALIAN,
    "Naranja",    //LANGUAGE_DEFINITION_SPANISH,
    "Laranja",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Orange",     //LANGUAGE_DEFINITION_POLISH,
    "Orange",     //LANGUAGE_DEFINITION_CZEC,
    "Orange",     //LANGUAGE_DEFINITION_SWEDISH,
    "Orange",     //LANGUAGE_DEFINITION_DUTCH,
    "Orange",     //LANGUAGE_DEFINITION_JAPANESE,
    "Orange",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Orange",     //LANGUAGE_DEFINITION_KOREAN,
    "Orange",     //LANGUAGE_DEFINITION_TURKISH,
    "Orange",     //LANGUAGE_DEFINITION_CHINESE
    "Orange",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Orange",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Orange",     //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_orange[] = {
    "Orange (gedimmt)",  //LANGUAGE_DEFINITION_GERMAN,
    "Dim Orange",        //LANGUAGE_DEFINITION_ENGLISH,
    "Dim Orange",        //LANGUAGE_DEFINITION_ENGLISH US,
    "Orange sombre",     //LANGUAGE_DEFINITION_FRENCH,
    "Arancione Tenue",   //LANGUAGE_DEFINITION_ITALIAN,
    "Naranja tenue",     //LANGUAGE_DEFINITION_SPANISH,
    "Laranja tenue",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim Orange",        //LANGUAGE_DEFINITION_POLISH,
    "Dim Orange",        //LANGUAGE_DEFINITION_CZEC,
    "Dim Orange",        //LANGUAGE_DEFINITION_SWEDISH,
    "Dim Orange",        //LANGUAGE_DEFINITION_DUTCH,
    "Dim Orange",        //LANGUAGE_DEFINITION_JAPANESE,
    "Dim Orange",        //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim Orange",        //LANGUAGE_DEFINITION_KOREAN,
    "Dim Orange",        //LANGUAGE_DEFINITION_TURKISH,
    "Dim Orange",        //LANGUAGE_DEFINITION_CHINESE
    "Dim Orange",        //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim Orange",        //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim Orange",        //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *yellow[] = {
    "Gelb",      //LANGUAGE_DEFINITION_GERMAN,
    "Yellow",    //LANGUAGE_DEFINITION_ENGLISH,
    "Yellow",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Jaune",     //LANGUAGE_DEFINITION_FRENCH,
    "Giallo",    //LANGUAGE_DEFINITION_ITALIAN,
    "Amarillo",  //LANGUAGE_DEFINITION_SPANISH,
    "Amarelo",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Yellow",    //LANGUAGE_DEFINITION_POLISH,
    "Yellow",    //LANGUAGE_DEFINITION_CZEC,
    "Yellow",    //LANGUAGE_DEFINITION_SWEDISH,
    "Yellow",    //LANGUAGE_DEFINITION_DUTCH,
    "Yellow",    //LANGUAGE_DEFINITION_JAPANESE,
    "Yellow",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Yellow",    //LANGUAGE_DEFINITION_KOREAN,
    "Yellow",    //LANGUAGE_DEFINITION_TURKISH,
    "Yellow",    //LANGUAGE_DEFINITION_CHINESE
    "Yellow",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Yellow",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Yellow",    //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_yellow[] = {
    "Gelb (gedimmt)",  //LANGUAGE_DEFINITION_GERMAN,
    "Dim Yellow",      //LANGUAGE_DEFINITION_ENGLISH,
    "Dim Yellow",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Jaune sombre",    //LANGUAGE_DEFINITION_FRENCH,
    "Giallo Tenue",    //LANGUAGE_DEFINITION_ITALIAN,
    "Amarillo tenue",  //LANGUAGE_DEFINITION_SPANISH,
    "Amarelo tenue",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim Yellow",      //LANGUAGE_DEFINITION_POLISH,
    "Dim Yellow",      //LANGUAGE_DEFINITION_CZEC,
    "Dim Yellow",      //LANGUAGE_DEFINITION_SWEDISH,
    "Dim Yellow",      //LANGUAGE_DEFINITION_DUTCH,
    "Dim Yellow",      //LANGUAGE_DEFINITION_JAPANESE,
    "Dim Yellow",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim Yellow",      //LANGUAGE_DEFINITION_KOREAN,
    "Dim Yellow",      //LANGUAGE_DEFINITION_TURKISH,
    "Dim Yellow",      //LANGUAGE_DEFINITION_CHINESE
    "Dim Yellow",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim Yellow",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim Yellow",      //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *white[] = {
    "Weiß",    //LANGUAGE_DEFINITION_GERMAN,
    "White",   //LANGUAGE_DEFINITION_ENGLISH,
    "White",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Blanc",   //LANGUAGE_DEFINITION_FRENCH,
    "Bianco",  //LANGUAGE_DEFINITION_ITALIAN,
    "Blanco",  //LANGUAGE_DEFINITION_SPANISH,
    "Branco",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "White",   //LANGUAGE_DEFINITION_POLISH,
    "White",   //LANGUAGE_DEFINITION_CZEC,
    "White",   //LANGUAGE_DEFINITION_SWEDISH,
    "White",   //LANGUAGE_DEFINITION_DUTCH,
    "White",   //LANGUAGE_DEFINITION_JAPANESE,
    "White",   //LANGUAGE_DEFINITION_RUSSIAN,
    "White",   //LANGUAGE_DEFINITION_KOREAN,
    "White",   //LANGUAGE_DEFINITION_TURKISH,
    "White",   //LANGUAGE_DEFINITION_CHINESE
    "White",   //LANGUAGE_DEFINITION_NORWEGIAN
    "White",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "White",   //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dim_white[] = {
    "Weiß (gedimmt)",  //LANGUAGE_DEFINITION_GERMAN,
    "Dim White",       //LANGUAGE_DEFINITION_ENGLISH,
    "Dim White",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Blanc sombre",    //LANGUAGE_DEFINITION_FRENCH,
    "Bianco Tenue",    //LANGUAGE_DEFINITION_ITALIAN,
    "Blanco tenue",    //LANGUAGE_DEFINITION_SPANISH,
    "Branco tenue",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Dim White",       //LANGUAGE_DEFINITION_POLISH,
    "Dim White",       //LANGUAGE_DEFINITION_CZEC,
    "Dim White",       //LANGUAGE_DEFINITION_SWEDISH,
    "Dim White",       //LANGUAGE_DEFINITION_DUTCH,
    "Dim White",       //LANGUAGE_DEFINITION_JAPANESE,
    "Dim White",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Dim White",       //LANGUAGE_DEFINITION_KOREAN,
    "Dim White",       //LANGUAGE_DEFINITION_TURKISH,
    "Dim White",       //LANGUAGE_DEFINITION_CHINESE
    "Dim White",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Dim White",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Dim White",       //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *factory_settings[] = {
    "Werkseinstellungen",     //LANGUAGE_DEFINITION_GERMAN,
    "Factory Settings",       //LANGUAGE_DEFINITION_ENGLISH,
    "Factory Settings",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Réglages d'usine",       //LANGUAGE_DEFINITION_FRENCH,
    "Impostazioni Iniziali",  //LANGUAGE_DEFINITION_ITALIAN,
    "Ajustes de fábrica",     //LANGUAGE_DEFINITION_SPANISH,
    "Definições de fábrica",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Factory Settings",       //LANGUAGE_DEFINITION_POLISH,
    "Factory Settings",       //LANGUAGE_DEFINITION_CZEC,
    "Factory Settings",       //LANGUAGE_DEFINITION_SWEDISH,
    "Factory Settings",       //LANGUAGE_DEFINITION_DUTCH,
    "Factory Settings",       //LANGUAGE_DEFINITION_JAPANESE,
    "Factory Settings",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Factory Settings",       //LANGUAGE_DEFINITION_KOREAN,
    "Factory Settings",       //LANGUAGE_DEFINITION_TURKISH,
    "Factory Settings",       //LANGUAGE_DEFINITION_CHINESE
    "Factory Settings",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Factory Settings",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Factory Settings",       //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *preflight_check[] = {
    "Prüfung vor Abfahrt",          //LANGUAGE_DEFINITION_GERMAN,
    "Preflight Check",              //LANGUAGE_DEFINITION_ENGLISH,
    "Preflight Check",              //LANGUAGE_DEFINITION_ENGLISH US,
    "Contrôle en amont",            //LANGUAGE_DEFINITION_FRENCH,
    "Avviso Sicurezza Avviamento",  //LANGUAGE_DEFINITION_ITALIAN,
    "Verificación previa",          //LANGUAGE_DEFINITION_SPANISH,
    "Verificação prévia",           //LANGUAGE_DEFINITION_PORTUGUESE,
    "Preflight Check",              //LANGUAGE_DEFINITION_POLISH,
    "Preflight Check",              //LANGUAGE_DEFINITION_CZEC,
    "Preflight Check",              //LANGUAGE_DEFINITION_SWEDISH,
    "Preflight Check",              //LANGUAGE_DEFINITION_DUTCH,
    "Preflight Check",              //LANGUAGE_DEFINITION_JAPANESE,
    "Preflight Check",              //LANGUAGE_DEFINITION_RUSSIAN,
    "Preflight Check",              //LANGUAGE_DEFINITION_KOREAN,
    "Preflight Check",              //LANGUAGE_DEFINITION_TURKISH,
    "Preflight Check",              //LANGUAGE_DEFINITION_CHINESE
    "Preflight Check",              //LANGUAGE_DEFINITION_NORWEGIAN
    "Preflight Check",              //LANGUAGE_DEFINITION_SLOVAKIAN
    "Preflight Check",              //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *on_str[] = {
    "Ein",   //!< LANGUAGE_DEFINITION_GERMAN
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "On",    //!< LANGUAGE_DEFINITION_FRENCH
    "On",    //!< LANGUAGE_DEFINITION_ITALIAN
    "On",    //!< LANGUAGE_DEFINITION_SPANISH
    "On",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "On",    //!< LANGUAGE_DEFINITION_POLISH
    "On",    //!< LANGUAGE_DEFINITION_CZECH
    "On",    //!< LANGUAGE_DEFINITION_SWEDISH
    "On",    //!< LANGUAGE_DEFINITION_DUTCH
    "On",    //!< LANGUAGE_DEFINITION_JAPANESE
    "On",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "On",    //!< LANGUAGE_DEFINITION_KOREAN
    "Açık",  //!< LANGUAGE_DEFINITION_TURKISH
    "On",    //!< LANGUAGE_DEFINITION_CHINESE
    "On",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "On",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "On",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *off_str[] = {
    "Aus",     //!< LANGUAGE_DEFINITION_GERMAN
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Off",     //!< LANGUAGE_DEFINITION_FRENCH
    "Off",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Off",     //!< LANGUAGE_DEFINITION_SPANISH
    "Off",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Off",     //!< LANGUAGE_DEFINITION_POLISH
    "Off",     //!< LANGUAGE_DEFINITION_CZECH
    "Off",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Off",     //!< LANGUAGE_DEFINITION_DUTCH
    "Off",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Off",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Off",     //!< LANGUAGE_DEFINITION_KOREAN
    "Kapalı",  //!< LANGUAGE_DEFINITION_TURKISH
    "Off",     //!< LANGUAGE_DEFINITION_CHINESE
    "Off",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Off",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Off",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *button_configuration[] = {
    "Tastenkonfiguration",        //!< LANGUAGE_DEFINITION_GERMAN
    "Button Configuration",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Configuration des Boutons",  //!< LANGUAGE_DEFINITION_FRENCH
    "Configurazione Pulsanti",    //!< LANGUAGE_DEFINITION_ITALIAN
    "Configuración botonera",     //!< LANGUAGE_DEFINITION_SPANISH
    "Tecla de configuração",      //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Button Configuration",       //!< LANGUAGE_DEFINITION_POLISH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_CZECH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_DUTCH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Button Configuration",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Button Configuration",       //!< LANGUAGE_DEFINITION_KOREAN
    "Button Configuration",       //!< LANGUAGE_DEFINITION_TURKISH
    "Button Configuration",       //!< LANGUAGE_DEFINITION_CHINESE
    "Button Configuration",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Button Configuration",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Button Configuration",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *horizontal[] = {
    "Horizontal",   //!< LANGUAGE_DEFINITION_GERMAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_ENGLISH
    "Horizontal",   //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Horizontal",   //!< LANGUAGE_DEFINITION_FRENCH
    "Orizzontale",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_SPANISH
    "Horizontal",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Horizontal",   //!< LANGUAGE_DEFINITION_POLISH
    "Horizontal",   //!< LANGUAGE_DEFINITION_CZECH
    "Horizontal",   //!< LANGUAGE_DEFINITION_SWEDISH
    "Horizontal",   //!< LANGUAGE_DEFINITION_DUTCH
    "Horizontal",   //!< LANGUAGE_DEFINITION_JAPANESE
    "Horizontal",   //!< LANGUAGE_DEFINITION_RUSSIAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_KOREAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_TURKISH
    "Horizontal",   //!< LANGUAGE_DEFINITION_CHINESE
    "Horizontal",   //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Horizontal",   //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *vertical[] = {
    "Vertikal",   //!< LANGUAGE_DEFINITION_GERMAN
    "Vertical",   //!< LANGUAGE_DEFINITION_ENGLISH
    "Vertical",   //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Vertical",   //!< LANGUAGE_DEFINITION_FRENCH
    "Verticale",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Vertical",   //!< LANGUAGE_DEFINITION_SPANISH
    "Vertical",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Vertical",   //!< LANGUAGE_DEFINITION_POLISH
    "Vertical",   //!< LANGUAGE_DEFINITION_CZECH
    "Vertical",   //!< LANGUAGE_DEFINITION_SWEDISH
    "Vertical",   //!< LANGUAGE_DEFINITION_DUTCH
    "Vertical",   //!< LANGUAGE_DEFINITION_JAPANESE
    "Vertical",   //!< LANGUAGE_DEFINITION_RUSSIAN
    "Vertical",   //!< LANGUAGE_DEFINITION_KOREAN
    "Vertical",   //!< LANGUAGE_DEFINITION_TURKISH
    "Vertical",   //!< LANGUAGE_DEFINITION_CHINESE
    "Vertical",   //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Vertical",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Vertical",   //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *steering_wheel[] = {
    "Lenkrad",              //!< LANGUAGE_DEFINITION_GERMAN
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Volant",               //!< LANGUAGE_DEFINITION_FRENCH
    "Volante",              //!< LANGUAGE_DEFINITION_ITALIAN
    "Mandos en volante",    //!< LANGUAGE_DEFINITION_SPANISH
    "Comandos de volante",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_POLISH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_CZECH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_DUTCH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_KOREAN
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_TURKISH
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_CHINESE
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Steering Wheel",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *left[] = {
    "Links",      //!< LANGUAGE_DEFINITION_GERMAN
    "Left",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Left",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Gauche",     //!< LANGUAGE_DEFINITION_FRENCH
    "Sinistra",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Izquierda",  //!< LANGUAGE_DEFINITION_SPANISH
    "Esquerda",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Left",       //!< LANGUAGE_DEFINITION_POLISH
    "Left",       //!< LANGUAGE_DEFINITION_CZECH
    "Left",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Left",       //!< LANGUAGE_DEFINITION_DUTCH
    "Left",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Left",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Left",       //!< LANGUAGE_DEFINITION_KOREAN
    "Left",       //!< LANGUAGE_DEFINITION_TURKISH
    "Left",       //!< LANGUAGE_DEFINITION_CHINESE
    "Left",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Left",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Left",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *right[] = {
    "Rechts",   //!< LANGUAGE_DEFINITION_GERMAN
    "Right",    //!< LANGUAGE_DEFINITION_ENGLISH
    "Right",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Droite",   //!< LANGUAGE_DEFINITION_FRENCH
    "Destra",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Derecha",  //!< LANGUAGE_DEFINITION_SPANISH
    "Direita",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Right",    //!< LANGUAGE_DEFINITION_POLISH
    "Right",    //!< LANGUAGE_DEFINITION_CZECH
    "Right",    //!< LANGUAGE_DEFINITION_SWEDISH
    "Right",    //!< LANGUAGE_DEFINITION_DUTCH
    "Right",    //!< LANGUAGE_DEFINITION_JAPANESE
    "Right",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "Right",    //!< LANGUAGE_DEFINITION_KOREAN
    "Right",    //!< LANGUAGE_DEFINITION_TURKISH
    "Right",    //!< LANGUAGE_DEFINITION_CHINESE
    "Right",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Right",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Right",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char empty[] = "\0";
static bool preflight_disable = false;
static bool ignition_logic_disable = false;
static bool reverse_lines_disable = false;
static bool ignition_lines_disable = false;
static bool buttons_lines_disable = false;

void alpine_menu_disable_buttons_lines(void) {
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_LED_COLOR);
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_BUTTON_POS);
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_STEERING_POS);
    buttons_lines_disable = true;
}

void alpine_menu_disable_reverse_lines(void) {
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE);
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_ENTER);
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_EXIT);
    reverse_lines_disable = true;
}

void alpine_menu_disable_ignition_lines(void) {
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION);
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION_DELAY);
    ignition_lines_disable = true;
}

void alpine_menu_disable_preflight_check(void) {
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_PREFLIGHT);
    preflight_disable = true;
}

void alpine_menu_disable_ignition_logic(void) {
    menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION);
    settings_write(RADIO_SETTING_IGNITION_BYTE, RADIO_SETTING_IGNITION_KEY);
    ignition_logic_disable = true;
}

static inline const char *ignition_logic_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (settings_read(RADIO_SETTING_IGNITION_BYTE)) {
        case RADIO_SETTING_IGNITION_DOOR:
            retVal = ignition_logic_door[language];
            break;
        default:
            retVal = ignition_logic_key[language];
            break;
    }
    return retVal;
}

static inline const char *button_conf_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (settings_read(BTN_CONFIGURATION)) {
        case BUTTON_CONFIGURATION_VERTICAL_LEFT:
        case BUTTON_CONFIGURATION_VERTICAL_RIGHT:
            retVal = vertical[language];
            break;
        default:
            retVal = horizontal[language];
            break;
    }
    return retVal;
}

static inline const char *drive_position_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (settings_read(BTN_CONFIGURATION)) {
        case BUTTON_CONFIGURATION_VERTICAL_LEFT:
        case BUTTON_CONFIGURATION_HORIZONTAL_LEFT:
            retVal = left[language];
            break;
        default:
            retVal = right[language];
            break;
    }
    return retVal;
}

static inline const char *reverse_logic_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (settings_read(RADIO_SETTING_REVERSE_BYTE)) {
        case RADIO_SETTING_REVERSE_DELAY:
            retVal = delayed[language];
            break;
        case RADIO_SETTING_REVERSE_NORMAL:
            retVal = natural[language];
            break;
        default:
            retVal = natural[language];
            settings_write(RADIO_SETTING_REVERSE_BYTE, RADIO_SETTING_REVERSE_NORMAL);
            break;
    }
    return retVal;
}

static inline const char *led_string(void) {
    const char *col = empty;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (controller_get_led_color()) {
        case LED_COLOR_RED:
            col = red[language];
            break;
        case LED_COLOR_RED_DIM:
            col = dim_red[language];
            break;
        case LED_COLOR_GREEN:
            col = green[language];
            break;
        case LED_COLOR_GREEN_DIM:
            col = dim_green[language];
            break;
        case LED_COLOR_BLUE:
            col = blue[language];
            break;
        case LED_COLOR_BLUE_DIM:
            col = dim_blue[language];
            break;
        case LED_COLOR_ORANGE:
            col = orange[language];
            break;
        case LED_COLOR_ORANGE_DIM:
            col = dim_orange[language];
            break;
        case LED_COLOR_YELLOW:
            col = yellow[language];
            break;
        case LED_COLOR_YELLOW_DIM:
            col = dim_yellow[language];
            break;
        case LED_COLOR_WHITE:
            col = white[language];
            break;
        case LED_COLOR_WHITE_DIM:
            col = dim_white[language];
            break;
        default:
            break;
    }
    return col;
}

static bool menu_settings_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(alpine_settings_menu_manager), INTERFACE_MENU_VERS, interface_settings_str[language]);
}

static bool menu_settings_send_ignition_logic(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    bool act_flag = 0x01;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (!ignition_logic_disable) {
        act_flag = 0x00;
    } else {
        settings_write(RADIO_SETTING_IGNITION_BYTE, RADIO_SETTING_IGNITION_KEY);
    }
    const char *ignition_logic_str = ignition_logic_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_ROWS_IGNITION, MESSAGE_30_ROW_TYPE_SUB_MENU, act_flag, ignition_logic[language], ignition_logic_str);
}

static bool menu_settings_send_ignition_logic_delay(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t min = settings_read(RADIO_SETTINGS_IGN_MINUTE_BYTE);
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_ROWS_IGNITION_DELAY, 0, delay_minutes[language], min_str[language], min, 0, 30);
}

static bool menu_settings_send_reverse_logic(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *reverse_logic_str = reverse_logic_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_ROWS_REVERSE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, reverse_logic[language], reverse_logic_str);
}

static bool menu_settings_send_reverse_logic_delay_enter(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t min = settings_read(RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE);
    uint8_t attr = 0x01;
    if (settings_read(RADIO_SETTING_REVERSE_BYTE) == RADIO_SETTING_REVERSE_DELAY) {
        attr = 0x00;
    }
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_ROWS_REVERSE_ENTER, attr, on_delay_seconds[language], sec_str[language], min, 0, 60);
}

static bool menu_settings_send_reverse_logic_delay_exit(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t min = settings_read(RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE);
    uint8_t attr = 0x01;
    if (settings_read(RADIO_SETTING_REVERSE_BYTE) == RADIO_SETTING_REVERSE_DELAY) {
        attr = 0x00;
    }
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_ROWS_REVERSE_EXIT, attr, off_delay_seconds[language], sec_str[language], min, 0, 60);
}

static bool menu_settings_send_led_color(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *led_str = led_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_LED_COLOR, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, led_color[language], led_str);
}

static bool menu_settings_send_preflight_check(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    enum RADIO_SETTING_PREFLIGHT_CHECK option_val = settings_read(RADIO_SETTING_PREFLIGHT_CHECK_BYTE);
    const char *option = off_str[language];
    bool opt = false;
    bool act_flag = 0x01;
    if (!preflight_disable) {
        act_flag = 0x00;
        if (option_val == RADIO_PREFLIGHT_CHECK_ON) {
            option = on_str[language];
            opt = true;
        }
    }

    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_PREFLIGHT, act_flag, preflight_check[language], option, opt);
}

static bool menu_settings_send_button_configuration(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *button_conf_str = button_conf_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_BUTTON_POS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, button_configuration[language], button_conf_str);
}

static bool menu_settings_send_steering_configuration(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *drive_pos_str = drive_position_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_STEERING_POS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, steering_wheel[language], drive_pos_str);
}
static bool menu_settings_send_factory_reset(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_FACTORY, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, factory_settings[language], empty);
}

static void handle_ignition_setting_change(void) {
    enum RADIO_SETTING_IGNITION ignition = settings_read(RADIO_SETTING_IGNITION_BYTE);
    ignition++;
    if (ignition == RADIO_SETTING_IGNITION_COUNT) {
        ignition = RADIO_SETTING_IGNITION_KEY;
    }
    settings_write(RADIO_SETTING_IGNITION_BYTE, ignition);
}

static void handle_reverse_setting_change(void) {
    enum RADIO_SETTING_REVERSE reverse = settings_read(RADIO_SETTING_REVERSE_BYTE);
    reverse++;
    if (reverse == RADIO_SETTING_REVERSE_COUNT) {
        reverse = RADIO_SETTING_REVERSE_NORMAL;
    }
    settings_write(RADIO_SETTING_REVERSE_BYTE, reverse);
}

static void handle_leds_setting_change(void) {
    enum LED_COLOR led = controller_get_led_color();
    led++;
    if (led >= LED_COLOR_COUNT) {
        led = LED_COLOR_RED;
    }

    struct CONTROLLER_DATA ctrl;
    ctrl.command = CONTROLLER_RW_ACTIVE_CONF;
    ctrl.data[0] = led;
    event_controller_emit(&ctrl);
}

static void handle_preflight_check(void) {
    enum RADIO_SETTING_PREFLIGHT_CHECK check = settings_read(RADIO_SETTING_PREFLIGHT_CHECK_BYTE);
    if (check == RADIO_PREFLIGHT_CHECK_ON) {
        check = RADIO_PREFLIGHT_CHECK_OFF;
    } else {
        check = RADIO_PREFLIGHT_CHECK_ON;
    }
    settings_write(RADIO_SETTING_PREFLIGHT_CHECK_BYTE, check);
}

static void handle_button_position(void) {
    enum BUTTON_CONFIGURATION btn = settings_read(BTN_CONFIGURATION);
    switch (btn) {
        case BUTTON_CONFIGURATION_VERTICAL_LEFT:
            btn = BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
            break;
        case BUTTON_CONFIGURATION_VERTICAL_RIGHT:
            btn = BUTTON_CONFIGURATION_HORIZONTAL_RIGHT;
            break;
        case BUTTON_CONFIGURATION_HORIZONTAL_LEFT:
            btn = BUTTON_CONFIGURATION_VERTICAL_LEFT;
            break;
        case BUTTON_CONFIGURATION_HORIZONTAL_RIGHT:
            btn = BUTTON_CONFIGURATION_VERTICAL_RIGHT;
            break;
        default:
            LOG_DEF_NORMAL("%s no valid config found.\r\n", __PRETTY_FUNCTION__);
            btn = BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
            break;
    }
    settings_write(BTN_CONFIGURATION, btn);
    init_button_mapping();
}

static void handle_steering_position(void) {
    enum BUTTON_CONFIGURATION btn = settings_read(BTN_CONFIGURATION);
    switch (btn) {
        case BUTTON_CONFIGURATION_VERTICAL_LEFT:
            btn = BUTTON_CONFIGURATION_VERTICAL_RIGHT;
            break;
        case BUTTON_CONFIGURATION_VERTICAL_RIGHT:
            btn = BUTTON_CONFIGURATION_VERTICAL_LEFT;
            break;
        case BUTTON_CONFIGURATION_HORIZONTAL_LEFT:
            btn = BUTTON_CONFIGURATION_HORIZONTAL_RIGHT;
            break;
        case BUTTON_CONFIGURATION_HORIZONTAL_RIGHT:
            btn = BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
            break;
        default:
            LOG_DEF_NORMAL("%s no valid config found.\r\n", __PRETTY_FUNCTION__);
            btn = BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
            break;
    }
    settings_write(BTN_CONFIGURATION, btn);
    init_button_mapping();
}

static void handle_factory_settings(void) {
    LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
    settings_factory_reset();
    factory_reset_activate();
}

static uint8_t edited_row = 0;
void handle_settings_submenu(const uint8_t menu_num, const uint8_t menu_index, const uint8_t row_pressed) {
    (void)menu_index;
    (void)row_pressed;
    if (menu_num == 0) {
        enable_radio_menu_refresh(alpine_settings_menu_manager);
        menu_walking_push(INTERFACE_MENU_VERS);
    } else if (menu_num == INTERFACE_MENU_VERS) {
        switch (row_pressed) {
            case INTERFACE_MENU_ROWS_IGNITION:
                handle_ignition_setting_change();
                break;
            case INTERFACE_MENU_ROWS_REVERSE:
                handle_reverse_setting_change();
                break;
            case INTERFACE_MENU_LED_COLOR:
                handle_leds_setting_change();
                break;
            case INTERFACE_MENU_PREFLIGHT:
                handle_preflight_check();
                break;
            case INTERFACE_MENU_BUTTON_POS:
                handle_button_position();
                break;
            case INTERFACE_MENU_STEERING_POS:
                handle_steering_position();
                break;
            case INTERFACE_MENU_FACTORY:
                handle_factory_settings();
                break;
            default:
                edited_row = row_pressed;
                break;
        }
        enable_radio_menu_refresh(alpine_settings_menu_manager);
        menu_walking_push(INTERFACE_MENU_VERS);
    }
}

void handle_settings_value_received(const uint8_t row_pressed, const uint8_t menu_index) {
    (void)menu_index;
    switch (edited_row) {
        case INTERFACE_MENU_ROWS_IGNITION_DELAY:
            //Timing sottochiave
            settings_write(RADIO_SETTINGS_IGN_MINUTE_BYTE, row_pressed);
            break;
        case INTERFACE_MENU_ROWS_REVERSE_ENTER:
            //Timing reverse
            settings_write(RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE, row_pressed);
            break;
        case INTERFACE_MENU_ROWS_REVERSE_EXIT:
            settings_write(RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE, row_pressed);
            break;
    }
}

void alpine_settings_menu_fill_data(menu_manager_t mngr) {
    alpine_settings_menu_manager = mngr;
    menu_manager_enable_bit_func(alpine_settings_menu_manager, 0, menu_settings_send_header);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION, menu_settings_send_ignition_logic);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION_DELAY, menu_settings_send_ignition_logic_delay);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE, menu_settings_send_reverse_logic);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_ENTER, menu_settings_send_reverse_logic_delay_enter);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_EXIT, menu_settings_send_reverse_logic_delay_exit);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_LED_COLOR, menu_settings_send_led_color);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_PREFLIGHT, menu_settings_send_preflight_check);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_BUTTON_POS, menu_settings_send_button_configuration);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_STEERING_POS, menu_settings_send_steering_configuration);
    menu_manager_enable_bit_func(alpine_settings_menu_manager, INTERFACE_MENU_FACTORY, menu_settings_send_factory_reset);

    if (preflight_disable) {
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_PREFLIGHT);
    }
    if (ignition_logic_disable) {
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION);
    }

    if (reverse_lines_disable) {
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE);
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_ENTER);
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_REVERSE_EXIT);
    }

    if (ignition_lines_disable) {
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION);
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_ROWS_IGNITION_DELAY);
    }

    if (buttons_lines_disable) {
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_LED_COLOR);
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_BUTTON_POS);
        menu_manager_clear_active_bit(alpine_settings_menu_manager, INTERFACE_MENU_STEERING_POS);
    }
}
