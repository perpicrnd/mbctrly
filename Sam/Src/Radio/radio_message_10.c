#include "event_list.h"
#include "ignition.h"
#include "radio_message.h"
#include "watchdog.h"
#include "wisker.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
static enum IGNITION_STATUS ignition = IGNITION_OFF;
static enum HANDBRAKE_STATUS handbrake = HANDBRAKE_OFF;
static enum REVERSE_STATUS reverse = REVERSE_OFF;
static enum LIGHTS_STATUS lights = LIGHTS_OFF;
static uint32_t speed = 0;
static uint8_t counter_tx = 10;

static bool event_message_10_transmit(union ALPINE_MESSAGE *buff, uint8_t counter) {
    return message_10_transmit(&buff->as_10, counter);
}

void handle_ignition(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_IGNITION_CHANGED) {
        if (ignition != msg->as_ignition.ignition) {
            LOG_DEF_NORMAL("Cambio di ignition\r\n");
            event_radio_request_send_func_emit(event_message_10_transmit);
        }
        ignition = msg->as_ignition.ignition;
        if (is_radio_turned_on()) {
            counter_tx = 10;
        }
    }
}

static bool handbrake_uses_speed = false;
void handle_handbrake(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_HANDBRAKE_CHANGED) {
        if (msg->as_handbrake.module == HANDBRAKE_MODULE_SPEED) {
            handbrake_uses_speed = true;
        }
        if (msg->as_handbrake.module == HANDBRAKE_MODULE_VEHICLE) {
            if (handbrake_uses_speed) {
                return;
            }
        }
        if (handbrake != msg->as_handbrake.handbrake) {
            LOG_DEF_NORMAL("Cambio di handbrake\r\n");
            event_radio_request_send_func_emit(event_message_10_transmit);
        }
        handbrake = msg->as_handbrake.handbrake;
    }
}

void handle_reverse(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_REVERSE_CHANGED) {
        if (reverse != msg->as_reverse.reverse) {
            LOG_DEF_NORMAL("Cambio di reverse\r\n");
            event_radio_request_send_func_emit(event_message_10_transmit);
        }
        reverse = msg->as_reverse.reverse;
    }
}

void handle_light(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_LIGHTS_CHANGED) {
        if (lights != msg->as_lights.lights) {
            LOG_DEF_NORMAL("Cambio di lights\r\n");
            event_radio_request_send_func_emit(event_message_10_transmit);
        }
        lights = msg->as_lights.lights;
    }
}

void handle_speed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SPEED_CHANGED) {
        if (speed != msg->as_speed.speed) {
            ///Disabilitato trasmissione su cambio speed.
            //			LOG_DEF_NORMAL("Cambio di speed\r\n");
            //			event_radio_request_send_func_emit(event_message_10_transmit);
        }
        speed = msg->as_speed.speed;
    }
}

void handle_timer(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (counter_tx > 0) {
            counter_tx--;
        }
    }
}

/**
 * @brief Function that encode a message 10
 * @param msg A pointer where to write
 * @param counter the sequence number to send.
 * @return Message always transmitted, return true.
 */
bool message_10_transmit(struct ALPINE_MESSAGE_10 *msg, uint8_t counter) {
    bool retVal = false;
    if (counter_tx > 0) {
        message_raw_reset(&msg->msg);
        message_raw_add_sof(&msg->msg);
        message_raw_add_uint8(&msg->msg, counter);
        message_raw_add_uint8(&msg->msg, 0x10);
        message_raw_add_uint8(&msg->msg, 0x00);
        message_raw_add_uint8(&msg->msg, 0x00);  //counter, viene riempito alla fine.

        uint8_t sign_stat = 0;
        if (ignition == IGNITION_ON) {
            sign_stat |= 0x01;  //Ignition
            sign_stat |= 0x20;  //S-Contact
            sign_stat |= 0x40;  //Engine running
        }
        //		if (lights == LIGHTS_ON){
        sign_stat |= 0x02;
        //		}
        if (reverse == REVERSE_ON) {
            sign_stat |= 0x04;
        }
        if (handbrake == HANDBRAKE_ON) {
            sign_stat |= 0x08;
        }
        message_raw_add_uint8(&msg->msg, sign_stat);
        message_raw_add_uint16(&msg->msg, speed);
        message_raw_add_uint8(&msg->msg, 0x00);
        message_raw_add_uint8(&msg->msg, 0x20);
        message_raw_update_message_len(&msg->msg);
        message_raw_crc_add(&msg->msg);
        retVal = message_raw_transmit(&msg->msg);
    } else {
        /// Se arrivo qui vuol dire che io non sto più inviando messaggi
        /// L'ignition era presente ed ora non lo è più.
        /// I messaggi verso la radio non vengono più mandati.
        watchdog_reset();
        wisker_reset(MODULE_UART_LOWLEVEL);
    }
    return retVal;
}

void message_10_connect_callback(void) {
    event_connect_callback(eEVENT_IGNITION_CHANGED, handle_ignition);
    event_connect_callback(eEVENT_HANDBRAKE_CHANGED, handle_handbrake);
    event_connect_callback(eEVENT_REVERSE_CHANGED, handle_reverse);
    event_connect_callback(eEVENT_LIGHTS_CHANGED, handle_light);
    event_connect_callback(eEVENT_SPEED_CHANGED, handle_speed);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_timer);
}

/**
 * @}
 */
