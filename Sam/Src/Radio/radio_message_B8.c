#include "radio_message.h"
#include "event_list.h"
#include "vehicle.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
static inline enum LANGUAGE_DEFINITION message_B8_language(struct ALPINE_MESSAGE_B8* msg)
{
    LOG_DEF_NORMAL("New language: %d\r\n", msg->msg.data[COMMAND_POS + 3]);
    enum LANGUAGE_DEFINITION retVal = LANGUAGE_DEFINITION_ENGLISH;
    switch (msg->msg.data[COMMAND_POS + 3]) {
    case LANGUAGE_GERMAN:
        retVal = LANGUAGE_DEFINITION_GERMAN;
        break;
    case LANGUAGE_ENGLISH:
        retVal = LANGUAGE_DEFINITION_ENGLISH;
        break;
    case LANGUAGE_ENGLISH_US:
        retVal = LANGUAGE_DEFINITION_ENGLISH_US;
        break;
    case LANGUAGE_FRENCH:
        retVal = LANGUAGE_DEFINITION_FRENCH;
        break;
    case LANGUAGE_ITALIAN:
        retVal = LANGUAGE_DEFINITION_ITALIAN;
        break;
    case LANGUAGE_SPANISH:
        retVal = LANGUAGE_DEFINITION_SPANISH;
        break;
    case LANGUAGE_PORTUGUESE:
        retVal = LANGUAGE_DEFINITION_PORTUGUESE;
        break;
    case LANGUAGE_POLISH:
        retVal = LANGUAGE_DEFINITION_POLISH;
        break;
    case LANGUAGE_CZECH:
        retVal = LANGUAGE_DEFINITION_CZECH;
        break;
    case LANGUAGE_SWEDISH:
        retVal = LANGUAGE_DEFINITION_SWEDISH;
        break;
    case LANGUAGE_DUTCH:
        retVal = LANGUAGE_DEFINITION_DUTCH;
        break;
    case LANGUAGE_JAPANESE:
        retVal = LANGUAGE_DEFINITION_JAPANESE;
        break;
    case LANGUAGE_RUSSIAN:
        retVal = LANGUAGE_DEFINITION_RUSSIAN;
        break;
    case LANGUAGE_KOREAN:
        retVal = LANGUAGE_DEFINITION_KOREAN;
        break;
    case LANGUAGE_TURKISH:
        retVal = LANGUAGE_DEFINITION_TURKISH;
        break;
    case LANGUAGE_CHINESE:
        retVal = LANGUAGE_DEFINITION_CHINESE;
        break;
    case LANGUAGE_NORWEGIAN:
        retVal = LANGUAGE_DEFINITION_NORWEGIAN;
        break;
    case LANGUAGE_SLOVAKIAN:
        retVal = LANGUAGE_DEFINITION_SLOVAKIAN;
        break;
    case LANGUAGE_HUNGARIAN:
        retVal = LANGUAGE_DEFINITION_HUNGARIAN;
        break;
    }
    return retVal;
}

void message_B8_process(struct ALPINE_MESSAGE_B8 *msg){
	event_language_changed_emit(message_B8_language(msg));
}

/**
 * @}
 */
