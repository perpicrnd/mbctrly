#include "radio_message_C0.h"
#include "event_list.h"


/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

static inline enum  RADIO_SOURCE_TYPE message_C0_get_source_type(struct ALPINE_MESSAGE_C0 *msg){
	return msg->msg.data[COMMAND_POS+3];
}

static inline enum RADIO_MEDIA_TYPE message_C0_get_media_type(struct ALPINE_MESSAGE_C0 *msg){
	return msg->msg.data[COMMAND_POS+4];
}

void message_C0_process(struct ALPINE_MESSAGE_C0 *msg){
	event_radio_media_changed_emit(message_C0_get_media_type(msg),
			message_C0_get_source_type(msg),
			msg->msg.data[COMMAND_POS+5],
			msg->msg.data[COMMAND_POS+6],
			msg->msg.data[COMMAND_POS+7],
			msg->msg.data[COMMAND_POS+8],
			msg->msg.data[COMMAND_POS+9],
			msg->msg.data[COMMAND_POS+10]
	);
}

/**
 * @}
 */
