#include "radio_message_B6.h"
#include "event_list.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */



void message_B6_process(struct ALPINE_MESSAGE_B6 *msg) {
	event_time_changed_emit(msg->msg.data[COMMAND_POS + 3], msg->msg.data[COMMAND_POS + 4], msg->msg.data[COMMAND_POS + 5], msg->msg.data[COMMAND_POS + 6], 2000+msg->msg.data[COMMAND_POS + 7]);
}

/**
 * @}
 */
