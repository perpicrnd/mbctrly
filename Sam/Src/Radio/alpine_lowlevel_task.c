#include "FreeRTOS.h"
#include "alpine_task.h"
#include "event_list.h"
#include "logger.h"
#include "radio_lowlevel_task.h"
#include "radio_message.h"
#include "semphr.h"
#include "serials.h"
#include "timers.h"
#include "vehicle.h"
#include "watchdog.h"
#include "wisker.h"

/**
 * @brief ACK and NACK string format in flash.
 */
static const uint8_t acknack[] = "%c%c%c%c";
/**
 * @brief ACK and NACK string HEX format, used for debugging purposes.
 */
#ifdef __amd64__
static const char acknackhex[] = "\t%05u\t%02X%02X%02X%02X\r\n";
#else
static const char acknackhex[] = "\t%05lu\t%02X%02X%02X%02X\r\n";
#endif

extern struct SERIAL uart;
static struct SERIAL *alpine_serial;

/**
 * @brief Contains the last received message.
 */
static union ALPINE_MESSAGE recv_message;

/**
 * Variable that contains the last received sequence number.
 */
static uint8_t seq_num_recv_pvt = 0;

/**
 * @brief Provides the last sequence number received by the radio.
 * @return the last received sequence number.
 */
static uint8_t seq_num_recv(void) {
    if (seq_num_recv_pvt > 0x1F) {
        seq_num_recv_pvt = 1;
    }
    return seq_num_recv_pvt;
}

/**
 * @brief Sets the received sequence number.
 * @param num the sequence number.
 */
static void seq_num_recv_set(uint8_t num) {
    seq_num_recv_pvt = num;
}

/**
 * @brief timer that is fired after 160msec from the transmission of a message if no ack/nack is received.
 * @param pxTimer not used.
 */
static void timer_msec160(TimerHandle_t pxTimer) {
    (void)pxTimer;
    ///Release the semaphore for unlock the radio task.
    //Non ho un sequence number sul timeout.
    event_acknack_emit(EVENT_ACKNACK_TIMEOUT, 0);
}

/**
 * @brief Timer handle for the TIMEOUT.
 */
static TimerHandle_t msec160;
/**
 * @brief Transmitting buffer length.
 *
 */
#define BUFF_LEN 254
/**
 * @brief Buffer that contains the raw message to be transmitted.
 */
uint8_t buffer[BUFF_LEN];

/**
 * Sends a message via UART to the radio.
 * The message that is sent via UART must not contains any 0xC7. The function take care of the byte stuffing algorithm.
 */
void radio_transmit_message(uint8_t *msg, uint8_t len) {
    uint8_t j = 0;

    for (uint8_t i = 0; i < len && j < BUFF_LEN; i++, j++) {
        ///For loop, i<len, j<final buffer size.
        if (msg[i] == 0xC7) {
            if (i != 0) {
                ///If a 0xC7 not in pos 0, change with 0xD8 0xAA
                buffer[j++] = 0xD8;
                buffer[j] = 0xAA;
            } else {
                buffer[j] = 0xC7;
            }
        } else if (msg[i] == 0xD8) {
            ///D8 in the message? We send 0xD8 0xD8
            buffer[j++] = 0xD8;
            buffer[j] = 0xD8;
        } else {
            ///otherwise copy the value in the sending buffer.
            buffer[j] = msg[i];
        }
    }
    ///Debugging pring on the UARTD
#ifdef __amd64__
    LOG_UART_INFO("\t%05u\t", xTaskGetTickCount());
#else
    LOG_UART_INFO("\t%05lu\t", xTaskGetTickCount());
#endif
    for (uint8_t i = 0; i < j; i++) {
        LOG_UART_INFO("%02X", buffer[i]);
    }
    LOG_UART_INFO("\r\n");

    ///Send the message on UARTC
    alpine_serial->put_string(alpine_serial, buffer, j);
    for (uint8_t i = 0; i < BUFF_LEN; i++) {
        buffer[i] = 0xFF;
    }
    ///Start the timeout detection.
    xTimerStart(msec160, 0);
}

/**
 * Task that handle the reception of the messages from the radio.
 */
static void isr_task(void *parameter) {
    (void)parameter;
    LOG_UART_INFO("Alpine Lowlevel\r\n");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    ///Starts with an empty buffer.
    message_raw_reset(&recv_message.as_raw);
    while (FOREVER()) {
        ///Radio does not reply to us for some time as start up.
        /// Keep resetting the watchdog after the power up for some time.

        unsigned char c;
        ///Read a char from the UART
        if (xStreamBufferReceive(alpine_serial->xBufferRxUartMessage, &c, 1, 10)) {
            ///Add the message to the buffer. It must handle the 0xC7 byte stuffing
            message_raw_add_uint8_from_extern(&recv_message.as_raw, c);

            if (message_raw_completed(&recv_message.as_raw)) {
                ///if we have a completed message we print it on the debugging serial interface
#ifdef __amd64__
                LOG_UART_INFO("\t%05u\t\t\t\t ", xTaskGetTickCount());
#else
                LOG_UART_INFO("\t%05lu\t\t\t\t ", xTaskGetTickCount());
#endif
                for (uint8_t i = 0; i < recv_message.as_raw.pos; i++) {
                    LOG_UART_INFO("%02X", recv_message.as_raw.data[i]);
                }
                LOG_UART_INFO("\r\n");
                ///We obtain the sequence number of the received message.
                uint8_t tmp = message_raw_seq_number(&recv_message.as_raw);
                ///And we save it internally.
                if (tmp <= 0x1F) {
                    seq_num_recv_set(tmp);
                }
            }
        } else {
            ///No chars received? Sleep for a couple of ms.
            vTaskDelay(2 / portTICK_PERIOD_MS);
        }

        ///If we have a completed message
        if (message_raw_completed(&recv_message.as_raw)) {
            ///Short sleep before do anything
            vTaskDelay(5 / portTICK_PERIOD_MS);
            if (message_raw_is_acknack(&recv_message.as_raw)) {
                ///We receive an ack/nack? transmission to the radio and back is ok. Reset the watchdog.
                wisker_reset(MODULE_UART_LOWLEVEL);
                watchdog_reset();
                ///Then we save the received ack flag for the high level task.
                uint8_t sn_recv = message_raw_seq_number(&recv_message.as_raw);
                if (message_raw_acknack_is_ack(&recv_message.as_raw)) {
                    if (message_raw_crc_check(&recv_message.as_raw) == CRC_OK) {
                        ///ACK Received with a correct CRC
                        message_raw_reset(&recv_message.as_raw);
                        event_acknack_emit(EVENT_ACKNACK_ACK, sn_recv);
                    } else {
                        ///ACK Received with a wrong CRC. I re-send my message.
                        message_raw_reset(&recv_message.as_raw);
                        event_acknack_emit(EVENT_ACKNACK_NACK, sn_recv);
                    }
                } else {
                    ///If it is not an ACK is a NACK. Save the NACK flag and notify it.
                    message_raw_reset(&recv_message.as_raw);
                    event_acknack_emit(EVENT_ACKNACK_NACK, sn_recv);
                }
                ///ACK/NACK received. Stop the TIMEOUT detection.
                xTimerStop(msec160, 10);
                ///Unlock the high level task.
            } else {
                ///If Received message is not an ACK NACK
                if (message_raw_crc_check(&recv_message.as_raw) == CRC_OK) {
                    ///Send back to the radio an ACK if the CRC is correct.
                    ///
                    alpine_serial->printf(alpine_serial, (const char *)acknack, 0xC7, 0xFF, seq_num_recv(), 0xFF ^ seq_num_recv());
                    // serial_printf(acknack, 0xC7, 0xFF, seq_num_recv(), 0xFF^seq_num_recv());
                    LOG_UART_INFO(acknackhex, xTaskGetTickCount(), 0xC7, 0xFF, seq_num_recv(), 0xFF ^ seq_num_recv());

                    ///Check if we received a request message from the radio.
                    uint8_t got_request_message = message_raw_command(&recv_message.as_raw);
                    if (got_request_message != 0xFF) {
                        ///And then ask the HIGH LEVEL task to handle the request.
                        switch (got_request_message) {
                            case 0x90:
                                ///Request interface informations
                                event_radio_request_emit(message_90_get_requested_mode(&recv_message.as_90));
                                break;
                            case 0x91:
                                ///Request menu
                                event_menu_request_emit_91(message_91_get_requested_mode(&recv_message.as_91), 0, 0);
                                break;
                            case 0xB6:
                                message_B6_process(&recv_message.as_b6);
                                break;
                            case 0xB7:
                                message_B7_process(&recv_message.as_b7);
                                break;
                            case 0xB8:
                                message_B8_process(&recv_message.as_b8);
                                break;
                            case 0xC0:
                                message_C0_process(&recv_message.as_c0);
                                break;
                            case 0xC1:
                                message_C1_process(&recv_message.as_c1);
                                break;
                            case 0xC3:
                                message_C3_process(&recv_message.as_c3);
                                break;
                            case 0xC4:
                                message_C4_process(&recv_message.as_c4);
                                break;
                            case 0xC5:
                                message_C5_process(&recv_message.as_c5);
                                break;
                            case 0xC6:
                                message_C6_process(&recv_message.as_c6);
                                break;
                            case 0xD0:
                                event_compass_emit(message_D0_get_compass(&recv_message.as_d0));
                                break;
                            case 0xD5:
                                event_radio_volume_emit(message_D5_get_volume(&recv_message.as_d5));
                                break;
                            case 0xE5:
                                message_E5_get_requested_mode(&recv_message.as_e5);
                                break;
                            case 0xE8:
                                message_E8_get_requested_mode(&recv_message.as_e8);
                                break;
                            default:
                                LOG_UART_ERROR("request not implemented %02X\r\n", got_request_message);
                                for (uint8_t i = 0; i < recv_message.as_raw.pos; i++) {
                                    LOG_UART_ERROR("%02X", recv_message.as_raw.data[i]);
                                }
                                LOG_UART_ERROR("\r\n");
                                break;
                        }
                    }
                    ///Then reset the message.
                    message_raw_reset(&recv_message.as_raw);
                } else {
                    ///The message CRC does not match. Send a NACK to the radio
                    alpine_serial->printf(alpine_serial, (const char *)acknack, 0xC7, 0xF0, seq_num_recv(), 0xFF ^ seq_num_recv());
                    LOG_UART_ERROR(acknackhex, xTaskGetTickCount(), 0xC7, 0xF0, seq_num_recv(), 0xFF ^ seq_num_recv());
                    ///Then reset the message.
                    message_raw_reset(&recv_message.as_raw);
                }
            }
        }
    }
#if __amd64__
    vTaskEndScheduler();
#endif
}

void radio_lowlevel_init(void) {
    LOG_UART_INFO("Alpine Lowlevel init\r\n");
    LOG_UART_DEBUG("serial init\r\n");
    alpine_serial = &uart;
    alpine_serial->init(alpine_serial, 115200, 'N');
    xTaskCreate(isr_task, (char *)"alpine_low", configMINIMAL_STACK_SIZE + 600, NULL, configMAX_PRIORITIES - 1, NULL);
    msec160 = xTimerCreate((char *)"t_160", 160 / portTICK_PERIOD_MS, pdFALSE, 0, timer_msec160);
}

void event_acknack_emit(enum EVENT_ACKNACK acknack, uint8_t msg_num) {
    union EVENT event;
    event.as_generic.event = eEVENT_ACKNACK_CHANGED;
    event.as_acknack.acknack = acknack;
    event.as_acknack.seq_num = msg_num;
    event_emit(&event);
}

void event_time_format_changed_emit(uint8_t time_mode, uint8_t date_mode) {
    union EVENT event;
    event.as_generic.event = eEVENT_TIME_FORMAT_CHANGED;
    event.as_time_format.time_format = time_mode;
    event.as_time_format.date_format = date_mode;
    event_emit(&event);
}

void event_language_changed_emit(enum LANGUAGE_DEFINITION lang) {
    union EVENT event;
    event.as_generic.event = eEVENT_LANGUAGE_CHANGED;
    event.as_language.language = lang;
    event_emit(&event);
}

void event_radio_volume_emit(uint8_t volume_level) {
    union EVENT event;
    event.as_generic.event = eEVENT_VOLUME_LEVEL;
    event.as_radio_volume.volume_level = volume_level;
    event_emit(&event);
}

void event_time_changed_emit(uint8_t hour, uint8_t minute, uint8_t day, uint8_t month, uint16_t year) {
    union EVENT event;
    event.as_generic.event = eEVENT_TIME_CHANGED;
    event.as_time.hour = hour;
    event.as_time.minute = minute;
    event.as_time.day = day;
    event.as_time.month = month;
    event.as_time.year = year;
    event_emit(&event);
}
/**
 * @}
 */
