#include <stdlib.h>
#include "vehicle.h"
#include "logger.h"
#include "event_list.h"
#include "settings.h"
#include "controller.h"
#include "buttons.h"

static VEHICLE_CHANGE_TIME_DATE callback = NULL;

void set_vehicle_change_date_func(VEHICLE_CHANGE_TIME_DATE func)
{
    callback = func;
}

void vehicle_send_changed_clock_date(uint16_t vehicle_year, uint8_t vehicle_month, uint8_t vehicle_day, uint8_t vehicle_hour, uint8_t vehicle_minute)
{
    if (callback != NULL) {
        callback(vehicle_year, vehicle_month, vehicle_day, vehicle_hour, vehicle_minute);
    } else {
        LOG_DEF_ERROR("No clock change function available.\r\n");
    }
}

void event_vehicle_time_changed_emit(uint8_t hour, uint8_t minute, uint8_t day, uint8_t month, uint16_t year)
{
    union EVENT event;
    event.as_generic.event = eEVENT_VEHICLE_TIME_CHANGED;
    event.as_time.hour = hour;
    event.as_time.minute = minute;
    event.as_time.day = day;
    event.as_time.month = month;
    event.as_time.year = year;
    event_emit(&event);
}

enum LED_COLOR vehicle_get_default_led_color()__attribute__ ((weak));
enum LED_COLOR vehicle_get_default_led_color(){
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return LED_COLOR_WHITE;
}