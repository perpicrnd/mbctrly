#include "radio_message_C4.h"
#include "event_list.h"


/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

static inline enum PHONE_STATUS message_C4_get_phone_status(struct ALPINE_MESSAGE_C4 *msg){
	return msg->msg.data[COMMAND_POS+3] & 0x0F;
}

static inline uint8_t message_C4_get_info_bits(struct ALPINE_MESSAGE_C4 *msg){
	return msg->msg.data[COMMAND_POS+4];
}

static inline uint8_t message_C4_get_signals(struct ALPINE_MESSAGE_C4 *msg){
	return msg->msg.data[COMMAND_POS+5];
}


void message_C4_process(struct ALPINE_MESSAGE_C4 *msg){
	event_phone_info_emit(message_C4_get_phone_status(msg), message_C4_get_info_bits(msg), message_C4_get_signals(msg));
}

/**
 * @}
 */
