#include "FreeRTOS.h"
#include "alpine_menu.h"
#include "menu_manager.h"
#include "timers.h"

menu_manager_t last_tx_menu = NULL;
menu_manager_t menu_to_send = NULL;
static TimerHandle_t timer_menu_refresh;

/**
 * @brief Timer utilizzato per ritardare la ritrasmissione del menu.
 * Il menu una volta premuto un pulsante deve essere ritrasmesso alla radio.
 * Io utilizzo delle variabili temporanee che vengono sovrascritte sia da veicolo e
 * radio.
 * Se il menu viene ritrasmesso subito ho il problema che il dato che io reinvio è quello
 * appena salvato. Quindi se un optional non è presente cambierei comunque il valore presentato all'utente.
 * Ritardando la ritrasmissione alla radio io ho un trasferimento del messaggio canbus verso il veicolo e
 * quindi una sua ritrasmissione verso di me. Se il dato non è stato cambiato dal veicolo vuol dire che
 * l'optional non è installato e quindi non cambio il valore.
 * @param pxTimer non usato.
 */
static void vTimerMenuRefresh(TimerHandle_t pxTimer) {
    (void)pxTimer;
    if (menu_to_send != NULL) {
        event_menu_send_emit(menu_to_send);
        last_tx_menu = menu_to_send;
        menu_to_send = NULL;
    }
}

void redraw_last_menu(void) {
    radio_menu_send_queue_clear();
    enable_radio_menu_refresh(last_tx_menu);
}

/**
 * @brief Abilita la ritrasmissione di un menu.
 * @param func il menu da ritrasmettere.
 */
void enable_radio_menu_refresh(menu_manager_t menu) {
    menu_to_send = menu;
    radio_menu_send_queue_clear();
    xTimerStart(timer_menu_refresh, 5);
}

void menu_refresher_init(void) {
    timer_menu_refresh = xTimerCreate((char *)"t_mrel", 500 / portTICK_PERIOD_MS, pdFALSE, 0, vTimerMenuRefresh);
}

void event_menu_send_emit(menu_manager_t menu) {
    union EVENT event;
    event.as_generic.event = eEVENT_MENU_SEND;
    event.as_menu_send.menu = menu;
    event_emit(&event);
    event_emit(&event);
}
