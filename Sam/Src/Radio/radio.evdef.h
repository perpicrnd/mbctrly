#pragma once

union ALPINE_MESSAGE;
struct ALPINE_RAW_MESSAGE;
struct ALPINE_MESSAGE_10;
struct ALPINE_MESSAGE_11;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_1A;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_12;
struct ALPINE_MESSAGE_30;
struct ALPINE_MESSAGE_40;
struct ALPINE_MESSAGE_90;
struct ALPINE_MESSAGE_91;
struct ALPINE_MESSAGE_B6;
struct ALPINE_MESSAGE_B7;
struct ALPINE_MESSAGE_B8;
struct ALPINE_MESSAGE_C0;
struct ALPINE_MESSAGE_C1;
struct ALPINE_MESSAGE_C3;
struct ALPINE_MESSAGE_C4;
struct ALPINE_MESSAGE_C5;
struct ALPINE_MESSAGE_C6;
struct ALPINE_MESSAGE_D0;
struct ALPINE_MESSAGE_D5;
struct ALPINE_MESSAGE_E5;
struct ALPINE_MESSAGE_E5;

/**
 * @brief evento di tipo velocità veicolo
 */
struct EVENT_COMPASS {
    /**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Velocità del veicolo in km/h
	 */
    uint16_t angle;
};

/**
 * @brief emette un messaggio contenente la direzione attuale
 * @param heading il valore della direzione attuale in gradi.
 */
void event_compass_emit(uint16_t heading);

enum FAN_DIR {
    FAN_DIR_WINDSCREEN = 0x01,
    FAN_DIR_FACE = 0x02,
    FAN_DIR_FEET = 0x04,
    FAN_DIR_AUTO = 0x08,
    FAN_DIR_FACE_FEET = FAN_DIR_FACE | FAN_DIR_FEET,
    FAN_DIR_FEET_WINDSCREEN = FAN_DIR_FEET | FAN_DIR_WINDSCREEN,

};

enum HEATING_POWER {
    HEATING_POWER_OFF = 0,
    HEATING_POWER_LOW,
    HEATING_POWER_MID,
    HEATING_POWER_HIGH
};

union SEAT_HEATING {
    struct {
        enum HEATING_POWER right : 4;
        enum HEATING_POWER left : 4;
    };
    uint8_t as_uint8;
};

union HVAC_INDICATORS {
    struct {
        uint8_t hvac_system_status : 1;
        uint8_t rear_window_heating : 1;
        uint8_t front_window_heating : 1;
        uint8_t air_conditioning : 1;
        uint8_t automatic_air_circulation : 1;
        uint8_t automatic_supplementary_heating : 1;
        uint8_t temperature_format : 1;
        uint8_t reserved : 1;
    };
    uint8_t as_uint8;
};

enum HVAC_PROFILE_VAL {
    HVAC_PROFILE_VAL_LOW = 0,
    HVAC_PROFILE_VAL_MID,
    HVAC_PROFILE_VAL_HIGH
};

union HVAC_PROFILE {
    struct {
        enum HVAC_PROFILE_VAL profile : 4;
        uint8_t reserved : 3;
        uint8_t setup_screen : 1;
    };
    uint8_t as_uint8;
};

union HVAC_FEATURES {
    struct {
        uint8_t rear_window_heating : 1;
        uint8_t front_window_heating : 1;
        uint8_t ac_control : 1;
        uint8_t automatic_air_circulation : 1;
        uint8_t seat_heating : 1;
        uint8_t auto_supplementary_heating : 1;
        uint8_t reserved : 2;
    };
    uint8_t as_uint8;
};

struct HVAC_INFO {
    struct EVENT_GENERIC generic;
    uint8_t left_temperature;
    uint8_t right_temperature;
    uint8_t fan_speed;
    enum FAN_DIR fan_direction;
    union SEAT_HEATING seat_heating;
    union HVAC_INDICATORS indicators;
    union HVAC_PROFILE profile;
    union HVAC_FEATURES features;
};

/**
 * @brief emette un messaggio di visualizzazione del clima della vettura
 * 
 * @param info le informazioni raccolte relative al clima.
 */
void event_hvac_changed_emit(const struct HVAC_INFO *const info);

typedef enum {
    MENU_REQUEST_NUM_91 = 0,
    MENU_REQUEST_NUM_E5,
    MENU_REQUEST_NUM_E8
} MENU_REQUEST_NUM;

typedef struct {
    /**
	 * numero del menu richiesto.
	 */
    uint8_t row_pressed_num;
    /**
	 * indice del dato toccato / modificato / selezionato.
	 */
    uint8_t menu_index;
    /**
	 * Valore del dato impostato sul menu_index.
	 * Value / Index: As specified by the appropriate item type in the “Menu data” command. For example:
	 * - for the first item of a drop down, it would be 0x00;
	 * - for a checkbox with a tick, 0x01,
	 * - for a checkbox without a tick, 0x00.
	 */
    uint8_t menu_data;
} MENU_REQUEST_91_STR;

typedef struct {
    /**
	 * numero del menu richiesto.
	 */
    uint8_t row_pressed_num;
    /**
	 * indice del dato toccato / modificato / selezionato.
	 */
    uint8_t menu_index;
    /**
	 * Valore del dato impostato sul menu_index.
	 * Value / Index: As specified by the appropriate item type in the “Menu data” command. For example:
	 * - for the first item of a drop down, it would be 0x00;
	 * - for a checkbox with a tick, 0x01,
	 * - for a checkbox without a tick, 0x00.
	 */
    uint8_t menu_data;
} MENU_REQUEST_E5_STR;

typedef struct {
    /**
	 * numero del menu richiesto.
	 */
    uint8_t row_pressed_num;
    /**
	 * indice del dato toccato / modificato / selezionato.
	 */
    uint8_t menu_index;
} MENU_REQUEST_E8_STR;

typedef union {
    MENU_REQUEST_91_STR as_91;
    MENU_REQUEST_E5_STR as_e5;
    MENU_REQUEST_E8_STR as_e8;
} MENU_REQUESTS;

/**
 * @brief richiesta di visualizzare un menu da parte della radio.
 * La radio può richiedere la visualizzazione di un menu da parte dell'interfaccia.
 * Dato che il menu può essere dipendente dal veicolo utilizzato ha senso inviare un evento che richieda un modo
 * per poter inviare un menu.
 * Quando la radio richiede l'invio di un menu viene emesso questo evento pasando il menu_request_num come parametro.
 * Ogni modulo avrà un determinato numero di menu che può visualizzare ed il numero di ogni menu sarà univoco.
 * Questo risulterà in una sola funzione che sarà in grado di fornire un evento di tipo struct EVENT_MENU_SEND che verrà
 * reinviato nell'event system.
 */
struct EVENT_MENU_REQUEST {
    /**
	 * Evento base
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Il tipo di messaggio che posso ricevere. O 0x91 o 0xE5 o 0xE8. In base a questo i dati sono differenti.
	 */
    MENU_REQUEST_NUM msg_type;
    MENU_REQUESTS request;
};

/**
 * @brief prototipo di funzione responsabile di inviare un menu alla radio.
 * La funzione responsabile di trasmettere un menù alla radio viene eseguita dal task alpine_task per far sì che
 * il sistema di ack/nack funzioni e rimanga semplice. Questo è il solo task che viene utilizzato per inviare informazioni
 * verso la radio quindi non ci sono lock o altro a cui pensare.
 *
 * Metodo di implementazione della funzione:
 * - La funzione di trasmissione del menu può inviare un solo messaggio ad ogni sua chiamata
 * - la funzione deve restituire true se qualcosa è stato inviato verso la radio
 * - la funzione deve restituire false se non è stato inviato nulla alla radio.
 * - Il chiamante incrementa automaticamente il numero row_num nel momento in cui la riga precedente è stata correttamente inviata
 *
 * @param row_num il numero della riga da inviare
 * @return true se è stato trasmesso qualcosa alla radio, false altrimenti
 */
typedef bool (*ALPINE_EVENT_MENU_SEND_FUNC)(union ALPINE_MESSAGE *msg, uint8_t row_num);

struct MENU_MANAGER;
typedef struct MENU_MANAGER *menu_manager_t;
/**
 * @brief evento di trasmissione verso la radio di un menu.
 */
struct EVENT_MENU_SEND {
    /**
	 * Evento base
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Callback chiamata dal task per effettuare la trasmissione. Questa funzione viene chiamata direttamente dal radio task.
	 * Verificare in ogni singolo modulo se servono dei lock sui dati.
	 */
    menu_manager_t menu;
};

/**
 * @brief emette un messaggio di richiesta di trasmissione di un menu.
 * @param func la funzione che effettua la trasmissione del menu.
 */

void event_menu_send_emit(menu_manager_t menu);

/**
 * @brief evento di trasmissione verso la radio di un menu.
 */
struct EVENT_WARNING_MESSAGE {
    /**
	 * Evento base
	 */
    struct EVENT_GENERIC generic;

    uint8_t chime;
    uint8_t seconds;
    const char *ptr_head;
    const char *ptr_body;
};

void event_warning_message_emit(struct EVENT_WARNING_MESSAGE *message);

union PARKING_DISTANCE_INFO {
    uint32_t as_uint;
    struct {
        uint32_t zone_d : 4;
        uint32_t zone_c : 4;
        uint32_t zone_b : 4;
        uint32_t zone_a : 4;
        uint32_t zone_h : 4;
        uint32_t zone_g : 4;
        uint32_t zone_f : 4;
        uint32_t zone_e : 4;
    };
    struct {
        uint8_t byte0;
        uint8_t byte1;
        uint8_t byte2;
        uint8_t byte3;
    };
};

union PARKING_STATUS_INFO {
    uint16_t as_uint;
    struct {
        uint32_t zone_h : 2;
        uint32_t zone_g : 2;
        uint32_t zone_f : 2;
        uint32_t zone_e : 2;
        uint32_t zone_d : 2;
        uint32_t zone_c : 2;
        uint32_t zone_b : 2;
        uint32_t zone_a : 2;
    };
    struct {
        uint8_t byte0;
        uint8_t byte1;
    };
};

union PARK_STATUS {
    struct {
        uint8_t reserved : 7;
        uint8_t active : 1;
    } as_flags;
    uint8_t as_uint8;
};

struct PARKING_SENSOR_DATA {
    union PARKING_DISTANCE_INFO distance;
    union PARKING_STATUS_INFO status;
    union PARK_STATUS park_status;
};

enum PARKING_SENSOR_STYLE {
    PARKING_SENSOR_UNKNOWN = 0,
    PARKING_SENSOR_OEM,
    PARKING_SENSOR_AFTERMARKET,
};

struct EVENT_PARKING_SENSOR {
    struct EVENT_GENERIC generic;
    union {
        struct PARKING_SENSOR_DATA data;
        uint8_t as_array[sizeof(struct PARKING_SENSOR_DATA)];
    };
    enum PARKING_SENSOR_STYLE prksens_type;
};

/**
 * @brief emette un messaggio di visualizzazione dei sensori di parcheggio.
 * @param data struct contenente le informazioni dei sensori di parcheggio.
 */
void event_parking_sensor_emit(struct EVENT_PARKING_SENSOR *data);

struct EXTERNAL_REQUEST_DATA {
    uint8_t type;
    uint8_t data;
};

struct EVENT_EXTERNAL_REQUEST {
    struct EVENT_GENERIC generic;
    struct EXTERNAL_REQUEST_DATA data;
};

/**
 * @brief emette un messaggio di cambio source tramite comando external request (comando 0x24).
 */
void event_external_request_emit(uint8_t type, uint8_t data);

/**
 * @brief Definizione dello stato del telefono
 */
enum PHONE_STATUS {
    PHONE_STATUS_INACTIVE,           //!< PHONE_STATUS_INACTIVE telefono non attivo.
    PHONE_STATUS_RINGING_INCOMING,   //!< PHONE_STATUS_RINGING_INCOMING telefono sta squillando
    PHONE_STATUS_DIALLING_OUTGOING,  //!< PHONE_STATUS_DIALLING_OUTGOING telefono in fase di composizione numero
    PHONE_STATUS_CONNECTED,          //!< PHONE_STATUS_CONNECTED telefono connesso
    PHONE_STATUS_TALKING,            //!< PHONE_STATUS_TALKING durante la telefonata
    PHONE_STATUS_ENDCALL             //!< PHONE_STATUS_ENDCALL fine telefonata
};

/**
 * @brief stato del telefono
 */
enum PHONE_SYSTEM_STATUS {
    PHONE_SYSTEM_OFF,  //!< PHONE_SYSTEM_OFF telefono OFF
    PHONE_SYSTEM_ON    //!< PHONE_SYSTEM_ON telefono ON
};

/**
 * @brief Stato del microfono
 */
enum PHONE_MIC_STATUS {
    PHONE_MIC_OFF,  //!< PHONE_MIC_OFF microfono spento
    PHONE_MIC_ON,   //!< PHONE_MIC_ON microfono acceso
};

/**
 * @brief bitfield contenente le informazioni ricevute dalla radio relativamente al telefono
 */
struct PHONE_BITS {
    /**
	 * Stato del telefono
	 */
    uint8_t phone_status : 4;
    /**
	 * Stato del sistema del telefono
	 */
    uint8_t phone_sys_status : 2;
    /**
	 * Stato del microfono
	 */
    uint8_t phone_mic_status : 2;
};

/**
 * @brief union per poter convertire un uint_8 in bitfield
 */
union uPHONE_BITS {
    /**
	 * rappresentazione come numero
	 */
    uint8_t as_uint8;
    /**
	 * rappresentazione bitfield
	 */
    struct PHONE_BITS as_struct;
};

/**
 * @brief definizione del segnale e della batteria del telefono
 */
struct PHONE_SIGNALS {
    /**
	 * Livello di carica della batteria
	 */
    uint8_t phone_battery_level : 4;
    /**
	 * Intensità del segnale del telefono
	 */
    uint8_t phone_signal : 4;
};

/**
 * @brief Union per poter convertire da bitfield a unit8_t
 */
union uPHONE_SIGNALS {
    /**
	 * rappresentazione come numero.
	 */
    uint8_t as_uint8;
    /**
	 * rappresentazione bitfield.
	 */
    struct PHONE_SIGNALS as_struct;
};

/**
 * @brief evento di tipo PHONE_INFO
 * Questo evento viene inviato dalla radio quando lo stato del telefono cambia
 */
struct EVENT_PHONE_INFO {
    /**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Stato del telefono.
	 */
    union uPHONE_BITS bits;
    /**
	 * Intensità di segnale e livello batteria.
	 */
    union uPHONE_SIGNALS signals;
};

#define PHONE_TEXT_STRING_LENGTH 30
struct EVENT_PHONE_TEXT {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Tipo di stringa:
	 * - 0 phone number - Number
	 * - 1 phone name
	 * - 0x01 ISO/IEC 8859-1 - string
	 * - 0x11 ISO/IEC 10646 - string
	 */
    uint8_t string_type;

    uint8_t string[PHONE_TEXT_STRING_LENGTH];
};

/**
 * @brief emette un messaggio indicante le informazioni del telefono.
 * @param phone_status lo stato del telefono.
 * @param mic_status lo stato del microfono del telefono.
 * @param signals lo stato dei segnali di ricezione e della batteria.
 */
void event_phone_info_emit(uint8_t phone_status, uint8_t mic_status, uint8_t signals);

enum EVENT_ACKNACK {
    EVENT_ACKNACK_ACK,
    EVENT_ACKNACK_NACK,
    EVENT_ACKNACK_TIMEOUT
};

/**
 * @brief bitfield contenente le informazioni ricevute dalla radio relativamente al telefono
 */
struct RADIO_ACKNACK {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Stato della trasmissione del messaggio.
	 */
    enum EVENT_ACKNACK acknack;
    /**
	 * Sequence number
	 */
    uint8_t seq_num;
};

/**
 * @brief emette un messaggio contenente il volume attuale della radio.
 * @param volume_level il volume corrente.
 */
void event_radio_volume_emit(uint8_t volume_level);

/**
 * @brief emette un messaggio con il nuovo orario.
 * @param hour le ore
 * @param minute i minuti
 * @param day il giorno
 * @param month il mese
 * @param year l'anno
 */
void event_time_changed_emit(uint8_t hour, uint8_t minute, uint8_t day, uint8_t month, uint16_t year);

void event_radio_media_play_status_emit(uint8_t val);

/**
 * @brief Se la radio manda in accensione il comando B7 allora la flag diventa true. In caso contrario è false.
 * 
 * @return true se la radio pilota il formato ora
 * @return false false in caso contrario.
 */
bool radio_get_handles_clock_format(void);

/**
 * @brief emette un messaggio con i formati di ora e data.
 * @param time_mode il formato dell'ora.
 * @param date_mode il formato della data.
 */
void event_time_format_changed_emit(uint8_t time_mode, uint8_t date_mode);
/**
 * @brief emette un messaggio indicante il cambio di media sulla radio.
 * @param media_type il nuovo media type.
 * @param source_type il nuovo source type.
 * @param info1 l'info1 della radio.
 * @param info2 l'info2 della radio.
 * @param info3 l'info3 della radio.
 * @param info4 l'info4 della radio.
 * @param info5 l'info5 della radio.
 * @param info6 l'info6 della radio.
 */
void event_radio_media_changed_emit(uint8_t media_type, uint8_t source_type, uint8_t info1, uint8_t info2, uint8_t info3, uint8_t info4, uint8_t info5, uint8_t info6);

/**
 * @brief sorgente visualizzata sulla radio
 */
enum RADIO_SOURCE_TYPE {
    RADIO_SOURCE_OFF = 0,         //!< RADIO_SOURCE_OFF radio spenta
    RADIO_SOURCE_TUNER,           //!< RADIO_SOURCE_TUNER tuner
    RADIO_SOURCE_DISK,            //!< RADIO_SOURCE_DISK disco
    RADIO_SOURCE_TV,              //!< RADIO_SOURCE_TV tv
    RADIO_SOURCE_NAVI,            //!< RADIO_SOURCE_NAVI navigazione
    RADIO_SOURCE_PHONE,           //!< RADIO_SOURCE_PHONE telefono
    RADIO_SOURCE_IPOD,            //!< RADIO_SOURCE_IPOD ipod
    RADIO_SOURCE_AUX,             //!< RADIO_SOURCE_AUX aux
    RADIO_SOURCE_USB,             //!< RADIO_SOURCE_USB usb
    RADIO_SOURCE_MMC,             //!< RADIO_SOURCE_MMC mmc
    RADIO_SOURCE_DVB_T,           //!< RADIO_SOURCE_DVB_T dvbt
    RADIO_SOURCE_BT_A2DP,         //!< RADIO_SOURCE_BT_A2DP bt a2dp
    RADIO_SOURCE_DAB,             //!< RADIO_SOURCE_DAB dab
    RADIO_SOURCE_HDRADIO,         //!< RADIO_SOURCE_HDRADIO hdradio
    RADIO_SOURCE_SXM,             //!< RADIO_SOURCE_SXM sxm
    RADIO_SOURCE_CARPLAY,         //!< RADIO_SOURCE_CARPLAY apple carplay
    RADIO_SOURCE_ANDROID_AUTO,    //!< RADIO_SOURCE_ANDROID_AUTO android auto
    RADIO_SOURCE_ALPINE_CONNECT,  //!< RADIO_SOURCE_ALPINE_CONNECT alpine connect
    RADIO_SOURCE_HDMI,            //!< RADIO_SOURCE_HDMI hdmi
    RADIO_SOURCE_CAMERA           //!< RADIO_SOURCE_CAMERA retrocamera
};

/**
 * @brief Tipo di media in riproduzione
 */
enum RADIO_MEDIA_TYPE {
    RADIO_MEDIA_UNKNOWN = 0,            //!< RADIO_MEDIA_UNKNOWN sconosciuto
    RADIO_MEDIA_TUNER,                  //!< RADIO_MEDIA_TUNER radio tuner
    RADIO_MEDIA_SAM = 0x10,             //!< RADIO_MEDIA_SAM Simple audio media
    RADIO_MEDIA_EAM = 0x11,             //!< RADIO_MEDIA_EAM Enhanced audio media
    RADIO_MEDIA_IPOD = 0x12,            //!< RADIO_MEDIA_IPOD ipod
    RADIO_MEDIA_FVIDEO = 0x20,          //!< RADIO_MEDIA_FVIDEO file video
    RADIO_MEDIA_DVIDEO = 0x21,          //!< RADIO_MEDIA_DVIDEO digital video
    RADIO_MEDIA_OVIDEO = 0x22,          //!< RADIO_MEDIA_OVIDEO ovideo
    RADIO_MEDIA_NAVI_AUX_OTHER = 0x30,  //!< RADIO_MEDIA_NAVI_AUX_OTHER navi/aux/altro
    RADIO_MEDIA_NAVI_TURNS = 0x31,      //!< RADIO_MEDIA_NAVI_TURNS svolte del navigatore
    RADIO_MEDIA_PHONE = 0x40            //!< RADIO_MEDIA_PHONE telefono
};

/**
 * @brief evento emesso in baso allo stato di visualizzazione sulla radio.
 */
struct EVENT_SOURCE {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Tipo di sorgente visualizzata
	 */
    enum RADIO_SOURCE_TYPE source_type;
    /**
	 * Tipo di media riprodotto
	 */
    enum RADIO_MEDIA_TYPE media_type;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info1;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info2;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info3;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info4;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info5;
    /**
	 * Byte informazioni stazione
	 */
    uint8_t info6;
};

#define MEDIA_TEXT_STRING_LENGTH 30
struct EVENT_MEDIA_TEXT {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Tipo di stringa:
	 * - 0 ISO 8859-1 - ASCII
	 * - 1 ISO 10646 - UNICODE
	 */
    uint8_t string_type;
    /**
	 * Id della stringa:
	 * tuner:
	 * - 1 Station name
	 * - 2 radio text
	 *
	 * disc / ipod / usb:
	 * - 1 album name
	 * - 2 artist name
	 * - 3 song name
	 *
	 * altri formati possono avere informazioni differenti
	 */
    uint8_t string_id;
    uint8_t string[MEDIA_TEXT_STRING_LENGTH];
};

struct EVENT_VOLUME_LEVEL {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    uint8_t volume_level;
};

struct MEDIA_PLAY_STATUS {
    struct EVENT_GENERIC generic;
    union {
        uint8_t value;
        struct {
            uint8_t volume : 7;
            uint8_t mute : 1;
        };
    };
};

/**
 * @brief evento relativo ad una richiesta effettuata dalla radio
 * La radio occasionalmente può richiedere di ricevere delle informazioni dalla scheda.
 * Dato che non tutte le informazioni sono relative alla scheda, ma alcune possono dipendere dal veicolo
 * la richiesta viene propagata tramite l'event system e se qualcuno ha delle informazioni da reinviare
 * verso la radio lo potrà fare emettendo un evento di tipo \ref EVENT_RADIO_REQUEST_SEND.
 */
struct EVENT_RADIO_REQUEST {
    struct EVENT_GENERIC generic;
    uint8_t radio_request_num;
};

/**
 * @brief Prototipo della funzione responsabile alla trasmissione di informazioni verso la radio.
 * La funzione responsabile di inviare le informazioni verso la radio deve utilizzare il buffer passato come parametro
 * per poter costruire il messaggio. Questo deve essere fatto perchè la funzione viene eseguita dall'alpine_task,
 * per semplificare la portabilità dei dati all'interno del software e poter fare i resend in caso di timeout.
 * L'array non verrà modificato da altre parti del software fino a trasmissione avvenuta.
 * Dato che la trasmissione viene effettuata dall'alpine task, eventuali strutture dati lette devono essere protette con mutex
 * all'interno del modulo che raccoglie i dati.
 * @param sending_msg_buffer Il buffer in cui salvare i dati prima di inviarli
 * @param sequence_number Il sequence number da utilizzare durante la trasmissione
 * @return true se qualcosa è stato inviato sulla seriale, false altrimenti.
 */
typedef bool (*ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC)(union ALPINE_MESSAGE *sending_msg_buffer, uint8_t sequence_number);

/**
 * @brief evento di richiesta di trasmissione di dati verso la radio.
 */
struct EVENT_RADIO_REQUEST_SEND {
    /**
	 * Evento generico
	 */
    struct EVENT_GENERIC generic;
    /**
	 * puntatore alla funzione che effettuerà la trasmissione dei dati.
	 */
    ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC func_to_call;
};

/**
 * @brief emette un messaggio di richiesta di trasmissione da parte della radio.
 * @param radio_request il numero del messaggio richiesto dalla radio.
 */
void event_radio_request_emit(uint8_t radio_request);

/**
 * @brief emette un messaggio di richiesta di trasmissione di un menu.
 * @param menu_request il menu da inviare.
 * @param menu_index l'indice nel menu che è stato premuto.
 * @param menu_data il valore presente nel menu[indice].
 */
void event_menu_request_emit_91(uint8_t menu_request, uint8_t menu_index, uint8_t menu_data);

/**
 * @brief emette un messaggio di richiesta di trasmissione di un menu.
 * @param menu_request il menu da inviare.
 * @param menu_index l'indice nel menu che è stato premuto.
 * @param menu_data il valore presente nel menu[indice].
 */
void event_menu_request_emit_E5(uint8_t menu_request, uint8_t menu_index, uint8_t menu_data);

/**
 * @brief emette un messaggio di richiesta di trasmissione di un menu.
 * @param menu_request il menu da inviare.
 * @param menu_index l'indice nel menu che è stato premuto.
 */
void event_menu_request_emit_E8(uint8_t menu_request, uint8_t menu_index);

/**
 * @brief trasmette un messaggio alla radio.
 * @param func la funzione che effettivamente trasmette il messaggio alla radio.
 */
void event_radio_request_send_func_emit(ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC func);

/**
 * @brief Definizione delle lingue gestite dall'interfaccia
 */
enum LANGUAGE_CODE {
    LANGUAGE_GERMAN = 0x00,     //!< LANGUAGE_GERMAN lingua tedesca
    LANGUAGE_ENGLISH,           //!< LANGUAGE_ENGLISH lingua inglese
    LANGUAGE_ENGLISH_US,        //!< LANGUAGE_ENGLISH_US lingua inglese (americana)
    LANGUAGE_FRENCH,            //!< LANGUAGE_FRENCH lingua francese
    LANGUAGE_ITALIAN,           //!< LANGUAGE_ITALIAN lingua italiana
    LANGUAGE_SPANISH,           //!< LANGUAGE_SPANISH lingua spagnola
    LANGUAGE_PORTUGUESE,        //!< LANGUAGE_PORTUGUESE lingua portoghese
    LANGUAGE_POLISH,            //!< LANGUAGE_POLISH lingua polacca
    LANGUAGE_CZECH,             //!< LANGUAGE_CZECH lingua ceca
    LANGUAGE_SWEDISH = 0x0B,    //!< LANGUAGE_SWEDISH lingua svedese
    LANGUAGE_DUTCH = 0x0D,      //!< LANGUAGE_DUTCH lingua olandese
    LANGUAGE_JAPANESE = 0x0F,   //!< LANGUAGE_JAPANESE lingua giapponese
    LANGUAGE_RUSSIAN = 0x10,    //!< LANGUAGE_RUSSIAN lingua russa
    LANGUAGE_KOREAN = 0x12,     //!< LANGUAGE_KOREAN lingua coreana
    LANGUAGE_TURKISH = 0x16,    //!< LANGUAGE_TURKISH lingua turca
    LANGUAGE_CHINESE = 0x17,    //!< LANGUAGE_CHINESE lingua cinese
    LANGUAGE_NORWEGIAN = 0x18,  //!< LANGUAGE_NORWEGIAN lingua norvegese
    LANGUAGE_SLOVAKIAN = 0x19,  //!< LANGUAGE_SLOVAKIAN lingua slovacca
    LANGUAGE_HUNGARIAN = 0x1A   //!< LANGUAGE_HUNGARIAN lingua ungherese
};

/**
 * @brief evento di tipo cambio lingua.
 * Evento generato dalla radio nel momento in cui viene rilevato un cambio della lingua.
 */
struct EVENT_LANGUAGE {
    struct EVENT_GENERIC generic;
    enum LANGUAGE_CODE language;
};
