#include "menu_manager.h"
#include <stdint.h>
#include <stdlib.h>
#include "logger.h"

#define NUM_SLOTS 32

struct MENU_MANAGER {
    uint32_t active_mask;
    uint32_t available_mask;
    uint32_t pos;
    ALPINE_EVENT_MENU_SEND_FUNC func_ptrs[NUM_SLOTS];
};

menu_manager_t menu_manager_create(void) {
    menu_manager_t retVal = calloc(1, sizeof(struct MENU_MANAGER));
    return retVal;
}

void menu_manager_reload(menu_manager_t menu) {
    if (menu) {
        menu->pos = 0;
    }
}

void menu_manager_set_active_bit(menu_manager_t menu, uint8_t pos) {
    if (menu) {
        menu->active_mask |= 1 << pos;
    }
}

void menu_manager_clear_active_bit(menu_manager_t menu, uint8_t pos) {
    if (menu) {
        menu->active_mask &= ~(1 << pos);
    }
}

void menu_manager_enable_bit(menu_manager_t menu, uint8_t pos) {
    if (menu) {
        menu->available_mask |= 1 << pos;
    }
}

void menu_manager_disable_bit(menu_manager_t menu, uint8_t pos) {
    if (menu) {
        menu->available_mask &= ~(1 << pos);
    }
}

void menu_manager_enable_bit_func(menu_manager_t menu, uint8_t pos, ALPINE_EVENT_MENU_SEND_FUNC func) {
    if (menu) {
        if (pos > NUM_SLOTS) {
            LOG_DEF_ERROR("Value out of range\r\n");
        } else {
            menu->func_ptrs[pos] = func;
            menu_manager_enable_bit(menu, pos);
            menu_manager_set_active_bit(menu, pos);
        }
    }
}

ALPINE_EVENT_MENU_SEND_FUNC menu_manager_iter_next(menu_manager_t menu) {
    ALPINE_EVENT_MENU_SEND_FUNC retVal = NULL;
    if (menu->pos == 0){
        LOG_DEF_NORMAL("menu %p\r\n", menu);
    }
    do {
        if (menu->available_mask & (1 << menu->pos)) {
            if (menu->active_mask & (1 << menu->pos)) {
                if (menu->func_ptrs[menu->pos]) {
                    retVal = menu->func_ptrs[menu->pos];
                }
            }
        }
        menu->pos++;
    } while ((retVal == NULL) && (menu->pos < NUM_SLOTS));
    return retVal;
}

uint8_t menu_manager_count_max(menu_manager_t menu) {
    uint8_t retVal = 0;
    for (uint8_t i = 1; i < NUM_SLOTS; i++) {
        if (menu->available_mask & (1 << i)) {
            if (menu->active_mask & (1 << i)) {
                if (menu->func_ptrs[i] != NULL) {
                    retVal++;
                }
            }
        }
    }

    return retVal;
}