
#include "event_list.h"
#include "radio_message.h"

bool message_18_generic_transmit(union ALPINE_MESSAGE *msg, uint8_t counter){
	return message_18_transmit(&msg->as_18, counter);
}

/**
 * @brief flag che definisce se il veicolo provvede a fornire un menu di configurazione proprio.
 * Questa flag viene messa a true dal veicolo nel caso in cui fornisca il menu aggiuntivo.
 */
static enum PARKING_SENSOR_TYPE parking_sensors_type = PARKING_SENSOR_TYPE_UNSUPPORTED;

void alpine_menu_parking_sensors_type(enum PARKING_SENSOR_TYPE type) {
	if (type != parking_sensors_type){
		parking_sensors_type = type;
		event_radio_request_send_func_emit(message_18_generic_transmit);
	}
}

bool message_18_transmit(struct ALPINE_MESSAGE_18 *msg, uint8_t counter){
	message_raw_reset(&msg->msg);
	message_raw_add_uint8(&msg->msg, 0xC7);
	message_raw_add_uint8(&msg->msg, 0xC8);
	message_raw_add_uint8(&msg->msg, counter);
	message_raw_add_uint8(&msg->msg, 0x18);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, 0x03);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, parking_sensors_type);
    message_raw_add_uint8(&msg->msg, 0x02);
	message_raw_crc_add(&msg->msg);
	return message_raw_transmit(&msg->msg);
}
