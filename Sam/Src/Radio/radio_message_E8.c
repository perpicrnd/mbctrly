#include "radio_message_E8.h"
#include "event_list.h"
#include "menu_walking.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

void message_E8_get_requested_mode(struct ALPINE_MESSAGE_E8 *msg){
	LOG_DEF_NORMAL("Ricevuto: ");
	message_raw_print(&msg->msg);

	event_menu_request_emit_E8(msg->msg.data[6], msg->msg.data[7]);
}


/**
 * @}
 */
