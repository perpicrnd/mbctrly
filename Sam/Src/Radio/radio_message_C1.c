#include "radio_message_C1.h"
#include "event_list.h"


/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */


void message_C1_process(struct ALPINE_MESSAGE_C1 *msg){
	LOG_DEF_NORMAL("qui\r\n");
	if (msg->msg.data[COMMAND_POS+3] == 2){
		event_radio_media_play_status_emit(msg->msg.data[COMMAND_POS+4]);
	}
}

/**
 * @}
 */
