#include "radio_message_19.h"

bool message_19_transmit(struct ALPINE_MESSAGE_19 *msg, uint8_t counter, struct PARKING_SENSOR_DATA *data){
	message_raw_reset(&msg->msg);
	message_raw_add_uint8(&msg->msg, 0xC7);
	message_raw_add_uint8(&msg->msg, 0xC8);
	message_raw_add_uint8(&msg->msg, counter);
	message_raw_add_uint8(&msg->msg, 0x19);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, 0x0E);
	message_raw_add_uint8(&msg->msg, data->distance.byte0);
	message_raw_add_uint8(&msg->msg, data->distance.byte1);
	message_raw_add_uint8(&msg->msg, data->distance.byte2);
	message_raw_add_uint8(&msg->msg, data->distance.byte3);
	message_raw_add_uint8(&msg->msg, 0x00);//data->status.byte0);
	message_raw_add_uint8(&msg->msg, data->status.byte1);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, data->park_status.as_uint8);
	message_raw_add_uint8(&msg->msg, 0xFF);
	message_raw_add_uint8(&msg->msg, 0xFF);
	message_raw_add_uint8(&msg->msg, 0xFF);
	message_raw_add_uint8(&msg->msg, 0xFF);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_add_uint8(&msg->msg, 0x00);
	message_raw_crc_add(&msg->msg);
	return message_raw_transmit(&msg->msg);
}
