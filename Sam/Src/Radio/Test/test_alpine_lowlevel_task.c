#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "alpine_lowlevel_task.c"

void vApplicationIdleHook(void){
//Make the linker happy.
}

void software_reset(void){

}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_serial_init(void){
	function_called();
}

BaseType_t __wrap_xTaskCreate(	TaskFunction_t pxTaskCode,
							const char * const pcName,
							const uint16_t usStackDepth,
							void * const pvParameters,
							UBaseType_t uxPriority,
							TaskHandle_t * const pxCreatedTask ) {
	(void)pxTaskCode;
	(void)pcName;
	(void)usStackDepth;
	(void)pvParameters;
	(void)uxPriority;
	(void)pxCreatedTask;
	// function_called();
	return pdTRUE;
}

TimerHandle_t __wrap_xTimerCreate(	const char * const pcTimerName,
								const TickType_t xTimerPeriodInTicks,
								const UBaseType_t uxAutoReload,
								void * const pvTimerID,
								TimerCallbackFunction_t pxCallbackFunction ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
{
	(void)pcTimerName;
	(void)xTimerPeriodInTicks;
	(void)uxAutoReload;
	(void)pvTimerID;
	(void)pxCallbackFunction;
	// function_called();
	return NULL;
}

void __wrap_vTaskDelay( const TickType_t xTicksToDelay ){
	(void)xTicksToDelay;
}

void __wrap_event_emit(union EVENT *event){
	(void)event;
	function_called();
}

void __wrap_vPortEndScheduler(void){

}

char *buffer_reception = NULL;
uint8_t buffer_len = 0;
uint8_t buffer_pos = 0;
size_t __wrap_xStreamBufferReceive( StreamBufferHandle_t xStreamBuffer, void *pvRxData, size_t xBufferLengthBytes, TickType_t xTicksToWait ){
	(void)xStreamBuffer;
	(void)pvRxData;
	(void)xBufferLengthBytes;
	(void)xTicksToWait;
	if (buffer_pos < buffer_len){
		memcpy(pvRxData, &buffer_reception[buffer_pos++], sizeof(char));
		return 1;
	}else{
		return 0;
	}
}

void serial_push_full_string(char *c, uint8_t len){
	buffer_reception = c;
	buffer_pos = 0;
	buffer_len = len;
}

void test_isr_task_valid_90(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\x90\x00\x01\x01\x5C", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_90(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\x90\x00\x01\x04\x5C", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_91(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\x91\x00\x02\x01\x00\x5A", 9);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_91(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\x91\x00\x02\x04\x00\x5A", 9);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_B8(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xB8\x00\x01\x04\x31", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_B8(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xB8\x00\x01\x08\x31", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_C0(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC0\x00\x08\x00\x00\x00\x00\x00\x00\x00\x00\x26", 15);
	NumLoops = 20; //Un valore maggiore di 15.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_C0(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC0\x00\x08\x00\x00\x00\x00\x00\x00\x00\x01\x26", 15);
	NumLoops = 20; //Un valore maggiore di 15.
	isr_task(NULL);
}

void test_isr_task_valid_C3(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC3\x00\x0CHello world\x00\xE3", 19);
	NumLoops = 20; //Un valore maggiore di 15.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_C3(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC3\x00\x0CHellp world\x00\xE3", 19);
	NumLoops = 20; //Un valore maggiore di 15.
	isr_task(NULL);
}

void test_isr_task_valid_C4(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC4\x00\x01\x08\x21", 8);
	NumLoops = 20; //Un valore maggiore di 15.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_C4(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x12\xC4\x00\x01\x08\x21", 8);
	NumLoops = 20; //Un valore maggiore di 15.
	isr_task(NULL);
}


void test_isr_task_valid_C5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC5\x00\x0CHello world\x00\xE1", 19);
	NumLoops = 20; //Un valore maggiore di 15.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_C5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xC5\x00\x0CHellp world\x00\xE1", 19);
	NumLoops = 20; //Un valore maggiore di 15.
	isr_task(NULL);
}

void test_isr_task_valid_D0(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xD0\x00\x02\x00\x00\x1C", 9);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_D0(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xD0\x00\x02\x01\x00\x1C", 9);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_D5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xD5\x00\x01\x04\x14", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_D5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xD5\x00\x01\x08\x14", 8);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_E5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xE5\x00\x03\x04\x01\x02\xFF", 10);
	NumLoops = 12; //Un valore maggiore di 9.
	expect_function_call(__wrap_event_emit);
	isr_task(NULL);
}

void test_isr_task_invalid_E5(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x15\xE5\x00\x03\x04\x01\x02\xFF", 10);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_valid_not_implemented(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x11\xEF\x00\x03\x04\x01\x02\xF5", 10);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}

void test_isr_task_invalid_not_implemented(void **state){
    (void)state;
	serial_push_full_string("\xC7\xC8\x15\xEF\x00\x03\x04\x01\x02\xF5", 10);
	NumLoops = 12; //Un valore maggiore di 9.
	isr_task(NULL);
}


void test_isr_task_ack(void **state){
    (void)state;
	for(uint8_t i=1; i<=0x1F; i++){
		char c[] = "\xC7\xFF\x00\x00";
		c[2] = i;
		c[3] = 0xFF ^ i;
		serial_push_full_string(c, 4);
		NumLoops = 12; //Un valore maggiore di 9.
		expect_function_call(__wrap_event_emit);
		isr_task(NULL);
	}
}

void test_isr_task_ack_wrong(void **state){
    (void)state;
	for(uint8_t i=1; i<=0x1F; i++){
		char c[] = "\xC7\xFF\x00\x00";
		c[2] = i;
		c[3] = 0xFF ^ (i+1);
		serial_push_full_string(c, 4);
		NumLoops = 12; //Un valore maggiore di 9.
		expect_function_call(__wrap_event_emit);
		isr_task(NULL);
	}
}

void test_isr_task_nack(void **state){
    (void)state;
	for(uint8_t i=1; i<=0x1F; i++){
		char c[] = "\xC7\xF0\x00\x00";
		c[2] = i;
		c[3] = 0xFF ^ i;
		serial_push_full_string(c, 4);
		NumLoops = 12; //Un valore maggiore di 9.
		expect_function_call(__wrap_event_emit);
		isr_task(NULL);
	}
}

void test_radio_init(void **state){
    (void)state;
	// expect_function_call(__wrap_xTaskCreate);
	// expect_function_call(__wrap_xTimerCreate);
	radio_lowlevel_init();
}


int setup(void **state){
	(void)state;
	NumLoops = 10;
	buffer_reception = NULL;
	buffer_len = 0;
	buffer_pos = 0;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
			cmocka_unit_test_setup(test_radio_init, setup),
			cmocka_unit_test_setup(test_isr_task_valid_90, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_90, setup),
			cmocka_unit_test_setup(test_isr_task_valid_91, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_91, setup),
			cmocka_unit_test_setup(test_isr_task_valid_B8, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_B8, setup),
			cmocka_unit_test_setup(test_isr_task_valid_C0, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_C0, setup),
			cmocka_unit_test_setup(test_isr_task_valid_C3, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_C3, setup),
			cmocka_unit_test_setup(test_isr_task_valid_C4, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_C4, setup),
			cmocka_unit_test_setup(test_isr_task_valid_C5, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_C5, setup),
			cmocka_unit_test_setup(test_isr_task_valid_D0, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_D0, setup),
			cmocka_unit_test_setup(test_isr_task_valid_D5, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_D5, setup),
			cmocka_unit_test_setup(test_isr_task_valid_E5, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_E5, setup),
			cmocka_unit_test_setup(test_isr_task_valid_not_implemented, setup),
			cmocka_unit_test_setup(test_isr_task_invalid_not_implemented, setup),
			cmocka_unit_test_setup(test_isr_task_ack, setup),
			cmocka_unit_test_setup(test_isr_task_ack_wrong, setup),
			cmocka_unit_test_setup(test_isr_task_nack, setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
