#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "radio_message_C5.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void event_out_service_emit(void) {
}

void software_reset(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_emit(union EVENT *event) {
    (void)event;
    function_called();
}

void test_radio_message_C3(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;
    message_raw_reset(&msg);
    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x11);
    message_raw_add_uint8_from_extern(&msg, 0xC5);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x0D);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 'h');
    message_raw_add_uint8_from_extern(&msg, 'e');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'o');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 'w');
    message_raw_add_uint8_from_extern(&msg, 'o');
    message_raw_add_uint8_from_extern(&msg, 'r');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'd');
    message_raw_add_uint8_from_extern(&msg, '\0');
    message_raw_add_uint8_from_extern(&msg, 0xC0);
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
    expect_function_call(__wrap_event_emit);
    message_C5_process((struct ALPINE_MESSAGE_C5 *)&msg);
}

void test_radio_message_C3_long_string(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;
    message_raw_reset(&msg);
    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x11);
    message_raw_add_uint8_from_extern(&msg, 0xC5);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x2F);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 'h');
    message_raw_add_uint8_from_extern(&msg, 'e');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'o');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 'w');
    message_raw_add_uint8_from_extern(&msg, 'o');
    message_raw_add_uint8_from_extern(&msg, 'r');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 't');
    message_raw_add_uint8_from_extern(&msg, 'h');
    message_raw_add_uint8_from_extern(&msg, 'i');
    message_raw_add_uint8_from_extern(&msg, 's');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 'i');
    message_raw_add_uint8_from_extern(&msg, 's');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 'a');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 'l');
    message_raw_add_uint8_from_extern(&msg, 'o');
    message_raw_add_uint8_from_extern(&msg, 'n');
    message_raw_add_uint8_from_extern(&msg, 'g');
    message_raw_add_uint8_from_extern(&msg, ' ');
    message_raw_add_uint8_from_extern(&msg, 's');
    message_raw_add_uint8_from_extern(&msg, 't');
    message_raw_add_uint8_from_extern(&msg, 'r');
    message_raw_add_uint8_from_extern(&msg, 'i');
    message_raw_add_uint8_from_extern(&msg, 'n');
    message_raw_add_uint8_from_extern(&msg, 'g');
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, 0x55);
    message_raw_add_uint8_from_extern(&msg, '\0');
    message_raw_add_uint8_from_extern(&msg, 0xD5);
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
    expect_function_call(__wrap_event_emit);
    message_C5_process((struct ALPINE_MESSAGE_C5 *)&msg);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_radio_message_C3, setup),
        cmocka_unit_test_setup(test_radio_message_C3_long_string, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
