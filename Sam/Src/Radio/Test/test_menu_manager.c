#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "menu_manager.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

//Funzione di trasmissione utilizzata per il test.
static bool row_tx_func(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)msg;
    (void)row_num;
    return false;
}

void test_manager_create(void **state) {
    (void)state;
    //Creo un menu
    menu_manager_t menu = menu_manager_create();
    assert_true(menu != NULL);

    //Il menu appena creato è inizializzato con dei dati di default.
    assert_true(menu->active_mask == 0x00);
    assert_true(menu->available_mask == 0x00);
    for (uint8_t i = 0; i < NUM_SLOTS; i++) {
        assert_true(menu->func_ptrs[i] == NULL);
    }
    assert_true(menu->pos == 0);

    //Il numero di righe attive è zero.
    assert_true(menu_manager_count_max(menu) == 0);

    //Aggiunto la riga del titolo, la riga 0.
    menu_manager_enable_bit_func(menu, 0, row_tx_func);
    //Il numero di righe deve comunque essere zero.
    assert_true(menu_manager_count_max(menu) == 0);

    //Aggiungo una riga in più...
    menu_manager_enable_bit_func(menu, 1, row_tx_func);
    assert_true(menu_manager_count_max(menu) == 1);

    //E ne aggiungo un altra saltandone una.
    menu_manager_enable_bit_func(menu, 3, row_tx_func);
    assert_true(menu_manager_count_max(menu) == 2);

    //Un altra ancora...
    menu_manager_enable_bit_func(menu, 31, row_tx_func);
    assert_true(menu_manager_count_max(menu) == 3);

    //Però adesso disattivo la prima...
    menu_manager_disable_bit(menu, 1);
    assert_true(menu_manager_count_max(menu) == 2);

    //La funzione iter next viene quindi chiamata per un numero di volte pari al numero delle righe attive. cioè...3, titolo + 2 righe attive
    uint8_t cnt = 0;
    while (menu_manager_iter_next(menu) != NULL) {
        cnt++;
    };
    assert_true(cnt == 3);

    //La posizione adesso è diversa da zero.
    assert_true(menu->pos != 0);
    //e se resetto la funzione torna a zero.
    menu_manager_reload(menu);
    assert_true(menu->pos == 0);

    //Se provo ad aggiungere un dato fuori dallo spazio riservato, invece non fa nulla.
    menu_manager_enable_bit_func(menu, NUM_SLOTS, row_tx_func);
    assert_true(menu_manager_count_max(menu) == 2);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_manager_create, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}