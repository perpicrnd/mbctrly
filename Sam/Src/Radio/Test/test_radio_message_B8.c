#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "radio_message_B8.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_emit(union EVENT *event) {
    (void)event;
    function_called();
}

void test_radio_message_B8(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;
    message_raw_reset(&msg);  //C7 C8 11 B8 00 01 04 31
    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x11);
    message_raw_add_uint8_from_extern(&msg, 0xB8);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x01);
    message_raw_add_uint8_from_extern(&msg, 0x04);
    message_raw_add_uint8_from_extern(&msg, 0x31);
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
    expect_function_call(__wrap_event_emit);
    message_B8_process((struct ALPINE_MESSAGE_B8 *)&msg);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_radio_message_B8, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
