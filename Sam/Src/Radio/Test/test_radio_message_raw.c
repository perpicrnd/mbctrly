#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "radio_message_raw.h"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void event_out_service_emit(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void test_acknack_ack(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;

    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xFF);
    message_raw_add_uint8_from_extern(&msg, 0x01);
    message_raw_add_uint8_from_extern(&msg, 0xFF ^ 0x01);

    assert_true(message_raw_is_acknack(&msg));
    assert_true(message_raw_acknack_is_ack(&msg));
    assert_false(message_raw_acknack_is_nack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
}

void test_acknack_ack_bad(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;

    message_raw_add_uint8_from_extern(&msg, 0xC7);
    assert_int_equal(message_raw_seq_number(&msg), 0xFF);
    message_raw_add_uint8_from_extern(&msg, 0xFF);
    message_raw_add_uint8_from_extern(&msg, 0x02);
    message_raw_add_uint8_from_extern(&msg, 0xFF ^ 0x01);

    assert_true(message_raw_is_acknack(&msg));
    assert_true(message_raw_acknack_is_ack(&msg));
    assert_false(message_raw_acknack_is_nack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_KO);
    assert_int_equal(message_raw_seq_number(&msg), 0x02);
}

void test_acknack_ack_wrong(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;

    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x02);
    message_raw_add_uint8_from_extern(&msg, 0xFF ^ 0x01);

    assert_false(message_raw_is_acknack(&msg));
    assert_false(message_raw_acknack_is_ack(&msg));
    assert_false(message_raw_acknack_is_nack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_UNKNOWN);
}

void test_message(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;

    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x1F);
    message_raw_add_uint8_from_extern(&msg, 0xD0);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x02);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x0E);

    assert_false(message_raw_is_acknack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
    assert_int_equal(message_raw_command(&msg), 0xD0);
}

void test_message_wrong(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;
    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x12);
    message_raw_add_uint8_from_extern(&msg, 0xD0);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x02);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x1C);

    assert_false(message_raw_is_acknack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_KO);
    assert_int_equal(message_raw_command(&msg), 0xD0);
}

void test_acknack_nack(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;

    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xF0);
    message_raw_add_uint8_from_extern(&msg, 0x01);
    message_raw_add_uint8_from_extern(&msg, 0xFF ^ 0x01);

    assert_true(message_raw_is_acknack(&msg));
    assert_true(message_raw_acknack_is_nack(&msg));
    assert_false(message_raw_acknack_is_ack(&msg));
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_acknack_ack, setup),
        cmocka_unit_test_setup(test_acknack_nack, setup),
        cmocka_unit_test_setup(test_acknack_ack_bad, setup),
        cmocka_unit_test_setup(test_acknack_ack_wrong, setup),
        cmocka_unit_test_setup(test_message, setup),
        cmocka_unit_test_setup(test_message_wrong, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
