#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "radio_message_E5.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void event_out_service_emit(void) {
}

void software_reset(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_emit(union EVENT *event) {
    (void)event;
    function_called();
}

void test_radio_message_E5(void **status) {
    (void)status;
    struct ALPINE_RAW_MESSAGE msg;
    message_raw_reset(&msg);
    message_raw_add_uint8_from_extern(&msg, 0xC7);
    message_raw_add_uint8_from_extern(&msg, 0xC8);
    message_raw_add_uint8_from_extern(&msg, 0x11);
    message_raw_add_uint8_from_extern(&msg, 0xE5);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x02);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x00);
    message_raw_add_uint8_from_extern(&msg, 0x07);
    assert_true(message_raw_crc_check(&msg) == CRC_OK);
    expect_function_call(__wrap_event_emit);
    message_E5_get_requested_mode((struct ALPINE_MESSAGE_E5 *)&msg);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_radio_message_E5, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
