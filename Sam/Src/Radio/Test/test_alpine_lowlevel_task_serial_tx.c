#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "alpine_lowlevel_task.c"

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void){
//Make the linker happy.
}

void software_reset(void){

}

char tx_buffer[255];
uint8_t tx_buffer_pos = 0;
int __wrap_serial_putstr(uint8_t *buff, int len){
	for(uint8_t i=0; i<len; i++){
		tx_buffer[i] = buff[i];
	}
	tx_buffer_pos = len;
	return 1;
}
size_t __wrap_xStreamBufferReceive( StreamBufferHandle_t xStreamBuffer, void *pvRxData, size_t xBufferLengthBytes, TickType_t xTicksToWait ){
	(void)xStreamBuffer;
	(void)pvRxData;
	(void)xBufferLengthBytes;
	(void)xTicksToWait;
		return 1;
}

size_t __wrap_xStreamBufferSend( StreamBufferHandle_t xStreamBuffer,
						  const void *pvTxData,
						  size_t xDataLengthBytes,
						  TickType_t xTicksToWait ){
    (void)xStreamBuffer;
    (void)pvTxData;
    (void)xDataLengthBytes;
    (void)xTicksToWait;
	return pdTRUE;
}

void test_radio_tx_message(void **status){
    (void)status;
	uint8_t *msg = (uint8_t *)"\xC7\xC8\x0B\x10\x00\x05\x61\x00\x00\x00\x20\x5E";
	radio_transmit_message(msg, 12);

	union ALPINE_MESSAGE message = {0};
	for(uint8_t i=0; i<tx_buffer_pos; i++){
		message_raw_add_uint8_from_extern(&message.as_raw, tx_buffer[i]);
	}
	assert_true(message_raw_completed(&message.as_raw));
	for(uint8_t i=0; i<message.as_raw.pos; i++){
		assert_true(msg[i] == message.as_raw.data[i]);
	}
}

void test_radio_tx_message_swap_D8(void **status){
    (void)status;
	uint8_t *msg = (uint8_t *)"\xC7\xC8\x0B\x10\x00\x05\xD8\x00\x00\x00\x20\x00";
	radio_transmit_message(msg, 12);

	union ALPINE_MESSAGE message = {0};
	for(uint8_t i=0; i<tx_buffer_pos; i++){
		message_raw_add_uint8_from_extern(&message.as_raw, tx_buffer[i]);
	}
	assert_true(message_raw_completed(&message.as_raw));
	for(uint8_t i=0; i<message.as_raw.pos; i++){
		assert_true(msg[i] == message.as_raw.data[i]);
	}
}

void test_radio_tx_message_swap_C7(void **status){
    (void)status;
	uint8_t *msg = (uint8_t *)"\xC7\xC8\x0B\x10\x00\x05\xC7\x00\x00\x00\x20\x00";
	radio_transmit_message(msg, 12);

	union ALPINE_MESSAGE message = {0};
	for(uint8_t i=0; i<tx_buffer_pos; i++){
		message_raw_add_uint8_from_extern(&message.as_raw, tx_buffer[i]);
	}
	assert_true(message_raw_completed(&message.as_raw));
	for(uint8_t i=0; i<message.as_raw.pos; i++){
		assert_true(msg[i] == message.as_raw.data[i]);
	}
}

int setup(void **state){
	(void)state;
	tx_buffer_pos = 0;
	memset(tx_buffer, '\x00', 255);
    alpine_serial = &uart;
    alpine_serial->init(alpine_serial, 115200, 'N');
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
			// cmocka_unit_test_setup(test_radio_tx_message, setup),
			// cmocka_unit_test_setup(test_radio_tx_message_swap_C7, setup),
			// cmocka_unit_test_setup(test_radio_tx_message_swap_D8, setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
