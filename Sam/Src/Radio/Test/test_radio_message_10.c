#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "radio_message_10.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

bool __wrap_radio_transmit_message(uint8_t *msg, uint8_t len) {
    (void)msg;
    (void)len;
    function_called();
    return true;
}

void __wrap_watchdog_reset(void) {
    function_called();
}

void __wrap_event_emit(union EVENT *event) {
    (void)event;
    function_called();
}

void test_radio_message_10_tx(void **status) {
    (void)status;
    union ALPINE_MESSAGE msg;
    expect_function_call(__wrap_radio_transmit_message);
    message_10_transmit(&msg.as_10, 0x01);

    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_CHANGED;
    event.as_ignition.ignition = IGNITION_ON;
    expect_function_call(__wrap_event_emit);
    event_task_loop(&event);

    union EVENT lights;
    lights.as_generic.event = eEVENT_LIGHTS_CHANGED;
    lights.as_lights.lights = LIGHTS_ON;
    expect_function_call(__wrap_event_emit);
    event_task_loop(&lights);

    union EVENT reverse;
    reverse.as_generic.event = eEVENT_REVERSE_CHANGED;
    reverse.as_reverse.reverse = REVERSE_ON;
    expect_function_call(__wrap_event_emit);
    event_task_loop(&reverse);

    union EVENT handbrake;
    handbrake.as_generic.event = eEVENT_HANDBRAKE_CHANGED;
    handbrake.as_handbrake.handbrake = HANDBRAKE_ON;
    expect_function_call(__wrap_event_emit);
    event_task_loop(&handbrake);

    union EVENT speed;
    speed.as_generic.event = eEVENT_SPEED_CHANGED;
    speed.as_speed.speed = 120;
    event_task_loop(&speed);

    expect_function_call(__wrap_radio_transmit_message);
    message_10_transmit(&msg.as_10, 0x02);

    union EVENT time;
    //Unload counter.
    for (uint8_t i = 0; i < 10; i++) {
        time.as_generic.event = eEVENT_SECONDS_CHANGED;
        time.as_timestamp.timestamp = i;
        event_task_loop(&time);
    }
    expect_function_call(__wrap_watchdog_reset);

    message_10_transmit(&msg.as_10, 0x01);
}

int setup(void **state) {
    (void)state;
    message_10_connect_callback();
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_radio_message_10_tx, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
