#include <string.h>
#include <stdbool.h>


#include "alpine_task.h"
#include "logger.h"
#include "event_list.h"

static uint8_t text_1[MEDIA_TEXT_STRING_LENGTH];
static uint8_t text_2[MEDIA_TEXT_STRING_LENGTH];
static uint8_t text_3[MEDIA_TEXT_STRING_LENGTH];


static enum RADIO_MEDIA_TYPE media_type;
static enum RADIO_SOURCE_TYPE source_type;
static uint8_t info1;
static uint8_t info2;
static uint8_t info3;
static uint8_t info4;
static uint8_t info5;
static uint8_t info6;

#define STD_TEXT "Alpine"

void __attribute__((weak)) write_text(uint8_t text[MEDIA_TEXT_STRING_LENGTH], bool changed){
    (void)text;
    (void)changed;
    LOG_DEF_NORMAL("no RDS INFO: weak %s\r\n", text);
}


static inline void reset_message(void){
    strcpy((char *)text_1, STD_TEXT);
    strcpy((char *)text_2, (char *)text_1);
    strcpy((char *)text_3, (char *)text_1);
}


static uint8_t last_text_sent[MEDIA_TEXT_STRING_LENGTH];

void  update_text(uint8_t text[MEDIA_TEXT_STRING_LENGTH]){
    strcpy((char *)last_text_sent, (char *)text);
    write_text(last_text_sent, true);
}

static inline void handle_second_changed(const union EVENT * const msg){
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
		if (strlen((const char *)last_text_sent) > 1){
        	write_text(last_text_sent, false);
		}else{
			reset_message();
			strcpy((char *)last_text_sent, (char *)text_1);
			write_text(text_1, true);
		}
    }
}

static void tx_send_frequency(void){
	uint16_t freq = info2 + (info3<<8);
	uint8_t to_send[20] = {0};

	uint8_t i = 0;
	to_send[i] = freq / 10000;
	if (to_send[i] > 0){
		freq -= to_send[i] * 10000;
		to_send[i] += '0'; //Lo faccio divendare un numero ascii
		i++;  //i++ solo se valore diverso da 0.
	}

	to_send[i] = (freq / 1000);
	if (to_send[i] > 0){
		freq -= to_send[i] * 1000;
	}
	to_send[i] += '0';
	i++;

	to_send[i] = (freq / 100);
	if (to_send[i] > 0){
		freq -= to_send[i] * 100;
	}
	to_send[i] += '0';
	i++;
	to_send[i] = '.';
	i++;

	to_send[i] = (freq / 10);
	if (to_send[i] > 0){
		freq -= to_send[i] * 10;
	}
	to_send[i] += '0';
	i++;
	to_send[i] = freq;
	to_send[i] += '0';

	to_send[++i] = ' ';
	to_send[++i] = 'M';
	to_send[++i] = 'H';
	to_send[++i] = 'z';
	strcpy((char *)text_1, (char *)to_send);
	update_text(text_1);
}

static inline void update_text_1_source_type(void){
    	switch(source_type){
		case RADIO_SOURCE_TUNER:
		LOG_DEF_NORMAL("Update frequency\r\n");
    		tx_send_frequency();
		break;
		case RADIO_SOURCE_DISK:
		{
			LOG_DEF_NORMAL("Disk\r\n");
			strcpy((char *)text_1, "DISK");
		}
		break;
		case RADIO_SOURCE_TV:
		{
			LOG_DEF_NORMAL("TV\r\n");
			strcpy((char *)text_1, "TV");
		}
		break;
		case RADIO_SOURCE_NAVI:
		{
			LOG_DEF_NORMAL("Navi\r\n");
            strcpy((char *)text_1, "NAVI");
		}
		break;
		case RADIO_SOURCE_PHONE:
		{
			LOG_DEF_NORMAL("Phone\r\n");
            strcpy((char *)text_1, "PHONE");
		}
		break;
		case RADIO_SOURCE_IPOD:
		{
			LOG_DEF_NORMAL("IPOD\r\n");
            strcpy((char *)text_1, "IPOD");
		}
		break;
		case RADIO_SOURCE_AUX:
		{
			LOG_DEF_NORMAL("AUX\r\n");
            strcpy((char *)text_1, "AUX");
		}
		break;
		case RADIO_SOURCE_USB:
		{
			LOG_DEF_NORMAL("USB\r\n");
            strcpy((char *)text_1, "USB");
		}
		break;
		case RADIO_SOURCE_DVB_T:
		{
			LOG_DEF_NORMAL("DVBT\r\n");
            strcpy((char *)text_1, "DVB");
		}
		break;
		case RADIO_SOURCE_BT_A2DP:
		{
			LOG_DEF_NORMAL("BT\r\n");
            strcpy((char *)text_1, "Bluetooth");
		}
		break;
		case RADIO_SOURCE_OFF:
		{
			LOG_DEF_NORMAL("OFF\r\n");
            strcpy((char *)text_1, "OFF");
		}
		break;
		default:
		{
			LOG_DEF_NORMAL("Default\r\n");
            strcpy((char *)text_1, STD_TEXT);
		}
		break;
	}
	update_text(text_1);

}


static void handle_media_changed(const union EVENT * const msg) {
	if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
		bool changed = false;
		if (media_type != msg->as_radio_source.media_type){
			media_type = msg->as_radio_source.media_type;
			changed = true;
		}

		if (source_type != msg->as_radio_source.source_type){
			source_type = msg->as_radio_source.source_type;
			changed = true;
		}

		if (info1 != msg->as_radio_source.info1){
			info1 =  msg->as_radio_source.info1;
			changed = true;
		}

		if (info2 != msg->as_radio_source.info2){
			info2 =  msg->as_radio_source.info2;
			changed = true;
		}

		if (info3 != msg->as_radio_source.info3){
			info3 =  msg->as_radio_source.info3;
			changed = true;
		}

		if (info4 != msg->as_radio_source.info4){
			info4 =  msg->as_radio_source.info4;
		}

		if (info5 != msg->as_radio_source.info5){
			info5 =  msg->as_radio_source.info5;
		}

		if (info6 != msg->as_radio_source.info6){
			info6 =  msg->as_radio_source.info6;
		}

		if (changed){
            reset_message();
			update_text_1_source_type();
			LOG_DEF_NORMAL("Radio media changed. Received: %02X %02X => %02X %02X %02X %02X %02X %02X\r\n", media_type, source_type, info1, info2, info3, info4, info5, info6);
		}
	}
}



static void handle_media_text(const union EVENT * const msg) {
	if (msg->as_generic.event == eEVENT_MEDIA_TEXT) {
		LOG_DEF_NORMAL("Radio media text changed. Received: %s %02X %02X\r\n", msg->as_media_text.string, msg->as_media_text.string_id, msg->as_media_text.string_type);
		switch(msg->as_media_text.string_id){
			case 1:
			if (msg->as_media_text.string_type == 0){
				strcpy((char *)text_1, (const char *)msg->as_media_text.string);
                LOG_DEF_NORMAL("Text1:-%s-%d\r\n", text_1, (uint8_t)strlen((const char *)text_1));
                LOG_DEF_NORMAL("Text2:-%s-%d\r\n", text_2, (uint8_t)strlen((const char *)text_2));
                LOG_DEF_NORMAL("Text3:-%s-%d\r\n", text_3, (uint8_t)strlen((const char *)text_3));
				if ((strlen((const char *)text_1 ) > 1) && strcmp((const char *)text_3, STD_TEXT) == 0){
					LOG_DEF_NORMAL("Send text1\r\n");
					update_text(text_1);
				}
			}else{
				LOG_DEF_NORMAL("Send source type\r\n");
				update_text_1_source_type();
			}
			break;
			case 2:
			strcpy((char *)text_2, (const char *)msg->as_media_text.string);
			break;
			case 3:
			if (msg->as_media_text.string_type == 0){
				LOG_DEF_NORMAL("Send text3\r\n");
				strcpy((char *)text_3, (const char *)msg->as_media_text.string);
				update_text(text_3);
			}else{
				LOG_DEF_NORMAL("Send source type\r\n");
				update_text_1_source_type();
			}
			break;
			default:
			break;
		}
		
	}
}



void radio_rds_info_init(void){
	event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_media_changed);
	event_connect_callback(eEVENT_MEDIA_TEXT, handle_media_text);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
    reset_message();
}
