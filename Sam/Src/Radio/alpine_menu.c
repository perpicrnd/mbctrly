#include "alpine_menu.h"
#include <stdbool.h>
#include "FreeRTOS.h"
#include "alpine_menu_priv.h"
#include "alpine_task.h"
#include "can_message.h"
#include "controller.h"
#include "event_list.h"
#include "logger.h"
#include "menu_manager.h"
#include "menu_refresher.h"
#include "menu_walking.h"
#include "radio_message.h"
#include "timers.h"
#include "vehicle.h"
#include "version.h"

menu_manager_t alpine_main_menu_manager;
menu_manager_t alpine_menu_manager;
menu_manager_t alpine_settings_menu_manager;
menu_manager_t alpine_menu_datetime_manager;
menu_manager_t alpine_preflight_menu_manager;

enum MAIN_MENU_ROWS {
    MAIN_MENU_ROWS_GENERAL = 1,
    MAIN_MENU_ROWS_INTERFACE,
    MAIN_MENU_ROWS_VEHICLE,
    MAIN_MENU_ROWS_PREFLIGHT,
};

enum VERSION_MENU_ROWS {
    VERSION_MENU_ROWS_VERSION = 1,
    VERSION_MENU_ROWS_IF_STATUS,
    VERSION_MENU_ROWS_CAN,
    VERSION_MENU_ROWS_LIN_STATUS,
    VERSION_MENU_ROWS_DATETIME,
};

enum DATETIME_MENU_ROWS {
    DATETIME_MENU_ROWS_ENABLE = 1,
    DATETIME_MENU_ROWS_YEARS,
    DATETIME_MENU_ROWS_MONTHS,
    DATETIME_MENU_ROWS_DAYS,
    DATETIME_MENU_ROWS_HOUR,
    DATETIME_MENU_ROWS_MINUTE,
    DATETIME_MENU_ROWS_HOUR_FORMAT,
};

enum PREFLIGHT_MENU_ROWS {
    PREFLIGHT_MENU_ROWS_TIME = 1,
    PREFLIGHT_MENU_ROWS_LENGTH_INT,
    PREFLIGHT_MENU_ROWS_LENGTH_DEC,
    PREFLIGHT_MENU_ROWS_WIDTH_INT,
    PREFLIGHT_MENU_ROWS_WIDTH_DEC,
    PREFLIGHT_MENU_ROWS_HEIGHT_INT,
    PREFLIGHT_MENU_ROWS_HEIGHT_DEC,
    PREFLIGHT_MENU_ROWS_WEIGHT,
    PREFLIGHT_MENU_ROWS_WEIGHT_DEC,
    PREFLIGHT_MENU_ROWS_FUEL_TYPE,
    PREFLIGHT_MENU_ROWS_RAMPS,
    PREFLIGHT_MENU_ROWS_SHOWER,
    PREFLIGHT_MENU_ROWS_WINDOWS,
    PREFLIGHT_MENU_ROWS_GAS,
    PREFLIGHT_MENU_ROWS_JACKS,
    PREFLIGHT_MENU_ROWS_STORAGE,
    PREFLIGHT_MENU_ROWS_MAINS,
    PREFLIGHT_MENU_ROWS_AWNING,
    PREFLIGHT_MENU_ROWS_ANTENNA,
    PREFLIGHT_MENU_ROWS_CABINETS,
    PREFLIGHT_MENU_ROWS_DRAWERS,
    PREFLIGHT_MENU_ROWS_OBJECTS,
    PREFLIGHT_MENU_ROWS_TABLE
};

enum PREFLIGHT_MENU_FUEL_TYPE {
    FUEL_TYPE_DIESEL,
    FUEL_TYPE_GASOLINE
};

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

typedef enum {
    INTERFACE_MENU_MAIN = 0x00,
    INTERFACE_MENU_MAIN2 = 0x01,
    GENERAL_MENU_VERS = 0x02,
    INTERFACE_PREFLIGHT_MENU = 0x03,
    INTERFACE_MENU_CLOCK = 0x0F,
    INTERFACE_MENU_VERS = 0x10,
    INTERFACE_VEHICLE_MENU = 0x80,
} INTERFACE_MENU;

/**
 * @brief Variabile contenente il nome del firmware che sta girando.
 */
extern const char interface_code[];

/**
 * @brief Traduzione della stringa Interface settings nelle varie lingue .
 */
char *interface_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Interface Einstellungen",    //!< LANGUAGE_DEFINITION_GERMAN
    "Interface Settings",         //!< LANGUAGE_DEFINITION_ENGLISH
    "Interface Settings",         //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Paramètres de l'Interface",  //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni Interfaccia",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Ajustes de interfaz",        //!< LANGUAGE_DEFINITION_SPANISH
    "Definições do interface",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Ustawienia Interfejsów",     //!< LANGUAGE_DEFINITION_POLISH
    "Interface Settings",         //!< LANGUAGE_DEFINITION_CZECH
    "Interface Settings",         //!< LANGUAGE_DEFINITION_SWEDISH
    "Interface-instellingen",     //!< LANGUAGE_DEFINITION_DUTCH
    "Interface Settings",         //!< LANGUAGE_DEFINITION_JAPANESE
    "Interface Settings",         //!< LANGUAGE_DEFINITION_RUSSIAN
    "Interface Settings",         //!< LANGUAGE_DEFINITION_KOREAN
    "Interface Settings",         //!< LANGUAGE_DEFINITION_TURKISH
    "Interface Settings",         //!< LANGUAGE_DEFINITION_CHINESE
    "Interface Settings",         //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Interface Settings",         //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Interface Settings",         //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *general_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Information",   //!< LANGUAGE_DEFINITION_GERMAN
    "Information",   //!< LANGUAGE_DEFINITION_ENGLISH
    "Information",   //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Information",   //!< LANGUAGE_DEFINITION_FRENCH
    "Informazioni",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Información",   //!< LANGUAGE_DEFINITION_SPANISH
    "Informação",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Information",   //!< LANGUAGE_DEFINITION_POLISH
    "Information",   //!< LANGUAGE_DEFINITION_CZECH
    "Information",   //!< LANGUAGE_DEFINITION_SWEDISH
    "Information",   //!< LANGUAGE_DEFINITION_DUTCH
    "Information",   //!< LANGUAGE_DEFINITION_JAPANESE
    "Information",   //!< LANGUAGE_DEFINITION_RUSSIAN
    "Information",   //!< LANGUAGE_DEFINITION_KOREAN
    "Information",   //!< LANGUAGE_DEFINITION_TURKISH
    "Information",   //!< LANGUAGE_DEFINITION_CHINESE
    "Information",   //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Information",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Information",   //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *vehicle_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Fahrzeugeinstellungen",       //!< LANGUAGE_DEFINITION_GERMAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Réglages du véhicule",        //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni veicolo",        //!< LANGUAGE_DEFINITION_ITALIAN
    "Configuración del vehículo",  //!< LANGUAGE_DEFINITION_SPANISH
    "Configuração do veiculo",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Ustawienia pojazdu",          //!< LANGUAGE_DEFINITION_POLISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_CZECH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_SWEDISH
    "Voertuig instellingen",       //!< LANGUAGE_DEFINITION_DUTCH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_JAPANESE
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_RUSSIAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_KOREAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_TURKISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_CHINESE
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *preflight_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Einstellungen Abfahrtskontrolle",  //!< LANGUAGE_DEFINITION_GERMAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_ENGLISH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni preflight check",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_SPANISH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_POLISH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_CZECH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_SWEDISH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_DUTCH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_JAPANESE
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_RUSSIAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_KOREAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_TURKISH
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_CHINESE
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Preflight check settings",         //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *swc_interface_str[LANGUAGE_DEFINITION_COUNT] = {
    "Software Version",  //!< LANGUAGE_DEFINITION_GERMAN
    "SW Version",        //!< LANGUAGE_DEFINITION_ENGLISH
    "SW Version",        //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Version SW",        //!< LANGUAGE_DEFINITION_FRENCH
    "Versione SW ",      //!< LANGUAGE_DEFINITION_ITALIAN
    "Versión SW",        //!< LANGUAGE_DEFINITION_SPANISH
    "Versão SW",         //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Wersja SW",         //!< LANGUAGE_DEFINITION_POLISH
    "SW Version",        //!< LANGUAGE_DEFINITION_CZECH
    "SW Version",        //!< LANGUAGE_DEFINITION_SWEDISH
    "SW-Versie",         //!< LANGUAGE_DEFINITION_DUTCH
    "SW Version",        //!< LANGUAGE_DEFINITION_JAPANESE
    "SW Version",        //!< LANGUAGE_DEFINITION_RUSSIAN
    "SW Version",        //!< LANGUAGE_DEFINITION_KOREAN
    "SW Version",        //!< LANGUAGE_DEFINITION_TURKISH
    "SW Version",        //!< LANGUAGE_DEFINITION_CHINESE
    "SW Version",        //!< LANGUAGE_DEFINITION_NORWEGIAN
    "SW Version",        //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "SW Version",        //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *software_version_str[LANGUAGE_DEFINITION_COUNT] = {
    "Information",   //!< LANGUAGE_DEFINITION_GERMAN
    "Information",   //!< LANGUAGE_DEFINITION_ENGLISH
    "Information",   //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Information",   //!< LANGUAGE_DEFINITION_FRENCH
    "Informazioni",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Información",   //!< LANGUAGE_DEFINITION_SPANISH
    "Informação",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Information",   //!< LANGUAGE_DEFINITION_POLISH
    "Information",   //!< LANGUAGE_DEFINITION_CZECH
    "Information",   //!< LANGUAGE_DEFINITION_SWEDISH
    "Information",   //!< LANGUAGE_DEFINITION_DUTCH
    "Information",   //!< LANGUAGE_DEFINITION_JAPANESE
    "Information",   //!< LANGUAGE_DEFINITION_RUSSIAN
    "Information",   //!< LANGUAGE_DEFINITION_KOREAN
    "Information",   //!< LANGUAGE_DEFINITION_TURKISH
    "Information",   //!< LANGUAGE_DEFINITION_CHINESE
    "Information",   //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Information",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Information",   //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char empty[] = "\0";

/**
 * Traduzione della stringa Data e Ora nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *clock_date_str[] =
    {
        "Uhrzeit und Datum",  //!< LANGUAGE_DEFINITION_GERMAN
        "Time & Date",        //!< LANGUAGE_DEFINITION_ENGLISH
        "Time & Date",        //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Horloge et date",    //!< LANGUAGE_DEFINITION_FRENCH
        "Data e Ora",         //!< LANGUAGE_DEFINITION_ITALIAN
        "Reloj y fecha",      //!< LANGUAGE_DEFINITION_SPANISH
        "Relógio e Data",     //!< LANGUAGE_DEFINITION_PORTUGUESE
        "Zegar i data",       //!< LANGUAGE_DEFINITION_POLISH
        "Clock & Date",       //!< LANGUAGE_DEFINITION_CZECH
        "Clock & Date",       //!< LANGUAGE_DEFINITION_SWEDISH
        "Klok en datum",      //!< LANGUAGE_DEFINITION_DUTCH
        "Clock & Date",       //!< LANGUAGE_DEFINITION_JAPANESE
        "Clock & Date",       //!< LANGUAGE_DEFINITION_RUSSIAN
        "Clock & Date",       //!< LANGUAGE_DEFINITION_KOREAN
        "Saat ve tarih",      //!< LANGUAGE_DEFINITION_TURKISH
        "Clock & Date",       //!< LANGUAGE_DEFINITION_CHINESE
        "Clock & Date",       //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Clock & Date",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Clock & Date",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa anno nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *year_str[] =
    {
        "Jahr",   //!< LANGUAGE_DEFINITION_GERMAN
        "Year",   //!< LANGUAGE_DEFINITION_ENGLISH
        "Year",   //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Année",  //!< LANGUAGE_DEFINITION_FRENCH
        "Anno",   //!< LANGUAGE_DEFINITION_ITALIAN
        "Año",    //!< LANGUAGE_DEFINITION_SPANISH
        "Ano",    //!< LANGUAGE_DEFINITION_PORTUGUESE
        "rok",    //!< LANGUAGE_DEFINITION_POLISH
        "Year",   //!< LANGUAGE_DEFINITION_CZECH
        "Year",   //!< LANGUAGE_DEFINITION_SWEDISH
        "jaar",   //!< LANGUAGE_DEFINITION_DUTCH
        "Year",   //!< LANGUAGE_DEFINITION_JAPANESE
        "Year",   //!< LANGUAGE_DEFINITION_RUSSIAN
        "Year",   //!< LANGUAGE_DEFINITION_KOREAN
        "yıl",    //!< LANGUAGE_DEFINITION_TURKISH
        "Year",   //!< LANGUAGE_DEFINITION_CHINESE
        "Year",   //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Year",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Year",   //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa mese nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *month_str[] =
    {
        "Monat",    //!< LANGUAGE_DEFINITION_GERMAN
        "Month",    //!< LANGUAGE_DEFINITION_ENGLISH
        "Month",    //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Mois",     //!< LANGUAGE_DEFINITION_FRENCH
        "Mese",     //!< LANGUAGE_DEFINITION_ITALIAN
        "Mes",      //!< LANGUAGE_DEFINITION_SPANISH
        "Mês",      //!< LANGUAGE_DEFINITION_PORTUGUESE
        "miesiąc",  //!< LANGUAGE_DEFINITION_POLISH
        "Month",    //!< LANGUAGE_DEFINITION_CZECH
        "Month",    //!< LANGUAGE_DEFINITION_SWEDISH
        "maand",    //!< LANGUAGE_DEFINITION_DUTCH
        "Month",    //!< LANGUAGE_DEFINITION_JAPANESE
        "Month",    //!< LANGUAGE_DEFINITION_RUSSIAN
        "Month",    //!< LANGUAGE_DEFINITION_KOREAN
        "ay",       //!< LANGUAGE_DEFINITION_TURKISH
        "Month",    //!< LANGUAGE_DEFINITION_CHINESE
        "Month",    //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Month",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Month",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa giorno nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *day_str[] =
    {
        "Tag",     //!< LANGUAGE_DEFINITION_GERMAN
        "Day",     //!< LANGUAGE_DEFINITION_ENGLISH
        "Day",     //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Jour",    //!< LANGUAGE_DEFINITION_FRENCH
        "Giorno",  //!< LANGUAGE_DEFINITION_ITALIAN
        "Día",     //!< LANGUAGE_DEFINITION_SPANISH
        "Día",     //!< LANGUAGE_DEFINITION_PORTUGUESE
        "dzień",   //!< LANGUAGE_DEFINITION_POLISH
        "Day",     //!< LANGUAGE_DEFINITION_CZECH
        "Day",     //!< LANGUAGE_DEFINITION_SWEDISH
        "dag",     //!< LANGUAGE_DEFINITION_DUTCH
        "Day",     //!< LANGUAGE_DEFINITION_JAPANESE
        "Day",     //!< LANGUAGE_DEFINITION_RUSSIAN
        "Day",     //!< LANGUAGE_DEFINITION_KOREAN
        "gün",     //!< LANGUAGE_DEFINITION_TURKISH
        "Day",     //!< LANGUAGE_DEFINITION_CHINESE
        "Day",     //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Day",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Day",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa Minuto nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *minute_str[] =
    {
        "Minute",  //!< LANGUAGE_DEFINITION_GERMAN
        "Minute",  //!< LANGUAGE_DEFINITION_ENGLISH
        "Minute",  //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Minute",  //!< LANGUAGE_DEFINITION_FRENCH
        "Minuto",  //!< LANGUAGE_DEFINITION_ITALIAN
        "Minuto",  //!< LANGUAGE_DEFINITION_SPANISH
        "Minuto",  //!< LANGUAGE_DEFINITION_PORTUGUESE
        "minuta",  //!< LANGUAGE_DEFINITION_POLISH
        "Minute",  //!< LANGUAGE_DEFINITION_CZECH
        "Minute",  //!< LANGUAGE_DEFINITION_SWEDISH
        "minuut",  //!< LANGUAGE_DEFINITION_DUTCH
        "Minute",  //!< LANGUAGE_DEFINITION_JAPANESE
        "Minute",  //!< LANGUAGE_DEFINITION_RUSSIAN
        "Minute",  //!< LANGUAGE_DEFINITION_KOREAN
        "dakika",  //!< LANGUAGE_DEFINITION_TURKISH
        "Minute",  //!< LANGUAGE_DEFINITION_CHINESE
        "Minute",  //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Minute",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Minute",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa Ora nelle varie lingue.
 */
//PIETRO GIA VISTO
static const char *hour_str[] =
    {
        "Stunde",   //!< LANGUAGE_DEFINITION_GERMAN
        "Hour",     //!< LANGUAGE_DEFINITION_ENGLISH
        "Hour",     //!< LANGUAGE_DEFINITION_ENGLISH_US
        "Heure",    //!< LANGUAGE_DEFINITION_FRENCH
        "Ora",      //!< LANGUAGE_DEFINITION_ITALIAN
        "Hora",     //!< LANGUAGE_DEFINITION_SPANISH
        "Hora",     //!< LANGUAGE_DEFINITION_PORTUGUESE
        "godzina",  //!< LANGUAGE_DEFINITION_POLISH
        "Hour",     //!< LANGUAGE_DEFINITION_CZECH
        "Hour",     //!< LANGUAGE_DEFINITION_SWEDISH
        "nu",       //!< LANGUAGE_DEFINITION_DUTCH
        "Hour",     //!< LANGUAGE_DEFINITION_JAPANESE
        "Hour",     //!< LANGUAGE_DEFINITION_RUSSIAN
        "Hour",     //!< LANGUAGE_DEFINITION_KOREAN
        "saat",     //!< LANGUAGE_DEFINITION_TURKISH
        "Hour",     //!< LANGUAGE_DEFINITION_CHINESE
        "Hour",     //!< LANGUAGE_DEFINITION_NORWEGIAN
        "Hour",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
        "Hour",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa sincronizza orologio nelle varie lingue
 */
static const char *sync_clock_str[] = {
    "Uhrzeit synchronisieren",       //!< LANGUAGE_DEFINITION_GERMAN
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_ENGLISH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Synchronisation de l'horloge",  //!< LANGUAGE_DEFINITION_FRENCH
    "Sinc. orologio radio",          //!< LANGUAGE_DEFINITION_ITALIAN
    "Sincronizar reloj unidad",      //!< LANGUAGE_DEFINITION_SPANISH
    "Sincronizar relógio radio",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_POLISH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_CZECH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_SWEDISH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_DUTCH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_JAPANESE
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_RUSSIAN
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_KOREAN
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_TURKISH
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_CHINESE
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Sync radio clock",              //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *on_str[] = {
    "On",    //!< LANGUAGE_DEFINITION_GERMAN
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "On",    //!< LANGUAGE_DEFINITION_FRENCH
    "On",    //!< LANGUAGE_DEFINITION_ITALIAN
    "On",    //!< LANGUAGE_DEFINITION_SPANISH
    "On",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "On",    //!< LANGUAGE_DEFINITION_POLISH
    "On",    //!< LANGUAGE_DEFINITION_CZECH
    "On",    //!< LANGUAGE_DEFINITION_SWEDISH
    "On",    //!< LANGUAGE_DEFINITION_DUTCH
    "On",    //!< LANGUAGE_DEFINITION_JAPANESE
    "On",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "On",    //!< LANGUAGE_DEFINITION_KOREAN
    "Açık",  //!< LANGUAGE_DEFINITION_TURKISH
    "On",    //!< LANGUAGE_DEFINITION_CHINESE
    "On",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "On",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "On",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *off_str[] = {
    "Aus",     //!< LANGUAGE_DEFINITION_GERMAN
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Off",     //!< LANGUAGE_DEFINITION_FRENCH
    "Off",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Off",     //!< LANGUAGE_DEFINITION_SPANISH
    "Off",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Off",     //!< LANGUAGE_DEFINITION_POLISH
    "Off",     //!< LANGUAGE_DEFINITION_CZECH
    "Off",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Off",     //!< LANGUAGE_DEFINITION_DUTCH
    "Off",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Off",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Off",     //!< LANGUAGE_DEFINITION_KOREAN
    "Kapalı",  //!< LANGUAGE_DEFINITION_TURKISH
    "Off",     //!< LANGUAGE_DEFINITION_CHINESE
    "Off",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Off",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Off",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *yes_str[] = {
    "Ja",   //!< LANGUAGE_DEFINITION_GERMAN
    "Yes",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Yes",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Oui",  //!< LANGUAGE_DEFINITION_FRENCH
    "Si",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Si",   //!< LANGUAGE_DEFINITION_SPANISH
    "Sim",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Yes",  //!< LANGUAGE_DEFINITION_POLISH
    "Yes",  //!< LANGUAGE_DEFINITION_CZECH
    "Yes",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Yes",  //!< LANGUAGE_DEFINITION_DUTCH
    "Yes",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Yes",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Yes",  //!< LANGUAGE_DEFINITION_KOREAN
    "Yes",  //!< LANGUAGE_DEFINITION_TURKISH
    "Yes",  //!< LANGUAGE_DEFINITION_CHINESE
    "Yes",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Yes",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Yes",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *no_str[] = {
    "Nein",  //!< LANGUAGE_DEFINITION_GERMAN
    "No",    //!< LANGUAGE_DEFINITION_ENGLISH
    "No",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Non",   //!< LANGUAGE_DEFINITION_FRENCH
    "No",    //!< LANGUAGE_DEFINITION_ITALIAN
    "No",    //!< LANGUAGE_DEFINITION_SPANISH
    "Não",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "No",    //!< LANGUAGE_DEFINITION_POLISH
    "No",    //!< LANGUAGE_DEFINITION_CZECH
    "No",    //!< LANGUAGE_DEFINITION_SWEDISH
    "No",    //!< LANGUAGE_DEFINITION_DUTCH
    "No",    //!< LANGUAGE_DEFINITION_JAPANESE
    "No",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "No",    //!< LANGUAGE_DEFINITION_KOREAN
    "No",    //!< LANGUAGE_DEFINITION_TURKISH
    "No",    //!< LANGUAGE_DEFINITION_CHINESE
    "No",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "No",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "No",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *ok_str[] = {
    "OK",  //!< LANGUAGE_DEFINITION_GERMAN
    "OK",  //!< LANGUAGE_DEFINITION_ENGLISH
    "OK",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "OK",  //!< LANGUAGE_DEFINITION_FRENCH
    "OK",  //!< LANGUAGE_DEFINITION_ITALIAN
    "OK",  //!< LANGUAGE_DEFINITION_SPANISH
    "OK",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "OK",  //!< LANGUAGE_DEFINITION_POLISH
    "OK",  //!< LANGUAGE_DEFINITION_CZECH
    "OK",  //!< LANGUAGE_DEFINITION_SWEDISH
    "OK",  //!< LANGUAGE_DEFINITION_DUTCH
    "OK",  //!< LANGUAGE_DEFINITION_JAPANESE
    "OK",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "OK",  //!< LANGUAGE_DEFINITION_KOREAN
    "OK",  //!< LANGUAGE_DEFINITION_TURKISH
    "OK",  //!< LANGUAGE_DEFINITION_CHINESE
    "OK",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "OK",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "OK",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *error_str[] = {
    "Fehler",  //!< LANGUAGE_DEFINITION_GERMAN
    "Error",   //!< LANGUAGE_DEFINITION_ENGLISH
    "Error",   //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Error",   //!< LANGUAGE_DEFINITION_FRENCH
    "Errore",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Error",   //!< LANGUAGE_DEFINITION_SPANISH
    "Error",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Error",   //!< LANGUAGE_DEFINITION_POLISH
    "Error",   //!< LANGUAGE_DEFINITION_CZECH
    "Error",   //!< LANGUAGE_DEFINITION_SWEDISH
    "Error",   //!< LANGUAGE_DEFINITION_DUTCH
    "Error",   //!< LANGUAGE_DEFINITION_JAPANESE
    "Error",   //!< LANGUAGE_DEFINITION_RUSSIAN
    "Error",   //!< LANGUAGE_DEFINITION_KOREAN
    "Error",   //!< LANGUAGE_DEFINITION_TURKISH
    "Error",   //!< LANGUAGE_DEFINITION_CHINESE
    "Error",   //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Error",   //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Error",   //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa Ora nelle varie lingue.
 */
static const char *time_format_str[] = {
    "Format",   //LANGUAGE_DEFINITION_GERMAN,
    "Format",   //LANGUAGE_DEFINITION_ENGLISH,
    "Format",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Format",   //LANGUAGE_DEFINITION_FRENCH,
    "Formato",  //LANGUAGE_DEFINITION_ITALIAN,
    "Formato",  //LANGUAGE_DEFINITION_SPANISH,
    "Formato",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Formatu",  //LANGUAGE_DEFINITION_POLISH,
    "Format",   //LANGUAGE_DEFINITION_CZEC,
    "Format",   //LANGUAGE_DEFINITION_SWEDISH,
    "Formaat",  //LANGUAGE_DEFINITION_DUTCH,
    "Format",   //LANGUAGE_DEFINITION_JAPANESE,
    "Format",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Format",   //LANGUAGE_DEFINITION_KOREAN,
    "Formatı",  //LANGUAGE_DEFINITION_TURKISH,
    "Format",   //LANGUAGE_DEFINITION_CHINESE
    "Format",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Format",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Format",   //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *can_activity_str[] = {
    "CAN Aktivität",   //LANGUAGE_DEFINITION_GERMAN,
    "CAN Activity",    //LANGUAGE_DEFINITION_ENGLISH,
    "CAN Activity",    //LANGUAGE_DEFINITION_ENGLISH US,
    "CAN statut",      //LANGUAGE_DEFINITION_FRENCH,
    "Attività CAN",    //LANGUAGE_DEFINITION_ITALIAN,
    "Actividad CAN ",  //LANGUAGE_DEFINITION_SPANISH,
    "Actividade CAN",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "CAN Activity",    //LANGUAGE_DEFINITION_POLISH,
    "CAN Activity",    //LANGUAGE_DEFINITION_CZEC,
    "CAN Activity",    //LANGUAGE_DEFINITION_SWEDISH,
    "CAN Activity",    //LANGUAGE_DEFINITION_DUTCH,
    "CAN Activity",    //LANGUAGE_DEFINITION_JAPANESE,
    "CAN Activity",    //LANGUAGE_DEFINITION_RUSSIAN,
    "CAN Activity",    //LANGUAGE_DEFINITION_KOREAN,
    "CAN Activity",    //LANGUAGE_DEFINITION_TURKISH,
    "CAN Activity",    //LANGUAGE_DEFINITION_CHINESE
    "CAN Activity",    //LANGUAGE_DEFINITION_NORWEGIAN
    "CAN Activity",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "CAN Activity",    //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *lin_activity_str[] = {
    "LIN Aktivität",   //LANGUAGE_DEFINITION_GERMAN,
    "LIN Activity",    //LANGUAGE_DEFINITION_ENGLISH,
    "LIN Activity",    //LANGUAGE_DEFINITION_ENGLISH US,
    "LIN Activity",    //LANGUAGE_DEFINITION_FRENCH,
    "Attività LIN",    //LANGUAGE_DEFINITION_ITALIAN,
    "Actividad LIN",   //LANGUAGE_DEFINITION_SPANISH,
    "Actividade LIN",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "LIN Activity",    //LANGUAGE_DEFINITION_POLISH,
    "LIN Activity",    //LANGUAGE_DEFINITION_CZEC,
    "LIN Activity",    //LANGUAGE_DEFINITION_SWEDISH,
    "LIN Activity",    //LANGUAGE_DEFINITION_DUTCH,
    "LIN Activity",    //LANGUAGE_DEFINITION_JAPANESE,
    "LIN Activity",    //LANGUAGE_DEFINITION_RUSSIAN,
    "LIN Activity",    //LANGUAGE_DEFINITION_KOREAN,
    "LIN Activity",    //LANGUAGE_DEFINITION_TURKISH,
    "LIN Activity",    //LANGUAGE_DEFINITION_CHINESE
    "LIN Activity",    //LANGUAGE_DEFINITION_NORWEGIAN
    "LIN Activity",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "LIN Activity",    //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *interface_status_str[] = {
    "Interface status",     //LANGUAGE_DEFINITION_GERMAN,
    "Interface status",     //LANGUAGE_DEFINITION_ENGLISH,
    "Interface status",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Interface statut",     //LANGUAGE_DEFINITION_FRENCH,
    "Stato interfaccia",    //LANGUAGE_DEFINITION_ITALIAN,
    "Estado del interfaz",  //LANGUAGE_DEFINITION_SPANISH,
    "Estado Interface",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Interface status",     //LANGUAGE_DEFINITION_POLISH,
    "Interface status",     //LANGUAGE_DEFINITION_CZEC,
    "Interface status",     //LANGUAGE_DEFINITION_SWEDISH,
    "Interface status",     //LANGUAGE_DEFINITION_DUTCH,
    "Interface status",     //LANGUAGE_DEFINITION_JAPANESE,
    "Interface status",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Interface status",     //LANGUAGE_DEFINITION_KOREAN,
    "Interface status",     //LANGUAGE_DEFINITION_TURKISH,
    "Interface status",     //LANGUAGE_DEFINITION_CHINESE
    "Interface status",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Interface status",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Interface status",     //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *h12[] = {
    "12h",    //LANGUAGE_DEFINITION_GERMAN,
    "AM/PM",  //LANGUAGE_DEFINITION_ENGLISH,
    "AM/PM",  //LANGUAGE_DEFINITION_ENGLISH US,
    "AM/PM",  //LANGUAGE_DEFINITION_FRENCH,
    "AM/PM",  //LANGUAGE_DEFINITION_ITALIAN,
    "AM/PM",  //LANGUAGE_DEFINITION_SPANISH,
    "AM/PM",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "AM/PM",  //LANGUAGE_DEFINITION_POLISH,
    "AM/PM",  //LANGUAGE_DEFINITION_CZEC,
    "AM/PM",  //LANGUAGE_DEFINITION_SWEDISH,
    "AM/PM",  //LANGUAGE_DEFINITION_DUTCH,
    "AM/PM",  //LANGUAGE_DEFINITION_JAPANESE,
    "AM/PM",  //LANGUAGE_DEFINITION_RUSSIAN,
    "AM/PM",  //LANGUAGE_DEFINITION_KOREAN,
    "AM/PM",  //LANGUAGE_DEFINITION_TURKISH,
    "AM/PM",  //LANGUAGE_DEFINITION_CHINESE
    "AM/PM",  //LANGUAGE_DEFINITION_NORWEGIAN
    "AM/PM",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "AM/PM",  //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *h24[] = {
    "24h",  //LANGUAGE_DEFINITION_GERMAN,
    "24h",  //LANGUAGE_DEFINITION_ENGLISH,
    "24h",  //LANGUAGE_DEFINITION_ENGLISH US,
    "24h",  //LANGUAGE_DEFINITION_FRENCH,
    "24h",  //LANGUAGE_DEFINITION_ITALIAN,
    "24h",  //LANGUAGE_DEFINITION_SPANISH,
    "24h",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "24h",  //LANGUAGE_DEFINITION_POLISH,
    "24h",  //LANGUAGE_DEFINITION_CZEC,
    "24h",  //LANGUAGE_DEFINITION_SWEDISH,
    "24h",  //LANGUAGE_DEFINITION_DUTCH,
    "24h",  //LANGUAGE_DEFINITION_JAPANESE,
    "24h",  //LANGUAGE_DEFINITION_RUSSIAN,
    "24h",  //LANGUAGE_DEFINITION_KOREAN,
    "24h",  //LANGUAGE_DEFINITION_TURKISH,
    "24h",  //LANGUAGE_DEFINITION_CHINESE
    "24h",  //LANGUAGE_DEFINITION_NORWEGIAN
    "24h",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "24h",  //LANGUAGE_DEFINITION_HUNGARIAN
};

/* PREFLIGHT_MENU_ROWS_TIME */
static const char *pfMenuHeader[] = {
    "Einstellungen Abfahrtskontrolle",  //LANGUAGE_DEFINITION_GERMAN,
    "Preflight check settings",         //LANGUAGE_DEFINITION_ENGLISH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_ENGLISH US,
    "Preflight check settings",         //LANGUAGE_DEFINITION_FRENCH,
    "Impostazioni Preflight check",     //LANGUAGE_DEFINITION_ITALIAN,
    "Preflight check settings",         //LANGUAGE_DEFINITION_SPANISH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_PORTUGUESE,
    "Preflight check settings",         //LANGUAGE_DEFINITION_POLISH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_CZEC,
    "Preflight check settings",         //LANGUAGE_DEFINITION_SWEDISH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_DUTCH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_JAPANESE,
    "Preflight check settings",         //LANGUAGE_DEFINITION_RUSSIAN,
    "Preflight check settings",         //LANGUAGE_DEFINITION_KOREAN,
    "Preflight check settings",         //LANGUAGE_DEFINITION_TURKISH,
    "Preflight check settings",         //LANGUAGE_DEFINITION_CHINESE
    "Preflight check settings",         //LANGUAGE_DEFINITION_NORWEGIAN
    "Preflight check settings",         //LANGUAGE_DEFINITION_SLOVAKIAN
    "Preflight check settings",         //LANGUAGE_DEFINITION_HUNGARIAN
};

/* PREFLIGHT_MENU_ROWS_TIME */
static const char *pfStrTime[] = {
    "Anzeigedauer",      //LANGUAGE_DEFINITION_GERMAN,
    "Message time",      //LANGUAGE_DEFINITION_ENGLISH,
    "Message time",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Message time",      //LANGUAGE_DEFINITION_FRENCH,
    "Durata messaggio",  //LANGUAGE_DEFINITION_ITALIAN,
    "Message time",      //LANGUAGE_DEFINITION_SPANISH,
    "Message time",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Message time",      //LANGUAGE_DEFINITION_POLISH,
    "Message time",      //LANGUAGE_DEFINITION_CZEC,
    "Message time",      //LANGUAGE_DEFINITION_SWEDISH,
    "Message time",      //LANGUAGE_DEFINITION_DUTCH,
    "Message time",      //LANGUAGE_DEFINITION_JAPANESE,
    "Message time",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Message time",      //LANGUAGE_DEFINITION_KOREAN,
    "Message time",      //LANGUAGE_DEFINITION_TURKISH,
    "Message time",      //LANGUAGE_DEFINITION_CHINESE
    "Message time",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Message time",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Message time",      //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_LENGTH_INT */
static const char *pfStrLengthInt[] = {
    "Länge (m)",      //LANGUAGE_DEFINITION_GERMAN,
    "Length (m)",     //LANGUAGE_DEFINITION_ENGLISH,
    "Length (m)",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Length (m)",     //LANGUAGE_DEFINITION_FRENCH,
    "Lunghezza (m)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Length (m)",     //LANGUAGE_DEFINITION_SPANISH,
    "Length (m)",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Length (m)",     //LANGUAGE_DEFINITION_POLISH,
    "Length (m)",     //LANGUAGE_DEFINITION_CZEC,
    "Length (m)",     //LANGUAGE_DEFINITION_SWEDISH,
    "Length (m)",     //LANGUAGE_DEFINITION_DUTCH,
    "Length (m)",     //LANGUAGE_DEFINITION_JAPANESE,
    "Length (m)",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Length (m)",     //LANGUAGE_DEFINITION_KOREAN,
    "Length (m)",     //LANGUAGE_DEFINITION_TURKISH,
    "Length (m)",     //LANGUAGE_DEFINITION_CHINESE
    "Length (m)",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Length (m)",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Length (m)",     //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_LENGTH_DEC */
static const char *pfStrLengthDec[] = {
    "Länge (cm)",      //LANGUAGE_DEFINITION_GERMAN,
    "Length (cm)",     //LANGUAGE_DEFINITION_ENGLISH,
    "Length (cm)",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Length (cm)",     //LANGUAGE_DEFINITION_FRENCH,
    "Lunghezza (cm)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Length (cm)",     //LANGUAGE_DEFINITION_SPANISH,
    "Length (cm)",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Length (cm)",     //LANGUAGE_DEFINITION_POLISH,
    "Length (cm)",     //LANGUAGE_DEFINITION_CZEC,
    "Length (cm)",     //LANGUAGE_DEFINITION_SWEDISH,
    "Length (cm)",     //LANGUAGE_DEFINITION_DUTCH,
    "Length (cm)",     //LANGUAGE_DEFINITION_JAPANESE,
    "Length (cm)",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Length (cm)",     //LANGUAGE_DEFINITION_KOREAN,
    "Length (cm)",     //LANGUAGE_DEFINITION_TURKISH,
    "Length (cm)",     //LANGUAGE_DEFINITION_CHINESE
    "Length (cm)",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Length (cm)",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Length (cm)",     //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_WIDTH_INT */
static const char *pfStrWidthInt[] = {
    "Breite (m)",     //LANGUAGE_DEFINITION_GERMAN,
    "Width (m)",      //LANGUAGE_DEFINITION_ENGLISH,
    "Width (m)",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Width (m)",      //LANGUAGE_DEFINITION_FRENCH,
    "Larghezza (m)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Width (m)",      //LANGUAGE_DEFINITION_SPANISH,
    "Width (m)",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Width (m)",      //LANGUAGE_DEFINITION_POLISH,
    "Width (m)",      //LANGUAGE_DEFINITION_CZEC,
    "Width (m)",      //LANGUAGE_DEFINITION_SWEDISH,
    "Width (m)",      //LANGUAGE_DEFINITION_DUTCH,
    "Width (m)",      //LANGUAGE_DEFINITION_JAPANESE,
    "Width (m)",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Width (m)",      //LANGUAGE_DEFINITION_KOREAN,
    "Width (m)",      //LANGUAGE_DEFINITION_TURKISH,
    "Width (m)",      //LANGUAGE_DEFINITION_CHINESE
    "Width (m)",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Width (m)",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Width (m)",      //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_WIDTH_DEC */
static const char *pfStrWidthDec[] = {
    "Breite (cm)",     //LANGUAGE_DEFINITION_GERMAN,
    "Width (cm)",      //LANGUAGE_DEFINITION_ENGLISH,
    "Width (cm)",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Width (cm)",      //LANGUAGE_DEFINITION_FRENCH,
    "Larghezza (cm)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Width (cm)",      //LANGUAGE_DEFINITION_SPANISH,
    "Width (cm)",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Width (cm)",      //LANGUAGE_DEFINITION_POLISH,
    "Width (cm)",      //LANGUAGE_DEFINITION_CZEC,
    "Width (cm)",      //LANGUAGE_DEFINITION_SWEDISH,
    "Width (cm)",      //LANGUAGE_DEFINITION_DUTCH,
    "Width (cm)",      //LANGUAGE_DEFINITION_JAPANESE,
    "Width (cm)",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Width (cm)",      //LANGUAGE_DEFINITION_KOREAN,
    "Width (cm)",      //LANGUAGE_DEFINITION_TURKISH,
    "Width (cm)",      //LANGUAGE_DEFINITION_CHINESE
    "Width (cm)",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Width (cm)",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Width (cm)",      //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_HEIGHT_INT */
static const char *pfStrHeightInt[] = {
    "Höhe (m)",     //LANGUAGE_DEFINITION_GERMAN,
    "Height (m)",   //LANGUAGE_DEFINITION_ENGLISH,
    "Height (m)",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Height (m)",   //LANGUAGE_DEFINITION_FRENCH,
    "Altezza (m)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Height (m)",   //LANGUAGE_DEFINITION_SPANISH,
    "Height (m)",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Height (m)",   //LANGUAGE_DEFINITION_POLISH,
    "Height (m)",   //LANGUAGE_DEFINITION_CZEC,
    "Height (m)",   //LANGUAGE_DEFINITION_SWEDISH,
    "Height (m)",   //LANGUAGE_DEFINITION_DUTCH,
    "Height (m)",   //LANGUAGE_DEFINITION_JAPANESE,
    "Height (m)",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Height (m)",   //LANGUAGE_DEFINITION_KOREAN,
    "Height (m)",   //LANGUAGE_DEFINITION_TURKISH,
    "Height (m)",   //LANGUAGE_DEFINITION_CHINESE
    "Height (m)",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Height (m)",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Height (m)",   //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_HEIGHT_DEC */
static const char *pfStrHeightDec[] = {
    "Höhe (cm)",     //LANGUAGE_DEFINITION_GERMAN,
    "Height (cm)",   //LANGUAGE_DEFINITION_ENGLISH,
    "Height (cm)",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Height (cm)",   //LANGUAGE_DEFINITION_FRENCH,
    "Altezza (cm)",  //LANGUAGE_DEFINITION_ITALIAN,
    "Height (cm)",   //LANGUAGE_DEFINITION_SPANISH,
    "Height (cm)",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Height (cm)",   //LANGUAGE_DEFINITION_POLISH,
    "Height (cm)",   //LANGUAGE_DEFINITION_CZEC,
    "Height (cm)",   //LANGUAGE_DEFINITION_SWEDISH,
    "Height (cm)",   //LANGUAGE_DEFINITION_DUTCH,
    "Height (cm)",   //LANGUAGE_DEFINITION_JAPANESE,
    "Height (cm)",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Height (cm)",   //LANGUAGE_DEFINITION_KOREAN,
    "Height (cm)",   //LANGUAGE_DEFINITION_TURKISH,
    "Height (cm)",   //LANGUAGE_DEFINITION_CHINESE
    "Height (cm)",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Height (cm)",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Height (cm)",   //LANGUAGE_DEFINITION_HUNGARIAN
};

/* PREFLIGHT_MENU_ROWS_WEIGHT */
static const char *pfStrWeight[] = {
    "Gewicht (t)",  //LANGUAGE_DEFINITION_GERMAN,
    "Weight (t)",   //LANGUAGE_DEFINITION_ENGLISH,
    "Weight (t)",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Weight (t)",   //LANGUAGE_DEFINITION_FRENCH,
    "Peso (t)",     //LANGUAGE_DEFINITION_ITALIAN,
    "Weight (t)",   //LANGUAGE_DEFINITION_SPANISH,
    "Weight (t)",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Weight (t)",   //LANGUAGE_DEFINITION_POLISH,
    "Weight (t)",   //LANGUAGE_DEFINITION_CZEC,
    "Weight (t)",   //LANGUAGE_DEFINITION_SWEDISH,
    "Weight (t)",   //LANGUAGE_DEFINITION_DUTCH,
    "Weight (t)",   //LANGUAGE_DEFINITION_JAPANESE,
    "Weight (t)",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Weight (t)",   //LANGUAGE_DEFINITION_KOREAN,
    "Weight (t)",   //LANGUAGE_DEFINITION_TURKISH,
    "Weight (t)",   //LANGUAGE_DEFINITION_CHINESE
    "Weight (t)",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Weight (t)",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Weight (t)",   //LANGUAGE_DEFINITION_HUNGARIAN
};

/* PREFLIGHT_MENU_ROWS_WEIGHT_DEC */
static const char *pfStrWeightDec[] = {
    "Gewicht (ct)",  //LANGUAGE_DEFINITION_GERMAN,
    "Weight (ct)",   //LANGUAGE_DEFINITION_ENGLISH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Weight (ct)",   //LANGUAGE_DEFINITION_FRENCH,
    "Peso (ct)",     //LANGUAGE_DEFINITION_ITALIAN,
    "Weight (ct)",   //LANGUAGE_DEFINITION_SPANISH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Weight (ct)",   //LANGUAGE_DEFINITION_POLISH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_CZEC,
    "Weight (ct)",   //LANGUAGE_DEFINITION_SWEDISH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_DUTCH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_JAPANESE,
    "Weight (ct)",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Weight (ct)",   //LANGUAGE_DEFINITION_KOREAN,
    "Weight (ct)",   //LANGUAGE_DEFINITION_TURKISH,
    "Weight (ct)",   //LANGUAGE_DEFINITION_CHINESE
    "Weight (ct)",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Weight (ct)",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Weight (ct)",   //LANGUAGE_DEFINITION_HUNGARIAN
};

/* PREFLIGHT_MENU_ROWS_FUEL_TYPE */
static const char *pfStrFuelType[] = {
    "Kraftstoffart",  //LANGUAGE_DEFINITION_GERMAN,
    "Fuel type",      //LANGUAGE_DEFINITION_ENGLISH,
    "Fuel type",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Fuel type",      //LANGUAGE_DEFINITION_FRENCH,
    "Carburante",     //LANGUAGE_DEFINITION_ITALIAN,
    "Fuel type",      //LANGUAGE_DEFINITION_SPANISH,
    "Fuel type",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Fuel type",      //LANGUAGE_DEFINITION_POLISH,
    "Fuel type",      //LANGUAGE_DEFINITION_CZEC,
    "Fuel type",      //LANGUAGE_DEFINITION_SWEDISH,
    "Fuel type",      //LANGUAGE_DEFINITION_DUTCH,
    "Fuel type",      //LANGUAGE_DEFINITION_JAPANESE,
    "Fuel type",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Fuel type",      //LANGUAGE_DEFINITION_KOREAN,
    "Fuel type",      //LANGUAGE_DEFINITION_TURKISH,
    "Fuel type",      //LANGUAGE_DEFINITION_CHINESE
    "Fuel type",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Fuel type",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Fuel type",      //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *pfStrFuelDiesel[] = {
    "Diesel",  //LANGUAGE_DEFINITION_GERMAN,
    "Diesel",  //LANGUAGE_DEFINITION_ENGLISH,
    "Diesel",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Diesel",  //LANGUAGE_DEFINITION_FRENCH,
    "Diesel",  //LANGUAGE_DEFINITION_ITALIAN,
    "Diesel",  //LANGUAGE_DEFINITION_SPANISH,
    "Diesel",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Diesel",  //LANGUAGE_DEFINITION_POLISH,
    "Diesel",  //LANGUAGE_DEFINITION_CZEC,
    "Diesel",  //LANGUAGE_DEFINITION_SWEDISH,
    "Diesel",  //LANGUAGE_DEFINITION_DUTCH,
    "Diesel",  //LANGUAGE_DEFINITION_JAPANESE,
    "Diesel",  //LANGUAGE_DEFINITION_RUSSIAN,
    "Diesel",  //LANGUAGE_DEFINITION_KOREAN,
    "Diesel",  //LANGUAGE_DEFINITION_TURKISH,
    "Diesel",  //LANGUAGE_DEFINITION_CHINESE
    "Diesel",  //LANGUAGE_DEFINITION_NORWEGIAN
    "Diesel",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "Diesel",  //LANGUAGE_DEFINITION_HUNGARIAN
};
static const char *pfStrFuelGasoline[] = {
    "Benzin",    //LANGUAGE_DEFINITION_GERMAN,
    "Gasoline",  //LANGUAGE_DEFINITION_ENGLISH,
    "Gasoline",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Essence",   //LANGUAGE_DEFINITION_FRENCH,
    "Benzina",   //LANGUAGE_DEFINITION_ITALIAN,
    "Gasolina",  //LANGUAGE_DEFINITION_SPANISH,
    "Gasoline",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Gasoline",  //LANGUAGE_DEFINITION_POLISH,
    "Gasoline",  //LANGUAGE_DEFINITION_CZEC,
    "Gasoline",  //LANGUAGE_DEFINITION_SWEDISH,
    "Gasoline",  //LANGUAGE_DEFINITION_DUTCH,
    "Gasoline",  //LANGUAGE_DEFINITION_JAPANESE,
    "Gasoline",  //LANGUAGE_DEFINITION_RUSSIAN,
    "Gasoline",  //LANGUAGE_DEFINITION_KOREAN,
    "Gasoline",  //LANGUAGE_DEFINITION_TURKISH,
    "Gasoline",  //LANGUAGE_DEFINITION_CHINESE
    "Gasoline",  //LANGUAGE_DEFINITION_NORWEGIAN
    "Gasoline",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "Gasoline",  //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_RAMPS */
static const char *pfStrRamps[] = {
    "Auffahrkeile",       //LANGUAGE_DEFINITION_GERMAN,
    "Levelling ramps",    //LANGUAGE_DEFINITION_ENGLISH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Cales de niveau",    //LANGUAGE_DEFINITION_FRENCH,
    "Cunei Livellatori",  //LANGUAGE_DEFINITION_ITALIAN,
    "Calzos",             //LANGUAGE_DEFINITION_SPANISH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Levelling ramps",    //LANGUAGE_DEFINITION_POLISH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_CZEC,
    "Levelling ramps",    //LANGUAGE_DEFINITION_SWEDISH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_DUTCH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_JAPANESE,
    "Levelling ramps",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Levelling ramps",    //LANGUAGE_DEFINITION_KOREAN,
    "Levelling ramps",    //LANGUAGE_DEFINITION_TURKISH,
    "Levelling ramps",    //LANGUAGE_DEFINITION_CHINESE
    "Levelling ramps",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Levelling ramps",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Levelling ramps",    //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_SHOWER */
static const char *pfStrShower[] = {
    "Duschtür",            //LANGUAGE_DEFINITION_GERMAN,
    "Shower door",         //LANGUAGE_DEFINITION_ENGLISH,
    "Shower door",         //LANGUAGE_DEFINITION_ENGLISH US,
    "Porte de douche",     //LANGUAGE_DEFINITION_FRENCH,
    "Porta della doccia",  //LANGUAGE_DEFINITION_ITALIAN,
    "Puerta del baño",     //LANGUAGE_DEFINITION_SPANISH,
    "Shower door",         //LANGUAGE_DEFINITION_PORTUGUESE,
    "Shower door",         //LANGUAGE_DEFINITION_POLISH,
    "Shower door",         //LANGUAGE_DEFINITION_CZEC,
    "Shower door",         //LANGUAGE_DEFINITION_SWEDISH,
    "Shower door",         //LANGUAGE_DEFINITION_DUTCH,
    "Shower door",         //LANGUAGE_DEFINITION_JAPANESE,
    "Shower door",         //LANGUAGE_DEFINITION_RUSSIAN,
    "Shower door",         //LANGUAGE_DEFINITION_KOREAN,
    "Shower door",         //LANGUAGE_DEFINITION_TURKISH,
    "Shower door",         //LANGUAGE_DEFINITION_CHINESE
    "Shower door",         //LANGUAGE_DEFINITION_NORWEGIAN
    "Shower door",         //LANGUAGE_DEFINITION_SLOVAKIAN
    "Shower door",         //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_WINDOWS */
static const char *pfStrWindows[] = {
    "Fenster",       //LANGUAGE_DEFINITION_GERMAN,
    "Windows",       //LANGUAGE_DEFINITION_ENGLISH,
    "Windows",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Les fenêtres",  //LANGUAGE_DEFINITION_FRENCH,
    "Finestre",      //LANGUAGE_DEFINITION_ITALIAN,
    "Ventanas",      //LANGUAGE_DEFINITION_SPANISH,
    "Windows",       //LANGUAGE_DEFINITION_PORTUGUESE,
    "Windows",       //LANGUAGE_DEFINITION_POLISH,
    "Windows",       //LANGUAGE_DEFINITION_CZEC,
    "Windows",       //LANGUAGE_DEFINITION_SWEDISH,
    "Windows",       //LANGUAGE_DEFINITION_DUTCH,
    "Windows",       //LANGUAGE_DEFINITION_JAPANESE,
    "Windows",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Windows",       //LANGUAGE_DEFINITION_KOREAN,
    "Windows",       //LANGUAGE_DEFINITION_TURKISH,
    "Windows",       //LANGUAGE_DEFINITION_CHINESE
    "Windows",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Windows",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Windows",       //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_GAS */
static const char *pfStrGas[] = {
    "Gas",  //LANGUAGE_DEFINITION_GERMAN,
    "Gas",  //LANGUAGE_DEFINITION_ENGLISH,
    "Gas",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Gaz",  //LANGUAGE_DEFINITION_FRENCH,
    "Gas",  //LANGUAGE_DEFINITION_ITALIAN,
    "Gas",  //LANGUAGE_DEFINITION_SPANISH,
    "Gas",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "Gas",  //LANGUAGE_DEFINITION_POLISH,
    "Gas",  //LANGUAGE_DEFINITION_CZEC,
    "Gas",  //LANGUAGE_DEFINITION_SWEDISH,
    "Gas",  //LANGUAGE_DEFINITION_DUTCH,
    "Gas",  //LANGUAGE_DEFINITION_JAPANESE,
    "Gas",  //LANGUAGE_DEFINITION_RUSSIAN,
    "Gas",  //LANGUAGE_DEFINITION_KOREAN,
    "Gas",  //LANGUAGE_DEFINITION_TURKISH,
    "Gas",  //LANGUAGE_DEFINITION_CHINESE
    "Gas",  //LANGUAGE_DEFINITION_NORWEGIAN
    "Gas",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "Gas",  //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_JACKS */
static const char *pfStrJacks[] = {
    "Hubstützen",           //LANGUAGE_DEFINITION_GERMAN,
    "Levelling jacks",      //LANGUAGE_DEFINITION_ENGLISH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_ENGLISH US,
    "Verin stabilisateur",  //LANGUAGE_DEFINITION_FRENCH,
    "Stabilizzatori",       //LANGUAGE_DEFINITION_ITALIAN,
    "Estabilizadore",       //LANGUAGE_DEFINITION_SPANISH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_PORTUGUESE,
    "Levelling jacks",      //LANGUAGE_DEFINITION_POLISH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_CZEC,
    "Levelling jacks",      //LANGUAGE_DEFINITION_SWEDISH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_DUTCH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_JAPANESE,
    "Levelling jacks",      //LANGUAGE_DEFINITION_RUSSIAN,
    "Levelling jacks",      //LANGUAGE_DEFINITION_KOREAN,
    "Levelling jacks",      //LANGUAGE_DEFINITION_TURKISH,
    "Levelling jacks",      //LANGUAGE_DEFINITION_CHINESE
    "Levelling jacks",      //LANGUAGE_DEFINITION_NORWEGIAN
    "Levelling jacks",      //LANGUAGE_DEFINITION_SLOVAKIAN
    "Levelling jacks",      //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_STORAGE */
static const char *pfStrStorage[] = {
    "Klappen",                 //LANGUAGE_DEFINITION_GERMAN,
    "External storage doors",  //LANGUAGE_DEFINITION_ENGLISH,
    "External storage doors",  //LANGUAGE_DEFINITION_ENGLISH US,
    "Volets",                  //LANGUAGE_DEFINITION_FRENCH,
    "Portelloni esterni",      //LANGUAGE_DEFINITION_ITALIAN,
    "Solapas exteriores",      //LANGUAGE_DEFINITION_SPANISH,
    "External storage doors",  //LANGUAGE_DEFINITION_PORTUGUESE,
    "External storage doors",  //LANGUAGE_DEFINITION_POLISH,
    "External storage doors",  //LANGUAGE_DEFINITION_CZEC,
    "External storage doors",  //LANGUAGE_DEFINITION_SWEDISH,
    "External storage doors",  //LANGUAGE_DEFINITION_DUTCH,
    "External storage doors",  //LANGUAGE_DEFINITION_JAPANESE,
    "External storage doors",  //LANGUAGE_DEFINITION_RUSSIAN,
    "External storage doors",  //LANGUAGE_DEFINITION_KOREAN,
    "External storage doors",  //LANGUAGE_DEFINITION_TURKISH,
    "External storage doors",  //LANGUAGE_DEFINITION_CHINESE
    "External storage doors",  //LANGUAGE_DEFINITION_NORWEGIAN
    "External storage doors",  //LANGUAGE_DEFINITION_SLOVAKIAN
    "External storage doors",  //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_MAINS */
static const char *pfStrMains[] = {
    "Landstrom",           //LANGUAGE_DEFINITION_GERMAN,
    "Mains (power)",       //LANGUAGE_DEFINITION_ENGLISH,
    "Mains (power)",       //LANGUAGE_DEFINITION_ENGLISH US,
    "Prise extérieure",    //LANGUAGE_DEFINITION_FRENCH,
    "Alimentazione 220V",  //LANGUAGE_DEFINITION_ITALIAN,
    "Toma eléctrica",      //LANGUAGE_DEFINITION_SPANISH,
    "Mains (power)",       //LANGUAGE_DEFINITION_PORTUGUESE,
    "Mains (power)",       //LANGUAGE_DEFINITION_POLISH,
    "Mains (power)",       //LANGUAGE_DEFINITION_CZEC,
    "Mains (power)",       //LANGUAGE_DEFINITION_SWEDISH,
    "Mains (power)",       //LANGUAGE_DEFINITION_DUTCH,
    "Mains (power)",       //LANGUAGE_DEFINITION_JAPANESE,
    "Mains (power)",       //LANGUAGE_DEFINITION_RUSSIAN,
    "Mains (power)",       //LANGUAGE_DEFINITION_KOREAN,
    "Mains (power)",       //LANGUAGE_DEFINITION_TURKISH,
    "Mains (power)",       //LANGUAGE_DEFINITION_CHINESE
    "Mains (power)",       //LANGUAGE_DEFINITION_NORWEGIAN
    "Mains (power)",       //LANGUAGE_DEFINITION_SLOVAKIAN
    "Mains (power)",       //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_AWNING */
static const char *pfStrAwning[] = {
    "Markise",    //LANGUAGE_DEFINITION_GERMAN,
    "Awning",     //LANGUAGE_DEFINITION_ENGLISH,
    "Awning",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Store",      //LANGUAGE_DEFINITION_FRENCH,
    "Tendalino",  //LANGUAGE_DEFINITION_ITALIAN,
    "Toldo",      //LANGUAGE_DEFINITION_SPANISH,
    "Awning",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Awning",     //LANGUAGE_DEFINITION_POLISH,
    "Awning",     //LANGUAGE_DEFINITION_CZEC,
    "Awning",     //LANGUAGE_DEFINITION_SWEDISH,
    "Awning",     //LANGUAGE_DEFINITION_DUTCH,
    "Awning",     //LANGUAGE_DEFINITION_JAPANESE,
    "Awning",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Awning",     //LANGUAGE_DEFINITION_KOREAN,
    "Awning",     //LANGUAGE_DEFINITION_TURKISH,
    "Awning",     //LANGUAGE_DEFINITION_CHINESE
    "Awning",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Awning",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Awning",     //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_ANTENNA */
static const char *pfStrAntenna[] = {
    "Sat-Antenne",          //LANGUAGE_DEFINITION_GERMAN,
    "Satellite antenna",    //LANGUAGE_DEFINITION_ENGLISH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Antenne satellite",    //LANGUAGE_DEFINITION_FRENCH,
    "Antenna Satellitare",  //LANGUAGE_DEFINITION_ITALIAN,
    "Antena satélite",      //LANGUAGE_DEFINITION_SPANISH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Satellite antenna",    //LANGUAGE_DEFINITION_POLISH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_CZEC,
    "Satellite antenna",    //LANGUAGE_DEFINITION_SWEDISH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_DUTCH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_JAPANESE,
    "Satellite antenna",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Satellite antenna",    //LANGUAGE_DEFINITION_KOREAN,
    "Satellite antenna",    //LANGUAGE_DEFINITION_TURKISH,
    "Satellite antenna",    //LANGUAGE_DEFINITION_CHINESE
    "Satellite antenna",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Satellite antenna",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Satellite antenna",    //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_CABINETS */
static const char *pfStrCabinets[] = {
    "Schränke",    //LANGUAGE_DEFINITION_GERMAN,
    "Cabinets",    //LANGUAGE_DEFINITION_ENGLISH,
    "Cabinets",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Armoires ",   //LANGUAGE_DEFINITION_FRENCH,
    "Armadietti",  //LANGUAGE_DEFINITION_ITALIAN,
    "Armarios",    //LANGUAGE_DEFINITION_SPANISH,
    "Cabinets",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Cabinets",    //LANGUAGE_DEFINITION_POLISH,
    "Cabinets",    //LANGUAGE_DEFINITION_CZEC,
    "Cabinets",    //LANGUAGE_DEFINITION_SWEDISH,
    "Cabinets",    //LANGUAGE_DEFINITION_DUTCH,
    "Cabinets",    //LANGUAGE_DEFINITION_JAPANESE,
    "Cabinets",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Cabinets",    //LANGUAGE_DEFINITION_KOREAN,
    "Cabinets",    //LANGUAGE_DEFINITION_TURKISH,
    "Cabinets",    //LANGUAGE_DEFINITION_CHINESE
    "Cabinets",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Cabinets",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Cabinets",    //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_DRAWERS */
static const char *pfStrDrawers[] = {
    "Schubladen",  //LANGUAGE_DEFINITION_GERMAN,
    "Drawers",     //LANGUAGE_DEFINITION_ENGLISH,
    "Drawers",     //LANGUAGE_DEFINITION_ENGLISH US,
    "Tiroirs",     //LANGUAGE_DEFINITION_FRENCH,
    "Cassetti",    //LANGUAGE_DEFINITION_ITALIAN,
    "Cajones",     //LANGUAGE_DEFINITION_SPANISH,
    "Drawers",     //LANGUAGE_DEFINITION_PORTUGUESE,
    "Drawers",     //LANGUAGE_DEFINITION_POLISH,
    "Drawers",     //LANGUAGE_DEFINITION_CZEC,
    "Drawers",     //LANGUAGE_DEFINITION_SWEDISH,
    "Drawers",     //LANGUAGE_DEFINITION_DUTCH,
    "Drawers",     //LANGUAGE_DEFINITION_JAPANESE,
    "Drawers",     //LANGUAGE_DEFINITION_RUSSIAN,
    "Drawers",     //LANGUAGE_DEFINITION_KOREAN,
    "Drawers",     //LANGUAGE_DEFINITION_TURKISH,
    "Drawers",     //LANGUAGE_DEFINITION_CHINESE
    "Drawers",     //LANGUAGE_DEFINITION_NORWEGIAN
    "Drawers",     //LANGUAGE_DEFINITION_SLOVAKIAN
    "Drawers",     //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_OBJECTS */
static const char *pfStrObjects[] = {
    "Lose Objekte",     //LANGUAGE_DEFINITION_GERMAN,
    "Loose objects",    //LANGUAGE_DEFINITION_ENGLISH,
    "Loose objects",    //LANGUAGE_DEFINITION_ENGLISH US,
    "Objets en vrac ",  //LANGUAGE_DEFINITION_FRENCH,
    "Oggetti Sciolti",  //LANGUAGE_DEFINITION_ITALIAN,
    "Objetos sueltos",  //LANGUAGE_DEFINITION_SPANISH,
    "Loose objects",    //LANGUAGE_DEFINITION_PORTUGUESE,
    "Loose objects",    //LANGUAGE_DEFINITION_POLISH,
    "Loose objects",    //LANGUAGE_DEFINITION_CZEC,
    "Loose objects",    //LANGUAGE_DEFINITION_SWEDISH,
    "Loose objects",    //LANGUAGE_DEFINITION_DUTCH,
    "Loose objects",    //LANGUAGE_DEFINITION_JAPANESE,
    "Loose objects",    //LANGUAGE_DEFINITION_RUSSIAN,
    "Loose objects",    //LANGUAGE_DEFINITION_KOREAN,
    "Loose objects",    //LANGUAGE_DEFINITION_TURKISH,
    "Loose objects",    //LANGUAGE_DEFINITION_CHINESE
    "Loose objects",    //LANGUAGE_DEFINITION_NORWEGIAN
    "Loose objects",    //LANGUAGE_DEFINITION_SLOVAKIAN
    "Loose objects",    //LANGUAGE_DEFINITION_HUNGARIAN
};
/* PREFLIGHT_MENU_ROWS_TABLE */
static const char *pfStrTable[] = {
    "Tisch",   //LANGUAGE_DEFINITION_GERMAN,
    "Table",   //LANGUAGE_DEFINITION_ENGLISH,
    "Table",   //LANGUAGE_DEFINITION_ENGLISH US,
    "Table",   //LANGUAGE_DEFINITION_FRENCH,
    "Tavolo",  //LANGUAGE_DEFINITION_ITALIAN,
    "Mesa",    //LANGUAGE_DEFINITION_SPANISH,
    "Table",   //LANGUAGE_DEFINITION_PORTUGUESE,
    "Table",   //LANGUAGE_DEFINITION_POLISH,
    "Table",   //LANGUAGE_DEFINITION_CZEC,
    "Table",   //LANGUAGE_DEFINITION_SWEDISH,
    "Table",   //LANGUAGE_DEFINITION_DUTCH,
    "Table",   //LANGUAGE_DEFINITION_JAPANESE,
    "Table",   //LANGUAGE_DEFINITION_RUSSIAN,
    "Table",   //LANGUAGE_DEFINITION_KOREAN,
    "Table",   //LANGUAGE_DEFINITION_TURKISH,
    "Table",   //LANGUAGE_DEFINITION_CHINESE
    "Table",   //LANGUAGE_DEFINITION_NORWEGIAN
    "Table",   //LANGUAGE_DEFINITION_SLOVAKIAN
    "Table",   //LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *const pfHeader[] = {
    "LxBxH: ",            //!< LANGUAGE_DEFINITION_GERMAN
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_ENGLISH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_ENGLISH_US
    "LxLxH: ",            //!< LANGUAGE_DEFINITION_FRENCH
    "LxPxA: ",            //!< LANGUAGE_DEFINITION_ITALIAN
    "lar.x an. x al.: ",  //!< LANGUAGE_DEFINITION_SPANISH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_PORTUGUESE
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_POLISH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_CZECH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_SWEDISH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_DUTCH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_JAPANESE
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_RUSSIAN
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_KOREAN
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_TURKISH
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_CHINESE
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_NORWEGIAN
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "LxWxH: ",            //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *const pfText1[] = {
    "Bitte sichern Sie: ",                   //!< LANGUAGE_DEFINITION_GERMAN
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_ENGLISH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Veuillez vous assurer de sécuriser: ",  //!< LANGUAGE_DEFINITION_FRENCH
    "Assicurarsi di bloccare: ",             //!< LANGUAGE_DEFINITION_ITALIAN
    "Por favor asegure: ",                   //!< LANGUAGE_DEFINITION_SPANISH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_POLISH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_CZECH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_SWEDISH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_DUTCH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_JAPANESE
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_RUSSIAN
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_KOREAN
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_TURKISH
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_CHINESE
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Please make sure to secure: ",          //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *const pfText2[] = {
    "Entfernen Sie: ",  //!< LANGUAGE_DEFINITION_GERMAN
    "Remove: ",         //!< LANGUAGE_DEFINITION_ENGLISH
    "Remove: ",         //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Supprimer: ",      //!< LANGUAGE_DEFINITION_FRENCH
    "Rimuovere: ",      //!< LANGUAGE_DEFINITION_ITALIAN
    "Retirar: ",        //!< LANGUAGE_DEFINITION_SPANISH
    "Remove: ",         //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Remove: ",         //!< LANGUAGE_DEFINITION_POLISH
    "Remove: ",         //!< LANGUAGE_DEFINITION_CZECH
    "Remove: ",         //!< LANGUAGE_DEFINITION_SWEDISH
    "Remove: ",         //!< LANGUAGE_DEFINITION_DUTCH
    "Remove: ",         //!< LANGUAGE_DEFINITION_JAPANESE
    "Remove: ",         //!< LANGUAGE_DEFINITION_RUSSIAN
    "Remove: ",         //!< LANGUAGE_DEFINITION_KOREAN
    "Remove: ",         //!< LANGUAGE_DEFINITION_TURKISH
    "Remove: ",         //!< LANGUAGE_DEFINITION_CHINESE
    "Remove: ",         //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Remove: ",         //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Remove: ",         //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static bool sync_clock_status = false;
bool vehicle_get_sync_clock(void) {
    return sync_clock_status;
}

void vehicle_set_sync_clock(bool val) {
    sync_clock_status = val;
}

/**
 * @brief variabile con lo stato del can. 0 => no can activity, != 0 => can activity
 * 
 */
uint32_t can_activity;
uint32_t lin_activity;

/**
 * @brief Restituisce la stringa associata allo stato dell'attivazione della camera temporizzato.
 * @return On o Off.
 */
static const char *vehicle_get_current_sync_clock_str(void) {
    bool option_val = vehicle_get_sync_clock();
    const char *option;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (option_val) {
        option = on_str[language];
    } else {
        option = off_str[language];
    }
    return option;
}

static bool vehicle_settings_disable = false;
static bool clock_management_disable = false;
static bool lin_management_disable = false;
static bool can_management_disable = false;

/**
 * @brief flag che definisce se il veicolo provvede a fornire un menu di configurazione proprio.
 * Questa flag viene messa a true dal veicolo nel caso in cui fornisca il menu aggiuntivo.
 */

void alpine_menu_disable_vehicle_settings(void) {
    menu_manager_clear_active_bit(alpine_main_menu_manager, MAIN_MENU_ROWS_VEHICLE);
    vehicle_settings_disable = true;
}

void alpine_menu_disable_clock_management(void) {
    menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_DATETIME);
    clock_management_disable = true;
}

void alpine_menu_disable_lin_management(void) {
    menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_LIN_STATUS);
    lin_management_disable = true;
}

void alpine_menu_disable_can_management(void) {
    menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_CAN);
    can_management_disable = true;
}

static bool main_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(alpine_main_menu_manager), INTERFACE_MENU_MAIN, interface_settings_str[language]);
}

static bool main_menu_send_general_settings(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, GENERAL_MENU_VERS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, general_settings_str[language], empty);
}

static bool main_menu_send_interface_settings(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_VERS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, interface_settings_str[language], empty);
}

static bool main_menu_send_vehicle_settings(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_VEHICLE_MENU, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, vehicle_settings_str[language], empty);
}

static bool main_menu_send_preflight_settings(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_PREFLIGHT_MENU, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, preflight_settings_str[language], empty);
}

static inline const char *can_status_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (can_activity != 0) {
        retVal = yes_str[language];
    } else {
        retVal = no_str[language];
    }
    return retVal;
}

static inline const char *lin_status_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (lin_activity != 0) {
        retVal = yes_str[language];
    } else {
        retVal = no_str[language];
    }
    return retVal;
}

static inline const char *interface_status_string(void) {
    const char *retVal;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (settings_read(EEPROM_VERSION) == EEPROM_VERSION_LOCAL) {
        retVal = ok_str[language];
    } else {
        retVal = error_str[language];
    }
    return retVal;
}

static bool version_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(alpine_menu_manager), GENERAL_MENU_VERS, software_version_str[language]);
}

static bool version_menu_send_general_settings(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, VERSION_MENU_ROWS_VERSION, MESSAGE_30_ROW_TYPE_2COLUMN, 0, swc_interface_str[language], interface_code);
}

static bool version_menu_send_can_status(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *can_act_str = can_status_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, VERSION_MENU_ROWS_CAN, MESSAGE_30_ROW_TYPE_2COLUMN, 0, can_activity_str[language], can_act_str);
}

static bool version_menu_send_interface_status(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *if_status_str = interface_status_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, VERSION_MENU_ROWS_IF_STATUS, MESSAGE_30_ROW_TYPE_2COLUMN, 0, interface_status_str[language], if_status_str);
}

static bool version_menu_send_lin_status(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *lin_act_str = lin_status_string();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, VERSION_MENU_ROWS_LIN_STATUS, MESSAGE_30_ROW_TYPE_2COLUMN, 0, lin_activity_str[language], lin_act_str);
}

static bool version_menu_send_datetime_menu(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, INTERFACE_MENU_CLOCK, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, clock_date_str[language], empty);
}

uint8_t day_in_month(uint16_t year, uint8_t month) {
    uint8_t retVal = 31;
    bool is_leap_year = false;
    if ((year % 4) == 0) {
        if ((year % 100) == 0) {
            if ((year % 400) == 0) {
                is_leap_year = true;
            } else {
                is_leap_year = false;
            }
        } else {
            is_leap_year = true;
        }
    }
    switch (month) {
        case 4:
        case 6:
        case 9:
        case 11:
            retVal = 30;
            break;
        case 2:
            if (is_leap_year) {
                retVal = 29;
            } else {
                retVal = 28;
            }
    }
    return retVal;
}

uint8_t check_day_border(uint16_t year, uint8_t month, uint8_t day) {
    uint8_t retVal = day_in_month(year, month);
    if (day == 0) {
        retVal = 1;
    } else if (day < retVal) {
        retVal = day;
    }
    return retVal;
}

static uint16_t vehicle_year = 2019;
static uint8_t vehicle_month = 1;
static uint8_t vehicle_day = 1;
static uint8_t vehicle_hour = 0;
static uint8_t vehicle_minute = 0;
void vehicle_update_sync_clock(bool val) {
    sync_clock_status = val;
    if (sync_clock_status) {
        if ((vehicle_year >= 2000) && (vehicle_year < 2055)) {
            if ((vehicle_month >= 1) && (vehicle_month <= 12)) {
                if ((vehicle_day >= 1) && (vehicle_day <= check_day_border(vehicle_year, vehicle_month, vehicle_day))) {
                    if (vehicle_hour < 24) {
                        if (vehicle_minute < 60) {
                            LOG_DEF_NORMAL("%s a valid date.\r\n", __PRETTY_FUNCTION__);
                            //Emit only if a valid date was received by the radio.
                            event_time_changed_emit(vehicle_hour, vehicle_minute, vehicle_day, vehicle_month, vehicle_year);
                        }
                    }
                }
            }
        }
    }
}

static bool clock_24h = false;

static bool datetime_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(alpine_menu_datetime_manager), INTERFACE_MENU_CLOCK, clock_date_str[language]);
}

static bool datetime_menu_send_clock_string(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    bool option = vehicle_get_sync_clock();
    const char *sync_str = vehicle_get_current_sync_clock_str();
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_ENABLE, 0, sync_clock_str[language], sync_str, option);
}

static bool datetime_menu_send_year(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t deactivate_manual = 0;
    bool option = vehicle_get_sync_clock();
    if (option) {
        deactivate_manual = 1;
    } else {
        deactivate_manual = 0;
    }
    uint8_t year = vehicle_year - 2000;
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_YEARS, deactivate_manual, year_str[language], empty, year, 10, 0x80);
}

static bool datetime_menu_send_month(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t deactivate_manual = 0;
    bool option = vehicle_get_sync_clock();
    if (option) {
        deactivate_manual = 1;
    } else {
        deactivate_manual = 0;
    }
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_MONTHS, deactivate_manual, month_str[language], empty, vehicle_month, 0, 12);
}

static bool datetime_menu_send_day(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t deactivate_manual = 0;
    bool option = vehicle_get_sync_clock();
    if (option) {
        deactivate_manual = 1;
    } else {
        deactivate_manual = 0;
    }
    uint8_t day = check_day_border(vehicle_year, vehicle_month, vehicle_day);
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_DAYS, deactivate_manual, day_str[language], empty, day, 0, 31);
}

static bool datetime_menu_send_hour(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t deactivate_manual = 0;
    bool option = vehicle_get_sync_clock();
    if (option) {
        deactivate_manual = 1;
    } else {
        deactivate_manual = 0;
    }
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_HOUR, deactivate_manual, hour_str[language], empty, vehicle_hour, 0, 23);
}

static bool datetime_menu_send_minute(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t deactivate_manual = 0;
    bool option = vehicle_get_sync_clock();
    if (option) {
        deactivate_manual = 1;
    } else {
        deactivate_manual = 0;
    }
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_MINUTE, deactivate_manual, minute_str[language], empty, vehicle_minute, 0, 59);
}

static bool datetime_menu_send_format(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    bool retVal = false;
    if (!radio_get_handles_clock_format()) {
        const char *str;
        if (clock_24h == true) {
            str = h24[language];
        } else {
            str = h12[language];
        }
        retVal = message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, DATETIME_MENU_ROWS_HOUR_FORMAT, MESSAGE_30_ROW_TYPE_2COLUMN, 0, time_format_str[language], str);
    }
    return retVal;
}

/**
 * @brief Riga del menu che viene modificata.
 * Valore ricevuto dal messaggio 0xE5 in attesa del valore del messaggio 0xE8.
 */
static uint8_t edited_row = 0;
/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_clock_date_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    (void)menu_index;
#if __amd64__
    LOG_DEF_NORMAL("%d %s %02X %02X %02X\r\n", xTaskGetTickCount(),
                   __PRETTY_FUNCTION__, row_pressed, menu_index, menu_data);
#else
    LOG_DEF_NORMAL("%ld %s %02X %02X %02X\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__, row_pressed, menu_index, menu_data);
#endif
    if (row_pressed == INTERFACE_MENU_CLOCK) {
        /// Una volta verificato che il menu visualizzato sulla radio e' quello corretto...
        /// leggo il dato della riga premuta ed imposto l'opzione corretta.
        ///
        if (menu_data == 0x01) {
            LOG_DEF_NORMAL("Cambio valore sync clock da %d ", vehicle_get_sync_clock());
            vehicle_update_sync_clock(!vehicle_get_sync_clock());
            LOG_DEF_NORMAL("a %d\r\n", vehicle_get_sync_clock());
            enable_radio_menu_refresh(alpine_menu_datetime_manager);
            menu_walking_push(INTERFACE_MENU_CLOCK);
        } else if (menu_data == 0x07) {
            LOG_DEF_NORMAL("Cambio 12/24H\r\n");
            clock_24h = !clock_24h;
            if (clock_24h) {
                event_time_format_changed_emit(0x00, 0x00);
            } else {
                event_time_format_changed_emit(0x01, 0x00);
            }
            enable_radio_menu_refresh(alpine_menu_datetime_manager);
            menu_walking_push(INTERFACE_MENU_CLOCK);
        } else {
            if (menu_data != edited_row) {
                LOG_DEF_NORMAL("Cambio riga attiva\r\n");
                edited_row = menu_data;
            }
        }
    }
}

static void handle_vehicle_time(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_VEHICLE_TIME_CHANGED) {
        bool changed = false;
        if (vehicle_year != msg->as_time.year) {
            changed = true;
        }
        if (vehicle_month != msg->as_time.month) {
            changed = true;
        }
        if (vehicle_day != msg->as_time.day) {
            changed = true;
        }
        if (vehicle_hour != msg->as_time.hour) {
            changed = true;
        }
        if (vehicle_minute != msg->as_time.minute) {
            changed = true;
        }
        vehicle_year = msg->as_time.year;
        vehicle_month = msg->as_time.month;
        vehicle_day = msg->as_time.day;
        vehicle_hour = msg->as_time.hour;
        vehicle_minute = msg->as_time.minute;
        if (changed) {
            redraw_last_menu();
        }
    }
}

static inline void handle_clock_date_value_received(
    const uint8_t row_pressed, const uint8_t menu_index) {
#if __amd64__
    LOG_DEF_NORMAL("%d %s %d %d\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__,
                   row_pressed, menu_index);
#else
    LOG_DEF_NORMAL("%ld %s %d %d\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__, row_pressed, menu_index);
#endif

    switch (edited_row) {
        case DATETIME_MENU_ROWS_YEARS:
            LOG_DEF_NORMAL("Cambio anno %d\r\n", 2000 + row_pressed);
            vehicle_year = 2000 + row_pressed;
            break;
        case DATETIME_MENU_ROWS_MONTHS:
            LOG_DEF_NORMAL("Cambio mese %d\r\n", row_pressed);
            vehicle_month = row_pressed;
            if (vehicle_month == 0x00) {
                vehicle_month = 1;
            }
            break;
        case DATETIME_MENU_ROWS_DAYS:
            LOG_DEF_NORMAL("Cambio giorno %d\r\n", row_pressed);
            vehicle_day = check_day_border(vehicle_year, vehicle_month, row_pressed);
            break;
        case DATETIME_MENU_ROWS_HOUR:
            LOG_DEF_NORMAL("Cambio ora %d\r\n", row_pressed);
            vehicle_hour = row_pressed;
            break;
        case DATETIME_MENU_ROWS_MINUTE:
            LOG_DEF_NORMAL("Cambio minuto %d\r\n", row_pressed);
            vehicle_minute = row_pressed;
            break;
        default:
            LOG_DEF_NORMAL("Row %d not handled\r\n", edited_row);
            break;
    }
    LOG_DEF_NORMAL("%s year: %d vehicle_month: %d vehicle_day: %d hour: %d minute: %d\r\n",
                   __PRETTY_FUNCTION__, vehicle_year, vehicle_month, vehicle_day, vehicle_hour, vehicle_minute);
    vehicle_send_changed_clock_date(vehicle_year, vehicle_month, vehicle_day, vehicle_hour, vehicle_minute);

    ///e quindi richiedo la ritrasmissione del menu.
    enable_radio_menu_refresh(alpine_menu_datetime_manager);
    menu_walking_push(INTERFACE_MENU_CLOCK);
}

/*********************************INIZIO GESTIONE PREFLIGHT***********************************************/
static uint8_t pfTime;
static uint8_t lengthInt;
static uint8_t lengthDec;
static uint8_t widthInt;
static uint8_t widthDec;
static uint8_t heightInt;
static uint8_t heightDec;
static uint8_t weight;
static uint8_t weightDec;
static enum PREFLIGHT_MENU_FUEL_TYPE fuelType;
static bool ramps;
static bool shower;
static bool windows;
static bool gas;
static bool jacks;
static bool storage;
static bool mains;
static bool awning;
static bool antenna;
static bool cabinets;
static bool drawers;
static bool objects;
static bool table;
static uint8_t flags0, flags1;
static bool preflight_check_displayed = false;

bool preflight_check_menu_was_displayed(void) {
    return preflight_check_displayed;
}

void preflight_check_menu_displayed_reset(void) {
    preflight_check_displayed = false;
}

static bool preflight_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    preflight_check_displayed = true;
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(alpine_preflight_menu_manager), INTERFACE_PREFLIGHT_MENU, pfMenuHeader[language]);
}

static bool preflight_menu_send_time(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_TIME, 0, pfStrTime[language], empty, pfTime, 1, 99);
}

static bool preflight_menu_send_lengthInt(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_LENGTH_INT, 0, pfStrLengthInt[language], empty, lengthInt, 1, 99);
}

static bool preflight_menu_send_lengthDec(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_LENGTH_DEC, 0, pfStrLengthDec[language], empty, lengthDec, 0, 99);
}

static bool preflight_menu_send_widthInt(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_WIDTH_INT, 0, pfStrWidthInt[language], empty, widthInt, 1, 99);
}

static bool preflight_menu_send_widthDec(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_WIDTH_DEC, 0, pfStrWidthDec[language], empty, widthDec, 0, 99);
}

static bool preflight_menu_send_heightInt(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_HEIGHT_INT, 0, pfStrHeightInt[language], empty, heightInt, 1, 99);
}

static bool preflight_menu_send_heightDec(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_HEIGHT_DEC, 0, pfStrHeightDec[language], empty, heightDec, 0, 99);
}

static bool preflight_menu_send_weight(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_WEIGHT, 0, pfStrWeight[language], empty, weight, 1, 99);
}

static bool preflight_menu_send_weightDec(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_textnumeric(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_WEIGHT_DEC, 0, pfStrWeightDec[language], empty, weightDec, 0, 99);
}

static bool preflight_menu_send_fuelType(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrFuelType[language];
    const char *col1_str;
    if (fuelType == FUEL_TYPE_DIESEL) {
        col1_str = pfStrFuelDiesel[language];
    } else {
        col1_str = pfStrFuelGasoline[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_FUEL_TYPE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, col0_str, col1_str);
}

static bool preflight_menu_send_ramps(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrRamps[language];
    const char *col1_str;
    if (ramps) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_RAMPS, 0, col0_str, col1_str, ramps);
}

static bool preflight_menu_send_shower(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrShower[language];
    const char *col1_str;
    if (shower) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_SHOWER, 0, col0_str, col1_str, shower);
}

static bool preflight_menu_send_windows(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrWindows[language];
    const char *col1_str;
    if (windows) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_WINDOWS, 0, col0_str, col1_str, windows);
}

static bool preflight_menu_send_gas(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrGas[language];
    const char *col1_str;
    if (gas) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_GAS, 0, col0_str, col1_str, gas);
}

static bool preflight_menu_send_jacks(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrJacks[language];
    const char *col1_str;
    if (jacks) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_JACKS, 0, col0_str, col1_str, jacks);
}

static bool preflight_menu_send_storage(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrStorage[language];
    const char *col1_str;
    if (storage) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_STORAGE, 0, col0_str, col1_str, storage);
}

static bool preflight_menu_send_mains(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrMains[language];
    const char *col1_str;
    if (mains) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_MAINS, 0, col0_str, col1_str, mains);
}

static bool preflight_menu_send_awning(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrAwning[language];
    const char *col1_str;
    if (awning) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_AWNING, 0, col0_str, col1_str, awning);
}

static bool preflight_menu_send_antenna(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrAntenna[language];
    const char *col1_str;
    if (antenna) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_ANTENNA, 0, col0_str, col1_str, antenna);
}

static bool preflight_menu_send_cabinets(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrCabinets[language];
    const char *col1_str;
    if (cabinets) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_CABINETS, 0, col0_str, col1_str, cabinets);
}

static bool preflight_menu_send_drawers(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrDrawers[language];
    const char *col1_str;
    if (drawers) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_DRAWERS, 0, col0_str, col1_str, drawers);
}

static bool preflight_menu_send_objects(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrObjects[language];
    const char *col1_str;
    if (objects) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_OBJECTS, 0, col0_str, col1_str, objects);
}

static bool preflight_menu_send_table(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *col0_str = pfStrTable[language];
    const char *col1_str;
    if (table) {
        col1_str = on_str[language];
    } else {
        col1_str = off_str[language];
    }
    return message_30_transmit_row_checkbox(&msg->as_30, seq_num_send(), row_num, PREFLIGHT_MENU_ROWS_TABLE, 0, col0_str, col1_str, table);
}

void alpine_preflight_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, preflight_menu_send_header);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_TIME, preflight_menu_send_time);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_LENGTH_INT, preflight_menu_send_lengthInt);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_LENGTH_DEC, preflight_menu_send_lengthDec);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_WIDTH_INT, preflight_menu_send_widthInt);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_WIDTH_DEC, preflight_menu_send_widthDec);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_HEIGHT_INT, preflight_menu_send_heightInt);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_HEIGHT_DEC, preflight_menu_send_heightDec);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_WEIGHT, preflight_menu_send_weight);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_WEIGHT_DEC, preflight_menu_send_weightDec);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_FUEL_TYPE, preflight_menu_send_fuelType);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_RAMPS, preflight_menu_send_ramps);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_SHOWER, preflight_menu_send_shower);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_WINDOWS, preflight_menu_send_windows);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_GAS, preflight_menu_send_gas);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_JACKS, preflight_menu_send_jacks);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_STORAGE, preflight_menu_send_storage);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_MAINS, preflight_menu_send_mains);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_AWNING, preflight_menu_send_awning);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_ANTENNA, preflight_menu_send_antenna);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_CABINETS, preflight_menu_send_cabinets);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_DRAWERS, preflight_menu_send_drawers);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_OBJECTS, preflight_menu_send_objects);
    menu_manager_enable_bit_func(mngr, PREFLIGHT_MENU_ROWS_TABLE, preflight_menu_send_table);
}

static inline void handle_preflight_value_received(
    const uint8_t row_pressed, const uint8_t menu_index) {
#if __amd64__
    LOG_DEF_NORMAL("%d %s %d %d\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__,
                   row_pressed, menu_index);
#else
    LOG_DEF_NORMAL("%ld %s %d %d\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__, row_pressed, menu_index);
#endif

    switch (edited_row) {
        case PREFLIGHT_MENU_ROWS_TIME:
            pfTime = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_0, pfTime);
            break;
        case PREFLIGHT_MENU_ROWS_LENGTH_INT:
            lengthInt = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_1, lengthInt);
            break;
        case PREFLIGHT_MENU_ROWS_LENGTH_DEC:
            lengthDec = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_2, lengthDec);
            break;
        case PREFLIGHT_MENU_ROWS_WIDTH_INT:
            widthInt = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_3, widthInt);
            break;
        case PREFLIGHT_MENU_ROWS_WIDTH_DEC:
            widthDec = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_4, widthDec);
            break;
        case PREFLIGHT_MENU_ROWS_HEIGHT_INT:
            heightInt = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_5, heightInt);
            break;
        case PREFLIGHT_MENU_ROWS_HEIGHT_DEC:
            heightDec = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_6, heightDec);
            break;
        case PREFLIGHT_MENU_ROWS_WEIGHT:
            weight = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_7, weight);
            break;
        case PREFLIGHT_MENU_ROWS_WEIGHT_DEC:
            weightDec = row_pressed;
            settings_write(PREFLIGHT_SETTING_BYTE_7, weightDec);
            break;
        default:
            LOG_DEF_NORMAL("Row %d not handled\r\n", edited_row);
            break;
    }
    LOG_DEF_NORMAL("%s year: %d vehicle_month: %d vehicle_day: %d hour: %d minute: %d\r\n",
                   __PRETTY_FUNCTION__, vehicle_year, vehicle_month, vehicle_day, vehicle_hour, vehicle_minute);
    vehicle_send_changed_clock_date(vehicle_year, vehicle_month, vehicle_day, vehicle_hour, vehicle_minute);

    ///e quindi richiedo la ritrasmissione del menu.
    enable_radio_menu_refresh(alpine_preflight_menu_manager);
    menu_walking_push(INTERFACE_PREFLIGHT_MENU);
}

static inline void handle_preflight_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    (void)menu_index;
#if __amd64__
    LOG_DEF_NORMAL("%d %s %02X %02X %02X\r\n", xTaskGetTickCount(),
                   __PRETTY_FUNCTION__, row_pressed, menu_index, menu_data);
#else
    LOG_DEF_NORMAL("%ld %s %02X %02X %02X\r\n", xTaskGetTickCount(), __PRETTY_FUNCTION__, row_pressed, menu_index, menu_data);
#endif
    if (row_pressed == INTERFACE_PREFLIGHT_MENU) {
        /// Una volta verificato che il menu visualizzato sulla radio e' quello corretto...
        /// leggo il dato della riga premuta ed imposto l'opzione corretta.
        switch (menu_data) {
            case PREFLIGHT_MENU_ROWS_FUEL_TYPE:
                fuelType = !fuelType;
                settings_write(PREFLIGHT_SETTING_BYTE_9, fuelType);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_RAMPS:
                ramps = !ramps;
                if (ramps) {
                    flags0 |= (1 << 0);
                } else {
                    flags0 &= ~(1 << 0);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_SHOWER:
                shower = !shower;
                if (shower) {
                    flags0 |= (1 << 1);
                } else {
                    flags0 &= ~(1 << 1);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_WINDOWS:
                windows = !windows;
                if (windows) {
                    flags0 |= (1 << 2);
                } else {
                    flags0 &= ~(1 << 2);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_GAS:
                gas = !gas;
                if (gas) {
                    flags0 |= (1 << 3);
                } else {
                    flags0 &= ~(1 << 3);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_JACKS:
                jacks = !jacks;
                if (jacks) {
                    flags0 |= (1 << 4);
                } else {
                    flags0 &= ~(1 << 4);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_STORAGE:
                storage = !storage;
                if (storage) {
                    flags0 |= (1 << 5);
                } else {
                    flags0 &= ~(1 << 5);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_MAINS:
                mains = !mains;
                if (mains) {
                    flags0 |= (1 << 6);
                } else {
                    flags0 &= ~(1 << 6);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_AWNING:
                awning = !awning;
                if (awning) {
                    flags0 |= (1 << 7);
                } else {
                    flags0 &= ~(1 << 7);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_ANTENNA:
                antenna = !antenna;
                if (antenna) {
                    flags1 |= (1 << 0);
                } else {
                    flags1 &= ~(1 << 0);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_CABINETS:
                cabinets = !cabinets;
                if (cabinets) {
                    flags1 |= (1 << 1);
                } else {
                    flags1 &= ~(1 << 1);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_DRAWERS:
                drawers = !drawers;
                if (drawers) {
                    flags1 |= (1 << 2);
                } else {
                    flags1 &= ~(1 << 2);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_OBJECTS:
                objects = !objects;
                if (objects) {
                    flags1 |= (1 << 3);
                } else {
                    flags1 &= ~(1 << 3);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            case PREFLIGHT_MENU_ROWS_TABLE:
                table = !table;
                if (table) {
                    flags1 |= (1 << 4);
                } else {
                    flags1 &= ~(1 << 4);
                }
                settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                enable_radio_menu_refresh(alpine_preflight_menu_manager);
                menu_walking_push(INTERFACE_PREFLIGHT_MENU);
                break;
            default:
                if (menu_data != edited_row) {
                    LOG_DEF_NORMAL("Cambio riga attiva\r\n");
                    edited_row = menu_data;
                }
                break;
        }
    }
}

uint8_t preflight_get_time(void) {
    return pfTime;
}

void display_preflight_check_message(void) {
    struct EVENT_WARNING_MESSAGE message;
    message.generic.event = eEVENT_WARNING_MESSAGE;
    message.chime = 0;
    message.seconds = preflight_get_time();
    message.ptr_head = preflight_get_header();
    char *text = preflight_get_text();
    uint8_t max_body_length = 243 - strlen(message.ptr_head);
    if (strlen(text) > max_body_length) {
        for (uint8_t i = 1; i < 4; i++) {
            text[max_body_length - i] = '.';
        }
    }
    text[max_body_length] = '\0';
    message.ptr_body = text;

    event_warning_message_emit(&message);
    //    engine_running = true;
}

#define TITLE_LENGTH 100  //numero di caratteri massimo visibili sullo schermo.
#define BODY_LENGTH 243
static char preflightStringTitle[TITLE_LENGTH];
static char preflightStringMsg[BODY_LENGTH];
static char tempString[10];

char *preflight_get_header(void) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    preflightStringTitle[0] = 0;
    strncat(preflightStringTitle, pfHeader[language], TITLE_LENGTH - (strlen(preflightStringTitle)+1));
    tempString[0] = 0;
    sprintf(tempString, "%d", lengthInt);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, ",", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", lengthDec);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, "m x ", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", widthInt);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, ",", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", widthDec);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, "m x ", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", heightInt);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, ",", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", heightDec);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, "m; ", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", weight);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, ",", TITLE_LENGTH - strlen(preflightStringTitle));
    tempString[0] = 0;
    sprintf(tempString, "%d", weightDec);
    strncat(preflightStringTitle, tempString, TITLE_LENGTH - strlen(preflightStringTitle));
    strncat(preflightStringTitle, "t; ", TITLE_LENGTH - strlen(preflightStringTitle));
    if (fuelType == FUEL_TYPE_DIESEL) {
        strncat(preflightStringTitle, pfStrFuelDiesel[language], TITLE_LENGTH - strlen(preflightStringTitle));
    } else {
        strncat(preflightStringTitle, pfStrFuelGasoline[language], TITLE_LENGTH - strlen(preflightStringTitle));
    }
    return preflightStringTitle;
}

char *preflight_get_text(void) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t dotPos;
    preflightStringMsg[0] = 0;
    if (shower || windows || gas || storage || awning || antenna || cabinets || drawers || objects || table) {
        strncat(preflightStringMsg, pfText1[language], BODY_LENGTH - (strlen(preflightStringMsg) + 1));

        if (shower) {
            strncat(preflightStringMsg, pfStrShower[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (windows) {
            strncat(preflightStringMsg, pfStrWindows[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (gas) {
            strncat(preflightStringMsg, pfStrGas[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (storage) {
            strncat(preflightStringMsg, pfStrStorage[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (awning) {
            strncat(preflightStringMsg, pfStrAwning[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (antenna) {
            strncat(preflightStringMsg, pfStrAntenna[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (cabinets) {
            strncat(preflightStringMsg, pfStrCabinets[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (drawers) {
            strncat(preflightStringMsg, pfStrDrawers[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (objects) {
            strncat(preflightStringMsg, pfStrObjects[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (table) {
            strncat(preflightStringMsg, pfStrTable[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }

        dotPos = strlen(preflightStringMsg) - 2;
        preflightStringMsg[dotPos] = '.';
        preflightStringMsg[dotPos + 1] = 0;
        strncat(preflightStringMsg, " ", BODY_LENGTH - strlen(preflightStringMsg));
    }
    if (ramps || jacks || mains) {
        strncat(preflightStringMsg, pfText2[language], BODY_LENGTH - strlen(preflightStringMsg));
        if (ramps) {
            strncat(preflightStringMsg, pfStrRamps[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (jacks) {
            strncat(preflightStringMsg, pfStrJacks[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        if (mains) {
            strncat(preflightStringMsg, pfStrMains[language], BODY_LENGTH - strlen(preflightStringMsg));
            strncat(preflightStringMsg, ", ", BODY_LENGTH - strlen(preflightStringMsg));
        }
        dotPos = strlen(preflightStringMsg) - 2;
        preflightStringMsg[dotPos] = '.';
        preflightStringMsg[dotPos + 1] = 0;
    }
    return preflightStringMsg;
}

/*********************************FINE GESTIONE PREFLIGHT***********************************************/

/**
 * @brief This is just a placeholder. I reach this point. The vehicle must provide it's own menu.
 * @param row_pressed not used.
 * @param menu_index not used.
 * @param menu_data not used.
 */
static inline void handle_interface_menu(const uint8_t row_pressed,
                                         const uint8_t menu_index, const uint8_t menu_data) {
    (void)menu_index;
    if (row_pressed == GENERAL_MENU_VERS) {
        /// Una volta verificato che il menu visualizzato sulla radio e' quello corretto...
        /// leggo il dato della riga premuta ed imposto l'opzione corretta.
        switch (menu_data) {
            case INTERFACE_MENU_CLOCK:
                enable_radio_menu_refresh(alpine_menu_datetime_manager);
                menu_walking_push(INTERFACE_MENU_CLOCK);
                break;
            default:
                enable_radio_menu_refresh(alpine_menu_manager);
                menu_walking_push(GENERAL_MENU_VERS);
                break;
        }
    } else {
        enable_radio_menu_refresh(alpine_menu_manager);
        menu_walking_push(GENERAL_MENU_VERS);
    }
}

/**
 * @brief inietta nel sistema ad eventi la richiesta di trasmissione del main menu.
 * @param row_pressed numero del menu visualizzato sulla radio.
 * @param menu_index non usato.
 * @param menu_data numero di riga premuto sulla radio.
 */
static inline void handle_main_menu(const uint8_t row_pressed,
                                    const uint8_t menu_index, const uint8_t menu_data) {
    if (!(row_pressed == 00 && menu_index == 00 && menu_data == 01)) {
        if (menu_data < INTERFACE_MENU_VERS) {
            switch (menu_data) {
                case INTERFACE_MENU_MAIN:
                case INTERFACE_MENU_MAIN2:
                    enable_radio_menu_refresh(alpine_main_menu_manager);
                    menu_walking_push(0x00);
                    break;
                case GENERAL_MENU_VERS:
                    enable_radio_menu_refresh(alpine_menu_manager);
                    menu_walking_push(0x02);
                    break;
                case INTERFACE_PREFLIGHT_MENU:
                    enable_radio_menu_refresh(alpine_preflight_menu_manager);
                    menu_walking_push(0x03);
                    break;
            }
        } else {
            if (menu_data < INTERFACE_VEHICLE_MENU) {
                handle_settings_submenu(row_pressed, menu_index, menu_data);
            }
        }
    } else {
        enable_radio_menu_refresh(alpine_main_menu_manager);
        menu_walking_push(0x00);
    }
}

/**
 * @brief inietta nel sistema ad eventi la richiesta di trasmissione del menu generale interfaccia.
 * @param msg il messaggio di richiesta ricevuta dal sistema ad eventi.
 */
static inline void handle_general_settings(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        ///Ricevuta una richiesta di trasmissione di menu
        switch (msg->as_menu_request.msg_type) {
            case MENU_REQUEST_NUM_91:
                if (msg->as_menu_request.request.as_91.row_pressed_num == 0x00) {
                    ///Enters the main menu. We have to display it.
                    menu_walking_reset();
                    menu_walking_push(0x00);
                    enable_radio_menu_refresh(alpine_main_menu_manager);
                }
                break;
            case MENU_REQUEST_NUM_E5: {
                const uint8_t cur_menu = menu_walking_current();
                if (cur_menu < INTERFACE_MENU_VERS) {
                    switch (menu_walking_current()) {
                        case INTERFACE_MENU_MAIN:
                        case INTERFACE_MENU_MAIN2:
                            handle_main_menu(
                                msg->as_menu_request.request.as_e5.row_pressed_num,
                                msg->as_menu_request.request.as_e5.menu_index,
                                msg->as_menu_request.request.as_e5.menu_data);
                            break;
                        case INTERFACE_MENU_CLOCK:
                            handle_clock_date_menu(msg->as_menu_request.request.as_e5.row_pressed_num,
                                                   msg->as_menu_request.request.as_e5.menu_index,
                                                   msg->as_menu_request.request.as_e5.menu_data);
                            break;
                        case INTERFACE_PREFLIGHT_MENU:
                            handle_preflight_menu(msg->as_menu_request.request.as_e5.row_pressed_num,
                                                  msg->as_menu_request.request.as_e5.menu_index,
                                                  msg->as_menu_request.request.as_e5.menu_data);
                            break;
                        case GENERAL_MENU_VERS:
                            handle_interface_menu(
                                msg->as_menu_request.request.as_e5.row_pressed_num,
                                msg->as_menu_request.request.as_e5.menu_index,
                                msg->as_menu_request.request.as_e5.menu_data);
                            break;
                        default:
                            LOG_DEF_NORMAL("Unknown menu %02X %02X %02X\r\n",
                                           msg->as_menu_request.request.as_e5.row_pressed_num,
                                           msg->as_menu_request.request.as_e5.menu_index,
                                           msg->as_menu_request.request.as_e5.menu_data);
                    }
                } else if (cur_menu < INTERFACE_VEHICLE_MENU) {
                    if (msg->as_menu_request.request.as_e5.menu_data < INTERFACE_VEHICLE_MENU) {
                        handle_settings_submenu(
                            msg->as_menu_request.request.as_e5.row_pressed_num,
                            msg->as_menu_request.request.as_e5.menu_index,
                            msg->as_menu_request.request.as_e5.menu_data);
                    }
                }
            } break;
            default:
                break;
        }
    }
}

static inline void handle_menu_data_selection(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        ///Ricevuta una richiesta di trasmissione di menu
        if (msg->as_menu_request.msg_type == MENU_REQUEST_NUM_E8) {
            uint8_t cur_menu = menu_walking_current();
            if (cur_menu == INTERFACE_MENU_CLOCK) {
                handle_clock_date_value_received(
                    msg->as_menu_request.request.as_e8.menu_index,
                    msg->as_menu_request.request.as_e8.row_pressed_num);
            } else if ((cur_menu >= INTERFACE_MENU_VERS) && (cur_menu < INTERFACE_VEHICLE_MENU)) {
                handle_settings_value_received(
                    msg->as_menu_request.request.as_e8.menu_index,
                    msg->as_menu_request.request.as_e8.row_pressed_num);
            } else if (cur_menu == INTERFACE_PREFLIGHT_MENU) {
                handle_preflight_value_received(
                    msg->as_menu_request.request.as_e8.menu_index,
                    msg->as_menu_request.request.as_e8.row_pressed_num);
            }
        }
    }
}

#define MAX_CAN 3
void record_can_activity(const union EVENT *const msg) {
    uint32_t initial = can_activity;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (can_activity < MAX_CAN) {
            can_activity = MAX_CAN;
        }
    }
    if (msg->as_generic.event == eEVENT_CAN1_RX_MESSAGE) {
        if (can_activity < MAX_CAN) {
            can_activity = MAX_CAN;
        }
    }
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (can_activity > 0) {
            can_activity--;
        }
    }
    bool a = (initial != 0);
    bool b = (can_activity != 0);
    if (a != b) {
        redraw_last_menu();
    }
}

#define MAX_LIN 3
void record_lin_activity(const union EVENT *const msg) {
    uint32_t initial = lin_activity;
    if (msg->as_generic.event == eEVENT_LIN_RX_MESSAGE) {
        if (lin_activity < MAX_LIN) {
            lin_activity = MAX_LIN;
        }
    }
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (lin_activity > 0) {
            lin_activity--;
        }
    }
    bool a = (initial != 0);
    bool b = (lin_activity != 0);
    if (a != b) {
        redraw_last_menu();
    }
}

void handle_led_color_setting(const union EVENT *const msg) {
    (void)msg;
    if (controller_get_led_color() >= LED_COLOR_COUNT) {
        struct CONTROLLER_DATA ctrl;
        ctrl.command = CONTROLLER_RW_ACTIVE_CONF;
        ctrl.data[0] = vehicle_get_default_led_color();
        event_controller_emit(&ctrl);
    }
}

void alpine_menu_disable_12_24_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_HOUR_FORMAT);
}

void alpine_menu_disable_hour_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_HOUR);
}

void alpine_menu_disable_minutes_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_MINUTE);
}

void alpine_menu_disable_day_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_DAYS);
}

void alpine_menu_disable_month_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_MONTHS);
}

void alpine_menu_disable_year_setup(void) {
    menu_manager_disable_bit(alpine_menu_datetime_manager, DATETIME_MENU_ROWS_YEARS);
}

void alpine_menu_datetime_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, datetime_menu_send_header);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_ENABLE, datetime_menu_send_clock_string);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_YEARS, datetime_menu_send_year);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_MONTHS, datetime_menu_send_month);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_DAYS, datetime_menu_send_day);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_HOUR, datetime_menu_send_hour);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_MINUTE, datetime_menu_send_minute);
    menu_manager_enable_bit_func(mngr, DATETIME_MENU_ROWS_HOUR_FORMAT, datetime_menu_send_format);
}

void alpine_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, version_menu_send_header);
    menu_manager_enable_bit_func(mngr, VERSION_MENU_ROWS_VERSION, version_menu_send_general_settings);
    menu_manager_enable_bit_func(mngr, VERSION_MENU_ROWS_IF_STATUS, version_menu_send_interface_status);
    menu_manager_enable_bit_func(mngr, VERSION_MENU_ROWS_CAN, version_menu_send_can_status);
    menu_manager_enable_bit_func(mngr, VERSION_MENU_ROWS_LIN_STATUS, version_menu_send_lin_status);
    menu_manager_enable_bit_func(mngr, VERSION_MENU_ROWS_DATETIME, version_menu_send_datetime_menu);
    if (clock_management_disable) {
        menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_DATETIME);
    }
    if (lin_management_disable) {
        menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_LIN_STATUS);
    }
    if (can_management_disable) {
        menu_manager_clear_active_bit(alpine_menu_manager, VERSION_MENU_ROWS_CAN);
    }
}

void alpine_main_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, main_menu_send_header);
    menu_manager_enable_bit_func(mngr, MAIN_MENU_ROWS_GENERAL, main_menu_send_general_settings);
    menu_manager_enable_bit_func(mngr, MAIN_MENU_ROWS_INTERFACE, main_menu_send_interface_settings);
    menu_manager_enable_bit_func(mngr, MAIN_MENU_ROWS_VEHICLE, main_menu_send_vehicle_settings);
    menu_manager_enable_bit_func(mngr, MAIN_MENU_ROWS_PREFLIGHT, main_menu_send_preflight_settings);

    if (vehicle_settings_disable) {
        menu_manager_clear_active_bit(alpine_main_menu_manager, MAIN_MENU_ROWS_VEHICLE);
    }
}

static bool read_config = false;
static void handle_config_setup(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SETTINGS_READ) {
        if (msg->as_settings_read.type == SETTING_READ_END) {
            if (read_config == false) {
                read_config = true;
                pfTime = settings_read(PREFLIGHT_SETTING_BYTE_0);
                if ((pfTime < 1) || (pfTime > 99)) {
                    pfTime = 15;
                    settings_write(PREFLIGHT_SETTING_BYTE_0, pfTime);
                }
                lengthInt = settings_read(PREFLIGHT_SETTING_BYTE_1);
                if ((lengthInt < 1) || (lengthInt > 99)) {
                    lengthInt = 6;
                    settings_write(PREFLIGHT_SETTING_BYTE_1, lengthInt);
                }
                lengthDec = settings_read(PREFLIGHT_SETTING_BYTE_2);
                if (/*(lengthDec < 1) || */ (lengthDec > 99)) {
                    lengthDec = 50;
                    settings_write(PREFLIGHT_SETTING_BYTE_2, lengthDec);
                }
                widthInt = settings_read(PREFLIGHT_SETTING_BYTE_3);
                if ((widthInt < 1) || (widthInt > 99)) {
                    widthInt = 2;
                    settings_write(PREFLIGHT_SETTING_BYTE_3, widthInt);
                }
                widthDec = settings_read(PREFLIGHT_SETTING_BYTE_4);
                if (/*(widthDec < 1) || */ (widthDec > 99)) {
                    widthDec = 25;
                    settings_write(PREFLIGHT_SETTING_BYTE_4, widthDec);
                }
                heightInt = settings_read(PREFLIGHT_SETTING_BYTE_5);
                if ((heightInt < 1) || (heightInt > 99)) {
                    heightInt = 3;
                    settings_write(PREFLIGHT_SETTING_BYTE_5, heightInt);
                }
                heightDec = settings_read(PREFLIGHT_SETTING_BYTE_6);
                if (/*(heightDec < 1) || */ (heightDec > 99)) {
                    heightDec = 0;
                    settings_write(PREFLIGHT_SETTING_BYTE_6, heightDec);
                }
                weight = settings_read(PREFLIGHT_SETTING_BYTE_7);
                if ((weight < 1) || (weight > 99)) {
                    weight = 3;
                    settings_write(PREFLIGHT_SETTING_BYTE_7, weight);
                }
                weightDec = settings_read(PREFLIGHT_SETTING_BYTE_8);
                if (/*(weightDec < 1) || */ (weightDec > 99)) {
                    weightDec = 49;
                    settings_write(PREFLIGHT_SETTING_BYTE_8, weightDec);
                }

                fuelType = settings_read(PREFLIGHT_SETTING_BYTE_9);
                if ((fuelType < FUEL_TYPE_DIESEL) || (fuelType > FUEL_TYPE_GASOLINE)) {
                    fuelType = FUEL_TYPE_DIESEL;
                    settings_write(PREFLIGHT_SETTING_BYTE_9, fuelType);
                }
                flags0 = settings_read(PREFLIGHT_SETTING_FLAGS_0);
                flags1 = settings_read(PREFLIGHT_SETTING_FLAGS_1);
                if ((flags0 == 0xFF) && (flags1 == 0xFF)) {
                    flags0 = 0;
                    flags1 = 0;
                    settings_write(PREFLIGHT_SETTING_FLAGS_0, flags0);
                    settings_write(PREFLIGHT_SETTING_FLAGS_1, flags1);
                }
                ramps = (bool)(flags0 & (1 << 0));
                shower = (bool)(flags0 & (1 << 1));
                windows = (bool)(flags0 & (1 << 2));
                gas = (bool)(flags0 & (1 << 3));
                jacks = (bool)(flags0 & (1 << 4));
                storage = (bool)(flags0 & (1 << 5));
                mains = (bool)(flags0 & (1 << 6));
                awning = (bool)(flags0 & (1 << 7));
                antenna = (bool)(flags1 & (1 << 0));
                cabinets = (bool)(flags1 & (1 << 1));
                drawers = (bool)(flags1 & (1 << 2));
                objects = (bool)(flags1 & (1 << 3));
                table = (bool)(flags1 & (1 << 4));
            }
        }
    }
}

/**
 * @brief inizializza il sistema di gestione dei menu.
 */
void menu_send_init(void) {
    event_connect_callback(eEVENT_MENU_REQUEST, handle_general_settings);
    event_connect_callback(eEVENT_MENU_REQUEST, handle_menu_data_selection);
    event_connect_callback(eEVENT_VEHICLE_TIME_CHANGED, handle_vehicle_time);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, record_can_activity);
    event_connect_callback(eEVENT_CAN1_RX_MESSAGE, record_can_activity);
    event_connect_callback(eEVENT_SECONDS_CHANGED, record_can_activity);
    event_connect_callback(eEVENT_LIN_RX_MESSAGE, record_lin_activity);
    event_connect_callback(eEVENT_SECONDS_CHANGED, record_lin_activity);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_led_color_setting);
    event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);

    alpine_main_menu_manager = menu_manager_create();
    alpine_main_menu_fill_data(alpine_main_menu_manager);
    alpine_menu_manager = menu_manager_create();
    alpine_menu_fill_data(alpine_menu_manager);
    alpine_menu_datetime_manager = menu_manager_create();
    alpine_menu_datetime_fill_data(alpine_menu_datetime_manager);
    alpine_settings_menu_manager = menu_manager_create();
    alpine_settings_menu_fill_data(alpine_settings_menu_manager);
    alpine_preflight_menu_manager = menu_manager_create();
    alpine_preflight_menu_fill_data(alpine_preflight_menu_manager);

    menu_walking_init();
    menu_refresher_init();
}

/**
 * @}
 */
