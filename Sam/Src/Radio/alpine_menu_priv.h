#pragma once

#include <stdint.h>
#include "alpine_menu.h"

enum ALPINE_MENU_ITEM {
    ALPINE_MENU_ITEM_COUNT
};

enum ALPINE_MENU_SETTING_ITEM {
    ALPINE_MENU_SETTING_ITEM_COUNT
};

void handle_settings_submenu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data);
void handle_settings_value_received(const uint8_t row_pressed, const uint8_t menu_index);
void alpine_settings_menu_fill_data(menu_manager_t mngr);