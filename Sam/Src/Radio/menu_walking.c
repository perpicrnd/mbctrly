#include "menu_walking.h"
#include "logger.h"

#define SUBMENU_WIDTH 10

static int16_t current = 0;
static uint8_t menu_list[SUBMENU_WIDTH] = {0xFF};

void menu_walking_push(uint8_t value){
	if ((menu_list[current-1] != value) || (current == 0)){
		menu_list[current] = value;
		if (current < SUBMENU_WIDTH){
			current++;
		}else{
			LOG_DEF_ERROR("MENU WALKING REACHED MAXIMUM WIDTH\r\n");
		}
	}
}

void menu_walking_pop(){
	if (current > 0){
		current--;
	}
	menu_list[current] = 0xFF;
}

uint8_t menu_walking_current(){
	if (current > 0){
		return menu_list[current-1];
	}else{
		return 0xFF;
	}
}

void menu_walking_reset(void){
	for(uint8_t i=0; i<SUBMENU_WIDTH; i++){
		menu_list[i] = 0xFF;
	}
	current = 0;
}

void menu_walking_init(void){
	menu_walking_reset();
}
