#ifndef RADIO_TASK_H
#define RADIO_TASK_H
#include "FreeRTOS.h"
#include "semphr.h"
#include "timers.h"

#include "alpine_menu.h"
#include "alpine_task.h"
#include "buzzer3.h"
#include "event_list.h"
#include "ignition.h"
#include "logger.h"
#include "menu_manager.h"
#include "radio_lowlevel_task.h"
#include "radio_message.h"
#include "vehicle.h"
#include "watchdog.h"
#include "wisker.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Number of the retransmissions of the message.
 * This is used in case the Radio does not transmit back an ACK.
 */
static uint8_t repetition = 0;

/**
 * @brief Contains the sequence number to be sent to the radio.
 * The sequence number is used to order the messages and keep track of message affinity.
 */
static uint8_t seq_num_send_pvt = 0;

/**
 */
uint8_t seq_num_send(void) {
    ///Il sequence number è un numero che varia da 0x01 a 0x1F.
    if (seq_num_send_pvt > 0x1F) {
        seq_num_send_pvt = 1;
    }
    return seq_num_send_pvt;
}

/**
 * @brief Set the current sequence number.
 * @param num the new sequence number.
 */
static inline void seq_num_send_set(uint8_t num) {
    seq_num_send_pvt = num;
}

/**
 * Gives the new sequence number and increase the counter. This function is used to get the new sequence number
 * that needs to be used during a transmission. If the message to be sent is a follower use the \ref seq_num_send.
 *
 */
uint8_t seq_num_send_next(void) {
    uint8_t tmp = seq_num_send();
    tmp++;
    if (tmp > 0x1F) {
        tmp = 1;
    }
    seq_num_send_set(tmp);
    return tmp;
}

/**
 * @brief flag that indicates if the 500msec timer is expired.
 */
static bool timer_500_msec_elapsed = false;

/**
 * @brief Timer that executes every 500msec.
 * @param pxTimer not used.
 */
static void timer_msec500(TimerHandle_t pxTimer) {
    (void)pxTimer;
    timer_500_msec_elapsed = true;
}

/**
 * @brief flag that indicates if the 200msec timer is expired.
 */
static bool timer_200_msec_elapsed = false;
/**
 * @brief TImer that executes every 200msec.
 * @param pxTimer not used.
 */
static void timer_msec200(TimerHandle_t pxTimer) {
    (void)pxTimer;
    timer_200_msec_elapsed = true;
}

/**
 * @brief timer handle
 */
static TimerHandle_t msec500;
/**
 * @brief timer handle
 */
static TimerHandle_t msec200;

/**
 * @brief Buffer of a transmitted message.
 * Messages are not kept in RAM all the time. Only a message buffer is used for transmitting every message.
 * From a transmission to the reception of the relative ACK no more transmitted messages.
 */
static union ALPINE_MESSAGE sending_msg_buffer;
/**
 * @brief Semaphore handle that notify the reception of a ACK/NACK.
 */
static SemaphoreHandle_t acknack_sem;

/**
 * @brief Flag used for incrementining the menu row to send.
 */
static bool sending_menu = false;
/**
 * @brief Menu row to send to the radio.
 *
 * Every menus has an undefined number of rows. This flag keeps track of the next requested  row number.
 * If the menu does not have this row number (they are less) nothing is sent and the \ref sending_menu variable is resetted.
 */
static uint8_t menu_row_num;
/**
 * @brief Fifo for transmitting SWC commands.
 */
static QueueHandle_t swc_queue;
/**
 * @brief Fifo for transmitting Menu to radio.
 */
static QueueHandle_t menu_tx_queue;
/**
 * @brief Fifo for transmitting radio requests.
 */
static QueueHandle_t requests_queue;

/**
 * @brief Fifo for transmitting parking sensors information.
 */
static QueueHandle_t parksensor_queue;

static QueueHandle_t warning_msg_queue = NULL;

static QueueHandle_t external_request_queue = NULL;

/**
 * @brief Fifo for transmitting HVAC / Clima information.
 * 
 */
static QueueHandle_t hvac_queue;
/**
 * @brief Radio source type. Utilizzato per definire se resettare il watchdog automaticamente.
 */
static enum RADIO_SOURCE_TYPE radio_source_type = RADIO_SOURCE_OFF;
/**
 * @brief Keeps the last ack/nack event.
 */
static union EVENT ack_value;

void handle_acknack(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_ACKNACK_CHANGED) {
        memcpy(&ack_value, msg, sizeof(union EVENT));
    }
    xSemaphoreGive(acknack_sem);
}
/**
 * @brief waits for an ACK or a NACK after the transmission of a message.
 *
 * This function handle the reception of the ACK and the NACK or if the message does not receive any reply.
 * If the message transmitted is related to a multiframe message the message counter will be increased here.
 * @return TRUE if a new transmission has beed made, false otherwise.
 */
static bool wait_for_acknack(void) {
    bool retVal = false;
    xSemaphoreTake(acknack_sem, 200);
    if (ack_value.as_acknack.acknack == EVENT_ACKNACK_NACK) {
        /// If Nack received
        LOG_UART_WARNING("NACK\t\n");
        if (repetition < 3) {
            /// less than 3 repetition
            LOG_UART_INFO("repeat tx %d\t\n", repetition);
            if (message_raw_seq_number(&sending_msg_buffer.as_raw) == (ack_value.as_acknack.seq_num)) {
                /// And the nack seq_number is the same of the last transmitted message
                /// try to send again the message.
                message_raw_transmit(&sending_msg_buffer.as_raw);
                retVal = true;  ///Reset the for loop
            }
            ///Increase repetition.
            repetition++;
        } else {
            LOG_UART_ERROR("GIVE UP, 3 NACKS\r\n");
            ///If the number of repetition is too high
            if (sending_menu) {
                ///and the transmitted message is a menu then reset the menu position.
                sending_menu = false;
                menu_row_num = 0;
            }
        }
    } else if (ack_value.as_acknack.acknack == EVENT_ACKNACK_TIMEOUT) {
        /// If Message was timed out
        LOG_UART_WARNING("TIMEOUTt\n");
        if (repetition < 3) {
            ///Try to send it again, we don't have an ack value to check so we send the last message.
            LOG_UART_INFO("repeat tx %d\r\n", repetition);
            message_raw_transmit(&sending_msg_buffer.as_raw);
            retVal = true;  ///Reset the for loop
        } else {
            LOG_UART_ERROR("GIVE UP, 3 TIMEOUT\r\n");
            ///If the number of repetition is too high
            if (sending_menu) {
                ///and the transmitted message is a menu then reset the menu position.
                menu_row_num = 0;
            }
            if (radio_source_type == RADIO_SOURCE_OFF) {
                ///If screen is OFF reset the watchdog. The radio could be shutting down and don't replying to our messages.
                ///This looks like a safe place for this feature. Timer task still running, this task running.
                /// Scheduler should be ok.
                // LOG_DEF_INFO("WDT RESET\r\n");
                watchdog_reset();
                wisker_reset(MODULE_UART_LOWLEVEL);
            }
            if (!is_radio_turned_on()) {
                //Same as before, radio is turned off...
                watchdog_reset();
                wisker_reset(MODULE_UART_LOWLEVEL);
            }
        }
        ///Increase repetition.
        repetition++;
    } else {
        if (sending_menu) {
            ///If the ack was received and we are transmitting the menu, increase the row_number.
            menu_row_num++;
        }
    }
    return retVal;
}

enum KEY_TX_STATUS {
    KEY_TX_STATUS_NONE,
    KEY_TX_STATUS_SINGLE,
    KEY_TX_STATUS_MULTIPLE
};
/**
 * @brief function that handle the transmission of the steering wheel commands.
 *
 * This function is used for sending the steeering wheel commands to the radio.
 * Internally the board keeps track of the steering wheel commands using a PRESS_COMMAND - NO COMMAND communication.
 * The radio needs a PRESS COMMAND - RELEASE COMMAND communication. Thereof a reception of a NO COMMAND needs to be
 * evaluated to:
 * - there was a previous command active?
 * - YES send a release command
 * - NO if a key is enabled send the new key.
 *
 * @return true if a message is transmitted, false otherwise.
 */
static inline __attribute__((always_inline)) enum KEY_TX_STATUS transmit_keys(void) {
    static struct SWC_EV old_key = {SWC_NO_BUTTON_PRESSED, 0};
    enum KEY_TX_STATUS retVal = KEY_TX_STATUS_NONE;
    struct SWC_EV key;

    if (old_key.counter == 0) {
        if (xQueueReceive(swc_queue, &key, 0)) {
            if (key.swc != old_key.swc) {
                bool ret = message_transmit_radio_code(&sending_msg_buffer, seq_num_send_next(), key.swc);
                if (!ret) {
                    LOG_DEF_ERROR("Error sending, pushing back swc %d\r\n", key.swc);
                    xQueueSendToFront(swc_queue, &key, 5);
                    retVal = KEY_TX_STATUS_NONE;
                } else {
                    memcpy(&old_key, &key, sizeof(struct SWC_EV));
                    retVal = KEY_TX_STATUS_SINGLE;
                }
            } else {
                old_key.counter += key.counter;
                retVal = KEY_TX_STATUS_MULTIPLE;
            }
            LOG_DEF_NORMAL("SWC %d retVal %d\r\n", key.swc, retVal);
        }
    } else {
        old_key.counter--;
        LOG_DEF_NORMAL("Retain key %02X for %d times.\r\n", old_key.swc, old_key.counter);
        retVal = KEY_TX_STATUS_MULTIPLE;
    }
    return retVal;
}

/**
 * @brief Send the message10 every ~500ms
 *
 * The message 0x10 needs to be sent every 500ms. The function checks the 500msec timer in polling from within the thread
 * so the message delay time is approximated.
 *
 * @return true if a message was transmitted, false otherwise.
 */
static inline __attribute__((always_inline)) bool transmit_message_10(void) {
    bool retVal = false;

    if (timer_500_msec_elapsed) {
        retVal = message_10_transmit(&sending_msg_buffer.as_10, seq_num_send_next());
        vTaskDelay(10 / portTICK_PERIOD_MS);
        timer_500_msec_elapsed = false;
    }
    return retVal;
}

/**
 * @brief Check if a request message was received by the radio.
 *
 * The radio
 * @return true se ha trasmesso dei dati, false altrimenti
 */
static inline __attribute__((always_inline)) bool check_request_messages(void) {
    bool retVal = false;
    ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC func = NULL;
    if (xQueueReceive(requests_queue, &func, 0)) {
        if (func != NULL) {
            retVal = func(&sending_msg_buffer, seq_num_send_next());
        }
    }
    return retVal;
}

/**
 * @brief Check if a parking sensor information has to be transmitted to the radio.
 *
 * @return true se ha trasmesso dei dati, false altrimenti
 */
static inline __attribute__((always_inline)) bool check_parking_sensor(void) {
    bool retVal = false;
    struct PARKING_SENSOR_DATA parking_sensor_data;
    if (xQueueReceive(parksensor_queue, &parking_sensor_data, 0)) {
        retVal = message_19_transmit(&sending_msg_buffer.as_19, seq_num_send_next(), &parking_sensor_data);
    }
    return retVal;
}

static inline __attribute__((always_inline)) bool check_hvac_info(void) {
    bool retVal = false;
    struct HVAC_INFO hvac_info;
    if (xQueueReceive(hvac_queue, &hvac_info, 0)) {
        retVal = message_1A_transmit(&sending_msg_buffer.as_1A, seq_num_send_next(), &hvac_info);
    }
    return retVal;
}

static inline __attribute__((always_inline)) bool check_ext_req_message(void) {
    bool retVal = false;
    struct EXTERNAL_REQUEST_DATA msg;
    if (xQueueReceive(external_request_queue, &msg, 0)) {
        LOG_DEF_NORMAL("****** Trasmetto il messaggio %02X %02X.\r\n", msg.type, msg.data);
        retVal = message_24_transmit(&sending_msg_buffer.as_24, seq_num_send_next(), msg.type, msg.data);
    }
    return retVal;
}

static menu_manager_t sending_menu_manager = NULL;
/**
 * @brief Check if a menu request was received by the radio.
 * @return true se viene trasmesso qualcosa, false altrimenti
 */
    static menu_manager_t menu = NULL;
static inline __attribute__((always_inline)) bool check_request_menu(void) {
    bool retVal = false;
    if ((sending_menu == true) && (sending_menu_manager != NULL)) {
         LOG_UART_INFO("Continuo menu %p\r\n", menu);
        ALPINE_EVENT_MENU_SEND_FUNC func = menu_manager_iter_next(menu);
        if (func != NULL) {
            retVal = func(&sending_msg_buffer, menu_row_num);
            if (!retVal) {
                sending_menu = false;
                menu_row_num = 0;
                sending_menu_manager = NULL;
            } else {
                sending_menu = true;
            }
        } else {
             LOG_UART_INFO("Fine menu %p\r\n", menu);
            sending_menu = false;
            menu_row_num = 0;
            sending_menu_manager = NULL;
        }
    } else if (xQueueReceive(menu_tx_queue, &menu, 0)) {
        if (menu != NULL) {
            LOG_UART_INFO("Invio menu %p\r\n", menu);
             menu_manager_reload(menu);
            ALPINE_EVENT_MENU_SEND_FUNC func = menu_manager_iter_next(menu);
            if (func != NULL) {
                retVal = func(&sending_msg_buffer, menu_row_num);
                if (!retVal) {
                    sending_menu = false;
                    menu_row_num = 0;
                    sending_menu_manager = NULL;
                } else {
                    sending_menu_manager = menu;
                    sending_menu = true;
                }
            }
        }
    }
    return retVal;
}

static inline __attribute__((always_inline)) bool check_warning_message(void) {
    bool retVal = false;
    struct EVENT_WARNING_MESSAGE msg;
    if (xQueueReceive(warning_msg_queue, &msg, 0)) {
        retVal = message_40_transmit(&sending_msg_buffer.as_40, seq_num_send_next(), msg.chime, msg.seconds, msg.ptr_head, msg.ptr_body);
    }
    return retVal;
}

/**
 * @brief radio_task handle message transmission from IF to radio.
 * The loop is configured for waiting about a received ack and then it schedule the message transmission
 * based on a priority based mechanism.
 *
 */
static void radio_task(void *parameter) {
    (void)parameter;
    msec500 = xTimerCreate((char *)"t_500", 500 / portTICK_PERIOD_MS, pdTRUE, 0, timer_msec500);
    msec200 = xTimerCreate((char *)"t_200", 200 / portTICK_PERIOD_MS, pdTRUE, 0, timer_msec200);
    menu_send_init();
    xTimerStart(msec500, 0);
    xTimerStart(msec200, 0);
    LOG_UART_INFO("radio alpine\r\n");
    message_raw_reset(&sending_msg_buffer.as_raw);
    message_10_transmit(&sending_msg_buffer.as_10, seq_num_send_next());
    ///Forever loop
    while (FOREVER()) {
        /// Every if restarts the loop if a message was transmitted. In this way after a transmission
        /// we always check for a returning ACK.
        if (wait_for_acknack()) {
            continue;
        }
        ///ACK received, wait a while before sending back another message.
        //		vTaskDelay(5/portTICK_PERIOD_MS);
        ///Reset the repetition flag. A new message will be sent.
        repetition = 0;
///Check if a menu request was received.
        if (check_request_menu()) {
            continue;
        }
        ///Check SWC to be transmitted.
        switch (transmit_keys()) {
            case KEY_TX_STATUS_NONE:
                ///No transmission, continue the checks for other components.
                break;
            case KEY_TX_STATUS_SINGLE:
                ///Single key transmitted. Go to verify ACK/NACK/Timeout.
                vTaskDelay(50 / portTICK_PERIOD_MS);
                continue;
                break;
            case KEY_TX_STATUS_MULTIPLE:
                ///Multiple Key, spend some time and checks for other components.
                vTaskDelay(10 / portTICK_PERIOD_MS);
                break;
        }

        ///Check hvac information.
        if (check_hvac_info()) {
            continue;
        }
        ///Check requests from the radio.
        if (check_request_messages()) {
            continue;
        }
        
        if (check_warning_message()) {
            continue;
        }
        if (check_ext_req_message()) {
            continue;
        }
        ///Check parking sensors information.
        if (check_parking_sensor()) {
            continue;
        }
        ///Idle message every 500ms.
        if (transmit_message_10()) {
            continue;
        }
        timer_500_msec_elapsed = false;
        timer_200_msec_elapsed = false;
    }
}

static enum SWC last_handled_swc = SWC_NO_BUTTON_PRESSED;
/**
 * @brief Handle sull'event loop per ricevere i comandi a volante.
 * I comandi a volante vengono letti tramite questa funzione e passati al task di gestione della trasmissione
 * della seriale tramite una fifo.
 * Dato che il task di trasmissione della seriale scarica la fifo con tempi abbastanza lunghi,
 * (deve fare altro durante il suo giro e alcune cose sono temporizzate) non si possono inviare tutti i messaggi
 * all'interno della FIFO ma bisogna filtrare i messaggi non richiesti in anticipo.
 * @param msg l'evento che ha generato la chiamata della funzione.
 */
static void handle_swc(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SWRC_CHANGED) {
        struct SWC_EV swc;
        memcpy(&swc, &msg->as_swrc.btn, sizeof(struct SWC_EV));

        if (swc.swc == SWC_NO_BUTTON_PRESSED) {
            ///Se il comando ricevuto è nessun comando premuto
            if (last_handled_swc != SWC_NO_BUTTON_PRESSED) {
                ///Se l'ultimo comando ricevuto è un comando premuto
                if ((last_handled_swc % 2) != 0) {
                    ///Se l'ultimo comando è di tipo comando premuto e non comando rilasciato
                    /// allora modifico il comando in comando rilasciato
                    /// perche' potrei aver perso un comando.
                    swc.swc = last_handled_swc++;
                    swc.counter = 0;
                }
            }
        }

        if ((swc.swc % 2) != 0) {
            ///se e' un comando premuto
            if (last_handled_swc % 2 != 0) {
                if (swc.swc != last_handled_swc) {
                    ///Se il vecchio comando era un comando premuto differente da quello attuale
                    /// Invio il comando di rilascio del comando precedente prima del comando attuale.
                    struct SWC_EV tmp;
                    tmp.swc = last_handled_swc + 1;
                    tmp.counter = 0;
                    LOG_DEF_NORMAL("Rilascio\r\n");
                    xQueueSendToBack(swc_queue, &tmp, portMAX_DELAY);
                }
            }
        }
        if (swc.swc != SWC_NO_BUTTON_PRESSED) {
            if (swc.swc != last_handled_swc) {
                LOG_DEF_NORMAL("Ritrasmetto\r\n");
                if (xQueueSendToBack(swc_queue, &swc, 200 / portTICK_PERIOD_MS)) {
                    last_handled_swc = swc.swc;
                } else {
                    LOG_UART_ERROR("handle_swc queue full\r\n");
                }
            }
        }
    }
}

static void handle_menu_send(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_SEND) {
        LOG_UART_DEBUG("a menu to send %p\r\n", msg->as_menu_send.menu);
        xQueueSendToBack(menu_tx_queue, &msg->as_menu_send.menu, 10);
    }
}

static void handle_radio_requests(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MESSAGE_SEND) {
        xQueueSendToBack(requests_queue, &msg->as_radio_request_send.func_to_call, 10);
    }
}

static void handle_source_type(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source_type = msg->as_radio_source.source_type;
    }
}

static bool event_message_18_transmit(union ALPINE_MESSAGE *buff, uint8_t counter) {
    return message_18_transmit(&buff->as_18, counter);
}

static void handle_radio_request_get(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_REQUEST) {
        switch (msg->as_radio_request.radio_request_num) {
            case 0x18:
                event_radio_request_send_func_emit(event_message_18_transmit);
                break;
        }
    }
}
static struct PARKING_SENSOR_DATA parking_sens_oem = {0};
static struct PARKING_SENSOR_DATA parking_sens_aft = {0};
static bool handle_parking_sensor_buzzer = false;
static uint8_t front_rear_seen_status = 0;

void set_vehicle_without_parking_sensor_buzzer(void) {
    buzzer3_enable_parkingsensors();
    handle_parking_sensor_buzzer = true;
}

static void buzzer_handling(void) {
    uint8_t min = (parking_sens_oem.distance.zone_a < parking_sens_oem.distance.zone_b) ? parking_sens_oem.distance.zone_a : parking_sens_oem.distance.zone_b;
    min = (min < parking_sens_oem.distance.zone_c) ? min : parking_sens_oem.distance.zone_c;
    min = (min < parking_sens_oem.distance.zone_d) ? min : parking_sens_oem.distance.zone_d;
    min = (min < parking_sens_oem.distance.zone_e) ? min : parking_sens_oem.distance.zone_e;
    min = (min < parking_sens_oem.distance.zone_f) ? min : parking_sens_oem.distance.zone_f;
    min = (min < parking_sens_oem.distance.zone_g) ? min : parking_sens_oem.distance.zone_g;
    min = (min < parking_sens_oem.distance.zone_h) ? min : parking_sens_oem.distance.zone_h;
    enum PARKING_SENSOR_LEVEL state = PARKING_SENSOR_LEVEL_OFF;
    switch (min) {
        case 0:
        case 1:
            state = PARKING_SENSOR_LEVEL_1;
            break;
        case 2:
            state = PARKING_SENSOR_LEVEL_2;
            break;
        case 3:
            state = PARKING_SENSOR_LEVEL_3;
            break;
        case 4:
        case 5:
            state = PARKING_SENSOR_LEVEL_4;
            break;
        case 6:
        case 7:
            state = PARKING_SENSOR_LEVEL_5;
            break;
        case 8:
        case 9:
            state = PARKING_SENSOR_LEVEL_6;
            break;
        case 10:
        case 11:
            state = PARKING_SENSOR_LEVEL_7;
            break;
        case 12:
        case 13:
        case 14:
            state = PARKING_SENSOR_LEVEL_8;
            break;
        default:
            state = PARKING_SENSOR_LEVEL_OFF;
            break;
    }
    buzzer3_set_parkingsensors_level(state);
}

static void handle_parksensor_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_PARKINGSENSOR_CHANGED) {
        switch (msg->as_parking_sensor.prksens_type) {
            case PARKING_SENSOR_AFTERMARKET:
                memcpy(&parking_sens_aft, &msg->as_parking_sensor.data, sizeof(struct PARKING_SENSOR_DATA));
                break;
            case PARKING_SENSOR_OEM:
                memcpy(&parking_sens_oem, &msg->as_parking_sensor.data, sizeof(struct PARKING_SENSOR_DATA));
                if (handle_parking_sensor_buzzer) {
                    buzzer_handling();
                }
                break;
            default:
                LOG_DEF_NORMAL("Unknown parksens type\r\n");
                break;
        }
        struct PARKING_SENSOR_DATA mix;
        mix.distance.zone_a = (parking_sens_oem.distance.zone_a < parking_sens_aft.distance.zone_a) ? parking_sens_oem.distance.zone_a : parking_sens_aft.distance.zone_a;
        mix.distance.zone_b = (parking_sens_oem.distance.zone_b < parking_sens_aft.distance.zone_b) ? parking_sens_oem.distance.zone_b : parking_sens_aft.distance.zone_b;
        mix.distance.zone_c = (parking_sens_oem.distance.zone_c < parking_sens_aft.distance.zone_c) ? parking_sens_oem.distance.zone_c : parking_sens_aft.distance.zone_c;
        mix.distance.zone_d = (parking_sens_oem.distance.zone_d < parking_sens_aft.distance.zone_d) ? parking_sens_oem.distance.zone_d : parking_sens_aft.distance.zone_d;
        mix.distance.zone_e = (parking_sens_oem.distance.zone_e < parking_sens_aft.distance.zone_e) ? parking_sens_oem.distance.zone_e : parking_sens_aft.distance.zone_e;
        mix.distance.zone_f = (parking_sens_oem.distance.zone_f < parking_sens_aft.distance.zone_f) ? parking_sens_oem.distance.zone_f : parking_sens_aft.distance.zone_f;
        mix.distance.zone_g = (parking_sens_oem.distance.zone_g < parking_sens_aft.distance.zone_g) ? parking_sens_oem.distance.zone_g : parking_sens_aft.distance.zone_g;
        mix.distance.zone_h = (parking_sens_oem.distance.zone_h < parking_sens_aft.distance.zone_h) ? parking_sens_oem.distance.zone_h : parking_sens_aft.distance.zone_h;

        mix.status.zone_a = (parking_sens_oem.status.zone_a > parking_sens_aft.status.zone_a) ? parking_sens_oem.status.zone_a : parking_sens_aft.status.zone_a;
        mix.status.zone_b = (parking_sens_oem.status.zone_b > parking_sens_aft.status.zone_b) ? parking_sens_oem.status.zone_b : parking_sens_aft.status.zone_b;
        mix.status.zone_c = (parking_sens_oem.status.zone_c > parking_sens_aft.status.zone_c) ? parking_sens_oem.status.zone_c : parking_sens_aft.status.zone_c;
        mix.status.zone_d = (parking_sens_oem.status.zone_d > parking_sens_aft.status.zone_d) ? parking_sens_oem.status.zone_d : parking_sens_aft.status.zone_d;
        mix.status.zone_e = (parking_sens_oem.status.zone_e > parking_sens_aft.status.zone_e) ? parking_sens_oem.status.zone_e : parking_sens_aft.status.zone_e;
        mix.status.zone_f = (parking_sens_oem.status.zone_f > parking_sens_aft.status.zone_f) ? parking_sens_oem.status.zone_f : parking_sens_aft.status.zone_f;
        mix.status.zone_g = (parking_sens_oem.status.zone_g > parking_sens_aft.status.zone_g) ? parking_sens_oem.status.zone_g : parking_sens_aft.status.zone_g;
        mix.status.zone_h = (parking_sens_oem.status.zone_h > parking_sens_aft.status.zone_h) ? parking_sens_oem.status.zone_h : parking_sens_aft.status.zone_h;

        mix.park_status.as_uint8 = 0x00;
        mix.park_status.as_flags.active = parking_sens_oem.park_status.as_flags.active || parking_sens_aft.park_status.as_flags.active;
        if ((mix.distance.zone_a != 0x0F) || (mix.distance.zone_b != 0x0F) || (mix.distance.zone_c != 0x0F) || (mix.distance.zone_d != 0x0F)) {
            front_rear_seen_status |= PARKING_SENSOR_TYPE_FRONT;
        }
        if ((mix.distance.zone_e != 0x0F) || (mix.distance.zone_f != 0x0F) || (mix.distance.zone_g != 0x0F) || (mix.distance.zone_h != 0x0F)) {
            front_rear_seen_status |= PARKING_SENSOR_TYPE_REAR;
        }
        LOG_DEF_NORMAL("Parksens type: %02X %02lX %02X %02X\r\n", front_rear_seen_status, mix.distance.as_uint, mix.park_status.as_uint8, mix.status.as_uint);
        LOG_DEF_NORMAL("Active: %02X %02X %02X\r\n", mix.park_status.as_uint8, parking_sens_oem.park_status.as_uint8, parking_sens_aft.park_status.as_uint8);
        alpine_menu_parking_sensors_type(front_rear_seen_status);
        xQueueSendToBack(parksensor_queue, &mix, 10);
    }
}

static void handle_hvac_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_HVAC_CHANGED) {
        xQueueSendToBack(hvac_queue, &msg->as_hvac_info, 10);
    }
}

static void handle_warning_message(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_WARNING_MESSAGE) {
        xQueueSendToBack(warning_msg_queue, &msg->as_warning_message, 10);
    }
}

static void handle_language_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_LANGUAGE_CHANGED) {
        settings_write(RADIO_SETTING_LANGUAGE, msg->as_language.language);
    }
}

static void handle_external_request(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_EXTERNAL_SOURCE) {
        LOG_DEF_NORMAL("Ho una richiesta di cambio schermata.\r\n");
        xQueueSendToBack(external_request_queue, &msg->as_external_request.data, 10);
    }
}

void radio_menu_send_queue_clear(void) {
    xQueueReset(menu_tx_queue);
}

static bool initialized = false;
/**
 * Radio task Setup.
 */
void radio_task_init() {
    if (!initialized) {
        swc_queue = xQueueCreate(16, sizeof(struct SWC_EV));
        menu_tx_queue = xQueueCreate(4, sizeof(menu_manager_t));
        requests_queue = xQueueCreate(4, sizeof(ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC));
        parksensor_queue = xQueueCreate(2, sizeof(struct PARKING_SENSOR_DATA));
        warning_msg_queue = xQueueCreate(2, sizeof(struct EVENT_WARNING_MESSAGE));
        external_request_queue = xQueueCreate(2, sizeof(struct EXTERNAL_REQUEST_DATA));
        hvac_queue = xQueueCreate(2, sizeof(struct HVAC_INFO));
        xTaskCreate(radio_task, (char *)"alpine_task", configMINIMAL_STACK_SIZE + 600, NULL, configMAX_PRIORITIES - 1, NULL);
        radio_lowlevel_init();
        event_connect_callback(eEVENT_SWRC_CHANGED, handle_swc);
        event_connect_callback(eEVENT_EXTERNAL_SOURCE, handle_external_request);
        event_connect_callback(eEVENT_MENU_SEND, handle_menu_send);
        event_connect_callback(eEVENT_RADIO_MESSAGE_SEND, handle_radio_requests);
        event_connect_callback(eEVENT_ACKNACK_CHANGED, handle_acknack);
        event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
        event_connect_callback(eEVENT_RADIO_REQUEST, handle_radio_request_get);
        event_connect_callback(eEVENT_PARKINGSENSOR_CHANGED, handle_parksensor_changed);
        event_connect_callback(eEVENT_HVAC_CHANGED, handle_hvac_changed);
        event_connect_callback(eEVENT_WARNING_MESSAGE, handle_warning_message);
        event_connect_callback(eEVENT_LANGUAGE_CHANGED, handle_language_changed);
        message_10_connect_callback();
        initialized = true;
        acknack_sem = xSemaphoreCreateBinary();
        parking_sens_oem.distance.as_uint = 0xFFFFFFFF;
        parking_sens_aft.distance.as_uint = 0xFFFFFFFF;
        parking_sens_oem.park_status.as_uint8 = 0x00;
        parking_sens_aft.park_status.as_uint8 = 0x00;
        parking_sens_oem.status.as_uint = 0xFFFF;
        parking_sens_aft.status.as_uint = 0xFFFF;
    }
}

void event_radio_request_send_func_emit(ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC func) {
    union EVENT event;
    event.as_generic.event = eEVENT_RADIO_MESSAGE_SEND;
    event.as_radio_request_send.func_to_call = func;
    event_emit(&event);
}

void event_parking_sensor_emit(struct EVENT_PARKING_SENSOR *data) {
    union EVENT event;
    memcpy(&event.as_parking_sensor, data, sizeof(struct EVENT_PARKING_SENSOR));
    event.as_generic.event = eEVENT_PARKINGSENSOR_CHANGED;
    event_emit(&event);
}

void event_radio_media_changed_emit(uint8_t media_type, uint8_t source_type, uint8_t info1, uint8_t info2, uint8_t info3, uint8_t info4, uint8_t info5, uint8_t info6) {
    union EVENT event;
    event.as_radio_source.generic.event = eEVENT_RADIO_MEDIA_CHANGED;
    event.as_radio_source.media_type = media_type;
    event.as_radio_source.source_type = source_type;
    event.as_radio_source.info1 = info1;
    event.as_radio_source.info2 = info2;
    event.as_radio_source.info3 = info3;
    event.as_radio_source.info4 = info4;
    event.as_radio_source.info5 = info5;
    event.as_radio_source.info6 = info6;
    event_emit(&event);
}

void event_phone_info_emit(uint8_t phone_status, uint8_t mic_status, uint8_t signals) {
    union EVENT event;
    event.as_phone_info.generic.event = eEVENT_PHONE_INFO;
    event.as_phone_info.bits.as_struct.phone_status = phone_status;
    event.as_phone_info.bits.as_struct.phone_mic_status = mic_status;
    event.as_phone_info.signals.as_uint8 = signals;
    event_emit(&event);
}

void event_hvac_changed_emit(const struct HVAC_INFO *const info) {
    union EVENT event;
    memcpy(&event.as_hvac_info, info, sizeof(struct HVAC_INFO));
    //Questo va dopo altrimenti viene sovrascritto.
    event.as_generic.event = eEVENT_HVAC_CHANGED;
    event_emit(&event);
}

void event_radio_media_play_status_emit(uint8_t val) {
    union EVENT event;
    event.as_generic.event = eEVENT_MEDIA_PLAY_STATUS;
    event.as_media_play_status.value = val;
    event_emit(&event);
}

void event_radio_request_emit(uint8_t radio_request) {
    union EVENT event;
    event.as_generic.event = eEVENT_RADIO_REQUEST;
    event.as_radio_request.radio_request_num = radio_request;
    event_emit(&event);
}

void event_menu_request_emit_91(uint8_t menu_request, uint8_t menu_index, uint8_t menu_data) {
    union EVENT event;
    event.as_generic.event = eEVENT_MENU_REQUEST;
    event.as_menu_request.msg_type = MENU_REQUEST_NUM_91;
    event.as_menu_request.request.as_91.row_pressed_num = menu_request;
    event.as_menu_request.request.as_91.menu_index = menu_index;
    event.as_menu_request.request.as_91.menu_data = menu_data;
    LOG_DEF_NORMAL("MR_91: %d %d %d\r\n", menu_request,menu_index,menu_data);
    event_emit(&event);
}

void event_menu_request_emit_E5(uint8_t menu_request, uint8_t menu_index, uint8_t menu_data) {
    union EVENT event;
    event.as_generic.event = eEVENT_MENU_REQUEST;
    event.as_menu_request.msg_type = MENU_REQUEST_NUM_E5;
    event.as_menu_request.request.as_e5.row_pressed_num = menu_request;
    event.as_menu_request.request.as_e5.menu_index = menu_index;
    event.as_menu_request.request.as_e5.menu_data = menu_data;
    LOG_DEF_NORMAL("MR_E5: %d %d %d\r\n", menu_request,menu_index,menu_data);
    event_emit(&event);
}

void event_menu_request_emit_E8(uint8_t menu_request, uint8_t menu_index) {
    union EVENT event;
    event.as_generic.event = eEVENT_MENU_REQUEST;
    event.as_menu_request.msg_type = MENU_REQUEST_NUM_E8;
    event.as_menu_request.request.as_e8.row_pressed_num = menu_request;
    event.as_menu_request.request.as_e8.menu_index = menu_index;
    LOG_DEF_NORMAL("MR_E8: %d %d\r\n", menu_request,menu_index);
    event_emit(&event);
}

void event_warning_message_emit(struct EVENT_WARNING_MESSAGE *message) {
    union EVENT event;
    memcpy(&event.as_warning_message, message, sizeof(struct EVENT_WARNING_MESSAGE));
    event.as_generic.event = eEVENT_WARNING_MESSAGE;
    event_emit(&event);
}

void event_compass_emit(uint16_t angle) {
    union EVENT event;
    event.as_generic.event = eEVENT_COMPASS;
    event.as_compass.angle = angle;
    event_emit(&event);
}

void event_external_request_emit(uint8_t type, uint8_t data) {
    union EVENT event;
    event.as_generic.event = eEVENT_EXTERNAL_SOURCE;
    event.as_external_request.data.type = type;
    event.as_external_request.data.data = data;
    event_emit(&event);
}

/**
 * Restituisce l'indice della lingua definita sulla radio.
 */
enum LANGUAGE_DEFINITION get_defined_language(void) {
    enum LANGUAGE_DEFINITION lang = settings_read(RADIO_SETTING_LANGUAGE);
    if (lang >= LANGUAGE_DEFINITION_COUNT) {
        lang = LANGUAGE_DEFINITION_ENGLISH;
        settings_write(RADIO_SETTING_LANGUAGE, LANGUAGE_DEFINITION_ENGLISH);
    }
    return lang;
}

/**
 * @}
 */

#endif
