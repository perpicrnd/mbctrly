#include "radio_message_C3.h"
#include "event_list.h"
#include "logger.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

void message_C3_process(struct ALPINE_MESSAGE_C3 *msg){
	union EVENT ev;
	ev.as_phone_info.generic.event = eEVENT_PHONE_TEXT;
	ev.as_phone_text.string_type = msg->msg.data[6];
	bool end_of_string=false;
	for(uint8_t i=0; i<PHONE_TEXT_STRING_LENGTH && end_of_string==false; i++){
		ev.as_phone_text.string[i] = msg->msg.data[7+i];
		if (msg->msg.data[7+i] == '\0'){
			end_of_string = true;
		}
		if (i == PHONE_TEXT_STRING_LENGTH-1){
			LOG_DEF_ERROR("Buffer media text string full\r\n");
			//Metto i puntini di terminazione, non voglio usare stringhe troppo lunghe.
			ev.as_media_text.string[PHONE_TEXT_STRING_LENGTH-2] = '.';
			ev.as_media_text.string[PHONE_TEXT_STRING_LENGTH-3] = '.';
			ev.as_media_text.string[PHONE_TEXT_STRING_LENGTH-4] = '.';
		}
	}
	event_emit(&ev);
}

/**
 * @}
 */
