#include "radio_message_E5.h"
#include "event_list.h"
#include "events.h"
#include "menu_walking.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

void message_E5_get_requested_mode(struct ALPINE_MESSAGE_E5 *msg) {
    LOG_DEF_NORMAL("Ricevuto: ");
    message_raw_print(&msg->msg);
    //Il pop dei menu e' messo qui in quanto mentre il caricamento del menu viene fatto esclusivamente da un evento,
    //la cancellazione potrebbe essere svolta da piu' funzioni. Per evitare cancellazioni doppie viene anticipata.
    if ((msg->msg.data[6] == 00) && (msg->msg.data[7]) == 00) {
        switch (msg->msg.data[8]) {
            case 0x00: {
                menu_walking_pop();
                union EVENT event;
                event.as_generic.event = eEVENT_EXIT_RADIO_MENU;
                event_emit(&event);
            } break;
            case 0x01:
                menu_walking_pop();
                break;
        }
    }
    event_menu_request_emit_E5(msg->msg.data[6], msg->msg.data[7], msg->msg.data[8]);
}

/**
 * @}
 */
