#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Car status driving mode
 * -----------------------
 * - Command: 0x1F
 * - Len 20
 * - MLen 20
 * - Mode RE
 *
 * Data
 * -----
 * - 0 driving mode enum8
 * - 1 available driving modes bitmaps8
 * - 2 engine configuration struct16 engine/drive characteristics
 * - 4 start stop configuration struct16 start/stop characteristics
 * - 6 DSG coasting configuration struct 16 direct shift gearbox characteristics
 * - 8 headlight configuration struct16 adaptive headlight characteristics
 * - 10 steering configuration struct16 power steering characteristics
 * - 12 reserved struct 16
 * - 14 DCC configuration struct16
 * - 16 air conditioning configuration struct16 climatronic characteristics
 * - 18 ACC configuration struct16 adaptive cruise control characteristics
 *
 * Mode configurations tructure
 * -----------------------------
 * 	- 0 configuration enum8 mode configuration
 * 	- 1 available configurations bitmap8 available mode configurations
 *
 * driving mode:
 * -------------
 * 	- 0 cancel driving mode screen
 * 	- 1 comfort
 * 	- 2 normal
 * 	- 3 sport
 * 	- 4 offroad
 * 	- 5 eco
 * 	- 6 race
 * 	- 7 individual
 * 	**(the mode 0 is used to cancel the driving mode screen.)**
 *
 * Available driving modes
 * -----------------------
 * 	- 0 -
 * 	- 1 comfort
 * 	- 2 normal
 * 	- 3 sport
 * 	- 4 offroad
 * 	- 5 eco
 * 	- 6 race
 * 	- 7 individual
 * 	**(only modes with bits set to 1 can be selected.)**
 *
 * mode configuration
 * ------------------
 * 	- 0 configuration not present
 * 	- 1 comfort
 * 	- 2 normal
 * 	- 3 sport
 * 	- 4 offroad
 * 	- 5 eco
 * 	- 6 race
 * 	- 7 individual
 * 	**(configurations that are set to 0 are not present and should not be displayed)**
 *
 * Available mode configurations
 * -----------------------------
 * - 	0 -
 * - 	1 comfort
 * - 	2 normal
 * - 	3 sport
 * - 	4 offroad
 * - 	5 eco
 * - 	6 race
 * - 	7 individual
 * 	**(only configurations with bits set to 1 can be selected. when all bits are 0 the mode configuration is fixed and cannot be changed.**
 *
 *
 *
 */

struct ALPINE_MESSAGE_1F{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_1F_transmit(struct ALPINE_MESSAGE_1F *msg, uint8_t counter, uint8_t byte1, uint8_t byte2){
	(void)counter;
	(void)byte1;
	(void)byte2;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
