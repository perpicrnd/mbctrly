#pragma once

#include <stdint.h>
#include "event_list.h"

struct MENU_MANAGER;
typedef struct MENU_MANAGER* menu_manager_t;

menu_manager_t menu_manager_create(void);
void menu_manager_reload(menu_manager_t menu);
void menu_manager_set_active_bit(menu_manager_t menu, uint8_t pos);
void menu_manager_clear_active_bit(menu_manager_t menu, uint8_t pos);
void menu_manager_enable_bit_func(menu_manager_t menu, uint8_t pos, ALPINE_EVENT_MENU_SEND_FUNC func);
void menu_manager_enable_bit(menu_manager_t menu, uint8_t pos);
void menu_manager_disable_bit(menu_manager_t menu, uint8_t pos);
uint8_t menu_manager_count_max(menu_manager_t menu);
ALPINE_EVENT_MENU_SEND_FUNC menu_manager_iter_next(menu_manager_t menu);
