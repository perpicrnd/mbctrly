#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Car status: driving information
 * Command: 0x1C
 * -------------
 * - Len 48
 * - MLen 48
 * - Mode R
 *
 * Data
 * ----
 * - 0 statistics since start struct112
 * - 14 statistics since refuelling struct112
 * - 28 statistics long term struct112
 * - 42 remaining fuel struct24
 * - 45 outside temperature struct24
 *
 * statistics:
 * -----------
 * - 0 average fuel consumption uint16
 * 	 + offset 0, resolution: 0.1 min 0 max: 999.9
 * - 2 consumption unit enum 8
 * 	 + 0 l/100km 1 km/l 2 mpg
 * - 3 distance travelled
 * 	 + offset 0 Resolution: 0.1 min 0 max 429496729.5
 * - 7 distance unit
 * 	 + 0 km 1 miles
 * - 8 driving times in minutes
 * 	 + offset 0 resolution 1 min 0 max 16777215
 * - 11 average speed
 * 	 + offset o resolution 1 min 0 max 65535
 * - 13 average speed unit
 * 	 + 0 km/h 1 mph
 *
 * Remaining fuel:
 * ---------------
 * 	- 0 range left until tank empty uint16
 * 		+ offset 0 resolution 1 min 0 max 65535
 * 	- 2 range unit enum8
 * 		+ 0 km 1 miles
 *
 * 	outside temperature
 * 	-------------------
 * 	- 0 temperature unit enum8
 * 	  + 0 °C,
 * 	  + 1 °F
 *  - 1 temperature sint16
 * 	  + offset 0, resolution 0.1
 * 	  + range -40°C +60°C / °F -40 to 160
 */

struct ALPINE_MESSAGE_1C{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_1C_transmit(struct ALPINE_MESSAGE_1C *msg, uint8_t counter){
	(void)counter;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
