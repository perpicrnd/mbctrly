#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Special Function
 * Command: 0x23
 * Len 1
 * MLen 1
 * Mode E
 *
 * 0 uint8 special function ID
 *
 * At present there is only one special function: 0x00 - Cancel current navigation route
 */
struct ALPINE_MESSAGE_23{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_23_transmit(struct ALPINE_MESSAGE_23 *msg, uint8_t counter, uint8_t byte1, uint8_t byte2){
	(void)counter;
	(void)byte1;
	(void)byte2;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
