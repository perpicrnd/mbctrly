#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * External Request
 * Command: 0x24
 * Len 2
 * MLen 2
 * Mode E
 *
 * 0 Request type uint8
 * 1 request data uint8
 *
 * Request type 0x00 Direct source request
 * Valid values for request data are the same as found in command 0xC0
 *
 * Request type 0x01 External Joystick
 * Valid values for "request data are:
 * 	0x00 left
 * 	0x01 right
 * 	0x02 right
 * 	0x03 down
 *
 */

struct ALPINE_MESSAGE_24 {
    struct ALPINE_RAW_MESSAGE msg;
};

static inline bool message_24_transmit(struct ALPINE_MESSAGE_24 *msg, uint8_t counter, uint8_t byte1, uint8_t byte2) {
    message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);
    message_raw_add_uint8(&msg->msg, 0xC7);
    message_raw_add_uint8(&msg->msg, 0xC8);
    message_raw_add_uint8(&msg->msg, counter);
    message_raw_add_uint8(&msg->msg, 0x24);
    message_raw_add_uint8(&msg->msg, 0x00);
    message_raw_add_uint8(&msg->msg, 0x02);
    message_raw_add_uint8(&msg->msg, byte1);
    message_raw_add_uint8(&msg->msg, byte2);
    message_raw_crc_add(&msg->msg);
    return message_raw_transmit(&msg->msg);
}

/**
 * @}
 */
