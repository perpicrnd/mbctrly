#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "logger.h"
#include "radio_lowlevel_task.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
#define RAWMESSAGE_LEN 264

#define EXTRA_CHARS 7	//!< Numero di caratteri presenti nel messaggio oltre al messaggio stesso.
#define LEN_POS 5		//!< Posizione in cui si trova il dato relativo alla lunghezza del messaggio.
#define SEQ_NUM_POS 2	//!< Posizione in cui si trova il dato relativo al numero di sequenza
#define COMMAND_POS 3	//!< Posizione in cui si trova il tipo di comando
#define NEXT_MSG_SEQ 4	//!< Posizione in cui si trova il numero di frame dello stesso messaggio che verranno trasmessi.

/**
 * Valore del crc salvato all'interno dello stato del messaggio
 */
typedef enum{
	CRC_UNKNOWN,//!< CRC_UNKNOWN
	CRC_OK,     //!< CRC_OK
	CRC_KO,     //!< CRC_KO
}CRC_CHECK;

/**
 * Struttura base relativa ad un messaggio trasmetto/ricevuto
 */
struct ALPINE_RAW_MESSAGE{
	unsigned char data[RAWMESSAGE_LEN]; //!<buffer di ricezione/trasmissione
	uint8_t pos;						//!<posizione attuale all'interno del buffer / lunghezza complessiva del messaggio
	bool stuffed;						//!<i dati che vengono ricevuti subiscono un byte stuffing, data[] contiene i dati senza byte stuffing, durante al ricezione viene normalizzato
	bool is_acknack;					//!<flag che indica se il messaggio ricevuto è di tipo ack/nack o messaggio normale
	CRC_CHECK crc;						//!<crc calcolato sul messaggio
};

static bool message_raw_completed(struct ALPINE_RAW_MESSAGE *msg);
static void message_raw_add_uint8_from_extern(struct ALPINE_RAW_MESSAGE *msg, unsigned char c);

/**
 * Number of bytes in the message data
 * @param msg the message to check
 * @return	the number of bytes in the message without sof/header
 */
static inline uint8_t message_raw_datalen(struct ALPINE_RAW_MESSAGE *msg){
	return msg->data[5];
}

/**
 * Check if the message is of type ack/nack or normal
 * @param msg the message to check
 * @return	true if it is an ack or nack, false if it is another type of message
 */
static inline bool message_raw_is_acknack(struct ALPINE_RAW_MESSAGE *msg){
	return msg->is_acknack;
}

/**
 * Check if the acknack message is an ack
 * WARNING: check if the message is acknack first!
 * @param msg	the message to check
 * @return	true if the message is an ack, false otherwise.
 */
static inline bool message_raw_acknack_is_ack(struct ALPINE_RAW_MESSAGE *msg){
	if (msg->data[1] == 0xFF){
		return true;
	}
	return false;
}

/**
 * Check if the acknack message is a nack
 * WARNING: check if the message is acknack first!
 * @param msg the message to check
 * @return true if the message is a nack, false otherwise.
 */
static inline bool message_raw_acknack_is_nack(struct ALPINE_RAW_MESSAGE *msg){
	if (msg->data[1] == 0xF0){
		return true;
	}
	return false;
}

/**
 * check the crc of the message
 * @param msg the message to check
 * @return	CRC_UNKNOWN if the message is not completed, CRC_OK if the CRC match, CRC_KO if the CRC does not match.
 */
static inline CRC_CHECK message_raw_crc_check(struct ALPINE_RAW_MESSAGE *msg){
	CRC_CHECK retVal = CRC_UNKNOWN;

	if (message_raw_completed(msg)){
		if (message_raw_is_acknack(msg)){
			retVal = msg->crc;
		}else{
			uint8_t crc = msg->data[SEQ_NUM_POS]+msg->data[COMMAND_POS]+msg->data[LEN_POS];
			for(uint16_t i=LEN_POS+1; i<msg->data[LEN_POS]+EXTRA_CHARS-1; i++){
				crc += msg->data[i];
			}
			crc ^= 0xFF;
			if (crc == msg->data[msg->data[LEN_POS]+EXTRA_CHARS-1]){
				msg->crc = CRC_OK;
				retVal = CRC_OK;
			}else{
				msg->crc = CRC_KO;
				retVal = CRC_KO;
			}
		}
	}
	return retVal;
}

/**
 * Add a character to the message
 * @param msg the message  where to add the character
 * @param c the char to add
 */
static inline void message_raw_add_uint8(struct ALPINE_RAW_MESSAGE *msg, unsigned char c){
	msg->data[msg->pos] = c;
	msg->pos++;
}

/**
 * When creating a new message this function handle the CRC calculation
 * @param msg the message to apply the CRC to
 * @return true if the CRC is calculated and added to the message.
 */
static inline bool message_raw_crc_add(struct ALPINE_RAW_MESSAGE *msg){
	bool retVal = false;
	if ((message_raw_datalen(msg)+6) == msg->pos){
		uint8_t crc = msg->data[SEQ_NUM_POS]+msg->data[COMMAND_POS]+msg->data[LEN_POS]+msg->data[NEXT_MSG_SEQ];
		for(uint16_t i=LEN_POS+1; i<msg->data[LEN_POS]+EXTRA_CHARS-1; i++){
			crc += msg->data[i];
		}
		crc ^= 0xFF;
		message_raw_add_uint8(msg, crc);
		retVal = true;
	}
	return retVal;
}

/**
 * Reset the message to the initial status
 * @param msg the message to reset.
 */
static inline void message_raw_reset(struct ALPINE_RAW_MESSAGE *msg){
	msg->pos = 0;
	msg->stuffed = false;
	for(uint16_t i=0; i<RAWMESSAGE_LEN; i++){
		msg->data[i] = 0xFF;
	}
	msg->is_acknack = false;
	msg->crc = CRC_UNKNOWN;
}

/**
 * Return the sequence number of the message.
 * @param msg the message to check
 * @return the sequence number of this message.
 */
static inline uint8_t message_raw_seq_number(struct ALPINE_RAW_MESSAGE *msg){
	if (msg->pos >= SEQ_NUM_POS){
		return msg->data[SEQ_NUM_POS];
	}else{
		return 0xFF; //Dato non valido per seq_number
	}
}

/**
 * Return the command byte of the message
 * @param msg the message to check
 * @return the command byte of the message.
 */
static inline uint8_t message_raw_command(struct ALPINE_RAW_MESSAGE *msg){
	return msg->data[COMMAND_POS];
}


/**
 * Update the len field of the message.
 * This function is likely to be used to perform some automatic len calculation,
 * first add all the data to the message, then call this function and then call the crc calculation
 * @param msg the message where to fix the len data
 */
static inline void message_raw_update_message_len(struct ALPINE_RAW_MESSAGE *msg){
	msg->data[LEN_POS] = msg->pos - (LEN_POS + 1);
}

/**
 * Function for add the sof to an empty message.
 * @param msg the message where to add the start of frame.
 */
static inline void message_raw_add_sof(struct ALPINE_RAW_MESSAGE *msg){
	message_raw_add_uint8(msg, 0xC7);
	message_raw_add_uint8(msg, 0xC8);
}


/**
 * Function that check for byte stuffing and ack/nack.
 * This is an extended version of the message_raw_add that is intended for parse receiving messages.
 * @param msg the message where to add the char.
 * @param c the char to add.
 */
static inline void message_raw_add_uint8_from_extern(struct ALPINE_RAW_MESSAGE *msg, unsigned char c){
	if (c == 0xC7){
		message_raw_reset(msg);
	}
	if (msg->pos >= 1){
		switch(msg->data[1]){
		case 0xC8:
		case 0xCA:
			msg->is_acknack = false;
			if (!msg->stuffed){
				if (c == 0xD8){
					msg->stuffed = true;
					return; //Ignoro il carattere.
				}
			}else{
				if (c == 0xAA){
					//Recv D8 AA => 0xC7
					c = 0xC7;
				}else if (c == 0xD8){
					//Nothing to do.
					//Recv D8 D8 => D8
				}else{
					//Recv D8 XX => Nothing.
					msg->stuffed = false;
					return; //Messaggio da ignorare
				}
				msg->stuffed = false;
			}
			break;
		case 0xFF:
			//ACK
		case 0xF0:
			//NACK
			msg->is_acknack = true;
			if (msg->pos == 3){
				msg->data[msg->pos] = c;
			}
			if (msg->pos>= 3){
				uint8_t crc = 0xFF ^ msg->data[2];
				if (crc == msg->data[3]){
					msg->crc = CRC_OK;
					//checksum ok
				}else{
					//checksum errato
					msg->crc = CRC_KO;
				}
			}
			break;
		}
	}
	message_raw_add_uint8(msg, c);

	//Ultimo controllo, se il dato in posizione 0 non è C7 allora resetto il messaggio.
	if (msg->data[0] != 0xC7){
		msg->pos = 0;
		msg->data[0] = 0;
	}
}

/**
 * Add two bytes to the message.
 * @param msg the message to add data to
 * @param data the data to add
 */
static inline void message_raw_add_uint16(struct ALPINE_RAW_MESSAGE *msg, uint16_t data){
	message_raw_add_uint8(msg, data>>8);
	message_raw_add_uint8(msg, data & 0xFF);
}

/**
* Add four bytes to the message.
 * @param msg the message to add data to
 * @param data the data to add
 */
static inline void message_raw_add_uint32(struct ALPINE_RAW_MESSAGE *msg, uint32_t data){
	message_raw_add_uint16(msg, data>>16);
	message_raw_add_uint16(msg, data & 0xFFFF);
}

/**
 * Add a string to the message
 * @param msg the message to add data to
 * @param len the length of the string to add
 * @param buffer the string
 */
static inline void message_raw_add_string(struct ALPINE_RAW_MESSAGE *msg, uint8_t len, const char *buffer){
	for(uint8_t i=0; i<len; i++){
		message_raw_add_uint8(msg, buffer[i]);
	}
}


/**
 * Print the message
 * @param msg the message to print
 */
static inline void message_raw_print(struct ALPINE_RAW_MESSAGE *msg){
	for (uint8_t i=0; i<msg->pos; i++){
		LOG_DEF_NORMAL("%02X", msg->data[i]);
	}
	LOG_DEF_NORMAL("\r\n");
}


/**
 * Check if the message is completed
 * @param msg the message to check
 * @return true if the message is completed, false otherwise
 */
static inline bool message_raw_completed(struct ALPINE_RAW_MESSAGE *msg){
	bool retVal = false;
	if (message_raw_datalen(msg)+7 == msg->pos){
		retVal = true;
	}
	if (message_raw_is_acknack(msg)){
		if (msg->pos >= 4){
			retVal = true;
		}
	}
	return retVal;
}

/**
 * Transmit the message on the serial line
 * @param msg the message to transmit
 * @return true if the message is sent to the transmit queue, false otherwise.
 */
static inline bool message_raw_transmit(struct ALPINE_RAW_MESSAGE *msg){
	bool retVal = false;
	if (message_raw_completed(msg)){
		///Message is good, transmit it.
		radio_transmit_message(msg->data, msg->pos);
		retVal = true;
	}else{
		LOG_DEF_ERROR("ERRORE!!!");
		///Message not good, transmit it anyway in order to have it logged.
		/// Return false from the function to notify the main task.
		radio_transmit_message(msg->data, msg->pos);
	}
	return retVal;
}

/**
 * @}
 */
