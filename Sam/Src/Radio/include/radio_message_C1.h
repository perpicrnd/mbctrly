#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_C1{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_C1_process(struct ALPINE_MESSAGE_C1 *msg);


/**
 * @}
 */
