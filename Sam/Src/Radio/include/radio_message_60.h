#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_60{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_60_transmit(struct ALPINE_MESSAGE_60 *msg, uint8_t counter, uint8_t byte1, uint8_t byte2){
	(void)counter;
	(void)byte1;
	(void)byte2;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
