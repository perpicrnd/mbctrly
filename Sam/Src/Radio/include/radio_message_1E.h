#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Car statys service information
 * ------------------------------
 * - Command: 0x1E
 * - Len 11
 * - MLen 11
 * - Mode R
 *
 * Data
 * -----
 * - 0 Oil change distance uint16 Offset 0 resolution: 100
 * - 2 Oil change unit enum8 0 km 1 miles
 * - 3 VW inspection distance uint16 Offset 0 resolution 100
 * - 5 VW inspection unit enum8 0 km 1 miles
 * - 6 Oil change duration in days uint16 offset 0 resolution 1
 * - 8 VW inspection duration in days uint16 offset 0 resolution 1
 * - 10 overdue indication bmap4
 *
 * overdue indication
 * ------------------
 * - 0 oil change distance
 * - 1 oil change duration
 * - 2 vw inspection distance
 * - 3 vw inspection duration
 *
 * (0 due in x km/mi 1 overdue by x km/mi)
 * when duration is zero, assume "due now" rather that "due in 0 days".
 *
 *
 */
struct ALPINE_MESSAGE_1E{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_1E_transmit(struct ALPINE_MESSAGE_1E *msg, uint8_t counter, uint8_t byte1, uint8_t byte2){
	(void)counter;
	(void)byte1;
	(void)byte2;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
