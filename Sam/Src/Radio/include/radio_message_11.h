#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Vehicle identification string
 * Command: 0x11
 * -------------
 * - Len 18
 * - MLen 18
 * - Mode R
 *
 * Data
 * ----
 * - 0+ Vehicle identification string: String 7 bit ascii null terminated.
 */
struct ALPINE_MESSAGE_11{
	struct ALPINE_RAW_MESSAGE msg;
};

/**
 *
 * @param msg the message where to stock the informations
 * @param counter the sequence number of the message.
 * @return Message always transmitted, return true.
 */
static inline bool message_11_transmit(struct ALPINE_MESSAGE_11 *msg, uint8_t counter){
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC7);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC8);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, counter);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x11);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x12);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 'A');
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);
	message_raw_crc_add((struct ALPINE_RAW_MESSAGE *)msg);
	return message_raw_transmit((struct ALPINE_RAW_MESSAGE *)msg);
}

/**
 * @}
 */
