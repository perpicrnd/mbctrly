#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Car status: car information
 * ---------------------------
 * - Command: 0x1D
 * - Len ODS
 * - MLen -
 * - Mode R
 *
 * note that this command may return multiple strings inside the array
 * example string reported are "please refuel" and "Doors open"
 */
struct ALPINE_MESSAGE_1D{
	struct  ALPINE_RAW_MESSAGE msg;
};


static inline bool message_1D_transmit(struct ALPINE_MESSAGE_1D *msg, uint8_t counter, uint8_t byte1, uint8_t byte2){
	(void)counter;
	(void)byte1;
	(void)byte2;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
