#pragma once

#include "radio_message_raw.h"
#include "logger.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

enum PARKING_SENSOR_TYPE {
	PARKING_SENSOR_TYPE_UNSUPPORTED = 0,
	PARKING_SENSOR_TYPE_REAR,
	PARKING_SENSOR_TYPE_FRONT,
	PARKING_SENSOR_TYPE_BOTH
};

/**
 * @brief Vehicle configuration information
 * Command: 0x18
 * -------------
 * - Len 2
 * - MLen 2
 * - Mode R
 * Data
 * ----
 * - 0 Navigation
 * - uint8 0 Unknown, 1 Primary, 2 Secondary
 * - 1 Parking sensors
 * - uint8 0 unsupported 1 rear 2 front, 3 both
 */
struct ALPINE_MESSAGE_18{
	struct ALPINE_RAW_MESSAGE msg;
};

/**
 *
 * @param msg the message buffer.
 * @param counter the sequence number
 * @return Message always transmitted, return true.
 */
bool message_18_transmit(struct ALPINE_MESSAGE_18 *msg, uint8_t counter);

/**
 * @brief imposta il tipo di sensore di parcheggio impostato.
 * @param type il tipo di sensore da configurare.
 */
void alpine_menu_parking_sensors_type(enum PARKING_SENSOR_TYPE type);

/**
 * @}
 */
