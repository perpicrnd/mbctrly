#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_B6{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_B6_process(struct ALPINE_MESSAGE_B6 *msg);

/**
 * @}
 */
