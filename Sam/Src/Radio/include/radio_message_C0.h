#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_C0{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_C0_process(struct ALPINE_MESSAGE_C0 *msg);


/**
 * @}
 */
