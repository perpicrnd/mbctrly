#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_D0{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline uint16_t message_D0_get_compass(struct ALPINE_MESSAGE_D0 *msg){
	return (((struct ALPINE_RAW_MESSAGE *)msg)->data[6]<<8) + ((struct ALPINE_RAW_MESSAGE *)msg)->data[7];
}
/**
 * @}
 */
