#pragma once

#include "radio_message_raw.h"
#include "event_list.h"
/*
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Parking sensor information
 * Command: 0x19
 * -------------
 * - Len 14
 * - MLen 8
 * - Mode C1000+RE
 *
 * Data
 * ----
 * - 0 Front/Rear distance information A-H struct32
 * - 4 front/rear status information A-H struct16
 * - 6 reserved
 * - 7 Parking system status BMAP
 * - 8 Side distance information I-P struct32
 * - 12 side status information I-P
 *
 * Distance information
 * --------------------
 * - 0 zones A/I Distance
 * - 0.4 zones B/J Distance
 * - 1.0 zones C/K distance
 * - 1.4 zones D/L distance
 * - 2.0 zones E/M distance
 * - 2.4 zones F/N distance
 * - 3.0 zones G/O distance
 * - 3.4 zones H/P distance
 *
 * status information
 * ------------------
 * - 4.0 zones B/J Distance
 * - 4.2 zones B/J Distance
 * - 4.4 zones C/K distance
 * - 4.6 zones D/L distance
 * - 5.0 zones E/M distance
 * - 5.2 zones F/N distance
 * - 5.4 zones G/O distance
 * - 5.6 zones H/P distance
 *
 * Parking system status
 * ---------------------
 * - bit 0-5 reserved
 * - bit 6 trailer connected
 * - bit 7 parking system active
 *
 * **Note**: this command will be sent every 1000ms whilst the parking system is active. It will also be sent on change and request.
 *
 */
struct ALPINE_MESSAGE_19{
	struct ALPINE_RAW_MESSAGE msg;
};



/**
 *
 * @param msg the message buffer.
 * @param counter the sequence number
 * @return Message always transmitted, return true.
 */
bool message_19_transmit(struct ALPINE_MESSAGE_19 *msg, uint8_t counter, struct PARKING_SENSOR_DATA *data);

/**
 * @}
 */
