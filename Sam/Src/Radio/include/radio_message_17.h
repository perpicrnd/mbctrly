#pragma once

#include "stdbool.h"
#include "radio_message_raw.h"
//#include "version.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Interface version information
 * Command: 0x17
 * -------------
 * - Len 4
 * - MLen 4
 * - Mode R
 *
 * Data
 * ----
 * - 0 Major version number
 * - 1 Minor version number
 * - 2-3 interface identification number
 */
struct ALPINE_MESSAGE_17{
	struct ALPINE_RAW_MESSAGE msg;
};

/**
 *
 * @param msg the buffer message.
 * @param counter the sequence number.
 * @return Message always transmitted, return true.
 */
static inline bool message_17_transmit(struct ALPINE_MESSAGE_17 *msg, uint8_t counter){
//	SOFTWARE_VERSION *version = get_software_version();

	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC7);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC8);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, counter);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x17);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x04);
//	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, version->as_uint8.major);
//	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, version->as_uint8.minor);
//	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, version->as_uint8.ident_num0);
//	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, version->as_uint8.ident_num1);
	message_raw_crc_add((struct ALPINE_RAW_MESSAGE *)msg);
	return message_raw_transmit((struct ALPINE_RAW_MESSAGE *)msg);
}

/**
 * @}
 */
