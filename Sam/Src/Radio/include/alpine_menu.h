#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "event_list.h"
#include "radio_message.h"
#include "settings.h"

enum RADIO_SETTING {
    RADIO_SETTING_IGNITION_BYTE = RADIO_SETTINGS_START,
    RADIO_SETTINGS_IGN_MINUTE_BYTE,
    RADIO_SETTING_REVERSE_BYTE,
    RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE,
    RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE,
    RADIO_SETTING_PREFLIGHT_CHECK_BYTE,
    PREFLIGHT_SETTING_BYTE_0,
    PREFLIGHT_SETTING_BYTE_1,
    PREFLIGHT_SETTING_BYTE_2,
    PREFLIGHT_SETTING_BYTE_3,
    PREFLIGHT_SETTING_BYTE_4,
    PREFLIGHT_SETTING_BYTE_5,
    PREFLIGHT_SETTING_BYTE_6,
    PREFLIGHT_SETTING_BYTE_7,
    PREFLIGHT_SETTING_BYTE_8,
    PREFLIGHT_SETTING_BYTE_9,
    PREFLIGHT_SETTING_FLAGS_0,
    PREFLIGHT_SETTING_FLAGS_1,
    RADIO_SETTING_LANGUAGE  //Non mettere niente sotto a questa.
};

//Se questa chiamata da errore, la enum sopra va in overlap con qualcos'altro.
typedef char alpine_menu_array_border_check[(RADIO_SETTING_LANGUAGE) < (RADIO_SETTINGS_START + RADIO_SETTINGS_SIZE) ? 1 : -1];

enum RADIO_SETTING_IGNITION {
    RADIO_SETTING_IGNITION_KEY = 0,
    RADIO_SETTING_IGNITION_DOOR,
    RADIO_SETTING_IGNITION_COUNT
};

enum RADIO_SETTING_REVERSE {
    RADIO_SETTING_REVERSE_NORMAL = 0,
    RADIO_SETTING_REVERSE_DELAY,
    RADIO_SETTING_REVERSE_COUNT
};

enum RADIO_SETTING_PREFLIGHT_CHECK {
    RADIO_PREFLIGHT_CHECK_OFF = 0,
    RADIO_PREFLIGHT_CHECK_ON
};

/**
 *
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Sends a menu row as defined by the parameters.
 * @param msg the message where to stock data.
 * @param menu_id the Menu ID.
 * @param menu_row_num The row number to send.
 * @return true if something is transmitted, false otherwise.
 */
bool menu_send_with_id(union ALPINE_MESSAGE* msg, uint8_t menu_id, uint8_t menu_row_num);

/**
 * @brief abilita la visualizzazione del menu veicolo.
 */
void alpine_menu_disable_vehicle_settings(void);
/**
 * @brief abilita la visualizzazione del menu di gestione dell'ora.
 */
void alpine_menu_disable_clock_management(void);
/**
 * @brief abilita la visualizzazione del menu LIN.
 */
void alpine_menu_disable_lin_management(void);
/**
 * @brief abilita la visualizzazione del menu CAN.
 */
void alpine_menu_disable_can_management(void);

void alpine_menu_disable_ignition_lines(void);
void alpine_menu_disable_reverse_lines(void);
void alpine_menu_disable_buttons_lines(void);

/**
 * @brief Abilita attivazione/disattivazione visualizzazione overlay all'accensione del motore.
 * 
 */
void alpine_menu_disable_preflight_check(void);
/**
 * @brief Abilita attivazione/disattivazione impostazione logica sottochiave con portiera.
 * 
 */
void alpine_menu_disable_ignition_logic(void);

/**
 * @brief Disabilita la riga di menu relativa al cambio formato dell'ora. 
 */
void alpine_menu_disable_12_24_setup(void);
/**
 * @brief Disabilita la riga di menu relativa alla regolazione dell'ora
 * 
 */
void alpine_menu_disable_hour_setup(void);

/**
 * @brief Disabilita la riga di menu relativa alla regolazione dei minuti
 * 
 */
void alpine_menu_disable_minutes_setup(void);

/**
 * @brief Disabilita la riga di menu relativa alla regolazione dei giorni
 * 
 */
void alpine_menu_disable_day_setup(void);

/**
 * @brief Disabilita la riga di menu relativa alla regolazione dei mesi
 * 
 */
void alpine_menu_disable_month_setup(void);

/**
 * @brief Disabilita la riga di menu relativa alla regolazione degli anni
 * 
 */
void alpine_menu_disable_year_setup(void);

/**
 * @brief Inizializza la gestione delle richieste relative alla trasmissione dei menu.
 */
void menu_send_init(void);

/**
 * @brief schedule a new menu transmission with a short delay.
 * @param func the func that draw the new menu.
 */
void enable_radio_menu_refresh(menu_manager_t menu);

/**
 * @brief Reinvia l'ultimo menu.
 * 
 */
void redraw_last_menu(void);

void radio_menu_send_queue_clear(void);

bool vehicle_get_sync_clock(void);
void vehicle_set_sync_clock(bool);

uint8_t preflight_get_time(void);
char* preflight_get_header(void);
char* preflight_get_text(void);
bool preflight_check_menu_was_displayed(void);
void preflight_check_menu_displayed_reset(void);
void display_preflight_check_message(void);