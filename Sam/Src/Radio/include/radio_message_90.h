#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_90{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline uint8_t message_90_get_requested_mode(struct ALPINE_MESSAGE_90 *msg){
	return ((struct ALPINE_RAW_MESSAGE *)msg)->data[6];
}

/**
 * @}
 */
