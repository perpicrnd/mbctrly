#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Stalk information (SWC commands)
 * Command: 0x12
 * -------------
 * - Len 2
 * - MLen 2
 * - Mode E
 *
 * Data
 * ----
 * - 0 Key status 1
 * - 1 Key status 2
 * - Key status 1 pressed 0 released
 *
 * Byte0:
 * ------
 * - 0 Disk/Preset+
 * - 1 Disk/Preset-
 * - 2 Seek/Track+
 * - 3 Seek/Track-
 * - 4 Volume+
 * - 5 Volume-
 * - 6 Mute
 * - 7 Source
 *
 * Byte1:
 * ------
 * - 0 Telephone PickUp
 * - 1 Telephone Hangup
 * - 3 Telephone Voice
 * - 4 reserved
 * - 5 reserved
 * - 6 reserved
 * - 7 reserved
 */
struct ALPINE_MESSAGE_12{
	struct ALPINE_RAW_MESSAGE msg;
};

/**
 *
 * @param msg The buffer where to write the message.
 * @param counter the sequence number
 * @param byte1 Key Status 0
 * @param byte2 Key Status 1
 * @param pressed If the button is pressed or released.
 * @return Message always transmitted, return true.
 */
static inline bool message_12_transmit(struct ALPINE_MESSAGE_12 *msg, uint8_t counter, uint8_t byte1, uint8_t byte2, uint8_t pressed){
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC7);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0xC8);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, counter);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x12);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x04);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, byte1);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, byte2);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, pressed);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, pressed);
	message_raw_crc_add((struct ALPINE_RAW_MESSAGE *)msg);
	return message_raw_transmit((struct ALPINE_RAW_MESSAGE *)msg);
}

/**
 * @}
 */

