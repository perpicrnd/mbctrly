#pragma once

#include <string.h>
#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Menu data (alternative)
 * Command: 0x30
 * Len ODS
 * Mode RE
 *
 * Whenever the device receives the above command it must display a suitable menu as described in the
 * contents. If the command is received whilst the unit is displaying a menu, the action depends on the
 * menu id. If the id is the same as the menu currently being displayed then the menu is updated in the
 * background and the context is saved. If the id is different than the existing menu then it is replaced by
 * the new menu and the menu context is lost and reset. The idea behind this is to allow the interface to
 * dynamically update the menu whilst it is being displayed.
 * ‘Menu Context’ is defined as what is required to store the current menu state, for example, it could
 * include which item is currently being highlighted (If a touch screen is not available and/or a rotary
 * encoder is used), the current item displayed at the top of the screen if the number of menu items
 * exceeds the display height, if a drop down is being displayed, etc.
 * If the menu consists of more items than what fits comfortably on the screen, then the user must be
 * able to scroll through the available items. If menu context is lost, the scroll position is reset to zero
 * (i.e., the first menu item must be visible), else the scroll position must be reserved as long as the menu
 * id remains the same.
 * The data format is specified as follows:
 *
 *
 *
 */
struct ALPINE_MESSAGE_30 {
    struct ALPINE_RAW_MESSAGE msg;
};

/**
 * @brief Transmit a Title row of a menu.
 *
 * Every menu has a title row. With the title row are transmitted in advance some information about how the menu is formed.
 *
 *
 * @param msg The struct that will contains the message.
 * @param counter the sequence number.
 * @param total_frames the number of rows that forms the menu.
 * @param str The string in PGMSPACE.
 * @return true if the message is sent. The message is always sent.
 */
bool message_30_transmit_header(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t total_frames, uint8_t this_menu, const char *str);

/**
 * @brief Defines the row type of a meny entry.
 * Every row of a menu can have different aspect / data / menu / submenu.
 * This enum will provide this information to the radio.
 */
enum MESSAGE_30_ROW_TYPE {
    MESSAGE_30_ROW_TYPE_1COLUMN = 0,        //!< MESSAGE_30_ROW_TYPE_1COLUMN One column of text.
    MESSAGE_30_ROW_TYPE_2COLUMN,            //!< MESSAGE_30_ROW_TYPE_2COLUMN Text transmitted in two columns.
    MESSAGE_30_ROW_TYPE_SUB_OPTION_LIST,    //!< MESSAGE_30_ROW_TYPE_SUB_OPTION_LIST Same as \ref MESSAGE_30_ROW_TYPE_1COLUMN but if selected will display an option list of values.
    MESSAGE_30_ROW_TYPE_SUB_MENU,           //!< MESSAGE_30_ROW_TYPE_SUB_MENU Same as \ref MESSAGE_30_ROW_TYPE_1COLUMN but if selected will display a submenu.
    MESSAGE_30_ROW_TYPE_CHECKBOX,           //!< MESSAGE_30_ROW_TYPE_CHECKBOX small box possibly with tick mark displayed on the left or right hand side of the text. Menu may consist of multiple items which are ticked.
    MESSAGE_30_ROW_TYPE_OPTIONBOX,          //!< MESSAGE_30_ROW_TYPE_OPTIONBOX small box possibly with tick mark or other graphic on the left of right hand side of the text. Menu may consist of multiple items, but only one of those will be ticked.
    MESSAGE_30_ROW_TYPE_TEXTNUMERIC,        //!< MESSAGE_30_ROW_TYPE_TEXTNUMERIC Text and numeric value, text should be displayed in first column, number should be displayed in second column
    MESSAGE_30_ROW_TYPE_TEXTNUMERICHIDDEN,  //!< MESSAGE_30_ROW_TYPE_TEXTNUMERICHIDDEN
};

/**
 * @brief Transmit a row that is contained in a menu.
 *
 *
 * @param msg the struct that will contains the message.
 * @param counter the sequence number. It must be the same as transmitted with \ref message_30_transmit.
 * @param index current row's index.
 * @param total_frames the number of frames that forms the message.
 * @param field_type row's field type.
 * @param attributes
 * @param str
 * @param str1
 * @return
 */
bool message_30_transmit_row_two_column(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, enum MESSAGE_30_ROW_TYPE field_type, uint8_t attributes, const char *str, const char *str1);
bool message_30_transmit_row_checkbox(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t value0);
bool message_30_transmit_row_optionbox(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, uint8_t value);
bool message_30_transmit_textnumeric(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t current, uint8_t min, uint8_t max);
bool message_30_transmit_textnumeric_hidden(struct ALPINE_MESSAGE_30 *msg, uint8_t counter, uint8_t index, uint8_t total_frames, uint8_t attributes, const char *str, const char *str1, uint8_t current, uint8_t min, uint8_t max);
/**
 * @}
 */
