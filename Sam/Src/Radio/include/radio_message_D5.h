#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_D5{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline uint8_t message_D5_get_volume(struct ALPINE_MESSAGE_D5 *msg){
	return ((struct ALPINE_RAW_MESSAGE *)msg)->data[6];
}
/**
 * @}
 */
