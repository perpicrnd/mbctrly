#pragma once

#include <stdint.h>

/**
 * defgroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Initialize the Alpine lowlevel task.
 */
void radio_lowlevel_init(void);

/**
 * @brief Function that provides the last request received by the radio.
 * @return the last request received.
 */
uint8_t get_request_message(void);

/**
 * @brief Function that reset the request message value.
 */
void request_message_reset(void);

/**
 * @brief Function that provides the last menu request received by the radio.
 * @return the last menu request received.
 */
uint8_t get_request_menu_message(void);

/**
 * @brief Function that reset the menu request value.
 */
void request_menu_message_reset(void);

/**
 * @brief Transmit a message to the radio.
 * @param msg the buffer to be transmitted
 * @param len the length of the buffer.
 */
void radio_transmit_message(uint8_t *msg, uint8_t len);

/**
 * @brief Inizializza la gestione delle frequenze della radio.
 * 
 */
void radio_rds_info_init(void);
/**
 * @}
 */
