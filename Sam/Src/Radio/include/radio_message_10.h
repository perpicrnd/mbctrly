#pragma once

#include <stdbool.h>
#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Vehicle information message
 * Command: 0x10
 * -------------
 * - Len 5
 * - MLen 5
 * - Mode P500+E
 *
 * Data
 * ----
 * - 0 Vehicle signal status BMAP8
 * - 1-2 Vehicle Speed
 * - 3 Vehicle information update
 * - 4 Illumination level
 */
struct ALPINE_MESSAGE_10{
	struct ALPINE_RAW_MESSAGE msg;
};

bool message_10_transmit(struct ALPINE_MESSAGE_10 *msg, uint8_t counter);

void message_10_connect_callback(void);
/**
 * @}
 */
