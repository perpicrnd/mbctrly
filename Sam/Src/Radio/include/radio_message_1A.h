#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Air conditioning information
 * Command: 0x1A
 * -------------
 * - Len 8
 * - MLen 8
 * - Mode RE
 *
 * Data
 * ----
 * - 0 Left temperature uint8
 * - 1 right temperature uint8
 * - 2 fan speed uint8
 * - 3 fandirection bmap8
 * - 4 seat heating struct8
 * - 5 indicators bmap8
 * - 6 profile & setup screen bmap8
 * - 7 features bmap8
 *
 * Fan direction
 * -------------
 *
 * bit
 * - 0 windscreen
 * - 1 face
 * - 2 feet
 * - 3 automatic
 * - 4-7 reserved
 *
 * Indicators: 1 = active
 * ----------------------
 * bit
 * - 0 hvac system status
 * - 1 rear window heating
 * - 2 front window heating
 * - 3 A/C
 * - 4 automatic air recirculation
 * - 5 automatic supplementary heater
 * - 6 temperature format
 * - 7 reserved
 *
 * Temperature format:
 * ------------------
 * - When temperature format is 0 (°C)
 *   + 0x00 = LOW, 0xFF = High
 *   + resolution: 0.1 Offset 10°C min 10°C max 35°C
 *
 * - When temperature format is 1 (°F)
 *   + 0x00 = low, oxFF = high
 *   + Resolution: 1
 *   + Offset 50°F min 50°F max 95°F
 *
 * Seat heating
 * ------------
 * - start 4.0 left seat setting uint4
 * - 4.4 right seat setting
 * - 0 = Off,
 * - 1 = low
 * - 2 = medium
 * - 3 = high
 *
 * Profile & setup
 * ---------------
 * - 0.4 profile uint4
 * - 7 setup screen
 * - 0 low, 1 = medium 2 = high
 *
 * - 7
 *   + 0 = normal screen,
 *   + 1 = setupp screen
 *
 * Features
 * --------
 * - 0 rear window heating
 * - 1 front window heating
 * - 2 ac control
 * - 3 automatic air recirculation
 * - 4 seat heating
 * - 5 automatic supplementary heater
 * - 6-7 reserved
 */
struct ALPINE_MESSAGE_1A{
	struct ALPINE_RAW_MESSAGE msg;
};

struct HVAC_INFO;

/**
 *
 * @param msg the message buffer.
 * @param counter the sequence number
 * @return Message always transmitted, return true.
 */
bool  message_1A_transmit(struct ALPINE_MESSAGE_1A *msg, uint8_t counter, const struct HVAC_INFO * const info);

/**
 * @}
 */
