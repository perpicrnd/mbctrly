#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_E8{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_E8_get_requested_mode(struct ALPINE_MESSAGE_E8 *msg);

/**
 * @}
 */
