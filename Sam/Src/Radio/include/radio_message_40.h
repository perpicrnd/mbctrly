#pragma once
#include <string.h>
#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * Special Function
 * Command: 0x23
 * Len 1
 * MLen 1
 * Mode E
 *
 * 0 uint8 special function ID
 *
 * At present there is only one special function: 0x00 - Cancel current navigation route
 */
struct ALPINE_MESSAGE_40{
	struct ALPINE_RAW_MESSAGE msg;
};


static inline bool message_40_transmit(struct ALPINE_MESSAGE_40 *msg, uint8_t counter, uint8_t chime, uint8_t duration, const char * headline, const char *message	){
    bool retVal;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);
	message_raw_add_sof(&msg->msg);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, counter);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x40);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, 0x00);//counter, viene riempito alla fine.
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, chime);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, duration);
    uint8_t len = strlen(headline);
	message_raw_add_string((struct ALPINE_RAW_MESSAGE *)msg, len, headline);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, '\0');
    len = strlen(message);
	message_raw_add_string((struct ALPINE_RAW_MESSAGE *)msg, len, message);
	message_raw_add_uint8((struct ALPINE_RAW_MESSAGE *)msg, '\0');
    message_raw_update_message_len(&msg->msg);
	message_raw_crc_add((struct ALPINE_RAW_MESSAGE *)msg);
    LOG_DEF_NORMAL("Invio allarme.\r\n");
    retVal = message_raw_transmit(&msg->msg);
	return retVal;
}

/**
 * @}
 */
