#pragma once

#include <stdint.h>
#include "controller.h"
#include "events.h"


enum LANGUAGE_DEFINITION {
    LANGUAGE_DEFINITION_GERMAN = 0x00, //!< LANGUAGE_DEFINITION_GERMAN lingua tedesca
    LANGUAGE_DEFINITION_ENGLISH, //!< LANGUAGE_DEFINITION_ENGLISH lingua inglese
    LANGUAGE_DEFINITION_ENGLISH_US, //!< LANGUAGE_DEFINITION_ENGLISH_US lingua inglese (americana)
    LANGUAGE_DEFINITION_FRENCH, //!< LANGUAGE_DEFINITION_FRENCH lingua francese
    LANGUAGE_DEFINITION_ITALIAN, //!< LANGUAGE_DEFINITION_ITALIAN lingua italiana
    LANGUAGE_DEFINITION_SPANISH, //!< LANGUAGE_DEFINITION_SPANISH lingua spagnola
    LANGUAGE_DEFINITION_PORTUGUESE, //!< LANGUAGE_DEFINITION_PORTUGUESE lingua portoghese
    LANGUAGE_DEFINITION_POLISH, //!< LANGUAGE_DEFINITION_POLISH lingua polacca
    LANGUAGE_DEFINITION_CZECH, //!< LANGUAGE_DEFINITION_CZECH lingua ceca
    LANGUAGE_DEFINITION_SWEDISH, //!< LANGUAGE_DEFINITION_SWEDISH lingua svedese
    LANGUAGE_DEFINITION_DUTCH, //!< LANGUAGE_DEFINITION_DUTCH lingua olandese
    LANGUAGE_DEFINITION_JAPANESE, //!< LANGUAGE_DEFINITION_JAPANESE lingua giapponese
    LANGUAGE_DEFINITION_RUSSIAN, //!< LANGUAGE_DEFINITION_RUSSIAN lingua russa
    LANGUAGE_DEFINITION_KOREAN, //!< LANGUAGE_DEFINITION_KOREAN lingua coreana
    LANGUAGE_DEFINITION_TURKISH, //!< LANGUAGE_DEFINITION_TURKISH lingua turca
    LANGUAGE_DEFINITION_CHINESE, //!< LANGUAGE_DEFINITION_CHINESE lingua cinese
    LANGUAGE_DEFINITION_NORWEGIAN, //!< LANGUAGE_DEFINITION_NORWEGIAN lingua norvegese
    LANGUAGE_DEFINITION_SLOVAKIAN, //!< LANGUAGE_DEFINITION_SLOVAKIAN lingua slovacca
    LANGUAGE_DEFINITION_HUNGARIAN, //!< LANGUAGE_DEFINITION_HUNGARIAN lingua ungherese
    LANGUAGE_DEFINITION_COUNT,
};


/**
 * @brief emette un messaggio indicante un cambio lingua sulla radio.
 * @param lang il messaggio con la nuova lingua impostata.
 */
void event_language_changed_emit(enum LANGUAGE_DEFINITION lang);


void vehicle_language_set(enum LANGUAGE_DEFINITION lang);
typedef void (*VEHICLE_CHANGE_TIME_DATE)(uint16_t vehicle_year, uint8_t vehicle_month, uint8_t vehicle_day, uint8_t vehicle_hour, uint8_t vehicle_minute);

/**
 * @brief Questa funzione deve essere riscritta dai veicoli che gestiscono la sincronizzazione dell'ora.
 *
 * @param vehicle_year l'anno da impostare sul veicolo.
 * @param vehicle_month il mese da impostare sul veicolo.
 * @param vehicle_day il giorno da impostare sul veicolo.
 * @param vehicle_hour le ore da impostare sul veicolo.
 * @param vehicle_minute i minuti da impostare sul veicolo.
 */
void vehicle_send_changed_clock_date(uint16_t vehicle_year, uint8_t vehicle_month, uint8_t vehicle_day, uint8_t vehicle_hour, uint8_t vehicle_minute);

void set_vehicle_change_date_func(VEHICLE_CHANGE_TIME_DATE func);

enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void);
enum LED_COLOR vehicle_get_default_led_color(void);
