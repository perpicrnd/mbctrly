#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_B7{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_B7_process(struct ALPINE_MESSAGE_B7 *msg);

/**
 * @}
 */
