#pragma once

#include "FreeRTOS.h"
#include "event_list.h"
#include "semphr.h"

/**
 * @defgroup RADIO_ALPINE_UART Alpine Uart
 * @brief Libreria di comunicazione con Alpine Uart
 * La libreria Alpine UART è responstabile della comunicazione con la radio Alpine.
 * Il protocollo definito nelle specifiche non è completo ed in alcune parti quanto notato
 * con i test differisce da quanto scritto sulle specifiche.
 *
 * Il protocollo prevede due linee RX e TX che possono essere anche contemporanee.
 *
 * L'implementazione prevede che un task si occupi esclusivamente della ricezione ed eventualmente di trasmettere un ack
 * verso la radio.
 *
 * Un altro task invece è responsabile della sola trasmissione.
 *
 * La comunicazione tra i due task avviene tramite l' \ref EVENTS event_system.
 *
 * La schelta di implementare in questo modo la trasmissione è data dal fatto che così facendo un solo task
 * è responsabile della trasmissione e quindi può mantenere un solo buffer di trasmissione.
 * I benefici sono legati prevalentemente al fatto che la trasmissione è sotto controllo, non ci possono essere conflitti
 * di trasmissione con due moduli che vogliono colloquiare contemporaneamente.
 * I contro sono invece legati al fatto che le tempistiche di trasmissione devono essere valutate e ponderate in modo da
 * poter soddisfare le richieste di trasmissione di tutti i messaggi.
 * In pratica non ci sono lock espliciti o mutex di trasmissione, semplicemente il task durante la sua esecuzione valuta
 * quale tipo di messaggio debba essere inviato in un determianto momento ed agisce di conseguenza.
 *
 * @{
 */

/**
 * @brief Give the sequence number to be used in the message.
 * @return the sequence number to be used.
 */
uint8_t seq_num_send(void);

/**
 * @brief Gives the new sequence number and increase the counter.
 * @return the new sequence number.
 */
uint8_t seq_num_send_next(void);

/**
 * @brief Initialize the radio task.
 */
void radio_task_init();

SemaphoreHandle_t get_acknack_sem(void);

void event_radio_request_send_func_emit(ALPINE_EVENT_RADIO_REQUEST_SEND_FUNC func);

/**
 * @brief emette un messaggio di ricezione dalla radio di ACK/NACK/TIMEOUT.
 * @param acknack il tipo di ack ricevuto.
 * @param msg_num il numero del messaggio che ha generato l'evento. Se l'evento è di tipo TIMEOUT msg_num sarà 0.
 */
void event_acknack_emit(enum EVENT_ACKNACK acknack, uint8_t msg_num);

/**
 * @brief Set the vehicle without parking sensor buzzer and enable the internal buzzer.
 * 
 */
void set_vehicle_without_parking_sensor_buzzer(void);

/**
 * @brief Fornisce l'indice della lingua definita sulla radio.
 * @return l'indice della lingua definita sulla radio.
 */
enum LANGUAGE_DEFINITION get_defined_language(void);

/**
 * @}
 */
