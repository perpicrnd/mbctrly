#pragma once

#include "radio_message_raw.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

/**
 * @brief Car status: start/stop information
 * Command: 0x1B
 * -------------
 * - Len ODS
 * - MLen -
 * - Mode R
 *
 * for example the returned data may be in UTF8
 * "Maximum air conditioning requires engine to be running.
 * Note that this command may return multiple strings inside the array
 */
struct ALPINE_MESSAGE_1B{
	struct ALPINE_RAW_MESSAGE msg;
};

static inline bool message_1B_transmit(struct ALPINE_MESSAGE_1B *msg, uint8_t counter){
	(void)counter;
	message_raw_reset((struct ALPINE_RAW_MESSAGE *)msg);

	return false;
}

/**
 * @}
 */
