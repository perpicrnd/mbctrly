#pragma once

#include "radio_message_raw.h"

/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */
struct ALPINE_MESSAGE_C4{
	struct ALPINE_RAW_MESSAGE msg;
};

void message_C4_process(struct ALPINE_MESSAGE_C4 *msg);
/**
 * @}
 */
