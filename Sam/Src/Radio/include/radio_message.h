#pragma once

#include "logger.h"
#include "radio_message_10.h"
#include "radio_message_11.h"
#include "radio_message_12.h"
#include "radio_message_17.h"
#include "radio_message_18.h"
#include "radio_message_19.h"
#include "radio_message_1A.h"
#include "radio_message_1B.h"
#include "radio_message_1C.h"
#include "radio_message_1D.h"
#include "radio_message_1E.h"
#include "radio_message_1F.h"
#include "radio_message_20.h"
#include "radio_message_21.h"
#include "radio_message_22.h"
#include "radio_message_23.h"
#include "radio_message_24.h"
#include "radio_message_30.h"
#include "radio_message_40.h"
#include "radio_message_60.h"
#include "radio_message_90.h"
#include "radio_message_91.h"
#include "radio_message_B6.h"
#include "radio_message_B7.h"
#include "radio_message_B8.h"
#include "radio_message_C0.h"
#include "radio_message_C1.h"
#include "radio_message_C3.h"
#include "radio_message_C4.h"
#include "radio_message_C5.h"
#include "radio_message_C6.h"
#include "radio_message_D0.h"
#include "radio_message_D5.h"
#include "radio_message_E5.h"
#include "radio_message_E8.h"
#include "radio_message_raw.h"
#include "swc.h"
/**
 * @addtogroup RADIO_ALPINE_UART
 * @{
 */

union ALPINE_MESSAGE {
    struct ALPINE_RAW_MESSAGE as_raw;
    struct ALPINE_MESSAGE_10 as_10;
    struct ALPINE_MESSAGE_11 as_11;
    struct ALPINE_MESSAGE_12 as_12;
    struct ALPINE_MESSAGE_17 as_17;
    struct ALPINE_MESSAGE_18 as_18;
    struct ALPINE_MESSAGE_19 as_19;
    struct ALPINE_MESSAGE_1A as_1A;
    struct ALPINE_MESSAGE_12 as_1B;
    struct ALPINE_MESSAGE_12 as_1C;
    struct ALPINE_MESSAGE_12 as_1D;
    struct ALPINE_MESSAGE_12 as_1E;
    struct ALPINE_MESSAGE_12 as_1F;
    struct ALPINE_MESSAGE_24 as_24;
    struct ALPINE_MESSAGE_30 as_30;
    struct ALPINE_MESSAGE_40 as_40;
    struct ALPINE_MESSAGE_90 as_90;
    struct ALPINE_MESSAGE_91 as_91;
    struct ALPINE_MESSAGE_B6 as_b6;
    struct ALPINE_MESSAGE_B7 as_b7;
    struct ALPINE_MESSAGE_B8 as_b8;
    struct ALPINE_MESSAGE_C0 as_c0;
    struct ALPINE_MESSAGE_C1 as_c1;
    struct ALPINE_MESSAGE_C3 as_c3;
    struct ALPINE_MESSAGE_C4 as_c4;
    struct ALPINE_MESSAGE_C5 as_c5;
    struct ALPINE_MESSAGE_C6 as_c6;
    struct ALPINE_MESSAGE_D0 as_d0;
    struct ALPINE_MESSAGE_D5 as_d5;
    struct ALPINE_MESSAGE_E5 as_e5;
    struct ALPINE_MESSAGE_E8 as_e8;
};

static inline bool message_transmit_radio_code(union ALPINE_MESSAGE *msg, uint8_t counter, enum SWC swc) {
    uint8_t byte1 = 0, byte2 = 0;
    bool retVal = false;
    switch (swc) {
        case SWC_NO_BUTTON_PRESSED:
            byte1 = 0;
            byte2 = 0;
            LOG_DEF_NORMAL("No Press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_VOL_UP_PRESSED:
            byte1 = 0x10;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Vol+ press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_VOL_UP_RELEASED:
            byte1 = 0x10;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Vol+ rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_VOL_DOWN_PRESSED:
            byte1 = 0x20;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Vol- press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_VOL_DOWN_RELEASED:
            byte1 = 0x20;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Vol- rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_SEEK_UP_PRESSED:
            byte1 = 0x04;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Seek+ press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_SEEK_UP_RELEASED:
            byte1 = 0x04;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Seek+ rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_SEEK_DOWN_PRESSED:
            byte1 = 0x08;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Seek- press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_SEEK_DOWN_RELEASED:
            byte1 = 0x08;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Seek- rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_MUTE_PRESSED:
            byte1 = 0x40;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Mute press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_MUTE_RELEASED:
            byte1 = 0x40;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Mute rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_PRESET_UP_PRESSED:
            byte1 = 0x01;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Preset+ press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_PRESET_UP_RELEASED:
            byte1 = 0x01;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Preset+ rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_PRESET_DOWN_PRESSED:
            byte1 = 0x02;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Preset- press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_PRESET_DOWN_RELEASED:
            byte1 = 0x02;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Preset- rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_SOURCE_PRESSED:
            byte1 = 0x80;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Source press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_SOURCE_RELEASED:
            byte1 = 0x80;
            byte2 = 0x00;
            LOG_DEF_NORMAL("Source rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_TEL_PICKUP_PRESSED:
            byte1 = 0x00;
            byte2 = 0x01;
            LOG_DEF_NORMAL("TelPickUp press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_TEL_PICKUP_RELEASED:
            byte1 = 0x00;
            byte2 = 0x01;
            LOG_DEF_NORMAL("TelPickUp rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_TEL_HANGUP_PRESSED:
            byte1 = 0x00;
            byte2 = 0x02;
            LOG_DEF_NORMAL("TelHangUp press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_TEL_HANGUP_RELEASED:
            byte1 = 0x00;
            byte2 = 0x02;
            LOG_DEF_NORMAL("TelHangUp rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        case SWC_SPEECH_PRESSED:
            byte1 = 0x00;
            byte2 = 0x04;
            LOG_DEF_NORMAL("Speech press\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 1);
            break;
        case SWC_SPEECH_RELEASED:
            byte1 = 0x00;
            byte2 = 0x04;
            LOG_DEF_NORMAL("Speech rel\r\n");
            retVal = message_12_transmit((struct ALPINE_MESSAGE_12 *)msg, counter, byte1, byte2, 0);
            break;
        default:
            LOG_DEF_WARNING("Unsupported SWC command\r\n");
            retVal = true;
            break;
    }
    return retVal;
}

/**
 * @}
 */
