#pragma once

#include <stdint.h>


void menu_walking_init(void);
void menu_walking_push(uint8_t value);
void menu_walking_pop();
uint8_t menu_walking_current();
void menu_walking_reset(void);

