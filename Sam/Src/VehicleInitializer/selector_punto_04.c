#include <stdio.h>

#include "alpine_task.h"
#include "canbus.h"
#include "fiat_punto.h"
#include "logger.h"
#include "selector.h"
#include "settings.h"
#include "updater.h"

void selector_initialize_vehicle(void) {
    LOG_DEF_NORMAL("Punto init 04\r\n");
    uint32_t var = selector_get_current();
    if (var != 0x07) {
        radio_task_init();
    } else {
        firmware_upgrade_init();
    }
    (void)var;
    fiat_punto_init();
}
