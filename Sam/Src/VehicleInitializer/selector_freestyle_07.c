#include <stdio.h>

#include "alpine_task.h"
#include "canbus.h"
#include "freestyle_rotary.h"
#include "logger.h"
#include "selector.h"
#include "updater.h"

void selector_initialize_vehicle(void) {
    LOG_DEF_NORMAL("Freestyle rotary 07\r\n");
    uint32_t var = selector_get_current();
    if (var != 0x07) {
        radio_task_init();
    } else {
        firmware_upgrade_init();
    }
    (void)var;
    freestyle_rotary_init();
}
