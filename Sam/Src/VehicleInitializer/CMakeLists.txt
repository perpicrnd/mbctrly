cmake_minimum_required(VERSION 3.0)

SET(LIBRARY_NAME selector_lib)

if (CMAKE_CROSSCOMPILING)
	set(SOURCES selector_sam.c)
else()
	find_package (Threads)
	set(SOURCES selector_host.c)
	add_subdirectory(Test)
endif()	

add_library(${LIBRARY_NAME} selector.c ${SOURCES})
target_link_libraries(${LIBRARY_NAME} eventlist)


if (CMAKE_CROSSCOMPILING)
		target_link_libraries(${LIBRARY_NAME} asf_library)
        target_include_directories(${LIBRARY_NAME}  PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
            $<INSTALL_INTERFACE:include/>
            PRIVATE .)
else()
        target_include_directories(${LIBRARY_NAME}  PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
            $<INSTALL_INTERFACE:include/>
            PRIVATE .)
endif()

add_library(selector_fiat_ducato_01 selector_ducato_01.c)
target_link_libraries(selector_fiat_ducato_01 ${LIBRARY_NAME} fiat_ducato updater)

add_library(selector_fiat_ducato_01_camos selector_ducato_01.c)
target_link_libraries(selector_fiat_ducato_01_camos ${LIBRARY_NAME} fiat_ducato updater camos_parksens)
target_compile_definitions(selector_fiat_ducato_01_camos PUBLIC CAMOS_PARK_SENS)

add_library(selector_fiat_ducato_01_camos_hdmi selector_ducato_01.c)
target_link_libraries(selector_fiat_ducato_01_camos_hdmi ${LIBRARY_NAME} fiat_ducato updater camos_parksens)
target_compile_definitions(selector_fiat_ducato_01_camos_hdmi PUBLIC CAMOS_PARK_SENS HDMI_HANDLER)

add_library(selector_iveco_daily_02 selector_daily_02.c)
target_link_libraries(selector_iveco_daily_02 ${LIBRARY_NAME} iveco_daily updater)

add_library(selector_iveco_daily_03 selector_daily_02.c)
target_link_libraries(selector_iveco_daily_03 ${LIBRARY_NAME} iveco_daily_with_lin updater)

add_library(selector_fiat_punto_04 selector_punto_04.c)
target_link_libraries(selector_fiat_punto_04 ${LIBRARY_NAME} fiat_punto updater)

add_library(selector_iveco_daily_06 selector_daily_02.c)
target_link_libraries(selector_iveco_daily_06 ${LIBRARY_NAME} iveco_daily_with_lin_no_btn updater)

add_library(selector_freestyle_rotary_07 selector_freestyle_07.c)
target_link_libraries(selector_freestyle_rotary_07 ${LIBRARY_NAME} freestyle_rotary updater)

add_library(selector_peugeot208 selector_peugeot208.c)
target_link_libraries (selector_peugeot208 ${LIBRARY_NAME} peugeot_208 updater)

add_library(selector_fiat_ducato_11 selector_ducato_11.c)
target_link_libraries(selector_fiat_ducato_11 ${LIBRARY_NAME} fiat_ducato_2021 updater)

add_library(selector_fiat_ducato_11_camos selector_ducato_11.c)
target_link_libraries(selector_fiat_ducato_11_camos ${LIBRARY_NAME} fiat_ducato_2021 updater camos_parksens)
target_compile_definitions(selector_fiat_ducato_11_camos PUBLIC CAMOS_PARK_SENS)