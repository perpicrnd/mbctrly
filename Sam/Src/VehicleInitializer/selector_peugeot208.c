

#include "alpine_task.h"
#include "canbus.h"
#include "logger.h"
#include "peugeot208.h"
#include "selector.h"
#include "settings.h"
#include "updater.h"

void selector_initialize_vehicle(void) {
    LOG_DEF_NORMAL("peugeot208 init\r\n");
    uint32_t var = selector_get_current();
    if (var != 0x07) {
        radio_task_init();
    } else {
        firmware_upgrade_init();
    }
    (void)var;
    peugeot208_init();
}
