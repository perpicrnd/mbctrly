#pragma once

#include <stdint.h>

void selector_init(void);

uint32_t selector_get_current(void);

void selector_initialize_vehicle(void);
