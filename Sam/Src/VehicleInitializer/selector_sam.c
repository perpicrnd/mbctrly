#include "asf.h"
#include "selector.h"

#define SELECTOR_MASK (Sel_1 | Sel_2 | Sel_3)

uint32_t selector_get_current(void) {
    uint32_t retVal = 0;
    if (!ioport_get_pin_level(Sel_1)) {
        retVal |= 0x01;
    }
    if (!ioport_get_pin_level(Sel_2)) {
        retVal |= 0x02;
    }
    if (!ioport_get_pin_level(Sel_3)) {
        retVal |= 0x04;
    }
    return retVal;
}
