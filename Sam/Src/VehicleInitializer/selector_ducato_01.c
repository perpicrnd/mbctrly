#include <stdio.h>

#include "alpine_task.h"
#include "canbus.h"
#include "fiat_ducato_2014.h"
#include "logger.h"
#include "selector.h"
#include "settings.h"
#include "updater.h"
#ifdef CAMOS_PARK_SENS
#include "camos_parksens.h"
#endif
#ifdef HDMI_HANDLER
#include "source_changer.h"
#endif

void selector_initialize_vehicle(void) {
    LOG_DEF_NORMAL("Ducato init 09\r\n");
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 50;
    can_conf.can0.listen_mode = false;
#ifdef CAMOS_PARK_SENS
    camos_parksens_init(&can_conf);
#endif
#ifdef HDMI_HANDLER
    source_changer_init();
#endif
    uint32_t var = selector_get_current();
    if (var != 0x07) {
        radio_task_init();
    } else {
        firmware_upgrade_init();
    }
    (void)var;
    fiat_ducato_init(&can_conf);
}
