#include "selector.h"
#include "controller.h"
#include "event_list.h"
#include "main.h"

/**
 * @brief Ultimo selector letto.
 */
static uint32_t current_selector = -1;

static void secondly_operation(const union EVENT* const event) {
    if (event->as_generic.event == eEVENT_SECONDS_CHANGED) {
        uint32_t var = selector_get_current();
        if (current_selector == (uint32_t)-1) {
            ///First run, save the selector.
            current_selector = var;
        } else if (var != current_selector) {
            software_reset();
        }
        struct CONTROLLER_DATA data;
        data.command = CONTROLLER_WO_SELECTOR;
        data.data0 = current_selector;
        event_controller_emit(&data);
    }
}

void selector_init(void) {
    event_connect_callback(eEVENT_SECONDS_CHANGED, secondly_operation);
}