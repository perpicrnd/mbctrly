#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "event_list.h"
#include "event_list_priv.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "logger.h"
#include "FreeRTOS_CLI.h"
#include "canbus.h"
#include "accessory_uart.h"

/**
 * @brief numero di eventi disponibili nella coda di trasferimento.
 */
#define EVENT_QUEUE_NUM_EV 54

typedef struct TEVENT_HANDLER_ITEM{
	EVENT_HANDLER handler; 	//!<La funzione per gestire il protocollo.
	struct TEVENT_HANDLER_ITEM *next; //!< L'elemento successivo.
}EVENT_HANDLER_ITEM;

static inline EVENT_HANDLER_ITEM *event_handler_item_next(EVENT_HANDLER_ITEM *item){
	EVENT_HANDLER_ITEM *retVal = NULL;
	if (item->next != NULL){
		retVal = item->next;
	}
	return retVal;
}


/**
 * @brief sopprime le stampe degli eventi se la fifo di gestione degli eventi è piena.
 * le stampe vengono soppresse in modo incrementale.
 * Le prime stampe che vengono soppresse sono quelle del canbus, poi le altre.
 * La variabile è definita volatile in quanto l'incremento viene verificato nelle routine di interrupt.
 */
volatile uint8_t event_suppress_canbus_events = 0;
/**
 * Controlla se esiste un elemento successivo nella lista.
  * @param item l'elemento corrente della lista.
 * @return true se esiste un successivo, false altrimenti.
 */
static inline bool event_handler_item_has_next(EVENT_HANDLER_ITEM *item){
	bool retVal = false;
	if (item->next != NULL){
		retVal = true;
	}
	return retVal;
}

/**
 * Crea in memoria un nuovo elemento.
 * @return L'elemento creato o NULL in caso di fallimento.
 */
static inline EVENT_HANDLER_ITEM *event_handler_item_new(void){

	EVENT_HANDLER_ITEM *retVal = (EVENT_HANDLER_ITEM *)calloc(1, sizeof(EVENT_HANDLER_ITEM));
	if (retVal != NULL){
		retVal->next = NULL;
		retVal->handler = NULL;
	}
	return retVal;
}


/**
 * @brief Controlla che item contenga un indirizzo di memoria differente da NULL
 * @param item Il contenitore del puntatore a funzione.
 * @return true in caso di puntatore valido, false altrimenti.
 */
static inline bool event_handler_item_is_valid(EVENT_HANDLER_ITEM *item){
	bool retVal = false;
	if (item->handler != NULL){
		retVal = true;
	}
	return retVal;
}

/**
 * Esegue in sequenza tutte le funzioni aggiunte nella lista.
 * Questa funzione effettua esclusivamente controlli sul fatto che il puntatore tmp->handler sia diverso da null.
 * in tal caso chiama la funzione.
 * La funzione è intesa per poter eseguire in maniera agevole tutte le funzioni associate a determinate azioni che si devono svolgere
 * ad un determinato evento.
 *
 * PIETRO o altri smemorati futuri... La genericità della funzione permette di mettere tutte le inizializzazioni
 * e le funzioni utilizzate nella configurazione della vettura. Se sei arrivato qui perchè vuoi sapere le azioni associate ad
 * un determinato evento. controlla nel file car.c o nel file di inizializzazione della macchina configurata.
 */
static inline void event_handler(EVENT_HANDLER_ITEM *handler, const union EVENT * const event){
	EVENT_HANDLER_ITEM *tmp = handler;
	bool go_on = true;
	do{
		if (event_handler_item_is_valid(tmp)){
			tmp->handler(event);
			go_on = event_handler_item_has_next(tmp);
			tmp = event_handler_item_next(tmp);
		}else{
			go_on = false;
		}
	}while(go_on);
}


/**
 * Aggiunge l'handler alla coda di handler dell'actiontable dopo aver controllato che l'handler richiesto non sia già presente.
 * @param dest La configurazione della vettura.
 * @param item il protocol handler da aggiungere.
 * @return true in caso di successo, false altrimenti.
 */
static inline bool event_add_handler(EVENT_HANDLER_ITEM * const dest, EVENT_HANDLER item){
	bool retVal = false;
	bool go_on = true;
	EVENT_HANDLER_ITEM *tmp = dest;

	do{
		if (tmp->handler == NULL){//Posto libero, inserisco l'item.
			tmp->handler = item;
			go_on = false;
			retVal = true;
		}else if (tmp->handler == item){//Posto occupato da un handler come item, esco.
			go_on = false;
			retVal = true;
		}else if (event_handler_item_has_next(tmp)){//Se esiste un successivo nella lista la scorro
			tmp = event_handler_item_next(tmp);
		}else{
			tmp->next = event_handler_item_new();//Altrimenti inserisco un nuovo elemento.
			if (tmp->next == NULL){//Se non allocato correttamente esco con errore
				go_on = false;
				retVal = false;
			}
		}
	}while(go_on);
	return retVal;
}

static EVENT_HANDLER_ITEM handlers[eEVENT_COUNT];
static QueueHandle_t task_queue;
static TimerHandle_t timer_timeout;
static uint64_t seconds_from_day = 0;
static uint8_t msec_counter = 0;

static void vTimerSecond(TimerHandle_t pxTimer)
{
	(void)pxTimer;
	union EVENT event;
	if (msec_counter++ >= 20){	
        LOG_DEF_DEBUG("%s\r\n", __PRETTY_FUNCTION__);
		event.as_generic.event = eEVENT_SECONDS_CHANGED;
		event.as_timestamp.timestamp = seconds_from_day;
		seconds_from_day += 1;
		event_emit(&event);
		msec_counter = 0;
	}
	event.as_generic.event =  eEVENT_50MSEC_CHANGED;
	event_emit(&event);
}


static bool using_real_clock = false;

bool system_using_real_clock(void){
	return using_real_clock;
}

static void minute_generation(const  union EVENT * const event){
	if ((event->as_timestamp.timestamp % 60) == 0){
		if (using_real_clock == false){
			union EVENT ev;
			memcpy(&ev, event, sizeof(union EVENT));
			ev.as_generic.event = eEVENT_MINUTES_CHANGED;
			event_emit(&ev);
		}
	}
}

static void hours_generation(const  union EVENT * const event){
	if ((event->as_timestamp.timestamp % 3600) == 0){
		if (using_real_clock == false){
			union EVENT ev;
			memcpy(&ev, event, sizeof(union EVENT));
			ev.as_generic.event = eEVENT_HOURS_CHANGED;
			event_emit(&ev);
		}
	}
}



void event_connect_callback(enum EVENTS event, EVENT_HANDLER handler){
	event_add_handler(&handlers[event], handler);
}

static uint8_t counter = 0;
void event_emit_fromISR(union EVENT *event, BaseType_t *woke){
	if(!xQueueSendFromISR(task_queue, event, woke)){
		if (counter++ == 0){
			LOG_DEF_ERROR("QF\r\n");
		}
	}
}

void event_emit(union EVENT *event){
	xQueueSendToBack(task_queue, event, 5);
}

typedef struct{
	uint32_t counter;
}EVENT_COUNTER;

static EVENT_COUNTER event_counter[eEVENT_COUNT];

void event_task_loop(const union EVENT * const event){
    if (event->as_generic.event < eEVENT_COUNT){
        event_handler(&handlers[event->as_generic.event], event);
        event_counter[event->as_generic.event].counter++;
    }
}

static void event_task(void *parameter){
	(void)parameter;
	union EVENT event;
	for(;;){
		
		if (xQueueReceive(task_queue, &event, 500)){
			event_task_loop(&event);
		}
#ifdef __amd64__

#else
        if (uxTaskGetStackHighWaterMark(NULL)<100){
            LOG_DEF_NORMAL("%s stack too low.\r\n", __PRETTY_FUNCTION__);
        }
#endif
	}
}



static bool initialized = false;
void event_init(void){
	LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
	if (!initialized){
		task_queue = xQueueCreate(EVENT_QUEUE_NUM_EV, sizeof(union EVENT));
		xTaskCreate( event_task,  (char *)"event_task", configMINIMAL_STACK_SIZE+400, NULL, configMAX_PRIORITIES - 2, NULL);
#ifdef __amd64__
		LOG_DEF_NORMAL("Using %ld bytes for handlers management. One is: %ld. Num events: %ld\r\n", sizeof(handlers), sizeof(handlers[0]), sizeof(handlers)/sizeof(handlers[0]));
#else
		LOG_EVENT_INFO("Using %d bytes for handlers management %d for queue.\r\n", sizeof(handlers), sizeof(union EVENT)*EVENT_QUEUE_NUM_EV);
#endif
		timer_timeout = xTimerCreate((char *)"t_brk" , 50/portTICK_PERIOD_MS, 	pdTRUE, 0, vTimerSecond);
		xTimerStart(timer_timeout, 0);
		event_connect_callback(eEVENT_SECONDS_CHANGED, minute_generation);
		event_connect_callback(eEVENT_MINUTES_CHANGED, hours_generation);
		// event_connect_callback(eEVENT_MINUTES_CHANGED, event_logger);
		accessory_uart_init();
		initialized = true;
	}
}
