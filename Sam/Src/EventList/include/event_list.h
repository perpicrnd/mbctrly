#pragma once

/**
 * @addtogroup EVENTS eventi del micro
 * @brief Sistema ad eventi del micro.
 * @{
 */
#include <FreeRTOS.h>

#include "events.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * @brief evento temporizzato
 * Evento emesso dall'event system ad intervalli regolari.
 */
struct EVENT_TIMESTAMP{
	/**
	 * riferimento all'evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * timestamp in cui viene generato l'evento.
	 */
	uint64_t timestamp;
};

struct EVENT_TIME{
	/**
	 * Riferimento all'evento generico.
	 */
	struct EVENT_GENERIC generic;

	uint8_t minute;
	uint8_t hour;
	uint8_t day;
	uint8_t month;
	uint16_t year;

};

struct EVENT_TIME_FORMAT{
	/**
	 * Riferimento all'evento generico.
	 */
	struct EVENT_GENERIC generic;

	uint8_t time_format;
	uint8_t date_format;
};

#include "event_list_provider.h"
#include "event_list_union.h"

/**
 * @brief indica se la scheda ha l'ora sincronizzata con la radio.
 * @return true se è sincronizzato, false in caso contrario.
 */
bool system_using_real_clock(void);


/**
 * @brief Firma della funzione che gestirà l'evento generato.
 * @param l'evento generato.
 */
typedef void (*EVENT_HANDLER)(const union EVENT * const);

/**
 * @brief Registra una funzione per la ricezione di un evento.
 * @param event Il tipo di evento per cui la funzione vuole ricevere notifiche
 * @param handler La funzione da chiamare
 */
void event_connect_callback(enum EVENTS event, EVENT_HANDLER handler);
/**
 * @brief Inietta un evento nell'event loop. Questa funzione deve essere chiamata da un interrupt.
 * @param event l'evento che deve essere emesso.
 * @param woke parametro per notificare un possibile context switch.
 */
void event_emit_fromISR(union EVENT *event, BaseType_t *woke);

/**
 * @brief Inietta un evento nell'event loop.
 * @param event l'evento che deve essere emesso.
 */
void event_emit(union EVENT *event);
/**
 * @brief Inizializza l'event loop.
 */
void event_init(void);

/**
 * @brief effettua un loop dell'event task per processare eventi pendenti.
 * @param event l'evento da processare
 */
void event_task_loop(const union EVENT * const event);


/**
 * @brief emette un messaggio con il nuovo orario ricevuto dal veicolo.
 * @param hour le ore
 * @param minute i minuti
 * @param day il giorno
 * @param month il mese
 * @param year l'anno
 */
void event_vehicle_time_changed_emit(uint8_t hour, uint8_t minute, uint8_t day, uint8_t month, uint16_t year);



/**
 * @}
 */
