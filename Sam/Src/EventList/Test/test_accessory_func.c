#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "event_list.h"
#include "accessory_func.c"
#include "event_list.c"
#include "queue.h"

void vApplicationIdleHook(void){
//Make the linker happy.
}

enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void){
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

bool function_xQueueGenericSend_called = false;
BaseType_t __wrap_xQueueGenericSend( QueueHandle_t xQueue, const void * const pvItemToQueue, TickType_t xTicksToWait, const BaseType_t xCopyPosition ){
	(void)xCopyPosition;
	(void)xTicksToWait;
	(void)pvItemToQueue;
	assert_ptr_equal(xQueue, task_queue);
	function_xQueueGenericSend_called = true;
	return pdTRUE;
}

int setup_test(void **state){
	(void)state;
	function_xQueueGenericSend_called = false;
	return 0;
}

int tear_down(void **state){
	(void)state;
	return 0;
}

static void test_event_phone_info_emit(){
	assert_false(function_xQueueGenericSend_called);

//	event_phone_info_emit(0, 0, 0);
//	assert_true(function_xQueueGenericSend_called);
}
//
//static void test_accessory_func_init(){
//	assert_null(access_timer);
//	accessory_init();
//	assert_non_null(access_timer);
//}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
//	  cmocka_unit_test_setup_teardown(test_accessory_func_init, setup_test, tear_down),
	  cmocka_unit_test_setup_teardown(test_event_phone_info_emit, setup_test, tear_down),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
