#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include "event_list.c"
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void) {
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}
void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

static bool function_dummy_handler_called = false;
static bool function_dummy_handler2_called = false;
static void dummy_handler(const union EVENT *const msg) {
    (void)msg;
    function_dummy_handler_called = true;
}

static void dummy_handler2(const union EVENT *const msg) {
    (void)msg;
    function_dummy_handler2_called = true;
}

void __wrap_event_emit(union EVENT *event) {
    event_task_loop(event);
}

void test_event_handler_item_next() {
    //Due elementi per poterli accodare l'uno all'altro.
    EVENT_HANDLER_ITEM *item = event_handler_item_new();
    EVENT_HANDLER_ITEM *next = event_handler_item_new();

    //Gli elementi non sono collegati
    assert_false(event_handler_item_has_next(item));
    assert_false(event_handler_item_has_next(next));
    assert_false(event_handler_item_is_valid(item));
    assert_false(event_handler_item_is_valid(next));
    assert_null(event_handler_item_next(item));
    assert_null(event_handler_item_next(next));

    //Collego manualmente gli elementi
    item->next = next;
    assert_true(event_handler_item_has_next(item));
    assert_ptr_equal(event_handler_item_next(item), next);

    //Non ho ancora messo un handler, verifichiamo che venga aggiunto correttamente
    assert_false(event_handler_item_is_valid(item));
    event_add_handler(item, dummy_handler);
    assert_true(event_handler_item_is_valid(item));

    free(item);
    free(next);
}
void test_event_add_handler() {
    //Un elemento su cui operare
    EVENT_HANDLER_ITEM *item = event_handler_item_new();

    //Non è inizializzato
    assert_false(event_handler_item_has_next(item));
    assert_null(event_handler_item_next(item));

    //Aggiungo l'elemento
    assert_true(event_add_handler(item, dummy_handler));
    //deve essere valido
    assert_true(event_handler_item_is_valid(item));
    //ma non avere nulla dopo.
    assert_null(event_handler_item_next(item));

    //Chiamo l'handler e verifico che sia chiamato.
    event_handler(item, NULL);
    assert_true(function_dummy_handler_called);
    assert_false(function_dummy_handler2_called);

    //Se lo riaggiungo
    assert_true(event_add_handler(item, dummy_handler));
    //rimane valido
    assert_true(event_handler_item_is_valid(item));
    //ma non è stato aggiunto.
    assert_null(event_handler_item_next(item));

    //Se aggiungo un handler differente
    assert_true(event_add_handler(item, dummy_handler2));
    //rimane valido
    assert_true(event_handler_item_is_valid(item));
    //ed ha un successivo.
    assert_non_null(event_handler_item_next(item));

    //Richiamo l'handler e verifico che siano entrambi chiamati.
    event_handler(item, NULL);
    assert_true(function_dummy_handler_called);
    assert_true(function_dummy_handler2_called);
}

void test_event_list_logger() {
    logging_register(LOGGING_EVENT, LOGGING_SEVERITY_DEBUG);
    CAN_MESSAGE msg = {0x6354000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    event_can0_rx_message_emit(&msg);
    event_can0_tx_message_emit(&msg);
    event_can1_rx_message_emit(&msg);
    event_can1_tx_message_emit(&msg);
    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
    event_ignition_emit(IGNITION_OFF);
    event_speed_emit(0x00);
    event_reverse_emit(REVERSE_OFF);
    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
    event_lights_emit(LIGHTS_OFF);

    logging_register(LOGGING_EVENT, LOGGING_SEVERITY_NORMAL);
}

int setup_test(void **state) {
    (void)state;
    function_dummy_handler_called = false;
    function_dummy_handler2_called = false;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_event_handler_item_next, setup_test),
        cmocka_unit_test_setup(test_event_add_handler, setup_test),
        cmocka_unit_test_setup(test_event_list_logger, setup_test),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
