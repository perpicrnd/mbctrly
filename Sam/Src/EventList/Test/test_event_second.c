#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include "event_list.c"
#include "settings.h"

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void) {
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

bool function_xQueueGenericSend_called = false;
BaseType_t __wrap_xQueueGenericSend(QueueHandle_t xQueue, const void *const pvItemToQueue, TickType_t xTicksToWait, const BaseType_t xCopyPosition) {
    (void)xCopyPosition;
    (void)xTicksToWait;
    (void)pvItemToQueue;
    assert_ptr_equal(xQueue, task_queue);
    function_xQueueGenericSend_called = true;
    return pdTRUE;
}

static void test_vTimerSecond() {
    //vTimerSecond genera un evento sul sistema. Lo vedo dal wrap assert sopra.
    vTimerSecond(NULL);
    assert_true(function_xQueueGenericSend_called);
}

static void test_minute_generation() {
    union EVENT event;
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 0;
    minute_generation(&event);
    assert_true(function_xQueueGenericSend_called);
}

static void test_minute_generation_not_called() {
    union EVENT event;
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 1;
    minute_generation(&event);
    assert_false(function_xQueueGenericSend_called);
}

static void test_hour_generation() {
    union EVENT event;
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 0;
    hours_generation(&event);
    assert_true(function_xQueueGenericSend_called);
}

static void test_hour_generation_not_called() {
    union EVENT event;
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 1;
    hours_generation(&event);
    assert_false(function_xQueueGenericSend_called);
}

static int test_setup(void **state) {
    (void)state;
    function_xQueueGenericSend_called = false;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_vTimerSecond, test_setup),
        cmocka_unit_test_setup(test_minute_generation, test_setup),
        cmocka_unit_test_setup(test_minute_generation_not_called, test_setup),
        cmocka_unit_test_setup(test_hour_generation, test_setup),
        cmocka_unit_test_setup(test_hour_generation_not_called, test_setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
