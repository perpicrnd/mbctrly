#pragma once
#include <stdint.h>
#include <stdbool.h>

enum BUTTONS{
    BUTTON_EXTRA,
    BUTTON_MUTE,
    BUTTON_CAM,
    BUTTON_MENU,
    BUTTON_NAVI,
    BUTTON_TEL,
    BUTTON_MIC,
    BUTTON_MUSIC,
    BUTTON_SEEK_UP,
    BUTTON_SEEK_DOWN
};

void received_ir_power(bool value);
void received_button_settings(uint8_t buttons);
void set_button_hw(enum BUTTONS button, bool value);
void init_button_mapping(void);
void buttons_init(void);