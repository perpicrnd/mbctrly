#include <stdbool.h>
#include "asf.h"
#include "buttons.h"
#include "buttons_priv.h"
#include "logger.h"

void set_button_hw(enum BUTTONS button, bool value){
    switch (button)
    {
    case BUTTON_EXTRA:
    ioport_set_pin_level(Cmd_05, value);
    break;
    case BUTTON_MUTE:
    ioport_set_pin_level(Cmd_04, value);
    break;
    case BUTTON_CAM:
    ioport_set_pin_level(Cmd_08, value);
    break;
    case BUTTON_MENU:
    ioport_set_pin_level(Cmd_09, value);
    break;
    case BUTTON_NAVI:
    ioport_set_pin_level(Cmd_06, value);
    break;
    case BUTTON_TEL:
    ioport_set_pin_level(Cmd_07, value);
    break;
    case BUTTON_MIC:
    ioport_set_pin_level(Cmd_00, value);
    break;
    case BUTTON_MUSIC:
    ioport_set_pin_level(Cmd_01, value);
    break;
    case BUTTON_SEEK_UP:
    ioport_set_pin_level(Cmd_10, value);
    break;
    case BUTTON_SEEK_DOWN:
    ioport_set_pin_level(Cmd_12, value);
    break;
    default:
    LOG_DEF_NORMAL("Sconosciuto %02X\r\n", button);
        ioport_set_pin_level(Cmd_00, false);
        ioport_set_pin_level(Cmd_01, false);
        ioport_set_pin_level(Cmd_02, false);
        ioport_set_pin_level(Cmd_03, false);
        ioport_set_pin_level(Cmd_04, false);
        ioport_set_pin_level(Cmd_05, false);
        ioport_set_pin_level(Cmd_06, false);
        ioport_set_pin_level(Cmd_07, false);
        ioport_set_pin_level(Cmd_08, false);
        ioport_set_pin_level(Cmd_09, false);
        ioport_set_pin_level(Cmd_10, false);
        ioport_set_pin_level(Cmd_11, false);
        ioport_set_pin_level(Cmd_12, false);
        break;
    }
}