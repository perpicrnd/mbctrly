#include "buttons.h"
#include "event_list_provider.h"
#include "settings.h"
#include "event_list.h"
#include "vehicle.h"
#include "logger.h"

#define MENU_COUNTER_OFF_TRIG 5
#define MENU_COUNTER_RELEASE 3

#define BTN_MUTE 0x40
#define BTN_CAM 0x30
#define BTN_MUSIC 0x20
#define BTN_MIC 0x10

#define BTN_EXTRA 0x04
#define BTN_MENU 0x03
#define BTN_NAVI 0x02
#define BTN_TEL 0x01

#define BTN_SEEK_UP 0x80
#define BTN_SEEK_DOWN 0x08

static uint8_t menu_counter = MENU_COUNTER_OFF_TRIG-1;
static bool first_run = true;
static bool ir_power_button = false;
static bool keys_power_button = false;
static bool safety_button = false;

void buttons_event_ignition_inverter_value_emit(void){
    if (ir_power_button || keys_power_button || safety_button){
        event_ignition_inverter_value_emit(true);
    }else{
        event_ignition_inverter_value_emit(false);
    }
}

static inline bool reveiced_button_menu(uint8_t buttons){
    bool retVal = false;
    if ((buttons & 0x77) == BTN_MENU){
        retVal = true;
        if (menu_counter < MENU_COUNTER_OFF_TRIG){
            LOG_DEF_NORMAL("Wait\r\n");
            menu_counter++;
        }else{
            LOG_DEF_NORMAL("Invert\r\n");
            keys_power_button = true;
            buttons_event_ignition_inverter_value_emit();
        }
    }else{
        if (first_run){
            menu_counter = 0;
            first_run = false;
        }
        if(menu_counter != 0){
            if (menu_counter < MENU_COUNTER_OFF_TRIG){
                if (menu_counter > MENU_COUNTER_RELEASE){
                    menu_counter = MENU_COUNTER_RELEASE;
                }
                retVal = true;
                set_button_hw(BUTTON_MENU, true);
                LOG_DEF_NORMAL("Menu\r\n");
                menu_counter--;
            }else{
                menu_counter = 0;
            }
        }else{
            set_button_hw(BUTTON_MENU, false);
        }
        keys_power_button = false;
        buttons_event_ignition_inverter_value_emit();
    }
    return retVal;
}

void received_ir_power(bool value){
    ir_power_button = value;
    buttons_event_ignition_inverter_value_emit();
}

static bool cam_no_press = true;
void received_button_settings(uint8_t buttons){
    //LOG_DEF_NORMAL("%d\r\n", buttons);
    if ((buttons & (BTN_EXTRA|BTN_MUTE)) == (BTN_EXTRA|BTN_MUTE)){
        safety_button = true;
        buttons_event_ignition_inverter_value_emit();
    }else if ((buttons & (BTN_EXTRA|BTN_MUTE)) == 0){
        safety_button = false;
        buttons_event_ignition_inverter_value_emit();
    }
    
    reveiced_button_menu(buttons);
    if (((buttons & 0x77) == BTN_MUTE) || ((buttons & 0x77) == BTN_EXTRA)){
        set_button_hw(BUTTON_MUTE, true);
        LOG_DEF_NORMAL("Mute\r\n");
    }else{
        set_button_hw(BUTTON_MUTE, false);
    }
    if ((buttons & 0x77) == BTN_CAM){
        LOG_DEF_NORMAL("CAM\r\n");
        set_button_hw(BUTTON_CAM, true);
        if (cam_no_press) {
            event_source_changer_emit(RADIO_SOURCE_HDMI);
            cam_no_press = false;
        }
    }else{
        set_button_hw(BUTTON_CAM, false);
        cam_no_press = true;
    }
    if ((buttons & 0x77) == BTN_MUSIC){
        LOG_DEF_NORMAL("Music\r\n");
        set_button_hw(BUTTON_MUSIC, true);
    }else{
        set_button_hw(BUTTON_MUSIC, false);
    }
    if ((buttons & 0x77) == BTN_MIC){
        LOG_DEF_NORMAL("Mic\r\n");
        set_button_hw(BUTTON_MIC, true);
    }else{
        set_button_hw(BUTTON_MIC, false);
    }

    if ((buttons & 0x77) == BTN_NAVI){
        LOG_DEF_NORMAL("Navi\r\n");
        set_button_hw(BUTTON_NAVI, true);
    }else{
        set_button_hw(BUTTON_NAVI, false);
    }
    if ((buttons & 0x77) == BTN_TEL){
        LOG_DEF_NORMAL("Tel\r\n");
        set_button_hw(BUTTON_TEL, true);
    }else{
        set_button_hw(BUTTON_TEL, false);
    }
    if ((buttons & 0x88) == BTN_SEEK_UP){
        LOG_DEF_NORMAL("SeekUp\r\n");
        set_button_hw(BUTTON_SEEK_UP, true);
    }else{
        set_button_hw(BUTTON_SEEK_UP, false);
    }
    if ((buttons & 0x88) == BTN_SEEK_DOWN){
        LOG_DEF_NORMAL("SeekDown\r\n");
        set_button_hw(BUTTON_SEEK_DOWN, true);
    }else{
        set_button_hw(BUTTON_SEEK_DOWN, false);
    }
}

void init_button_mapping(void){
}

void buttons_init(void){
    init_button_mapping();
}

enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void) __attribute__((weak));
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void){
    LOG_DEF_NORMAL("%s PLEASE OVERRIDE\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_VERTICAL_LEFT;
}