#include <FreeRTOS.h>
#include <semphr.h>
#include <stdint.h>

#include "asf.h"
#include "event_list.h"
#include "logger.h"
#include "serial_priv.h"
#include "serials.h"

extern struct SERIAL usart2;

void USART2_Handler(void) {
    uint32_t uartStatus = usart_get_status(USART2);
    if ((uartStatus & US_CSR_RXRDY) != 0) {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        uint8_t rx_byte;
        usart_read(USART2, (uint32_t *)&rx_byte);
        xStreamBufferSendFromISR(usart2.xBufferRxUartMessage, &rx_byte, 1, &xHigherPriorityTaskWoken);

        if (xHigherPriorityTaskWoken) {
            taskYIELD();
        }
    }
    if ((uartStatus & US_CSR_TXRDY) != 0) {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        uint8_t tx_byte;
        if (xStreamBufferReceiveFromISR(usart2.xBufferTxUartMessage, &tx_byte, 1, &xHigherPriorityTaskWoken)) {
            if (xHigherPriorityTaskWoken) {
                taskYIELD();
            }
            usart_write(USART2, tx_byte);
        } else {
            usart_disable_interrupt(USART2, (US_IER_TXRDY));
        }
    }
}

void serial_usart2_enable_rx_interrupt(void) {
    usart_enable_interrupt(USART2, (US_CSR_RXRDY));
}

void serial_usart2_enable_tx_interrupt(void) {
    usart_enable_interrupt(USART2, (US_IER_TXRDY));
}

static void usart2_serial_error(const union EVENT *const event) {
    (void)event;
    LOG_DEF_NORMAL("%s multiple configuration\r\n", __PRETTY_FUNCTION__);
}

static bool configured = false;
void serial_usart2_init(struct SERIAL *self, uint32_t uart_speed, const char c) {
    if (configured) {
        event_connect_callback(eEVENT_SECONDS_CHANGED, usart2_serial_error);
    } else {
        configured = true;
        if (self->xBufferRxUartMessage == NULL) {
            self->xBufferRxUartMessage = xStreamBufferCreate(96, 1);
        }
        if (self->xBufferTxUartMessage == NULL) {
            self->xBufferTxUartMessage = xStreamBufferCreate(256, 1);
        }
        usart_reset(USART2);
        usart_disable_interrupt(USART2, US_CSR_TXRDY | US_CSR_RXRDY);

        sam_usart_opt_t usart_console_settings;

        usart_console_settings.baudrate = uart_speed;
        usart_console_settings.char_length = US_MR_CHRL_8_BIT;
        usart_console_settings.parity_type = US_MR_PAR_NO;
        switch (c) {
            case 'E':
                usart_console_settings.parity_type = UART_MR_PAR_EVEN;
                break;
            case 'O':
                usart_console_settings.parity_type = UART_MR_PAR_ODD;
                break;
            default:
                usart_console_settings.parity_type = UART_MR_PAR_NO;
                break;
        }
        usart_console_settings.stop_bits = false;
        usart_console_settings.channel_mode = US_MR_CHMODE_NORMAL;
        usart_console_settings.irda_filter = 0;

        sysclk_enable_peripheral_clock(ID_USART2);
        usart_init_rs232(USART2, &usart_console_settings, sysclk_get_cpu_hz());
        NVIC_EnableIRQ(USART2_IRQn);
        usart_enable_rx(USART2);
        usart_enable_tx(USART2);
        usart_enable_interrupt(USART2, US_IER_RXRDY);  //abilita [rx ready]
        NVIC_SetPriority(USART2_IRQn, 15);
        NVIC_EnableIRQ(USART2_IRQn);
    }
}