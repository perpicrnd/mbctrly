#pragma once

#include "serials.h"
/**
 * @brief Inizializzazione dell'hardware responsabile della lettura dei dati da seriale.
 *
 * Questa funzione è chiamata da \ref serial_init.
 */
void serial_uart_init(struct SERIAL *self, uint32_t uart_speed, const char c);
void serial_usart0_init(struct SERIAL *self, uint32_t uart_speed, const char c);
void serial_usart1_init(struct SERIAL *self, uint32_t uart_speed, const char c);
void serial_usart2_init(struct SERIAL *self, uint32_t uart_speed, const char c);

void serial_tx_buff(char *buff);
int serial_printf(struct SERIAL *serial, const char *__fmt, ...);
void serial_putstr(struct SERIAL *serial, uint8_t *buff, int len);
int serial_tx_char(struct SERIAL *serial, char c);

void serial_uart_enable_rx_interrupt(void);
void serial_uart_enable_tx_interrupt(void);
void serial_usart0_enable_rx_interrupt(void);
void serial_usart0_enable_tx_interrupt(void);
void serial_usart1_enable_rx_interrupt(void);
void serial_usart1_enable_tx_interrupt(void);
void serial_usart2_enable_rx_interrupt(void);
void serial_usart2_enable_tx_interrupt(void);