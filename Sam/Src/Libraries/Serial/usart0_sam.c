#include <FreeRTOS.h>
#include <semphr.h>
#include <stdint.h>
#include <string.h>

#include "serials.h"
#include "serial_priv.h"
#include "asf.h"
#include "logger.h"
#include "event_list.h"

extern struct SERIAL usart0;
static bool configured_rs232 = false;
static bool configured_lin = false;
static uint32_t lin_buffer[LIN_BUFFER_MAX_LEN] = {0x00, 0x55};
static uint8_t lin_buffer_index = 0;

void USART0_Handler(void)
{
    uint32_t usartStatus = usart_get_status(USART0);
    if (configured_rs232)
    {
        if ((usartStatus & US_CSR_RXRDY) != 0)
        {
            BaseType_t xHigherPriorityTaskWoken = pdFALSE;
            uint8_t rx_byte;
            usart_read(USART0, (uint32_t *)&rx_byte);
            xStreamBufferSendFromISR(usart0.xBufferRxUartMessage, &rx_byte, 1, &xHigherPriorityTaskWoken);

            if (xHigherPriorityTaskWoken)
            {
                taskYIELD();
            }
        }
        if ((usartStatus & US_CSR_TXRDY) != 0)
        {
            BaseType_t xHigherPriorityTaskWoken = pdFALSE;
            uint8_t tx_byte;
            if (xStreamBufferReceiveFromISR(usart0.xBufferTxUartMessage, &tx_byte, 1, &xHigherPriorityTaskWoken))
            {
                if (xHigherPriorityTaskWoken)
                {
                    taskYIELD();
                }
                usart_write(USART0, tx_byte);
            }
            else
            {
                usart_disable_interrupt(USART0, (US_IER_TXRDY));
            }
        }
    }
    if (configured_lin)
    {
        //	Se sono in modalità LIN vado a vedere il motivo dell'interrupt
        if ((usartStatus & US_CSR_LINBK) != 0)
        {
            // Ricevuto break field
            lin_buffer_index = LIN_BUFFER_ID_INDEX;
            usart_reset_status(USART0);
        }
        if ((usartStatus & US_CSR_LINID) != 0)
        {
            // Ricevuto ID
            lin_buffer[lin_buffer_index] = usart_lin_read_identifier(USART0);
            //LOG_DEF_NORMAL("%d", (int)lin_buffer[lin_buffer_index]);
            lin_buffer_index++;
            usart_reset_status(USART0);
        }
        if ((usartStatus & US_CSR_RXRDY) != 0)
        {
            // Ricevuto dato (1 byte)
            if (lin_buffer_index < LIN_BUFFER_MAX_LEN)
            {
                usart_read(USART0, &lin_buffer[lin_buffer_index]);
                //	LOG_DEF_NORMAL("%d", (int)lin_buffer[lin_buffer_index]);
                lin_buffer_index++;
            }
        }
        if ((usartStatus & US_CSR_LINTC) != 0)
        {
            // Trasmissione completata (ho ricevuto tutti i byte previsti)
            //	LOG_DEF_NORMAL("\r\n");
            BaseType_t xHigherPriorityTaskWoken = pdFALSE;
            union EVENT event;
            event.as_generic.event = eEVENT_LIN_RX_MESSAGE;
            memcpy(&event.as_lin_message.msg, lin_buffer, sizeof(LIN_MESSAGE));
            event_emit_fromISR(&event, &xHigherPriorityTaskWoken);
            lin_buffer_index = LIN_BUFFER_ID_INDEX;
            usart_reset_status(USART0);
        }
    }
}

void serial_usart0_enable_rx_interrupt(void)
{
    usart_enable_interrupt(USART0, (US_CSR_RXRDY));
}

void serial_usart0_enable_tx_interrupt(void)
{
    usart_enable_interrupt(USART0, US_IER_TXRDY);
}

static void usart0_serial_error(const union EVENT *const event)
{
    (void)event;
    LOG_DEF_NORMAL("%s multiple configuration\r\n", __PRETTY_FUNCTION__);
}

void serial_usart0_init(struct SERIAL *self, uint32_t uart_speed, const char c)
{
    if (configured_rs232 || configured_lin)
    {
        event_connect_callback(eEVENT_SECONDS_CHANGED, usart0_serial_error);
    }
    else
    {
        configured_lin = false;
        configured_rs232 = true;

        if (self->xBufferRxUartMessage == NULL)
        {
            self->xBufferRxUartMessage = xStreamBufferCreate(96, 1);
        }
        if (self->xBufferTxUartMessage == NULL)
        {
            self->xBufferTxUartMessage = xStreamBufferCreate(256, 1);
        }

        usart_reset(USART0);
        usart_disable_interrupt(USART0, US_CSR_TXRDY | US_CSR_RXRDY);

        sam_usart_opt_t usart_console_settings;

        usart_console_settings.baudrate = uart_speed;
        usart_console_settings.char_length = US_MR_CHRL_8_BIT;
        switch (c)
        {
        case 'E':
            usart_console_settings.parity_type = UART_MR_PAR_EVEN;
            break;
        case 'O':
            usart_console_settings.parity_type = UART_MR_PAR_ODD;
            break;
        default:
            usart_console_settings.parity_type = UART_MR_PAR_NO;
            break;
        }
        usart_console_settings.stop_bits = false;
        usart_console_settings.channel_mode = US_MR_CHMODE_NORMAL;
        usart_console_settings.irda_filter = 0;

        sysclk_enable_peripheral_clock(ID_USART0);
        usart_init_rs232(USART0, &usart_console_settings, sysclk_get_cpu_hz());
        NVIC_EnableIRQ(USART0_IRQn);
        usart_enable_rx(USART0);
        usart_enable_tx(USART0);
        usart_enable_interrupt(USART0, US_IER_RXRDY); //abilita [rx ready]
        NVIC_SetPriority(USART0_IRQn, 15);
        NVIC_EnableIRQ(USART0_IRQn);
    }
}

void serial_usart0_init_LIN(uint32_t uart_speed)
{
    if (configured_rs232 || configured_lin)
    {
        event_connect_callback(eEVENT_SECONDS_CHANGED, usart0_serial_error);
    }
    else
    {
        configured_lin = true;
        configured_rs232 = false;
        usart_reset(USART0);
        usart_disable_interrupt(USART0, US_CSR_TXRDY | US_CSR_RXRDY);
        sysclk_enable_peripheral_clock(ID_USART0);
        usart_lin_disable_pdc_mode(USART0); //Non usiamo il DMA
        if (usart_init_lin_slave(USART0, uart_speed, sysclk_get_cpu_hz()))
        {
            LOG_DEF_NORMAL("LIN init KO\r\n");
        }
        else
        {
			usart_lin_disable_pdc_mode(USART0);					  //Non usiamo il DMA
			//Configurazione del LIN (rif. 35.7.8.15)
			usart_lin_enable_parity(USART0);
			usart_lin_enable_checksum(USART0);
			usart_lin_set_checksum_type(USART0, 0);
			usart_lin_set_data_len_mode(USART0, 0);				  //Usiamo una len fissa e non la logica dello standard lin (rif. 35.7.8.11)
			usart_lin_set_response_data_len(USART0, LIN_MSG_LEN); //Impostiamo la lunghezza del messaggio (rif. 35.7.8.11)
			usart_lin_set_node_action(USART0, 1);				  //LIN in configurazione SUBSCRIBE per avere un interrupt ad ogni byte ricevuto
			NVIC_EnableIRQ(USART0_IRQn);
			usart_enable_rx(USART0);
			usart_enable_interrupt(USART0, US_IER_RXRDY | US_IER_LINTC | US_IER_LINID | US_IER_LINBK);	 //Configuriamo gli interrupt che ci interessano
			NVIC_SetPriority(USART0_IRQn, 15);
			NVIC_EnableIRQ(USART0_IRQn);
			LOG_DEF_NORMAL("LIN init OK\r\n");
        }
    }
}