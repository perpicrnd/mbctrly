#include <FreeRTOS.h>
#include <semphr.h>
#include <stdint.h>

#include "serials.h"
#include "serial_priv.h"
#include "asf.h"
#include "logger.h"
#include "event_list.h"

extern struct SERIAL uart;

void UART_Handler(void){
	uint32_t uartStatus = uart_get_status(UART);
	if ((uartStatus & UART_SR_RXRDY) != 0) {
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		uint8_t rx_byte;
		uart_read(UART, &rx_byte );
		xStreamBufferSendFromISR(uart.xBufferRxUartMessage, &rx_byte, 1, &xHigherPriorityTaskWoken);
		if (xHigherPriorityTaskWoken) {
			taskYIELD ();
		}
	}
	if ((uartStatus & UART_SR_TXRDY) != 0) {
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		uint8_t tx_byte;
		if (xStreamBufferReceiveFromISR(uart.xBufferTxUartMessage, &tx_byte, 1, &xHigherPriorityTaskWoken)){
			if (xHigherPriorityTaskWoken) {
				taskYIELD();
			}
			uart_write(UART, tx_byte);
		} else {
			uart_disable_interrupt(UART, (UART_SR_TXRDY));
		}
	}
}

void serial_uart_enable_rx_interrupt(void){
	uart_enable_interrupt (UART, (UART_SR_RXRDY));
}

void restart_system(void){
	NVIC_SystemReset();
}

void serial_uart_enable_tx_interrupt(void){
	uart_enable_interrupt (UART, (UART_SR_TXRDY));//abilita [end of tx] e [rx ready]
}

static void uart_serial_error(const union EVENT * const event){
	(void)event;
	LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
}

static bool configured = false;
void serial_uart_init(struct SERIAL * self, uint32_t uart_speed, const char c) {
	LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
	if (configured){
		event_connect_callback(eEVENT_SECONDS_CHANGED, uart_serial_error);
	}else{
		configured = true;
		if (self->xBufferRxUartMessage == NULL) {
            self->xBufferRxUartMessage = xStreamBufferCreate(96, 1);
        }
        if (self->xBufferTxUartMessage == NULL) {
            self->xBufferTxUartMessage = xStreamBufferCreate(256, 1);
        }
		
		sam_uart_opt_t  uart_serial_options = {
			.ul_baudrate = uart_speed,
			.ul_mck = sysclk_get_peripheral_hz(),
			.ul_mode = UART_MR_PAR_NO,
		};
        switch(c){
            case 'E':
            uart_serial_options.ul_mode = UART_MR_PAR_EVEN;
            break;
            case 'O':
            uart_serial_options.ul_mode = UART_MR_PAR_ODD;
            break;
            default:
            uart_serial_options.ul_mode = UART_MR_PAR_NO;
            break;
        }
		/* Configure UART. */
		NVIC_EnableIRQ(UART_IRQn);
		pmc_enable_periph_clk(ID_UART);
		uart_init(UART, &uart_serial_options);
		uart_reset(UART);
		uart_enable(UART);
		uart_enable_interrupt(UART, (UART_SR_RXRDY));	//abilita [rx ready]
		NVIC_SetPriority(UART_IRQn, 15);
	}
}

