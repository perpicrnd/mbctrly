#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <FreeRTOS.h>
#include <semphr.h>
#include "stream_buffer.h"

#include "logger.h"
#include "serial_priv.h"
#include "serials.h"

static SemaphoreHandle_t tx_mutex = NULL;
struct SERIAL uart;
struct SERIAL usart0;
struct SERIAL usart1;
struct SERIAL usart2;

int serial_tx_char(struct SERIAL *serial, char c) {
    int retVal = -1;
    if (serial->xBufferTxUartMessage != NULL) {
        xStreamBufferSend(serial->xBufferTxUartMessage, &c, 1, 10 / portTICK_PERIOD_MS);
        if (serial->tx_isr_ena != NULL) {
            serial->tx_isr_ena();
        }
        retVal = c;
    }
    return retVal;
}

#define ENABLE_UART_LOGGING 0

inline void serial_putstr(struct SERIAL *serial, uint8_t *buff, int len) {
#if ENABLE_UART_LOGGING
    bool skip_print = false;
    if (serial == &uart) {
        LOG_DEF_NORMAL("UART  : ");
    } else if (serial == &usart0) {
        // skip_print = true;
        LOG_DEF_NORMAL("USART0: ");
    } else if (serial == &usart1) {
        LOG_DEF_NORMAL("USART1: ");
    }

    if (!skip_print) {
        if (buff[0] == 0x9F) {
            for (uint8_t i = 0; i < len; i++) {
                LOG_DEF_NORMAL("%02X ", buff[i]);
            }
            LOG_DEF_NORMAL("\r\n");
        } else {
            LOG_DEF_NORMAL("%s", (char *)buff);
        }
    }
#endif
    if (!xStreamBufferSend(serial->xBufferTxUartMessage, buff, len, 10 / portTICK_PERIOD_MS)) {
        LOG_DEF_NORMAL("Coda di trasmissione piena.\r\n");
    }
    if (serial->tx_isr_ena != NULL) {
        serial->tx_isr_ena();
    }
}
#define BUFFER_SIZE 256
static char tx_buffer[BUFFER_SIZE];
int serial_printf(struct SERIAL *serial, const char *__fmt, ...) {
    int retVal = 0;
    if (tx_mutex != NULL) {
        if (xSemaphoreTake(tx_mutex, 100)) {
            va_list ap;
            va_start(ap, __fmt);
            vsnprintf(tx_buffer, BUFFER_SIZE, (char *)__fmt, ap);
            va_end(ap);
            serial_putstr(serial, (uint8_t *)tx_buffer, strlen(tx_buffer));
            xSemaphoreGive(tx_mutex);
        }
    }
    return retVal;
}

void serial_init(void) {
    if (tx_mutex == NULL) {
        tx_mutex = xSemaphoreCreateMutex();
    }
}

struct SERIAL usart0 = {
    .init = serial_usart0_init,
    .rx_isr_ena = serial_usart0_enable_rx_interrupt,
    .tx_isr_ena = serial_usart0_enable_tx_interrupt,
    .tx_char = serial_tx_char,
    .put_string = serial_putstr,
    .printf = serial_printf,
    .xBufferRxUartMessage = NULL,
    .xBufferTxUartMessage = NULL,
};

struct SERIAL usart1 = {
    .init = serial_usart1_init,
    .rx_isr_ena = serial_usart1_enable_rx_interrupt,
    .tx_isr_ena = serial_usart1_enable_tx_interrupt,
    .tx_char = serial_tx_char,
    .put_string = serial_putstr,
    .printf = serial_printf,
    .xBufferRxUartMessage = NULL,
    .xBufferTxUartMessage = NULL,
};

struct SERIAL usart2 = {
    .init = serial_usart2_init,
    .rx_isr_ena = serial_usart2_enable_rx_interrupt,
    .tx_isr_ena = serial_usart2_enable_tx_interrupt,
    .tx_char = serial_tx_char,
    .put_string = serial_putstr,
    .printf = serial_printf,
    .xBufferRxUartMessage = NULL,
    .xBufferTxUartMessage = NULL,
};
struct SERIAL uart = {
    .init = serial_uart_init,
    .rx_isr_ena = serial_uart_enable_rx_interrupt,
    .tx_isr_ena = serial_uart_enable_tx_interrupt,
    .tx_char = serial_tx_char,
    .put_string = serial_putstr,
    .printf = serial_printf,
    .xBufferRxUartMessage = NULL,
    .xBufferTxUartMessage = NULL,
};

