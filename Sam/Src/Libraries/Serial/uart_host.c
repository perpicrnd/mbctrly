#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include "FreeRTOS.h"
#include "logger.h"
#include "serial_priv.h"
#include "serials.h"
#include "timers.h"

extern struct SERIAL usart0;
extern struct SERIAL usart1;
extern struct SERIAL usart2;
extern struct SERIAL uart;

static FILE* handle_uart0;
static FILE* handle_usart0;
static FILE* handle_usart1;
static FILE* handle_usart2;

static bool uart0_recv_9F = false;
static bool usart0_recv_9F = false;
static bool usart1_recv_9F = false;

static TimerHandle_t timer = NULL;

int _kbhit() {
    static const int STDIN = 0;
    static bool initialized = false;

    if (!initialized) {
        // Use termios to turn off line buffering
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

#define BUFF_LEN 40

static void timer_printout(TimerHandle_t timer) {
    (void)timer;
    uint8_t c;
    char buff[BUFF_LEN];
    if (xStreamBufferReceive(usart0.xBufferTxUartMessage, &c, 1, 0)) {
        //write(handle_uart0, &c, 1);
        snprintf(buff, BUFF_LEN, "%02X ", c);
        if (usart0_recv_9F) {
            if (c == 0x02) {
                snprintf(buff, BUFF_LEN, "%lu 9F %02X ", (unsigned long)time(NULL), c);
            }
            if (c == 0x03) {
                snprintf(buff, BUFF_LEN, "9F %02X\r\n", c);
            }
            if (c == 0x9F) {
                snprintf(buff, BUFF_LEN, "9F %02X ", c);
            }
            usart0_recv_9F = false;
        } else {
            if (c == 0x9F) {
                usart0_recv_9F = true;
            }
        }
        if (!usart0_recv_9F) {
            fwrite(buff, 1, strlen(buff), handle_usart0);
            fflush(handle_usart0);
        }
    }
    if (xStreamBufferReceive(usart1.xBufferTxUartMessage, &c, 1, 0)) {
        // handle_uart0 =  open("/tmp/uart.txt", O_WRONLY | O_APPEND | O_CREAT, 0644);
        // write(handle_usart0, &c, 1);
        // close(handle_uart0);
        snprintf(buff, BUFF_LEN, "%02X ", c);
        if (usart1_recv_9F) {
            if (c == 0x02) {
                snprintf(buff, BUFF_LEN, "%lu 9F %02X ", (unsigned long)time(NULL), c);
            }
            if (c == 0x03) {
                snprintf(buff, BUFF_LEN, "9F %02X\r\n", c);
            }
            if (c == 0x9F) {
                snprintf(buff, BUFF_LEN, "9F %02X ", c);
            }
            usart1_recv_9F = false;
        } else {
            if (c == 0x9F) {
                usart1_recv_9F = true;
            }
        }
        if (!usart1_recv_9F) {
            fwrite(buff, 1, strlen(buff), handle_usart0);
            fflush(handle_usart0);
        }
    }
    if (xStreamBufferReceive(uart.xBufferTxUartMessage, &c, 1, 0)) {
        // write(handle_usart1, &c, 1);
        // fsync(handle_usart1);
        snprintf(buff, BUFF_LEN, "%02X ", c);
        if (uart0_recv_9F) {
            if (c == 0x02) {
                snprintf(buff, BUFF_LEN, "%lu 9F %02X ", (unsigned long)time(NULL), c);
            }
            if (c == 0x03) {
                snprintf(buff, BUFF_LEN, "9F %02X\r\n", c);
            }
            if (c == 0x9F) {
                snprintf(buff, BUFF_LEN, "9F %02X ", c);
            }
            uart0_recv_9F = false;
        } else {
            if (c == 0x9F) {
                uart0_recv_9F = true;
            }
        }
        if (!uart0_recv_9F) {
            fwrite(buff, 1, strlen(buff), handle_usart0);
            fflush(handle_usart0);
        }
    }
    if (_kbhit()) {
        uint8_t c = getc(stdin);
        xStreamBufferSend(uart.xBufferRxUartMessage, &c, 1, 0);
    }
}

void serial_uart_init(struct SERIAL* self, uint32_t uart_speed, const char c) {
    (void)self;
    (void)uart_speed;
    (void)c;
    if (self->xBufferRxUartMessage == NULL) {
        self->xBufferRxUartMessage = xStreamBufferCreate(96, 2);
    }
    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        LOG_DEF_NORMAL("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_uart0 = fopen("/tmp/uart.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_usart0_init_LIN(uint32_t uart_speed) {
    (void)uart_speed;
}

void serial_usart0_init(struct SERIAL* self, uint32_t uart_speed, const char c) {
    (void)self;
    (void)uart_speed;
    (void)c;
    if (self->xBufferRxUartMessage == NULL) {
        self->xBufferRxUartMessage = xStreamBufferCreate(96, 2);
    }
    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        LOG_DEF_NORMAL("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_usart0 = fopen("/tmp/usart0.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_usart1_init(struct SERIAL* self, uint32_t uart_speed, const char c) {
    (void)self;
    (void)uart_speed;
    (void)c;
    if (self->xBufferRxUartMessage == NULL) {
        self->xBufferRxUartMessage = xStreamBufferCreate(96, 2);
    }
    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        LOG_DEF_NORMAL("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_usart1 = fopen("/tmp/usart1.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_usart2_init(struct SERIAL* self, uint32_t uart_speed, const char c) {
    (void)self;
    (void)uart_speed;
    (void)c;
    if (self->xBufferRxUartMessage == NULL) {
        self->xBufferRxUartMessage = xStreamBufferCreate(96, 2);
    }
    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        LOG_DEF_NORMAL("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_usart2 = fopen("/tmp/usart2.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_uart_enable_rx_interrupt(void) {
}
void serial_uart_enable_tx_interrupt(void) {
}
void serial_usart0_enable_rx_interrupt(void) {
}
void serial_usart0_enable_tx_interrupt(void) {
}
void serial_usart1_enable_rx_interrupt(void) {
}
void serial_usart1_enable_tx_interrupt(void) {
}

void serial_usart2_enable_rx_interrupt(void) {
}
void serial_usart2_enable_tx_interrupt(void) {
}
