#ifdef __amd64__

#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "serials.h"

extern struct SERIAL uart;
extern struct SERIAL usart0;
extern struct SERIAL usart1;

struct SERIAL *serial = &uart;

void serial_enable_rx_interrupt(void){

}

void serial_enable_tx_interrupt(void){

}

static int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}
//
//static void set_mincount(int fd, int mcount)
//{
//    struct termios tty;
//
//    if (tcgetattr(fd, &tty) < 0) {
//        printf("Error tcgetattr: %s\n", strerror(errno));
//        return;
//    }
//
//    tty.c_cc[VMIN] = mcount ? 1 : 0;
//    tty.c_cc[VTIME] = 5;        /* half second timer */
//
//    if (tcsetattr(fd, TCSANOW, &tty) < 0)
//        printf("Error tcsetattr: %s\n", strerror(errno));
//}

int usart_fd;

static void serial_task(void *parameter){
	(void)parameter;
    /* simple noncanonical input */
    do {
        unsigned char buf[80];
        int rdlen;

        rdlen = read(usart_fd, buf, sizeof(buf) - 1);
        if (rdlen > 0) {
            unsigned char   *p;
            for (p = buf; rdlen-- > 0; p++){
                xStreamBufferSend(serial->xBufferRxUartMessage, p, 1, 5);
            }
        } else if (rdlen < 0) {
            vTaskDelay(10/portTICK_PERIOD_MS);
        }
        /* repeat read to get full message */
    } while (1);
}

static void serial_task_tx(void *parameter){
	(void)parameter;
	do{
		uint8_t c[1];
		if (xStreamBufferReceive(serial->xBufferTxUartMessage, &c[0], 1, 5)){
			int res = write(usart_fd, c, 1);
            (void)res;
		}
	}while(1);
}

void serial_hw_init(void){
    char *portname = "/dev/ttyUSB0";
    usart_fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (usart_fd < 0) {
        perror("Error opening serial\n");
    }
    /*baudrate 115200, 8 bits, no parity, 1 stop bit */
    set_interface_attribs(usart_fd, B115200);
    //set_mincount(fd, 0);                /* set to pure timed read */
    xTaskCreate( serial_task_tx, "serial_task_tx_host", configMINIMAL_STACK_SIZE+200, 0, 1, NULL);
    xTaskCreate( serial_task,  "serial_task_host", configMINIMAL_STACK_SIZE+200, 0, 1, NULL);
}

#endif
