#pragma once

#include <stdint.h>
#include <stdarg.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "stream_buffer.h"

#define LIN_MSG_LEN             4
#define LIN_BUFFER_MAX_LEN      LIN_MSG_LEN + 3
#define LIN_BUFFER_ID_INDEX     2

struct SERIAL; 

typedef void (* RX_ISR_ENA)(void);
typedef void (* TX_ISR_ENA)(void);
typedef int (* TX_CHAR)(struct SERIAL *serial, char c);
typedef void (* PUTSTRING)(struct SERIAL *serial, uint8_t *buff, int len);
typedef int  (* PRINTF)(struct SERIAL *serial, const char *__fmt, ...);
typedef void (* SERIAL_INIT)(struct SERIAL *, uint32_t, const char);
typedef int ( *VFPRINTF)(struct SERIAL *serial, const char *__restrict __fmt, va_list __arg);

struct SERIAL{
    RX_ISR_ENA rx_isr_ena;
    TX_ISR_ENA tx_isr_ena;
    TX_CHAR tx_char;
    PUTSTRING put_string;
    PRINTF printf;
    SERIAL_INIT init;
    StreamBufferHandle_t xBufferRxUartMessage;
    StreamBufferHandle_t xBufferTxUartMessage;
};

void serial_init(void);
void serial_usart0_init_LIN(uint32_t uart_speed);


