#pragma once

#include <stdint.h>

/**
 * @brief struttura contenente i dati del messaggio canbus
 * I dati sono salvati in una struttura e si può accedere ai singoli byte.
 */
typedef struct
{
	uint32_t byte0;
	uint32_t byte1;
	uint32_t byte2;
	uint32_t byte3;
} LIN_DATA;

/**
 * @brief Struttura contenente le informazioni relative ad un messaggio canbus
 * Un messaggio lin bus contiene tutte le informazioni necessarie per la sua gestione sia in
 * ricezione che tramissione.
 */
typedef struct
{
	/**
	 * byte di break del messaggio lin.
	 */
	uint32_t lin_break;
	/**
	 * byte di sync del messaggio lin.
	 */
	uint32_t lin_sync;
	/**
	 * Id del messaggio lin.
	 */
	uint32_t lin_id;
	/**
	 * Dati contenuti nel messaggio lin.
	 */
	LIN_DATA lin_data;
	/**
	 * byte di checksum del messaggio lin.
	 */
	uint32_t lin_chk;
} LIN_MESSAGE;

/**
 * @brief evento di tipo messaggio canbus ricevuto dal veicolo
 */
struct EVENT_LIN_MESSAGE
{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Messaggio canbus ricevuto.
	 */
	LIN_MESSAGE msg;
};

void event_lin_rx_message(const LIN_MESSAGE *const msg);
