#include "source_changer.h"
#include "alpine_task.h"
#include "event_list.h"
#include "events.h"
#include "logger.h"

static enum RADIO_SOURCE_TYPE current_source_data;
static enum RADIO_SOURCE_TYPE last_source_data;
static enum RADIO_SOURCE_TYPE last_requested_source_data;

static void handle_source_change(const union EVENT *const msg) {
    current_source_data = msg->as_radio_source.source_type;
}

static void handle_source_change_request(const union EVENT *const msg) {
    union EVENT event;
    event.as_external_request.generic.event = eEVENT_EXTERNAL_SOURCE;
    event.as_external_request.data.type = 0;
    if (current_source_data != last_requested_source_data) {
        last_source_data = current_source_data;
        event.as_external_request.data.data = msg->as_source_changer.source;
        last_requested_source_data = msg->as_source_changer.source;
    } else {
        event.as_external_request.data.data = last_source_data;
    }
    event_emit(&event); 
}

void source_changer_init(void) {
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_change);
    event_connect_callback(eEVENT_SOURCE_CHANGER, handle_source_change_request);
}

void event_source_changer_emit(enum RADIO_SOURCE_TYPE source) {
    union EVENT event;
    event.as_generic.event = eEVENT_SOURCE_CHANGER;
    event.as_source_changer.source = source;
    event_emit(&event);
}