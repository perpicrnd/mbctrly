#pragma once


struct EVENT_SOURCE_CHANGER{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
    enum RADIO_SOURCE_TYPE source;
};


/**
 * @brief emette una notifica di lettura di settaggi.
 */
void event_source_changer_emit(enum RADIO_SOURCE_TYPE source);
