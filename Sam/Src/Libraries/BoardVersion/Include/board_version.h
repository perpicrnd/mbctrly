
#pragma once

enum BOARD_VERSION{
    BOARD_VERSION_00,
    BOARD_VERSION_01,
};

enum BOARD_VERSION board_get_version(void);
void board_set_version(enum BOARD_VERSION version);