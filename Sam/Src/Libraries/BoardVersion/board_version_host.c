
#include "board_version.h"

static enum BOARD_VERSION bv = BOARD_VERSION_00;

enum BOARD_VERSION board_get_version(void){
    return bv;
}

void board_set_version(enum BOARD_VERSION version){
    bv = version;
}