#pragma once

#include "perpic_message_raw_simple.h"

struct PERPIC_MESSAGE_BUTTONS {
    struct PERPIC_RAW_MESSAGE_SIMPLE as_raw;
};

union PERPIC_CONTROLLER_MESSAGE {
    struct PERPIC_RAW_MESSAGE_SIMPLE as_raw;
    struct PERPIC_MESSAGE_BUTTONS as_buttons;
};
