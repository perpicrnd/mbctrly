#include "controller.h"
#include <string.h>
#include "FreeRTOS.h"
#include "buttons.h"
#include "controller_priv.h"
#include "event_list.h"
#include "perpic_message_raw_simple.h"
#include "queue.h"
#include "serials.h"
#include "task.h"
#include "timers.h"

#define CONTROLLER_QUEUE_NUM 40
#define SHARED_BUFFER_LEN 200

extern struct SERIAL usart1;
struct SERIAL *controller_uart;

static union PERPIC_CONTROLLER_MESSAGE tx_message;
static union PERPIC_CONTROLLER_MESSAGE rx_message;
static QueueHandle_t task_queue;
static TimerHandle_t msec160;
static uint8_t timeout_counter = 0;
static bool ack_received = true;
static uint8_t nack_counter = 0;

static inline void reset_counters(void) {
    taskENTER_CRITICAL();
    timeout_counter = 0;
    nack_counter = 0;
    ack_received = true;
    taskEXIT_CRITICAL();
}

static uint8_t tx_shared_buffer[SHARED_BUFFER_LEN];
static uint8_t tx_shared_buffer_pos = 0;
static inline void reset_buffer(void) {
    for (uint8_t i = 0; i < tx_shared_buffer_pos; i++) {
        tx_shared_buffer[i] = 0x00;
    }
    tx_shared_buffer_pos = 0;
}

static inline void controller_print_buffer(void) {
    for (uint8_t i = 0; i < tx_shared_buffer_pos; i++) {
        LOG_DEF_NORMAL("%02X ", tx_shared_buffer[i]);
    }
    LOG_DEF_NORMAL("\r\n");
}

static void controller_tx_message(union PERPIC_CONTROLLER_MESSAGE *msg) {
    reset_buffer();
    tx_shared_buffer[0] = 0x9F;
    tx_shared_buffer[1] = 0x02;
    tx_shared_buffer[2] = msg->as_raw.pos;
    uint8_t j = 3;
    for (uint8_t i = 0; i <= msg->as_raw.pos; i++, j++) {
        switch (msg->as_raw.data[i]) {
            case 0x9F:
                tx_shared_buffer[j] = 0x9F;
                j++;
                break;
        }
        tx_shared_buffer[j] = msg->as_raw.data[i];
    }
    tx_shared_buffer[j++] = 0x9F;
    tx_shared_buffer[j++] = 0x03;
    xTimerStart(msec160, 0);
    tx_shared_buffer_pos = j;
    controller_uart->put_string(controller_uart, tx_shared_buffer, tx_shared_buffer_pos);
}

#define ACKNACK_LEN 8
static inline void controller_tx_message_ack(void) {
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x06\x07\x9F\x03"};
    controller_uart->put_string(controller_uart, tx_buffer, ACKNACK_LEN);
}

static inline void controller_tx_message_nack(void) {
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x15\x14\x9F\x03"};
    controller_uart->put_string(controller_uart, tx_buffer, ACKNACK_LEN);
}

static void controller_tx(struct CONTROLLER_DATA *controller) {
    perpic_message_raw_simple_reset(&tx_message.as_raw);
    perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->command);
    switch (controller->command) {
        case CONTROLLER_WO_ALIVE_SECONDS:
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data0);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        case CONTROLLER_RW_ACTIVE_CONF:
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data0);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        case CONTROLLER_RO_EEPROM:
            perpic_message_raw_simple_add_uint32(&tx_message.as_raw, (intptr_t)controller->callback);
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[1]);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        case CONTROLLER_WO_EEPROM:
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[1]);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        case CONTROLLER_RO_EEPROM_PAGE:
            perpic_message_raw_simple_add_uint32(&tx_message.as_raw, (intptr_t)controller->callback);
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[1]);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        case CONTROLLER_WO_SELECTOR:
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data0);
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
        default:
            perpic_message_raw_simple_add_crc(&tx_message.as_raw);
            controller_tx_message(&tx_message);
            break;
    }
}

void handle_controller_buttons(union PERPIC_CONTROLLER_MESSAGE *msg) {
    received_button_settings(msg->as_raw.data[1]);
}

void handle_ir_power_button(union PERPIC_CONTROLLER_MESSAGE *msg) {
    received_ir_power(msg->as_raw.data[1]);
}

static inline void handle_eeprom_callback(union PERPIC_CONTROLLER_MESSAGE *msg) {
    CONTROLLER_CALLBACK_DATA read_callback;
    uint32_t data = (msg->as_raw.data[1]) | (msg->as_raw.data[2] << 8) | (msg->as_raw.data[3] << 16) | (msg->as_raw.data[4] << 24);
    read_callback = (CONTROLLER_CALLBACK_DATA)(intptr_t)data;
    for (uint8_t i = 0; i < msg->as_raw.pos; i++) {
        LOG_DEF_NORMAL("%02X ", msg->as_raw.data[i]);
    }
    LOG_DEF_NORMAL("\r\n");
    if (read_callback != NULL) {
        read_callback(msg->as_raw.data[5], msg->as_raw.data[6], &msg->as_raw.data[7]);
    }
}

enum LED_COLOR led_color;
enum LED_COLOR controller_get_led_color(void) {
    return led_color;
}

static inline void handle_led_current_conf(union PERPIC_CONTROLLER_MESSAGE *msg) {
    led_color = msg->as_raw.data[1];
}

#ifdef __amd64__

#else
static uint8_t counter = 0;
#endif

static SemaphoreHandle_t acknack_sem;
static bool clear_buffer = false;
static void controller_task(void *parameter) {
    (void)parameter;
    controller_uart->init(controller_uart, 115200, 'N');
    for (;;) {
        do {
            if (xSemaphoreTake(acknack_sem, 0)) {
                controller_tx_message(&tx_message);
                break;
            }

            if (ack_received) {
                struct CONTROLLER_DATA msg;
                if (xQueueReceive(task_queue, &msg, 0)) {
                    controller_tx(&msg);
                    ack_received = false;
                }
            }
        } while (0);

        char c;
        bool ch_recv;
        do {
            ch_recv = false;
            if (xStreamBufferReceive(controller_uart->xBufferRxUartMessage, &c, 1, 10)) {
                ch_recv = true;
                perpic_message_raw_simple_add_uint8_from_extern(&rx_message.as_raw, c);
                if (perpic_message_raw_simple_is_completed(&rx_message.as_raw)) {
                    if (perpic_message_raw_simple_is_acknack(&rx_message.as_raw)) {
                        xTimerStop(msec160, 10);
                        if (perpic_message_raw_simple_is_nack(&rx_message.as_raw)) {
                            if (nack_counter <= 3) {
                                LOG_DEF_NORMAL("NACK\r\n");
                                xSemaphoreGive(acknack_sem);
                                nack_counter++;
                            } else {
                                LOG_DEF_NORMAL("3 NACK, give up transmitting\r\n");
                                controller_print_buffer();
                                reset_counters();
                            }
                        } else {
                            reset_counters();
                        }
                    } else {
                        if (perpic_message_raw_simple_crc_check(&rx_message.as_raw) == PERPIC_CRC_SIMPLE_OK) {
                            controller_tx_message_ack();
                            switch (perpic_message_raw_simple_get_id(&rx_message.as_raw)) {
                                case CONTROLLER_RO_BUTTONS:
                                    handle_controller_buttons(&rx_message);
                                    break;
                                case CONTROLLER_RO_EEPROM:
                                case CONTROLLER_RO_EEPROM_PAGE:
                                    handle_eeprom_callback(&rx_message);
                                    break;
                                case CONTROLLER_RW_ACTIVE_CONF:
                                    handle_led_current_conf(&rx_message);
                                    break;
                                case CONTROLLER_WO_POWER:
                                    handle_ir_power_button(&rx_message);
                                    break;
                                default:
                                    LOG_DEF_NORMAL("No handlers for:\r\n");
                                    perpic_message_raw_simple_print(&rx_message.as_raw);
                                    break;
                            }
                        } else {
                            controller_tx_message_nack();
                        }
                    }
                    perpic_message_raw_simple_reset(&rx_message.as_raw);
                }
            }
        } while (ch_recv);

#ifdef __amd64__

#else
        if (counter++ == 0) {
            if (uxTaskGetStackHighWaterMark(NULL) < 100) {
                LOG_DEF_NORMAL("%s stack too low.\r\n", __PRETTY_FUNCTION__);
            }
        }
#endif
    }
}

static void timer_msec160(TimerHandle_t pxTimer) {
    (void)pxTimer;
    ///Release the semaphore for unlock the radio task.
    //Non ho un sequence number sul timeout.
    clear_buffer = true;
    if (timeout_counter < 3) {
        LOG_DEF_NORMAL("timeout\r\n");
        timeout_counter++;
        xSemaphoreGive(acknack_sem);
    } else {
        LOG_DEF_NORMAL("Too much errors without reply, give up\r\n");
        controller_print_buffer();
        reset_counters();
    }
}

static bool configured = false;
void controller_init(void) {
    if (!configured) {
        controller_uart = &usart1;
        xTaskCreate(controller_task, (char *)"ctrl_task", configMINIMAL_STACK_SIZE + 200, NULL, configMAX_PRIORITIES - 2, NULL);
        task_queue = xQueueCreate(CONTROLLER_QUEUE_NUM, sizeof(struct CONTROLLER_DATA));
        msec160 = xTimerCreate((char *)"t_160", 160 / portTICK_PERIOD_MS, pdFALSE, 0, timer_msec160);
        acknack_sem = xSemaphoreCreateBinary();
        configured = true;
    }
}

void event_controller_emit(struct CONTROLLER_DATA *controller) {
    xQueueSendToBack(task_queue, controller, 2 / portTICK_PERIOD_MS);
}