#pragma once

#define CONTROLLER_EVENT_EEPROM_DATALEN 14

typedef void (*CONTROLLER_CALLBACK_DATA)(uint8_t start, const uint8_t len, const uint8_t *data);

enum CONTROLLER_COMMAND{
    CONTROLLER_WO_ALIVE_SECONDS = 0,
    CONTROLLER_RO_BUTTONS,
    CONTROLLER_RW_LEDS,
    CONTROLLER_RW_ACTIVE_CONF,
    CONTROLLER_RO_EEPROM,
    CONTROLLER_WO_EEPROM,
    CONTROLLER_ACK = 0x06,
    CONTROLLER_NACK = 0x15,
    CONTROLLER_WO_POWER,
    CONTROLLER_RO_EEPROM_PAGE,
    CONTROLLER_WO_SELECTOR,
    CONTROLLER_FACTORY_SETTING = 0x7F,
};

struct CONTROLLER_PWM{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t white;
};

struct CONTROLLER_DATA{
    enum CONTROLLER_COMMAND command;
    CONTROLLER_CALLBACK_DATA callback;
    union{
        uint8_t data0;
        struct CONTROLLER_PWM pwm;       
        uint8_t data[CONTROLLER_EVENT_EEPROM_DATALEN]; 
    };
};

struct EVENT_CONTROLLER{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
    struct CONTROLLER_DATA data;
};

void event_controller_emit(struct CONTROLLER_DATA *controller);