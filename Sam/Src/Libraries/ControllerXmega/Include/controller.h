#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "serials.h"
#include "logger.h"

enum LED_COLOR{
    LED_COLOR_RED = 0,
    LED_COLOR_RED_DIM,
    LED_COLOR_GREEN,
    LED_COLOR_GREEN_DIM,
    LED_COLOR_BLUE,
    LED_COLOR_BLUE_DIM,
    LED_COLOR_WHITE,
    LED_COLOR_WHITE_DIM,
    LED_COLOR_ORANGE,
    LED_COLOR_ORANGE_DIM,
    LED_COLOR_YELLOW,
    LED_COLOR_YELLOW_DIM,
    LED_COLOR_COUNT
};

enum LED_COLOR controller_get_led_color(void);


uint8_t controller_read_cached_output(void);

void controller_init(void);