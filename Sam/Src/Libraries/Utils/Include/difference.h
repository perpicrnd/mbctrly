#pragma once

#define DIFF(A, B) ((A) > (B) ? ((A) - (B)) : ((B) - (A)))