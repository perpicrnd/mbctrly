#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "logger.h"
#include "serials.h"

#ifndef __amd64__
#include "asf.h"
#endif

extern uint8_t stop_printout; //Si trova in freertos_clitask.c

struct SERIAL *logger = NULL;
typedef struct{
	LOGGING_SEVERITY severity[LOGGING_COUNT];
}LOGGING_pvt;

LOGGING_pvt logging;

void logging_register(enum LOGGING log, LOGGING_SEVERITY severity){
	if (log >= LOGGING_COUNT){
		return;
	}
	logging.severity[log] = severity;
}

static inline bool ITM_Available(enum LOGGING port){
    #ifdef __amd64__
    (void)port;
    return true;
    #else
    return (ITM->TER & (1UL << port)) != 0;
    #endif
}

static inline uint32_t ITM_SendCharPort (uint32_t ch, enum LOGGING port){
#ifdef __amd64__
    (void)port;
    return printf("%c", ch);
#else
  if ((ITM->TCR & ITM_TCR_ITMENA_Msk)                  &&      /* ITM enabled */
      (ITM->TER & (1UL << port)        )                    )     /* ITM Port "port" enabled */
  {
    while (ITM->PORT[port].u32 == 0);
    ITM->PORT[port].u8 = (uint8_t) ch;
  }
  return (ch);
#endif
}

void SWO_PrintStringDef(const char *s){
    while (*s) {
        ITM_SendCharPort(*s++, 0);
    }
}

static char pcWriteBuffer[100];
void SWO_PrintStringFmt(const char *format, ...){
    va_list ap;
    va_start(ap, format);
    
    vsnprintf( pcWriteBuffer, 100, format, ap);
    va_end(ap);
    SWO_PrintStringDef(pcWriteBuffer);
}

static inline void SWO_PrintString(const char *s, enum LOGGING log) {
    while (*s) {
        ITM_SendCharPort(*s++, log);
    }
}

int logging_notification(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...){
	int retVal = 0;
	if (stop_printout > 0){
		return retVal;
	}

    if (!ITM_Available(log)){
        return retVal;
    }
        
        
	if (logging.severity[log] >= severity){
		va_list ap;
		va_start(ap, format);
        
		vsnprintf( pcWriteBuffer, 100, format, ap);
		va_end(ap);
        
		SWO_PrintString(pcWriteBuffer, log);
	}
	return retVal;
}


static const char * const log_normal 			= "NORMAL";
static const char * const log_info 				= "INFO";
static const char * const log_error 			= "ERROR";
static const char * const log_warning 			= "WARNING";
static const char * const log_debug 			= "DEBUG";

static const char * const log_default 			= "DEFAULT";
static const char * const log_uart				= "UART";
static const char * const log_event				= "EVENT";
static const char * const log_can 				= "CAN";
static const char * const log_repeater 				= "REPEATER";
static const char * const log_adc 				= "ADC";

static BaseType_t change_logging_type( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString ){
	(void)pcWriteBuffer;
	(void)xWriteBufferLen;
	int8_t *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;

	pcParameter1 = (int8_t *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParameter1StringLength);
	pcParameter2 = (int8_t *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &xParameter2StringLength);

	pcParameter1[ xParameter1StringLength ] = 0x00;
	pcParameter2[ xParameter2StringLength ] = 0x00;
	LOGGING_SEVERITY verb = LOGGING_SEVERITY_NORMAL;
	enum LOGGING log = LOGGING_COUNT;

	if (strcasecmp((const char *)pcParameter1, (const char *)log_default) == 0){
		log = LOGGING_DEFAULT;
	}else if (strcasecmp((const char *)pcParameter1, (const char *)log_can) == 0){
		log = LOGGING_CANBUS;
	}else if (strcasecmp((const char *)pcParameter1, (const char *)log_uart) == 0){
		log = LOGGING_UART;
	}else if (strcasecmp((const char *)pcParameter1, (const char *)log_event) == 0){
		log = LOGGING_EVENT;
	}else if (strcasecmp((const char *)pcParameter1, (const char *)log_adc) == 0){
		log = LOGGING_ADC;
	}else if (strcasecmp((const char *)pcParameter1, (const char *)log_repeater) == 0){
		log = LOGGING_REPEATER;
	}

	if (strcasecmp((const char *)pcParameter2, (const char *)log_normal) == 0){
		verb = LOGGING_SEVERITY_NORMAL;
	}else if (strcasecmp((const char *)pcParameter2, (const char *)log_info) == 0){
		verb = LOGGING_SEVERITY_INFO;
	}else if (strcasecmp((const char *)pcParameter2, (const char *)log_error) == 0){
		verb = LOGGING_SEVERITY_ERROR;
	}else if (strcasecmp((const char *)pcParameter2, (const char *)log_warning) == 0){
		verb = LOGGING_SEVERITY_WARNING;
	}else if (strcasecmp((const char *)pcParameter2, (const char *)log_debug) == 0){
		verb = LOGGING_SEVERITY_DEBUG;
	}
	logging_register(log, verb);
	LOG_DEF_NORMAL("Logger %s:%d now set as %s:%d\r\n", pcParameter1, log, pcParameter2, verb);
	stop_printout = 0;
	return pdFALSE;
}


static const CLI_Command_Definition_t xVerboseCommand =
{
	"verbose",
	"\r\nverbose:\r\nverbose [DEFAULT|CAN|UART|EVENT|REPEATER|ADC] [NORMAL|INFO|ERROR|WARNING|DEBUG]\r\n\r\n",
	change_logging_type,
    2
};

void logger_init(void){
	logging_register(LOGGING_DEFAULT, LOGGING_SEVERITY_NORMAL);
	logging_register(LOGGING_CANBUS, LOGGING_SEVERITY_NORMAL);
	logging_register(LOGGING_EVENT, LOGGING_SEVERITY_NORMAL);
	logging_register(LOGGING_UART, LOGGING_SEVERITY_DEBUG);
	logging_register(LOGGING_REPEATER, LOGGING_SEVERITY_NORMAL);
	logging_register(LOGGING_ADC, LOGGING_SEVERITY_NORMAL);
	FreeRTOS_CLIRegisterCommand( &xVerboseCommand );
	LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
};
