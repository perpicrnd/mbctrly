#pragma once

#include <stdarg.h>

enum LOGGING{
	LOGGING_DEFAULT = 0,
	LOGGING_CANBUS,
	LOGGING_UART,
	LOGGING_EVENT,
    LOGGING_REPEATER,
    LOGGING_ADC,
	LOGGING_COUNT
};

typedef enum{
	LOGGING_SEVERITY_ERROR,
	LOGGING_SEVERITY_NORMAL,
	LOGGING_SEVERITY_INFO,
	LOGGING_SEVERITY_WARNING,
	LOGGING_SEVERITY_DEBUG,
}LOGGING_SEVERITY;

void logger_init(void);
void logging_register(enum LOGGING log, LOGGING_SEVERITY severity);
int logging_notification(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...)__attribute__ ((format (gnu_printf, 3, 4))); 
void SWO_PrintStringDef(const char *s);
void SWO_PrintStringFmt(const char *format, ...);

#define LOG_CAN_DEBUG(...) 		logging_notification(LOGGING_CANBUS, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_CAN_WARNING(...)	logging_notification(LOGGING_CANBUS, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_CAN_ERROR(...)		logging_notification(LOGGING_CANBUS, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_CAN_NORMAL(...)		logging_notification(LOGGING_CANBUS, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)

#define LOG_DEF_DEBUG(...) 		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_DEF_WARNING(...)	logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_DEF_ERROR(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_DEF_INFO(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_DEF_NORMAL(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)

#define LOG_UART_DEBUG(...) 	logging_notification(LOGGING_UART, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_UART_WARNING(...)	logging_notification(LOGGING_UART, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_UART_ERROR(...)		logging_notification(LOGGING_UART, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_UART_INFO(...)		logging_notification(LOGGING_UART, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_UART_NORMAL(...)	logging_notification(LOGGING_UART, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)

#define LOG_REPEATER_DEBUG(...) 	logging_notification(LOGGING_REPEATER, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_REPEATER_WARNING(...)	logging_notification(LOGGING_REPEATER, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_REPEATER_ERROR(...)		logging_notification(LOGGING_REPEATER, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_REPEATER_INFO(...)		logging_notification(LOGGING_REPEATER, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_REPEATER_NORMAL(...)	logging_notification(LOGGING_REPEATER, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)

#define LOG_EVENT_DEBUG(...) 	logging_notification(LOGGING_EVENT, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_EVENT_WARNING(...)	logging_notification(LOGGING_EVENT, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_EVENT_ERROR(...)	logging_notification(LOGGING_EVENT, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_EVENT_INFO(...)		logging_notification(LOGGING_EVENT, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_EVENT_NORMAL(...)	logging_notification(LOGGING_EVENT, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)


#define LOG_ADC_DEBUG(...) 		logging_notification(LOGGING_ADC, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_ADC_WARNING(...)	logging_notification(LOGGING_ADC, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_ADC_ERROR(...)		logging_notification(LOGGING_ADC, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_ADC_INFO(...)		logging_notification(LOGGING_ADC, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_ADC_NORMAL(...)		logging_notification(LOGGING_ADC, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)

