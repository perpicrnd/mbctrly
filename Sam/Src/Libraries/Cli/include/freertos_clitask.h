#pragma once

/**
 *	/defgroup CLI_TASK
 *	@brief Task di gestione della seriale
 *	@{
 */

/**
 * @brief Inizializza il task di gestione della seriale.
 */
void freertos_cli_init(void);

/**
 * @}
 */
