#include "watchdog.h"
#include "asf.h"

void watchdog_init(void){
	uint32_t timeout_value = wdt_get_timeout_value(4000 * 1000,BOARD_FREQ_SLCK_XTAL);
	uint32_t wdt_mode =  WDT_MR_WDRSTEN | WDT_MR_WDRPROC | WDT_MR_WDDBGHLT;
	wdt_init(WDT, wdt_mode, timeout_value, timeout_value);
	wdt_restart(WDT);
}

void watchdog_reset(void){
	wdt_restart(WDT);
}
