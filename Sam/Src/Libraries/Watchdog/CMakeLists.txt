cmake_minimum_required(VERSION 3.0)

SET(LIBRARY_NAME watchdog_lib)

if (CMAKE_CROSSCOMPILING)
	set(SOURCES watchdog_sam.c)
else()
	set(SOURCES watchdog_host.c)
endif()	


set(SOURCES ${SOURCES}
	watchdog.c

)
add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries(${LIBRARY_NAME} freertos_cli)
if (CMAKE_CROSSCOMPILING)
	target_link_libraries (${LIBRARY_NAME} asf_library )
endif()

target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)