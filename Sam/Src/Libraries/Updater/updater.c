#include <FreeRTOS.h>
#include <semphr.h>
#include <stddef.h>
#include <task.h>
#include <timers.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "version.h"

#include "updater.h"
#include "wisker.h"
#include "watchdog.h"
#include "serials.h"
#include "logger.h"
#include "event_list.h"
#include "settings.h"
#include "ignition.h"

extern const char interface_code[];
#define BUFF_RX_SIZE 512	//Dimensione buffer di ricezione da PC
#define SAM_PAGE_SIZE 256	//Dimesione pagina flash microprocessore
#define MASK_PAGE_SIZE 0xFF
#define MAX_BYTES_RX 150	//Dimesione del comando ricevuto (in hex)
#define MAX_BYTES_TX 150	//Dimesione dei dati (in ASCII) da inviare al PC
#define MAX_BLOCCHI 32		//Numero di segmenti in cui � diviso il programma
#define BLOCCHI_PER_RECORD 5 //numero di blocchi in ogni record
#define DATI_PER_RECORD 128	//Dati da aggiornare trasferiti in ogni record

typedef struct {
	uint16_t numBlocchi;
	uint16_t numBlocchiEEP;
	uint16_t cntBlocchi;
	uint32_t checksum;
	uint16_t numRecordFile;
	uint16_t cntRXRecord;
	uint16_t keyCrypt;
	uint8_t rndVal[2];
	uint16_t addrBlocco[MAX_BLOCCHI][4];
	uint8_t decrypt[16];
} VAL_ENVIR;

static VAL_ENVIR valEnvir = { decrypt: { 0x03, 0x0D, 0x07, 0x0E, 0x02, 0x0F,
		0x08, 0x0B, 0x05, 0x00, 0x04, 0x06, 0x09, 0x0C, 0x0A, 0x01 } };

typedef struct {
	uint32_t BankWrite;
	uint32_t addrPageFlash;			//Indirizzo inizio pagina flash in elaborazione
	char pageFlash [SAM_PAGE_SIZE];	//Pagina da trasferire in flash
	uint16_t addrHigh;
	uint16_t addrLow;				//16 bit low indirizzo flash in cui scrivere i dati
	uint32_t Act_Addr_Free;			//Attuale indirizzo puntato nella flash
	uint16_t Idx_Act_Blocco;		//Indice del blocco attualmente in uso
	bool Fl_First_Record;			//Si aspetta di ricevere il primo record del blocco
	uint32_t End_Blocco;			//Primo indirizzo libero alla fine del blocco in elaborazione
	uint16_t Index_Mem;				//Indice buffer di transito
	bool Fl_End_RX_Data;			// Flag di fine trasferimento dati per aggiornamento
	uint16_t checkParziale;
	uint16_t numByteDati;
} MICRO;
static MICRO micro;
#define SEGMENT 0					//Indice parte alta indirizzo
#define START_ADDR_BLOCCO 1			//Indice dell'indirizzo (low) del blocco
#define LEN_DATI_BLOCCO	2			//Indice del numero di byte che compongono il blocco

volatile uint16_t pos = 0;
volatile char recvBuffer[BUFF_RX_SIZE];			//Buffer di ricezione da PC

//static char productName[10];			//Nome del prodotto
//static char productCode[10];			//Codice associato al prodotto
static uint16_t numBytesRX;			//Num byte utili del comando
static char strHex[6]; 				//Substringa estratta (da convertire)
static char **endPtr;
static uint8_t bytesRX[MAX_BYTES_RX];//Record (in hex) ricevuto dal PC (senza STX - ETX - LF)
static char strTX[MAX_BYTES_TX];		//Recod (parte dati) da inviare al PC
static bool go_on = false;

static SemaphoreHandle_t xSemaphore;

extern struct SERIAL uart;

static struct SERIAL *updater_uart = NULL;

extern QueueHandle_t xQueueRxUartMessage;
extern QueueHandle_t xQueueTxUartMessage;

void Eval_Param_Blocco (void);
void leggiPaginaFlash(void);

void elaboraComando(void);
//Formato record trasmesso: XON Num D0..Dn Ceck XOFF LF
// XON = 0x02
// Num = 3 bytes (H-M-L) Numero Dati da D0 a Dn
// Check = 2 bytes (H-L) Somma + 1 dei bytes da Num a Dn (somma del byte ASCII e non del valore che rappresenta - esclusi XON XOFF LF e Check)
// XOFF = 0x03
// LF = 0x0A - Inserito per poter utilizzare in ricezione ReadLine()
// Tutti i dati sono in HEX e rappresentati con caratteri ASCII (esluso XON XOFF e LF)
void trasmeRisposta (void);
void trasmeRisposta (void){
	uint8_t lenRecordTX;
	char bytesOut[200];
	char locStr[10];
	uint32_t checkSum;
	lenRecordTX = strlen(strTX);
	bytesOut[0] = 0x02;															// STX
	sprintf(locStr,"%03X",lenRecordTX);
	bytesOut[1] = locStr[0]; bytesOut[2] = locStr[1]; bytesOut[3] = locStr[2];	// Lunghezza dati record
	checkSum = 1 + bytesOut[1] + bytesOut[2] + bytesOut[3];	//Init check
	uint16_t j=4;
	for (uint8_t i = 0; i < lenRecordTX; i++,j++){
		bytesOut[j] = strTX[i];												// Corpo dati
		checkSum += bytesOut[j];
	}
	sprintf(locStr,"%04X",(uint16_t) (checkSum & 0xFFFF));						// Checksum
	for (uint8_t i = 0; i < 4;i++,j++) {
		bytesOut[j] = locStr[i];
	}
	bytesOut[j++] = 0x03;
	bytesOut[j++] = 0x0A;								// ETX
    updater_uart->put_string(updater_uart, (uint8_t *)bytesOut, lenRecordTX + 10);
}
static inline void getSubstringHex(volatile char *orig, char *dest, uint16_t begin, uint8_t Len);
static inline void getSubstringHex(volatile char *orig, char *dest, uint16_t begin,uint8_t Len) {
	uint16_t i;
	for (i = 0; i < Len; i++){
		dest[i] = orig[begin+i];
	}
	dest[i] = 0;
}

static inline void clear_buffer(void);
static inline void clear_buffer(void){
	recvBuffer[0] = 0;
	go_on = false;
	return;
}

SemaphoreHandle_t recv_semaphore;
static void firmware_task(void *parameter);
static void firmware_task(void *parameter) {
	(void) parameter;
	go_on = false;
	char c;
	uint16_t hand_pos = 0;
	bool msg_completed = false;
	for (;;) {
		go_on = false;
        event_ignition_emit(IGNITION_ON);
		wisker_reset(MODULE_VEHICLE);
		wisker_reset(MODULE_UART_LOWLEVEL);
		watchdog_reset();
        if (xStreamBufferReceive(updater_uart->xBufferRxUartMessage, &c, 1, 10)){
			if (recvBuffer[0] != 0x02){
				hand_pos = 0;
			}
            recvBuffer[hand_pos] = c;
			if (hand_pos >= BUFF_RX_SIZE){
				hand_pos = 0;
			}
			
			if (recvBuffer[hand_pos++]==0x0A){
				msg_completed = true;
				pos = hand_pos;
				hand_pos = 0;

			}
		}
		if (msg_completed){
			if (recvBuffer[0]==0x02){
				if (recvBuffer[pos-1]==0x0A){
					msg_completed = false;
					//Ho un messaggio completo nel buffer comprensivo di 0x02 iniziale e 0x10 finale.
					uint16_t i, j;
					uint32_t Check_Sum;
					Check_Sum = 1 + recvBuffer[1] + recvBuffer[2] + recvBuffer[3];
					getSubstringHex(recvBuffer, strHex, 1, 3);
					numBytesRX = (uint16_t) strtol(strHex, endPtr, 16);
					if (numBytesRX == pos - 10) {
						for (j = 0, i = 4; j < numBytesRX; i++, j++) {
							Check_Sum += recvBuffer[i];
						}
						if (recvBuffer[i + 4] == 0x03) {
							// Verifica il check dei dati ricevuti
							getSubstringHex(recvBuffer, strHex, i, 4);
							if ((strtol(strHex, endPtr, 16) & 0xFFFF) == (Check_Sum & 0xFFFF)) {
								elaboraComando();
							}
						} else {
							clear_buffer();
							// Errore -- Non riconosciuto XOFF -- Non inviare risposta
						}
					} else {
						clear_buffer();
						// Errore -- Lunghezza dati errata -- Non inviare risposta
					}
				}
			}
		}
	}
}



uint8_t convSubstring2Hex(volatile char *orig, uint16_t begin);
uint8_t convSubstring2Hex(volatile char *orig, uint16_t begin) {

	uint8_t i;
	char park[3];
	for (i = 0; i < 2; i++) {
		park[i] = orig[begin++];
	}
	park[i] = 0;
	return (uint8_t) (strtol(park, endPtr, 16));
}
void memorizzaBlocchi(uint16_t i, volatile char *Dati_RX);
void memorizzaBlocchi(uint16_t i, volatile char *Dati_RX) {
	(void) Dati_RX;
	uint16_t key[3] = { 0x63, 0x28, 0xB3 };
	uint16_t j;
	uint16_t kk;
	do {
		j = 0;
		do {
			for (kk = 0; kk < 3; kk++, i += 4) {
				getSubstringHex(recvBuffer, strHex, i, 4);
				valEnvir.numRecordFile = (uint16_t) (strtol(strHex, endPtr, 16)
						& 0xFFFF);
				valEnvir.addrBlocco[valEnvir.cntBlocchi][kk] =
						((uint16_t) (strtol(strHex, endPtr, 16) & 0xFFFF))
								^ key[kk];
			}
			valEnvir.addrBlocco[valEnvir.cntBlocchi][kk] = convSubstring2Hex(
					recvBuffer, i);
			i += 2;
			valEnvir.cntBlocchi++;
			j++;
		} while ((valEnvir.cntBlocchi < valEnvir.numBlocchi)&& (j < BLOCCHI_PER_RECORD));
	} while ((valEnvir.cntBlocchi < valEnvir.numBlocchi)&& (j < BLOCCHI_PER_RECORD));
}
typedef union{
	uint32_t as_32;
	uint8_t as_8;
	char as_c[4];
}DWordUnion;

void scriviPaginaFlash(void);
void scriviPaginaFlash(void){
	uint32_t ul_rc;
	ul_rc = updater_flash_write_page(&micro.addrPageFlash, micro.pageFlash, 256);
	(void)ul_rc;
	micro.addrPageFlash += SAM_PAGE_SIZE;
	micro.Index_Mem = 0;		// Azzera indice del buffer di transito
}

static bool factory_done = false;
static TimerHandle_t timer_reset;
void vTimerReset(TimerHandle_t pxTimer);
void vTimerReset(TimerHandle_t pxTimer){
	(void)pxTimer;
	LOG_DEF_NORMAL("Reboot\r\n");
    if (!factory_done){
        settings_factory_reset();
        factory_reset_activate();
        factory_done = true;
    }else{
	    restart_system();
    }
}

void elaboraComandoNonCriptatoNumerico(uint16_t i, uint16_t j);
void elaboraComandoNonCriptatoNumerico(uint16_t i, uint16_t j) {
	char loc_str[30];
	uint8_t architet;
	uint8_t id_size_flash;
	uint32_t cntBytes;			//Counter bytes processati nel controllo flash a fine scrittura
	uint32_t checksum_flash;
//	DWordUnion tmpData;
//	uint32_t ul_rc;
//	uint32_t *ptrAddReadFlash = NULL;	
		
	for (; (i << 1) < numBytesRX; i++, j += 2) {
		bytesRX[i] = convSubstring2Hex(recvBuffer, j);
	}

	switch (bytesRX[1]) {
	case 1:						// Richiesta Info
		// Determina il banco da cui si effettua il boot e calcola l'offset per l'indirizzo di accesso alla flash
		{
			uint8_t bank;
			if (updater_active_bank(&bank)){
				if (bank) {
				    LOG_DEF_NORMAL("Banco 1 attivo\r\n");
					micro.BankWrite = 0;	// Il boot avviene dal banco 1 ==> si dovrà scrivere sul banco 0
				}else {
				    LOG_DEF_NORMAL("Banco 0 attivo\r\n");
					micro.BankWrite = 1;	// Il boot avviene dal banco 0 ==> si dovrà scrive sul banco 1
				}
				loc_str[0] = '6';
				loc_str[1] = '0';
				loc_str[2] = ':';
				loc_str[3] = '\0';
				strcat(strTX, loc_str);
				sprintf(loc_str, "%c%c%c:", interface_code[1], interface_code[2], interface_code[3]);
				strcat(strTX, loc_str);
				sprintf(loc_str, "%X:",firmware_car_number());
				strcat(strTX, loc_str);
				sprintf(loc_str,"%X:",hardware_version());
				strcat(strTX, loc_str);
				sprintf(loc_str,"%X:",hardware_release());
				strcat(strTX, loc_str);
				// Estrai dal Chip ID Register l'identifier del micro (SAM3AxC = 0x83 -- SAM3XxC = 0x84)
				// Determina dimensione flash (0x09 = 128K -- 0x0A = 256K)
				architet = updater_get_architecture();
				id_size_flash = updater_get_flash_size_id();
#ifdef __amd64__
				sprintf(loc_str, "%X:%X:%X:" ,architet,id_size_flash,micro.BankWrite);
#else
				sprintf(loc_str, "%X:%X:%lX:" ,architet,id_size_flash,micro.BankWrite);
#endif
				strcat(strTX, loc_str);
			}
			else {
				strcat(strTX, "0E");	// Errore in fase di gestione GPNVM
			}
		}
		break;
	case 2:						// Attiva procedura di scrittura flash
		valEnvir.rndVal[0] = bytesRX[2] ^ 0x59;
		valEnvir.rndVal[1] = bytesRX[4] ^ 0x1A;
		valEnvir.checksum = ((bytesRX[3] ^ valEnvir.rndVal[1]) << 8) + (bytesRX[5] ^ valEnvir.rndVal[0]);
		valEnvir.keyCrypt = ((valEnvir.rndVal[0] ^ 0x6C) << 8) + (valEnvir.rndVal[1] ^ 0xD7);
		getSubstringHex(recvBuffer, strHex, 20, 4);
		valEnvir.numBlocchiEEP = (uint16_t) (strtol(strHex, endPtr, 16) & 0xFFFF) ^ valEnvir.keyCrypt;
		getSubstringHex(recvBuffer, strHex, 26, 4);
		valEnvir.numRecordFile = (uint16_t) (strtol(strHex, endPtr, 16) & 0xFFFF) ^ valEnvir.keyCrypt;
		valEnvir.cntRXRecord = -1;
		micro.Idx_Act_Blocco = 0;		//Indice del blocco attualmente in uso
		Eval_Param_Blocco ();			//Calcola i parametri del blocco e init index buffer di transito
		micro.Fl_End_RX_Data = false;	// Azzera flag di fine trasferimento dati per aggiornamento
		micro.checkParziale = 0;
		// L'identifier del micro ed il size della flash sono stati già determinati
		strcat(strTX, "62");
		break;
	case 3:	// Attiva procedura di scrittura mappa dei blocchi di memoria programma
		getSubstringHex(recvBuffer, strHex, 8, 4);
		valEnvir.numBlocchi = (uint16_t) strtol(strHex, endPtr, 16);
		valEnvir.cntBlocchi = 0;
		memorizzaBlocchi(12, recvBuffer);
		strcat(strTX, "63");
		break;
	case 4:					// Scrittura mappa dei blocchi di memoria programma
		memorizzaBlocchi(8, recvBuffer);
		strcat(strTX, "64");
		break;
	case 5:						// Fine trasferimento dati (scrittura flash)
		if (micro.Fl_End_RX_Data == true){
			// Verifica corretta scrittura flash tramite controllo del checksum globale
			checksum_flash = 0;
			for (micro.Idx_Act_Blocco = 0; micro.Idx_Act_Blocco < valEnvir.numBlocchi; micro.Idx_Act_Blocco++){
				Eval_Param_Blocco();
				micro.addrLow = valEnvir.addrBlocco[micro.Idx_Act_Blocco][START_ADDR_BLOCCO];
				micro.addrPageFlash = (uint32_t)(valEnvir.addrBlocco[micro.Idx_Act_Blocco][SEGMENT] << 4) + (micro.addrLow & 0xFF00);
				cntBytes = 0;
				while (cntBytes < (valEnvir.addrBlocco[micro.Idx_Act_Blocco][LEN_DATI_BLOCCO])+(uint32_t)1) {
					leggiPaginaFlash();
					micro.Index_Mem = 0;
					micro.addrPageFlash += SAM_PAGE_SIZE;
					if ((micro.addrLow & MASK_PAGE_SIZE) != 0) {	//Indirizzo allineato alla pagina?
						micro.Index_Mem = micro.addrLow & 0xFF;		//NO
					}
					for (i = micro.Index_Mem; (i < SAM_PAGE_SIZE) && (micro.addrLow < micro.End_Blocco); i++) {
						checksum_flash += micro.pageFlash[i];
						micro.addrLow++;
						cntBytes++;
					}
				}
			}

			if (valEnvir.checksum == (uint16_t)(checksum_flash & 0xFFFF)) {
				bool esito = updater_change_boot_sector(micro.BankWrite);
				// Si deve cambiare banco di boot
				if (esito) {
					strcat(strTX, "66");		//" Scrittura flash avvenuta correttamente;
					xTimerStart(timer_reset, 10);
				}else {
					strcat(strTX, "0E");	// Errore in fase di gestione GPNVM
				}
			}else {
				strcat(strTX, "05");	// Errore in fase di verifica flash (Check errato)
			}
		}
		else {
			strcat(strTX, "0B");		// Ricevuto "Fine aggiornamento" senza aver completato il trasferimento di tutti i blocchi
		}
		break;
	default:					// Comando sconosciuto
		strcat(strTX, "01");
		break;
	}

}
void Eval_Param_Blocco (void){			//Calcola i parametri del blocco
	//Primo indirizzo libero alla fine del blocco in elaborazione
	micro.End_Blocco = valEnvir.addrBlocco[micro.Idx_Act_Blocco][START_ADDR_BLOCCO] + valEnvir.addrBlocco[micro.Idx_Act_Blocco][LEN_DATI_BLOCCO]+(uint32_t)1;
	micro.Act_Addr_Free = valEnvir.addrBlocco[micro.Idx_Act_Blocco][START_ADDR_BLOCCO];
	micro.Fl_First_Record = true;		//Si aspetta di ricevere il primo record del blocco
	micro.Index_Mem = 0;				//Inizializza indice buffer di transito
}

void leggiPaginaFlash(void){
	uint32_t *ptrAddReadFlash = NULL;
	DWordUnion tmpData;
	uint16_t i, idxMem;
	ptrAddReadFlash = (uint32_t *)(uintptr_t)micro.addrPageFlash;
	idxMem = 0;
	for (i= 0; i < 64; i++, ptrAddReadFlash++){
		tmpData.as_32 = (uint32_t)(*ptrAddReadFlash);
		micro.pageFlash[idxMem++] = tmpData.as_c[0];
		micro.pageFlash[idxMem++] = tmpData.as_c[1];
		micro.pageFlash[idxMem++] = tmpData.as_c[2];
		micro.pageFlash[idxMem++] = tmpData.as_c[3];
	}
}

static uint16_t decryptRecord (void);
static uint16_t decryptRecord (void){
	uint16_t n = 0;
	uint16_t cod_err = 0;
	uint16_t i;  
	uint8_t B_Crypt[2];
	uint16_t position = 8;
	if (bytesRX[1] == 8) {
		micro.numByteDati = (numBytesRX - 7) / 2;
		getSubstringHex(recvBuffer, strHex, position, 3);
		if ((++valEnvir.cntRXRecord)== ((uint16_t) strtol(strHex, endPtr, 16) & 0xFFF)) {
			position += 3;
			if (micro.Fl_End_RX_Data == true) {
				strcat(strTX, "0A");		// Ricevuto record dopo il fine trasferimento dati
				cod_err = 0x0A;
			}
		}
		else {
			strcat(strTX, "03");	// "Record non consecutivo"
			cod_err = 0x03;
		}	
	}
	else {
		micro.numByteDati = (numBytesRX - 4) / 2;
	}
	if (cod_err == 0) {
		for (i = 0; i < micro.numByteDati; i++, position += 2) {
			B_Crypt[0] = (convSubstring2Hex(recvBuffer, position) ^ valEnvir.rndVal[n & 1])^ n;
			B_Crypt[1] = (B_Crypt[0] & 0xF0) >> 4;
			B_Crypt[0] = B_Crypt[0] & 0x0F;
			bytesRX[i+2] = (valEnvir.decrypt[B_Crypt[0]] << 4) + valEnvir.decrypt[B_Crypt[1]];
			n++;
		}
	}
	return cod_err;
}

void elaboraComandoCriptatoNumerico5(void);
void elaboraComandoCriptatoNumerico5(void){
	strcat(strTX, "01");		//Comando non supportato -- " Scrittura E2P";
}
void elaboraComandoCriptatoNumerico6(void);
void elaboraComandoCriptatoNumerico6(void){
	uint16_t cod_err = 0;
	micro.addrHigh = (uint16_t)(bytesRX[2] << 8) + bytesRX[3];
	if (micro.Fl_First_Record) {				// Si aspetta il primo record ?
		if (micro.addrHigh != valEnvir.addrBlocco[micro.Idx_Act_Blocco][SEGMENT]){
			cod_err = 1;
		}
	}
	else {
		if (micro.Index_Mem > 0) {
			for (; micro.Index_Mem < SAM_PAGE_SIZE; micro.Index_Mem++){
				micro.pageFlash[micro.Index_Mem] = 0xFF; // Fill restante pagina della flash
			}
			// Micro: Scrivi pagina flash e azzera indice del buffer di transito
			scriviPaginaFlash();
		}
		micro.Idx_Act_Blocco++;
		if (valEnvir.numBlocchi != micro.Idx_Act_Blocco) {	// Considerati tutti blocchi ?
			Eval_Param_Blocco();								// NO ==> Predisponi per il prossimo blocco
			if (micro.addrHigh != valEnvir.addrBlocco[micro.Idx_Act_Blocco][SEGMENT]){
				cod_err = 1;
			}
		}
		else {
			micro.Fl_End_RX_Data = true;		// Non vi sono altri blocchi da gestire
		}
	}
	if (cod_err == 0) {
		strcat(strTX, "66");		//" Impostato segmento esteso";
	}
	else {
		strcat(strTX, "0C");	// Il segmento esteso non coincide con l'inizio del blocco
	}
}

uint16_t elaboraComandoCriptatoNumerico8(void);
uint16_t elaboraComandoCriptatoNumerico8(void){
	uint16_t n = 0, cod_err = 0;
	uint16_t i, j, actCheck;
	uint16_t rx_index;
	char loc_str[10];

	for (j = 4; j < micro.numByteDati; j++) {
		micro.checkParziale += (uint16_t)bytesRX[j];
	}
	micro.checkParziale &= 0xFFFF;
	actCheck = (uint16_t)(bytesRX[micro.numByteDati] << 8) + (uint16_t)bytesRX[micro.numByteDati+1];
	if (actCheck != micro.checkParziale) {
		cod_err = 0x04;		//Check parziale errato
	}
	if (cod_err == 0) {
		micro.addrLow = (bytesRX[2] << 8) + bytesRX[3];
		rx_index = micro.numByteDati - 4;
		if (micro.Fl_First_Record) {				// Si aspetta il primo record ?
			if (micro.addrLow == valEnvir.addrBlocco[micro.Idx_Act_Blocco][START_ADDR_BLOCCO]) {
			    LOG_DEF_NORMAL("AB\r\n");
				micro.Fl_First_Record = false;
				micro.addrPageFlash = (uint32_t)(valEnvir.addrBlocco[micro.Idx_Act_Blocco][SEGMENT] << 4) + (uint32_t)(micro.addrLow & 0xFF00);
				if ((micro.addrLow & MASK_PAGE_SIZE) != 0) {	// SI ==> Indirizzo allineato alla pagina ?
					// NO ==> Leggi pagina dalla flash
					leggiPaginaFlash();
					micro.Index_Mem = micro.addrLow & MASK_PAGE_SIZE; // Imposta indice buffer
				}
			}
			else {
			    LOG_DEF_NORMAL("Err. 7\r\n");
				cod_err = 7;	// Indirizzo di start blocco diverso da quello del primo record
			}
		}
	}
	if (cod_err == 0) {
		for (i = 0, n= 4; i < rx_index; i++,micro.Act_Addr_Free++,n++) {
			micro.pageFlash[micro.Index_Mem++] = bytesRX[n];	// Trasferisci dati nel buffer
			if (micro.Index_Mem == SAM_PAGE_SIZE) {	// Pagina completa ?
				// Micro: Scrivi pagina flash e azzera indice del buffer di transito
				scriviPaginaFlash();
			}
		}
		if (DATI_PER_RECORD != rx_index) {					// Record incompleto (lunghezza minore del previsto)
			if (micro.Act_Addr_Free == micro.End_Blocco) {
				if (micro.Index_Mem > 0) {
				    LOG_DEF_NORMAL("BB\r\n");
					for (; micro.Index_Mem < SAM_PAGE_SIZE; micro.Index_Mem++){
						micro.pageFlash[micro.Index_Mem] = 0xFF; // Fill restante pagina della flash
					}
					// Micro: Scrivi pagina flash e azzera indice del buffer di transito
					scriviPaginaFlash();
				}
			}
			else {
			    LOG_DEF_NORMAL("Err. 8\r\n");
				cod_err = 8;	// Ad un record incompleto non corrisponde un fine blocco
			}
		}
	}
	if (cod_err == 0) {
		if (micro.Act_Addr_Free == micro.End_Blocco){
		    LOG_DEF_NORMAL("CB\r\n");
			micro.Idx_Act_Blocco++;
			if (valEnvir.numBlocchi != micro.Idx_Act_Blocco) {	// Considerati tutti blocchi ?
				LOG_DEF_NORMAL("CC\r\n");
				Eval_Param_Blocco();				// SI ==> Predisponi per il prossimo blocco
			}
			else {
				micro.Fl_End_RX_Data = true;
			}
		}
	}
	switch (cod_err) {
	case 0:
		strcat(strTX, "68");					//" Acquisititi i dati all'indirizzo passato nel record";
		sprintf(loc_str,"%03X",valEnvir.cntRXRecord);
		strcat(strTX, loc_str);
		break;
	case 4:
		strcat(strTX, "04"); break;	// Check parziale errato 
	case 7:
		strcat(strTX, "07"); break;	// Indirizzo di start blocco diverso da quello del primo record
	case 8:
		strcat(strTX, "08"); break;	//Ad un record incompleto non corrisponde un fine blocco

	} 
	return cod_err;

}
void elaboraComandoCriptatoNumerico9(void);
void elaboraComandoCriptatoNumerico9(void){
	if (micro.Index_Mem > 0) {
		// Micro: Scrivi pagina flash e azzera indice del buffer di transito
		scriviPaginaFlash();
	}
	strcat(strTX, "69");		//" Impostati i registri CS e IP di start programma";
}
void elaboraComandoCriptatoNumerico(void);
void elaboraComandoCriptatoNumerico(void) {
	//I comandi criptati iniziano tutti con un numero univoco. Possono essere di tipi differenti.
	if ( decryptRecord() == 0 ){
		switch(bytesRX[1]){
			case 0x05:
				elaboraComandoCriptatoNumerico5();
				break;
			case 0x06:
				elaboraComandoCriptatoNumerico6();
				break;
			case 0x08:
				elaboraComandoCriptatoNumerico8();
				break;
			case 0x09:
				elaboraComandoCriptatoNumerico9();
				break;
			default:
				strcat(strTX, "01");
				LOG_DEF_NORMAL("Comando non gestito\r\n");
				break;
		}
	}
}
void elaboraComando() {
	uint16_t i;
	uint16_t j;
	for (i = 0, j = 4; i < 2; i++, j+=2) {
		bytesRX[i] = convSubstring2Hex(recvBuffer, j);
	}
	getSubstringHex(recvBuffer, strTX, 4, 4);//Inizializza risposta da trasmettere
	switch (bytesRX[0]) {
	case 0x20:				// Comandi non criptati con records solo numerici
		elaboraComandoNonCriptatoNumerico(i, j);
		break;
	case 0x26:						// Comandi con record numerico criptato
		elaboraComandoCriptatoNumerico();
		break;
	default:
		strcat(strTX, "01");		//comando non gestito;
		break;
	}
	trasmeRisposta ();
}

void do_nothing(void);
void do_nothing(void){
//	car_event_set_ignition(car_event_get(), EVENT_IGNITION_ON, 1);
//	uint8_t buff[10] = {'C', 'i', 'a', 'o', '\r', '\n'};
//	sendChars(buff, 6);
//	reset_counter++;
//	if (reset_counter >= 10){
//		clear_buffer();
//	}
}

static bool uart_configured = false;
bool updater_configured(void){
	return uart_configured;
}

static bool configured = false;
void firmware_upgrade_init() {
	if (!configured){
		configured = true;
		LOG_DEF_NORMAL("Updater init\r\n");
		firmware_upgrade_hw_init();
		uart_configured = true;
        updater_uart = &uart;
        updater_uart->init(updater_uart, 115200, 'N');
        
		vSemaphoreCreateBinary(xSemaphore);
		timer_reset = xTimerCreate("t_reset" , 1000/portTICK_PERIOD_MS, 	pdTRUE, 0, vTimerReset);
		if (timer_reset == NULL){
			LOG_DEF_ERROR("Cannot create timer.\r\n");
		}
		xTaskCreate(firmware_task, "update task", configMINIMAL_STACK_SIZE+400, NULL, configMAX_PRIORITIES-2, NULL);
	}
}




