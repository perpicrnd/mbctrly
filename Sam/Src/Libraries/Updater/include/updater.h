#pragma once

#include <stdbool.h>
#include <stdint.h>

void firmware_upgrade_init();
void firmware_upgrade_hw_init();
bool updater_configured(void);

int updater_flash_write_page(uint32_t *where, const void *what, int size);

void restart_system(void);
bool updater_active_bank(uint8_t *bank);
uint8_t updater_get_architecture(void);
uint8_t updater_get_flash_size_id(void);
bool updater_change_boot_sector(uint8_t bank);

