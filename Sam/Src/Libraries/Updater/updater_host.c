#include "updater.h"

#include <stdio.h>

void restart_system(void){
	printf("Se fossi su arm mi riavvierei\r\n");
}

bool updater_active_bank(uint8_t *bank){
	(void)bank;
	return false;
}

int updater_flash_write_page(uint32_t *where, const void *what, int size){
	(void)where;
	(void)what;
	(void)size;
	return 0;
}

uint8_t updater_get_architecture(void){
	return (uint8_t)(0x01);
}

uint8_t updater_get_flash_size_id(void){
	return (uint8_t)(0x00);
}

bool updater_change_boot_sector(uint8_t bank){
	(void)bank;
	return true;
}

void firmware_upgrade_hw_init(){

}
