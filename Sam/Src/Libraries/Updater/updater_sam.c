#include "updater.h"
#include "asf.h"
#include "flash_efc.h"
#include "logger.h"
//#include "hardware_config.h"

void firmware_upgrade_hw_init(){
	uint32_t ul_rc = flash_init(FLASH_ACCESS_MODE_128, 6);
	if (ul_rc != FLASH_RC_OK) {
		LOG_DEF_NORMAL("-F- Initialization error %lu\n\r", ul_rc);
		return;
	}
}


int updater_flash_write_page(uint32_t *where, const void *what, int size){
	return flash_write(*where, what, 256, 1);
}

/**
 *
 * @param bank the active bank computed by the function.
 * @return true on success, false otherwise.
 */
bool updater_active_bank(uint8_t *bank){
	bool retVal = false;
	if (EFC_RC_OK == efc_perform_command(EFC0, EFC_FCMD_GGPB, 0)) {
		retVal = true;
		uint32_t gpnvmBit = efc_get_result(EFC0);
		if (gpnvmBit & 0x04L) {
			LOG_DEF_NORMAL("Banco 1 attivo\r\n");
			*bank = 1;	// Il boot avviene dal banco 1 ==> si dovrà scrivere sul banco 0
		}else {
			LOG_DEF_NORMAL("Banco 0 attivo\r\n");
			*bank = 0;	// Il boot avviene dal banco 0 ==> si dovrà scrive sul banco 1
		}
	}
	return retVal;
}

uint8_t updater_get_flash_size_id(void){
	return (uint8_t)(((CHIPID->CHIPID_CIDR) >> 8) & 0x0F);
}

uint8_t updater_get_architecture(void) {
    //return (uint8_t)(((CHIPID->CHIPID_CIDR) >> 20) & 0xFF);
    uint8_t ret_id;

    //Il boot loader di Buzzanca seleziona banchi di Flash di indirizzi 0x8000 e 0xA000 (Flash da 256K) se vede id = 0x83,
    //di indirizzi 0x8000 e 0xC000 (Flash da 512K) se vede id = 0x84
    if (updater_get_flash_size_id() == 9) {  //Flash da 256K
        ret_id = 0x83;
    } else {
        ret_id = 0x84;
    }

    return ret_id;
}

bool updater_change_boot_sector(uint8_t bank){
	bool retVal = false;
	uint32_t esito = EFC_RC_OK;
	if (bank!= 0) {	// Il boot attuale avviene dal banco 0 e si deve passare al banco 1
		esito = efc_perform_command(EFC0, EFC_FCMD_SGPB, 0x02);
	}else {
		esito = efc_perform_command(EFC0, EFC_FCMD_CGPB, 0x02u);
	}
	if (esito == EFC_RC_OK) {
		retVal = true;
	}
	return retVal;
}
