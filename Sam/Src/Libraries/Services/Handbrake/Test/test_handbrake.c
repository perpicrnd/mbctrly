#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "handbrake.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void) {
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected(handler);
}

void software_reset(void) {
}

void __wrap_event_emit(union EVENT *event) {
    (void)event;
}

extern bool handbrake_hw_configured;
extern enum HANDBRAKE_STATUS handbrake_hw_value;

void test_handbrake() {
    expect_value(__wrap_event_connect_callback, event, eEVENT_HANDBRAKE_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_handbrake);
    expect_value(__wrap_event_connect_callback, event, eEVENT_50MSEC_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, read_wired_handbrake);
    handbrake_init();
    assert_true(handbrake_hw_configured);
    assert_true(handbrake_hw_value == HANDBRAKE_ON);

    union EVENT event;
    event.as_generic.event = eEVENT_HANDBRAKE_CHANGED;
    event.as_handbrake.handbrake = HANDBRAKE_OFF;
    handle_handbrake(&event);
    assert_true(handbrake_hw_value == HANDBRAKE_OFF);

    event.as_generic.event = eEVENT_HANDBRAKE_CHANGED;
    event.as_handbrake.handbrake = HANDBRAKE_ON;

    handle_handbrake(&event);
    assert_true(handbrake_hw_value == HANDBRAKE_ON);
    handle_handbrake(&event);
    assert_true(handbrake_hw_value == HANDBRAKE_ON);

    event.as_handbrake.handbrake = HANDBRAKE_OFF;

    handle_handbrake(&event);
    assert_true(handbrake_hw_value == HANDBRAKE_OFF);
    handle_handbrake(&event);
    assert_true(handbrake_hw_value == HANDBRAKE_OFF);
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_handbrake, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
