#include "asf.h"

#include "handbrake.h"

static bool old1, mid1;
enum HANDBRAKE_STATUS handbrake_wired_in(void) {
    bool retVal = HANDBRAKE_OFF;
    bool val = ioport_get_pin_level(Brk_In);
    bool res = (((mid1 | val) & old1) | (mid1 & val));
    old1 = mid1;
    mid1 = val;
    if (res == 0) {
        retVal = HANDBRAKE_ON;
    }
    return retVal;
}

void handbrake_hw_set(enum HANDBRAKE_STATUS handbrake) {
    if (handbrake == HANDBRAKE_ON) {
        ioport_set_pin_level(Brk_Out, true);
    } else {
        ioport_set_pin_level(Brk_Out, false);
    }
}

void handbrake_hw_init(void) {
    ioport_set_pin_dir(Brk_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Brk_Out, true);
    ioport_set_pin_dir(Brk_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Brk_In, IOPORT_MODE_PULLUP);
}
