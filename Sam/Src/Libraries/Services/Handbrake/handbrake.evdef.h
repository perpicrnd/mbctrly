#pragma once

enum HANDBRAKE_MODULE {
    HANDBRAKE_MODULE_VEHICLE,
    HANDBRAKE_MODULE_SPEED
};

/**
 * @brief Definizione dei possibili stati del freno a mano.
 */
enum HANDBRAKE_STATUS {
    HANDBRAKE_ON,   //!< HANDBRAKE_ON freno tirato
    HANDBRAKE_OFF,  //!< HANDBRAKE_OFF freno rilasciato
};

/**
 * @brief evento di tipo freno a mano
 */
struct EVENT_HANDBRAKE {
    /**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Stato del freno a mano
	 */
    enum HANDBRAKE_STATUS handbrake;
    enum HANDBRAKE_MODULE module;
};

struct EVENT_WIRED_HANDBRAKE {
    /**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Stato del freno a mano
	 */
    enum HANDBRAKE_STATUS handbrake;
};

/**
 * @brief emette un messaggio con lo stato attuale del freno.
 * @param status lo stato attuale del freno.
 */
void event_handbrake_emit(enum HANDBRAKE_STATUS status, enum HANDBRAKE_MODULE);
void event_wired_handbrake_emit(enum HANDBRAKE_STATUS status);