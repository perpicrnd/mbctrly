#include "handbrake.h"
#include "event_list.h"
#include "logger.h"
#include "service.h"

static bool handbrake_uses_speed = false;
static bool vehicle_handbrake_available = false;
static enum HANDBRAKE_STATUS last_handbrake = HANDBRAKE_OFF;
static enum HANDBRAKE_STATUS vehicle_handbrake = HANDBRAKE_OFF;

enum HANDBRAKE_STATUS get_hw_handbrake(void) {
    return vehicle_handbrake;
}

/**
 * @brief Gestione dell'handbrake
 * 
 * @param EVENT l'evento contenente le informazioni relative al freno a mano
 * 
 * Il freno a mano può arrivare o tramite un messaggio CAN o tramite la generazione dal segnale di velocità.
 * Se il freno arriva tramite il segnale di velocità lo preferisco in quanto è più bello da vedere in macchina.
 * Mi salvo comunque il freno se ricevuto tramite leva perché voglio utilizzare questo segnale da reinviare ad altre interfacce che
 * possono essere collegate tramite UART alla nostra scheda. 
 */
static void handle_handbrake(const union EVENT* const msg) {
    enum HANDBRAKE_STATUS brake = msg->as_handbrake.handbrake;
    if (msg->as_handbrake.module == HANDBRAKE_MODULE_SPEED) {
        handbrake_uses_speed = true;
    }
    if (msg->as_handbrake.module == HANDBRAKE_MODULE_VEHICLE) {
        vehicle_handbrake_available = true;
        vehicle_handbrake = msg->as_handbrake.handbrake;
        if (handbrake_uses_speed) {
            return;
        }
    }
    handbrake_hw_set(brake);
    if (brake != last_handbrake) {
        event_out_service_emit();
        if (brake == HANDBRAKE_ON) {
            LOG_DEF_NORMAL("HANDBRAKE_ON\r\n");
        } else {
            LOG_DEF_NORMAL("HANDBRAKE_OFF\r\n");
        }
        last_handbrake = brake;
    }
}

static uint8_t counter = 0;
static void read_wired_handbrake(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        enum HANDBRAKE_STATUS hnd = handbrake_wired_in();
        if (counter < 5) {
            counter++;
        } else {
            event_wired_handbrake_emit(hnd);
        }
    }
}

void handbrake_init(void) {
    handbrake_hw_init();
    event_connect_callback(eEVENT_HANDBRAKE_CHANGED, handle_handbrake);
    handbrake_hw_set(HANDBRAKE_ON);
    event_connect_callback(eEVENT_50MSEC_CHANGED, read_wired_handbrake);
}

void event_handbrake_emit(enum HANDBRAKE_STATUS status, enum HANDBRAKE_MODULE module) {
    struct BASE_EVENTS* base_events = base_event_get();
    if ((base_events->handbrake != status) || ((event_can_be_forwarded(eEVENT_HANDBRAKE_CHANGED) != 0))) {
        event_lock_forward(eEVENT_HANDBRAKE_CHANGED);
        base_events->handbrake = status;

        union EVENT event;
        event.as_generic.event = eEVENT_HANDBRAKE_CHANGED;
        event.as_handbrake.handbrake = status;
        event.as_handbrake.module = module;
        event_emit(&event);
    }
}

void event_wired_handbrake_emit(enum HANDBRAKE_STATUS status) {
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_HANDBRAKE_CHANGED;
    event.as_wired_handbrake.handbrake = status;
    event_emit(&event);
}