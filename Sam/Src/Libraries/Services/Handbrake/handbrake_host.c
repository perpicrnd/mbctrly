#include <stdbool.h>

#include "handbrake.h"

bool handbrake_hw_configured = false;
enum HANDBRAKE_STATUS handbrake_hw_value = HANDBRAKE_OFF;
void handbrake_hw_init(void) {
    handbrake_hw_configured = true;
}

void handbrake_hw_set(enum HANDBRAKE_STATUS handbrake) {
    handbrake_hw_value = handbrake;
}

enum HANDBRAKE_STATUS handbrake_wired_in(void) {
    return HANDBRAKE_OFF;
}