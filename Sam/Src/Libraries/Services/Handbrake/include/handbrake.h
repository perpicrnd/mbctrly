#pragma once

#include "event_list.h"

void handbrake_init(void);
void handbrake_hw_init(void);
enum HANDBRAKE_STATUS get_hw_handbrake(void);
void handbrake_hw_set(enum HANDBRAKE_STATUS handbrake);
enum HANDBRAKE_STATUS handbrake_wired_in(void);