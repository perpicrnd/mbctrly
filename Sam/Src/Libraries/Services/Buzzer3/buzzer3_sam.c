#include "asf.h"
#include "buzzer3.h"

void buzzer3_init(void) {
    ioport_set_pin_dir(Spare_3_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_3_Out, true);
}

void buzzer3_set_level(bool level) {
    ioport_set_pin_level(Spare_3_Out, level);
}

bool buzzer3_pin_status(void) {
    return ioport_get_pin_level(Spare_3_Out);
}