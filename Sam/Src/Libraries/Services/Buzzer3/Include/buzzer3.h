#pragma once

#include <stdbool.h>

enum PARKING_SENSOR_LEVEL {
    PARKING_SENSOR_LEVEL_OFF = 0,
    PARKING_SENSOR_LEVEL_1,
    PARKING_SENSOR_LEVEL_2,
    PARKING_SENSOR_LEVEL_3,
    PARKING_SENSOR_LEVEL_4,
    PARKING_SENSOR_LEVEL_5,
    PARKING_SENSOR_LEVEL_6,
    PARKING_SENSOR_LEVEL_7,
    PARKING_SENSOR_LEVEL_8,

};

enum BUZZER_USAGE {
    BUZZER_USAGE_PARKINGSENSOR,
    BUZZER_USAGE_LANEASSIST,
};

void buzzer3_init(void);
void buzzer3_set_parkingsensors_level(enum PARKING_SENSOR_LEVEL state);
void buzzer3_set_laneassist_level(bool state);
void buzzer3_enable_parkingsensors(void);
void buzzer3_enable_laneassist(void);