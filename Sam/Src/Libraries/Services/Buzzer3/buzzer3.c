#include "buzzer3.h"
#include <string.h>
#include "buzzer3_priv.h"
#include "event_list.h"

struct TIMING {
    uint8_t t_on;
    uint16_t t_off;
};

static bool parkingsensorenabled = false;
static bool laneassistenabled = false;
static enum PARKING_SENSOR_LEVEL level;
static bool laneassist_level = false;
static struct TIMING loaded_timing = {0};
static struct TIMING running_timing = {0};

void handle_50msec(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        if (laneassist_level == false) {
            switch (level) {
                case PARKING_SENSOR_LEVEL_OFF:
                    buzzer3_set_level(false);
                    break;
                case PARKING_SENSOR_LEVEL_1:
                    buzzer3_set_level(true);
                    break;
                default:
                    if (buzzer3_pin_status()) {
                        if (running_timing.t_on > 0) {
                            running_timing.t_on--;
                        } else {
                            buzzer3_set_level(false);
                        }
                    } else {
                        if (running_timing.t_off > 0) {
                            running_timing.t_off--;
                        } else {
                            buzzer3_set_level(true);
                            memcpy(&running_timing, &loaded_timing, sizeof(struct TIMING));
                        }
                    }
            }
        } else {
            running_timing.t_off = 0;
            running_timing.t_on = 0;
            level = PARKING_SENSOR_LEVEL_OFF;
        }
    }
}

void buzzer3_set_parkingsensors_level(enum PARKING_SENSOR_LEVEL state) {
    level = state;
    switch (level) {
        case PARKING_SENSOR_LEVEL_OFF:
            loaded_timing.t_off = 0;
            loaded_timing.t_on = 0;
            break;
        default:
            loaded_timing.t_on = 2;
            loaded_timing.t_off = 2 * state;
            break;
    }
}

void buzzer3_set_laneassist_level(bool state) {
    laneassist_level = state;
    buzzer3_set_level(state);
}

void buzzer3_enable_parkingsensors(void) {
    parkingsensorenabled = true;
    buzzer3_init();
    event_connect_callback(eEVENT_50MSEC_CHANGED, handle_50msec);
}

void buzzer3_enable_laneassist(void) {
    laneassistenabled = true;
}