#pragma once

#include "handbrake.h"
#include "ignition.h"
#include "lights.h"
#include "reverse.h"
#include "speed.h"
#include "event_list.h"

void service_init(void);
bool event_can_be_forwarded(enum EVENTS event);
void event_lock_forward(enum EVENTS event);

struct BASE_EVENTS{
	enum IGNITION_STATUS ignition;
	enum HANDBRAKE_STATUS handbrake;
	enum REVERSE_STATUS reverse;
	enum LIGHTS_STATUS lights;
    enum SWC swc;
	enum SWC analog_swc;
	uint32_t speed;
};

struct BASE_EVENTS* base_event_get(void);