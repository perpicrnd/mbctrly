#pragma once

#include "event_list.h"

void hud_hw_set(enum IGNITION_STATUS ignition);
void ignition_init(void);
void ignition_hw_init(void);
void ignition_hw_set(enum IGNITION_STATUS ignition);
enum IGNITION_STATUS ignition_wired_in1(void);
enum IGNITION_STATUS ignition_wired_in2(void);
enum IGNITION_STATUS get_hw_ignition(void);
bool is_radio_turned_on(void);
bool ignition_hw_get_level(void);
void factory_reset_activate(void);
enum IGNITION_STATUS vehicle_requested_ignition(void);