#include "asf.h"
#include "ignition.h"
#include "logger.h"

void ignition_hw_set(enum IGNITION_STATUS ignition){
	if (ignition == IGNITION_ON){
		ioport_set_pin_level(Ign_Out, true);
	}else{
		ioport_set_pin_level(Ign_Out, false);
	}
}

bool ignition_hw_get_level(void){
    return ioport_get_pin_level(Ign_Out);
}

enum IGNITION_STATUS get_hw_ignition(void){
    if (ioport_get_pin_level(Ign_Out)){
        return IGNITION_ON;
    }else{
        return IGNITION_OFF;
    }
}

static bool old1, mid1;
enum IGNITION_STATUS ignition_wired_in1(void){
	bool retVal = IGNITION_OFF;
	bool val = ioport_get_pin_level(Ign_In_1);
	bool res = (((mid1 | val) & old1) | (mid1 & val));
	old1 = mid1;
	mid1 = val;
	if (res == 0){
		retVal = IGNITION_ON;
	}
	return retVal;
}

static bool old2, mid2;
enum IGNITION_STATUS ignition_wired_in2(void){
	bool retVal = IGNITION_OFF;
	bool val = ioport_get_pin_level(Ign_In_2);
	bool res = (((mid2 | val) & old2) | (mid2 & val));
	old2 = mid2;
	mid2 = val;
	if (res == 0){
		retVal = IGNITION_ON;
	}
	return retVal;
}

void ignition_hw_init(void){
	ioport_set_pin_dir(Ign_Out, IOPORT_DIR_OUTPUT);

}

