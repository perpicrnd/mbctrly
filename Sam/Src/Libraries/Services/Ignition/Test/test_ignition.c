#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "ignition.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

extern bool ignition_hw_configured;
extern enum IGNITION_STATUS ignition_hw_value;

enum IGNITION_STATUS __wrap_ignition_wired_in(void) {
    return mock_type(enum IGNITION_STATUS);
}

void __wrap_event_emit(union EVENT *event) {
    (void)event;
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected(handler);
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

void __wrap_received_button_settings(uint8_t buttons) {
    (void)buttons;
}

void __wrap_received_ir_power(bool value) {
    (void)value;
}

void test_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_CHANGED;
    event.as_ignition.ignition = IGNITION_ON;

    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_ON);
    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_ON);

    event.as_ignition.ignition = IGNITION_OFF;

    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_OFF);
    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_OFF);

    invert_ignition = true;

    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_ON);

    event.as_ignition.ignition = IGNITION_ON;
    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_ON);
}

void test_ignition_delayed(void **state) {
    (void)state;

    union EVENT second;
    requested_ignition = IGNITION_OFF;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    handle_second_changed(&second);
    assert_true(delayed_ignition == 0);

    requested_ignition = IGNITION_ON;
    will_return(__wrap_settings_read, 1);
    handle_second_changed(&second);
    assert_true(delayed_ignition == 60);

    requested_ignition = IGNITION_OFF;
    handle_second_changed(&second);
    assert_true(delayed_ignition == 59);

    handle_second_changed(&second);
    assert_true(delayed_ignition == 58);

    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_CHANGED;
    event.as_ignition.ignition = IGNITION_OFF;

    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_ON);

    delayed_ignition = 0;
    handle_ignition(&event);
    assert_true(ignition_hw_value == IGNITION_OFF);
}

void test_ignition_inverter(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_INVERTER_VALUE;
    event.as_ignition_inverter.value = true;

    assert_true(saw_no_action);
    assert_false(invert_ignition);

    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    event.as_ignition_inverter.value = false;
    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    event.as_ignition_inverter.value = true;
    handle_ignition_inverter(&event);
    assert_false(invert_ignition);
}

void test_vehicle_ignition_off_with_radio_on_for_delay(void **state) {
    (void)state;
    //Evento secondo
    union EVENT second;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;

    //Evento ignition ON
    union EVENT event_ignition_on;
    event_ignition_on.as_generic.event = eEVENT_IGNITION_CHANGED;
    event_ignition_on.as_ignition.ignition = IGNITION_ON;

    //Evento ignition OFF
    union EVENT event_ignition_off;
    event_ignition_off.as_generic.event = eEVENT_IGNITION_CHANGED;
    event_ignition_off.as_ignition.ignition = IGNITION_OFF;

    //Attivo il sottochiave => Accendo
    handle_ignition(&event_ignition_on);
    assert_true(ignition_hw_value == IGNITION_ON);
    assert_true(requested_ignition == IGNITION_ON);

    //Configurato per rimanere acceso 60 secondi
    will_return(__wrap_settings_read, 1);
    handle_second_changed(&second);
    assert_true(delayed_ignition == 60);

    {
        //Schiaccio il tasto per spegnere manualmente la radio
        union EVENT event;
        event.as_generic.event = eEVENT_IGNITION_INVERTER_VALUE;
        event.as_ignition_inverter.value = true;

        assert_true(saw_no_action);
        assert_false(invert_ignition);

        handle_ignition_inverter(&event);
        assert_true(invert_ignition);

        //Adesso mi aspetto l'ignition invertita.
        handle_ignition(&event_ignition_on);
        assert_true(ignition_hw_value == IGNITION_OFF);

        //Quindi adesso ho l'ignition verso la radio a OFF ma la richiesta di ignition ON
        handle_ignition(&event_ignition_on);
        assert_true(ignition_hw_value == IGNITION_OFF);
        assert_true(requested_ignition == IGNITION_ON);

        will_return(__wrap_settings_read, 1);
        handle_second_changed(&second);
        assert_true(delayed_ignition == 60);

        //Adesso apro la portiera, quindi mi arriva una richiesta di ignition OFF.
        //In questo momento, dato che la radio era spenta deve rimanere spenta.
        handle_ignition(&event_ignition_off);
        assert_true(ignition_hw_value == IGNITION_OFF);
        assert_true(requested_ignition == IGNITION_OFF);

        handle_second_changed(&second);
        assert_true(delayed_ignition == 0);

        handle_ignition(&event_ignition_off);
        assert_true(ignition_hw_value == IGNITION_OFF);
        assert_true(requested_ignition == IGNITION_OFF);
        assert_true(delayed_ignition == 0);

        handle_second_changed(&second);
        assert_true(delayed_ignition == 0);
    }
}

void test_ignition_invert_with_delay(void **state) {
    (void)state;
    //Evento secondo
    union EVENT second;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;

    //Evento ignition ON
    union EVENT event_ignition_on;
    event_ignition_on.as_generic.event = eEVENT_IGNITION_CHANGED;
    event_ignition_on.as_ignition.ignition = IGNITION_ON;

    //Evento ignition OFF
    union EVENT event_ignition_off;
    event_ignition_off.as_generic.event = eEVENT_IGNITION_CHANGED;
    event_ignition_off.as_ignition.ignition = IGNITION_OFF;

    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_INVERTER_VALUE;
    event.as_ignition_inverter.value = true;

    //Attivo il sottochiave => Accendo
    handle_ignition(&event_ignition_on);
    assert_true(ignition_hw_value == IGNITION_ON);
    assert_true(requested_ignition == IGNITION_ON);

    //Configurato per rimanere acceso 60 secondi
    will_return(__wrap_settings_read, 1);
    handle_second_changed(&second);
    assert_true(delayed_ignition == 60);

    handle_ignition(&event_ignition_off);
    assert_true(ignition_hw_value == IGNITION_ON);
    assert_true(requested_ignition == IGNITION_OFF);

    assert_true(saw_no_action);
    assert_false(invert_ignition);

    handle_ignition_inverter(&event);
    assert_true(invert_ignition);

    handle_ignition(&event_ignition_off);
    assert_true(ignition_hw_value == IGNITION_OFF);
    assert_true(requested_ignition == IGNITION_OFF);

    handle_ignition(&event_ignition_off);
    assert_true(ignition_hw_value == IGNITION_OFF);
    assert_true(requested_ignition == IGNITION_OFF);

    handle_ignition(&event_ignition_off);
    assert_true(ignition_hw_value == IGNITION_OFF);
    assert_true(requested_ignition == IGNITION_OFF);

    handle_ignition(&event_ignition_off);
    assert_true(ignition_hw_value == IGNITION_OFF);
    assert_true(requested_ignition == IGNITION_OFF);
}

void test_ignition_init(void **state) {
    (void)state;

    expect_value(__wrap_event_connect_callback, event, eEVENT_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_ignition);
    expect_value(__wrap_event_connect_callback, event, eEVENT_50MSEC_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_ignition);

#if (ENABLE_PRINTOUT)
    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_second_changed);
#endif

    expect_value(__wrap_event_connect_callback, event, eEVENT_IGNITION_INVERTER_VALUE);
    expect_value(__wrap_event_connect_callback, handler, handle_ignition_inverter);

    expect_value(__wrap_event_connect_callback, event, eEVENT_50MSEC_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, read_wired_ignition);

    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_second_changed);

    ignition_init();
    assert_true(ignition_hw_configured);
    assert_true(ignition_hw_value == IGNITION_OFF);
}

int setup(void **state) {
    (void)state;
    saw_no_action = true;
    invert_ignition = false;
    delayed_ignition = 0;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_ignition, setup),
        cmocka_unit_test_setup(test_ignition_delayed, setup),
        cmocka_unit_test_setup(test_ignition_init, setup),
        cmocka_unit_test_setup(test_ignition_inverter, setup),
        cmocka_unit_test_setup(test_vehicle_ignition_off_with_radio_on_for_delay, setup),
        cmocka_unit_test_setup(test_ignition_invert_with_delay, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
