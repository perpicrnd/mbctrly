cmake_minimum_required(VERSION 3.8)

SET(LIBRARY_NAME ignition_lib)

if (CMAKE_CROSSCOMPILING)
	set(SOURCES ignition_sam.c)
else()
	set(SOURCES ignition_host.c)
	add_subdirectory(Test)
endif()	


set(SOURCES ${SOURCES}
	ignition.c
)

option(IGNITION_PRINTOUT "Abilita la stampa ogni secondo del messaggio di controllo per il banco di test." OFF)

if (IGNITION_PRINTOUT)
    MESSAGE("Su sam la trasmissione dei messaggi di debug e' abilitata.")
endif()


add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries(${LIBRARY_NAME} eventlist freertos_cli radio buttons settings)
if (CMAKE_CROSSCOMPILING)
	target_link_libraries (${LIBRARY_NAME} asf_library )
	if (IGNITION_PRINTOUT)
        target_compile_definitions(${LIBRARY_NAME} PRIVATE "ENABLE_PRINTOUT=1")
    endif()
endif()
        
target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)
