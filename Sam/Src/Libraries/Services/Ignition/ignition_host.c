#include <stdbool.h>
#include <stdint.h>

#include "ignition.h"

bool ignition_hw_configured = false;
enum IGNITION_STATUS ignition_hw_value = IGNITION_OFF;
enum IGNITION_STATUS hud_hw_value = IGNITION_OFF;

enum IGNITION_STATUS get_hw_ignition(void) {
    return ignition_hw_value;
}

void ignition_hw_init(void) {
    ignition_hw_configured = true;
}

void ignition_hw_set(enum IGNITION_STATUS ignition) {
    ignition_hw_value = ignition;
}

enum IGNITION_STATUS ignition_wired_in1(void) {
    return IGNITION_ON;
}

enum IGNITION_STATUS ignition_wired_in2(void) {
    return IGNITION_ON;
}

void hud_hw_set(enum IGNITION_STATUS ignition) {
    hud_hw_value = ignition;
}

bool ignition_hw_get_level(void) {
    if (ignition_hw_value == IGNITION_ON) {
        return true;
    } else {
        return false;
    }
}