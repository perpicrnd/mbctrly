#pragma once

/**
 * @brief Definizione dei possibili stati della chiave del veicolo.
 */
enum KEY_POS{
	KEY_OUT,  //!< KEY_OUT key fuori dal quadro.
	KEY_POS_0,//!< KEY_POS_0 chiave inserita nel quadro.
	KEY_POS_1,//!< KEY_POS_1 chiave girata di uno scatto.
	KEY_POS_2,//!< KEY_POS_2 chiave girata di due scatti.
};

/**
 * @brief Definizione dei possibili stati del sottochiave.
 */
enum IGNITION_STATUS{
	IGNITION_ON, //!< IGNITION_ON ignition presente
	IGNITION_OFF,//!< IGNITION_OFF ignition non presente
};

enum WIRED_IGNITION_MODULE{
    WIRED_IGNITION_MODULE_0 = 0,
    WIRED_IGNITION_MODULE_1
};

/**
 * @brief evento emesso alla ricezione di un messaggio di tipo sottochiave
 */
struct EVENT_IGNITION{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato del sottochiave.
	 */
	enum IGNITION_STATUS ignition;
};

struct EVENT_WIRED_IGNITION{
    /**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato del sottochiave.
	 */
	enum IGNITION_STATUS ignition;
    /**
     * Modulo che ha pilotato il cambio di sottochiave.
     * 
     */
    uint8_t module;
};


struct EVENT_IGNITION_INVERTER_VALUE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Messaggio canbus ricevuto.
	 */
	bool value;
};

/**
 * @brief emette un messaggio con lo stato dell'ignition.
 * @param status lo stato dell'ignition da inviare.
 */
void event_ignition_emit(enum IGNITION_STATUS status);

void event_wired_ignition_emit(enum IGNITION_STATUS status, uint8_t ignition_module);

void event_ignition_inverter_value_emit(bool value);