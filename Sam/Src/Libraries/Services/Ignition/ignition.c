#include "ignition.h"
#include "FreeRTOS.h"
#include "alpine_menu.h"
#include "event_list.h"
#include "logger.h"
#include "main.h"
#include "service.h"
#include "settings.h"
#include "timers.h"
#include "wisker.h"

static enum IGNITION_STATUS last_ignition = IGNITION_OFF;

#if ENABLE_PRINTOUT
#warning "Printout enabled, do not use in production code."
static uint32_t ignition_counter_seconds;
static void handle_second_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (last_ignition == IGNITION_ON) {
            CAN_MESSAGE msg = {0x10, true, {'T', 'E', 'S', 'T', 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
            msg.can_id = maybe_unique_can_id();
            msg.can_data.byte4 = last_ignition;
            msg.can_data.byte5 = ignition_counter_seconds >> 16;
            msg.can_data.byte6 = ignition_counter_seconds >> 8;
            msg.can_data.byte7 = ignition_counter_seconds;
            ignition_counter_seconds++;
            event_can0_tx_message_emit(&msg);
        }
    }
}
#endif

static bool saw_no_action = true;
static bool invert_ignition = false;
static uint32_t delayed_ignition = 0;
static enum IGNITION_STATUS requested_ignition = IGNITION_OFF;
static enum IGNITION_STATUS old_requested_ignition = IGNITION_OFF;
static enum IGNITION_STATUS ign = IGNITION_OFF;
static bool factory_reset_pressed = false;
static uint8_t factory_reset_counter = 0;

void factory_reset_activate(void) {
    factory_reset_pressed = true;
}

enum IGNITION_STATUS vehicle_requested_ignition(void) {
    return requested_ignition;
}

static void handle_ignition(const union EVENT* const msg) {
    bool process_ignition = false;
    if (msg->as_generic.event == eEVENT_IGNITION_CHANGED) {
        process_ignition = true;
        ign = msg->as_ignition.ignition;
        requested_ignition = msg->as_ignition.ignition;
        if ((delayed_ignition != 0) && (requested_ignition == IGNITION_OFF)) {
            if (is_radio_turned_on()) {
                // LOG_DEF_NORMAL("invert %d\r\n", invert_ignition);
                ign = IGNITION_ON;
            } else {
                delayed_ignition = 0;
            }
        }

        if (requested_ignition != old_requested_ignition) {
            invert_ignition = false;
            old_requested_ignition = requested_ignition;
        }
    }

    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        process_ignition = true;
    }

    if (process_ignition) {
        // LOG_DEF_NORMAL("Ignition: %02X inverter: %02X\r\n", ign, invert_ignition);
        enum IGNITION_STATUS app_ignition = ign;
        if (invert_ignition) {
            if (app_ignition == IGNITION_OFF) {
                app_ignition = IGNITION_ON;
            } else if (app_ignition == IGNITION_ON) {
                app_ignition = IGNITION_OFF;
                if ((delayed_ignition != 0) && (requested_ignition == IGNITION_OFF)) {
                    //Sto spegnendo con request già off ma mantenuto acceso dal delayed_ignition
                    delayed_ignition = 0;
                    //quindi resetto la flag di invert ignition
                    invert_ignition = false;
                }
            }
        }
        if (factory_reset_pressed) {
            app_ignition = IGNITION_OFF;
            ign = IGNITION_OFF;
            invert_ignition = false;
            LOG_DEF_NORMAL("Factory reset requested. Ignition OFF.\r\n");
        }

        ignition_hw_set(app_ignition);
        if ((app_ignition == IGNITION_ON) || (requested_ignition == IGNITION_ON)) {
            wisker_reset(MODULE_VEHICLE);
        }

        if (app_ignition != last_ignition) {
            event_out_service_emit();
            if (app_ignition == IGNITION_ON) {
                LOG_DEF_NORMAL("IGNITION_ON\r\n");
            } else {
                LOG_DEF_NORMAL("IGNITION_OFF\r\n");
            }
            last_ignition = app_ignition;
        }
    }
}

static void handle_second_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (requested_ignition == IGNITION_ON) {
            uint8_t minutes = settings_read(RADIO_SETTINGS_IGN_MINUTE_BYTE);
            if (minutes > 30) {
                minutes = 0;
                settings_write(RADIO_SETTINGS_IGN_MINUTE_BYTE, minutes);
            }
            delayed_ignition = minutes * 60;
        } else {
            if (delayed_ignition > 0) {
                delayed_ignition--;
#ifdef __amd64__
                LOG_DEF_NORMAL("Delayed ignition %d seconds remaining\r\n", delayed_ignition);
#else
                LOG_DEF_NORMAL("Delayed ignition %ld seconds remaining\r\n", delayed_ignition);
#endif
            }
        }
        if (factory_reset_pressed) {
            if (factory_reset_counter++ > 2) {
                software_reset();
            }
        }
    }
}

static void handle_ignition_inverter(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_IGNITION_INVERTER_VALUE) {
        bool tmp = msg->as_ignition_inverter.value;
        if (tmp == false) {
            saw_no_action = true;
        } else {
            if (saw_no_action) {
                invert_ignition = !invert_ignition;
                saw_no_action = false;
            }
        }
    }
}

bool is_radio_turned_on(void) {
    return ignition_hw_get_level();
}

/**
 * Timer per la gestione del sottochiave.
 */
static void read_wired_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        enum IGNITION_STATUS ign = ignition_wired_in1();
        event_wired_ignition_emit(ign, WIRED_IGNITION_MODULE_0);
        ign = ignition_wired_in2();
        event_wired_ignition_emit(ign, WIRED_IGNITION_MODULE_1);
    }
}

void ignition_init(void) {
    ignition_hw_init();
    ignition_hw_set(IGNITION_OFF);
    event_connect_callback(eEVENT_IGNITION_CHANGED, handle_ignition);
    event_connect_callback(eEVENT_50MSEC_CHANGED, handle_ignition);
#if (ENABLE_PRINTOUT)
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
#endif
    event_connect_callback(eEVENT_IGNITION_INVERTER_VALUE, handle_ignition_inverter);
    event_connect_callback(eEVENT_50MSEC_CHANGED, read_wired_ignition);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
}

void event_wired_ignition_emit(enum IGNITION_STATUS status, uint8_t ignition_module) {
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.ignition = status;
    event.as_wired_ignition.module = ignition_module;
    event_emit(&event);
}

void event_ignition_inverter_value_emit(bool value) {
    union EVENT event;
    event.as_generic.event = eEVENT_IGNITION_INVERTER_VALUE;
    event.as_ignition_inverter.value = value;
    event_emit(&event);
}
