#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "lights.c"
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void){
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

void vApplicationIdleHook(void){
//Make the linker happy.
}

void software_reset(void){

}


void __wrap_event_emit(union EVENT *event){
    (void)event;
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

extern bool lights_hw_configured;
extern enum LIGHTS_STATUS lights_hw_value;

void test_lights(){
	lights_init();
	assert_true(lights_hw_configured);
	assert_true(lights_hw_value == LIGHTS_OFF);

	union EVENT event;
	event.as_generic.event = eEVENT_LIGHTS_CHANGED;
	event.as_lights.lights = LIGHTS_ON;

	handle_lights(&event);
	assert_true(lights_hw_value == LIGHTS_ON);
	handle_lights(&event);
	assert_true(lights_hw_value == LIGHTS_ON);

	event.as_lights.lights = LIGHTS_OFF;

	handle_lights(&event);
	assert_true(lights_hw_value == LIGHTS_OFF);
	handle_lights(&event);
	assert_true(lights_hw_value == LIGHTS_OFF);
}

int setup(void **state){
	(void)state;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
			cmocka_unit_test_setup(test_lights, setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
