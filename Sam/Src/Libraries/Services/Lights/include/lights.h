#pragma once

#include "event_list.h"


void lights_init(void);
void lights_hw_init(void);
enum LIGHTS_STATUS get_hw_lights(void);
void lights_hw_set(enum LIGHTS_STATUS lights);

