#include "asf.h"

#include "lights.h"

void lights_hw_set(enum LIGHTS_STATUS lights){
	if (lights == LIGHTS_ON){
		ioport_set_pin_level(Lig_Out, true);
	}else{
		ioport_set_pin_level(Lig_Out, false);
	}
}

enum LIGHTS_STATUS get_hw_lights(void){
    if (ioport_get_pin_level(Lig_Out)){
        return LIGHTS_ON;
    }else{
        return LIGHTS_OFF;
    }
}

void lights_hw_init(void){
	ioport_set_pin_dir(Lig_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Lig_Out, true);
}

