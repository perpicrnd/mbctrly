#include "lights.h"
#include "event_list.h"
#include "logger.h"
#include "service.h"

static enum LIGHTS_STATUS last_lights = LIGHTS_OFF;
static void handle_lights(const union EVENT * const msg){
	enum LIGHTS_STATUS lights = msg->as_lights.lights;
	lights_hw_set(lights);

	if (lights != last_lights){
        event_out_service_emit();
		if (lights == LIGHTS_ON){
			LOG_DEF_NORMAL("LIGHTS_ON\r\n");
		}else{
			LOG_DEF_NORMAL("LIGHTS_OFF\r\n");
		}
		last_lights = lights;
	}
}

void lights_init(void){
	lights_hw_init();
	event_connect_callback(eEVENT_LIGHTS_CHANGED, handle_lights);
    lights_hw_set(LIGHTS_OFF);
}


void event_lights_emit(enum LIGHTS_STATUS status){
	struct BASE_EVENTS * base_events = base_event_get();
	if ((base_events->lights != status) || ((event_can_be_forwarded(eEVENT_LIGHTS_CHANGED) != 0))){
		event_lock_forward(eEVENT_LIGHTS_CHANGED);
		base_events->lights = status;

		union EVENT event;
		event.as_generic.event = eEVENT_LIGHTS_CHANGED;
		event.as_lights.lights = status;
		event_emit(&event);
	}
}
