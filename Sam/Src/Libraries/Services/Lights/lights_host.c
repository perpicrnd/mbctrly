#include <stdbool.h>

#include "lights.h"

bool lights_hw_configured = false;
enum LIGHTS_STATUS lights_hw_value = LIGHTS_OFF;

void lights_hw_init(void){
	lights_hw_configured = true;
}

enum LIGHTS_STATUS get_hw_lights(void){
    if (lights_hw_value){
        return LIGHTS_ON;
    }else{
        return LIGHTS_OFF;
    }
}


void lights_hw_set(enum LIGHTS_STATUS lights){
	lights_hw_value = lights;
}
