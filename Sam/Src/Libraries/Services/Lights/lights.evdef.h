#pragma once


/**
 * @brief Definizione dei possibili stati delle luci del veicolo.
 */
enum LIGHTS_STATUS{
	LIGHTS_ON, //!< LIGHTS_ON luci accese
	LIGHTS_OFF,//!< LIGHTS_OFF luci spente
};

/**
 * @brief evento di tipo illuminazione
 */
struct EVENT_LIGHTS{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato delle luci.
	 */
	enum LIGHTS_STATUS lights;
};


/**
 * @brief emette un messaggio con lo stato delle luci.
 * @param status lo stato delle luci da inviare.
 */
void event_lights_emit(enum LIGHTS_STATUS status);