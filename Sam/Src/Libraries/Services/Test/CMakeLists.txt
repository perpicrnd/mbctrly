include_directories ("${CMAKE_CURRENT_SOURCE_DIR}/..")


add_executable(test_service test_service.c)
add_dependencies(test_service test_service)
add_test(test_service test_service)
target_link_libraries(test_service service_lib cmocka)
set_target_properties(test_service PROPERTIES LINK_FLAGS 
	"-Wl,--wrap=event_connect_callback ")