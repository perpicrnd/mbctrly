#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "service.c"
#include "event_list.h"
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void){
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

void vApplicationIdleHook(void){
//Make the linker happy.
}


void software_reset(void){

}


int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler){
	(void)handler;
	function_called();
	check_expected(event);

}


int setup(void **state){
	(void)state;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
