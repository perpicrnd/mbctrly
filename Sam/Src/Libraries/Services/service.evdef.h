#pragma once


struct OUT_SERVICE{
    enum IGNITION_STATUS ignition;
    enum REVERSE_STATUS reverse;
    enum LIGHTS_STATUS lights;
    enum HANDBRAKE_STATUS handbrake;
    uint16_t speed;
};

struct EVENT_OUT_SERVICE{
	/**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    struct OUT_SERVICE out_service;
};

/**
 * @brief emette un messaggio con lo stato attuale dei servizi.
 */
void event_out_service_emit(void);
