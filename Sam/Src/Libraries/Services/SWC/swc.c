#include <FreeRTOS.h>
#include <timers.h>

#include <stdio.h>

#include "event_list.h"
#include "logger.h"
#include "service.h"

/**
 * @addtogroup SWC
 * @{
 */

/**
 * @brief Timer che gestisce il tempo di mantenimento su un comando sul rilascio del tasto multiplo prima del timeout.
 */
static TimerHandle_t timer_release;
/**
 * @brief Timer che definisce il tempo di timeout per la prima o la seconda funzione del tasto.
 */
static TimerHandle_t timer_timeout;
/**
 * @brief comando da applicare dopo lo scadere del timeout.
 */
static enum SWC swc_timeout;
/**
 * @brief comando da applicare se il tasto viene rilasciato prima dello scadere del timeout.
 */
static enum SWC swc_release;
/**
 *
 * @brief Flag che segnala il fatto che sia già arrivato un press prima del release.
 * la flag contiene il codice del comando che ha generato l'evento, in modo che più moduli
 * possano coesistere utilizzando le stesse variabili.
 */
static enum SWC press_received;

/**
 * @brief modulo dei comandi a volante da ritrasmettere.
 * 
 */
static enum SWC_MODULE module;
/**
 * @brief Timer che gestisce il tempo di mantenimento della pressione del tasto.
 * @param pxTimer non usato.
 */
static void vTimerRelease(TimerHandle_t pxTimer)
{
	(void)pxTimer;
	LOG_DEF_NORMAL("Rilascio temporizzato %d %d\r\n", swc_release, module);
	event_swc_emit(swc_release, module);
	module = SWC_MODULE_NONE;
}

/**
 * @brief Timer che gestisce il tempo di timeout per il tasto multiplo.
 * @param pxTimer
 */
static void vTimerTimeout(TimerHandle_t pxTimer)
{
	(void)pxTimer;
	LOG_DEF_NORMAL("Timeout %d\r\n", swc_timeout);
	event_swc_emit(swc_timeout, module);
}

/**
 * @brief Gestione del comando multiplo TEL_PICKUP_SPEECH
 * @param msg l'evento ricevuto
 */
static inline void swc_handle_pickup_hangup(const union EVENT * const msg){
	do{
		if (msg->as_swrc.module != module){
			if (module != SWC_MODULE_NONE){
				break;
			}
		}
		switch(msg->as_swrc.btn.swc){
		case SWC_TEL_PICKUP_HANGUP_PRESSED:
			if (press_received == SWC_NO_BUTTON_PRESSED){
				if (!xTimerIsTimerActive(timer_timeout)){
					xTimerStart(timer_timeout, 10);
					swc_timeout = SWC_TEL_HANGUP_PRESSED;
					swc_release = SWC_TEL_PICKUP_RELEASED;
				}
				press_received = SWC_TEL_PICKUP_HANGUP_PRESSED;
				module = msg->as_swrc.module;
			}
			break;
		case SWC_TEL_PICKUP_HANGUP_RELEASED:
			if (press_received == SWC_TEL_PICKUP_HANGUP_PRESSED){
				module = msg->as_swrc.module;
				if (xTimerIsTimerActive(timer_timeout)){
					event_swc_emit(SWC_TEL_PICKUP_PRESSED, msg->as_swrc.module);
					xTimerStop(timer_timeout, 10);
					xTimerStart(timer_release, 10);
				}else{
					event_swc_emit(SWC_TEL_HANGUP_RELEASED, msg->as_swrc.module);
					module = SWC_MODULE_NONE;
				}
				
			}
			break;
		default:
			if (press_received == SWC_TEL_PICKUP_HANGUP_PRESSED){
				press_received = SWC_NO_BUTTON_PRESSED;
				module = SWC_MODULE_NONE;
			}
			break;
		}
	}while(0);
}

/**
 * @brief Gestione del comando multiplo mute / ignition 
 * @param msg l'evento ricevuto
 */
static inline void swc_handle_mute_ignition(const union EVENT * const msg){
	do{
		if (msg->as_swrc.module != module){
			if (module != SWC_MODULE_NONE){
				break;
			}
		}

		switch(msg->as_swrc.btn.swc){
		case SWC_MUTE_IGNITION_PRESSED:
			if (press_received == SWC_NO_BUTTON_PRESSED){
				if (!xTimerIsTimerActive(timer_timeout)){
					xTimerStart(timer_timeout, 10);
					swc_timeout = SWC_IGNITION_PRESSED;
					swc_release = SWC_MUTE_RELEASED;
				}
				press_received = SWC_MUTE_IGNITION_PRESSED;
				module = msg->as_swrc.module;
			}
			break;
		case SWC_MUTE_IGNITION_RELEASED:
			if (press_received == SWC_MUTE_IGNITION_PRESSED){
				module = msg->as_swrc.module;
				if (xTimerIsTimerActive(timer_timeout)){
					event_swc_emit(SWC_MUTE_PRESSED, msg->as_swrc.module);
					xTimerStop(timer_timeout, 20);
					xTimerStart(timer_release, 80);
				}else{
					event_swc_emit(SWC_IGNITION_RELEASED, msg->as_swrc.module);
					module = SWC_MODULE_NONE;
				}
			}
			break;
		default:
			if (press_received == SWC_MUTE_IGNITION_PRESSED){
				press_received = SWC_NO_BUTTON_PRESSED;
			}
			break;
		}
	}while(0);
}


/**
 * @brief callback dall'event system che verifica i tipi di comandi multipli attivabili.
 * @param msg il comando dall'event system.
 */
static void swc_handle_multiple_buttons(const union EVENT * const msg){
	swc_handle_pickup_hangup(msg);
	swc_handle_mute_ignition(msg);
}

/**
 * Inizializza il sistema di gestione dei comandi a volante.
 */
void swc_init(void){
	timer_timeout = xTimerCreate((char *)"t_brk" , 600/portTICK_PERIOD_MS, 	pdFALSE, 0, vTimerTimeout);
	timer_release = xTimerCreate((char *)"t_brk" , 100/portTICK_PERIOD_MS, 	pdFALSE, 0, vTimerRelease);
	event_connect_callback(eEVENT_SWRC_MULTIPLE_CHANGED, swc_handle_multiple_buttons);
}


#define MULTIPLE_SENT_VAL 5
static uint8_t multiple_sent = 0;
void event_swc_emit(enum SWC status, enum SWC_MODULE module){
    struct BASE_EVENTS* base_events = base_event_get();
    if ((base_events->swc != status) || (event_can_be_forwarded(eEVENT_SWRC_CHANGED) != 0)) {
        event_lock_forward(eEVENT_SWRC_CHANGED);
        base_events->swc = status;

        union EVENT event;
        if (status > SWC_MULTIPLE_BUTTON_START){
            event.as_generic.event = eEVENT_SWRC_MULTIPLE_CHANGED;
            multiple_sent = MULTIPLE_SENT_VAL;
        }else{
            event.as_generic.event = eEVENT_SWRC_CHANGED;
        }
        event.as_swrc.btn.swc = status;
		event.as_swrc.btn.counter = 0;
        event.as_swrc.module = module;
        event_emit(&event);

        ///Se è un button none lo invio per alcune volte anche sul swrc multiple.
        if (event.as_swrc.btn.swc == SWC_NO_BUTTON_PRESSED){
            if (multiple_sent > 0){
                event.as_generic.event = eEVENT_SWRC_MULTIPLE_CHANGED;
                event_emit(&event);
                multiple_sent--;
            }
        }
    }
}

/**
 * @}
 */
