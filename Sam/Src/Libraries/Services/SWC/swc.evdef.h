#pragma once

#include <stdint.h>

/**
 * @brief Definizione degli eventi generabili dai comandi a volante della vettura.
 *
 */
enum SWC {
    SWC_NO_BUTTON_PRESSED,
    SWC_VOL_UP_PRESSED, //!< BUTTON_VOL_UP_PRESSED Pressione del Vol+.
    SWC_VOL_UP_RELEASED, //!< SWC_VOL_UP_RELEASED Rilascio del Vol+.
    SWC_VOL_DOWN_PRESSED, //!< BUTTON_VOL_DOWN_PRESSED Pressione del Vol-.
    SWC_VOL_DOWN_RELEASED, //!< BUTTON_VOL_DOWN_RELEASED Rilascio del Vol-.
    SWC_SEEK_UP_PRESSED, //!< BUTTON_SEEK_UP_PRESSED Pressione del Seek+.
    SWC_SEEK_UP_RELEASED, //!< BUTTON_SEEK_UP_RELEASED Rilascio del Seek+.
    SWC_SEEK_DOWN_PRESSED, //!< BUTTON_SEEK_DOWN_PRESSED Pressione del Seek-.
    SWC_SEEK_DOWN_RELEASED, //!< BUTTON_SEEK_DOWN_RELEASED Rilascio del Seek-.
    SWC_SOURCE_PRESSED, //!< BUTTON_SOURCE_PRESSED Pressione del Source.
    SWC_SOURCE_RELEASED, //!< BUTTON_SOURCE_RELEASED Rilascio del Source.
    SWC_MUTE_PRESSED, //!< BUTTON_MUTE_PRESSED Pressione del mute.
    SWC_MUTE_RELEASED, //!< BUTTON_MUTE_RELEASED Rilascio del mute.
    SWC_PRESET_UP_PRESSED, //!< BUTTON_PRESET_UP_PRESSED Pressione del Preset+.
    SWC_PRESET_UP_RELEASED, //!< BUTTON_PRESET_UP_RELEASED Rilascio del Preset+.
    SWC_PRESET_DOWN_PRESSED, //!< BUTTON_PRESET_DOWN_PRESSED Pressione del Preset-.
    SWC_PRESET_DOWN_RELEASED, //!< BUTTON_PRESET_DOWN_RELEASED Rilascio del Preset-.
    SWC_TEL_PICKUP_PRESSED, //!< BUTTON_TEL_PICKUP_PRESSED Pressione del tasto risposta al telefono.
    SWC_TEL_PICKUP_RELEASED, //!< BUTTON_TEL_PICKUP_RELEASED Rilascio del tasto risposta al telefono.
    SWC_TEL_HANGUP_PRESSED, //!< BUTTON_TEL_HANGUP_PRESSED Pressione del tasto riaggancio telefono.
    SWC_TEL_HANGUP_RELEASED, //!< BUTTON_TEL_HANGUP_RELEASED Rilascio del tasto riaggancio telefono.
    SWC_SPEECH_PRESSED, //!< BUTTON_SPEECH_PRESSED Pressione del tasto Voice del telefono.
    SWC_SPEECH_RELEASED, //!< BUTTON_SPEECH_RELEASED Rilascio del tasto Voice del telefono.
    SWC_IGNITION_PRESSED,		//!< Pressione di un comando ignition
	SWC_IGNITION_RELEASED,		//!< Rilascio di un comando ignition
	SWC_CAMERA_PRESSED,
	SWC_CAMERA_RELEASED,
    SWC_MULTIPLE_BUTTON_START, //!< SWC_MULTIPLE_BUTTON_START flag per indicare che da qui in poi sono tasti multipli
    SWC_MULTIPLE_BUTTON_SPARE, //!< SWC_MULTIPLE_BUTTON_SPARE posto riservato per mantenere dispari / pari press /release.
    SWC_TEL_PICKUP_SPEECH_PRESSED, //!< SWC_TEL_PICKUP_SPEECH_PRESSED
    SWC_TEL_PICKUP_SPEECH_RELEASED, //!< SWC_TEL_PICKUP_SPEECH_RELEASED
    SWC_TEL_PICKUP_HANGUP_PRESSED, //!< SWC_TEL_PICKUP_SPEECH_PRESSED
    SWC_TEL_PICKUP_HANGUP_RELEASED, //!< SWC_TEL_PICKUP_SPEECH_RELEASED
    SWC_NO_COMMAND_PRESSED, //!< SWC_NO_COMMAND_PRESSEED Un comando a volante che non invia nulla verso la radio. Usato per eseguire comandi sulla centralina.
    SWC_NO_COMMAND_RELEASED, //!< SWC_NO_COMMAND_RELEASED
    SWC_MUTE_IGNITION_PRESSED,
	SWC_MUTE_IGNITION_RELEASED,
};

struct SWC_EV {
    enum SWC swc;
    uint8_t counter;
};

enum SWC_MODULE{
	SWC_MODULE_NONE = 0,
	SWC_MODULE_IR,
	SWC_MODULE_FASCIA,
	SWC_MODULE_VEHICLE,
    SWC_MODULE_COUNT,
};

/**
 * @brief evento emesso alla pressione/rilascio di un comando a volante
 */
struct EVENT_SWRC {
    /**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Evento pulsante
	 */
    struct SWC_EV btn;
    /**
	 * @brief modulo che trasmette il dato.
	 * 
	 */
	enum SWC_MODULE module;
};

struct EVENT_ANALOG_SWRC{
	/**
	 * Evento generico.
	 */
    struct EVENT_GENERIC generic;
    /**
	 * Evento pulsante
	 */
    struct SWC_EV btn;
};

void event_swc_emit(enum SWC status, enum SWC_MODULE module);
void event_swc_emit_multiple(enum SWC status, uint8_t rep);