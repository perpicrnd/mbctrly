#pragma once

#include "event_list.h"
/**
 * @defgroup SWC
 * @brief Gestione del sistema di comandi a volante del veicolo.
 * @{
 */





/**
 * @brief Inizializzazione della gestione dei comandi a volante.
 */
void swc_init(void);

/**
 * @}
 */
