#pragma once

/**
 * @brief evento di tipo velocità veicolo
 */
struct EVENT_SPEED{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Velocità del veicolo in km/h
	 */
	uint32_t speed;
};

/**
 * @brief emette un messaggio con la velocità attuale.
 * @param speed la velocità attuale.
 */
void event_speed_emit(uint32_t speed);
