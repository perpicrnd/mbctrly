#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "speed.c"
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void){
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

void vApplicationIdleHook(void){
//Make the linker happy.
}


void software_reset(void){

}


int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

extern bool speed_hw_configured;
extern uint32_t speed_hw_value;
extern bool speed_enabled;

void test_speed(){
	speed_init();
	assert_true(speed_hw_configured);
	assert_true(speed_hw_value == 0);

	union EVENT event;
	event.as_generic.event = eEVENT_SPEED_CHANGED;
	event.as_speed.speed = 10;
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	event.as_speed.speed = 30;
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	event.as_speed.speed = 0;
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	event.as_speed.speed = 70;
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	event.as_speed.speed = 150;
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(150/50));
	assert_true(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(150/50));
	assert_true(speed_enabled);

	event.as_speed.speed = 300;
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(300/50));
	assert_true(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(300/50));
	assert_true(speed_enabled);

	event.as_speed.speed = 310;
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(300/50));
	assert_true(speed_enabled);
	handle_speed(&event);
	assert_true(speed_hw_value == 60000/(300/50));
	assert_true(speed_enabled);
}

void test_speed_2(){
	speed_init();
	union EVENT event;
	event.as_generic.event = eEVENT_SPEED_CHANGED;
	event.as_speed.speed = 0;
	handle_speed(&event);
	assert_true(speed_hw_value == 0);
	assert_false(speed_enabled);

	for(uint16_t i=0; i<65000; i++){
		event.as_speed.speed = i;
		handle_speed(&event);

		if (i<100){
			assert_true(speed_hw_value == 0);
			assert_false(speed_enabled);
		}else{
			uint16_t data = 60000/(i/50);
			assert_true(speed_hw_value == data);
			assert_true(speed_enabled);
		}
	}

	for(int16_t i=65000; i>=0; i-=40){
		event.as_speed.speed = i;
		handle_speed(&event);

		if (i<100){
			assert_true(speed_hw_value == 0);
			assert_false(speed_enabled);
		}else{
			uint16_t data = 60000/(i/50);
			assert_true(speed_hw_value == data);
			assert_true(speed_enabled);
		}
	}
}


void __wrap_event_emit(union EVENT *event){
	(void)event;
}

static void set_odo_at(uint8_t j){
	for(uint8_t i=0; i<ODO_STEP; i++){
		odo_stack[i] = j;
	}
}

void test_speed_in(){

	set_odo_at(2);
	vWiredOdoCheck(NULL);

	//Se non cambia per n chiamate consecutive non viene generato l'evento.
	set_odo_at(2);
	vWiredOdoCheck(NULL);

	set_odo_at(2);
	vWiredOdoCheck(NULL);

	set_odo_at(2);
	vWiredOdoCheck(NULL);

	set_odo_at(2);
	vWiredOdoCheck(NULL);

	//Al cambio viene generato l'evento.
	set_odo_at(10);
	vWiredOdoCheck(NULL);

	set_odo_at(10);
	vWiredOdoCheck(NULL);

	set_odo_at(10);
	vWiredOdoCheck(NULL);

	set_odo_at(10);
	vWiredOdoCheck(NULL);

	set_odo_at(10);
	vWiredOdoCheck(NULL);

	//Ricambio.
	set_odo_at(30);
	vWiredOdoCheck(NULL);

	set_odo_at(0);
	vWiredOdoCheck(NULL);


}

int setup(void **state){
	(void)state;
	for(uint8_t i=0; i<ODO_STEP; i++){
		odo_stack[i] = 0;
	}
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
			cmocka_unit_test_setup(test_speed, setup),
			cmocka_unit_test_setup(test_speed_2, setup),
			cmocka_unit_test_setup(test_speed_in, setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
