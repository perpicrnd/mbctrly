#include "asf.h"

#include "logger.h"
#include "speed.h"

#define PWM_MIN_FREQ 1
#define MAX_SPEED_PWM 300

#define PWM_CH_SPEED PWM_CHANNEL_7

static pwm_channel_t pwm_channel_speed;

void speed_hw_set(bool enable, uint32_t speed) {
    (void)enable;
    uint32_t period = speed;
    uint32_t duty = period / 2;
    if (!pwm_channel_update_period(PWM, &pwm_channel_speed, period)) {
        pwm_channel_update_duty(PWM, &pwm_channel_speed, duty);
    } else {
        pwm_channel_update_duty(PWM, &pwm_channel_speed, duty);
        pwm_channel_update_period(PWM, &pwm_channel_speed, period);
    }
}

#define PERIOD_VALUE 100
#define PWM_FREQUENCY 300
#define INIT_DUTY_VALUE 50
void speed_hw_init(void) {
    pmc_enable_periph_clk(ID_PWM);
    pwm_channel_disable(PWM, PWM_CH_SPEED);
    pmc_enable_periph_clk(ID_PWM);

    /* Set PWM clock A as PWM_FREQUENCY*PERIOD_VALUE (clock B is not used) */
    pwm_clock_t clock_setting = {
        .ul_clka = PWM_FREQUENCY * PERIOD_VALUE,
        .ul_clkb = 0,
        .ul_mck = sysclk_get_cpu_hz()};
    if (pwm_init(PWM, &clock_setting) != 0) {
        LOG_DEF_ERROR("Invalid PWM configuration\r\n");
    }

    /* Initialize PWM channel for LED0 */
    /* Period is left-aligned */
    pwm_channel_speed.alignment = PWM_ALIGN_LEFT;
    /* Output waveform starts at a low level */
    pwm_channel_speed.polarity = PWM_HIGH;
    /* Use PWM clock A as source clock */
    pwm_channel_speed.ul_prescaler = PWM_CMR_CPRE_CLKA;
    /* Period value of output waveform */
    pwm_channel_speed.ul_period = PERIOD_VALUE;
    /* Duty cycle value of output waveform */
    pwm_channel_speed.ul_duty = INIT_DUTY_VALUE;
    pwm_channel_speed.channel = PWM_CH_SPEED;
    pwm_channel_init(PWM, &pwm_channel_speed);
    pwm_channel_enable(PWM, PWM_CH_SPEED);
}

bool wired_odo_pin_level(void) {
    return ioport_get_pin_level(Spd_In);
}

/**
 * @brief Inizializzazione dell'interrupt odometro filare.
 */
#define PIO_IER_PA21 0x200000
void wired_odo_hw_init(void) {
    ioport_set_pin_dir(Spd_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Spd_In, IOPORT_MODE_PULLUP);
    ioport_set_pin_sense_mode(Spd_In, IOPORT_SENSE_FALLING);
    pio_configure_interrupt(PIOA, Spd_In, PIO_IT_EDGE);
    pio_enable_interrupt(PIOA, PIO_IER_PA21);
    NVIC_EnableIRQ(PIOA_IRQn);
}
