#include <stdbool.h>
#include "speed.h"

bool speed_hw_configured = false;
bool speed_enabled = false;

uint32_t speed_hw_value = 0;
void speed_hw_init(void){
	speed_hw_configured = true;
	speed_enabled = false;
}

void speed_hw_set(bool enable, uint32_t speed){
	speed_hw_value = speed;
	speed_enabled = enable;

}

bool wired_odo_pin_level(void){
    return true;
}