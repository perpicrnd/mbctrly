#include "speed.h"
#include "event_list.h"
#include "logger.h"

#include "FreeRTOS.h"
#include "difference.h"
#include "service.h"
#include "timers.h"

static uint32_t last_good_speed = 0;
static void handle_speed(const union EVENT* const msg) {
    uint32_t speed = msg->as_speed.speed;

    if (DIFF(last_good_speed, speed) >= 20) {
        LOG_DEF_NORMAL("Speed now %f\r\n", speed / 100.0);
    }

    if (speed < 100) {
        ///Fino a 1 km/h non calcolo la velocità... Sono fermo.
        speed_hw_set(false, 0);
        last_good_speed = 0;
    } else {
        if (DIFF(last_good_speed, speed) >= 50) {
            ///Non posso avere mai un diviso 0 perché speed è almeno 100 dam/h
            speed_hw_set(true, 60000 / (speed / 50));
            event_out_service_emit();
            last_good_speed = speed;
        }
    }
}

uint32_t get_hw_speed(void) {
    return last_good_speed;
}

#define MS_CHECK_SPEED 100
#define ODO_STEP 10
static TimerHandle_t wired_odo;
volatile uint16_t counter_odo = 0;

static uint16_t odo_stack[ODO_STEP] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint8_t i = 0;
static uint32_t old_speed = 0;
static void vWiredOdoCheck(TimerHandle_t pxTimer) {
    (void)pxTimer;
    uint32_t tmp = 0;

    portDISABLE_INTERRUPTS();
    odo_stack[i] = counter_odo;
    counter_odo = 0;
    portENABLE_INTERRUPTS();
    i++;
    i = i % ODO_STEP;
    for (uint8_t j = 0; j < ODO_STEP; j++) {
        tmp += odo_stack[j];
    }
    if (tmp != old_speed) {
        union EVENT event;
        event.as_generic.event = eEVENT_WIRED_SPEED_CHANGED;
        event.as_speed.speed = tmp;
        event_emit(&event);
        old_speed = tmp;
    }
}

static bool configured = false;
void speed_init(void) {
    if (!configured) {
        speed_hw_init();
        event_connect_callback(eEVENT_SPEED_CHANGED, handle_speed);
        wired_odo = xTimerCreate("t_wir", MS_CHECK_SPEED / portTICK_PERIOD_MS, pdTRUE, 0, vWiredOdoCheck);
        xTimerStart(wired_odo, 0);
        configured = true;
        speed_hw_set(false, 0);
    }
}

void event_speed_emit(uint32_t status) {
    struct BASE_EVENTS* base_events = base_event_get();
    if ((DIFF(base_events->speed, status) >= 2) || ((event_can_be_forwarded(eEVENT_SPEED_CHANGED) != 0))) {
        event_lock_forward(eEVENT_SPEED_CHANGED);
        base_events->speed = status;

        union EVENT event;
        event.as_generic.event = eEVENT_SPEED_CHANGED;
        event.as_speed.speed = status;
        event_emit(&event);
    }
}
