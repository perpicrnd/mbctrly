#pragma once
#include <stdbool.h>
#include <stdint.h>

#include "event_list.h"

void speed_init(void);
void speed_hw_init(void);
void speed_hw_set(bool enable, uint32_t speed);
void wired_odo_hw_init(void);
uint32_t get_hw_speed(void);
bool wired_odo_pin_level(void);