#pragma once

#include "event_list.h"


void reverse_init(void);
void reverse_hw_init(void);
void reverse_hw_set(enum REVERSE_STATUS reverse);
bool reverse_hw_get(void);

enum REVERSE_STATUS get_hw_reverse(void);
enum REVERSE_STATUS reverse_wired_in(void);

