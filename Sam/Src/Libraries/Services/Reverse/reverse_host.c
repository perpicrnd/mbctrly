#include <stdbool.h>

#include "reverse.h"

bool reverse_hw_configured = false;
enum REVERSE_STATUS reverse_hw_value = REVERSE_OFF;

void reverse_hw_init(void) {
    reverse_hw_configured = true;
}

void reverse_hw_set(enum REVERSE_STATUS reverse) {
    reverse_hw_value = reverse;
}

enum REVERSE_STATUS get_hw_reverse(void) {
    if (reverse_hw_value) {
        return REVERSE_ON;
    } else {
        return REVERSE_OFF;
    }
}

enum REVERSE_STATUS reverse_wired_in(void) {
    return REVERSE_ON;
}

bool reverse_hw_get(void) {
    if (reverse_hw_value == REVERSE_ON) {
        return true;
    } else {
        return false;
    }
}