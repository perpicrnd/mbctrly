#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "reverse.c"
#include "settings.h"
enum BUTTON_CONFIGURATION get_default_vehicle_button_configuration(void) {
    LOG_DEF_NORMAL("%s Override this function in your vehicle.\r\n", __PRETTY_FUNCTION__);
    return BUTTON_CONFIGURATION_HORIZONTAL_LEFT;
}

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void __wrap_event_emit(union EVENT *event) {
    (void)event;
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected(handler);
}

enum IGNITION_STATUS __wrap_vehicle_requested_ignition(void) {
    return mock_type(enum IGNITION_STATUS);
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

extern bool reverse_hw_configured;
extern enum REVERSE_STATUS reverse_hw_value;

void test_reverse(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    assert_true(reverse_hw_configured);
    assert_true(reverse_hw_value == REVERSE_OFF);

    union EVENT event;
    event.as_generic.event = eEVENT_REVERSE_CHANGED;
    event.as_reverse.reverse = REVERSE_ON;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    event.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
}

void test_reverse_off_delay(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    reverse_activity_timeout = 20;
    union EVENT second;
    requested_reverse = REVERSE_OFF;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 0);

    requested_reverse = REVERSE_ON;
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 10);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 10);

    requested_reverse = REVERSE_OFF;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 9);

    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 8);

    union EVENT event;
    event.as_generic.event = eEVENT_REVERSE_CHANGED;
    event.as_reverse.reverse = REVERSE_ON;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    event.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    off_reverse_timeout = 0;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
}

void test_reverse_on_delay(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    union EVENT second;
    requested_reverse = REVERSE_ON;
    reverse_activity_timeout = 20;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(on_reverse_timeout == 0);

    requested_reverse = REVERSE_OFF;
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 10);
    handle_second_changed(&second);
    assert_true(on_reverse_timeout == 10);

    requested_reverse = REVERSE_ON;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(on_reverse_timeout == 9);

    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(on_reverse_timeout == 8);

    union EVENT event;
    event.as_generic.event = eEVENT_REVERSE_CHANGED;
    event.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);

    event.as_reverse.reverse = REVERSE_ON;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);

    on_reverse_timeout = 0;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);
}

void test_reverse_delay_speed(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    union EVENT second;
    reverse_activity_timeout = 20;
    requested_reverse = REVERSE_OFF;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_NORMAL);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 0);
    assert_true(on_reverse_timeout == 0);

    requested_reverse = REVERSE_ON;
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 10);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 10);

    requested_reverse = REVERSE_OFF;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 9);

    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 8);

    union EVENT event;
    event.as_generic.event = eEVENT_REVERSE_CHANGED;
    event.as_reverse.reverse = REVERSE_ON;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    event.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    union EVENT speed;
    speed.as_generic.event = eEVENT_SPEED_CHANGED;
    speed.as_speed.speed = 100;

    handle_speed_changed(&speed);
    assert_true(off_reverse_timeout == 8);

    speed.as_speed.speed = 1000;
    handle_speed_changed(&speed);
    assert_true(off_reverse_timeout == 8);

    speed.as_speed.speed = 2000;
    handle_speed_changed(&speed);
    assert_true(off_reverse_timeout == 0);

    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
}

void test_reverse_delay_on_with_reverse_already_on(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    on_reverse_timeout = 0;
    off_reverse_timeout = 10;
    last_reverse = REVERSE_ON;

    union EVENT event_rev_on;
    event_rev_on.as_generic.event = eEVENT_REVERSE_CHANGED;
    event_rev_on.as_reverse.reverse = REVERSE_ON;

    union EVENT event_rev_off;
    event_rev_off.as_generic.event = eEVENT_REVERSE_CHANGED;
    event_rev_off.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event_rev_on);
    assert_true(reverse_hw_value == REVERSE_ON);

    handle_reverse(&event_rev_off);
    assert_true(reverse_hw_value == REVERSE_ON);

    on_reverse_timeout = 10;
    //Mi aspetto che rimanga ad ON perchè è già ad ON.
    handle_reverse(&event_rev_on);
    assert_true(reverse_hw_value == REVERSE_ON);
}

void test_reverse_delay_off_with_reverse_already_off(void **state) {
    (void)state;
    will_return_always(__wrap_vehicle_requested_ignition, IGNITION_ON);
    on_reverse_timeout = 10;
    off_reverse_timeout = 0;
    last_reverse = REVERSE_OFF;

    union EVENT event_rev_on;
    event_rev_on.as_generic.event = eEVENT_REVERSE_CHANGED;
    event_rev_on.as_reverse.reverse = REVERSE_ON;

    union EVENT event_rev_off;
    event_rev_off.as_generic.event = eEVENT_REVERSE_CHANGED;
    event_rev_off.as_reverse.reverse = REVERSE_OFF;

    handle_reverse(&event_rev_off);
    assert_true(reverse_hw_value == REVERSE_OFF);

    handle_reverse(&event_rev_on);
    assert_true(reverse_hw_value == REVERSE_OFF);

    off_reverse_timeout = 10;
    //Mi aspetto che rimanga ad ON perchè è già ad ON.
    handle_reverse(&event_rev_off);
    assert_true(reverse_hw_value == REVERSE_OFF);
}

void test_reverse_delay_remove_ignition(void **state) {
    (void)state;

    union EVENT second;
    requested_reverse = REVERSE_OFF;
    reverse_activity_timeout = 3;
    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 0);

    requested_reverse = REVERSE_ON;
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, 10);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 10);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 10);

    requested_reverse = REVERSE_OFF;
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);
    assert_true(off_reverse_timeout == 9);

    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, 0);
    will_return(__wrap_settings_read, RADIO_SETTING_REVERSE_DELAY);
    will_return(__wrap_settings_read, 0);
    handle_second_changed(&second);

    assert_true(off_reverse_timeout == 0);

    union EVENT event;
    event.as_generic.event = eEVENT_REVERSE_CHANGED;
    event.as_reverse.reverse = REVERSE_ON;

    will_return(__wrap_vehicle_requested_ignition, IGNITION_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_ON);

    event.as_reverse.reverse = REVERSE_OFF;

    will_return(__wrap_vehicle_requested_ignition, IGNITION_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    will_return(__wrap_vehicle_requested_ignition, IGNITION_ON);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);

    will_return(__wrap_vehicle_requested_ignition, IGNITION_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
    will_return(__wrap_vehicle_requested_ignition, IGNITION_OFF);
    handle_reverse(&event);
    assert_true(reverse_hw_value == REVERSE_OFF);
}

void test_reverse_init(void **state) {
    (void)state;

    expect_value(__wrap_event_connect_callback, event, eEVENT_REVERSE_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_reverse);
    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_second_changed);
    expect_value(__wrap_event_connect_callback, event, eEVENT_SPEED_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_speed_changed);
    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_change);
    reverse_init();
}

int setup(void **state) {
    (void)state;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_reverse_init, setup),
        cmocka_unit_test_setup(test_reverse, setup),
        cmocka_unit_test_setup(test_reverse_off_delay, setup),
        cmocka_unit_test_setup(test_reverse_on_delay, setup),
        cmocka_unit_test_setup(test_reverse_delay_speed, setup),
        cmocka_unit_test_setup(test_reverse_delay_remove_ignition, setup),
        cmocka_unit_test_setup(test_reverse_delay_on_with_reverse_already_on, setup),
        cmocka_unit_test_setup(test_reverse_delay_off_with_reverse_already_off, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
