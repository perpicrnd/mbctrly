#pragma once

/**
 * @brief Definizione dei possibili stati della retromarcia
 */
enum REVERSE_STATUS{
	REVERSE_ON, //!< REVERSE_ON retromarcia inserita
	REVERSE_OFF,//!< REVERSE_OFF retromarcia non inserita
};

/**
 * @brief evento di tipo reverse
 */
struct EVENT_REVERSE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato della retromarcia.
	 */
	enum REVERSE_STATUS reverse;
};

/**
 * @brief emette un messaggio con lo stato della retromarcia.
 * @param status lo stato della retromarcia da inviare.
 */
void event_reverse_emit(enum REVERSE_STATUS status);