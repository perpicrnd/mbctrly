#include "asf.h"

#include "reverse.h"

void reverse_hw_set(enum REVERSE_STATUS reverse){
	if (reverse == REVERSE_ON){
		ioport_set_pin_level(Rev_Out, true);
	}else{
		ioport_set_pin_level(Rev_Out, false);
	}
}

enum REVERSE_STATUS get_hw_reverse(void){
    if (ioport_get_pin_level(Rev_Out)){
        return REVERSE_ON;
    }else{
        return REVERSE_OFF;
    }
}

bool reverse_hw_get(void){
    return ioport_get_pin_level(Rev_Out);
}

void reverse_hw_init(void){
	ioport_set_pin_dir(Rev_Out, IOPORT_DIR_OUTPUT);
}

