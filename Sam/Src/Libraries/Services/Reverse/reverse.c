#include "reverse.h"
#include "FreeRTOS.h"
#include "alpine_menu.h"
#include "event_list.h"
#include "event_list_provider.h"
#include "logger.h"
#include "service.h"
#include "settings.h"
#include "timers.h"

static uint8_t off_reverse_timeout = 0;
static uint8_t on_reverse_timeout = 0;
static uint8_t reverse_activity_timeout = 3;
static enum REVERSE_STATUS last_reverse = REVERSE_OFF;
static enum REVERSE_STATUS requested_reverse = REVERSE_OFF;
static enum RADIO_SOURCE_TYPE current_source_data;

static void handle_source_change(const union EVENT* const msg) {
    current_source_data = msg->as_radio_source.source_type;
}

static void handle_reverse(const union EVENT* const msg) {
    enum REVERSE_STATUS rev = msg->as_reverse.reverse;
    requested_reverse = rev;

    reverse_activity_timeout = 3;

    if (rev == REVERSE_OFF) {
        if (reverse_hw_get() == true) {
            if (off_reverse_timeout != 0) {
                rev = REVERSE_ON;
            }
        }
    } else {
        if (reverse_hw_get() == false) {
            if (on_reverse_timeout != 0) {
                rev = REVERSE_OFF;
            }
        }
    }

    //Spengo esplicitamente se non ho il veicolo acceso.
    if (vehicle_requested_ignition() == IGNITION_OFF) {
        rev = REVERSE_OFF;
    }

    reverse_hw_set(rev);

    if (rev != last_reverse) {
        event_out_service_emit();
        if (((rev == REVERSE_OFF) && (current_source_data == RADIO_SOURCE_HDMI)) ||
            ((rev == REVERSE_ON)&&(current_source_data != RADIO_SOURCE_HDMI))){
            event_source_changer_emit(RADIO_SOURCE_HDMI);
        }
        if (rev == REVERSE_ON) {
            LOG_DEF_NORMAL("REVERSE_ON\r\n");
        }
        else {
            LOG_DEF_NORMAL("REVERSE_OFF\r\n");
        }
        last_reverse = rev;
    }
}

static void handle_second_changed(const union EVENT* const msg) {
    if (!reverse_activity_timeout) {
        off_reverse_timeout = 0;
        on_reverse_timeout = 0;
        reverse_activity_timeout = 3;
        last_reverse = REVERSE_OFF;
        requested_reverse = REVERSE_OFF;
        reverse_hw_set(requested_reverse);
    } else {
        reverse_activity_timeout--;
    }

    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (settings_read(RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE) > 30) {
            settings_write(RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE, 0);
        }
        if (settings_read(RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE) > 30) {
            settings_write(RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE, 0);
        }

        if (requested_reverse == REVERSE_ON) {
            if (settings_read(RADIO_SETTING_REVERSE_BYTE) == RADIO_SETTING_REVERSE_DELAY) {
                off_reverse_timeout = settings_read(RADIO_SETTING_EXIT_REVERSE_DELAY_BYTE);
            }
            if (on_reverse_timeout > 0) {
                on_reverse_timeout--;
                LOG_DEF_NORMAL("Delayed on reverse %d seconds remaining\r\n", on_reverse_timeout);
            }
        } else {
            if (settings_read(RADIO_SETTING_REVERSE_BYTE) == RADIO_SETTING_REVERSE_DELAY) {
                on_reverse_timeout = settings_read(RADIO_SETTING_ENTER_REVERSE_DELAY_BYTE);
            }
            if (off_reverse_timeout > 0) {
                off_reverse_timeout--;
                LOG_DEF_NORMAL("Delayed off reverse %d seconds remaining\r\n", off_reverse_timeout);
            }
        }
    }
}

static void handle_speed_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SPEED_CHANGED) {
        if (msg->as_speed.speed > 1400) {
            off_reverse_timeout = 0;
        }
    }
}

void reverse_init(void) {
    reverse_hw_init();
    event_connect_callback(eEVENT_REVERSE_CHANGED, handle_reverse);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
    event_connect_callback(eEVENT_SPEED_CHANGED, handle_speed_changed);
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_change);
    reverse_hw_set(REVERSE_OFF);
}

void event_reverse_emit(enum REVERSE_STATUS status) {
    struct BASE_EVENTS* base_events = base_event_get();
    if ((base_events->reverse != status) || ((event_can_be_forwarded(eEVENT_REVERSE_CHANGED) != 0))) {
        event_lock_forward(eEVENT_REVERSE_CHANGED);
        base_events->reverse = status;

        union EVENT event;
        event.as_generic.event = eEVENT_REVERSE_CHANGED;
        event.as_reverse.reverse = status;
        event_emit(&event);
    }
}
