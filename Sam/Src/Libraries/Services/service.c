#include <stdio.h>

#include "FreeRTOS.h"
#include "handbrake.h"
#include "ignition.h"
#include "lights.h"
#include "logger.h"
#include "reverse.h"
#include "service.h"
#include "speed.h"
#include "timers.h"

uint64_t def_event_can_be_forwarded;

static struct BASE_EVENTS base_events;

bool event_can_be_forwarded(enum EVENTS event) {
    return (def_event_can_be_forwarded & 1 << event) != 0;
}

void event_lock_forward(enum EVENTS event) {
    def_event_can_be_forwarded &= ~(1 << event);
}

struct BASE_EVENTS* base_event_get(void) {
    return &base_events;
}

static void accessory_timer(TimerHandle_t pxTimer) {
    (void)pxTimer;
    def_event_can_be_forwarded = -1;
}

void event_ignition_emit(enum IGNITION_STATUS status) {
    struct BASE_EVENTS* base_events = base_event_get();
    if ((base_events->ignition != status) || ((event_can_be_forwarded(eEVENT_IGNITION_CHANGED) != 0))) {
        event_lock_forward(eEVENT_IGNITION_CHANGED);
        base_events->ignition = status;
        union EVENT event;
        event.as_generic.event = eEVENT_IGNITION_CHANGED;
        event.as_ignition.ignition = status;
        event_emit(&event);
    }
}

static TimerHandle_t update_timer;
void event_out_service_emit(void) {
    union EVENT event;
    event.as_out_service.out_service.ignition = get_hw_ignition();
    event.as_out_service.out_service.lights = get_hw_lights();
    event.as_out_service.out_service.reverse = get_hw_reverse();
    event.as_out_service.out_service.speed = get_hw_speed();
    event.as_out_service.out_service.handbrake = get_hw_handbrake();
    event.as_generic.event = eEVENT_OUT_SERVICE_CHANGED;
    event_emit(&event);
    xTimerStart(update_timer, 10 / portTICK_PERIOD_MS);
}

static void update_timer_func(TimerHandle_t pxTimer) {
    (void)pxTimer;
    event_out_service_emit();
}

static TimerHandle_t access_timer;

static bool configured = false;
void service_init(void) {
    LOG_DEF_NORMAL("Service init\r\n");
    handbrake_init();
    ignition_init();
    lights_init();
    reverse_init();
    speed_init();

    if (!configured) {
        access_timer = xTimerCreate((char*)"t_brk", 100 / portTICK_PERIOD_MS, pdTRUE, 0, accessory_timer);
        update_timer = xTimerCreate((char*)"t_upd", 250 / portTICK_PERIOD_MS, pdTRUE, 0, update_timer_func);
        xTimerStart(access_timer, 10);
        configured = true;
    }
}
