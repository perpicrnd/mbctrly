#pragma once

#include <stdint.h>

enum SWC_ANALOG_TYPE{
    SWC_ANALOG_VOLTAGE,
    SWC_ANALOG_OHM,
};

struct SWC_ANALOG_CONVERSION{
    uint32_t channel_a;
    enum SWC_ANALOG_TYPE type_channel_a;
    uint32_t channel_b;
    enum SWC_ANALOG_TYPE type_channel_b;
};

struct EVENT_ANALOG_VALUE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Messaggio canbus ricevuto.
	 */
	struct SWC_ANALOG_CONVERSION swc_analog;
};

void event_analog_value_emit(const struct SWC_ANALOG_CONVERSION * const ev);



