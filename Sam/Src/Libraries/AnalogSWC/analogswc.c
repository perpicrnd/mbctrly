
#include <stdbool.h>
#include <stdlib.h>

#include "analogswc.h"
#include "analogswc_priv.h"
#include "board_version.h"
#include "difference.h"
#include "logger.h"

struct SWC_CHANNEL swc_channels[SWC_CHANNELS];
static TimerHandle_t timer;

static uint32_t adc_counter_tick = 0;
static bool adc_enabled = false;

void adc_counter_increase(void) {
    if (adc_counter_tick < 4000 / TIMER_TICK) {
        adc_counter_tick++;
    } else {
        adc_enabled = true;
    }
}

bool adc_is_enabled(void) {
    return adc_enabled;
}

void analogswc_add_value(struct SWC_CHANNEL *channel, uint16_t value) {
    channel->adc_buff_pos++;
    if (channel->adc_buff_pos >= ADC_BUFF_LEN) {
        channel->adc_buff_pos = 0;
    }
    channel->adc_read_values[channel->adc_buff_pos] = value;
    // LOG_DEF_NORMAL("%d \r\n", value);
    if (channel->to_skip > 0) {
        channel->to_skip--;
    }
    // LOG_DEF_NORMAL("%d \r\n", channel->to_skip);
    // LOG_DEF_NORMAL("%s %d %d %d %d skip: %d\r\n", __PRETTY_FUNCTION__, channel->adc_read_values[0], channel->adc_read_values[1], channel->adc_read_values[2], channel->adc_read_values[3], channel->to_skip);
}

bool analogswc_channel_ready(struct SWC_CHANNEL *channel) {
    bool retVal = false;
    if (channel->to_skip == 0) {
        retVal = true;
    }
    return retVal;
}

void analogswc_channel_debounce(struct SWC_CHANNEL *channel, uint16_t cur_adc) {
    channel->adc_read_mid = (((uint32_t)channel->adc_read_mid) + cur_adc) >> 1;
}

bool analogswc_channel_is_stable(struct SWC_CHANNEL *channel, uint16_t cur_adc) {
    bool retVal = false;
    uint16_t range = cur_adc >> 4;
    if (range < 10) {
        range = 10;
    }
    if (DIFF(cur_adc, channel->adc_read_mid) < range) {
        retVal = true;
    }

    return retVal;
}

void analogswc_activate_scale(struct SWC_CHANNEL *channel, uint8_t num) {
    if (channel->scale_available) {
        switch (num) {
            case 1:

                // LOG_DEF_NORMAL("%s 1 on channel %uzd\r\n", __PRETTY_FUNCTION__, (unsigned int)channel->adc_channel);
                scale_output(channel->an_scale_1, false);
                scale_output(channel->an_scale_2, false);
                break;
            case 2:
                // LOG_DEF_NORMAL("%s 2 on channel %uzd\r\n", __PRETTY_FUNCTION__, (unsigned int)channel->adc_channel);
                scale_output(channel->an_scale_1, true);
                scale_output(channel->an_scale_2, false);
                break;
            default:
                // LOG_DEF_NORMAL("%s 3 on channel %uzd\r\n", __PRETTY_FUNCTION__, (unsigned int)channel->adc_channel);
                scale_output(channel->an_scale_1, true);
                scale_output(channel->an_scale_2, true);
                num = 3;
                break;
        }
        channel->current_scale = num;
        channel->to_skip = ADC_BUFF_LEN + 1;
    }
}

void analogswc_check_if_change_scale_needed(struct SWC_CHANNEL *channel) {
    if (channel->scale_available) {
        if (channel->to_skip == 0) {
            switch (channel->current_scale) {
                case 1:
                    if (channel->adc_read_values[channel->adc_buff_pos] > 0xA00) {
                        analogswc_activate_scale(channel, 2);
                        // LOG_DEF_NORMAL("CH 1 %X\r\n", channel->adc_read_values[channel->adc_buff_pos]);
                    }
                    break;
                case 2:
                    if (channel->adc_read_values[channel->adc_buff_pos] < 0x300) {
                        analogswc_activate_scale(channel, 1);
                        // LOG_DEF_NORMAL("CH 2 %X\r\n", channel->adc_read_values[channel->adc_buff_pos]);
                    } else if (channel->adc_read_values[channel->adc_buff_pos] > 0xA40) {
                        analogswc_activate_scale(channel, 3);
                        // LOG_DEF_NORMAL("CH 2 %X\r\n", channel->adc_read_values[channel->adc_buff_pos]);
                    }
                    break;
                case 3:
                    if (channel->adc_read_values[channel->adc_buff_pos] < 0x300) {
                        // LOG_DEF_NORMAL("CH 3 %X\r\n", channel->adc_read_values[channel->adc_buff_pos]);
                        analogswc_activate_scale(channel, 2);
                    }
                    break;
            }
        }
    }
}

uint32_t analogswc_get_current_pullup(struct SWC_CHANNEL *channel) {
    uint32_t pullup = -1;
    switch (channel->current_scale) {
        case 1:
            pullup = channel->pullup_0;
            break;
        case 2:
            pullup = channel->pullup_1;
            break;
        case 3:
            pullup = channel->pullup_2;
            break;
    }
    return pullup;
}

double analogswc_get_amplifier(struct SWC_CHANNEL *channel) {
    double amplifier_multiplier;
    double amplifier_divider;

    if (channel->multi_r_down != (uint32_t)-1 && channel->multi_r_up != 0) {
        amplifier_multiplier = 1 + (channel->multi_r_up / channel->multi_r_down);
    } else {
        amplifier_multiplier = 1;
    }

    if (channel->part_low != (uint32_t)-1) {
        amplifier_divider = channel->part_low / (double)(channel->part_high + channel->part_low);
    } else {
        amplifier_divider = 1;
    }
    return amplifier_multiplier / amplifier_divider;
}

uint32_t analogswc_get_external_adc_value(struct SWC_CHANNEL *channel) {
    uint32_t adc_tmp = 0;
    for (uint8_t i = 0; i < ADC_BUFF_LEN; i++) {
        adc_tmp += channel->adc_read_values[i];
    }
    adc_tmp /= ADC_BUFF_LEN;
    return adc_tmp;
}

static bool configured = false;
void analogswc_init(void) {
    if (!configured) {
        LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
        analogswc_init_pin0(&swc_channels[0]);
        analogswc_init_pin1(&swc_channels[1]);
        switch (board_get_version()) {
            case BOARD_VERSION_00:
                timer = xTimerCreate("swc_tim", TIMER_TICK / portTICK_PERIOD_MS, pdTRUE, NULL, timer_swc_res_conversions);
                xTimerStart(timer, TIMER_TICK);
                break;
            case BOARD_VERSION_01:
                timer = xTimerCreate("swc_tim", TIMER_TICK / portTICK_PERIOD_MS, pdTRUE, NULL, timer_swc_vol_conversions);
                xTimerStart(timer, TIMER_TICK);
                break;
            default:
                LOG_DEF_ERROR("%s: board version not handled.\r\n", __PRETTY_FUNCTION__);
                break;
        }
        configured = true;
        analogswc_hw_init();
        analogswc_activate_scale(&swc_channels[0], 3);
        analogswc_activate_scale(&swc_channels[1], 3);
    }
}
