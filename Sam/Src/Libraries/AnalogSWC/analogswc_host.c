#include "analogswc.h"
#include "analogswc_priv.h"
#include "board_version.h"

bool analogswc_amplifiers_active(void){
    return true;
}

void analogswc_start(struct SWC_CHANNEL * channel){
    (void)channel;
}

void analogswc_hw_init(void){

}

uint16_t analogswc1_get_latest_value(void){
    return 0;
}

uint16_t analogswc0_get_latest_value(void){
    return 0;
}

void analogswc_init_pin1(struct SWC_CHANNEL * channel){
    switch(board_get_version()){
        case BOARD_VERSION_00:
        channel->pullup_0 = 680;
        channel->pullup_1 = 680 + 3300;
        channel->pullup_2 = 680 + 3300 + 24000;
        channel->part_high = 3300;
        channel->part_low = (uint32_t)-1;
        channel->current_scale = 3;
        channel->scale_available = true;
        channel->to_skip = ADC_BUFF_LEN+1;
        channel->adc_channel = 11;
        break;
        case BOARD_VERSION_01:
        channel->pullup_0 = (uint32_t)-1;
        channel->pullup_1 = (uint32_t)-1;
        channel->pullup_2 = (uint32_t)-1;
        channel->part_high = 3300;
        channel->part_low = 3300;  
        channel->current_scale = (uint8_t)-1; 
        channel->scale_available = false; 
        channel->to_skip = ADC_BUFF_LEN+1;  
        channel->adc_channel = 11; 
        default:
        break;
    }
}

void scale_output(ioport_pin_t pin, bool level){
    (void)pin;
    (void)level;
}

void analogswc_init_pin0(struct SWC_CHANNEL * channel){
    switch(board_get_version()){
        case BOARD_VERSION_00:
        channel->pullup_0 = 680;
        channel->pullup_1 = 680 + 3300;
        channel->pullup_2 = 680 + 3300 + 24000;
        channel->part_high = 3300;
        channel->part_low = (uint32_t)-1;  
        channel->current_scale = 3; 
        channel->scale_available = true; 
        channel->to_skip = ADC_BUFF_LEN+1;  
        channel->adc_channel = 10; 
        break;
        case BOARD_VERSION_01:
        channel->pullup_0 = (uint32_t)-1;
        channel->pullup_1 = (uint32_t)-1;
        channel->pullup_2 = (uint32_t)-1;
        channel->part_high = 3300;
        channel->part_low = 3300;  
        channel->current_scale = (uint8_t)-1; 
        channel->scale_available = false; 
        channel->to_skip = ADC_BUFF_LEN+1;  
        channel->adc_channel = 10; 
        break;
        default:
        break;

    }
}