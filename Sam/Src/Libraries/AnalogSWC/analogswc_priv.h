#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "timers.h"
#ifndef ioport_pin_t
typedef uint32_t ioport_pin_t;

#endif

#define TIMER_TICK 1
#define ADC_BUFF_LEN 4
#define SWC_CHANNELS 2


struct SWC_CHANNEL{
    uint32_t pullup_0;   //Valore in ohm del primo pullup attivabile 
    uint32_t pullup_1;   //Valore in ohm del secondo pullup attivabile 
    uint32_t pullup_2;   //Valore in ohm del terzo pullup attivabile 
    uint32_t part_high;  //Valore in ohm della parte alta del partitore 
    uint32_t part_low;   //Valore in ohm della parte bassa del partitore 
    
    uint32_t multi_r_up;    
    uint32_t multi_r_down;

    bool scale_available; //If the scales are available or not.
    uint8_t current_scale; //The current scale setup.
    ioport_pin_t an_scale_1; //pin di attivazione della prima scala
    ioport_pin_t an_scale_2; //pin di attivazione della seconda scala

    uint8_t used_scale; //Scala utilizzata per la lettura.
    uint8_t to_skip;    //Numero di lettore da saltare prima di poter trasmettere il dato.
    uint16_t adc_read_values[ADC_BUFF_LEN]; //Valori letti sull'adc
    uint16_t adc_read_mid; //Media vecchie letture del canale ADC
    uint8_t adc_buff_pos;   //Posizione corrente all'interno del buffer.
    uint32_t adc_channel;
};

union ADC_VALUE{
    struct{
        uint16_t adc_value;
        uint8_t adc_channel;
    };
    uint32_t as_uint;
};

void analogswc_init_pin0(struct SWC_CHANNEL * channel);
void analogswc_init_pin1(struct SWC_CHANNEL * channel);
void analogswc_hw_init(void);
uint16_t analogswc0_get_latest_value(void);
uint16_t analogswc1_get_latest_value(void);
void scale_output(ioport_pin_t pin, bool level);
void analogswc_start(struct SWC_CHANNEL * channel);
bool analogswc_amplifiers_active(void);
void analogswc_add_value(struct SWC_CHANNEL *channel, uint16_t value);
bool analogswc_channel_ready(struct SWC_CHANNEL * channel);
void analogswc_activate_scale(struct SWC_CHANNEL * channel, uint8_t num);
void analogswc_check_if_change_scale_needed(struct SWC_CHANNEL * channel);
uint32_t analogswc_get_current_pullup(struct SWC_CHANNEL * channel);
double analogswc_get_amplifier(struct SWC_CHANNEL * channel);
uint32_t analogswc_get_external_adc_value(struct SWC_CHANNEL * channel);
bool analogswc_channel_is_stable(struct SWC_CHANNEL * channel, uint16_t cur_adc);
void analogswc_channel_debounce(struct SWC_CHANNEL * channel, uint16_t cur_adc);
void adc_counter_increase(void);
bool adc_is_enabled(void);

void timer_swc_res_conversions(TimerHandle_t timer);
void timer_swc_vol_conversions(TimerHandle_t timer);