
#include <string.h>

#include "analogswc.h"
#include "analogswc_priv.h"

#include "event_list.h"

void event_analog_value_emit(const struct SWC_ANALOG_CONVERSION * const ev){
    union EVENT event;
    event.as_generic.event = eEVENT_ANALOG_VALUE;
    memcpy(&event.as_analog_value.swc_analog, ev, sizeof(struct SWC_ANALOG_CONVERSION));
    event_emit(&event);
}