#include "analogswc.h"
#include "analogswc_priv.h"
#include "board_version.h"
#include "event_list.h"
#include "logger.h"

/**
 * @brief struttura comandi a volante analogici. definita in analogswc.c
 * 
 */
extern struct SWC_CHANNEL swc_channels[SWC_CHANNELS];

void timer_swc_vol_conversions(TimerHandle_t timer)
{
    (void)timer;
    static bool two_channels_flip = false;
    if (!analogswc_amplifiers_active()) {
        return;
    }
    adc_counter_increase();

    if (two_channels_flip) {
        analogswc_add_value(&swc_channels[0], analogswc0_get_latest_value());
    } else {
        analogswc_add_value(&swc_channels[1], analogswc1_get_latest_value());
    }

    if (analogswc_channel_ready(&swc_channels[0]) && analogswc_channel_ready(&swc_channels[1])) {
        uint8_t bv = board_get_version();
        switch (bv) {
            case BOARD_VERSION_01:
            {
                uint32_t adc_val = analogswc_get_external_adc_value(&swc_channels[0]);

                struct SWC_ANALOG_CONVERSION event;
                //ADC_LETTO : 0xFFF = mvolt : 3300
                event.channel_a = ((adc_val * 3300 / 0xFFF) * analogswc_get_amplifier(&swc_channels[0]));
                event.type_channel_a = SWC_ANALOG_VOLTAGE;

                adc_val = analogswc_get_external_adc_value(&swc_channels[1]);
                event.channel_b = ((adc_val * 3300 / 0xFFF) * analogswc_get_amplifier(&swc_channels[1]));
                event.type_channel_b = SWC_ANALOG_VOLTAGE;
                if (adc_is_enabled()){
                    event_analog_value_emit(&event);
                }
                swc_channels[0].to_skip = ADC_BUFF_LEN+1;  
                swc_channels[1].to_skip = ADC_BUFF_LEN+1;  
            }
            break;
            default:
                LOG_DEF_ERROR("%s: board version %d not handled.\r\n", __PRETTY_FUNCTION__, bv);
                break;
        }
    }

    //Faccio ripartire l'ADC.
    if (two_channels_flip){
        analogswc_start(&swc_channels[1]);
    }else{
        analogswc_start(&swc_channels[0]);
    }
    two_channels_flip = !two_channels_flip;
}