#include "FreeRTOS.h"
#include "timers.h"

#include "analogswc.h"
#include "analogswc_priv.h"
#include "asf.h"
#include "board_version.h"
#include "logger.h"
#include "controller.h"
#include "wisker.h"

bool analogswc_amplifiers_active(void){
    return services_enabled();
}

void analogswc_init_pin0(struct SWC_CHANNEL * channel){
    switch(board_get_version()){
        case BOARD_VERSION_00:
        channel->pullup_0 = 680;
        channel->pullup_1 = 680 + 3300;
        channel->pullup_2 = 680 + 3300 + 24000;
        channel->part_high = 3300;
        channel->part_low = (uint32_t)-1;
        channel->an_scale_1 = An_Scale_1_1;
        channel->an_scale_2 = An_Scale_1_2;   
        channel->current_scale = 3; 
        channel->scale_available = true;    
        ioport_set_pin_level(channel->an_scale_1, true);
        ioport_set_pin_level(channel->an_scale_2, true);
        channel->to_skip = ADC_BUFF_LEN+1;
        channel->adc_channel = ADC_CHANNEL_10;
        break;
        case BOARD_VERSION_01:
        channel->pullup_0 = (uint32_t)-1;
        channel->pullup_1 = (uint32_t)-1;
        channel->pullup_2 = (uint32_t)-1;
        channel->part_high = 3300;
        channel->part_low = 3300;  
        channel->current_scale = (uint8_t)-1; 
        channel->scale_available = false; 
        channel->to_skip = ADC_BUFF_LEN+1;  
        channel->adc_channel = ADC_CHANNEL_10; 
        default:
        LOG_DEF_ERROR("%s: board version not handled.\r\n", __PRETTY_FUNCTION__);
        break;
    }
}

void scale_output(ioport_pin_t pin, bool level){
    ioport_set_pin_level(pin, level);
}

void analogswc_init_pin1(struct SWC_CHANNEL * channel){
    switch(board_get_version()){
        case BOARD_VERSION_00:
        channel->pullup_0 = 680;
        channel->pullup_1 = 680 + 3300;
        channel->pullup_2 = 680 + 3300 + 24000;
        channel->part_high = 3300;
        channel->part_low = (uint32_t)-1;
        channel->an_scale_1 = An_Scale_2_1;
        channel->an_scale_2 = An_Scale_2_2;     
        channel->current_scale = 3;
        channel->scale_available = true;
        ioport_set_pin_level(channel->an_scale_1, true);
        ioport_set_pin_level(channel->an_scale_2, true);
        channel->to_skip = ADC_BUFF_LEN+1;
        channel->adc_channel = ADC_CHANNEL_0;
        break;
        case BOARD_VERSION_01:
        channel->pullup_0 = (uint32_t)-1;
        channel->pullup_1 = (uint32_t)-1;
        channel->pullup_2 = (uint32_t)-1;
        channel->part_high = 3300;
        channel->part_low = 3300;  
        channel->current_scale = (uint8_t)-1; 
        channel->scale_available = false; 
        channel->to_skip = ADC_BUFF_LEN+1;  
        channel->adc_channel = ADC_CHANNEL_0; 
        default:
        LOG_DEF_ERROR("%s: board version not handled.\r\n", __PRETTY_FUNCTION__);
        break;

    }
}

uint16_t analogswc0_get_latest_value(void){
    return adc_get_channel_value(ADC, ADC_CHANNEL_10);
}

uint16_t analogswc1_get_latest_value(void){
    return adc_get_channel_value(ADC, ADC_CHANNEL_0);
}

void analogswc_start(struct SWC_CHANNEL * channel){
    adc_enable_channel(ADC, channel->adc_channel);
    adc_start(ADC);
}


void analogswc_hw_init(void){
    sysclk_enable_peripheral_clock(ID_ADC);
    adc_init(ADC, sysclk_get_cpu_hz(), 128000, ADC_STARTUP_TIME_8);
    adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1);
    
    adc_enable_tag(ADC);
	adc_set_resolution(ADC, ADC_MR_LOWRES_BITS_12);
	adc_enable_channel(ADC, ADC_CHANNEL_10);
    
}

