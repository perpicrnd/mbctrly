#include "analogswc.h"
#include "analogswc_priv.h"
#include "board_version.h"
#include "event_list.h"
#include "logger.h"


/**
 * @brief struttura comandi a volante analogici. definita in analogswc.c
 * 
 */
extern struct SWC_CHANNEL swc_channels[SWC_CHANNELS];


void timer_swc_res_conversions(TimerHandle_t timer){
    (void)timer;
    static bool two_channels_flip = false;
    if (!analogswc_amplifiers_active()){
        return;
    }
    adc_counter_increase();
    
    //Leggo il valore dall'ADC
    if (two_channels_flip){
        analogswc_add_value(&swc_channels[0], analogswc0_get_latest_value());
        analogswc_check_if_change_scale_needed(&swc_channels[0]);
    }else{
        analogswc_add_value(&swc_channels[1], analogswc1_get_latest_value());
        analogswc_check_if_change_scale_needed(&swc_channels[1]);
    }
    
    if (analogswc_channel_ready(&swc_channels[0])&&
        analogswc_channel_ready(&swc_channels[1])){  
        do{
            uint32_t pullup = analogswc_get_current_pullup(&swc_channels[0]);
            uint32_t adc_val_a = analogswc_get_external_adc_value(&swc_channels[0]);
            uint32_t adc_val_b = analogswc_get_external_adc_value(&swc_channels[1]);
            analogswc_channel_debounce(&swc_channels[0], adc_val_a);
            analogswc_channel_debounce(&swc_channels[1], adc_val_b);
            bool stable = analogswc_channel_is_stable(&swc_channels[0], adc_val_a);
            stable &= analogswc_channel_is_stable(&swc_channels[1], adc_val_b);
            if (!stable){
                break;
            }
            
            //3300 : 0xFFF = milli : adc
            struct SWC_ANALOG_CONVERSION event;
            event.channel_a = (adc_val_a * pullup) / ((0x1000 * analogswc_get_amplifier(&swc_channels[0])) - adc_val_a);
            event.type_channel_a = SWC_ANALOG_OHM;

            pullup = analogswc_get_current_pullup(&swc_channels[1]);
            
            
            event.channel_b = adc_val_b * pullup / ((0x1000 * analogswc_get_amplifier(&swc_channels[1])) - adc_val_b);
            event.type_channel_b = SWC_ANALOG_OHM;
            if (adc_is_enabled()){
                event_analog_value_emit(&event);
            }
        }while(0);
        swc_channels[0].to_skip = ADC_BUFF_LEN+1;  
        swc_channels[1].to_skip = ADC_BUFF_LEN+1;  
    }
    //Faccio ripartire l'ADC.
    if (two_channels_flip){
        analogswc_start(&swc_channels[1]);
    }else{
        analogswc_start(&swc_channels[0]);
    }
    two_channels_flip = !two_channels_flip;
}