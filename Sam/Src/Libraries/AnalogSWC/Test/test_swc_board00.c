#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "event_list.h"
#include "wisker.h"

#include <cmocka.h>
#include "analogswc.c"

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_parking_sensor_emit(struct EVENT_PARKING_SENSOR *data) {
    (void)data;
}

void wisker_reset(MODULES module) {
    (void)module;
}

enum BOARD_VERSION __wrap_board_get_version(void) {
    return mock_type(enum BOARD_VERSION);
}

TimerHandle_t __real_xTimerCreate(const char *const pcTimerName,
                                  const TickType_t xTimerPeriodInTicks,
                                  const UBaseType_t uxAutoReload,
                                  void *const pvTimerID,
                                  TimerCallbackFunction_t pxCallbackFunction);
TimerHandle_t __wrap_xTimerCreate(const char *const pcTimerName,
                                  const TickType_t xTimerPeriodInTicks,
                                  const UBaseType_t uxAutoReload,
                                  void *const pvTimerID,
                                  TimerCallbackFunction_t pxCallbackFunction) {
    function_called();
    check_expected(pxCallbackFunction);
    return __real_xTimerCreate(pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction);
}

uint16_t __wrap_analogswc0_get_latest_value(void) {
    return mock_type(uint16_t);
}

uint16_t __wrap_analogswc1_get_latest_value(void) {
    return mock_type(uint16_t);
}

void __wrap_event_analog_value_emit(const struct SWC_ANALOG_CONVERSION *const ev) {
    check_expected(ev->channel_a);
    check_expected(ev->channel_b);
    function_called();
}

uint8_t adc_channel = 0;
void __wrap_analogswc_start(struct SWC_CHANNEL *channel) {
    adc_channel = channel->adc_channel;
}

int setup(void **state) {
    (void)state;
    return 0;
}

void test_timer_swc_res_conversions() {
    adc_enabled = true;
    expect_function_call(__wrap_event_analog_value_emit);
    expect_in_range(__wrap_event_analog_value_emit, ev->channel_a, 114578000, 114578200);
    expect_in_range(__wrap_event_analog_value_emit, ev->channel_b, 114578000, 114578200);
    //Riempio il buffer...
    adc_channel = 11;
    for (uint16_t i = 0; i < 0x38; i++) {
        if (adc_channel == 10) {
            will_return(__wrap_analogswc0_get_latest_value, 0xFFF);
        } else {
            will_return(__wrap_analogswc1_get_latest_value, 0xFFF);
        }
        timer_swc_res_conversions(NULL);
    }

    // timer_swc_res_conversions(NULL);
}

void test_analogswc_init() {
    assert_false(configured);
    will_return_always(__wrap_board_get_version, BOARD_VERSION_00);
    expect_function_call(__wrap_xTimerCreate);
    expect_value(__wrap_xTimerCreate, pxCallbackFunction, timer_swc_res_conversions);
    analogswc_init();
    assert_true(configured);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_analogswc_init, setup),
        cmocka_unit_test_setup(test_timer_swc_res_conversions, setup),

    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}