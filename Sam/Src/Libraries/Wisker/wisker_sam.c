#include "wisker_priv.h"
#include "asf.h"

void wisker_enable_services(bool enable){
	ioport_set_pin_dir(En_Services, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(En_Services, enable);
}

bool services_enabled(void){
	return ioport_get_pin_level(En_Services);
}