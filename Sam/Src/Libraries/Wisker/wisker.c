#include <FreeRTOS.h>
#include <timers.h>
#include "event_list.h"
#include <stdio.h>
#include "logger.h"
#include "wisker.h"
#include "controller.h"
#include "wisker_priv.h"
#include "watchdog.h"
#include "main.h"

/**
 * @addtogroup WISKER
 * @{
 * Il wisker è un watchdog software che utilizza il watchgod hardware del microprocessore.
 * Il software è stato scritto in modo modulare, ogni singolo modulo ha la responsabilità di resettare il proprio wisker.
 * Se qualcuno fallisce, la scheda si resetta.
 */

/// @brief Tempo in cui il wisker mantiene accesa la scheda
#define STAYING_ALIVE (90000L)
/// @brief Tempo di esecuzione del timer
#define TIMER_SPEED (250)
/// Valore a cui viene impostato il counnter del wisker
#define MAX_COUNTER ((STAYING_ALIVE) / (TIMER_SPEED) )

#define WISKER_DISABLED 0xFFFF

/**
 * @brief Ogni modulo ha un counter, vengono mantenuti all'interno di questo array
 */
static uint32_t can_toggle[MODULE_COUNT];

/**
 * Ogni singolo modulo deve resettare il wisker come se si trattasse di un watchdog.
 * Il wisker resetta il watchdog se tutti i moduli resettano il wisker.
 */
void wisker_reset(MODULES module){
	///Imposto can_toggle[module] al valore MAX_COUNTER.
	can_toggle[module] = MAX_COUNTER;
}

void wisker_disable_module(MODULES module){
	can_toggle[module] = WISKER_DISABLED;
}

/// @brief Flag in cui salvo se il wisker è attivo o no.
bool wisker_status;


void wisker_enable(bool value){
	wisker_status = value;
}

/// @brief counter interrupt/task di verifica dello speed
volatile int8_t speed_counter;


static uint8_t wisker_min_counter(void){
	uint8_t retVal = 0xFF;
	for(uint8_t i=0; i<MODULE_COUNT; i++){
		uint8_t tmp = (can_toggle[i]);
		if (tmp < retVal){
			retVal = tmp;
		}
	}
	return retVal;
}
/**
 * @brief Timer del wisker
 * Questo è il timer del wisker, colui che decrementa i counter e fa il reset del watchdog.
 */
void vWisker_clock(TimerHandle_t pxTimer);
void vWisker_clock(TimerHandle_t pxTimer){
	(void)pxTimer;
	static uint8_t flipflop = 0;
	flipflop++;
	if (!wisker_status){
		///Se il wisker non è attivo resetto il watchdog.
        struct CONTROLLER_DATA data;
        data.command = CONTROLLER_WO_ALIVE_SECONDS;
        data.data0 = wisker_min_counter();
		event_controller_emit(&data);
        wisker_enable_services(true);
	}else{
		///Se il wisker è attivo verifico che i singoli moduli vengano resettati.
		uint32_t wisval = 0;
		for(uint8_t i=0; i<MODULE_COUNT; i++){
			///Il valore 0xFFFF viene utilizzato come valore di disattivazione del modulo.
			if (can_toggle[i] != 0xFFFF){
				if (can_toggle[i] > 0){
					can_toggle[i]--;
					wisker_enable_services(true);
				}else{
					///Se uno dei task fallisce nel resettare il wisker viene resettato il microprocessore.
					/// utilizzando il pin di killer che abbiamo impostato sul microprocessore in modo da resettare
					/// tutta la scheda.
					LOG_DEF_NORMAL("Wisker module %d timedout\r\n", i);
					wisval |= 1<<i;
					wisker_enable_services(false);
				}
			}
		}
		if (wisval == 0){
			///Qualcuno ha fatto il reset, quindi resetto il watchdog.
			struct CONTROLLER_DATA data;
            data.command = CONTROLLER_WO_ALIVE_SECONDS;
            data.data0 = wisker_min_counter();
            watchdog_reset();
            event_controller_emit(&data);
		}else{
			LOG_DEF_NORMAL("Not giving wisker\r\n");
			struct CONTROLLER_DATA data;
            data.command = CONTROLLER_WO_ALIVE_SECONDS;
            data.data0 = 0;
            event_controller_emit(&data);
			vTaskDelay(1000/portTICK_PERIOD_MS);
            software_reset();
		}
	}
}

static bool configured = false;
void wisker_init(void){
	LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
	if (!configured){
		configured = true;
		TimerHandle_t timer_wisker = xTimerCreate("t_wsk" , TIMER_SPEED/portTICK_PERIOD_MS, 	pdTRUE, 0, vWisker_clock);
		xTimerStart(timer_wisker, 0);
		for (uint8_t i=0; i<MODULE_COUNT; i++){
			///Inizializzo tutti i moduli impostati al valore di disable che è 0xFFFF.
			can_toggle[i] = WISKER_DISABLED;
		}
	}
}

/**
 * @}
 */
