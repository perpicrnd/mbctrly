#ifndef WISKER_H
#define WISKER_H
#include <stdint.h>
#include <stdbool.h>

/**
 * @defgroup WISKER
 * @{
 * @brief Il modulo wisker è un watchdog software.
 */

/**
 * @brief Definisce i moduli che utilizzano il wisker.
 */
typedef enum{
	MODULE_VEHICLE,
	MODULE_UART_LOWLEVEL,
	MODULE_COUNT      //!< MODULE_COUNT
}MODULES;

/**
 * @brief Resetta il modulo wisker passato come parametro.
 * @param module il modulo che richiede il reset.
 */
void wisker_reset(MODULES module);
/**
 * @brief Abilita o disabilita il modulo wisker.
 * @param value true se si vuole abilitare, false per disabilitare.
 */
void wisker_enable(bool value);

void wisker_disable_module(MODULES module);
/**
 * @brief Inizializzazione del wisker.
 */
void wisker_init(void);

bool services_enabled(void);

/**
 * @}
 */

#endif
