
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include <timers.h>
#include "twi_task.h"
#include "twi_task_priv.h"
#include "event_list.h"
#include "logger.h"

QueueHandle_t task_queue;

static bool enable_hard_reset = false;

static void twi_task(void* parameter)
{
    (void)parameter;
    vTaskDelay(10 / portTICK_PERIOD_MS);
    struct TWI_COMMAND command;
    for (;;) {
        // vTaskDelay(1/portTICK_PERIOD_MS);
        if (twi_in_fault()){
            if (enable_hard_reset){
                perform_hard_reset();
            }
        }
        if (xQueueReceive(task_queue, &command, 1000 / portTICK_PERIOD_MS)) {
            switch (command.type) {
            case TWI_COMMAND_WRITE:
                write_register(&command);
                break;
            case TWI_COMMAND_READ:
                if (command.rdata != NULL) {
                    uint8_t buffer[TWI_BUFFER_LEN] = {0};
                    read_register(&command, buffer);
                    read_register(&command, buffer);
                    command.rdata(buffer, command.len);
                }
                break;
            case TWI_COMMAND_PROBE:
                if (command.rdata != NULL) {
                    uint8_t buffer[TWI_BUFFER_LEN] = {0};
                    buffer[0] = probe_ic(command.chip);
                    command.rdata(buffer, 1);
                }
                break;
            }
        }
#ifdef __amd64__

#else
        if (uxTaskGetStackHighWaterMark(NULL)<100){
            LOG_DEF_NORMAL("%s stack too low.\r\n", __PRETTY_FUNCTION__);
        }
#endif
    }
}

inline void event_alarm_emit(void){
    union EVENT event;
    event.as_generic.event = eEVENT_ALARM;
    event_emit(&event);
}


inline void event_ext_alarm_emit(void){
    union EVENT event;
    event.as_generic.event = eEVENT_EXT_ALARM;
    event_emit(&event);
}

void handle_second_changed(const union EVENT * const event){
    if (event->as_generic.event == eEVENT_SECONDS_CHANGED){
        if (twi_alarm_pin_active()){
            LOG_DEF_NORMAL("Alarm pin is active.\r\n");
        }
        event_alarm_emit();
    }
}

void event_alarm_emit_fromISR(void){
    union EVENT event;
    event.as_generic.event = eEVENT_ALARM;
    BaseType_t woke;
    event_emit_fromISR(&event, &woke);
}


void event_ext_alarm_emit_fromISR(void){
    union EVENT event;
    event.as_generic.event = eEVENT_EXT_ALARM;
    BaseType_t woke;
    event_emit_fromISR(&event, &woke);
}

void twi_command_apply(struct TWI_COMMAND *command){
    xQueueSendToBack(task_queue, command, 10);
}

void twi_command_apply_from_ISR(struct TWI_COMMAND *command){
    BaseType_t woke;
    xQueueSendToBackFromISR(task_queue, command, &woke);
}



void twi_priority_command_apply(struct TWI_COMMAND *command){
    xQueueSendToFront(task_queue, command, 10);
}

void twi_enable_hard_reset(void){
    enable_hard_reset = true;
}

static bool configured = false;
void twi_task_init(void)
{
    LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
    if (!configured) {
        xTaskCreate(twi_task, (char*)"twi_task", configMINIMAL_STACK_SIZE + 200, NULL, configMAX_PRIORITIES - 1, NULL);
        task_queue = xQueueCreate(10, sizeof(struct TWI_COMMAND));
        event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
        twi_hw_init();
        configured = true;
    }
}
