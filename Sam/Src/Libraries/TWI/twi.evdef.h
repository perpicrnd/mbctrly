
#pragma once

/**
 * @brief Genera un evento di notifica da parte di un integrato collegato al twi.
 */
struct EVENT_ALARM{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
};

/**
 * @brief Genera un evento di notifica da parte di un integrato collegato al twi.
 */
struct EVENT_EXT_ALARM{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
};

/**
 * @brief emette un messaggio contenente la direzione attuale
 */
void event_alarm_emit(void);

/**
 * @brief emette un messaggio contenente la direzione attuale. Da usare all'interno degli interrupt.
 * 
 * 
 */
void event_alarm_emit_fromISR(void);


/**
 * @brief emette un messaggio contenente la direzione attuale
 */
void event_ext_alarm_emit(void);

/**
 * @brief emette un messaggio contenente la direzione attuale. Da usare all'interno degli interrupt.
 * 
 * 
 */
void event_ext_alarm_emit_fromISR(void);
