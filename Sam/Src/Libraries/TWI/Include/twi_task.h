#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "events.h"

typedef void (*TWI_CALLBACK)(uint8_t *rdata, uint8_t len);

enum TWI_COMMAND_TYPE{
    TWI_COMMAND_WRITE = 0,
    TWI_COMMAND_READ,
    TWI_COMMAND_PROBE,
};

#define TWI_BUFFER_LEN 10

struct TWI_COMMAND{
    ///true => Write, false=> Read
    enum TWI_COMMAND_TYPE type;

    /// External chip address.
    uint8_t chip;

    /// External register (used only on read/write)
    uint8_t reg[3];
    uint8_t reg_len;

    union{
        ///Data to be written (used by write)
        uint8_t wdata[TWI_BUFFER_LEN];

        ///Callback to be called on data available. (used by read / probe)
        TWI_CALLBACK rdata;
    };
    uint8_t len;
};


void twi_command_apply(struct TWI_COMMAND *command);
void twi_command_apply_from_ISR(struct TWI_COMMAND *command);
void twi_task_init(void);
void twi_enable_hard_reset(void);