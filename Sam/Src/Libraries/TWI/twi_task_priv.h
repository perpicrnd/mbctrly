#pragma once

#include <stdint.h>

uint8_t write_register(struct TWI_COMMAND *command);
uint8_t read_register(struct TWI_COMMAND *command, uint8_t *data);
uint8_t probe_ic(uint8_t chip);
bool twi_alarm_pin_active(void);

void twi_hw_init(void);
bool twi_in_fault(void);
void perform_hard_reset(void);