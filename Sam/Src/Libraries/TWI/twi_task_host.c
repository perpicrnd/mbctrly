#include "twi_task.h"
#include "twi_task_priv.h"

uint8_t write_register(struct TWI_COMMAND *command){
    (void)command;
    return 0;
}

uint8_t read_register(struct TWI_COMMAND *command, uint8_t *data){
    (void)command;
    (void)data;
    return 0;
}

uint8_t probe_ic(uint8_t chip){
    (void)chip;
    return 0;
}


bool twi_alarm_pin_active(void){
    return false;   
}

void  twi_hw_init(void){

}

bool twi_in_fault(void){
    return false;
}
void perform_hard_reset(void){
    
}