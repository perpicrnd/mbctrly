# Impostazioni del twi

Il TWI è stato implementato con una logica di lettura/scrittura byte a byte.

L'indirizzamento del twi sul software di nostra produzione ragiona con un sistema di indirizzamento primario ed uno secondario. 
Questo nasce dal tipo di memorie presenti all'interno dei processori.

L'indirizzamento di tipo primario consente di specificare il tipo di memoria in cui andare a leggere,
questo permette al twi di poter leggere nei primi 255 byte di ogni tipo di memoria in modo diretto.

durante la scrittura di un byte bisogna quindi indicare la posizione di scrittura con scritture di differenti byte, esempio:
  - indirizzo target
  - tipo di memoria target
  - numero di registro
  - valore da scrivere

ne consegue che la lettura dal twi richiede:
  
  - scrittura
    - indirizzo target
    - tipo di memoria target
    - numero di registro

  - lettura
    - valore da leggere

## Timing

E' stato verificato da datasheet che la safe operating area inferiore dell'attiny permette un funzionamento a 1.8V di 4MHz
partendo da questo fatto è stato definito che la velocità massima raggiungibile dal twi è pari a:

4.000.000 / TWI_PRESCALER 

TWI_PRESCALER da datasheet attiny è 16.

quindi velocità massima: 250.000

All'interno del nostro software per rimanere all'interno del dato massimo utilizzabile la velocità del twi è stata impostata a 200.000

