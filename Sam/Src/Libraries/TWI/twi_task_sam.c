#include <string.h>
#include "twi_task.h"
#include "twi_task_priv.h"
#include "asf.h"
#include "logger.h"
twi_packet_t ext;

static SemaphoreHandle_t twi_sem = NULL;
static bool fault_condition = true;

bool twi_in_fault(void){
	return fault_condition;
}

static void pin_high(ioport_pin_t pin){
	ioport_set_pin_dir(pin, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(pin, IOPORT_MODE_PULLUP);
}

static void pin_low(ioport_pin_t pin){
	ioport_set_pin_dir(pin, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(pin, false);
}

static void my_delay(uint8_t delay){
	for(uint8_t i=0; i<delay; i++){
		for(uint8_t j=0; j<0xF0; j++){
			asm("nop");
		}
	}
}

void perform_hard_reset(void){
	disable_twi_pins();
	pin_low(Twi_Sda);
	my_delay(1);
	pin_low(Twi_Scl);
	my_delay(1);
	pin_high(Twi_Scl);
	my_delay(1);
	pin_high(Twi_Sda);
	for(uint8_t i=0; i<63; i++){
		my_delay(1);
		pin_low(Twi_Scl);
		my_delay(1);
		pin_high(Twi_Scl);
	}
	my_delay(2);
	pin_low(Twi_Scl);
	my_delay(5);
	pin_low(Twi_Sda);
	my_delay(5);
	pin_high(Twi_Scl);
	my_delay(5);
	pin_high(Twi_Sda);
	my_delay(1);
	pin_low(Twi_Scl);
	my_delay(1);
	enable_twi_pins();
	fault_condition = false;
	vTaskDelay(100/portTICK_PERIOD_MS);
}

void TC1_Handler(void){
    if ((TC0->TC_CHANNEL[1].TC_SR & TC_SR_CPAS) == TC_SR_CPAS) {
        BaseType_t woke;
        xSemaphoreGiveFromISR(twi_sem, &woke);
		portYIELD_FROM_ISR( woke );
        tc_stop(TC0, 1);
    }
}

static void twi_hw_init_registers(void){
   uint8_t chip_id = 0x40;
	LOG_DEF_NORMAL("init twi\r\n");
	twi_options_t m_options = {
		.speed     = 200000,
		.chip      = chip_id,
		.master_clk = sysclk_get_cpu_hz()
	};

	pmc_enable_periph_clk(ID_TWI0);
	twi_master_init(TWI0, &m_options);
}

// uint8_t write_register(uint8_t chip, uint8_t reg, uint8_t *data, uint8_t len){
uint8_t write_register(struct TWI_COMMAND *command){
    if (!xSemaphoreTake(twi_sem, 2)){
        twi_hw_init();
    }
	ext.addr[0] = command->reg[0];
	ext.addr[1] = command->reg[1];
	ext.addr[2] = command->reg[2];
	ext.addr_length = command->reg_len;
	ext.chip = command->chip;
	ext.length = command->len;
	uint8_t txrx_buffer[TWI_BUFFER_LEN] = {0};
	ext.buffer = txrx_buffer;
	memcpy(ext.buffer, command->wdata, command->len);
	// vTaskDelay(4/portTICK_PERIOD_MS);
	uint32_t result;
	uint8_t counter = 0;
	do{
		result = twi_master_write(TWI0, &ext);
		tc_start(TC0, 1);
		if (STATUS_OK != result){
			vTaskDelay(1/portTICK_PERIOD_MS);
			twi_hw_init();
			counter++;
			if (counter > 8){
				fault_condition = true;
				LOG_DEF_NORMAL("TWI FAULT CONDITION DETECTED\r\n");
				return 0xFF;
			}
		}
	}while(STATUS_OK != result);
	return TWI_SUCCESS;
}

// uint8_t read_register(uint8_t chip, uint8_t reg, uint8_t *data, uint8_t len){
uint8_t read_register(struct TWI_COMMAND * command, uint8_t *data){
    if (!xSemaphoreTake(twi_sem, 2)){
        twi_hw_init();
    }
	ext.addr[0] = command->reg[0];
	ext.addr[1] = command->reg[1];
	ext.addr[2] = command->reg[2];
	ext.addr_length = command->reg_len;
	ext.length = command->len;
	ext.chip = command->chip;
	ext.buffer = data;
	
	uint32_t result;
	uint8_t counter = 0;
	uint8_t *buff = (uint8_t *)ext.buffer;
	do{
		result = twi_master_read(TWI0, &ext);
        tc_start(TC0, 1);
		if (STATUS_OK != result){
			vTaskDelay(1/portTICK_PERIOD_MS);
			twi_hw_init();
			counter++;
			if (counter > 8){
				fault_condition = true;
				LOG_DEF_NORMAL("TWI FAULT CONDITION DETECTED\r\n");
				return 0xFF;
			}
		}
	}while(STATUS_OK != result);
	return buff[0];
}

uint8_t probe_ic(uint8_t chip){
    return twi_probe(TWI0, chip);
}


static void tc_capture_initialize(void)
{
    /* Configure the PMC to enable the TC module */
    sysclk_enable_peripheral_clock(ID_TC1);
    /* disabilita write protect */
    tc_set_writeprotect(TC0, 0);
    /* Init TC to capture mode. */
    tc_init(TC0, 1,
            TC_CMR_TCCLKS_TIMER_CLOCK1 	/* Clock Selection 						*/
            | TC_CMR_CPCSTOP
            | TC_CMR_WAVE		
            | TC_CMR_LDRB_NONE 		
    );
    tc_start(TC0, 1);
}

void twi_hw_init(void){
 	twi_hw_init_registers();
    
    if (twi_sem == NULL){
        twi_sem = xSemaphoreCreateBinary();
    }
    
    tc_capture_initialize();
    tc_enable_interrupt(TC0, 1, TC_IER_CPAS);
    tc_write_ra(TC0, 1, 42 * 100);
    NVIC_DisableIRQ(TC1_IRQn);
    NVIC_ClearPendingIRQ(TC1_IRQn);
    NVIC_SetPriority(TC1_IRQn, 15);
    NVIC_EnableIRQ(TC1_IRQn);

}