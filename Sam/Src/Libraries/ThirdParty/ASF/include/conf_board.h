/**
 * \file
 *
 * \brief User board configuration template
 *
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

/*! LED #0 pin definition (BLUE). */
#define LED_0_NAME      "blue LED D2"
#define LED0_GPIO       (PIO_PA12_IDX)
#define LED0_FLAGS      (PIO_TYPE_PIO_OUTPUT_1 | PIO_DEFAULT)
#define LED0_ACTIVE_LEVEL 0

#define PIN_LED_0       {PIO_PA12, PIOA, ID_PIOA, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_LED_0_MASK  PIO_PA12
#define PIN_LED_0_PIO   PIOA
#define PIN_LED_0_ID    ID_PIOA
#define PIN_LED_0_TYPE  PIO_OUTPUT_1
#define PIN_LED_0_ATTR  PIO_DEFAULT

#define LED0_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! LED #1 pin definition (GREEN). */
#define LED_1_NAME      "green LED D4"
#define LED1_GPIO       (PIO_PB13_IDX)
#define LED1_FLAGS      (PIO_TYPE_PIO_OUTPUT_1 | PIO_DEFAULT)
#define LED1_ACTIVE_LEVEL 0

#define PIN_LED_1       {PIO_PB13, PIOB, ID_PIOB, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_LED_1_MASK  PIO_PB13
#define PIN_LED_1_PIO   PIOB
#define PIN_LED_1_ID    ID_PIOB
#define PIN_LED_1_TYPE  PIO_OUTPUT_1
#define PIN_LED_1_ATTR  PIO_DEFAULT

#define LED1_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! LED #2 pin detection (AMBER). */
#define LED2_GPIO       (PIO_PB12_IDX)
#define LED2_FLAGS      (PIO_TYPE_PIO_OUTPUT_1 | PIO_DEFAULT)
#define LED2_ACTIVE_LEVEL 0

#define PIN_LED_2_MASK   PIO_PB12
#define PIN_LED_2_PIO    PIOB
#define PIN_LED_2_ID     ID_PIOB
#define PIN_LED_2_TYPE   PIO_OUTPUT_1
#define PIN_LED_2_ATTR   PIO_DEFAULT

#define LED2_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/*! LED #3 pin detection (power) */
#define LED3_GPIO       (PIO_PA13_IDX)
#define LED3_FLAGS      (PIO_TYPE_PIO_OUTPUT_0 | PIO_DEFAULT)
#define LED3_ACTIVE_LEVEL 1

#define PIN_LED_3_MASK   PIO_PA13
#define PIN_LED_3_PIO    PIOA
#define PIN_LED_3_ID     ID_PIOA
#define PIN_LED_3_TYPE   PIO_OUTPUT_1
#define PIN_LED_3_ATTR   PIO_DEFAULT

#define LED3_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

#define BOARD_NUM_OF_LED 4


#endif // CONF_BOARD_H
