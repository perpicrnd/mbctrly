/**
 * \file
 *
 * \brief User board initialization template
 *
 */

#pragma once

#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "gpio.h"
#include "ioport.h"

#define En_Services         PIO_PA29_IDX

#define Can_0_RS_Ctrl_Out	PIO_PB24_IDX	//Abilitazione output transceiver CAN_0
#define Can_1_RS_Ctrl_Out	PIO_PB22_IDX	//Abilitazione output transceiver CAN_1

#define Can_0_Tx			PIO_PA0_IDX		//CAN_0 TX
#define Can_0_Rx			PIO_PA1_IDX		//CAN_0 RX
#define Can_0_ISR           PIO_ISR_P1
#define Can_1_Rx			PIO_PB15_IDX	//CAN_1 RX
#define Can_1_Tx			PIO_PB14_IDX	//CAN_1 TX
#define Can_1_ISR           PIO_ISR_P15


#define Urxd_1				PIO_PA8_IDX		//RX UART
#define Utxd_1				PIO_PA9_IDX		//TX UART
#define Urxd_2				PIO_PA10_IDX	//RX USART0
#define Utxd_2				PIO_PA11_IDX	//TX USART0
#define Rxd_1				PIO_PA12_IDX	//RX USART1
#define Txd_1				PIO_PA13_IDX	//TX USART1
#define Rxd_2				PIO_PB21_IDX	//RX USART2
#define Txd_2				PIO_PB20_IDX	//TX USART2
#define Rxd1_ISR            PIO_ISR_P12

#define Twi_Sda				PIO_PA17_IDX	//TWI Serial Data
#define Twi_Scl				PIO_PA18_IDX	//TWI Clock

#define An_Scale_1_1	    PIO_PB1_IDX
#define An_Scale_1_2	    PIO_PB0_IDX
#define An_Scale_2_1	    PIO_PB3_IDX
#define An_Scale_2_2	    PIO_PB2_IDX


#define Lig_Out	            PIO_PB6_IDX
#define Rev_Out	            PIO_PB5_IDX
#define Spd_Out	            PIO_PB9_IDX
#define Brk_Out	            PIO_PB25_IDX
#define Ign_Out	            PIO_PB12_IDX
#define Spd_In	            PIO_PA3_IDX


#define An_Vcc_1	        PIO_PB17_IDX
#define An_Vcc_2	        PIO_PA2_IDX

#define Ign_In_1            PIO_PA16_IDX
#define Ign_In_2            PIO_PB16_IDX
#define Ign_In_1_ISR        PIO_ISR_P16
#define Ign_In_2_ISR        PIO_ISR_P16

#define Spare_1_Out	        PIO_PA20_IDX
#define Spare_2_Out	        PIO_PA19_IDX
#define Spare_3_Out	        PIO_PA14_IDX
	
#define Cmd_01				PIO_PA4_IDX			
#define Cmd_00				PIO_PA6_IDX			
#define Cmd_03				PIO_PA22_IDX			
#define Cmd_02				PIO_PA23_IDX			
#define Cmd_04				PIO_PA24_IDX			
#define Cmd_06				PIO_PB7_IDX			
#define Cmd_09				PIO_PB8_IDX			
#define Cmd_10				PIO_PB10_IDX			
#define Cmd_12				PIO_PB11_IDX		
#define Cmd_07				PIO_PB13_IDX		
#define Cmd_08				PIO_PB18_IDX			
#define Cmd_05				PIO_PB19_IDX			
#define Cmd_11				PIO_PB26_IDX		

#define Cmd_00_BV           PIO_PA6
#define Cmd_01_BV			PIO_PA4
#define Cmd_02_BV			PIO_PA23
#define Cmd_03_BV			PIO_PA22
#define Cmd_04_BV			PIO_PA24
#define Cmd_05_BV			PIO_PB19
#define Cmd_06_BV			PIO_PB7
#define Cmd_07_BV			PIO_PB13
#define Cmd_08_BV			PIO_PB18
#define Cmd_09_BV			PIO_PB8
#define Cmd_10_BV			PIO_PB10
#define Cmd_11_BV			PIO_PB26
#define Cmd_12_BV			PIO_PB11

#define Rev_In              PIO_PA15_IDX
#define Brk_In              PIO_PA21_IDX
#define Sel_1               PIO_PA5_IDX
#define Sel_2               PIO_PA7_IDX
#define Sel_3               PIO_PB23_IDX

void disable_twi_pins(void);
void enable_twi_pins(void);


void board_init_sleep(void);

