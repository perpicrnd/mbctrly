#include "user_board/init.h"

#include "ioport.h"

void enable_twi_pins(void) {
    //Inizializzazione del twi
    ioport_disable_pin(Twi_Sda);
    ioport_disable_pin(Twi_Scl);
    ioport_set_pin_mode(Twi_Sda, IOPORT_MODE_MUX_A);
    ioport_set_pin_mode(Twi_Scl, IOPORT_MODE_MUX_A);
}

void disable_twi_pins(void) {
    //Inizializzazione del twi
    ioport_enable_pin(Twi_Sda);
    ioport_enable_pin(Twi_Scl);
    ioport_set_pin_dir(Twi_Scl, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Twi_Sda, IOPORT_DIR_OUTPUT);
}

void board_init(void) {
    ioport_init();

    //Inizializzazione del CAN
    ioport_set_pin_dir(Can_0_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Can_0_Tx, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Can_0_Rx);
    ioport_disable_pin(Can_0_Tx);
    ioport_set_pin_mode(Can_0_Rx, IOPORT_MODE_MUX_A);
    ioport_set_pin_mode(Can_0_Tx, IOPORT_MODE_MUX_A);

    ioport_set_pin_dir(Can_1_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Can_1_Tx, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Can_1_Rx);
    ioport_disable_pin(Can_1_Tx);
    ioport_set_pin_mode(Can_1_Rx, IOPORT_MODE_MUX_A);
    ioport_set_pin_mode(Can_1_Tx, IOPORT_MODE_MUX_A);

    ioport_set_pin_dir(Can_0_RS_Ctrl_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Can_0_RS_Ctrl_Out, true);

    ioport_set_pin_dir(Can_1_RS_Ctrl_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Can_1_RS_Ctrl_Out, true);

    //Inizializzazione della UART
    ioport_set_pin_dir(Urxd_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Utxd_1, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Urxd_1);
    ioport_disable_pin(Utxd_1);
    ioport_set_pin_mode(Urxd_1, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);
    ioport_set_pin_mode(Utxd_1, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);

    //Inizializzazione della UART
    ioport_set_pin_dir(Urxd_2, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Utxd_2, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Urxd_2);
    ioport_disable_pin(Utxd_2);
    ioport_set_pin_mode(Urxd_2, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);
    ioport_set_pin_mode(Utxd_2, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Rxd_1, IOPORT_DIR_INPUT);
    ioport_disable_pin(Rxd_1);
    ioport_set_pin_mode(Rxd_1, IOPORT_MODE_MUX_A);
    ioport_set_pin_dir(Txd_1, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Txd_1);
    ioport_set_pin_mode(Txd_1, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Rxd_2, IOPORT_DIR_INPUT);
    ioport_disable_pin(Rxd_2);
    ioport_set_pin_mode(Rxd_2, IOPORT_MODE_MUX_A);
    ioport_set_pin_dir(Txd_2, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Txd_2);
    ioport_set_pin_mode(Txd_2, IOPORT_MODE_MUX_A | IOPORT_MODE_PULLUP);

    enable_twi_pins();

    ioport_set_pin_dir(An_Scale_1_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_1_1, true);
    ioport_set_pin_dir(An_Scale_1_2, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_1_2, true);
    ioport_set_pin_dir(An_Scale_2_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_2_1, true);
    ioport_set_pin_dir(An_Scale_2_2, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_2_2, true);

    //Inizializzazione dei servizi
    ioport_set_pin_dir(Lig_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Ign_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Spd_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Rev_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Brk_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Lig_Out, false);
    ioport_set_pin_level(Ign_Out, false);
    ioport_set_pin_level(Spd_Out, false);
    ioport_set_pin_level(Rev_Out, false);
    ioport_set_pin_level(Brk_Out, false);

    ioport_set_pin_dir(Spd_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Spd_In, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Spd_Out, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Spd_Out);
    ioport_set_pin_mode(Spd_Out, IOPORT_MODE_MUX_B);
    ioport_set_pin_dir(Ign_In_1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ign_In_1, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Ign_In_2, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ign_In_2, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(An_Vcc_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(An_Vcc_2, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Spare_1_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_1_Out, false);
    ioport_set_pin_dir(Spare_2_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_2_Out, false);
    ioport_set_pin_dir(Spare_3_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_3_Out, false);

    ioport_set_pin_dir(Sel_1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Sel_1, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Sel_2, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Sel_2, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Cmd_00, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_01, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_02, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_03, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_04, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_05, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_06, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_07, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_08, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_09, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_10, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_11, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_12, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(Cmd_00, false);
    ioport_set_pin_level(Cmd_01, false);
    ioport_set_pin_level(Cmd_02, false);
    ioport_set_pin_level(Cmd_03, false);
    ioport_set_pin_level(Cmd_04, false);
    ioport_set_pin_level(Cmd_05, false);
    ioport_set_pin_level(Cmd_06, false);
    ioport_set_pin_level(Cmd_07, false);
    ioport_set_pin_level(Cmd_08, false);
    ioport_set_pin_level(Cmd_09, false);
    ioport_set_pin_level(Cmd_10, false);
    ioport_set_pin_level(Cmd_11, false);
    ioport_set_pin_level(Cmd_12, false);
}

void board_init_sleep(void) {
    ioport_init();

    ioport_set_pin_dir(Can_0_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Can_0_Tx, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Can_0_Rx);
    ioport_disable_pin(Can_0_Tx);
    ioport_set_pin_mode(Can_0_Rx, IOPORT_MODE_MUX_A);
    ioport_set_pin_mode(Can_0_Tx, IOPORT_MODE_MUX_A);

    ioport_set_pin_dir(Can_1_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Can_1_Tx, IOPORT_DIR_OUTPUT);
    ioport_disable_pin(Can_1_Rx);
    ioport_disable_pin(Can_1_Tx);
    ioport_set_pin_mode(Can_1_Rx, IOPORT_MODE_MUX_A);
    ioport_set_pin_mode(Can_1_Tx, IOPORT_MODE_MUX_A);

    ioport_set_pin_dir(Can_0_RS_Ctrl_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Can_0_RS_Ctrl_Out, true);

    ioport_set_pin_dir(Can_1_RS_Ctrl_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Can_1_RS_Ctrl_Out, true);

    //Inizializzazione della UART
    ioport_set_pin_dir(Urxd_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Utxd_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Utxd_1, true);

    // //Inizializzazione della UART
    ioport_set_pin_dir(Urxd_2, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Utxd_2, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Utxd_2, true);

    ioport_set_pin_dir(Rxd_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Txd_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Txd_1, true);

    ioport_set_pin_dir(Twi_Sda, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Twi_Sda, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Twi_Scl, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Twi_Scl, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(An_Scale_1_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_1_1, true);
    ioport_set_pin_dir(An_Scale_1_2, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_1_2, true);
    ioport_set_pin_dir(An_Scale_2_1, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_2_1, true);
    ioport_set_pin_dir(An_Scale_2_2, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(An_Scale_2_2, true);

    // //Inizializzazione dei servizi
    ioport_set_pin_level(En_Services, false);
    ioport_set_pin_level(Ign_Out, false);
    ioport_set_pin_dir(En_Services, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Lig_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Ign_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Spd_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Rev_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Brk_Out, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(Lig_Out, false);
    ioport_set_pin_level(Spd_Out, false);
    ioport_set_pin_level(Rev_Out, false);
    ioport_set_pin_level(Brk_Out, false);

    ioport_set_pin_dir(Ign_In_1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ign_In_1, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Ign_In_2, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ign_In_2, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Spd_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Spd_In, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(An_Vcc_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(An_Vcc_2, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Spare_1_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_1_Out, false);
    ioport_set_pin_dir(Spare_2_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_2_Out, false);
    ioport_set_pin_dir(Spare_3_Out, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Spare_3_Out, false);

    ioport_set_pin_dir(Sel_1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Sel_1, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Sel_2, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Sel_2, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Sel_3, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Sel_3, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Cmd_00, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_01, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_02, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_03, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_04, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_05, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_06, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_07, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_08, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_09, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_10, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_11, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Cmd_12, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Cmd_00, false);
    ioport_set_pin_level(Cmd_01, false);
    ioport_set_pin_level(Cmd_02, false);
    ioport_set_pin_level(Cmd_03, false);
    ioport_set_pin_level(Cmd_04, false);
    ioport_set_pin_level(Cmd_05, false);
    ioport_set_pin_level(Cmd_06, false);
    ioport_set_pin_level(Cmd_07, false);
    ioport_set_pin_level(Cmd_08, false);
    ioport_set_pin_level(Cmd_09, false);
    ioport_set_pin_level(Cmd_10, false);
    ioport_set_pin_level(Cmd_11, false);
    ioport_set_pin_level(Cmd_12, false);

    ioport_set_pin_dir(Rev_In, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Brk_In, IOPORT_DIR_INPUT);
}