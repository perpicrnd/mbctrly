#pragma once

#ifdef __amd64__
extern int NumLoops;
#define FOREVER() ((NumLoops--) > 0 )
#else
#define FOREVER() 1
#endif

#ifdef __amd64__
#include "FreeRTOSConfig_host.h"
#endif

#ifdef BOARD
#include "FreeRTOSConfig_sam.h"
#endif


