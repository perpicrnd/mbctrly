#pragma once

#include <stdint.h>

enum ACCESSORY_COMMAND {
    ACCESSORY_FORMAT = 1,
    ACCESSORY_SERVICES,
    ACCESSORY_PARKING_SENSORS,
};

struct ACCESSORY_FORMAT_DATA {
    uint8_t version;
};

struct ACCESSORY_SERVICE_DATA {
    uint8_t ignition : 1;
    uint8_t reverse : 1;
    uint8_t lights : 1;
    uint8_t handbrake : 1;
    uint8_t reserved : 4;
    uint16_t speed;
};

union ACC_SERV_DATA {
    struct ACCESSORY_FORMAT_DATA as_format_data;
    struct PARKING_SENSOR_DATA as_parking_sensor_data;
    struct ACCESSORY_SERVICE_DATA as_service_data;
};

struct ACCESSORY_DATA {
    enum ACCESSORY_COMMAND command;
    union {
        union ACC_SERV_DATA as_data;
        uint8_t as_array[sizeof(union ACC_SERV_DATA)];
    };
};

#define ACCESSORY_EVENT_DATALEN (sizeof(union ACC_SERV_DATA))

typedef char accessory_check_border_fmt[(sizeof(struct ACCESSORY_FORMAT_DATA)) <= (sizeof(union ACC_SERV_DATA)) ? 1 : -1];
typedef char accessory_check_border_srv[(sizeof(struct ACCESSORY_SERVICE_DATA)) <= (sizeof(union ACC_SERV_DATA)) ? 1 : -1];
typedef char accessory_check_border_prk[(sizeof(struct PARKING_SENSOR_DATA)) <= (sizeof(union ACC_SERV_DATA)) ? 1 : -1];
//Se queste chiamate danno errore, Non c'è sufficiente spazio nel buffer di trasmissione.
struct EVENT_ACCESSORY_DATA {
    struct EVENT_GENERIC generic;
    struct ACCESSORY_DATA data;
};

void event_accessory_emit(struct ACCESSORY_DATA *accessory);