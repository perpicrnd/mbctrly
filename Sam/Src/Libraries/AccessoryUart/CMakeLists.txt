cmake_minimum_required(VERSION 3.8)

SET(LIBRARY_NAME accessory_uart)

set(SOURCES ${SOURCES}
	accessory_uart.c
)

if (CMAKE_CROSSCOMPILING)
else()
		add_subdirectory(Test)
endif()

add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries (${LIBRARY_NAME} perpic_msg_parser service_lib serial_task  )


target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .) 
