
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "accessory_uart.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

static union PERPIC_ACCESSORY_MESSAGE test_rx_message;
static struct EVENT_PARKING_SENSOR recv_data;
size_t __wrap_xStreamBufferSend(StreamBufferHandle_t xStreamBuffer,
                                const void *pvTxData,
                                size_t xDataLengthBytes,
                                TickType_t xTicksToWait) {
    (void)xStreamBuffer;
    (void)pvTxData;
    (void)xTicksToWait;
    function_called();
    const unsigned char *buff = pvTxData;
    for (uint8_t i = 0; i < xDataLengthBytes; i++) {
        perpic_message_raw_add_uint8_from_extern(&test_rx_message.as_raw, buff[i]);
    }
    return xDataLengthBytes;
}

void __wrap_event_parking_sensor_emit(struct EVENT_PARKING_SENSOR *data) {
    memcpy(&recv_data, data, sizeof(struct EVENT_PARKING_SENSOR));
}

void software_reset(void) {
}

void event_out_service_emit() {
}

void __wrap_serial_putstr(uint8_t *buff, int len) {
    (void)buff;
    (void)len;
    function_called();
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

static void fake_tx_rx(union PERPIC_ACCESSORY_MESSAGE *msg) {
    uint8_t tx_buffer[200] = {0x00};
    tx_buffer[0] = 0x9F;
    tx_buffer[1] = 0x02;
    tx_buffer[2] = msg->as_raw.pos;
    uint8_t j = 3;
    for (uint8_t i = 0; i <= msg->as_raw.pos; i++, j++) {
        switch (msg->as_raw.data[i]) {
            case 0x9F:
                tx_buffer[j] = 0x9F;
                j++;
                break;
        }
        tx_buffer[j] = msg->as_raw.data[i];
    }
    tx_buffer[j++] = 0x9F;
    tx_buffer[j++] = 0x03;
    for (uint8_t i = 0; i < j; i++) {
        perpic_message_raw_add_uint8_from_extern(&rx_message.as_raw, tx_buffer[i]);
    }
}

static void prepare_accessory_message(struct ACCESSORY_DATA *data) {
    union PERPIC_ACCESSORY_MESSAGE tmp_message;
    perpic_message_raw_reset(&tmp_message.as_raw);
    perpic_message_raw_add_uint8(&tmp_message.as_raw, IF_CODE_GENERIC);
    perpic_message_raw_add_uint8(&tmp_message.as_raw, IF_CODE_BROADCAST);
    perpic_message_raw_add_uint8(&tmp_message.as_raw, data->command);
    for (uint8_t i = 0; i < sizeof(struct EVENT_PARKING_SENSOR); i++) {
        perpic_message_raw_add_uint8(&tmp_message.as_raw, data->as_array[i]);
    }
    perpic_message_raw_add_crc(&tmp_message.as_raw);
    fake_tx_rx(&tmp_message);
}

static void test_received_message_off(void **state) {
    (void)state;
    assert_false(false);
    perpic_message_raw_reset(&rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x00000000;
    data.as_data.as_parking_sensor_data.park_status.as_uint8 = 0;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x0000;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x12;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = false;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x00;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data2(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x24;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x10;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data3(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x240;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0xFF;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data4(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x2400;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0xFD;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data5(void **state) {
    (void)state;
    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0xFFFF0000;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = false;
    data.as_data.as_parking_sensor_data.status.as_uint = 0xFF;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data6(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x0000FFFF;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x10;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data7(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x01234567;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x10;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data8(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x76543210;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x10;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data9(void **state) {
    (void)state;
    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x02468ACE;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0xAC;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

void test_receive_data10(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    data.command = ACCESSORY_PARKING_SENSORS;
    data.as_data.as_parking_sensor_data.distance.as_uint = 0x13579BDF;
    data.as_data.as_parking_sensor_data.park_status.as_flags.active = true;
    data.as_data.as_parking_sensor_data.status.as_uint = 0x5A;
    prepare_accessory_message(&data);
    expect_function_call(__wrap_xStreamBufferSend);
    process_received_message();
    assert_true(recv_data.prksens_type == PARKING_SENSOR_AFTERMARKET);
    assert_true(recv_data.data.distance.as_uint == data.as_data.as_parking_sensor_data.distance.as_uint);
    assert_true(recv_data.data.park_status.as_uint8 == data.as_data.as_parking_sensor_data.park_status.as_uint8);
    assert_true(recv_data.data.status.as_uint == data.as_data.as_parking_sensor_data.status.as_uint);
}

int setup(void **state) {
    (void)state;
    accessory_uart = &usart2;
    accessory_uart->init(accessory_uart, 115200, 'N');
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_received_message_off, setup),
        cmocka_unit_test_setup(test_receive_data, setup),
        cmocka_unit_test_setup(test_receive_data2, setup),
        cmocka_unit_test_setup(test_receive_data3, setup),
        cmocka_unit_test_setup(test_receive_data4, setup),
        cmocka_unit_test_setup(test_receive_data5, setup),
        cmocka_unit_test_setup(test_receive_data6, setup),
        cmocka_unit_test_setup(test_receive_data7, setup),
        cmocka_unit_test_setup(test_receive_data8, setup),
        cmocka_unit_test_setup(test_receive_data9, setup),
        cmocka_unit_test_setup(test_receive_data10, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}