#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "accessory_uart.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

static union PERPIC_ACCESSORY_MESSAGE test_rx_message;
size_t __wrap_xStreamBufferSend(StreamBufferHandle_t xStreamBuffer,
                                const void *pvTxData,
                                size_t xDataLengthBytes,
                                TickType_t xTicksToWait) {
    (void)xStreamBuffer;
    (void)pvTxData;
    (void)xTicksToWait;
    function_called();
    const unsigned char *buff = pvTxData;
    for (uint8_t i = 0; i < xDataLengthBytes; i++) {
        perpic_message_raw_add_uint8_from_extern(&test_rx_message.as_raw, buff[i]);
    }
    return xDataLengthBytes;
}

void software_reset(void) {
}

void event_out_service_emit() {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t speed) {
    check_expected(speed);
}

int setup(void **state) {
    (void)state;
    accessory_uart = &usart2;
    accessory_uart->init(accessory_uart, 115200, 'N');
    return 0;
}

static void test_received_message_off(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = false;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 0;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_ign_on(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = true;
    data.as_data.as_service_data.reverse = false;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 0;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_rev_on(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = true;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 0;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_lig_on(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = true;
    data.as_data.as_service_data.lights = true;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 0;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_hnd_on(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = true;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = true;
    data.as_data.as_service_data.speed = 0;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_spd_100(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = true;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 100;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

static void test_received_message_on(void **state) {
    (void)state;

    struct ACCESSORY_DATA data;
    struct ACCESSORY_DATA reread;
    data.command = ACCESSORY_SERVICES;
    data.as_data.as_service_data.ignition = false;
    data.as_data.as_service_data.reverse = true;
    data.as_data.as_service_data.lights = false;
    data.as_data.as_service_data.handbrake = false;
    data.as_data.as_service_data.speed = 500;
    perpic_message_raw_reset(&test_rx_message.as_raw);
    expect_function_call(__wrap_xStreamBufferSend);
    accessory_tx(&data, IF_CODE_BROADCAST);

    assert_true(perpic_message_raw_is_completed(&test_rx_message.as_raw));
    memcpy(&reread.as_array, &test_rx_message.as_raw.data[3], sizeof(struct ACCESSORY_SERVICE_DATA));
    assert_true(reread.as_data.as_service_data.ignition == data.as_data.as_service_data.ignition);
    assert_true(reread.as_data.as_service_data.handbrake == data.as_data.as_service_data.handbrake);
    assert_true(reread.as_data.as_service_data.lights == data.as_data.as_service_data.lights);
    assert_true(reread.as_data.as_service_data.reverse == data.as_data.as_service_data.reverse);
    assert_true(reread.as_data.as_service_data.speed == data.as_data.as_service_data.speed);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_received_message_off, setup),
        cmocka_unit_test_setup(test_received_message_ign_on, setup),
        cmocka_unit_test_setup(test_received_message_rev_on, setup),
        cmocka_unit_test_setup(test_received_message_lig_on, setup),
        cmocka_unit_test_setup(test_received_message_hnd_on, setup),
        cmocka_unit_test_setup(test_received_message_spd_100, setup),
        cmocka_unit_test_setup(test_received_message_on, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}