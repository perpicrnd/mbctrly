#pragma once

#include "perpic_message_raw.h"

union PERPIC_ACCESSORY_MESSAGE{
    struct PERPIC_RAW_MESSAGE as_raw;
};