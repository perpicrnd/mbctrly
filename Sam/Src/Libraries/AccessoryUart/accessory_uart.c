#include "accessory_uart.h"
#include <string.h>
#include "FreeRTOS.h"
#include "accessory_uart_priv.h"
#include "event_list.h"
#include "perpic_message_raw.h"
#include "queue.h"
#include "serials.h"
#include "task.h"
#include "timers.h"

#define CONTROLLER_QUEUE_NUM 40
#define SHARED_BUFFER_LEN 200

extern struct SERIAL usart2;
struct SERIAL *accessory_uart;

static union PERPIC_ACCESSORY_MESSAGE tx_message;
static union PERPIC_ACCESSORY_MESSAGE rx_message;
static QueueHandle_t task_queue;
static TimerHandle_t msec160;
static uint8_t timeout_counter = 0;
static bool ack_received = true;
static uint8_t nack_counter = 0;

static inline void reset_counters(void) {
    taskENTER_CRITICAL();
    timeout_counter = 0;
    nack_counter = 0;
    ack_received = true;
    taskEXIT_CRITICAL();
}

static uint8_t tx_shared_buffer[SHARED_BUFFER_LEN];
static uint8_t tx_shared_buffer_pos = 0;
static inline void reset_buffer(void) {
    for (uint8_t i = 0; i < tx_shared_buffer_pos; i++) {
        tx_shared_buffer[i] = 0x00;
    }
    tx_shared_buffer_pos = 0;
}

static inline void controller_print_buffer(void) {
    for (uint8_t i = 0; i < tx_shared_buffer_pos; i++) {
        LOG_DEF_NORMAL("%02X ", tx_shared_buffer[i]);
    }
    LOG_DEF_NORMAL("\r\n");
}

static void accessory_uart_tx_message(union PERPIC_ACCESSORY_MESSAGE *msg) {
    reset_buffer();
    tx_shared_buffer[0] = 0x9F;
    tx_shared_buffer[1] = 0x02;
    tx_shared_buffer[2] = msg->as_raw.pos;
    uint8_t j = 3;
    for (uint8_t i = 0; i <= msg->as_raw.pos; i++, j++) {
        switch (msg->as_raw.data[i]) {
            case 0x9F:
                tx_shared_buffer[j] = 0x9F;
                j++;
                break;
        }
        tx_shared_buffer[j] = msg->as_raw.data[i];
    }
    tx_shared_buffer[j++] = 0x9F;
    tx_shared_buffer[j++] = 0x03;
    xTimerStart(msec160, 0);
    tx_shared_buffer_pos = j;
    accessory_uart->put_string(accessory_uart, tx_shared_buffer, tx_shared_buffer_pos);
}

#define ACKNACK_LEN 8
static inline void accessory_uart_tx_message_ack(void) {
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x06\x07\x9F\x03"};
    accessory_uart->put_string(accessory_uart, tx_buffer, ACKNACK_LEN);
}

static inline void accessory_uart_tx_message_nack(void) {
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x15\x14\x9F\x03"};
    accessory_uart->put_string(accessory_uart, tx_buffer, ACKNACK_LEN);
}

static void accessory_tx(struct ACCESSORY_DATA *accessory, enum IF_CODE if_code_to) {
    perpic_message_raw_reset(&tx_message.as_raw);
    perpic_message_raw_add_uint8(&tx_message.as_raw, IF_CODE_SERVICE);
    perpic_message_raw_add_uint8(&tx_message.as_raw, if_code_to);
    perpic_message_raw_add_uint8(&tx_message.as_raw, accessory->command);
    switch (accessory->command) {
        case ACCESSORY_SERVICES:
            for (uint8_t i = 0; i < sizeof(struct ACCESSORY_SERVICE_DATA); i++) {
                LOG_DEF_NORMAL("%02X ", accessory->as_array[i]);
                perpic_message_raw_add_uint8(&tx_message.as_raw, accessory->as_array[i]);
            }
            perpic_message_raw_add_crc(&tx_message.as_raw);
            accessory_uart_tx_message(&tx_message);
            break;
        default:
            perpic_message_raw_add_crc(&tx_message.as_raw);
            accessory_uart_tx_message(&tx_message);
            break;
    }
}

#ifdef __amd64__

#else
static uint8_t counter = 0;
#endif

static void accessory_tx_message(union PERPIC_ACCESSORY_MESSAGE *msg) {
    reset_buffer();
    tx_shared_buffer[0] = 0x9F;
    tx_shared_buffer[1] = 0x02;
    tx_shared_buffer[2] = msg->as_raw.pos;
    uint8_t j = 3;
    for (uint8_t i = 0; i <= msg->as_raw.pos; i++, j++) {
        switch (msg->as_raw.data[i]) {
            case 0x9F:
                tx_shared_buffer[j] = 0x9F;
                j++;
                break;
        }
        tx_shared_buffer[j] = msg->as_raw.data[i];
    }
    tx_shared_buffer[j++] = 0x9F;
    tx_shared_buffer[j++] = 0x03;
    xTimerStart(msec160, 0);
    tx_shared_buffer_pos = j;
    accessory_uart->put_string(accessory_uart, tx_shared_buffer, tx_shared_buffer_pos);
}

static void parse_parking_sensors(struct PERPIC_RAW_MESSAGE *msg) {
    struct EVENT_PARKING_SENSOR prk;

    memcpy(&prk.data, &msg->data[3], sizeof(struct PARKING_SENSOR_DATA));
    prk.prksens_type = PARKING_SENSOR_AFTERMARKET;
    LOG_DEF_NORMAL("Parking sensor active: %02X\r\n", prk.data.park_status.as_uint8);
    event_parking_sensor_emit(&prk);
}

static void process_received_message(void) {
    if (perpic_message_raw_crc_check(&rx_message.as_raw) == PERPIC_CRC_OK) {
        accessory_uart_tx_message_ack();
        uint8_t receiver = perpic_message_raw_get_receiver(&rx_message.as_raw);
        if ((receiver == IF_CODE_ALPARK4) || (receiver == IF_CODE_BROADCAST)) {
            switch (perpic_message_raw_get_id(&rx_message.as_raw)) {
                case ACCESSORY_PARKING_SENSORS:
                    parse_parking_sensors(&rx_message.as_raw);
                    break;
                default:
                    LOG_DEF_NORMAL("No handlers for:\r\n");
                    perpic_message_raw_print(&rx_message.as_raw);
                    break;
            }
        }
    } else {
        accessory_uart_tx_message_nack();
    }
}

static SemaphoreHandle_t acknack_sem;
static bool clear_buffer = false;
static bool something_connected = false;
static void process_received_ack(void) {
    xTimerStop(msec160, 10);
    if (perpic_message_raw_is_nack(&rx_message.as_raw)) {
        if (nack_counter <= 3) {
            LOG_DEF_NORMAL("NACK\r\n");
            xSemaphoreGive(acknack_sem);
            nack_counter++;
        } else {
            LOG_DEF_NORMAL("3 NACK, give up transmitting\r\n");
            controller_print_buffer();
            reset_counters();
        }
    } else {
        reset_counters();
    }
}

static void accessory_task(void *parameter) {
    (void)parameter;
    accessory_uart->init(accessory_uart, 115200, 'N');
    for (;;) {
        do {
            if (xSemaphoreTake(acknack_sem, 0)) {
                if (something_connected) {
                    accessory_tx_message(&tx_message);
                    break;
                }
            }

            if (ack_received) {
                struct ACCESSORY_DATA msg;
                if (xQueueReceive(task_queue, &msg, 0)) {
    
                    if (something_connected) {
                        accessory_tx(&msg, IF_CODE_BROADCAST);
                        ack_received = false;
                    }
                }
            }
        } while (0);

        char c;
        bool ch_recv;
        do {
            ch_recv = false;
            if (xStreamBufferReceive(accessory_uart->xBufferRxUartMessage, &c, 1, 10)) {
                ch_recv = true;
                perpic_message_raw_add_uint8_from_extern(&rx_message.as_raw, c);
                if (perpic_message_raw_is_completed(&rx_message.as_raw)) {
                    something_connected = true;
                    if (perpic_message_raw_is_acknack(&rx_message.as_raw)) {
                        process_received_ack();
                    } else {
                        process_received_message();
                    }
                    perpic_message_raw_reset(&rx_message.as_raw);
                }
            }
        } while (ch_recv);

#ifdef __amd64__

#else
        if (counter++ == 0) {
            if (uxTaskGetStackHighWaterMark(NULL) < 100) {
                LOG_DEF_NORMAL("%s stack too low.\r\n", __PRETTY_FUNCTION__);
            }
        }
#endif
    }
}

static void timer_msec160(TimerHandle_t pxTimer) {
    (void)pxTimer;
    ///Release the semaphore for unlock the radio task.
    //Non ho un sequence number sul timeout.
    clear_buffer = true;
    if (timeout_counter < 3) {
        LOG_DEF_NORMAL("timeout\r\n");
        timeout_counter++;
        xSemaphoreGive(acknack_sem);
    } else {
        LOG_DEF_NORMAL("Too much errors without reply, give up\r\n");
        xQueueReset(task_queue);
        controller_print_buffer();
        reset_counters();
    }
}

static void handle_services(const union EVENT *const event) {
    if (event->as_generic.event == eEVENT_OUT_SERVICE_CHANGED) {
        struct ACCESSORY_DATA accessory;
        struct ACCESSORY_SERVICE_DATA services = {0};
        if (event->as_out_service.out_service.ignition == IGNITION_ON) {
            services.ignition = 1;
        }
        if (event->as_out_service.out_service.handbrake == HANDBRAKE_ON) {
            services.handbrake = 1;
        }
        if (event->as_out_service.out_service.lights == LIGHTS_ON) {
            services.lights = 1;
        }
        if (event->as_out_service.out_service.reverse == REVERSE_ON) {
            services.reverse = 1;
        }
        services.speed = event->as_out_service.out_service.speed;
        memcpy(&accessory.as_data.as_service_data, &services, sizeof(struct ACCESSORY_SERVICE_DATA));
        accessory.command = ACCESSORY_SERVICES;
        event_accessory_emit(&accessory);
    }
}

static bool configured = false;
void accessory_uart_init(void) {
    if (!configured) {
        accessory_uart = &usart2;
        xTaskCreate(accessory_task, (char *)"acc_task", configMINIMAL_STACK_SIZE + 200, NULL, configMAX_PRIORITIES - 2, NULL);
        task_queue = xQueueCreate(CONTROLLER_QUEUE_NUM, sizeof(struct ACCESSORY_DATA));
        msec160 = xTimerCreate((char *)"t_160", 160 / portTICK_PERIOD_MS, pdTRUE, 0, timer_msec160);
        acknack_sem = xSemaphoreCreateBinary();
        configured = true;
        event_connect_callback(eEVENT_OUT_SERVICE_CHANGED, handle_services);
    }
}

void event_accessory_emit(struct ACCESSORY_DATA *accessory) {
    xQueueSendToBack(task_queue, accessory, 2 / portTICK_PERIOD_MS);
}
