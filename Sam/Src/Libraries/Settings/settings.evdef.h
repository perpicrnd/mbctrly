#pragma once

enum SETTING_READ_TYPE{
    SETTING_READ_BUTTONS,
    SETTING_READ_VEHICLE_TYPE,
    SETTING_READ_END,
    SETTING_READ_BUTTONS_RETRY,
};

struct EVENT_SETTINGS_READ{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
    enum SETTING_READ_TYPE type;
};


/**
 * @brief emette una notifica di lettura di settaggi.
 */
void event_settings_read_emit(enum SETTING_READ_TYPE type);
