#include "settings.h"
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "event_list.h"
#include "logger.h"
#include "timers.h"

union EEPROM_MEMORY {
    uint8_t eeprom_data_bank[E2ARRAY_PART][E2ARRAY_PART_LEN];
    uint8_t eeprom_array[E2ARRAY];
};

uint8_t eeprom_init_read_pos = 0;
static TimerHandle_t timer_fill;
static union EEPROM_MEMORY eeprom_data;
static bool first_read_done = false;

uint8_t settings_read(uint8_t reg_num) {
    return eeprom_data.eeprom_array[reg_num];
}

void settings_write(uint8_t reg_num, uint8_t data) {
    if (eeprom_data.eeprom_array[reg_num] != data) {
        eeprom_data.eeprom_array[reg_num] = data;
        struct CONTROLLER_DATA ctrl;
        ctrl.command = CONTROLLER_WO_EEPROM;
        ctrl.data[0] = reg_num;
        ctrl.data[1] = data;
        event_controller_emit(&ctrl);
    }
}

void settings_start_fill(uint8_t pos, const uint8_t len, const uint8_t *data) {
    uint8_t bs = pos * E2ARRAY_PART_LEN;
    for (uint8_t i = 0; i < len; i++) {
        if ((bs + i) < (E2ARRAY)) {
            eeprom_data.eeprom_array[bs + i] = data[i];
            if (bs + i == VEHICLE_TYPE) {
                event_settings_read_emit(SETTING_READ_VEHICLE_TYPE);
            }
            eeprom_init_read_pos = bs + i;
            if ((bs + i) == SETTINGS_FULLY_READ) {
                if (!eeprom_data.eeprom_array[SETTINGS_FULLY_READ]) {
                    settings_write(SETTINGS_FULLY_READ, true);
                }
                event_settings_read_emit(SETTING_READ_END);
            }
            if ((bs + i) <= (E2ARRAY)) {
                eeprom_init_read_pos++;
            }
        }
    }
    first_read_done = true;
}

static void timer_fill_func(TimerHandle_t pxTimer) {
    (void)pxTimer;
    if (eeprom_init_read_pos >= (SETTINGS_FULLY_READ)) {
        xTimerStop(timer_fill, 10);
    } else {
        xTimerChangePeriod(timer_fill, 300 / portTICK_PERIOD_MS, 10);
    }
    struct CONTROLLER_DATA data;
    data.command = CONTROLLER_RO_EEPROM_PAGE;
    data.data0 = eeprom_init_read_pos / E2ARRAY_PART_LEN;
    LOG_DEF_NORMAL("Filling E2 runtime %d %d.\r\n", eeprom_init_read_pos, data.data0);
    data.data[1] = 0;
    data.callback = settings_start_fill;
    event_controller_emit(&data);
}

void settings_factory_reset(void) {
    struct CONTROLLER_DATA data;
    data.command = CONTROLLER_FACTORY_SETTING;
    event_controller_emit(&data);
}

void reread_all_settings(void) {
    if (first_read_done) {
        if (!xTimerIsTimerActive(timer_fill)) {
            LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
            eeprom_init_read_pos = 0;
            xTimerStart(timer_fill, 10);
            first_read_done = false;
        }
    }
}

static bool configured = false;
void settings_init(void) {
    if (!configured) {
        for (uint8_t i = 0; i < E2ARRAY; i++) {
            eeprom_data.eeprom_array[i] = 0xFF;
        }
        configured = true;
        timer_fill = xTimerCreate((char *)"t_160", 400 / portTICK_PERIOD_MS, pdTRUE, 0, timer_fill_func);
        xTimerStart(timer_fill, 10);
    }
}

void event_settings_read_emit(enum SETTING_READ_TYPE type) {
    union EVENT event;
    event.as_generic.event = eEVENT_SETTINGS_READ;
    event.as_settings_read.type = type;
    event_emit(&event);
}