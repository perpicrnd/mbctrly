
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "camos_parksens.c"
void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void test_one_sensor(void **state){
    (void)state;
    assert_true(true);
}

int setup(void **state) {
    (void)state;
  
    return 0;
}

void test_parking_sensor_zone_distance(void **state) {
    (void)state;
    
    uint8_t status;
    uint8_t distance;

    parking_sensor_zone_distance(0, 0, &status, &distance);
    assert_true(status == 3);
    assert_true(distance == 1);
    parking_sensor_zone_distance(1, 0, &status, &distance);
    assert_true(status == 2);
    assert_true(distance == 5);
    parking_sensor_zone_distance(2, 0, &status, &distance);
    assert_true(status == 1);
    assert_true(distance == 10);
    parking_sensor_zone_distance(3, 0, &status, &distance);
    assert_true(status == 1);
    assert_true(distance == 0x0F);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_one_sensor, setup),
        cmocka_unit_test_setup(test_parking_sensor_zone_distance, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
