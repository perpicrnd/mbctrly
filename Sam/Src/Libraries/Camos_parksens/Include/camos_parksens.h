#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "canbus.h"
#include "logger.h"

//void camos_parksens_init(void);
void camos_parksens_init(struct CANBUS_CONFIG *can_conf);