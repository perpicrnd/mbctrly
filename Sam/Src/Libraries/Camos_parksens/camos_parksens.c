#include "camos_parksens.h"
#include "camos_parksens_priv.h"
#include "event_list.h"
#include "radio_message.h"
#include "reverse.h"

static bool sensor_status[6];
static struct EVENT_PARKING_SENSOR prk = {0};
static struct EVENT_PARKING_SENSOR old_prk = {0};

static inline void parking_sensor_zone_distance(uint8_t value, uint8_t pos, uint8_t *status, uint8_t *distance) {
    switch (value) {
        case 0:
            *status = 3;
            *distance = 1;
            break;

        case 1:
            *status = 2;
            *distance = 5;
            break;

        case 2:
            *status = 1;
            if (pos < 3) {
                *distance = 10;
            } else {
                *distance = 7;
            }
            break;

        default:
            *status = 1;
            *distance = 0x0F;
            break;
    }
}

static void limit(uint8_t *dist, uint8_t *stat, uint8_t distance, uint8_t status) {
    if ((distance > 5) && (distance < 0x0F) && (status == 1)) {
        *dist = 5;
        *stat = 2;
    } else {
        *dist = distance;
        *stat = status;
    }
}

static inline void assign_status_distance(uint8_t pos, uint8_t status, uint8_t distance) {
    uint8_t lim_distance;
    uint8_t lim_status;
    switch (pos) {
        case 0x00:
            limit(&lim_distance, &lim_status, distance, status);
            prk.data.distance.zone_e = lim_distance;
            prk.data.status.zone_e = lim_status;
            if (!sensor_status[1]) {
                prk.data.distance.zone_f = distance;
                prk.data.status.zone_f = status;
            }
            break;
        case 0x01:
            prk.data.distance.zone_f = distance;
            prk.data.status.zone_f = status;
            prk.data.distance.zone_g = distance;
            prk.data.status.zone_g = status;
            if (!sensor_status[0]) {
                limit(&lim_distance, &lim_status, distance, status);
                prk.data.distance.zone_e = lim_distance;
                prk.data.status.zone_e = lim_status;
            }
            if (!sensor_status[2]) {
                limit(&lim_distance, &lim_status, distance, status);
                prk.data.distance.zone_h = lim_distance;
                prk.data.status.zone_h = lim_status;
            }
            break;
        case 0x02:
            limit(&lim_distance, &lim_status, distance, status);
            prk.data.distance.zone_h = lim_distance;
            prk.data.status.zone_h = lim_status;
            if (!sensor_status[1]) {
                prk.data.distance.zone_g = distance;
                prk.data.status.zone_g = status;
            }
            break;
        case 0x03:
            limit(&lim_distance, &lim_status, distance, status);
            prk.data.distance.zone_a = lim_distance;
            prk.data.status.zone_a = lim_status;
            if (!sensor_status[4]) {
                prk.data.distance.zone_b = distance;
                prk.data.status.zone_b = status;
            }
            break;
        case 0x04:
            prk.data.distance.zone_b = distance;
            prk.data.status.zone_b = status;
            prk.data.distance.zone_c = distance;
            prk.data.status.zone_c = status;
            if (!sensor_status[3]) {
                limit(&lim_distance, &lim_status, distance, status);
                prk.data.distance.zone_a = lim_distance;
                prk.data.status.zone_a = lim_status;
            }
            if (!sensor_status[5]) {
                limit(&lim_distance, &lim_status, distance, status);
                prk.data.distance.zone_d = lim_distance;
                prk.data.status.zone_d = lim_status;
            }
            break;
        case 0x05:
            limit(&lim_distance, &lim_status, distance, status);
            prk.data.distance.zone_d = lim_distance;
            prk.data.status.zone_d = lim_status;
            if (!sensor_status[4]) {
                prk.data.distance.zone_c = distance;
                prk.data.status.zone_c = status;
            }
            break;
        default:
            break;
    }
}

#define BUFF_LEN 10
struct SENSOR_BUFF {
    uint8_t buff[BUFF_LEN];
    uint8_t buff_pos;
};

struct SENSOR_BUFF sensor_buffer[6] = {0};

uint8_t min(uint8_t *buff, uint8_t val, uint8_t *pos) {
    uint8_t retVal = 0xFF;
    buff[*pos] = val;
    *pos += 1;
    *pos = (*pos) % BUFF_LEN;

    for (uint8_t i = 0; i < BUFF_LEN; i++) {
        if (buff[i] < retVal) {
            retVal = buff[i];
        }
    }
    return retVal;
}

static uint8_t reset_counter;
static uint8_t disable_counter = 0;
static void handle_parksens(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_CAN1_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_1) {
            uint32_t id = msg->as_can_message.msg.can_id & 0x00FFFFFF;
            uint8_t byte0 = msg->as_can_message.msg.can_data.byte0;
            if (((id == 0x5115AB) || (id == 0x6015AB)) && (byte0 == 1)) {
                LOG_DEF_NORMAL("Camos prk sens\r\n");
                uint8_t pos = (msg->as_can_message.msg.can_id >> 24) - 1;
                if (pos == 5) {
                    reset_counter = 0;
                } else {
                    if (reset_counter < 0xFE) {
                        reset_counter++;
                    }
                }

                if (pos <= 5) {
                    sensor_status[pos] = true;

                    uint8_t distance;
                    uint8_t status;
                    uint8_t value;

                    value = msg->as_can_message.msg.can_data.byte1;
                    value = min(sensor_buffer[pos].buff, value, &sensor_buffer[pos].buff_pos);

                    //parking_sensor_zone_distance(msg->as_can_message.msg.can_data.byte1, &status, &distance);
                    parking_sensor_zone_distance(value, pos, &status, &distance);
                    assign_status_distance(pos, status, distance);

                    prk.prksens_type = PARKING_SENSOR_AFTERMARKET;
                    if (memcmp(&prk, &old_prk, sizeof(struct EVENT_PARKING_SENSOR))) {
                        event_parking_sensor_emit(&prk);
                        memcpy(&old_prk, &prk, sizeof(struct EVENT_PARKING_SENSOR));
                    }
                }
                alpine_menu_parking_sensors_type(PARKING_SENSOR_TYPE_BOTH);
            }
        }
    }
}

static void tx_parking_sensors(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (disable_counter != 0) {
            disable_counter--;
            LOG_DEF_NORMAL("Disable counter %d\r\n", disable_counter);
            event_parking_sensor_emit(&prk);
        }
    }
}

static void reset_prksens(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        if (reset_counter > 0x20) {
            LOG_DEF_NORMAL("Resetto sensori.\r\n");
            reset_counter = 0x00;
            event_lights_emit(LIGHTS_OFF);
        }
    }
}

static bool speed_enabler = false;
static void handle_lights(const union EVENT *const msg) {
    bool perform_check = false;
    if (msg->as_generic.event == eEVENT_SPEED_CHANGED) {
        speed_enabler = ((msg->as_speed.speed != 0) && (msg->as_speed.speed < 100));
        perform_check = true;
    }
    if (msg->as_generic.event == eEVENT_REVERSE_CHANGED) {
        perform_check = true;
    }
    if (perform_check) {
        if (reverse_hw_get() || speed_enabler) {
            event_lights_emit(LIGHTS_ON);
            prk.data.park_status.as_flags.active = true;
        } else {
            event_lights_emit(LIGHTS_OFF);
            if (prk.data.park_status.as_flags.active) {
                disable_counter = 10;
            }
            prk.data.park_status.as_flags.active = false;
        }
    }
}
static void handle_config_setup(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SETTINGS_READ) {
        canbus1_enable_tx();
    }
}

static bool configured = false;
void camos_parksens_init(struct CANBUS_CONFIG *can_conf) {
    if (!configured) {
        configured = true;

        //struct CANBUS_CONFIG can_conf = {0};
        can_conf->can1.can_speed = 500;
        can_conf->can1.listen_mode = false;
        //canbus_init(&can_conf);

        event_connect_callback(eEVENT_SPEED_CHANGED, handle_lights);
        event_connect_callback(eEVENT_REVERSE_CHANGED, handle_lights);
        event_connect_callback(eEVENT_SECONDS_CHANGED, tx_parking_sensors);

        event_connect_callback(eEVENT_CAN1_RX_MESSAGE, handle_parksens);
        event_connect_callback(eEVENT_50MSEC_CHANGED, reset_prksens);
        event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x016015AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x026015AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x036015AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x046015AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x056015AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x066015AB);

        canbus_add_accepted_id(CAN_INTERFACE_1, 0x015115AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x025115AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x035115AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x045115AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x055115AB);
        canbus_add_accepted_id(CAN_INTERFACE_1, 0x065115AB);
    }
}