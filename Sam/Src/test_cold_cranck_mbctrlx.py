#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial
import sys
import os
import time
from random import randint
    
if os.name == "POSIX" or os.name == "posix":
    ser = serial.Serial("/dev/ttyUSB0", 230400, 8, 'N', 1, timeout=0.1)
else:
    ser = serial.Serial("COM50", 230400, 8 , 'N', timeout=0.1)

def write_command(command):
    print(command)
    for c in command:
        ser.write(c)
        time.sleep(0.005)
    ser.write("\n")
    time.sleep(0.05)
    print(ser.readall())
    sys.stdout.flush()


def sleep(sec):
    print("Sleeping for "),
    print(sec),
    print(" seconds.")
    sys.stdout.flush()

    s = 0
    while s < sec:
        print(ser.readall()),
        s += 0.1
#        #time.sleep(0.1)
#        print(" ")
        if (int(s) % 10) is 0:
#            print("Mancano "),
#            print(sec - s),
#            print(" secondi.")
             sys.stdout.flush()



write_command("voltage 12000")
sleep (2.00)
write_command("can_init 250")
sleep (2.00)
write_command("ignition on")
sleep (2.00) 
write_command("mrestore")
sleep(1.00)
write_command("mprint")
write_command("mwait 9")
while True:
    sleep (0.100)  
    
    
    write_command("voltage 12000")
    write_command("delay 4000")
    write_command("voltage_scale 4000 5")
    write_command("delay 9000")
    write_command("voltage_scale 6000 50")
    write_command("delay 5300")
    write_command("voltage_scale 12000 100")

    
    
    sleep(randint(30, 60))
    print("fine invio comandi.")
    sys.stdout.flush()


    sleep (4)
    print("Un altro ciclo.") 
    sys.stdout.flush()

