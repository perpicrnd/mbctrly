#include <string.h>

#include "canbus.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"
#include "wisker.h"
#include "event_list.h"
#include "asf.h"
#include "logger.h"
/**
 * @addtogroup CANBUS
 * @{
 */

/**
 * @brief CAN0 Transceiver
 */
static sn65hvd234_ctrl_t can0_transceiver;

/**
 * @brief CAN1 Transceiver
 */
static sn65hvd234_ctrl_t can1_transceiver;

/**
 * @brief CAN0 Mailbox utilizzata per la ricezione dei dati dal canbus
 */
static can_mb_conf_t can0_mailbox;

/**
 * @brief Priorità della mailbox in trasmissione
 */
#define CAN_TX_PRIO         15
/**
 * numero delle mailbox utilizzate per la trasmissione
 */
#define MAILBOX_NUM			5
/**
 * @brief Array contenente il numero della mailbox attiva in trasmissione sui due canali.
 */
static uint8_t active_send_mbox[CAN_INTERFACE_COUNT];
/**
 * @brief Array contenente le mailbox utilizzate per la trasmissione sui due canali.
 */
static can_mb_conf_t can_send_mailbox[CAN_INTERFACE_COUNT][MAILBOX_NUM];

/**
 * @brief flag che indica se il can0 è stato inizializzato.
 */
static bool can0_initialized = false;
/**
 * @brief flag che indica se il can1 è stato inizializzato.
 */
static bool can1_initialized = false;
/**
 * @brief CAN1 mailbox utilizzata per la ricezione dei messaggi dal can1
 */
static can_mb_conf_t can1_mailbox;

/**
 * @brief array contenente il numero di messaggi ricevuti dal canbus.
 */
static volatile uint32_t msg_num[CAN_INTERFACE_COUNT];

/**
 * @brief struttura utilizzata per la conversione dei dati da can_mb_conf_t a struttura \ref CAN_MESSAGE.
 */
typedef struct{
	uint32_t byte0: 8;
	uint32_t byte1: 8;
	uint32_t byte2: 8;
	uint32_t byte3: 8;
}_HALF_CAN_DATA;

/**
 * @brief struttura utilizzata per la conversione dei dati da can_mb_conf_t a struttura \ref CAN_MESSAGE.
 */
typedef struct{
	_HALF_CAN_DATA low;
	_HALF_CAN_DATA high;
}_CAN_DATA;

/**
 * @brief struttura utilizzata per la conversione dei dati da can_mb_conf_t a struttura \ref CAN_MESSAGE.
 */
typedef struct{
	uint32_t low;
	uint32_t high;
}_CAN_DATA_32;

/**
 * @brief struttura utilizzata per la conversione dei dati da can_mb_conf_t a struttura \ref CAN_MESSAGE.
 */
typedef union{
	CAN_DATA as_uint8;
	_CAN_DATA as_struct;
	_CAN_DATA_32 as_uint32;
}CAN_DATA2;


uint32_t canbus_get_message_num(CAN_INTERFACE interface){
	return msg_num[interface];
}

///Definito in can_task.c
extern uint32_t can_accepted_id[CAN_INTERFACE_COUNT][ACCEPTED_ID_NUM];

/**
 * Check if the id passed as parameter is accepted on the selected interface.
 * @param interface the interface to query
 * @param num the id number to check
 * @return true if the id is accepted, false otherwise.
 */
static inline bool can_is_accepted_id(CAN_INTERFACE interface, uint32_t num);
static inline bool can_is_accepted_id(CAN_INTERFACE interface, uint32_t num){
	bool found = false;
	for (uint8_t i = 0; i<ACCEPTED_ID_NUM && found == false; i++){
		if (can_accepted_id[interface][i] == num){
			found = true;
		}
	}
	return found;
}

/**
 * \brief Reset mailbox configure structure.
 *
 *  \param p_mailbox Pointer to mailbox configure structure.
 */
static inline void reset_mailbox_conf(can_mb_conf_t *p_mailbox)
{
	p_mailbox->ul_mb_idx = 0;
	p_mailbox->uc_obj_type = 0;
	p_mailbox->uc_id_ver = 0;
	p_mailbox->uc_length = 0;
	p_mailbox->uc_tx_prio = 0;
	p_mailbox->ul_status = 0;
	p_mailbox->ul_id_msk = 0;
	p_mailbox->ul_id = 0;
	p_mailbox->ul_fid = 0;
	p_mailbox->ul_datal = 0;
	p_mailbox->ul_datah = 0;
}

/**
 * \brief Default interrupt handler for CAN 0.
 */
void CAN0_Handler(void)
{
	uint32_t ul_status;
	ul_status = can_get_status(CAN0);
	if (ul_status & GLOBAL_MAILBOX_MASK) {
		for (uint8_t i = 0; i < CANMB_NUMBER; i++) {
			ul_status = can_mailbox_get_status(CAN0, i);
			if ((ul_status & CAN_MSR_MRDY) == CAN_MSR_MRDY) {
				can0_mailbox.ul_mb_idx = i;
				can0_mailbox.ul_status = ul_status;
				can_mailbox_read(CAN0, &can0_mailbox);
				CAN_DATA2 tmp;
				tmp.as_uint32.high = can0_mailbox.ul_datah;
				tmp.as_uint32.low = can0_mailbox.ul_datal;
				CAN_MESSAGE to_send;
				memcpy(&to_send.can_data, &tmp.as_uint8, sizeof(CAN_DATA));
				to_send.can_id = can0_mailbox.ul_id;
				to_send.datalen = can0_mailbox.uc_length;
				to_send.interface = CAN_INTERFACE_0;
				to_send.can_is_ext = can0_mailbox.uc_id_ver;
				reset_mailbox_conf(&can0_mailbox);
				BaseType_t xHigherPriorityTaskWoken = pdFALSE;
				msg_num[CAN_INTERFACE_0]++;
				if (can_is_accepted_id(CAN_INTERFACE_0, to_send.can_id)){
					union EVENT event;
					event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
					memcpy(&event.as_can_message.msg, &to_send, sizeof(CAN_MESSAGE));
					event_emit_fromISR(&event, &xHigherPriorityTaskWoken);
					if( xHigherPriorityTaskWoken )
					{
						taskYIELD ();
					}
					break;
				}
				break;
			}
		}
	}
}

/**
 * \brief Default interrupt handler for CAN 1.
 */
void CAN1_Handler(void)
{
	uint32_t ul_status;
	ul_status = can_get_status(CAN1);
	if (ul_status & GLOBAL_MAILBOX_MASK) {
		for (uint8_t i = 0; i < CANMB_NUMBER; i++) {
			ul_status = can_mailbox_get_status(CAN1, i);
			if ((ul_status & CAN_MSR_MRDY) == CAN_MSR_MRDY) {
				can1_mailbox.ul_mb_idx = i;
				can1_mailbox.ul_status = ul_status;
				can_mailbox_read(CAN1, &can1_mailbox);
				CAN_DATA2 tmp;
				tmp.as_uint32.high = can1_mailbox.ul_datah;
				tmp.as_uint32.low = can1_mailbox.ul_datal;
				CAN_MESSAGE to_send;
				memcpy(&to_send.can_data, &tmp.as_uint8, sizeof(CAN_DATA));
				to_send.can_id = can1_mailbox.ul_id;
				to_send.datalen = can1_mailbox.uc_length;
				to_send.interface = CAN_INTERFACE_1;
				to_send.can_is_ext = can1_mailbox.uc_id_ver;
				reset_mailbox_conf(&can1_mailbox);


				BaseType_t xHigherPriorityTaskWoken = pdFALSE;
				msg_num[CAN_INTERFACE_1]++;
				if (can_is_accepted_id(CAN_INTERFACE_1, to_send.can_id)){
					union EVENT event;
					event.as_generic.event = eEVENT_CAN1_RX_MESSAGE;
					memcpy(&event.as_can_message.msg, &to_send, sizeof(CAN_MESSAGE));
					event_emit_fromISR(&event, &xHigherPriorityTaskWoken);
					if( xHigherPriorityTaskWoken )
					{
						taskYIELD ();
					}
					break;
				}
				break;
			}
		}
	}
}


void canbus_send_message(const CAN_MESSAGE * const msg){
	uint32_t tmp = 0;

	tmp |= msg->can_data.byte0 | msg->can_data.byte1 << 8 | msg->can_data.byte2 << 16 | msg->can_data.byte3 << 24;
	can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].ul_datal = tmp;
	tmp = 0;
	tmp |= msg->can_data.byte4| msg->can_data.byte5 << 8 | msg->can_data.byte6 << 16 | msg->can_data.byte7 << 24;
	can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].ul_datah = tmp;
	if (msg->can_is_ext){
		can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].uc_id_ver = 1;
		can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].ul_id = msg->can_id;
	}else{
		can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].uc_id_ver = 0;
		can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].ul_id = CAN_MID_MIDvA(msg->can_id);
	}
	can_send_mailbox[msg->interface][active_send_mbox[msg->interface]].uc_length = msg->datalen;
	//Aspetto che la mailbox sia vuota.
	//salvo il timer di sistema.

	Can *interface = CAN1;
	if (msg->interface == CAN_INTERFACE_0){
		interface = CAN0;
	}else if (msg->interface == CAN_INTERFACE_1){
		interface = CAN1;
	}else{
		LOG_DEF_NORMAL("Impossible can interface\r\n");
	}

	uint8_t counter = 0;
	while(CAN_MAILBOX_NOT_READY == can_mailbox_write(interface, &can_send_mailbox[msg->interface][active_send_mbox[msg->interface]])){
		LOG_DEF_NORMAL("Mailbox %d piena %d\r\n", msg->interface, active_send_mbox[msg->interface]);
		vTaskDelay(5/portTICK_PERIOD_MS);
		if (counter++ > 10){
			//se il timer incrementa più di 10ms resetto la mailbox.
			LOG_DEF_NORMAL("Abort!!!\r\n");
			can_mailbox_send_abort_cmd(interface, &can_send_mailbox[msg->interface][active_send_mbox[msg->interface]]);
			return;
		}
	}
	can_global_send_transfer_cmd(interface, CAN_TCR_MB3<<active_send_mbox[msg->interface]);
	active_send_mbox[msg->interface]++;
	active_send_mbox[msg->interface] = active_send_mbox[msg->interface] % MAILBOX_NUM;
}

TimerHandle_t timer_fault = NULL;
static void vTimerFault(TimerHandle_t handle){
	(void)handle;
	LOG_DEF_NORMAL("too much CAN errors\r\n");

	if(1)
	{//FIXME: sistemare if (selector_get_current() SELECTOR_FIRMWARE_UPGRADE){
		//Non voglio andare avanti con il reset.
		LOG_DEF_NORMAL("Updater, not resetting\r\n");
		return;
	}else{
		LOG_DEF_NORMAL("Asking for a reset\r\n");

		vTaskDelay(10/portTICK_PERIOD_MS);
		//FIXME: da sistemare
//		write_board_controller_registers('8', 0);
		vTaskDelay(10/portTICK_PERIOD_MS);
		rstc_reset_extern(RSTC);

	}

}

//FIXME: Vedere se è da reinserire
//static inline void update_virtual_can_reg(uint8_t *reg, bool val){
//	if (!val){
//		if (*reg < 0xA0){
//			*reg += 8;
//		}
//	}else{
//		if (*reg > 0){
//			*reg-=1;
//		}
//	}
//}

//FIXME: Vedere se è da reinserire
//static inline void can_task_detect_errors(void){
//	uint32_t ul_status = can_get_status(CAN0);
//	if (ul_status & (CAN_SR_WARN | CAN_SR_ERRP | CAN_SR_BOFF | CAN_SR_CERR | CAN_SR_SERR | CAN_SR_AERR | CAN_SR_FERR | CAN_SR_BERR)){
//		LOG_DEF_NORMAL("Verifica stato: %08lX %02X %02X\r\n", ul_status, can_get_tx_error_cnt(CAN0), can_get_rx_error_cnt(CAN0));
//		if (ul_status & CAN_SR_WARN){
//			LOG_DEF_NORMAL("CAN_SR_WARN\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_ERRP){
//			LOG_DEF_NORMAL("CAN_SR_ERRP\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_BOFF){
//			LOG_DEF_NORMAL("CAN_SR_BOFF\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_CERR){
//			LOG_DEF_NORMAL("CAN_SR_CERR\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_SERR){
//			LOG_DEF_NORMAL("CAN_SR_SERR\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_AERR){
//			LOG_DEF_NORMAL("CAN_SR_AERR\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_FERR){
//			LOG_DEF_NORMAL("CAN_SR_FERR\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//		if (ul_status & CAN_SR_BERR){
//			LOG_DEF_NORMAL("CAN_SR_BERR\r\n");
//			vTaskDelay(10/portTICK_PERIOD_MS);
//		}
//	}
//
//	static uint8_t can_sr_errp;
//	static uint8_t can_sr_boff;
//	static uint8_t can_sr_cerr;
//	static uint8_t can_sr_serr;
//	static uint8_t can_sr_aerr;
//	static uint8_t can_sr_ferr;
//	static uint8_t can_sr_berr;
//	if (ul_status & CAN_SR_ERRP){
//		update_virtual_can_reg(&can_sr_errp, false);
//	}else{
//		update_virtual_can_reg(&can_sr_errp, true);
//	}
//	if (ul_status & CAN_SR_BOFF){
//		update_virtual_can_reg(&can_sr_boff, false);
//	}else{
//		update_virtual_can_reg(&can_sr_boff, true);
//	}
//	if (ul_status & CAN_SR_CERR){
//		update_virtual_can_reg(&can_sr_cerr, false);
//	}else{
//		update_virtual_can_reg(&can_sr_cerr, true);
//	}
//	if (ul_status & CAN_SR_SERR){
//		update_virtual_can_reg(&can_sr_serr, false);
//	}else{
//		update_virtual_can_reg(&can_sr_serr, true);
//	}
//	if (ul_status & CAN_SR_AERR){
//		update_virtual_can_reg(&can_sr_aerr, false);
//	}else{
//		update_virtual_can_reg(&can_sr_aerr, true);
//	}
//	if (ul_status & CAN_SR_FERR){
//		update_virtual_can_reg(&can_sr_ferr, false);
//	}else{
//		update_virtual_can_reg(&can_sr_ferr, true);
//	}
//	if (ul_status & CAN_SR_BERR){
//		update_virtual_can_reg(&can_sr_berr, false);
//	}else{
//		update_virtual_can_reg(&can_sr_berr, true);
//	}
//	uint8_t can_max_errs = 0;
//	{
//		uint8_t can0_max_errs;
//		uint8_t can1_max_errs;
//		{
//			uint8_t can_rec_errs = can_get_rx_error_cnt(CAN0);
//			uint8_t can_tec_errs = can_get_tx_error_cnt(CAN0);
//			can0_max_errs = max(can_rec_errs, can_tec_errs);
//		}
//
//		{
//			uint8_t can_rec_errs = can_get_rx_error_cnt(CAN1);
//			uint8_t can_tec_errs = can_get_tx_error_cnt(CAN1);
//			can1_max_errs = max(can_rec_errs, can_tec_errs);
//		}
//
//		can_max_errs = max(can0_max_errs, can1_max_errs);
//	}
//	can_max_errs = max(can_max_errs, can_sr_errp);
//	can_max_errs = max(can_max_errs, can_sr_boff);
//	can_max_errs = max(can_max_errs, can_sr_cerr);
//	can_max_errs = max(can_max_errs, can_sr_serr);
//	can_max_errs = max(can_max_errs, can_sr_aerr);
//	can_max_errs = max(can_max_errs, can_sr_ferr);
//	can_max_errs = max(can_max_errs, can_sr_berr);
//
//	if (can_max_errs < 0x7F){
//		xTimerStart(timer_fault, 5);
//	}
//}

//FIXME: Vedere se è da reinserire
//static inline void can_detect_activity(void){
//	static uint32_t old_can0_msg;
//	static uint32_t old_can1_msg;
//
//	if (can0_initialized){
//		uint32_t num_msg = canbus_get_message_num(CAN_INTERFACE_0);
//		if (num_msg != old_can0_msg){
////			LOG_DEF_NORMAL("Invio ack can0\r\n");
////			send_ack_can0();
//			old_can0_msg = num_msg;
//		}else{
////			LOG_DEF_NORMAL("Numero di messaggi uguali %04ld\r\n", num_msg);
//		}
//	}
//	if (can1_initialized){
//		uint32_t num_msg = canbus_get_message_num(CAN_INTERFACE_1);
//		if (num_msg != old_can1_msg){
////			LOG_DEF_NORMAL("Invio ack can1\r\n");
//			//FIXME: da rimettere? send_ack_can1();
//			old_can1_msg = num_msg;
//		}
//	}
//
//}



void reset_mailbox_conf_n(uint8_t i){
	if (i < MAILBOX_NUM){
		reset_mailbox_conf(can_send_mailbox[i]);
	}
}


void canbus0_init(uint32_t canspeed){
	/* Initialize CAN0 Transceiver. */
//	sn65hvd234_set_rs(&can0_transceiver, CAN0_TR_RS);
//	sn65hvd234_set_en(&can0_transceiver, CAN0_TR_EN);
	sn65hvd234_set_rs(&can0_transceiver, Can_0_RS_Ctrl_Out);

	can0_initialized = true;
	/* Enable CAN0 Transceiver. */
	canbus0_disable_tx();

	//Enable clock
	pmc_enable_periph_clk(ID_CAN0);
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	if (can_init(CAN0, ul_sysclk, canspeed)){
		LOG_DEF_NORMAL("Can0 initialization completed\r\n");
	}

	can_disable_interrupt(CAN0, CAN_DISABLE_ALL_INTERRUPT_MASK);

	can_reset_all_mailbox(CAN0);

	for (uint8_t i = 0; i<MAILBOX_NUM; i++){
		reset_mailbox_conf(&can_send_mailbox[CAN_INTERFACE_0][i]);
		can_send_mailbox[CAN_INTERFACE_0][i].ul_mb_idx = 3+i;
		can_send_mailbox[CAN_INTERFACE_0][i].uc_obj_type = CAN_MB_TX_MODE;
		can_send_mailbox[CAN_INTERFACE_0][i].uc_tx_prio = CAN_TX_PRIO+(i<<3);
		can_send_mailbox[CAN_INTERFACE_0][i].uc_id_ver = 0;
		can_send_mailbox[CAN_INTERFACE_0][i].ul_id_msk = 0;
		can_mailbox_init(CAN0, &can_send_mailbox[CAN_INTERFACE_0][i]);
	}
}

void canbus0_enable_tx(void){
	sn65hvd234_disable_low_power(&can0_transceiver);
}

void canbus0_disable_tx(void){
	sn65hvd234_enable_low_power(&can0_transceiver);
}

void canbus1_enable_tx(void){
	sn65hvd234_disable_low_power(&can1_transceiver);
}

void canbus1_disable_tx(void){
	sn65hvd234_enable_low_power(&can1_transceiver);
}

void canbus1_init(uint32_t canspeed){
	/* Initialize CAN1 Transceiver. */
//	sn65hvd234_set_rs(&can1_transceiver, CAN1_TR_RS);
//	sn65hvd234_set_en(&can1_transceiver, CAN1_TR_EN);
	sn65hvd234_set_rs(&can1_transceiver, Can_1_RS_Ctrl_Out);
	can1_initialized = true;
	/* Enable CAN1 Transceiver. */
	canbus1_disable_tx();

	//Enable clock
	pmc_enable_periph_clk(ID_CAN1);
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	if (can_init(CAN1, ul_sysclk, canspeed)){
		LOG_DEF_NORMAL("Can1 initialization completed\r\n");
	}

	can_disable_interrupt(CAN1, CAN_DISABLE_ALL_INTERRUPT_MASK);

	can_reset_all_mailbox(CAN1);

	for (uint8_t i = 0; i<MAILBOX_NUM; i++){
		reset_mailbox_conf(&can_send_mailbox[CAN_INTERFACE_1][i]);
		can_send_mailbox[CAN_INTERFACE_1][i].ul_mb_idx = 3+i;
		can_send_mailbox[CAN_INTERFACE_1][i].uc_obj_type = CAN_MB_TX_MODE;
		can_send_mailbox[CAN_INTERFACE_1][i].uc_tx_prio = CAN_TX_PRIO+(i<<3);
		can_send_mailbox[CAN_INTERFACE_1][i].uc_id_ver = 0;
		can_send_mailbox[CAN_INTERFACE_1][i].ul_id_msk = 0;
		can_mailbox_init(CAN1, &can_send_mailbox[CAN_INTERFACE_1][i]);
	}
}

void canbus0_enable_interrupt(void){
	reset_mailbox_conf(&can0_mailbox);
	can0_mailbox.ul_mb_idx = 0;
	can0_mailbox.uc_obj_type = CAN_MB_RX_MODE;
	can0_mailbox.ul_id_msk = 0;
	can0_mailbox.ul_id = CAN_MID_MIDvA(0x7FFu);
	can_mailbox_init(CAN0, &can0_mailbox);
	can_enable_interrupt(CAN0, CAN_IER_MB0);

	NVIC_EnableIRQ(CAN0_IRQn);
	NVIC_SetPriority(CAN0_IRQn, 15);
}

void canbus1_enable_interrupt(void){
	reset_mailbox_conf(&can1_mailbox);
	can1_mailbox.ul_mb_idx = 0;
	can1_mailbox.uc_obj_type = CAN_MB_RX_MODE;
	can1_mailbox.ul_id_msk = 0;
	can1_mailbox.ul_id = CAN_MID_MIDvA(0x7FFu);
	can_mailbox_init(CAN1, &can1_mailbox);
	can_enable_interrupt(CAN1, CAN_IER_MB0);
	NVIC_EnableIRQ(CAN1_IRQn);
	NVIC_SetPriority(CAN1_IRQn, 15);
}

void canbus_hw_init(void){
	timer_fault = xTimerCreate("t_keys" , 10000/portTICK_PERIOD_MS, 	pdTRUE, 0, vTimerFault);
}

/**
 * @}
 */
