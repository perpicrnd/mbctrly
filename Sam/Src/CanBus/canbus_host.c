#ifdef __amd64__

#include <canbus.h>
#include <FreeRTOS.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <libgen.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <linux/can.h>
#include <net/if.h>
#include <linux/can/raw.h>
#include <queue.h>

#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <task.h>

#include "event_list.h"
#include "logger.h"

/**
 * @brief Numero di interfacce abilitate.
 */
#define CAN_IF_NUM 2

/**
 * @brief Valore del socket non valido.
 */
#define INVALID_SOCKET -1

///Definito in can_task.c
extern uint32_t can_accepted_id[CAN_INTERFACE_COUNT][ACCEPTED_ID_NUM];

/**
 * Check if the id passed as parameter is accepted on the selected interface.
 * @param interface the interface to query
 * @param num the id number to check
 * @return true if the id is accepted, false otherwise.
 */
static inline bool can_is_accepted_id(CAN_INTERFACE interface, uint32_t num);
static inline bool can_is_accepted_id(CAN_INTERFACE interface, uint32_t num){
	bool found = false;
	for (uint8_t i = 0; i<ACCEPTED_ID_NUM && found == false; i++){
		if (can_accepted_id[interface][i] == num){
			found = true;
		}
	}
	return found;
}

/**
 * @brief Array contenente lo stato Attivo/Non attivo delle interfacce can.
 */
static bool can_enabled[CAN_IF_NUM] = {false, false};
/**
 * @brief Array contenente i socket utilizzati per i vari canbus
 */
int m_socket[CAN_IF_NUM];
/**
 * @brief Array contenente i can frame letti.
 */
struct can_frame frame[CAN_IF_NUM];
/**
 * @brief Attiva l'interfaccia.
 * @param interfaceName l'interfaccia da abilitare.
 * @param can_num il numero dell'interfaccia per posizionarla nell'array.
 * @param errorCode Variabile in cui viene salvato l'eventuale codice di errore generato.
 * @return true se l'interfaccia viene aperta, false in caso contrario.
 */
static bool open_can_port(const char *interfaceName, int can_num, int *errorCode)
{
	struct sockaddr_can addr;
	struct ifreq ifr;

	int ret;

	*errorCode = 0;

	m_socket[can_num] = socket(PF_CAN, SOCK_RAW, CAN_RAW);

	// Get index for a certain name
	strcpy(ifr.ifr_name, interfaceName);
	ret = ioctl(m_socket[can_num], SIOCGIFINDEX, &ifr);
	if(ret)
	{
		*errorCode = errno;

		return false;
	}

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	ret = bind(m_socket[can_num], (struct sockaddr *)&addr, sizeof(addr));
	if(ret)
	{
		*errorCode = errno;

		close(m_socket[can_num]);
		m_socket[can_num] = INVALID_SOCKET;

		return false;
	}


	return true;
}

/**
 * @brief legge un messaggio dalla porta CAN specificata.
 * @param can_num il numero della porta can da cui leggere.
 * @param extended true se il messaggio letto è esteso, false in caso contrario.
 * @param rtr
 * @param error
 * @param errorCode
 * @param timeout
 * @return true se viene letto qualcosa, false altrimenti.
 */
static bool get_msg(CAN_INTERFACE can_num, bool *extended, bool *rtr, long int *error, int *errorCode, struct timeval *timeout)
{
	int bytesRead;
	int ret;
	fd_set rfds;
	*errorCode = 0;

	// Set up a file descriptor set only containing one socket
	FD_ZERO(&rfds);
	FD_SET(m_socket[can_num], &rfds);

	// Use select to be able to use a timeout
	ret = select(m_socket[can_num]+1, &rfds, NULL, NULL, timeout);
	if (!can_enabled[can_num]){
		return false;
	}
	if(ret < 0)
	{
		vTaskDelay(10/portTICK_PERIOD_MS);
		*errorCode = errno;

		return false;
	}

	if(ret > 0)
	{
		bytesRead = read(m_socket[can_num], &frame[can_num], sizeof(struct can_frame));

		if(bytesRead < 0)
		{
			*errorCode = errno;

			return false;
		}
		if(bytesRead == sizeof(struct can_frame))
		{
			*error = frame[can_num].can_id & CAN_ERR_FLAG;
			*extended = frame[can_num].can_id & CAN_EFF_FLAG;
			*rtr = frame[can_num].can_id & CAN_RTR_FLAG;

			if(error)
			{
				frame[can_num].can_id  &= CAN_ERR_MASK;
			}

			if(extended)
			{
				frame[can_num].can_id  &= CAN_EFF_MASK;
			}
			else
			{
				frame[can_num].can_id &= CAN_SFF_MASK;
			}
			if (can_is_accepted_id(can_num, frame[can_num].can_id)){
				return true;
			}
		}
	}

	return false;
}

/**
 * @brief Task di gestione del can0
 * @param parameter non usato.
 */
static void can0_task(void *parameter){
	(void)parameter;
	CAN_INTERFACE can_num = CAN_INTERFACE_0;
	bool extended;
	bool rtr;
	long int error;
	int errorCode;
	for(;;){
		struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		if(get_msg(can_num, &extended, &rtr, &error, &errorCode, &tv)){
			///se viene letto un messaggio converto da struct can_frame a \ref CAN_MESSAGE
			CAN_MESSAGE msg;
			msg.interface = CAN_INTERFACE_0;
			msg.datalen = frame[can_num].can_dlc;
			msg.can_id = frame[can_num].can_id;
			if (msg.can_id <= 0x7FF){
				msg.can_is_ext = false;
			}else{
				msg.can_is_ext = true;
			}
			msg.can_data.byte0 = frame[can_num].data[0];
			msg.can_data.byte1 = frame[can_num].data[1];
			msg.can_data.byte2 = frame[can_num].data[2];
			msg.can_data.byte3 = frame[can_num].data[3];
			msg.can_data.byte4 = frame[can_num].data[4];
			msg.can_data.byte5 = frame[can_num].data[5];
			msg.can_data.byte6 = frame[can_num].data[6];
			msg.can_data.byte7 = frame[can_num].data[7];
			///Quindi viene generato un evento sulla ricezione.
			event_can0_rx_message_emit(&msg);
		}
	}
}


/**
 * @brief Task di gestione del can1
 * @param parameter non usato.
 */
static void can1_task(void *parameter){
	(void)parameter;
	CAN_INTERFACE can_num = CAN_INTERFACE_1;
	bool extended;
	bool rtr;
	long int error;
	int errorCode;
	for(;;){
		struct timeval tv;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		if(get_msg(can_num, &extended, &rtr, &error, &errorCode, &tv)){
			///se viene letto un messaggio converto da struct can_frame a \ref CAN_MESSAGE
			CAN_MESSAGE msg;
			msg.interface = CAN_INTERFACE_1;
			msg.datalen = frame[can_num].can_dlc;
			msg.can_id = frame[can_num].can_id;
			if (msg.can_id <= 0x7FF){
				msg.can_is_ext = false;
			}else{
				msg.can_is_ext = true;
			}
			msg.can_data.byte0 = frame[can_num].data[0];
			msg.can_data.byte1 = frame[can_num].data[1];
			msg.can_data.byte2 = frame[can_num].data[2];
			msg.can_data.byte3 = frame[can_num].data[3];
			msg.can_data.byte4 = frame[can_num].data[4];
			msg.can_data.byte5 = frame[can_num].data[5];
			msg.can_data.byte6 = frame[can_num].data[6];
			msg.can_data.byte7 = frame[can_num].data[7];
			///Quindi viene generato un evento sulla ricezione.
			event_can1_rx_message_emit(&msg);
		}
	}
}

void canbus_send_message(const CAN_MESSAGE * const msg){
	int res;
	struct can_frame fr = {0};
	CAN_INTERFACE can_num = msg->interface;
	if (!can_enabled[can_num]){
		return;
	}
	if(msg->can_is_ext)
	{
		fr.can_id |= CAN_EFF_FLAG;
	}
	fr.can_dlc = msg->datalen;
	fr.can_id = msg->can_id;
	fr.data[0] = msg->can_data.byte0;
	fr.data[1] = msg->can_data.byte1;
	fr.data[2] = msg->can_data.byte2;
	fr.data[3] = msg->can_data.byte3;
	fr.data[4] = msg->can_data.byte4;
	fr.data[5] = msg->can_data.byte5;
	fr.data[6] = msg->can_data.byte6;
	fr.data[7] = msg->can_data.byte7;
	res = write(m_socket[can_num], &fr, sizeof(struct can_frame));

	if(res < 0)
	{
		perror("could not send");
		LOG_CAN_ERROR("errno is %d\r\n", errno);
	}
}

/**
 * @brief Abilita la ricezione dei messaggi sul can0.
 */
void canbus0_enable_interrupt(void){
	can_enabled[0] = true;
}

/**
 * @brief Abilita la ricezione dei messaggi sul can1.
 */
void canbus1_enable_interrupt(void){
	can_enabled[1] = true;
}

/**
 * @brief inizializza il sottosistema hardware per la ricezione dei messaggi canbus.
 */
bool initialized = false;
void canbus_hw_init(void){
	if (!initialized){
		initialized = true;
		int err = 0;
		open_can_port("vcan0", 0, &err);
		LOG_CAN_ERROR("Err code: %d\r\n", err);
		xTaskCreate( can0_task,  "can0_task_host", configMINIMAL_STACK_SIZE+200, 0, 1, NULL);
		err = 0;
		open_can_port("vcan1", 1, &err);
		LOG_CAN_ERROR("Err code: %d\r\n", err);
		xTaskCreate( can1_task,  "can1_task_host", configMINIMAL_STACK_SIZE+200, 0, 1, NULL);
	}
}

void canbus0_init(uint32_t canspeed){
	(void)canspeed;
	LOG_DEF_NORMAL("Canbus speed to be set from Command line.\r\n");
}

void canbus1_init(uint32_t canspeed){
	(void)canspeed;
	LOG_DEF_NORMAL("Canbus speed to be set from Command line.\r\n");
}


void canbus0_enable_tx(void){
}

void canbus0_disable_tx(void){
}

void canbus1_enable_tx(void){
}

void canbus1_disable_tx(void){
}

#endif


