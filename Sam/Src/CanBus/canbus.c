#include <canbus.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>
#include <stdio.h>
#include <string.h>

#include "canbus.h"
#include "canbus_priv.h"
#include "wisker.h"
#include "event_list.h"
#include "logger.h"

/**
 * @addtogroup CANBUS
 * @{
 */

/**
 * @brief Coda contenente i messaggi in trasmissione
 */
static QueueHandle_t task_queue;

/**
 * Array contenente gli ID accettati in ricezione.
 */
uint32_t can_accepted_id[CAN_INTERFACE_COUNT][ACCEPTED_ID_NUM];

/**
 * This function also checks if an ID is already present in the list.
 * In this situation the function do nothing.
 */
bool canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num){
	bool found = false;
	for (uint8_t i = 0; i<ACCEPTED_ID_NUM && found==false; i++){
		if (can_accepted_id[interface][i] == num){
			found = true;
		}
		if (can_accepted_id[interface][i] == 0){
			found = true;
			can_accepted_id[interface][i] = num;
		}
	}
	return found;
}

static inline void canbus_task_tx_forloop(void){
	CAN_MESSAGE msg;
	if (xQueueReceive(task_queue, &msg, portMAX_DELAY)){
		if (msg.interface < CAN_INTERFACE_COUNT){
			canbus_send_message(&msg);
			vTaskDelay(20/portTICK_PERIOD_MS);
		}
	}
}

static void canbus_task_tx(void *parameter){
	(void)parameter;
	vTaskDelay(20/portTICK_PERIOD_MS);
	for(;;){
		canbus_task_tx_forloop();
#ifdef __amd64__

#else
        if (uxTaskGetStackHighWaterMark(NULL)<100){
            LOG_DEF_NORMAL("%s stack too low.\r\n", __PRETTY_FUNCTION__);
        }
#endif
	}

}

static void handle_can_tx(const union EVENT * const msg){
	//I don't want to wait in the event_loop.
	if(!xQueueSendToBack(task_queue, &msg->as_can_message.msg, 0)){
		//Qui si potrebbe arrivare nel caso di ripetuti errori di trasmissione.
		//Non ci posso fare nulla dal software.
		//Ignoro il messaggio.
	}
}


static bool initialized = false;
void canbus_init(struct CANBUS_CONFIG *can_conf){
	if (!initialized){
		xTaskCreate( canbus_task_tx,  (char *)"can_task", configMINIMAL_STACK_SIZE+100, NULL, configMAX_PRIORITIES - 2, NULL);
		task_queue = xQueueCreate(16, sizeof(CAN_MESSAGE));
		LOG_DEF_NORMAL("Can task init\r\n");
		canbus_hw_init();
		canbus0_init(can_conf->can0.can_speed);
		canbus1_init(can_conf->can1.can_speed);
		/* Init CAN0 Mailbox 0 to Reception Mailbox. */
		canbus0_enable_interrupt();
		/* Init CAN1 Mailbox 0 to Reception Mailbox. */
		canbus1_enable_interrupt();
		initialized = true;
		event_connect_callback(eEVENT_CAN0_TX_MESSAGE, handle_can_tx);
		event_connect_callback(eEVENT_CAN1_TX_MESSAGE, handle_can_tx);
	}
}


void event_can0_rx_message_emit(const CAN_MESSAGE * const msg){
	union EVENT event;
	event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
	memcpy(&event.as_can_message.msg, msg, sizeof(CAN_MESSAGE));
	event_emit(&event);
}

void event_can1_rx_message_emit(const CAN_MESSAGE * const msg){
	union EVENT event;
	event.as_generic.event = eEVENT_CAN1_RX_MESSAGE;
	memcpy(&event.as_can_message.msg, msg, sizeof(CAN_MESSAGE));
	event_emit(&event);
}


void event_can0_tx_message_emit(const CAN_MESSAGE * const msg){
	union EVENT event;
	event.as_generic.event = eEVENT_CAN0_TX_MESSAGE;
	memcpy(&event.as_can_message.msg, msg, sizeof(CAN_MESSAGE));
	event_emit(&event);
}

void event_can1_tx_message_emit(const CAN_MESSAGE * const msg){
	union EVENT event;
	event.as_generic.event = eEVENT_CAN1_TX_MESSAGE;
	memcpy(&event.as_can_message.msg, msg, sizeof(CAN_MESSAGE));
	event_emit(&event);
}


void event_canbus_ignition_emit(enum IGNITION_STATUS status){
	union EVENT event;
	event.as_generic.event = eEVENT_CANBUS_IGNITION_CHANGED;
	event.as_canbus_ignition.ignition = status;
	event_emit(&event);
}


void event_canbus_reverse_emit(enum REVERSE_STATUS status){
	union EVENT event;
	event.as_generic.event = eEVENT_CANBUS_REVERSE_CHANGED;
	event.as_canbus_reverse.reverse = status;
	event_emit(&event);
}

/**
 * @}
 */
