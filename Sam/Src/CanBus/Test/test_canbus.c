#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "canbus.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

BaseType_t __wrap_xTaskCreate(TaskFunction_t pxTaskCode,
                              const char *const pcName,
                              const uint16_t usStackDepth,
                              void *const pvParameters,
                              UBaseType_t uxPriority,
                              TaskHandle_t *const pxCreatedTask) {
    (void)pxTaskCode;
    (void)pcName;
    (void)usStackDepth;
    (void)pvParameters;
    (void)uxPriority;
    (void)pxCreatedTask;
    function_called();
    return pdTRUE;
}

void __wrap_vTaskDelay(const TickType_t xTicksToDelay) {
    (void)xTicksToDelay;
    function_called();
}

void __wrap_canbus0_init(uint32_t canspeed) {
    (void)canspeed;
    function_called();
}

void __wrap_canbus1_init(uint32_t canspeed) {
    (void)canspeed;
    function_called();
}

void __wrap_canbus0_enable_interrupt(void) {
    function_called();
}

void __wrap_canbus1_enable_interrupt(void) {
    function_called();
}

QueueHandle_t __wrap_xQueueGenericCreate(const UBaseType_t uxQueueLength, const UBaseType_t uxItemSize, const uint8_t ucQueueType) {
    (void)uxQueueLength;
    (void)uxItemSize;
    (void)ucQueueType;

    function_called();
    return NULL;
}

BaseType_t __wrap_xQueueReceive(QueueHandle_t xQueue, void *const pvBuffer, TickType_t xTicksToWait) {
    (void)xQueue;
    CAN_MESSAGE *msg = (CAN_MESSAGE *)mock();
    canbus_print_frame(msg);
    memcpy(pvBuffer, msg, sizeof(CAN_MESSAGE));
    (void)xTicksToWait;
    function_called();
    return (BaseType_t)mock();
}

void __wrap_canbus_send_message(const CAN_MESSAGE *const msg) {
    (void)msg;
    function_called();
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    (void)event;
    (void)handler;
    function_called();
}

void test_canbus_init() {
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 50;
    can_conf.can0.listen_mode = false;
    expect_function_call_any(__wrap_xTaskCreate);
    expect_function_call(__wrap_xQueueGenericCreate);
    expect_function_call(__wrap_canbus0_init);
    expect_function_call(__wrap_canbus1_init);
    expect_function_call(__wrap_canbus0_enable_interrupt);
    expect_function_call(__wrap_canbus1_enable_interrupt);
    expect_function_call(__wrap_event_connect_callback);
    expect_function_call(__wrap_event_connect_callback);
    canbus_init(&can_conf);
}

void test_add_accepted_id() {
    assert_true(canbus_add_accepted_id(CAN_INTERFACE_0, 0x01));
    assert_true(can_accepted_id[CAN_INTERFACE_0][0] == 0x01);
    //No duplicati
    assert_true(canbus_add_accepted_id(CAN_INTERFACE_0, 0x01));
    assert_false(can_accepted_id[CAN_INTERFACE_0][1] == 0x01);

    for (uint8_t i = 0; i < CAN_INTERFACE_COUNT; i++) {
        for (uint8_t j = 0; j < ACCEPTED_ID_NUM; j++) {
            assert_true(canbus_add_accepted_id(i, j));
            can_accepted_id[i][j] = j;
        }
    }
    //Slot finiti, non posso aggiungere altro.
    assert_false(canbus_add_accepted_id(CAN_INTERFACE_0, 0x150));
}

void test_canbus_mask() {
    CAN_MESSAGE msg = {0x6354000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    CAN_MESSAGE_CMP msg_cmp = {{0x6354000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0}, 0x80};

    assert_false(can_message_mask(&msg, &msg_cmp));
    msg_cmp.m.can_data.byte0 = 0x80;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte1 = 0x80;
    msg.can_data.byte1 = 0x80;
    msg_cmp.mask = 0xC0;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte2 = 0x80;
    msg.can_data.byte2 = 0x80;
    msg_cmp.mask = 0xE0;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte3 = 0x80;
    msg.can_data.byte3 = 0x80;
    msg_cmp.mask = 0xF0;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte4 = 0x80;
    msg.can_data.byte4 = 0x80;
    msg_cmp.mask = 0xF8;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte5 = 0x80;
    msg.can_data.byte5 = 0x80;
    msg_cmp.mask = 0xFC;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte6 = 0x80;
    msg.can_data.byte6 = 0x80;
    msg_cmp.mask = 0xFE;
    assert_true(can_message_mask(&msg, &msg_cmp));

    msg_cmp.m.can_data.byte7 = 0x80;
    msg.can_data.byte7 = 0x80;
    msg_cmp.mask = 0xFF;
    assert_true(can_message_mask(&msg, &msg_cmp));
}

void test_canbus_task_tx() {
    expect_function_call(__wrap_xQueueReceive);
    CAN_MESSAGE msg = {0x6354000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    will_return(__wrap_xQueueReceive, &msg);
    will_return(__wrap_xQueueReceive, pdTRUE);
    expect_function_call(__wrap_canbus_send_message);
    expect_function_call(__wrap_vTaskDelay);
    canbus_task_tx_forloop();

    msg.interface = CAN_INTERFACE_1;
    expect_function_call(__wrap_xQueueReceive);
    will_return(__wrap_xQueueReceive, &msg);
    will_return(__wrap_xQueueReceive, pdTRUE);
    expect_function_call(__wrap_canbus_send_message);
    expect_function_call(__wrap_vTaskDelay);
    canbus_task_tx_forloop();

    msg.interface = CAN_INTERFACE_UNKNOWN;
    expect_function_call(__wrap_xQueueReceive);
    will_return(__wrap_xQueueReceive, &msg);
    will_return(__wrap_xQueueReceive, pdTRUE);
    canbus_task_tx_forloop();
}

int setup(void **state) {
    (void)state;
    for (uint8_t i = 0; i < CAN_INTERFACE_COUNT; i++) {
        for (uint8_t j = 0; j < ACCEPTED_ID_NUM; j++) {
            can_accepted_id[i][j] = 0;
        }
    }
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_canbus_init, setup),
        cmocka_unit_test_setup(test_add_accepted_id, setup),
        cmocka_unit_test_setup(test_canbus_task_tx, setup),
        cmocka_unit_test_setup(test_canbus_mask, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
