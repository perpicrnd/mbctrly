#pragma once

/**
 * @addtogroup CANBUS
 */

#include <stdbool.h>
#include <stdint.h>
#include "event_list.h"


/**
 * @brief Struttura utilizzata per memorizzare i messaggi da confrontare con quelli in arrivo sul canbus.
 * La struttura permette un veloce confronto di un messaggio ricevuto con uno salvato. La verifica è fatta
 * lasciando la possibilità all'utilizzatore di segnare alcuni byte da verificare o da ignorare.
 */
typedef struct{
	///Messaggio di controllo
	CAN_MESSAGE m;
	///Maschera con i byte da controllare
	uint8_t mask;
}CAN_MESSAGE_CMP;

/**
 * @brief Funzione di controllo di due messaggi.
 * @param received messaggio ricevuto dal canbus.
 * @param stored messaggio salvato utilizzato per il confronto
 * @return true in caso di match, false altrimenti.
 */
bool can_message_mask(const CAN_MESSAGE * const received, const CAN_MESSAGE_CMP * const stored);

/**
 * @brief Funzione che stampa il messaggio canbus.
 * @param message il messaggio da stampare.
 */
void canbus_print_frame(const CAN_MESSAGE * const message);

/**
 * @}
 */
