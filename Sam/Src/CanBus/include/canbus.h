#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "can_message.h"
#include "event_list.h"

/**
 * @defgroup CANBUS Can Bus module
 * @brief Modulo di comunicazione tramite canbus
 * Questo modulo effettua tutta la gestione tramite il canbus.
 * Il modulo è asincrono, riveve ed invia i messaggi utilizzando l'event loop.
 * @{
 */


/**
 * @brief struttura contenente le informazioni per l'inizializzazione del modulo canbus.
 */
struct CAN_CONF{
	/**
	 * Velocità del canbus
	 */
	uint32_t can_speed;
	/**
	 * Se l'interfaccia deve rimanere in listen mode o può inviare ack sul bus.
	 */
	bool listen_mode;
};
/**
 * @brief Struttura contenente le informazioni per l'inizializzazione del modulo canbus.
 */
struct CANBUS_CONFIG{
	struct CAN_CONF can0;
	struct CAN_CONF can1;
};

/**
 * @brief Numero massimo di ID accettati dal sistema di ricezione.
 */
#define ACCEPTED_ID_NUM 30



/**
 * @brief Inizializzazione del can.
 * Inizializza il modulo canbus e le relative sottocomponenti.
 */
void canbus_init(struct CANBUS_CONFIG *can_conf);


/**
 * @brief Add an ID to the list of the ID we want to receive.
 * @param interface the interface on which accept the message
 * @param num the id number to accept.
 * @return true if the value is added to the list, false otherwise.
 */
bool canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num);

void canbus0_enable_tx(void);
void canbus0_disable_tx(void);
void canbus1_enable_tx(void);
void canbus1_disable_tx(void);
/**
 * @}
 */
