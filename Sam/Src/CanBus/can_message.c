#include "can_message.h"

#include <stdio.h>
#include "logger.h"

/**
 * @addtogroup CANBUS
 * @{
 */

/**
 * Se il messaggio received è in match con stored return true
 */
bool can_message_mask(const CAN_MESSAGE  * const received, const CAN_MESSAGE_CMP * const stored){
	bool retVal = false;
	uint8_t	in_msg[8];		//messaggio ricevuto
	uint8_t	stor_msg[8];	//messaggio in flash x comparazione

	if((received->datalen == stored->m.datalen) && (received->interface == stored->m.interface) && (received->can_id == stored->m.can_id)){
		/// salvo la struct con i dati ricevuti in un array.
		in_msg[0] = received->can_data.byte0;
		in_msg[1] = received->can_data.byte1;
		in_msg[2] = received->can_data.byte2;
		in_msg[3] = received->can_data.byte3;
		in_msg[4] = received->can_data.byte4;
		in_msg[5] = received->can_data.byte5;
		in_msg[6] = received->can_data.byte6;
		in_msg[7] = received->can_data.byte7;
		/// Salvo la struct con i dati salvati in un array.
		stor_msg[0] = stored->m.can_data.byte0;
		stor_msg[1] = stored->m.can_data.byte1;
		stor_msg[2] = stored->m.can_data.byte2;
		stor_msg[3] = stored->m.can_data.byte3;
		stor_msg[4] = stored->m.can_data.byte4;
		stor_msg[5] = stored->m.can_data.byte5;
		stor_msg[6] = stored->m.can_data.byte6;
		stor_msg[7] = stored->m.can_data.byte7;
		///effettuo la comparazione
		retVal = true;
		uint8_t bit = 7;
		for(uint8_t i=0; i< received->datalen; i++){
			if ((stored->mask & 1<<bit) != 0){
				if (in_msg[i] != stor_msg[i]){
					retVal = false;
					break;
				}
			}
			bit--;
		}
	}
	return retVal;
}


void canbus_print_frame(const CAN_MESSAGE * const message){
	if (message->interface == CAN_INTERFACE_0){
		LOG_DEF_NORMAL("CAN0: ");
	}else if (message->interface == CAN_INTERFACE_1){
		LOG_DEF_NORMAL("CAN1: ");
	}else{
		LOG_DEF_NORMAL( "CANE: ");
	}
	LOG_DEF_NORMAL( "%s ", message->can_is_ext ? "EXT" : "STD");
#ifdef __amd64__
	LOG_DEF_NORMAL("<%06X> ", message->can_id);
#else
	LOG_DEF_NORMAL("<%06lX> ", message->can_id);
#endif
	if (message->datalen > 0){
		LOG_DEF_NORMAL( "%02X ", message->can_data.byte0);
		if (message->datalen > 1){
			LOG_DEF_NORMAL( "%02X ", message->can_data.byte1);
			if (message->datalen > 2){
				LOG_DEF_NORMAL( "%02X ", message->can_data.byte2);
				if (message->datalen > 3){
					LOG_DEF_NORMAL( "%02X ", message->can_data.byte3);
					if (message->datalen > 4){
						LOG_DEF_NORMAL( "%02X ", message->can_data.byte4);
						if (message->datalen > 5){
							LOG_DEF_NORMAL( "%02X ", message->can_data.byte5);
							if (message->datalen > 6){
								LOG_DEF_NORMAL( "%02X ", message->can_data.byte6);
								if (message->datalen > 7){
									LOG_DEF_NORMAL( "%02X ", message->can_data.byte7);
								}
							}
						}
					}
				}
			}
		}
	}
	LOG_DEF_NORMAL("\r\n");
}

/**
 * @}
 */
