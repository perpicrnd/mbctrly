#pragma once

/**
 * @addtogroup CANBUS
 * @{
 */

/**
 * @brief Abilita il sottosistema hardware dell'interfaccia.
 */
void canbus_hw_init(void);


/**
 * @brief Verifica il numero di messaggi che sono stati ricevuti sul canbus.
 * @param interface l'interfaccia di cui si vuole sapere il numero di messaggi ricevuti.
 * @return
 */
uint32_t canbus_get_message_num(CAN_INTERFACE interface);

/**
 * @brief Abilita gli interrupt del can0.
 */
void canbus0_enable_interrupt(void);

/**
 * @brief Abilita gli interrupt del can1
 */
void canbus1_enable_interrupt(void);

/**
 * @brief Inizializza il can0
 * @param canspeed la velocità del canbus.
 */
void canbus0_init(uint32_t canspeed);

/**
 * @brief Inizializza il can1
 * @param canspeed la velocità del canbus.
 */
void canbus1_init(uint32_t canspeed);

/**
 * @brief Invia un messaggio utilizzando il canbus.
 * @param msg il messaggio da inviare.
 */
void canbus_send_message(const CAN_MESSAGE * const msg);
/**
 * @}
 */
