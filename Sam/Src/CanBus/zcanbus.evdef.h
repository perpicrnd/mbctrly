#pragma once
#include <stdint.h>
#include "events.h"
/**
 * @brief Elenco delle interfacce canbus presenti sull'interfaccia.
 */
typedef enum{
	CAN_INTERFACE_UNKNOWN = (uint8_t)-1,//!< CAN_INTERFACE_UNKNOWN interfaccia sconosciuta
	CAN_INTERFACE_0 = 0,       //!< CAN_INTERFACE_0 interfaccia CAN0
	CAN_INTERFACE_1,           //!< CAN_INTERFACE_1 interfaccia CAN1
	CAN_INTERFACE_COUNT        //!< CAN_INTERFACE_COUNT Numero delle interfacce
}CAN_INTERFACE;


/**
 * @brief struttura contenente i dati del messaggio canbus
 * I dati sono salvati in una struttura e si può accedere ai singoli byte.
 */
typedef struct{
	uint32_t byte0: 8;
	uint32_t byte1: 8;
	uint32_t byte2: 8;
	uint32_t byte3: 8;
	uint32_t byte4: 8;
	uint32_t byte5: 8;
	uint32_t byte6: 8;
	uint32_t byte7: 8;
}CAN_DATA;

/**
 * @brief Struttura contenente le informazioni relative ad un messaggio canbus
 * Un messaggio canbus contiene tutte le informazioni necessarie per la sua gestione sia in
 * ricezione che tramissione.
 */
typedef struct{
	/**
	 * Id del messaggio can.
	 */
	uint32_t can_id;
	/**
	 * Flag che indica se il messaggio è standard o esteso.
	 */
	bool can_is_ext;
	/**
	 * Dati contenuti nel messaggio can.
	 */
	CAN_DATA can_data;
	/**
	 * Numero di byte contenuti in \ref CAN_DATA.
	 */
	uint8_t datalen;
	/**
	 * Interfaccia da cui è stato ricevuto il messaggio o su cui verrà trasmesso.
	 */
	CAN_INTERFACE interface;
}CAN_MESSAGE;

/**
 * @brief evento di tipo messaggio canbus ricevuto dal veicolo
 */
struct EVENT_CAN_MESSAGE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Messaggio canbus ricevuto.
	 */
	CAN_MESSAGE msg;
};

struct EVENT_CANBUS_IGNITION{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato del sottochiave.
	 */
	enum IGNITION_STATUS ignition;
};

struct EVENT_CANBUS_REVERSE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Stato del sottochiave.
	 */
	enum REVERSE_STATUS reverse;
};


/**
 * @brief emette un messaggio contenente un messaggio can ricevuto sul can0
 * @param msg il messaggio canbus ricevuto.
 */
void event_can0_rx_message_emit(const CAN_MESSAGE * const msg);

/**
 * @brief emette un messaggio contenente un messaggio can ricevuto sul can1
 * @param msg il messaggio canbus ricevuto.
 */
void event_can1_rx_message_emit(const CAN_MESSAGE * const msg);

/**
 * @brief emette un messaggio con la richiesta di trasmettere un messaggio sul can0
 * @param msg il messaggio canbus da trasmettere.
 */
void event_can0_tx_message_emit(const CAN_MESSAGE * const msg);

/**
 * @brief emette un messaggio con la richiesta di trasmettere un messaggio sul can1
 * @param msg il messaggio canbus da trasmettere.
 */
void event_can1_tx_message_emit(const CAN_MESSAGE * const msg);

enum IGNITION_STATUS;
/**
 * @brief emette un messaggio con l'ignition processata dal veicolo.
 * 
 * @param IGNITION l'ignition processata.
 */
void event_canbus_ignition_emit(enum IGNITION_STATUS status);
/**
 * @brief emette un messaggio con il reverse come processato dal veicolo.
 * 
 * @param REVERSE_STATUS il reverse processato dal veicolo.
 */
enum REVERSE_STATUS;;
void event_canbus_reverse_emit(enum REVERSE_STATUS status);
