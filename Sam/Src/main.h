#pragma once

#include <stdint.h>
/**
 * @addtogroup Main
 * @{
 */

/**
 * @brief Inizializza la parte hardware della scheda.
 */
void init_board(void);

/**
 * @brief Software reset function.
 */
void software_reset(void);


/**
 * @brief Restituisce una parte dell'unique id del processore da utilizzare come can id che si spera sia univoco.
 * Logicamente possono esserci conflitti in quanto lo unique_id è su 128 bit ma si spera che le collisioni siano poche.
 * 
 * @return uint32_t 
 */
uint32_t maybe_unique_can_id(void);

void selector_initialize_vehicle(void);
/**
 * @}
 */

