#include <canbus.h>
#include <stdio.h>
#include <unistd.h>
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#include "event_list.h"
#include "wisker.h"
#include "service.h"
#include "logger.h"
#include "version.h"
#include "watchdog.h"
#include "board_version.h"
#include "controller.h"
#include "serials.h"
#include "settings.h"
#include "buttons.h"
#include "selector.h"

/**
 * @defgroup Main Avvio del sistema
 * @brief Inizializzazione del sistema
 * Inizializza il sistema e configura il tipo di veicolo utilizzato.
 * @{
 */


/**
 * @brief array per il buffering della stringa
 */
// static char szList[512];
/**
 * @brief Funzione eseguita ogni minuto
 * @param event l'evento che è stato generato e che deve essere gestito.
 */
static void minutely_operation(const union EVENT * const event){
	(void)event;
	LOG_DEF_NORMAL("CAmbio minuto\r\n");
#if 0
    portENTER_CRITICAL();
	SWO_PrintStringf("\r\nTask\t\tStatus\tprio\thighw\tnum\r\n");
	vTaskList(szList);
    SWO_PrintStringf("length: %d\r\n", strlen(szList));
    SWO_PrintString(szList);
#ifdef BOARD
	SWO_PrintStringf("\r\nHeap: %d\r\n", xPortGetFreeHeapSize());
#endif
    portEXIT_CRITICAL();
#endif
}

/**
 * @brief variabile contenente il numero di volte che il sistema è passato dall'idle callback
 * Il sistema arriva nell'idle callback se non ha nulla da fare. A questo punto viene messo in sleep.
 * Se il sistema si sveglia è perchè è stato ricevuto un interrupt. (Es. Systick - canbus - uart) etc.
 * Se il sistema è fermo e non ci sono eventi il numero di passaggi nell'idle sarà ~1000
 * con UART, CAN attivi il numero di passaggi dall'idle aumenterà.
 * Può fornire un idea di quale sia l'occupazione del processore.
 */
static uint32_t idle_counter = 0;

extern struct SERIAL *logger;
extern struct SERIAL uart;
/**
 * @brief Funzione eseguita ogni secondo
 * @param event l'evento che è stato generato e che deve essere gestito.
 */
static void secondly_operation(const union EVENT * const event){
	logger = &uart;
#ifdef __amd64__
	LOG_DEF_NORMAL("Now it's %ld - idle %d\r\n", event->as_timestamp.timestamp, idle_counter);
#else
	// LOG_DEF_INFO("Now it's %lld - idle %ld\r\n", event->as_timestamp.timestamp, idle_counter);
#endif
	idle_counter = 0;
}


/**
 * @brief Funzione eseguita ogni ora
 * @param event l'evento che è stato generato e che deve essere gestito.
 */
static void hourly_operation(const union EVENT * const event){
	(void)event;
	LOG_EVENT_INFO("Changed hour\r\n");
}

static void ignition_verifier(const union EVENT * const event){
	if (event->as_generic.event == eEVENT_IGNITION_CHANGED){
		if (event->as_ignition.ignition == IGNITION_ON){
			wisker_reset(MODULE_VEHICLE);
		}
	}
}

/**
 * Definizione della stringa contenente il codice del software.
 */
extern const char interface_code[];

/**
 * @brief Entry point del programma.
 * Punto di avvio del programma.
 * @param argc numero dei parametri.
 * @param argv array con i parametri.
 * @return La funzione main non esce mai.
 */
int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	watchdog_init();
	init_board();
    logger_init();
	serial_init();
	wisker_init();
	wisker_enable(true);
	wisker_reset(MODULE_UART_LOWLEVEL);
	wisker_reset(MODULE_VEHICLE);
	event_init();
	
    event_connect_callback(eEVENT_SECONDS_CHANGED, secondly_operation);
	event_connect_callback(eEVENT_MINUTES_CHANGED, minutely_operation);
	event_connect_callback(eEVENT_HOURS_CHANGED, hourly_operation);
	event_connect_callback(eEVENT_IGNITION_CHANGED, ignition_verifier);
	service_init();
	LOG_DEF_NORMAL("Running %c%c%c - %s - %s %s\r\n", interface_code[0], interface_code[1], interface_code[2], interface_code, __DATE__, __TIME__);
    controller_init();
    settings_init();
    buttons_init();
    selector_init();
	selector_initialize_vehicle();
    LOG_DEF_NORMAL("scheduler\r\n");
	vTaskStartScheduler();
	return 0;
}
/**
 * @brief Callback chiamata dall'idle loop
 * Se questa funzione viene chiamata dall'rtos vuol dire che il processore non ha dati da elaborare.
 * Viene messo in sleep fino a quando non arriva un interrupt.
 * Il counter idle_counter viene utilizzato per determinare quante volte il processore viene svegliato.
 * Un suo numero basso vuol dire che ci sono stati pochi interrupt.
 */
void vApplicationIdleHook( void )
{
#ifdef __amd64__
	usleep(1000);
#else
	asm("WFI");
#endif
	idle_counter++;
}


void HardFault_Handler(void){
    static uint32_t counter = 0;
    if (counter++ >= 655350){
        counter = 0;
	    LOG_DEF_NORMAL("Hard fault\r\n");
    }
}

/**
 * @brief Callback chiamata in caso di errore irreversibile.
 * la funzione non dovrebbe mai essere chiamata. E' bloccante in quanto se si arriva qui è un errore irreversibile
 * causato dal non poter allocare memoria per l'RTOS.
 */
void vApplicationMallocFailedHook(){
	for(int i=0;i<100;i++){
        LOG_DEF_ERROR("Failed\r\n");
    }
}
/**
 * @}
 */

/*! \mainpage AlPark4 - Alpine
 *
 * \section intro_sec Introduzione
 *
 * Questa versione del software che può essere installata sulla scheda AlPark4.
 * Lo scopo del software è far comunicare il canbus del veicolo con la radio tramite il protocollo UART Alpine.
 *
 * \section implementation_sec Implementazione
 *
 * Il software si basa sul concetto di evento, quasi tutto quello che avviene viene trasformato in un evento e viene gestito
 * attraverso un unico task detto event_loop.
 * Il sistema di event permette la registrazione di funzioni che verranno eseguite in modo asincrono dall'event_loop ogni
 * volta che viene emesso un evento di tipo corretto.
 * E' responsabilità dell'implementatore registrare l'evento nell'event_loop e verificare che la funzione utilizzi i tipi di dati
 * corretti. L'event_loop esegue la funzione, non può verificare che i parametri passati alla funzione siano coerenti.
 *
 * \section Avvertenze durante lo sviluppo
 *
 * - il sistema ad eventi utilizza una fifo dell'RTOS per passare gli eventi da un task all'altro.
 * - La maggior parte del codice gira all'interno dell'event loop, verificare la ram riservata al task
 * - se la fifo dell'event loop si riempie degli eventi vengono persi, non eccedere con il numero di eventi che vengono trasmessi
 * - l'utilizzo di stampe all'interno dell'event loop è stato implementato, però se queste sono eccessive la fifo si riempie velocemente
 * - per il passaggio di dati agli altri task dall'event loop si possono usare delle callback. La callback può poi inserire dei dati all'interno di altre fifo. Se non si vuota velocemente la fifo dal task può bloccarsi l'event loop.
 * - in generale... non bloccare l'event loop e non fare cose troppo onerose al suo interno altrimenti le performance calano.
 * - è previsto un sistema di visualizzazione dei messaggi configurabile in runtime. Se si imposta in debug e gli eventi sono molti alcuni evento potrebbero venire scartati.
 *
 *
 * \section install_sec Installazione
 *
 * L'installazione può essere effettuata utilizzando Atmel studio oppure utilizzando openocd e/o il build system.
 * Dopo aver fatto la compilazione dei sorgenti make target_install o ninja target_install effettuerà la compilazione.
 *
 * etc...
 */
