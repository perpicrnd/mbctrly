if (CMAKE_CROSSCOMPILING)
	enable_testing()	#DEVE ESSERE PRIMA DI TUTTE LE ADD_SUBDIRECTORY
	
	SET(USED_UC "__SAM3A4C__")
	
	MESSAGE("Build type ALPARK4")
    SET(USED_BOARD "BOARD=BOARD_ALPARK4")
	
	add_definitions(-D${USED_UC}=1)
	add_definitions(-D${USED_BOARD})
	
	SET(CMAKE_EXE_LINKER_FLAGS_8 "-T${CMAKE_CURRENT_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x4/gcc/flash.ld  -static")
	SET(CMAKE_EXE_LINKER_FLAGS_A "-T${CMAKE_CURRENT_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x4/gcc/flash_mid.ld  -static")
	SET(CMAKE_EXE_LINKER_FLAGS_C "-T${CMAKE_CURRENT_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x8/gcc/flash_high.ld  -static")
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG   "${CMAKE_EXE_LINKER_FLAGS_DEBUG}   ${CMAKE_EXE_LINKER_FLAGS_COMMON}")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${CMAKE_EXE_LINKER_FLAGS_COMMON}")
	
	SET(CFLAGS "-mcpu=cortex-m3 -Os -pipe -mthumb -Wall -fno-common -ffreestanding  -fno-builtin  -mapcs -std=gnu99 -ffunction-sections  -fdata-sections -Wl,--gc-sections")
	SET(CMAKE_ASM_FLAGS "${CFLAGS} ${ASM_OPTIONS}" )
	SET(CMAKE_C_FLAGS  ${CFLAGS})
	
	SET(CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG} -g  ${CMAKE_ASM_FLAGS_COMMON}")
	SET(CMAKE_ASM_FLAGS_RELEASE "${CMAKE_ASM_FLAGS_RELEASE} ${CMAKE_ASM_FLAGS_COMMON}")  
	SET(CMAKE_ASM_FLAGS_DEBUG "${CMAKE_ASM_FLAGS_DEBUG}  -DDEBUG")
	SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g  -O0 ${CMAKE_C_FLAGS_COMMON}")  
	SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Os ${CMAKE_C_FLAGS_COMMON}")
	SET(CMAKE_EXE_LINKER_FLAGS_DEBUG   "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -g ${CMAKE_EXE_LINKER_FLAGS_COMMON}")  
	SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE}  ${CMAKE_EXE_LINKER_FLAGS_COMMON}")
	
	
	SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}  -DDEBUG")
	SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}  -DNDEBUG")
	
	SET(CMAKE_EXE_LINKER_FLAGS_LOW "-T${CMAKE_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x4/gcc/flash.ld  -static")
	SET(CMAKE_EXE_LINKER_FLAGS_MID "-T${CMAKE_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x4/gcc/flash_mid.ld  -static")
	SET(CMAKE_EXE_LINKER_FLAGS_HIGH "-T${CMAKE_SOURCE_DIR}/Libraries/ThirdParty/ASF/sam/utils/linker_scripts/sam3x/sam3x8/gcc/flash_high.ld  -static")
endif()