#pragma once
#include <stdint.h>

uint8_t firmware_car_number(void);
uint8_t hardware_version();
uint8_t hardware_release();
