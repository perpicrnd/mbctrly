#include <string.h>
#include "alpine_menu.h"
#include "alpine_task.h"
#include "canbus.h"
#include "event_list.h"
#include "logger.h"
#include "menu_manager.h"
#include "menu_walking.h"
#include "peugeot208.h"
#include "peugeot208_priv.h"
#include "vehicle.h"

static CAN_MESSAGE recv_message;
static menu_manager_t peugeot_208_menu;

enum PEUGEOT_208_MENU_DESC {
    PEUGEOT_208_MENU_MAIN = 0x80,
};

enum PEUGEOT_208_MENU {
    PEUGEOT_208_MENU_BUTTON_TRACTION_CONTROL_PRESENCE = 1,
    PEUGEOT_208_MENU_TRACTION_CONTROL,
    PEUGEOT_208_MENU_FUEL_CONSUMPTION,
    PEUGEOT_208_MENU_TEMPERATURE,
    PEUGEOT_208_MENU_GUIDE_ME_HOME,
    PEUGEOT_208_MENU_WELCOME_LIGHTS,
    PEUGEOT_208_MENU_WIPER_SETTINGS,
    PEUGEOT_208_MENU_AUTOSAFETY_BRAKE,
    PEUGEOT_208_MENU_DAILY_LIGHT,
    PEUGEOT_208_MENU_DIRECTION_LIGHTS,
    PEUGEOT_208_MENU_TIRE_RESET,
};

enum CONSUMPTIONS {
    CONS_KM_L,
    CONS_L_100KM,
    CONS_MPG,
};

enum TEMPERATURE {
    TEMPERATURE_CELSIUS = 0x00,  //Visualizza la temperatura in °C
    TEMPERATURE_FAHRENHEIT,      //Visualizza la temperatura in °F
};

enum LIGHT_TIME {
    TIME_OFF = 0x00,  //Time = OFF
    TIME_15SEC,       //Time = 15 sec
    TIME_30SEC,       //Time = 30 sec
    TIME_60SEC,       //Time = 60 sec
};

static const char empty[] = "\0";
static const char fahrenheit[] = "Fahrenheit";  //!< String used in the menu.
static const char celsius[] = "Celsius";        //!< String used in the menu.
static const char km_l[] = "Km / L";            //!< String used in the menu.
static const char l_km[] = "L / 100Km";         //!< String used in the menu.
static const char miles[] = "mpg";              //!< String used in the menu.

static const char t_off[] = "Off";       //!< String used in the menu.
static const char t_15sec[] = "15 Sec";  //!< String used in the menu.
static const char t_30sec[] = "30 Sec";  //!< String used in the menu.
static const char t_60sec[] = "60 Sec";  //!< String used in the menu.

static enum CONSUMPTIONS cons_measure_unit;
static enum TEMPERATURE temperature;
static enum LIGHT_TIME guide_me_home_lights = TIME_OFF;
static enum LIGHT_TIME welcome_lights = TIME_OFF;

static bool wiper_enable = false;
static bool safety_brake_enable = false;
static bool auto_drl = false;
static bool direction_light = false;
static uint8_t tyre_reset_enable_counter = 0;
static uint8_t no_action_tyre_reset_counter = 0;
static bool traction_control_button_present = false;
static bool traction_control = false;

static void inc_light_time(enum LIGHT_TIME *selected) {
    if (*selected == TIME_60SEC) {
        *selected = TIME_OFF;
    } else {
        (*selected)++;
    }
}

static enum LIGHT_TIME read_light_time(uint8_t value) {
    enum LIGHT_TIME ret;
    switch (value) {
        case 0x08:
            ret = TIME_15SEC;
            break;
        case 0x0A:
            ret = TIME_30SEC;
            break;
        case 0x0C:
            ret = TIME_60SEC;
            break;
        case 0x00:
        default:
            ret = TIME_OFF;
            break;
    }
    return (ret);
}

static void SetOptionVariables(const CAN_MESSAGE *const msg) {
    guide_me_home_lights = read_light_time(msg->can_data.byte3 >> 4);  //HIGH NIBBLE((msg->can_data.byte3 & 0x80) == 0x80);
    welcome_lights = read_light_time(msg->can_data.byte3 & 0x0F);      //LOW NIBBLE((msg->can_data.byte3 & 0x08) == 0x08);
    wiper_enable = ((msg->can_data.byte4 & 0x20) == 0x20);
    auto_drl = ((msg->can_data.byte2 & 0x02) == 0x02);
    safety_brake_enable = ((msg->can_data.byte5 & 0x10) == 0x10);
    direction_light = ((msg->can_data.byte2 & 0x04) == 0x04);

    if ((msg->can_data.byte1 & 0x40) == 0x40) {
        temperature = TEMPERATURE_FAHRENHEIT;
    } else {
        temperature = TEMPERATURE_CELSIUS;
    }

    uint8_t tmp;
    tmp = msg->can_data.byte0 & 0x03;
    switch (tmp) {
        case 0:
            cons_measure_unit = CONS_L_100KM;
            break;
        case 1:
            cons_measure_unit = CONS_KM_L;
            break;
        case 3:
            cons_measure_unit = CONS_MPG;
            break;
        default:
            cons_measure_unit = CONS_KM_L;
            break;
    }
}

static void handle_can_message(const union EVENT *const msg) {
    const uint32_t id = 0x260;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            memcpy(&recv_message, &msg->as_can_message.msg, sizeof(CAN_MESSAGE));
            recv_message.can_id = 0x15B;
            //settare le variabili a seconda del messaggio ricevuto
            const CAN_MESSAGE *const message = &msg->as_can_message.msg;
            SetOptionVariables(message);
        }
    }
}

static void handle_traction_can_msg(const union EVENT *const msg) {
    const uint32_t id = 0x268;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if (!traction_control_button_present) {
                if ((msg->as_can_message.msg.can_data.byte5 & 0x02) == 0x00) {
                    traction_control = true;
                } else {
                    traction_control = false;
                }
            }
        }
    }
}

//void option_fuelconsumption_complete(uint8_t selected)
static void option_fuelconsumption_complete(enum CONSUMPTIONS cons_measure_unit) {
    switch (cons_measure_unit) {
        case CONS_KM_L:
            recv_message.can_data.byte0 = 0x91;
            recv_message.can_data.byte1 &= 0x0F;
            recv_message.can_data.byte1 |= 0x10;
            event_can0_tx_message_emit(&recv_message);
            break;
        case CONS_L_100KM:
            recv_message.can_data.byte0 = 0x90;
            recv_message.can_data.byte1 &= 0x0F;
            recv_message.can_data.byte1 |= 0x10;
            event_can0_tx_message_emit(&recv_message);
            break;
        case CONS_MPG:
            recv_message.can_data.byte0 = 0x93;
            recv_message.can_data.byte1 &= 0x0F;
            recv_message.can_data.byte1 |= 0x90;
            event_can0_tx_message_emit(&recv_message);
            break;
    }
}

static void option_wiper_settings(bool active) {
    if (active) {
        recv_message.can_data.byte4 |= 0x20;
    } else {
        recv_message.can_data.byte4 &= ~0x20;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_guide_home(enum LIGHT_TIME selected) {
    switch (selected) {
        case TIME_OFF:  //OFF
            recv_message.can_data.byte3 &= 0x0F;
            break;
        case TIME_15SEC:  //15sec
            recv_message.can_data.byte3 &= 0x0F;
            recv_message.can_data.byte3 |= 0x80;
            break;
        case TIME_30SEC:  //30sec
            recv_message.can_data.byte3 &= 0x0F;
            recv_message.can_data.byte3 |= 0xA0;
            break;
        case TIME_60SEC:  //60 sec
            recv_message.can_data.byte3 &= 0x0F;
            recv_message.can_data.byte3 |= 0xC0;
            break;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_welcome_light(enum LIGHT_TIME selected) {
    switch (selected) {
        case TIME_OFF:  //OFF
            recv_message.can_data.byte3 &= 0xF0;
            break;
        case TIME_15SEC:  //15sec
            recv_message.can_data.byte3 &= 0xF0;
            recv_message.can_data.byte3 |= 0x08;
            break;
        case TIME_30SEC:  //30sec
            recv_message.can_data.byte3 &= 0xF0;
            recv_message.can_data.byte3 |= 0x0A;
            break;
        case TIME_60SEC:  //60 sec
            recv_message.can_data.byte3 &= 0xF0;
            recv_message.can_data.byte3 |= 0x0C;
            break;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_safety_brake(bool active) {
    if (active) {
        recv_message.can_data.byte5 |= 0x10;
    } else {
        recv_message.can_data.byte5 &= ~0x10;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_daily_light(bool active) {  //daytime running lamps
    if (active) {
        recv_message.can_data.byte2 |= 0x02;
    } else {
        recv_message.can_data.byte2 &= ~0x02;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_direction_lights(bool active) {
    if (active) {
        recv_message.can_data.byte2 |= 0x04;
    } else {
        recv_message.can_data.byte2 &= ~0x04;
    }
    event_can0_tx_message_emit(&recv_message);
}

static void option_temperature_measure(enum TEMPERATURE temp) {
    if (temp == TEMPERATURE_FAHRENHEIT) {
        recv_message.can_data.byte1 |= 0x40;
    } else {
        recv_message.can_data.byte1 &= ~0x40;
    }
    event_can0_tx_message_emit(&recv_message);
}

/**
 * @brief Traduzione della stringa Vehicle settings nelle varie lingue .
 */
static const char *vehicle_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Fahrzeugeinstellungen",       //!< LANGUAGE_DEFINITION_GERMAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Réglages du véhicule",        //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni veicolo",        //!< LANGUAGE_DEFINITION_ITALIAN
    "Configuración del vehículo",  //!< LANGUAGE_DEFINITION_SPANISH
    "Configuração do veiculo",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Ustawienia Pojazdu",          //!< LANGUAGE_DEFINITION_POLISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CZECH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SWEDISH
    "Voertuig Instellingen",       //!< LANGUAGE_DEFINITION_DUTCH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_JAPANESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_RUSSIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_KOREAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_TURKISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CHINESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *traction_control_button_presence_str[LANGUAGE_DEFINITION_COUNT] = {
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_GERMAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_FRENCH
    "Pulsante Controllo Trazione",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_SPANISH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_POLISH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_CZECH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_DUTCH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_KOREAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_TURKISH
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_CHINESE
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Traction Control Button",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *traction_control_str[LANGUAGE_DEFINITION_COUNT] = {
    "Traction Control",    //!< LANGUAGE_DEFINITION_GERMAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_ENGLISH
    "Traction Control",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Traction Control",    //!< LANGUAGE_DEFINITION_FRENCH
    "Controllo Trazione",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_SPANISH
    "Traction Control",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Traction Control",    //!< LANGUAGE_DEFINITION_POLISH
    "Traction Control",    //!< LANGUAGE_DEFINITION_CZECH
    "Traction Control",    //!< LANGUAGE_DEFINITION_SWEDISH
    "Traction Control",    //!< LANGUAGE_DEFINITION_DUTCH
    "Traction Control",    //!< LANGUAGE_DEFINITION_JAPANESE
    "Traction Control",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_KOREAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_TURKISH
    "Traction Control",    //!< LANGUAGE_DEFINITION_CHINESE
    "Traction Control",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Traction Control",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *measure_units_str[] = {
    "Kraftstoffverbrauch",  //!< LANGUAGE_DEFINITION_GERMAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Consommation",         //!< LANGUAGE_DEFINITION_FRENCH
    "Consumi",              //!< LANGUAGE_DEFINITION_ITALIAN
    "Consumos",             //!< LANGUAGE_DEFINITION_SPANISH
    "Consumos",             //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Zużycie paliwa",       //!< LANGUAGE_DEFINITION_POLISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_CZECH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Brandstofverbruik",    //!< LANGUAGE_DEFINITION_DUTCH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_KOREAN
    "Tüketim",              //!< LANGUAGE_DEFINITION_TURKISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_CHINESE
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * @brief Traduzione della stringa Temperature units nelle varie lingue.
 */
static const char *temperature_units_str[] = {
    "Temperatur",   //!< LANGUAGE_DEFINITION_GERMAN
    "Temperature",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Temperature",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Température",  //!< LANGUAGE_DEFINITION_FRENCH
    "Temperatura",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Temperatura",  //!< LANGUAGE_DEFINITION_SPANISH
    "Temperatura",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Temperatura",  //!< LANGUAGE_DEFINITION_POLISH
    "Temperature",  //!< LANGUAGE_DEFINITION_CZECH
    "Temperature",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Temperatuur",  //!< LANGUAGE_DEFINITION_DUTCH
    "Temperature",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Temperature",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_KOREAN
    "Sıcaklık",     //!< LANGUAGE_DEFINITION_TURKISH
    "Temperature",  //!< LANGUAGE_DEFINITION_CHINESE
    "Temperature",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_HUNGARIAN

};

static const char *guide_home_str[] = {
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_GERMAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_FRENCH
    "Luci di Localizzazione",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_SPANISH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_POLISH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_CZECH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_DUTCH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_KOREAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_TURKISH
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_CHINESE
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Guide Home Lights",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *welcome_str[] = {
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_GERMAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_FRENCH
    "Luci di Benvenuto",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_SPANISH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_POLISH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_CZECH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_DUTCH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_KOREAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_TURKISH
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_CHINESE
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Welcome Lights",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *wiper_str[] = {
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_GERMAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_ENGLISH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_FRENCH
    "Tergicristallo Automatico",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_SPANISH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_POLISH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_CZECH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_SWEDISH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_DUTCH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_JAPANESE
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_RUSSIAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_KOREAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_TURKISH
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_CHINESE
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Auto Wiper",                 //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *autobrake_str[] = {
    "Auto Brake",               //!< LANGUAGE_DEFINITION_GERMAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_ENGLISH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Auto Brake",               //!< LANGUAGE_DEFINITION_FRENCH
    "Freno a mano Automatico",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_SPANISH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Auto Brake",               //!< LANGUAGE_DEFINITION_POLISH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_CZECH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_SWEDISH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_DUTCH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_JAPANESE
    "Auto Brake",               //!< LANGUAGE_DEFINITION_RUSSIAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_KOREAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_TURKISH
    "Auto Brake",               //!< LANGUAGE_DEFINITION_CHINESE
    "Auto Brake",               //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Auto Brake",               //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *daily_light_str[] = {
    "Auto DRL",     //!< LANGUAGE_DEFINITION_GERMAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Auto DRL",     //!< LANGUAGE_DEFINITION_FRENCH
    "Luci Diurne",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_SPANISH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Auto DRL",     //!< LANGUAGE_DEFINITION_POLISH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_CZECH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_DUTCH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Auto DRL",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_KOREAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_TURKISH
    "Auto DRL",     //!< LANGUAGE_DEFINITION_CHINESE
    "Auto DRL",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Auto DRL",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *direction_light_str[] = {
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_GERMAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_FRENCH
    "Luci Direzionali",       //!< LANGUAGE_DEFINITION_ITALIAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_SPANISH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_POLISH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_CZECH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_DUTCH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_KOREAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_TURKISH
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_CHINESE
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Auto Direction Lights",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *tire_pressure_reset_str[] = {
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_GERMAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_ENGLISH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_FRENCH
    "Reset Pressione Pneumatici",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_SPANISH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_POLISH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_CZECH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_SWEDISH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_DUTCH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_JAPANESE
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_RUSSIAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_KOREAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_TURKISH
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_CHINESE
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Tire Pressure Reset",         //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *on_str[] = {
    "Ein",   //!< LANGUAGE_DEFINITION_GERMAN
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "On",    //!< LANGUAGE_DEFINITION_FRENCH
    "On",    //!< LANGUAGE_DEFINITION_ITALIAN
    "On",    //!< LANGUAGE_DEFINITION_SPANISH
    "On",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "On",    //!< LANGUAGE_DEFINITION_POLISH
    "On",    //!< LANGUAGE_DEFINITION_CZECH
    "On",    //!< LANGUAGE_DEFINITION_SWEDISH
    "On",    //!< LANGUAGE_DEFINITION_DUTCH
    "On",    //!< LANGUAGE_DEFINITION_JAPANESE
    "On",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "On",    //!< LANGUAGE_DEFINITION_KOREAN
    "Açık",  //!< LANGUAGE_DEFINITION_TURKISH
    "On",    //!< LANGUAGE_DEFINITION_CHINESE
    "On",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "On",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "On",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *off_str[] = {
    "Aus",     //!< LANGUAGE_DEFINITION_GERMAN
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Off",     //!< LANGUAGE_DEFINITION_FRENCH
    "Off",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Off",     //!< LANGUAGE_DEFINITION_SPANISH
    "Off",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Off",     //!< LANGUAGE_DEFINITION_POLISH
    "Off",     //!< LANGUAGE_DEFINITION_CZECH
    "Off",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Off",     //!< LANGUAGE_DEFINITION_DUTCH
    "Off",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Off",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Off",     //!< LANGUAGE_DEFINITION_KOREAN
    "Kapalı",  //!< LANGUAGE_DEFINITION_TURKISH
    "Off",     //!< LANGUAGE_DEFINITION_CHINESE
    "Off",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Off",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Off",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *yes_str[] = {
    "Ja",   //!< LANGUAGE_DEFINITION_GERMAN
    "Yes",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Yes",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Yes",  //!< LANGUAGE_DEFINITION_FRENCH
    "Sì",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Yes",  //!< LANGUAGE_DEFINITION_SPANISH
    "Yes",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Yes",  //!< LANGUAGE_DEFINITION_POLISH
    "Yes",  //!< LANGUAGE_DEFINITION_CZECH
    "Yes",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Yes",  //!< LANGUAGE_DEFINITION_DUTCH
    "Yes",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Yes",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Yes",  //!< LANGUAGE_DEFINITION_KOREAN
    "Yes",  //!< LANGUAGE_DEFINITION_TURKISH
    "Yes",  //!< LANGUAGE_DEFINITION_CHINESE
    "Yes",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Yes",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Yes",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *no_str[] = {
    "Nein",  //!< LANGUAGE_DEFINITION_GERMAN
    "No",    //!< LANGUAGE_DEFINITION_ENGLISH
    "No",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "No",    //!< LANGUAGE_DEFINITION_FRENCH
    "No",    //!< LANGUAGE_DEFINITION_ITALIAN
    "No",    //!< LANGUAGE_DEFINITION_SPANISH
    "No",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "No",    //!< LANGUAGE_DEFINITION_POLISH
    "No",    //!< LANGUAGE_DEFINITION_CZECH
    "No",    //!< LANGUAGE_DEFINITION_SWEDISH
    "No",    //!< LANGUAGE_DEFINITION_DUTCH
    "No",    //!< LANGUAGE_DEFINITION_JAPANESE
    "No",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "No",    //!< LANGUAGE_DEFINITION_KOREAN
    "No",    //!< LANGUAGE_DEFINITION_TURKISH
    "No",    //!< LANGUAGE_DEFINITION_CHINESE
    "No",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "No",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "No",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * @brief Restituisce la stringa associata all'unita' di misura dei consumi corrente.
 * @return km/l l/100km miglia/gallone
 */
static const char *peugeot_208_get_current_measure_str(void) {
    enum CONSUMPTIONS comp_value = cons_measure_unit;
    const char *comp_str;
    if (comp_value == CONS_KM_L) {
        comp_str = km_l;
    } else if (comp_value == CONS_L_100KM) {
        comp_str = l_km;
    } else {
        comp_str = miles;
    }
    return comp_str;
}

/**
 * @brief Restituisce la stringa associata all'unita' di misura della temperatura corrente.
 * @return celsius o fahrenheit.
 */
static const char *peugeot_208_get_current_temperature_str() {
    enum TEMPERATURE temp_value = temperature;
    const char *temp_str;
    if (temp_value == TEMPERATURE_CELSIUS) {
        temp_str = celsius;
    } else {
        temp_str = fahrenheit;
    }
    return temp_str;
}

/**
 * @brief Restituisce la stringa associata alla durata di luce on
 * @return OFF, 15 sec, 30 sec, 60 sec.
 */
static const char *peugeot_208_get_current_light_time_str(enum LIGHT_TIME l_time) {
    const char *temp_str;
    switch (l_time) {
        case TIME_15SEC:
            temp_str = t_15sec;
            break;
        case TIME_30SEC:
            temp_str = t_30sec;
            break;
        case TIME_60SEC:
            temp_str = t_60sec;
            break;
        //case TIME_OFF
        default:
            temp_str = t_off;
            break;
    }

    return temp_str;
}

static bool peugeot_208_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(peugeot_208_menu), PEUGEOT_208_MENU_MAIN, vehicle_settings_str[language]);
}

static bool peugeot_208_menu_traction_control_button_presence(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *yesno = no_str[language];
    if (traction_control_button_present) {
        yesno = yes_str[language];
    }

    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, PEUGEOT_208_MENU_BUTTON_TRACTION_CONTROL_PRESENCE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, traction_control_button_presence_str[language], yesno);
}
static bool peugeot_208_menu_traction_control(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *onoff = off_str[language];
    if (traction_control) {
        onoff = on_str[language];
    }

    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, PEUGEOT_208_MENU_TRACTION_CONTROL, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, traction_control_str[language], onoff);
}

static bool peugeot_208_menu_fuel_consumption(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_FUEL_CONSUMPTION, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, measure_units_str[language], peugeot_208_get_current_measure_str());
}

static bool peugeot_208_menu_temperature(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_TEMPERATURE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, temperature_units_str[language], peugeot_208_get_current_temperature_str());
}

static bool peugeot_208_menu_guide_me_home(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();

    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_GUIDE_ME_HOME, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, guide_home_str[language], peugeot_208_get_current_light_time_str(guide_me_home_lights));
}

static bool peugeot_208_menu_welcome_lights(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();

    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_WELCOME_LIGHTS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, welcome_str[language], peugeot_208_get_current_light_time_str(welcome_lights));
}

static bool peugeot_208_menu_wiper(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *onoff = off_str[language];
    if (wiper_enable) {
        onoff = on_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_WIPER_SETTINGS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, wiper_str[language], onoff);
}

static bool peugeot_208_menu_autosafety(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *onoff = off_str[language];
    if (safety_brake_enable) {
        onoff = on_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_AUTOSAFETY_BRAKE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, autobrake_str[language], onoff);
}

static bool peugeot_208_menu_daily_lights(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *onoff = off_str[language];
    if (auto_drl) {
        onoff = on_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_DAILY_LIGHT, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, daily_light_str[language], onoff);
}

static bool peugeot_208_menu_direction_lights(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *onoff = off_str[language];
    if (direction_light) {
        onoff = on_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_DIRECTION_LIGHTS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, direction_light_str[language], onoff);
}

static bool peugeot_208_menu_tire_reset(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num,
                                              PEUGEOT_208_MENU_TIRE_RESET, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, tire_pressure_reset_str[language], empty);
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */

static inline void handle_vehicle_main_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    if (!(row_pressed == 00 && menu_index == 00 && menu_data == 01)) {
        switch (menu_data) {
            case PEUGEOT_208_MENU_BUTTON_TRACTION_CONTROL_PRESENCE:
                traction_control_button_present = !traction_control_button_present;
                pegeout_208_set_traction_control_button_presence(traction_control_button_present);
                if (traction_control_button_present) {
                    menu_manager_disable_bit(peugeot_208_menu, PEUGEOT_208_MENU_TRACTION_CONTROL);
                } else {
                    menu_manager_enable_bit(peugeot_208_menu, PEUGEOT_208_MENU_TRACTION_CONTROL);
                }
                break;
            case PEUGEOT_208_MENU_TRACTION_CONTROL:
                traction_control = !traction_control;
                break;
            case PEUGEOT_208_MENU_FUEL_CONSUMPTION:
                if (cons_measure_unit == CONS_MPG) {
                    cons_measure_unit = CONS_KM_L;
                } else {
                    cons_measure_unit++;
                }
                option_fuelconsumption_complete(cons_measure_unit);
                break;
            case PEUGEOT_208_MENU_TEMPERATURE:
                if (temperature == TEMPERATURE_CELSIUS) {
                    temperature = TEMPERATURE_FAHRENHEIT;
                } else {
                    temperature = TEMPERATURE_CELSIUS;
                }
                option_temperature_measure(temperature);
                break;
            case PEUGEOT_208_MENU_GUIDE_ME_HOME:
                inc_light_time(&guide_me_home_lights);
                option_guide_home(guide_me_home_lights);
                break;
            case PEUGEOT_208_MENU_WELCOME_LIGHTS:
                inc_light_time(&welcome_lights);
                option_welcome_light(welcome_lights);
                break;
            case PEUGEOT_208_MENU_WIPER_SETTINGS:
                wiper_enable = !wiper_enable;
                option_wiper_settings(wiper_enable);
                break;
            case PEUGEOT_208_MENU_AUTOSAFETY_BRAKE:
                safety_brake_enable = !safety_brake_enable;
                option_safety_brake(safety_brake_enable);
                break;
            case PEUGEOT_208_MENU_DAILY_LIGHT:
                auto_drl = !auto_drl;
                option_daily_light(auto_drl);
                break;
            case PEUGEOT_208_MENU_DIRECTION_LIGHTS:
                direction_light = !direction_light;
                option_direction_lights(direction_light);
                break;
            case PEUGEOT_208_MENU_TIRE_RESET:
                tyre_reset_enable_counter = 10;
                break;
            default:
                break;
        }
        menu_walking_push(PEUGEOT_208_MENU_MAIN);
        enable_radio_menu_refresh(peugeot_208_menu);
    } else {
        menu_walking_push(PEUGEOT_208_MENU_MAIN);
        enable_radio_menu_refresh(peugeot_208_menu);
    }
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_general_settings(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
        ///Ricevuta una richiesta di trasmissione di menu
        if (msg->as_menu_request.msg_type == MENU_REQUEST_NUM_E5) {
            const uint8_t row_pressed =
                msg->as_menu_request.request.as_e5.row_pressed_num;
            const uint8_t menu_index =
                msg->as_menu_request.request.as_e5.menu_index;
            const uint8_t menu_data =
                msg->as_menu_request.request.as_e5.menu_data;
            uint8_t current_menu = menu_walking_current();
            if ((current_menu == 0x00) && (menu_data == PEUGEOT_208_MENU_MAIN)) {
                LOG_DEF_NORMAL("Peugeot 208 main menu\r\n");
                handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
            } else if (current_menu >= PEUGEOT_208_MENU_MAIN) {
                switch (current_menu) {
                    case PEUGEOT_208_MENU_MAIN:
                        handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
                        break;
                    default:
                        LOG_DEF_ERROR("Vehicle: Unknown menu %02X %02X %02X %02X\r\n", current_menu, row_pressed, menu_index, menu_data);
                        break;
                }
            }
        }
    }
}

void peugeot_208_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, peugeot_208_menu_send_header);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_BUTTON_TRACTION_CONTROL_PRESENCE, peugeot_208_menu_traction_control_button_presence);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_TRACTION_CONTROL, peugeot_208_menu_traction_control);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_FUEL_CONSUMPTION, peugeot_208_menu_fuel_consumption);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_TEMPERATURE, peugeot_208_menu_temperature);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_GUIDE_ME_HOME, peugeot_208_menu_guide_me_home);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_WELCOME_LIGHTS, peugeot_208_menu_welcome_lights);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_WIPER_SETTINGS, peugeot_208_menu_wiper);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_AUTOSAFETY_BRAKE, peugeot_208_menu_autosafety);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_DAILY_LIGHT, peugeot_208_menu_daily_lights);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_DIRECTION_LIGHTS, peugeot_208_menu_direction_lights);
    menu_manager_enable_bit_func(mngr, PEUGEOT_208_MENU_TIRE_RESET, peugeot_208_menu_tire_reset);
}

static CAN_MESSAGE message_button_pressure_mgmt;
static void set_pressure_bit(void) {
    message_button_pressure_mgmt.can_id = 0x1A9;
    message_button_pressure_mgmt.can_is_ext = false;
    message_button_pressure_mgmt.interface = CAN_INTERFACE_0;
    message_button_pressure_mgmt.can_data.byte0 = 0x00;
    message_button_pressure_mgmt.can_data.byte1 = 0x7F;
    message_button_pressure_mgmt.can_data.byte2 = 0xFF;
    message_button_pressure_mgmt.can_data.byte3 = 0x00;
    message_button_pressure_mgmt.can_data.byte4 = 0xFF;
    message_button_pressure_mgmt.can_data.byte5 = 0x01;  //
    message_button_pressure_mgmt.can_data.byte6 = 0x50;
    message_button_pressure_mgmt.can_data.byte7 = 0x00;
    message_button_pressure_mgmt.datalen = 8;
}

static void reset_pressure_bit(void) {
    message_button_pressure_mgmt.can_id = 0x1A9;
    message_button_pressure_mgmt.can_is_ext = false;
    message_button_pressure_mgmt.interface = CAN_INTERFACE_0;
    message_button_pressure_mgmt.can_data.byte0 = 0x00;
    message_button_pressure_mgmt.can_data.byte1 = 0x7F;
    message_button_pressure_mgmt.can_data.byte2 = 0xFF;
    message_button_pressure_mgmt.can_data.byte3 = 0x00;
    message_button_pressure_mgmt.can_data.byte4 = 0xFF;
    message_button_pressure_mgmt.can_data.byte5 = 0x00;  //
    message_button_pressure_mgmt.can_data.byte6 = 0x50;
    message_button_pressure_mgmt.can_data.byte7 = 0x00;
    message_button_pressure_mgmt.datalen = 8;
}

static void handle_tyres_pressure(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        if (is_vehicle_can_ignition_on()) {
            if (tyre_reset_enable_counter != 0) {
                tyre_reset_enable_counter--;
                //imposto il bit a uno del reset e trasmetto il messaggio
                set_pressure_bit();
                event_can0_tx_message_emit(&message_button_pressure_mgmt);
            } else {
                if (no_action_tyre_reset_counter++ == 1) {
                    no_action_tyre_reset_counter = 0;
                    //trasmetto il messaggio senza azioni
                    reset_pressure_bit();
                    event_can0_tx_message_emit(&message_button_pressure_mgmt);
                }
            }
        }
    }
}

static CAN_MESSAGE message_button_traction_mgmt;

void set_traction_bit(void) {
    message_button_traction_mgmt.can_id = 0x329;
    message_button_traction_mgmt.can_is_ext = false;
    message_button_traction_mgmt.interface = CAN_INTERFACE_0;
    message_button_traction_mgmt.can_data.byte0 = 0x0C;
    message_button_traction_mgmt.can_data.byte1 = 0x00;
    message_button_traction_mgmt.can_data.byte2 = 0x04;
    message_button_traction_mgmt.can_data.byte3 = 0x01;  //
    message_button_traction_mgmt.can_data.byte4 = 0x00;
    message_button_traction_mgmt.can_data.byte5 = 0x00;
    message_button_traction_mgmt.datalen = 6;
}

void reset_traction_bit(void) {
    message_button_traction_mgmt.can_id = 0x329;
    message_button_traction_mgmt.can_is_ext = false;
    message_button_traction_mgmt.interface = CAN_INTERFACE_0;
    message_button_traction_mgmt.can_data.byte0 = 0x0C;
    message_button_traction_mgmt.can_data.byte1 = 0x00;
    message_button_traction_mgmt.can_data.byte2 = 0x04;
    message_button_traction_mgmt.can_data.byte3 = 0x00;  //
    message_button_traction_mgmt.can_data.byte4 = 0x00;
    message_button_traction_mgmt.can_data.byte5 = 0x00;
    message_button_traction_mgmt.datalen = 6;
}

static bool tx_flag = false;
static bool old_traction = false;
static void handle_traction_control(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_50MSEC_CHANGED) {
        if (is_vehicle_can_ignition_on()) {
            if (!traction_control_button_present) {
                //Non ho i pulsanti attaccati alla macchina e quindi devo trasmettere io.
                tx_flag = !tx_flag;
                if (tx_flag) {
                    //Sono passati 100ms.
                    if (old_traction != traction_control) {
                        old_traction = traction_control;
                        if (traction_control) {
                            set_traction_bit();
                        } else {
                            reset_traction_bit();
                        }
                    }
                    event_can0_tx_message_emit(&message_button_traction_mgmt);
                }
            }
        }
    }
}

void initialize_traction_control_status(void) {
    traction_control_button_present = peugeot_208_traction_control_button_present();
    if (traction_control_button_present) {
        menu_manager_disable_bit(peugeot_208_menu, PEUGEOT_208_MENU_TRACTION_CONTROL);
    } else {
        menu_manager_enable_bit(peugeot_208_menu, PEUGEOT_208_MENU_TRACTION_CONTROL);
    }
}

void peugeot208_menu_init(void) {
    peugeot_208_menu = menu_manager_create();
    peugeot_208_menu_fill_data(peugeot_208_menu);
    alpine_menu_disable_lin_management();
    alpine_menu_disable_preflight_check();
    event_connect_callback(eEVENT_MENU_REQUEST, handle_vehicle_general_settings);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_can_message);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_traction_can_msg);
    event_connect_callback(eEVENT_50MSEC_CHANGED, handle_tyres_pressure);
    event_connect_callback(eEVENT_50MSEC_CHANGED, handle_traction_control);
}