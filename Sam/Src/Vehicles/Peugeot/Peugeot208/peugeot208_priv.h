#pragma once

#include <stdbool.h>
#include "settings.h"

enum PEUGEOT_SETTINGS {
    PEUGEOT_SETTING_TRACTION_CONTROL_BUTTON_PRESENCE = VEHICLE_SETTINGS_START,
    PEUGEOT_SETTING_LAST  //Non mettere niente sotto a questa. Al momento disponibile per essere usato.
};

void peugeot208_menu_init(void);
void peugeot_208_parksens_init(void);
void initialize_traction_control_status(void);

bool peugeot_208_traction_control_button_present(void);
void pegeout_208_set_traction_control_button_presence(bool value);
bool is_vehicle_can_ignition_on(void);