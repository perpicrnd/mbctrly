#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "event_list.h"
#include "events.h"
#include "string.h"

#include <cmocka.h>
#include "peugeot208_menu.c"

bool vehicle_ignition = false;

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void software_reset(void) {
}

void event_out_service_emit(void) {
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

menu_manager_t __real_menu_manager_create(void);
menu_manager_t __wrap_menu_manager_create(void) {
    function_called();
    return __real_menu_manager_create();
}

bool __wrap_is_vehicle_can_ignition_on(void) {
    return (vehicle_ignition);
}

void __wrap_menu_manager_enable_bit_func(menu_manager_t menu, uint8_t pos, ALPINE_EVENT_MENU_SEND_FUNC func) {
    (void)menu;
    (void)pos;
    (void)func;
    function_called();
}

void __wrap_alpine_menu_disable_lin_management(void) {
    function_called();
}

void __wrap_alpine_menu_disable_preflight_check(void) {
    function_called();
}

void __wrap_event_can0_tx_message_emit(const CAN_MESSAGE *const msg) {
    (void)msg;
    function_called();
}

void test_peugeot_208_menu_init(void **setup) {
    (void)setup;

    expect_function_call(__wrap_menu_manager_create);
    expect_function_calls(__wrap_menu_manager_enable_bit_func, 12);
    expect_function_call(__wrap_alpine_menu_disable_lin_management);
    expect_function_call(__wrap_alpine_menu_disable_preflight_check);
    expect_value(__wrap_event_connect_callback, event, eEVENT_MENU_REQUEST);
    expect_value(__wrap_event_connect_callback, handler, handle_vehicle_general_settings);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_message);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_traction_can_msg);
    expect_value(__wrap_event_connect_callback, event, eEVENT_50MSEC_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_tyres_pressure);
    expect_value(__wrap_event_connect_callback, event, eEVENT_50MSEC_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_traction_control);
    peugeot208_menu_init();
}

void test_handle_can_message(void **state) {
    (void)state;

    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0x260;
    event.as_can_message.msg.can_data.byte0 = 0x85;
    event.as_can_message.msg.can_data.byte1 = 0x74;
    event.as_can_message.msg.can_data.byte2 = 0x8D;
    event.as_can_message.msg.can_data.byte3 = 0x3A;
    event.as_can_message.msg.can_data.byte4 = 0xE0;
    event.as_can_message.msg.can_data.byte5 = 0x44;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    event.as_can_message.msg.can_data.byte7 = 0x00;

    handle_can_message(&event);
    assert_true(guide_me_home_lights == TIME_OFF);
    assert_true(welcome_lights == TIME_30SEC);
    assert_true(wiper_enable == true);
    assert_true(auto_drl == false);
    assert_true(safety_brake_enable == false);
    assert_true(direction_light == true);
    assert_true(temperature == TEMPERATURE_FAHRENHEIT);
    assert_true(cons_measure_unit == CONS_KM_L);

    event.as_can_message.msg.can_data.byte0 = 0x87;
    event.as_can_message.msg.can_data.byte1 = 0xF4;
    event.as_can_message.msg.can_data.byte2 = 0x8F;
    event.as_can_message.msg.can_data.byte3 = 0xC8;
    event.as_can_message.msg.can_data.byte4 = 0xC0;
    event.as_can_message.msg.can_data.byte5 = 0x44;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    event.as_can_message.msg.can_data.byte7 = 0x00;

    handle_can_message(&event);
    assert_true(guide_me_home_lights == TIME_60SEC);
    assert_true(welcome_lights == TIME_15SEC);
    assert_true(wiper_enable == false);
    assert_true(auto_drl == true);
    assert_true(safety_brake_enable == false);
    assert_true(direction_light == true);
    assert_true(temperature == TEMPERATURE_FAHRENHEIT);
    assert_true(cons_measure_unit == CONS_MPG);
}

void test_handle_tyres_pressure(void **state) {
    (void)state;

    union EVENT event;
    event.as_generic.event = eEVENT_50MSEC_CHANGED;

    vehicle_ignition = false;
    handle_tyres_pressure(&event);

    vehicle_ignition = true;
    tyre_reset_enable_counter = 10;
    expect_function_call(__wrap_event_can0_tx_message_emit);
    //
    handle_tyres_pressure(&event);
    assert_true(tyre_reset_enable_counter == 9);
    assert_true(message_button_pressure_mgmt.can_data.byte5 == 0x01);

    tyre_reset_enable_counter = 0;
    no_action_tyre_reset_counter = 0;

    handle_tyres_pressure(&event);
    assert_true(no_action_tyre_reset_counter == 1);

    no_action_tyre_reset_counter = 1;
    expect_function_call(__wrap_event_can0_tx_message_emit);
    handle_tyres_pressure(&event);
    assert_true(no_action_tyre_reset_counter == 0);
    assert_true(message_button_pressure_mgmt.can_data.byte5 == 0x00);
}

void test_handle_traction_control(void **state) {
    (void)state;

    traction_control_button_present = false;
    tx_flag = false;
    old_traction = false;
    traction_control = true;

    union EVENT event;
    event.as_generic.event = eEVENT_50MSEC_CHANGED;

    vehicle_ignition = false;
    handle_traction_control(&event);

    vehicle_ignition = true;
    expect_function_call(__wrap_event_can0_tx_message_emit);

    handle_traction_control(&event);

    assert_true(message_button_traction_mgmt.can_data.byte3 == 0x01);
    assert_true(old_traction == true);
    assert_true(old_traction == traction_control);

    traction_control = false;
    tx_flag = false;

    expect_function_call(__wrap_event_can0_tx_message_emit);

    handle_traction_control(&event);

    assert_true(message_button_traction_mgmt.can_data.byte3 == 0x00);
    assert_true(old_traction == false);
    assert_true(old_traction == traction_control);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_peugeot_208_menu_init),
        cmocka_unit_test(test_handle_can_message),
        cmocka_unit_test(test_handle_tyres_pressure),
        cmocka_unit_test(test_handle_traction_control),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}