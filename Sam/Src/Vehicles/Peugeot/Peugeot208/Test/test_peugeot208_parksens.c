#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "event_list.h"
#include "events.h"
#include "string.h"

#include <cmocka.h>
#include "peugeot208_parksens.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void software_reset(void) {
}

void event_out_service_emit(void) {
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

void __wrap_alpine_menu_parking_sensors_type(enum PARKING_SENSOR_TYPE type) {
    check_expected(type);
}

struct EVENT_PARKING_SENSOR transmitted = {0};
void __wrap_event_parking_sensor_emit(struct EVENT_PARKING_SENSOR *data) {
    memcpy(&transmitted, data, sizeof(struct EVENT_PARKING_SENSOR));
    function_called();
}

void __wrap_set_vehicle_without_parking_sensor_buzzer(void) {
    function_called();
}

void test_handle_parksens_once_second(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0xE1;

    event.as_can_message.msg.can_data.byte0 = 0x60;
    event.as_can_message.msg.can_data.byte2 = 0x3F;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);

    handle_parksens(&event);
    assert_false(tx_enable_1sec);
    assert_true(tx_enable_counter == 0);

    //rimando gli stessi dati, mi aspetto che non ci sia l'emit dei sensori perché uguali ed il secondo non è ancora passato e tx_enable_counter = 0.
    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    //expect_function_call(__wrap_event_parking_sensor_emit);
    handle_parksens(&event);
    assert_false(tx_enable_1sec);
    assert_true(tx_enable_counter == 0);

    //Scatta il secondo
    tx_enable_1sec = true;

    //A questo punto ho ancora la trasmissione. I sensori li abilito (act_value == 0) con un opportuno valore sul byte 0.
    event.as_can_message.msg.can_data.byte0 = 0x0;
    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);

    handle_parksens(&event);
    assert_false(tx_enable_1sec);
    assert_true(tx_enable_counter == 5);
    assert_true(old_act_value == 0);
    assert_true(transmitted.data.park_status.as_flags.active == 1);
    assert_true(parksensor_counter == 1);

    //A questo punto disabilito i sensori (act_value != 0) con un opportuno valore sul byte 0.
    event.as_can_message.msg.can_data.byte0 = 0x62;
    tx_enable_1sec = true;
    tx_enable_counter = 4;
    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);

    handle_parksens(&event);
    assert_false(tx_enable_1sec);
    assert_true(tx_enable_counter == 5);
    assert_true(old_act_value == 0x60);
    assert_true(parksensor_counter == 0);
    assert_true(transmitted.data.park_status.as_flags.active == 1);

    //Dopo 5 secondi, non trasmetto più nulla (tx_enable_counter = 0)
    tx_enable_1sec = true;
    tx_enable_counter = 0;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    handle_parksens(&event);
    assert_true(tx_enable_1sec);
    assert_true(tx_enable_counter == 0);
    assert_true(old_act_value == 0x60);
    assert_true(parksensor_counter == 0);
}

void test_handle_parksens(void **setup) {
    (void)setup;

    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0xE1;

    event.as_can_message.msg.can_data.byte0 = 0x60;
    event.as_can_message.msg.can_data.byte2 = 0x3F;

    tx_enable_counter = 4;
    tx_enable_1sec = true;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);
    handle_parksens(&event);
    assert_false(tx_enable_1sec);
    assert_true(tx_enable_counter == 4);

    event.as_can_message.msg.can_data.byte0 = 0x00;
    event.as_can_message.msg.can_data.byte2 = 0x00;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);

    handle_parksens(&event);
    assert_true(old_act_value == 0x00);
    assert_true(tx_enable_counter == 5);

    event.as_can_message.msg.can_data.byte0 = 0x00;
    event.as_can_message.msg.can_data.byte2 = 0x20;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);

    handle_parksens(&event);
    assert_true(old_act_value == 0x00);
    assert_true(transmitted.data.distance.zone_a == 15);
    assert_true(transmitted.data.distance.zone_e == 5);

    assert_true(transmitted.data.status.zone_a == 1);
    assert_true(transmitted.data.status.zone_e == 2);

    event.as_can_message.msg.can_data.byte0 = 0x98;
    event.as_can_message.msg.can_data.byte2 = 0x0;

    expect_value(__wrap_alpine_menu_parking_sensors_type, type, PARKING_SENSOR_TYPE_REAR);
    expect_function_call(__wrap_event_parking_sensor_emit);

    handle_parksens(&event);
    assert_true(old_act_value == 0x00);

    assert_true(transmitted.data.distance.zone_a == 15);
    assert_true(transmitted.data.distance.zone_e == 0);

    assert_true(transmitted.data.status.zone_a == 1);
    assert_true(transmitted.data.status.zone_e == 3);
}

void test_handle_second_changed(void **setup) {
    (void)setup;

    union EVENT event;
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    tx_enable_counter = 4;

    handle_second_changed(&event);
    assert_true(tx_enable_1sec == true);
    assert_true(tx_enable_counter == 3);

    tx_enable_counter = 0;

    handle_second_changed(&event);
    assert_true(tx_enable_1sec == true);
    assert_true(tx_enable_counter == 0);
}

void test_normalize_value(void **setup) {
    (void)setup;
    assert_true(normalize_value(10) == 0);
    assert_true(normalize_value(5) == 0);
    assert_true(normalize_value(0) == 0);
    assert_true(normalize_value(0x0D) == 0);
    assert_true(normalize_value(0x0E) == 0);
    assert_true(normalize_value(20) == 2);
    assert_true(normalize_value(30) == 5);
    assert_true(normalize_value(40) == 8);
    assert_true(normalize_value(50) == 11);
    assert_true(normalize_value(60) == 14);
    assert_in_range(normalize_value(0x0E), 0x00, 0x02);
    assert_in_range(normalize_value(0x3E), 0x0E, 0x0F);
}

void test_parking_sensor_calc_critical(void **setup) {
    (void)setup;
    assert_true(parking_sensor_calc_critical(0) == 3);
    assert_true(parking_sensor_calc_critical(4) == 2);
    assert_true(parking_sensor_calc_critical(15) == 1);
}

void test_peugeot_208_parksens_init(void **setup) {
    (void)setup;

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_parksens);

    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_second_changed);

    expect_function_call(__wrap_set_vehicle_without_parking_sensor_buzzer);

    peugeot_208_parksens_init();
}

int setup_func(void **state) {
    (void)state;
    struct EVENT_PARKING_SENSOR prk = {0};
    memcpy(&old_prk, &prk, sizeof(struct EVENT_PARKING_SENSOR));
    old_act_value = 0x20;
    parksensor_counter = 0;
    tx_enable_1sec = false;
    tx_enable_counter = 0;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_handle_parksens_once_second, setup_func),
        cmocka_unit_test_setup(test_handle_parksens, setup_func),
        cmocka_unit_test_setup(test_handle_second_changed, setup_func),
        cmocka_unit_test_setup(test_peugeot_208_parksens_init, setup_func),
        cmocka_unit_test_setup(test_normalize_value, setup_func),
        cmocka_unit_test_setup(test_parking_sensor_calc_critical, setup_func),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}