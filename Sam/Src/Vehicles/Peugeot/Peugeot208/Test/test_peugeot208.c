#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "controller.h"
#include "string.h"

#include <cmocka.h>
#include "peugeot208.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void event_out_service_emit(void) {
}

void __wrap_software_reset(void) {
    function_called();
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

void __wrap_vehicle_set_sync_clock(bool val) {
    check_expected(val);
}

void __wrap_wisker_reset(MODULES module) {
    check_expected(module);
}

void __wrap_peugeot208_menu_init(void) {
    function_called();
}

void __wrap_wisker_disable_module(MODULES module) {
    check_expected(module);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_can0_tx_message_emit(const CAN_MESSAGE *const msg) {
    (void)msg;
    function_called();
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status, enum HANDBRAKE_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    check_expected(can_conf->can0.can_speed);
    check_expected(can_conf->can0.listen_mode);
}

void __wrap_canbus0_enable_tx(void) {
    function_called();
}

void __wrap_swc_init(void) {
    function_called();
}

void __wrap_alpine_menu_disable_buttons_lines(void) {
    function_called();
}

void __wrap_alpine_menu_disable_ignition_logic(void) {
    function_called();
}

void __wrap_alpine_menu_disable_clock_management(void) {
    function_called();
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_peugeot_208_parksens_init(void) {
    function_called();
}

void __wrap_event_swc_emit(enum SWC status, enum SWC_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void __wrap_peugeot_208_2018_swc_touch(void) {
    function_called();
}

void __wrap_peugeot_208_2018_swc_single_din(void) {
    function_called();
}

//void event_controller_emit(struct CONTROLLER_DATA *controller) {
//    (void)controller;
//}

void test_ignition(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0x18;
    event.as_can_message.msg.can_data.byte2 = 0x00;
    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    expect_value(__wrap_vehicle_set_sync_clock, val, false);
    handle_ignition(&event);
    event.as_can_message.msg.can_data.byte2 = 0x02;
    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_ignition(&event);

    event.as_can_message.msg.can_data.byte2 = 0x00;
    wired_ignition_0 = IGNITION_ON;
    wired_ignition_1 = IGNITION_OFF;
    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_ignition(&event);

    event.as_can_message.msg.can_data.byte2 = 0x00;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_ON;
    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_ignition(&event);
}

void test_wired_ignition(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_OFF;

    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    expect_value(__wrap_vehicle_set_sync_clock, val, false);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    wired_ignition_0 = IGNITION_OFF;

    event.as_wired_ignition.module = 1;
    event.as_wired_ignition.ignition = IGNITION_OFF;

    expect_value(__wrap_vehicle_set_sync_clock, val, false);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);
}

void test_revmov(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0xF6;
    event.as_can_message.msg.can_data.byte7 = 0x00;
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_revmov(&event);
    event.as_can_message.msg.can_data.byte7 = 0x80;
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);
}

void test_lights(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0x128;
    event.as_can_message.msg.can_data.byte0 = 0x80;
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    handle_lights(&event);
    event.as_can_message.msg.can_data.byte0 = 0x81;
    expect_value(__wrap_event_lights_emit, status, LIGHTS_ON);
    handle_lights(&event);
}

void test_speed(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0xB6;
    event.as_can_message.msg.can_data.byte2 = 0x01;
    event.as_can_message.msg.can_data.byte3 = 0x10;
    expect_value(__wrap_event_speed_emit, status, 2);
    handle_speed(&event);
    event.as_can_message.msg.can_data.byte2 = 0x10;
    event.as_can_message.msg.can_data.byte3 = 0x10;
    expect_value(__wrap_event_speed_emit, status, 42);
    handle_speed(&event);
}

void test_handbrake(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_id = 0x128;
    event.as_can_message.msg.can_data.byte3 = 0x00;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    expect_value(__wrap_event_handbrake_emit, module, HANDBRAKE_MODULE_VEHICLE);
    handle_handbrake(&event);
    event.as_can_message.msg.can_data.byte3 = 0x02;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_handbrake_emit, module, HANDBRAKE_MODULE_VEHICLE);
    handle_handbrake(&event);
}

//void test_parking_sensors(void **setup) {
//    (void)setup;
//    union EVENT event;
//
//    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
//    event.as_can_message.msg.interface = CAN_INTERFACE_0;
//    event.as_can_message.msg.can_id = 0xE1;
//    event.as_can_message.msg.can_data.byte3 = 0x00;
//    handle_parking_sensors(&event);
//    event.as_can_message.msg.can_data.byte3 = 0x02;
//    handle_parking_sensors(&event);
//}

void test_peugeot208_init(void **setup) {
    (void)setup;

    expect_value(__wrap_canbus_init, can_conf->can0.can_speed, 125);
    expect_value(__wrap_canbus_init, can_conf->can0.listen_mode, false);

    expect_function_call(__wrap_canbus0_enable_tx);
    expect_function_call(__wrap_swc_init);

    expect_function_call(__wrap_alpine_menu_disable_buttons_lines);
    expect_function_call(__wrap_alpine_menu_disable_ignition_logic);
    expect_function_call(__wrap_alpine_menu_disable_clock_management);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x18);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x128);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0xF6);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0xB6);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x21F);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0xA2);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x1A1);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x0E1);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x1A9);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x260);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x268);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x329);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_ignition);

    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_revmov);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_lights);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_speed);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_handbrake);

    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_HANDBRAKE_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_initial_configuration);

    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_type);

    expect_value(__wrap_event_connect_callback, event, eEVENT_PHONE_INFO);
    expect_value(__wrap_event_connect_callback, handler, handle_phone_info);

    expect_value(__wrap_event_connect_callback, event, eEVENT_SETTINGS_READ);
    expect_value(__wrap_event_connect_callback, handler, handle_config_setup);

    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);

    expect_function_call(__wrap_peugeot_208_parksens_init);

    peugeot208_init();
}

void test_configuration(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_HANDBRAKE_CHANGED;
    event.as_wired_handbrake.handbrake = HANDBRAKE_OFF;

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, peugeot_208_2018_swc_single_din);

    handle_initial_configuration(&event);
    assert_true(conf_init == 0x02);

    conf_init = 0xFF;
    event.as_wired_handbrake.handbrake = HANDBRAKE_ON;
    expect_function_call(__wrap_peugeot208_menu_init);
    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_acknowledge);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, peugeot_208_2018_swc_touch);

    handle_initial_configuration(&event);
    assert_true(conf_init == 0x01);

    event.as_wired_handbrake.handbrake = HANDBRAKE_OFF;
    //expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    //expect_value(__wrap_event_connect_callback, handler, peugeot_208_2018_swc_single_din);
    expect_function_call(__wrap_software_reset);
    handle_initial_configuration(&event);
    assert_true(conf_init == 0x02);
}

void test_acknowledge(void **state) {
    (void)state;
    union EVENT second;

    second.as_generic.event = eEVENT_SECONDS_CHANGED;
    handle_acknowledge(&second);
    assert_true(old_vehicle_ignition == IGNITION_OFF);

    can_ignition = IGNITION_ON;
    expect_function_calls(__wrap_event_can0_tx_message_emit, 9);
    handle_acknowledge(&second);
    assert_true(old_vehicle_ignition == IGNITION_ON);

    expect_function_calls(__wrap_event_can0_tx_message_emit, 4);
    handle_acknowledge(&second);
    assert_true(old_vehicle_ignition == IGNITION_ON);

    can_ignition = IGNITION_OFF;
    handle_acknowledge(&second);
    assert_true(old_vehicle_ignition == IGNITION_OFF);
}

static int setup_func(void **state) {
    (void)state;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;
    can_ignition = IGNITION_OFF;
    vehicle_ignition = IGNITION_OFF;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();

    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_ignition, setup_func),
        cmocka_unit_test_setup(test_wired_ignition, setup_func),
        cmocka_unit_test_setup(test_revmov, setup_func),
        // cmocka_unit_test_setup(test_swc, setup_func),
        cmocka_unit_test_setup(test_lights, setup_func),
        cmocka_unit_test_setup(test_speed, setup_func),
        cmocka_unit_test_setup(test_handbrake, setup_func),
        cmocka_unit_test_setup(test_peugeot208_init, setup_func),
        cmocka_unit_test_setup(test_configuration, setup_func),
        cmocka_unit_test_setup(test_acknowledge, setup_func),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}