
cmake_minimum_required(VERSION 3.0)

if (CMAKE_CROSSCOMPILING)
	
else()
	add_subdirectory(Test)
endif()	


SET(LIBRARY_NAME peugeot_208)


set(SOURCES
    peugeot208.c
    peugeot208_parksens.c
    peugeot208_menu.c
    peugeot208_eep.c
)

add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries (${LIBRARY_NAME} eventlist wisker)
target_compile_definitions(${LIBRARY_NAME} PUBLIC REVERSE_ON_FLASHLIGHT=1)
        
target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)
