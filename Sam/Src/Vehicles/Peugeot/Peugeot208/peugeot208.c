#include "peugeot208.h"
#include "alpine_menu.h"
#include "event_list.h"
#include "events.h"
#include "main.h"
#include "peugeot208_priv.h"
#include "wisker.h"

#include "buzzer3.h"
#include "canbus.h"
#include "logger.h"

static enum IGNITION_STATUS can_ignition = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;
static enum IGNITION_STATUS vehicle_ignition = IGNITION_OFF;

static void event_peugeot_208_ignition_emit(void) {
    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON) ||
        (can_ignition == IGNITION_ON)) {
        vehicle_ignition = IGNITION_ON;
    } else {
        vehicle_ignition = IGNITION_OFF;
        vehicle_set_sync_clock(false);
    }
    event_ignition_emit(vehicle_ignition);
}

bool is_vehicle_can_ignition_on(void) {
    return (can_ignition == IGNITION_ON);
}

static void handle_ignition(const union EVENT* const msg) {
    const uint32_t id = 0x18;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            wisker_reset(MODULE_VEHICLE);
            if ((msg->as_can_message.msg.can_data.byte2 & 0x02) == 0x02) {
                can_ignition = IGNITION_ON;
            } else {
                can_ignition = IGNITION_OFF;
            }
            event_peugeot_208_ignition_emit();
        }
    }
}

static void handle_wired_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_peugeot_208_ignition_emit();
    }
}

static void handle_handbrake(const union EVENT* const msg) {
    const uint32_t id = 0x128;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte3 & 0x02) == 0x02) {
                event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_VEHICLE);
            } else {
                event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_VEHICLE);
            }
        }
    }
}

static void handle_lights(const union EVENT* const msg) {
    const uint32_t id = 0x128;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if (msg->as_can_message.msg.can_data.byte0 > 0x80) {
                event_lights_emit(LIGHTS_ON);
            } else {
                event_lights_emit(LIGHTS_OFF);
            }
        }
    }
}

static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0xF6;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte7 & 0x80) == 0x80) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}

static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0xB6;
    uint32_t tmp = 0;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            tmp = ((msg->as_can_message.msg.can_data.byte2 << 8) + msg->as_can_message.msg.can_data.byte3) / 96;
            event_speed_emit(tmp);
        }
    }
}

static inline int __attribute__((const)) is_increasing(uint8_t value, uint8_t old_value);
static inline int is_increasing(uint8_t value, uint8_t old_value) {
    int8_t retVal;
    retVal = 0;
    if (value != old_value) {
        //il conteggio incrementa, ha superato 0x7F (overflow) e è tornato a incrementare a partire da 0
        if ((old_value > 0x60) && (old_value <= 0x7f) && (value > 0x20)) {
            retVal = (-1) * (0x7F - old_value + value);
            //il conteggio decrementa, è sceso sotto 0x80 (underflow) e è tornato a decrementare a partire da 0xFF
        } else if ((old_value < 0xA0) && (old_value >= 0x80) && (value == 0x00)) {
            retVal = (-1) * (0x80 - old_value);
            //il conteggio decrementa, è sceso sotto 0x80 (underflow) e è tornato a decrementare a partire da 0xFF
        } else if ((old_value < 0xA0) && (old_value >= 0x80) && (value > 0xD0)) {
            retVal = (-1) * ((0x80 - old_value) + (value - 0xFF));
            //il conteggio sta variando lontano da over/underflow
        } else {
            retVal = (-1) * (value - old_value);
        }
    }
    return retVal;
}

static enum PHONE_STATUS phone_status = PHONE_STATUS_INACTIVE;
static enum RADIO_SOURCE_TYPE radio_source = RADIO_SOURCE_OFF;
static enum SWC swc_id_0x21F = SWC_NO_BUTTON_PRESSED;
static enum SWC swc_id_0xA2 = SWC_NO_BUTTON_PRESSED;
static bool swc_active_id_0x21F = false;

static void peugeot_208_2018_swc_single_din(const union EVENT* const msg);
static void peugeot_208_2018_swc_single_din(const union EVENT* const msg) {
    const uint32_t id = 0x21F;
    static uint8_t old_seek = 0x00;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            //controllo i seek
            int ret = is_increasing(msg->as_can_message.msg.can_data.byte1, old_seek);
            if (ret > 0) {
                // LOG_DEF_NORMAL("SEEK DOWN\r\n");
                swc_id_0x21F = SWC_SEEK_DOWN_PRESSED;
                swc_active_id_0x21F = true;
            } else if (ret < 0) {
                // LOG_DEF_NORMAL("SEEK UP\r\n");
                swc_id_0x21F = SWC_SEEK_UP_PRESSED;
                swc_active_id_0x21F = true;
                //Mute non richiesto da Peter Van Mol
                // }else if ((msg->as_can_message.msg.can_data.byte0 & 0x0C)== 0x0C){
                // 	//Mute va prima dei volumi...
                //     // LOG_DEF_NORMAL("MUTE\r\n");
                // 	swc_id_0x21F = SWC_MUTE_PRESSED;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x08) == 0x08) {
                // LOG_DEF_NORMAL("Vol+\r\n");
                swc_id_0x21F = SWC_VOL_UP_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x04) == 0x04) {
                // LOG_DEF_NORMAL("Vol-\r\n");
                swc_id_0x21F = SWC_VOL_DOWN_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x80) == 0x80) {
                // LOG_DEF_NORMAL("PRESET+\r\n");
                swc_id_0x21F = SWC_PRESET_UP_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x40) == 0x40) {
                // LOG_DEF_NORMAL("PRESET-\r\n");
                swc_id_0x21F = SWC_PRESET_DOWN_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x02) == 0x02) {
                // LOG_DEF_NORMAL("SOURCE / SPEECH\r\n");
                swc_id_0x21F = SWC_SOURCE_PRESSED;
                swc_active_id_0x21F = true;
            } else if (msg->as_can_message.msg.can_data.byte2 == 0x40) {
                // LOG_DEF_NORMAL("SOURCE\r\n");
                swc_id_0x21F = SWC_SOURCE_PRESSED;
                swc_active_id_0x21F = true;
            } else if (msg->as_can_message.msg.can_data.byte2 == 0x80) {
                //tasto ok
                // LOG_DEF_NORMAL("PRESET UP\r\n");
                swc_id_0x21F = SWC_PRESET_UP_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x01) == 0x01) {
                // LOG_DEF_NORMAL("TEL DIAL HANGUP\r\n");

                if (radio_source == RADIO_SOURCE_PHONE) {
                    if (phone_status == PHONE_STATUS_RINGING_INCOMING) {
                        swc_id_0x21F = SWC_TEL_PICKUP_PRESSED;
                    } else {
                        swc_id_0x21F = SWC_TEL_HANGUP_PRESSED;
                    }
                } else {
                    swc_id_0x21F = SWC_SPEECH_PRESSED;
                }
                swc_active_id_0x21F = true;
            } else {
                if (swc_active_id_0x21F) {
                    swc_id_0x21F++;
                    swc_active_id_0x21F = false;
                } else {
                    swc_id_0x21F = SWC_NO_BUTTON_PRESSED;
                }
            }
            old_seek = msg->as_can_message.msg.can_data.byte1;
        }

        event_swc_emit(swc_id_0x21F, SWC_MODULE_VEHICLE);
    }
}

static int16_t accu_seek = 0;
static int8_t accu_vols = 0;
static bool swc_active_id_0xA2 = false;
static void peugeot_208_2018_swc_touch(const union EVENT* const msg);
static void peugeot_208_2018_swc_touch(const union EVENT* const msg) {
    const uint32_t id = 0x21F;
    const uint32_t id2 = 0xA2;
    static uint8_t old_seek = 0x00;
    static uint8_t old_vol = 0x00;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            //controllo i seek
            int ret = is_increasing(msg->as_can_message.msg.can_data.byte1, old_seek);
            if (accu_seek < 0) {
                // LOG_DEF_NORMAL("SEEK DOWN\r\n");
                swc_id_0x21F = SWC_SEEK_DOWN_PRESSED;
                accu_seek++;
                swc_active_id_0x21F = true;
            } else if (accu_seek > 0) {
                // LOG_DEF_NORMAL("SEEK UP\r\n");
                swc_id_0x21F = SWC_SEEK_UP_PRESSED;
                accu_seek--;
                swc_active_id_0x21F = true;
            } else if (msg->as_can_message.msg.can_data.byte2 == 0x40) {
                // LOG_DEF_NORMAL("SOURCE/SPEECH\r\n");
                swc_id_0x21F = SWC_SOURCE_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x0C) == 0x0C) {
                // LOG_DEF_NORMAL("MUTE\r\n");
                swc_id_0x21F = SWC_MUTE_PRESSED;
                swc_active_id_0x21F = true;
            } else if (msg->as_can_message.msg.can_data.byte2 == 0x80) {
                //tasto ok
                // LOG_DEF_NORMAL("PRESET UP\r\n");
                swc_id_0x21F = SWC_PRESET_UP_PRESSED;
                swc_active_id_0x21F = true;
            } else if ((msg->as_can_message.msg.can_data.byte0 & 0x01) == 0x01) {
                // LOG_DEF_NORMAL("TEL DIAL HANGUP\r\n");

                if (radio_source == RADIO_SOURCE_PHONE) {
                    if (phone_status == PHONE_STATUS_RINGING_INCOMING) {
                        swc_id_0x21F = SWC_TEL_PICKUP_PRESSED;
                    } else {
                        swc_id_0x21F = SWC_TEL_HANGUP_PRESSED;
                    }
                } else {
                    swc_id_0x21F = SWC_SPEECH_PRESSED;
                }
                swc_active_id_0x21F = true;
            } else {
                if (swc_active_id_0x21F) {
                    swc_id_0x21F++;
                    swc_active_id_0x21F = false;
                } else {
                    swc_id_0x21F = SWC_NO_BUTTON_PRESSED;
                }
                //swc_id_0x21F = SWC_NO_BUTTON_PRESSED;
            }
            old_seek = msg->as_can_message.msg.can_data.byte1;

            if (((ret < 0) && (accu_seek > 0)) || ((ret > 0) && (accu_seek < 0))) {
                accu_seek = 0;
            } else {
                accu_seek += ret;
                if (accu_seek > 10) {
                    accu_seek = 10;
                } else if (accu_seek < -10) {
                    accu_seek = -10;
                }
            }
        }

        if (msg->as_can_message.msg.can_id == id2) {
            static int16_t remaining_steps = 0;
            int steps = (is_increasing(msg->as_can_message.msg.can_data.byte5, old_vol));
            if (((steps < 0) && (remaining_steps > 0)) || ((steps > 0) && (remaining_steps < 0))) {
                remaining_steps = 0;
            } else {
                remaining_steps += steps;
                if (remaining_steps > 10) {
                    remaining_steps = 10;
                } else if (remaining_steps < -10) {
                    remaining_steps = -10;
                }
            }

            if (remaining_steps > 0) {
                // LOG_DEF_NORMAL("VOL UP\r\n");
                swc_id_0xA2 = SWC_VOL_UP_PRESSED;
                swc_active_id_0xA2 = true;
                remaining_steps--;
            } else if (remaining_steps < 0) {
                // LOG_DEF_NORMAL("VOL DOWN\r\n");
                swc_id_0xA2 = SWC_VOL_DOWN_PRESSED;
                swc_active_id_0xA2 = true;
                remaining_steps++;
            } else {
                if (swc_active_id_0xA2) {
                    swc_id_0xA2++;
                    swc_active_id_0xA2 = false;
                } else {
                    swc_id_0xA2 = SWC_NO_BUTTON_PRESSED;
                }
            }
            old_vol = msg->as_can_message.msg.can_data.byte5;
            if (accu_vols > 0) {  //utilizzare steps per aggiornare accu_vols
                accu_vols--;
            } else if (accu_vols < 0) {
                accu_vols++;
            }
        }

        if (swc_id_0x21F != SWC_NO_BUTTON_PRESSED) {
            event_swc_emit(swc_id_0x21F, SWC_MODULE_VEHICLE);
        } else if (swc_id_0xA2 != SWC_NO_BUTTON_PRESSED) {
            event_swc_emit(swc_id_0xA2, SWC_MODULE_VEHICLE);
        } else {
            event_swc_emit(SWC_NO_BUTTON_PRESSED, SWC_MODULE_VEHICLE);
        }
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source = msg->as_radio_source.source_type;
        LOG_DEF_NORMAL("**NIS source type: %02X\r\n", radio_source);
    }
}

static void handle_phone_info(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_PHONE_INFO) {
        phone_status = msg->as_phone_info.bits.as_struct.phone_status;
        LOG_DEF_NORMAL("**NIS phone status: %02X\r\n", phone_status);
    }
}

static enum IGNITION_STATUS old_vehicle_ignition = IGNITION_OFF;
static void handle_acknowledge(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (is_vehicle_can_ignition_on()) {
            if (old_vehicle_ignition != can_ignition) {
                {
                    const CAN_MESSAGE message = {0x1C, false, {0x10, 0x04, 0x1F, 0x32, 0x46, 0x5A, 0x6E, 0x82}, 8, CAN_INTERFACE_0};
                    event_can0_tx_message_emit(&message);
                }
                {
                    const CAN_MESSAGE message = {0x1C, false, {0x10, 0x04, 0x1F, 0x32, 0x46, 0x5A, 0x6E, 0x82}, 8, CAN_INTERFACE_0};
                    event_can0_tx_message_emit(&message);
                }
                {
                    const CAN_MESSAGE message = {0x229, false, {0x02, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0};
                    event_can0_tx_message_emit(&message);
                }
                {
                    const CAN_MESSAGE message = {0x269, false, {0x30, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0};
                    event_can0_tx_message_emit(&message);
                }
                {
                    const CAN_MESSAGE message = {0x2DC, false, {0x30, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0};
                    event_can0_tx_message_emit(&message);
                }
            }
            {
                const CAN_MESSAGE message = {0x129, false, {0x00, 0x00, 0x00, 0xFF, 0xFF, 0x02, 0xF8, 0x00}, 7, CAN_INTERFACE_0};
                event_can0_tx_message_emit(&message);
            }
            {
                const CAN_MESSAGE message = {0x264, false, {0x00, 0x00, 0xFF, 0xE0, 0x00, 0x00, 0x00, 0x00}, 4, CAN_INTERFACE_0};
                event_can0_tx_message_emit(&message);
            }
            {
                const CAN_MESSAGE message = {0x364, false, {0x00, 0x00, 0x1E, 0x78, 0xE0, 0x00, 0x00, 0x00}, 6, CAN_INTERFACE_0};
                event_can0_tx_message_emit(&message);
            }
            {
                const CAN_MESSAGE message = {0x385, false, {0x00, 0x00, 0x60, 0x07, 0xFF, 0xFF, 0x00, 0x00}, 6, CAN_INTERFACE_0};
                event_can0_tx_message_emit(&message);
            }
        }
        old_vehicle_ignition = can_ignition;
    }
}

static bool configured = false;
static void handle_config_setup(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SETTINGS_READ) {
        if (msg->as_settings_read.type == SETTING_READ_END) {
            if (!configured) {
                configured = true;
                initialize_traction_control_status();
            }
        }
    }
}

static uint8_t conf_init = 0xFF;
static void handle_initial_configuration(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_HANDBRAKE_CHANGED) {
        uint8_t conf = 0x00;
        if (msg->as_wired_handbrake.handbrake == HANDBRAKE_ON) {
            //Configurazione radio touch screen, abilito funzionalità ignition su can
            //Ed abilito il menu veicolo.
            if (conf_init == 0xFF) {
                peugeot208_menu_init();
                event_connect_callback(eEVENT_SECONDS_CHANGED, handle_acknowledge);
                event_connect_callback(eEVENT_CAN0_RX_MESSAGE, peugeot_208_2018_swc_touch);
            }
            conf = 0x01;
        } else {
            conf = 0x02;
            if (conf_init == 0xFF) {
                alpine_menu_disable_vehicle_settings();
                event_connect_callback(eEVENT_CAN0_RX_MESSAGE, peugeot_208_2018_swc_single_din);
            }
        }
        if (conf_init != 0xFF) {
            if (conf_init != conf) {
                software_reset();
            }
        }
        conf_init = conf;
    }
}

void peugeot208_init(void) {
    LOG_DEF_NORMAL("Peugeot 208\r\n");
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 125;
    can_conf.can0.listen_mode = false;
    canbus_init(&can_conf);
    canbus0_enable_tx();
    swc_init();

    alpine_menu_disable_buttons_lines();
    alpine_menu_disable_ignition_logic();
    alpine_menu_disable_clock_management();

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x18);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x128);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xF6);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xB6);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x21F);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xA2);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x1A1);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x0E1);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x1A9);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x260);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x268);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x329);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_ignition);
    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_handbrake);
    event_connect_callback(eEVENT_WIRED_HANDBRAKE_CHANGED, handle_initial_configuration);
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_PHONE_INFO, handle_phone_info);
    event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);
    wisker_reset(MODULE_VEHICLE);

    peugeot_208_parksens_init();
}
