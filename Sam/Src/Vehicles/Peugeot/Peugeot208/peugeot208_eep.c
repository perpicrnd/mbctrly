
#include "peugeot208_priv.h"
#include "settings.h"

bool peugeot_208_traction_control_button_present(void) {
    return settings_read(PEUGEOT_SETTING_TRACTION_CONTROL_BUTTON_PRESENCE);
}

void pegeout_208_set_traction_control_button_presence(bool value) {
    settings_write(PEUGEOT_SETTING_TRACTION_CONTROL_BUTTON_PRESENCE, value);
}