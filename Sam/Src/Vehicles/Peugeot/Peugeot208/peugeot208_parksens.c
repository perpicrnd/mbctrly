#include <string.h>
#include "alpine_task.h"
#include "event_list.h"
#include "peugeot208.h"
#include "radio_message.h"

static inline bool prk_sens_changed(struct EVENT_PARKING_SENSOR *old, struct EVENT_PARKING_SENSOR *curr) {
    bool changed = false;

    changed |= (old->data.distance.zone_a != curr->data.distance.zone_a);
    changed |= (old->data.distance.zone_b != curr->data.distance.zone_b);
    changed |= (old->data.distance.zone_c != curr->data.distance.zone_c);
    changed |= (old->data.distance.zone_d != curr->data.distance.zone_d);
    changed |= (old->data.distance.zone_e != curr->data.distance.zone_e);
    changed |= (old->data.distance.zone_f != curr->data.distance.zone_f);
    changed |= (old->data.distance.zone_g != curr->data.distance.zone_g);
    changed |= (old->data.distance.zone_h != curr->data.distance.zone_h);
    changed |= (old->data.status.zone_a != curr->data.status.zone_a);
    changed |= (old->data.status.zone_b != curr->data.status.zone_b);
    changed |= (old->data.status.zone_c != curr->data.status.zone_c);
    changed |= (old->data.status.zone_d != curr->data.status.zone_d);
    changed |= (old->data.status.zone_e != curr->data.status.zone_e);
    changed |= (old->data.status.zone_f != curr->data.status.zone_f);
    changed |= (old->data.status.zone_g != curr->data.status.zone_g);
    changed |= (old->data.status.zone_h != curr->data.status.zone_h);
    return changed;
}

static inline uint8_t parking_sensor_calc_critical(uint8_t value) {
    if (value < 2) {
        return 3;
    } else if (value < 6) {
        return 2;
    } else {
        return 1;
    }
}

static inline uint8_t normalize_value(uint8_t value) {
    uint8_t retVal = 0x00;
    if (value < 0x0D)
        retVal = 0;
    else {
        retVal = (value - 0x0D) * (0x0F) / (0x3F - 0x0D);
    }
    return retVal;
}

static uint8_t old_act_value = 0x20;
static uint8_t parksensor_counter = 0;
static bool tx_enable_1sec = false;
static uint8_t tx_enable_counter = 0;
static struct EVENT_PARKING_SENSOR old_prk = {0};
static inline void parking_sensor_emit(const CAN_MESSAGE *const message) {
    struct EVENT_PARKING_SENSOR prk = {0};

    prk.data.distance.zone_a = 0x0F;
    prk.data.distance.zone_b = 0x0F;
    prk.data.distance.zone_c = 0x0F;
    prk.data.distance.zone_d = 0x0F;

    prk.data.distance.zone_e = 0x0F;
    prk.data.distance.zone_f = 0x0F;
    prk.data.distance.zone_g = 0x0F;
    prk.data.distance.zone_h = 0x0F;

    uint8_t act_value = message->can_data.byte0 & 0x60;
    uint8_t value;
    if (act_value == 0) {
        if (old_act_value != act_value) {
            LOG_DEF_NORMAL("Attivazione sensori di parcheggio\r\n");
            value = 0;
        } else {
            uint8_t distance = message->can_data.byte2;
            value = normalize_value(distance);  //da 0x0D a 0x3F lo trasformo in 0x00..0x0F
        }

        prk.data.distance.zone_e = value;
        prk.data.distance.zone_f = value;
        prk.data.distance.zone_g = value;
        prk.data.distance.zone_h = value;
    }
    old_act_value = act_value;

    prk.data.status.zone_a = parking_sensor_calc_critical(prk.data.distance.zone_a);
    prk.data.status.zone_b = parking_sensor_calc_critical(prk.data.distance.zone_b);
    prk.data.status.zone_c = parking_sensor_calc_critical(prk.data.distance.zone_c);
    prk.data.status.zone_d = parking_sensor_calc_critical(prk.data.distance.zone_d);
    prk.data.status.zone_e = parking_sensor_calc_critical(prk.data.distance.zone_e);
    prk.data.status.zone_f = parking_sensor_calc_critical(prk.data.distance.zone_f);
    prk.data.status.zone_g = parking_sensor_calc_critical(prk.data.distance.zone_g);
    prk.data.status.zone_h = parking_sensor_calc_critical(prk.data.distance.zone_h);

    prk.data.park_status.as_flags.active = 0;
    if (act_value == 0) {
        parksensor_counter = 2;
    }
    if (parksensor_counter > 0) {
        parksensor_counter--;
        prk.data.park_status.as_flags.active = 1;
        tx_enable_counter = 5;
    }
    prk.prksens_type = PARKING_SENSOR_OEM;

    if (((prk_sens_changed(&old_prk, &prk)) || tx_enable_1sec) && (tx_enable_counter != 0)) {
        event_parking_sensor_emit(&prk);
        memcpy(&old_prk, &prk, sizeof(struct EVENT_PARKING_SENSOR));
        tx_enable_1sec = false;
    }
}

/**
 * Flag messa a true quando si trasmettono i sensori di parcheggio.
 * In questo modo posso inviare un successivo messaggio contenente tutto spento.
 */
static void handle_second_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (tx_enable_counter > 0) {
            tx_enable_counter--;
        }
        tx_enable_1sec = true;
    }
}

static void handle_parksens(const union EVENT *const msg) {
    const uint32_t id = 0xE1;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE *message = &(msg->as_can_message.msg);
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == id) {
                alpine_menu_parking_sensors_type(PARKING_SENSOR_TYPE_REAR);
                parking_sensor_emit(message);
            }
        }
    }
}

void peugeot_208_parksens_init(void) {
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_parksens);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_second_changed);
    set_vehicle_without_parking_sensor_buzzer();
}