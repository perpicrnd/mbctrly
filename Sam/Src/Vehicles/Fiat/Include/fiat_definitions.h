#pragma once

#include <stdint.h>

/**
 * Queste sono le lingue supportate dalla radio testata.
 */
enum FIAT_LANGUAGE{
	FIAT_LANGUAGE_UNKNOWN = -1,
	FIAT_LANGUAGE_GERMAN,
	FIAT_LANGUAGE_ENGLISH,
	FIAT_LANGUAGE_SPANISH,
	FIAT_LANGUAGE_FRENCH,
	FIAT_LANGUAGE_ITALIAN,
	FIAT_LANGUAGE_DUTCH,
	FIAT_LANGUAGE_POLISH,
	FIAT_LANGUAGE_PORTUGUESE,
	FIAT_LANGUAGE_TURKISH,
	FIAT_LANGUAGE_COUNT
};

enum FIAT_CONSUMPTION{
	FIAT_CONSUMPTION_L_100KM 	= 0x01, //Visualizza i consumi come l/100Km (Disponibile solo se impostata unità di misura metro)
	FIAT_CONSUMPTION_KM_L		= 0x00,	 //Visualizza i consumi come Km/l (Disponibile solo se impostata unità di misura metro)
	FIAT_CONSUMPTION_MIL_GAL	= 0x02, //Visualizza i consumi come Miglia a gallone (Disponibile solo se impostata unità di misura miglia
};

enum FIAT_DISTANCE{
	FIAT_DISTANCE_MILES  		= 0x80,	//unità di visualizzazione delle distanze: Miglia
	FIAT_DISTANCE_METERS 		= 0x00, //unità di visualizzazione delle distanze: Metri
};

enum FIAT_TEMPERATURE{
	FIAT_TEMPERATURE_CELSIUS	= 0x00,	//Visualizza la temperatura in °C
	FIAT_TEMPERATURE_FAHRENHEIT	= 0x40, //Visualizza la temperatura in °F
};

enum FIAT_TIME{
	FIAT_TIME_AM_PM 			= 0x02, //Ora visualizzata in formato 12H
	FIAT_TIME_24H				= 0x00, //Ora visualizzata in formato 24H
};

enum FIAT_REVERSE_WITH_DELAY{
	FIAT_REVERSE_WITH_DELAY_NA = 0,
	FIAT_REVERSE_WITH_DELAY_OFF,
	FIAT_REVERSE_WITH_DELAY_ON
};

enum FIAT_MUTE_ON_LANE_ASSIST_ALERT{
	FIAT_MUTE_ON_LANE_ASSIST_ALERT_OFF = 0,
	FIAT_MUTE_ON_LANE_ASSIST_ALERT_ON,
};

enum FIAT_DAILY_LIGHT{
	FIAT_DAILY_LIGHT_OFF = 0,
	FIAT_DAILY_LIGHT_ON,
};

enum FIAT_LIGHT_SENS_LEVEL{
	FIAT_LIGHT_SENS_LEVEL3 = 0,
	FIAT_LIGHT_SENS_LEVEL2,
	FIAT_LIGHT_SENS_LEVEL1,
	FIAT_LIGHT_SENS_LEVEL0
};

/**
 * La lingua viene trasmessa in due byte, per convenienza salvo entrambi i byte in una struttura per poter accedere
 * facilmente ad entrambi.
 */
struct FIAT_LANGUAGE_STR{
	uint8_t byte0;
	uint8_t byte1;
};

uint16_t year_hex_to_dec(uint8_t byte0, uint8_t byte1);
uint8_t hex_to_dec(uint8_t byte0);