#include <stdio.h>

#include "FreeRTOS.h"
#include "accessory_uart.h"
#include "canbus.h"
#include "event_list.h"
#include "logger.h"
#include "timers.h"
#include "wisker.h"

typedef enum {
    DOOR_UNKNOWN = 0,
    DOOR_OPEN,
    DOOR_CLOSE
} DOOR;

/**
 * @brief Valore definito per un ignition sicuramente assente
 */
#define NEVER_SAW_IGNITION (uint32_t) - 1
/**
 * @brief Timestamp di quando e' stata vista l'ultima volta l'ignition
 */
static uint32_t last_seen_ign_on = NEVER_SAW_IGNITION;
/**
 * @brief variabile contenente l'ultimo tick di sistema
 */
static uint32_t last_tick = 0;
/**
 * @brief stato delle portiere del veicolo
 */
static DOOR door = DOOR_UNKNOWN;
/**
 * @brief l'ultima ignition verificata
 */
static enum IGNITION_STATUS ignition = IGNITION_OFF;

/**
 * @brief l'ultima ignition filare verificata
 */
static enum IGNITION_STATUS wired_ignition = IGNITION_OFF;

/**
 * @brief Il numero di secondi in cui il veicolo rimane acceso prima che si apra la portiera.
 */
#define IGNITION_ON_TIME 30 * 60
static void handle_ignition(const union EVENT* const msg) {
    bool process_ignition = false;
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        if (msg->as_ignition.ignition == IGNITION_ON) {
            wired_ignition = IGNITION_ON;
            last_seen_ign_on = last_tick;
            wisker_reset(MODULE_VEHICLE);
        } else {
            wired_ignition = IGNITION_OFF;
        }
        process_ignition = true;
    } else if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        ///Verifica messaggio temporizzato
        last_tick = msg->as_timestamp.timestamp;
        if (ignition == IGNITION_ON || wired_ignition == IGNITION_ON) {
            last_seen_ign_on = msg->as_timestamp.timestamp;
        }
        process_ignition = true;
    } else if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        ///Verifica messaggio can
        const CAN_MESSAGE* const message = &msg->as_can_message.msg;
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == 0x06214000) {
                wisker_reset(MODULE_VEHICLE);
                ///Verifica del sottochiave
                if ((message->can_data.byte2 & 0x40) == 0x40) {
                    ignition = IGNITION_ON;
                    last_seen_ign_on = last_tick;
                } else {
                    ignition = IGNITION_OFF;
                }
                ///Verifica delle portiere
                if ((message->can_data.byte1 & 0x04) == 0x04) {
                    door = DOOR_OPEN;
                } else {
                    door = DOOR_CLOSE;
                }
            }
        }
        process_ignition = true;
    }

    ///Prendo il minestrone per generare l'evento.
    if (process_ignition) {
        if (last_seen_ign_on == NEVER_SAW_IGNITION) {
            /// Se last_seen_ign_on e' -1 o non è mai arrivata un ignition o e' scaduto il tempo di keep alive
            /// in ogni caso e' un ignition off.
            event_ignition_emit(IGNITION_OFF);
        } else {
            if (ignition == IGNITION_ON || wired_ignition == IGNITION_ON) {
                ///Ignition on esplicita.
                event_ignition_emit(IGNITION_ON);
                wisker_reset(MODULE_VEHICLE);
            } else {
                if (door == DOOR_OPEN) {
                    ///Ignition off e porta aperta => ignition off.
                    event_ignition_emit(IGNITION_OFF);
                    ///Resetto la variabile di ignition vista in modo da rientrare sempre dal primo caso.
                    last_seen_ign_on = NEVER_SAW_IGNITION;
                } else {
                    uint32_t time_delta = last_tick - last_seen_ign_on;
                    if (time_delta < IGNITION_ON_TIME) {
                        ///Ignition off e porta aperta prima dello scadere del tempo => ignition on.
                        if ((time_delta % 60) == 0) {
#ifdef __amd64__
                            LOG_DEF_NORMAL("Stay on for %d minutes\r\n", ((IGNITION_ON_TIME - time_delta) / 60));
#else
                            LOG_DEF_INFO("Stay on for %ld minutes\r\n", ((IGNITION_ON_TIME - time_delta) / 60));
#endif
                        }
                        event_ignition_emit(IGNITION_ON);
                        wisker_reset(MODULE_VEHICLE);
                    } else {
                        event_ignition_emit(IGNITION_OFF);
                        last_seen_ign_on = NEVER_SAW_IGNITION;
                    }
                }
            }
        }
    }
}

static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0x04214001;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte7 & 0x04) == 0x04) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}

/**
 * La vettura ha il blue&me integrato
 * Durante lo sviluppo del software e' stato deciso di lasciare il blue&me collegato
 * e di utilizzare il tasto per lo speech per fare tel dial e tel hangup
 * Il tasto tel rimarra' muto entrando nel menu del blue&me (divenuto inutilizzabile)
 */
static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static enum RADIO_SOURCE_TYPE radio_source_type = RADIO_SOURCE_OFF;
static enum PHONE_STATUS phone_status = PHONE_STATUS_INACTIVE;
static void handle_swc(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x6354000;
            if (msg->as_can_message.msg.can_id == id) {
                uint16_t tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp == 0x8000) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x4000) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0040) {
                    if (RADIO_SOURCE_PHONE == radio_source_type) {
                        if (PHONE_STATUS_RINGING_INCOMING == phone_status) {
                            cur_active = SWC_TEL_PICKUP_HANGUP_PRESSED;
                        } else {
                            cur_active = SWC_TEL_HANGUP_PRESSED;
                        }
                    } else {
                        cur_active = SWC_SPEECH_PRESSED;
                    }
                    swc_active = true;
                } else if (tmp == 0x2000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x1000) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0800) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0400) {
                    cur_active = SWC_SOURCE_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
            }
        }
    }
}

//In questo progetto viene utilizzata l'uscita retro per generare le luci in quanto
//bisogna alimentare dei led e mi serve un uscita di potenza.
#if REVERSE_ON_FLASHLIGHT
static bool reverse_sent = false;
#endif
static void handle_lights(const union EVENT* const msg) {
    const uint32_t id = 0x2214000;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x20) == 0x20) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
#if REVERSE_ON_FLASHLIGHT
            if ((msg->as_can_message.msg.can_data.byte1 & 0x10) == 0x10) {
                if (!reverse_sent) {
                    event_reverse_emit(REVERSE_ON);
                    event_reverse_emit(REVERSE_OFF);
                    reverse_sent = true;
                }
            } else {
                reverse_sent = false;
            }
#endif
        }
    }
}

static void handle_handbrake(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE* const message = &msg->as_can_message.msg;
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == 0x06214000) {
                if ((message->can_data.byte0 & 0x20) == 0x20) {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_VEHICLE);
                } else {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_VEHICLE);
                }
            }
        }
    }
}

/*
 * Il documento del cliente dice che i bit 60-48 length 13 sono lo speed.
 * la risoluzione del dato è 1 = 0.00625 km/h
 * accuratezza del dato 2%
 */
static uint32_t old_speed;
static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0x4394000;
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if ((old_speed - tmp) != 0) {
                    event_speed_emit(tmp);
                    old_speed = tmp;
                }
            }
        }
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source_type = msg->as_radio_source.source_type;
        LOG_DEF_NORMAL("Current radio media type: %d\r\n", radio_source_type);
    }
}

static void handle_phone_info(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_PHONE_INFO) {
        phone_status = msg->as_phone_info.bits.as_struct.phone_status;
        LOG_DEF_NORMAL("Current phone status: %d\r\n", phone_status);
    }
}

void fiat_punto_init(void) {
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 50;
    can_conf.can0.listen_mode = false;
    canbus_init(&can_conf);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06214000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04214001);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06284000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x02214000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04394000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06354000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06214000);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_ignition);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_ignition);
    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_ignition);

    event_connect_callback(eEVENT_WIRED_SPEED_CHANGED, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_handbrake);
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_PHONE_INFO, handle_phone_info);
    accessory_uart_init();
}
