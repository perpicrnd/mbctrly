#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>

#include "fiat_punto.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    (void)can_conf;
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS ign) {
    check_expected(ign);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void test_fiat_punto_init() {
    expect_value(__wrap_canbus_add_accepted_id, num, 0x06214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04214001);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x06284000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x02214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04394000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x06354000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x06214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    fiat_punto_init();
}

int setup(void **state) {
    (void)state;
    last_seen_ign_on = NEVER_SAW_IGNITION;
    last_tick = 0;
    door = DOOR_UNKNOWN;
    ignition = IGNITION_OFF;
    wired_ignition = IGNITION_OFF;
    return 0;
}

void test_handle_wired_ignition() {
    union EVENT event;
    //Fornisco e verifico l'ignition ON
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Do l'ignition OFF ma rimane ad ON perché devo aspettare o 30 minuti o l'apertura della portiera.
    event.as_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Invio un dato con l'orario.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 2;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Simulo che siano passati alcuni minuti ma meno dello spegnimento.
    event.as_timestamp.timestamp = 60 * 5;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Simulo che siano passati alcuni minuti ma meno dello spegnimento.
    event.as_timestamp.timestamp = 60 * 29;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Sono passati i 30 minuti. Spengo.
    event.as_timestamp.timestamp = 60 * 60;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);
}

void test_handle_can_ignition_door_open() {
    union EVENT event;

    //Invio un dato con l'orario per inizializzare le variabili.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 2;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);

    //Ignition ON su canbus
    //Fornisco e verifico l'ignition ON
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x06214000, true, {0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Ignition ON con portiera aperta
    CAN_MESSAGE msg1 = {0x06214000, true, {0x00, 0x04, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Invio un dato con l'orario comunque è ignition ON.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 4;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Ignition OFF con portiera chiusa
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x06214000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Ignition OFF con portiera aperta
    CAN_MESSAGE msg3 = {0x06214000, true, {0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg3, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);
}

void test_handle_can_ignition_time_expires() {
    union EVENT event;

    //Invio un dato con l'orario per inizializzare le variabili.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 2;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);

    //Ignition ON su canbus
    //Fornisco e verifico l'ignition ON
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x06214000, true, {0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Ignition OFF con portiera chiusa
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x06214000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Simulo che siano passati alcuni minuti ma meno dello spegnimento.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 60 * 5;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Simulo che siano passati alcuni minuti ma meno dello spegnimento.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 60 * 19;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Simulo che siano passati alcuni minuti ma meno dello spegnimento.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 60 * 29;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_ON);
    handle_ignition(&event);

    //Sono passati i 30 minuti. Spengo.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 60 * 31;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);

    //Sono passati i 30 minuti. Spengo.
    event.as_generic.event = eEVENT_SECONDS_CHANGED;
    event.as_timestamp.timestamp = 60 * 60;
    expect_value(__wrap_event_ignition_emit, ign, IGNITION_OFF);
    handle_ignition(&event);
}

void test_handle_reverse() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);

    CAN_MESSAGE msg1 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_revmov(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);
}

void test_handle_lights() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x2214000, true, {0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_ON);
    handle_lights(&event);

    CAN_MESSAGE msg1 = {0x2214000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    handle_lights(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x2214000, true, {0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_ON);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_lights(&event);
}

void test_handle_speedhandbrake() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x4394000, true, {0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_in_range(__wrap_event_speed_emit, status, 0x1900, 0x2100);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte0 = 0x00;
    expect_value(__wrap_event_speed_emit, status, 0);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    handle_speed(&event);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_punto_init, setup),
        cmocka_unit_test_setup(test_handle_wired_ignition, setup),
        cmocka_unit_test_setup(test_handle_can_ignition_door_open, setup),
        cmocka_unit_test_setup(test_handle_can_ignition_time_expires, setup),
        cmocka_unit_test_setup(test_handle_reverse, setup),
        cmocka_unit_test_setup(test_handle_lights, setup),
        cmocka_unit_test_setup(test_handle_speedhandbrake, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
