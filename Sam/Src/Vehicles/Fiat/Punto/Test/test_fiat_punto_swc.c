#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>

#include "fiat_punto.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

void test_fiat_punto_swc() {
    union EVENT event;

    //Invio volume+
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x6354000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_swc(&event);

    //Rilascio volume+
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Volume -
    event.as_can_message.msg.can_data.byte0 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Mute
    event.as_can_message.msg.can_data.byte0 = 0x20;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek+
    event.as_can_message.msg.can_data.byte0 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek-
    event.as_can_message.msg.can_data.byte0 = 0x08;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Source
    event.as_can_message.msg.can_data.byte0 = 0x04;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Voice
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Attivo il telefono come source sulla radio.
    union EVENT event_source;
    event_source.as_generic.event = eEVENT_RADIO_MEDIA_CHANGED;
    event_source.as_radio_source.source_type = RADIO_SOURCE_PHONE;
    handle_source_type(&event_source);

    //Voice
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Simulo chiamata in arrivo
    union EVENT event_phone;
    event_phone.as_generic.event = eEVENT_PHONE_INFO;
    event_phone.as_phone_info.bits.as_struct.phone_status = PHONE_STATUS_RINGING_INCOMING;
    handle_phone_info(&event_phone);

    //Voice
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_HANGUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_HANGUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Riprovo un comando normale dopo aver resettato le flag di chiamata.
    event_phone.as_phone_info.bits.as_struct.phone_status = PHONE_STATUS_INACTIVE;
    handle_phone_info(&event_phone);
    event_source.as_radio_source.source_type = RADIO_SOURCE_TUNER;
    handle_source_type(&event_source);

    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);
}

int setup(void **state) {
    (void)state;
    last_seen_ign_on = NEVER_SAW_IGNITION;
    last_tick = 0;
    door = DOOR_UNKNOWN;
    ignition = IGNITION_OFF;
    wired_ignition = IGNITION_OFF;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_punto_swc, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
