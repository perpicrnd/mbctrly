
#include <canbus.h>
#include <stdbool.h>
#include <string.h>
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "logger.h"

/**
 * Questo array contiene i messaggi con le risposte da inviare per la configurazione 0.
 */
static const CAN_MESSAGE messages_runtime_conf0[] = {
    {0x6314024, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0},  //0x00
    {0xC3D4000, true, {0x00, 0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 2, CAN_INTERFACE_0},  //0x01
    {0xE094024, true, {0x00, 0x3A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 2, CAN_INTERFACE_0},  //0x02
};
static const CAN_MESSAGE messages_start_conf0[] = {
    {0xA094005, true, {0x00, 0x7D, 0xEF, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0},   //0x03
    {0xA114000, true, {0x00, 0x2D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0},   //0x04
    {0x1E114000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 0, CAN_INTERFACE_0},  //0x05
};

/**
 * @brief flag che indica se bisogna inviare i messaggi all'avvio.
 */
static bool need_to_send_everything = true;

/**
 * @brief Funzione che reagisce ad un messaggio sul can per la configurazione 1.
 * @param msg il messaggio ricevuto sul canbus.
 */
static void fiat_ducato_ack_conf0_1(const union EVENT *const msg) {
    const uint32_t id = 0x1E114003;
    const CAN_MESSAGE *message = &msg->as_can_message.msg;
    if (message->interface == CAN_INTERFACE_0) {
        if (message->can_id == id) {
            CAN_MESSAGE mess;
            memcpy(&mess, message, sizeof(CAN_MESSAGE));
            mess.can_id = 0x1E114024;
            event_can0_tx_message_emit(&mess);
            CAN_MESSAGE msg = {0xE094024, true, {0x00, 0xD3, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0};
            event_can0_tx_message_emit(&msg);
        }
    }
}

/**
 * @brief Funzione che reagisce ad un messaggio sul can per la configurazione 1.
 * @param msg il messaggio ricevuto sul canbus.
 */
static void fiat_ducato_ack_conf0_0(const union EVENT *const msg) {
    const CAN_MESSAGE *message = &msg->as_can_message.msg;
    if (message->interface == CAN_INTERFACE_0) {
        if (message->can_id == 0x6314003) {
            CAN_MESSAGE msg = {0x6314024, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
            event_can0_tx_message_emit(&msg);
        }
    }
}

/**
 * @brief Funzione chiamata nel momento in cui bisogna inviare i messaggi verso il veicolo.
 * @param msg l'evento che ha generato la chiamata della funzione.
 */
void fiat_ducato_handle_can_tx_0(const union EVENT *const msg) {
    if ((msg->as_generic.event == eEVENT_SECONDS_CHANGED)) {
        if (fiat_ducato_can_ignition_raw() == IGNITION_ON) {
            ///Configurazione 0 devo inviare alcuni messaggi.
            if (need_to_send_everything) {
                for (uint8_t i = 0; i < (sizeof(messages_start_conf0) / sizeof(messages_start_conf0[0])); i++) {
                    event_can0_tx_message_emit(&messages_start_conf0[i]);
                }
            }
            for (uint8_t i = 0; i < (sizeof(messages_runtime_conf0) / sizeof(messages_runtime_conf0[0])); i++) {
                event_can0_tx_message_emit(&messages_runtime_conf0[i]);
            }
            ///Sono arrivato in fondo, cancello la flag. Si riattiva solo con l'ignition OFF.
            need_to_send_everything = false;
        } else {
            need_to_send_everything = true;
        }
    }
}

void fiat_ducato_vp2_ack_init(void) {
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x6314003);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x1E114003);
    event_connect_callback(eEVENT_SECONDS_CHANGED, fiat_ducato_handle_can_tx_0);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, fiat_ducato_ack_conf0_0);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, fiat_ducato_ack_conf0_1);
}