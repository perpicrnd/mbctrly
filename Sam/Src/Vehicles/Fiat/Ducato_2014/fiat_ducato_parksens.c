
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "radio_message.h"

static inline uint8_t parking_sensor_calc_critical(uint8_t value) {
    if (value < 2) {
        return 3;
    } else if (value < 6) {
        return 2;
    } else {
        return 1;
    }
}

static uint8_t parksensor_counter = 0;
static inline void parking_sensor_emit(const CAN_MESSAGE *const message) {
    struct EVENT_PARKING_SENSOR prk;
    prk.data.distance.zone_a = 0x0F;
    prk.data.distance.zone_b = 0x0F;
    prk.data.distance.zone_c = 0x0F;
    prk.data.distance.zone_d = 0x0F;
    if ((message->can_data.byte0 & 0xC0) == 0) {
        uint8_t value = message->can_data.byte3;
        if ((value == 0) && ((message->can_data.byte0 & 0x03) == 0)) {
            value = 0x0F;
        }
        prk.data.distance.zone_e = value;
        prk.data.distance.zone_f = value;
        prk.data.distance.zone_g = value;
        prk.data.distance.zone_h = value;
    } else {
        prk.data.distance.zone_e = 0x0F;
        prk.data.distance.zone_f = 0x0F;
        prk.data.distance.zone_g = 0x0F;
        prk.data.distance.zone_h = 0x0F;
    }

    prk.data.status.zone_a = parking_sensor_calc_critical(prk.data.distance.zone_a);
    prk.data.status.zone_b = parking_sensor_calc_critical(prk.data.distance.zone_b);
    prk.data.status.zone_c = parking_sensor_calc_critical(prk.data.distance.zone_c);
    prk.data.status.zone_d = parking_sensor_calc_critical(prk.data.distance.zone_d);
    prk.data.status.zone_e = parking_sensor_calc_critical(prk.data.distance.zone_e);
    prk.data.status.zone_f = parking_sensor_calc_critical(prk.data.distance.zone_f);
    prk.data.status.zone_g = parking_sensor_calc_critical(prk.data.distance.zone_g);
    prk.data.status.zone_h = parking_sensor_calc_critical(prk.data.distance.zone_h);

    prk.data.park_status.as_flags.active = 0;
    if ((message->can_data.byte0 & 0xC0) == 0) {
        parksensor_counter = 2;
    }
    if (parksensor_counter > 0) {
        parksensor_counter--;
        prk.data.park_status.as_flags.active = 1;
    }
    prk.prksens_type = PARKING_SENSOR_OEM;
    event_parking_sensor_emit(&prk);
}

/**
 * Flag messa a true quando si trasmettono i sensori di parcheggio.
 * In questo modo posso inviare un successivo messaggio contenente tutto spento.
 */
static void handle_parksens(const union EVENT *const msg) {
    const uint32_t id = 0x06314018;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE *message = &(msg->as_can_message.msg);
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == id) {
                alpine_menu_parking_sensors_type(PARKING_SENSOR_TYPE_REAR);
                parking_sensor_emit(message);
            }
        }
    }
}

void fiat_ducato_parksens_init(void) {
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_parksens);
}