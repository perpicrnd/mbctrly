#include "buzzer3.h"
#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "logger.h"

static uint8_t radio_volume = 0;
static bool radio_muted_by_me = false;
static void buzzer_with_mute(bool status, bool mute_wanted) {
    buzzer3_set_laneassist_level(status);
    if (fiat_ducato_lane_assist_warning()) {
        if (mute_wanted) {
            if ((!radio_muted_by_me) && (radio_volume != 0)) {
                //Radio non in mute da me.
                //Radio non in mute.
                event_swc_emit(SWC_MUTE_PRESSED, SWC_MODULE_VEHICLE);
                event_swc_emit(SWC_MUTE_RELEASED, SWC_MODULE_VEHICLE);
                radio_muted_by_me = true;
            }
        } else {
            if (radio_volume == 0) {
                if (radio_muted_by_me) {
                    event_swc_emit(SWC_MUTE_PRESSED, SWC_MODULE_VEHICLE);
                    event_swc_emit(SWC_MUTE_RELEASED, SWC_MODULE_VEHICLE);
                    radio_muted_by_me = false;
                }
            }
        }
    }
}

//Pulsante di attivazione / disattivazione
//id 421401E
//
//00 00 00 00 00 74 00 00 Attivo left
//00 00 00 00 00 78 00 00 Attivo right
//00 00 00 40 00 00 00 00 Non attivo
static void handle_laneassist(const union EVENT* const msg) {
    const uint32_t id = 0x421401E;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte3 & 0x40) == 0x40) {
                //Laneassist non attivo.
                buzzer_with_mute(false, false);
            } else {  //Laneassisst attivo
                if ((msg->as_can_message.msg.can_data.byte5 & 0x0C) != 0) {
                    buzzer_with_mute(true, true);
                } else {
                    buzzer_with_mute(false, false);
                }
            }
        }
    }
}

static void handle_media_play(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_VOLUME_LEVEL) {
        LOG_DEF_NORMAL("Volume now at: %d\r\n", msg->as_radio_volume.volume_level);
        radio_volume = msg->as_radio_volume.volume_level;
    }
}

void fiat_ducato_laneassist_init(void) {
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x421401E);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_laneassist);
    event_connect_callback(eEVENT_VOLUME_LEVEL, handle_media_play);
}