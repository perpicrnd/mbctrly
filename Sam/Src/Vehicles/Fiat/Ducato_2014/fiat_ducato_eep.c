
#include "fiat_definitions.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "settings.h"

enum FIAT_MUTE_ON_LANE_ASSIST_ALERT fiat_ducato_lane_assist_warning(void) {
    return settings_read(FIAT_SETTING_MUTE_LANEASSIST);
}

void fiat_ducato_set_lane_assist_warning(bool value) {
    settings_write(FIAT_SETTING_MUTE_LANEASSIST, value);
}