#pragma once

#include <stdint.h>

#include <stdbool.h>
#include "fiat_definitions.h"

struct CANBUS_CONFIG;

/**
 * @brief inizializzazione di Fiat Ducato.
 * @param config il numero di configurazione altera il funzionamento del software. 0 => VP2, 1 => VP1
 *
 */
void fiat_ducato_init(struct CANBUS_CONFIG *can_conf);
