#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"

static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static enum RADIO_MEDIA_TYPE radio_media_type = RADIO_MEDIA_UNKNOWN;

static void handle_swc(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x6284000;
            if (msg->as_can_message.msg.can_id == id) {
                uint16_t tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp == 0x8000) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x4000) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x2000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x1000) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0800) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0400) {
                    cur_active = SWC_SOURCE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0200) {
                    if (radio_media_type == RADIO_MEDIA_PHONE) {
                        cur_active = SWC_TEL_PICKUP_PRESSED;
                    } else {
                        cur_active = SWC_SPEECH_PRESSED;
                    }
                    swc_active = true;
                } else if (tmp == 0x0100) {
                    if (radio_media_type == RADIO_MEDIA_PHONE) {
                        cur_active = SWC_TEL_HANGUP_PRESSED;
                    } else {
                        cur_active = SWC_SOURCE_PRESSED;
                    }
                    swc_active = true;
                } else if (tmp == 0x0040) {
                    cur_active = SWC_SPEECH_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
            }
        }
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_media_type = msg->as_radio_source.media_type;
    }
}

void fiat_ducato_canswc_init(void) {
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x6284000);
}