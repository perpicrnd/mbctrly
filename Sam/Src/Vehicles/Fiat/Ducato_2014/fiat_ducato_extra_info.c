#include <stdbool.h>

#include "fiat_definitions.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"

#include "alpine_menu.h"
#include "can_message.h"
#include "event_list.h"
#include "vehicle.h"

/**
 * Codici delle lingue. Ogni codice è in posizione corretta rispetto alla enum LANGUAGE
 */

static const struct FIAT_LANGUAGE_STR language_str[] = {
    {0x00, 0x20},  //LANGUAGE_GERMAN
    {0x00, 0x40},  //LANGUAGE_ENGLISH
    {0x00, 0x40},  //LANGUAGE_ENGLISH US
    {0x00, 0x80},  //LANGUAGE_FRENCH
    {0x00, 0x00},  //LANGUAGE_ITALIAN
    {0x00, 0x60},  //LANGUAGE_SPANISH
    {0x00, 0xA0},  //LANGUAGE_PORTUGUESE
    {0x00, 0xC0},  //LANGUAGE_POLISH
    {0x00, 0x40},  //LANGUAGE_ENGLISH CZEC,
    {0x00, 0x40},  //LANGUAGE_ENGLISH SWEDISH,
    {0x00, 0xE0},  //LANGUAGE_DUTCH
    {0x00, 0x40},  //LANGUAGE_ENGLISH JAPANESE,
    {0x00, 0x40},  //LANGUAGE_ENGLISH RUSSIAN,
    {0x00, 0x40},  //LANGUAGE_ENGLISH KOREAN,
    {0x01, 0x20},  //LANGUAGE_TURKISH
    {0x00, 0x40},  //LANGUAGE_ENGLISH CHINESE
    {0x00, 0x40},  //LANGUAGE_ENGLISH NORWEGIAN
    {0x00, 0x40},  //LANGUAGE_ENGLISH SLOVAKIAN
    {0x00, 0x40},  //LANGUAGE_ENGLISH HUNGARIAN
};

static enum FIAT_DISTANCE distance = FIAT_DISTANCE_METERS;                 //Variabile che memorizza quale visualizzazione usare per la distanza.
static enum LANGUAGE_DEFINITION current_language = LANGUAGE_ENGLISH;       //Variabile contenente la lingua attuale.
static enum FIAT_CONSUMPTION current_consumption = FIAT_CONSUMPTION_KM_L;  //Variabile contenente il tipo di visualizzazione dei consumi
static enum FIAT_TIME time_form = FIAT_TIME_24H;                           //Variabile contenente il tipo di visualizzazione dell'ora.
static enum FIAT_TEMPERATURE temperature = FIAT_TEMPERATURE_CELSIUS;       //Variabile contenente il tipo di visualizzazione della temperatura.
static uint8_t display_trip_b = false;                                     //Variabile contenente l'abilitazione alla visualizzazione del trip b
static uint8_t autoclose_status = false;                                   //Variabile contenente lo stato dell'autoclose del veicolo.
static enum FIAT_LIGHT_SENS_LEVEL light_sensor = FIAT_LIGHT_SENS_LEVEL0;

void fiat_ducato_send_extra_informations(void);
/**
 * @brief fornisce l'unita' di misura dei consumi attualmente impostata.
 * @return l'unita' di misura dei conumi impostata.
 */
enum FIAT_CONSUMPTION fiat_ducato_get_measure_unit(void) {
    return current_consumption;
}

/**
 * @brief Restituisce lo stato attuale del tripb
 * @return true se il tripb è attivo, false altrimenti.
 */
bool fiat_ducato_get_tripb(void) {
    return display_trip_b;
}

/**
 * @brief Set di consumi, distanze e temperatura.
 * @param byte0 il byte contenente le informazioni.
 */
bool fiat_ducato_set_extra(uint8_t byte0) {
    bool retVal = false;
    uint8_t bits = byte0 & 0x03;
    enum FIAT_CONSUMPTION tmp_current_consumption;
    if (bits == 0) {
        tmp_current_consumption = FIAT_CONSUMPTION_KM_L;
    } else if (bits == 1) {
        tmp_current_consumption = FIAT_CONSUMPTION_L_100KM;
    } else {
        tmp_current_consumption = FIAT_CONSUMPTION_MIL_GAL;
    }
    if (tmp_current_consumption != current_consumption) {
        current_consumption = tmp_current_consumption;
        retVal = true;
    }

    enum FIAT_DISTANCE tmp_distance;
    if ((byte0 & 0x80) == 0x80) {
        tmp_distance = FIAT_DISTANCE_MILES;
    } else {
        tmp_distance = FIAT_DISTANCE_METERS;
    }
    if (tmp_distance != distance) {
        distance = tmp_distance;
        retVal = true;
    }

    enum FIAT_TEMPERATURE tmp_temperature;
    if ((byte0 & 0x40) == 0x40) {
        tmp_temperature = FIAT_TEMPERATURE_FAHRENHEIT;
    } else {
        tmp_temperature = FIAT_TEMPERATURE_CELSIUS;
    }
    if (tmp_temperature != temperature) {
        temperature = tmp_temperature;
        retVal = true;
    }
    return retVal;
}

/**
 * @brief Restituisce lo stato dell'autoclose delle porte.
 * @return true se è attivo, false altrimenti.
 */
bool fiat_ducato_get_autoclose(void) {
    return autoclose_status;
}

/**
 * @brief Imposta il valore per la chiusura delle porte.
 * @param val true se da attivare, false se da disattivare.
 */
bool fiat_ducato_set_autoclose(uint8_t byte) {
    bool retVal = false;
    uint8_t tmp_autoclose_status;
    if ((byte & 0x10) == 0x10) {
        tmp_autoclose_status = true;
    } else {
        tmp_autoclose_status = false;
    }
    if (autoclose_status != tmp_autoclose_status) {
        autoclose_status = tmp_autoclose_status;
        retVal = true;
    }
    return retVal;
}

/**
 * @brief Imposta il formato ora utilizzato dalla vettura.
 * @param byte0 il byte contenente le informazioni relative alla visualizzazione dell'ora.
 */
bool fiat_ducato_set_time_visualization(uint8_t byte0) {
    bool retVal;
    enum FIAT_TIME tmp_time_form;
    if ((byte0 & 0x02) == 0x02) {
        tmp_time_form = FIAT_TIME_AM_PM;
    } else {
        tmp_time_form = FIAT_TIME_24H;
    }
    if (tmp_time_form != time_form) {
        time_form = tmp_time_form;
        retVal = true;
    }
    return retVal;
}

/**
 * @brief Imposta il nuovo stato del tripB
 * @param byte il byte ricevuto dal canbus. 0x08 attiva, 0x00 disattiva.
 */
bool fiat_ducato_set_tripb(uint8_t byte) {
    bool retVal = false;
    uint8_t tmp_display_trip_b;
    if ((byte & 0x08) == 0x08) {
        tmp_display_trip_b = true;
    } else {
        tmp_display_trip_b = false;
    }
    if (tmp_display_trip_b != display_trip_b) {
        retVal = true;
        display_trip_b = tmp_display_trip_b;
    }
    return retVal;
}

bool fiat_ducato_change_lightsensor_level(enum FIAT_LIGHT_SENS_LEVEL light) {
    bool retVal = false;
    if (light_sensor != light) {
        light_sensor = light;
        retVal = true;
        fiat_ducato_send_extra_informations();
    }
    return retVal;
}

bool fiat_ducato_set_lightsensor_level(enum FIAT_LIGHT_SENS_LEVEL light) {
    bool retVal = false;
    if (light_sensor != light) {
        retVal = true;
    }
    light_sensor = light;
    return retVal;
}

enum FIAT_LIGHT_SENS_LEVEL fiat_ducato_get_lightsensor_level(void) {
    return light_sensor;
}

/**
 * @brief Trasmette su CAN bus le informazioni riguardanti unità di misura e lingua.
 */
void fiat_ducato_send_extra_informations(void) {
    CAN_MESSAGE message;
    message.can_id = 0xC394024;
    message.datalen = 8;
    message.interface = CAN_INTERFACE_0;
    message.can_is_ext = true;

    message.can_data.byte0 = distance | current_consumption | temperature;
    message.can_data.byte1 = time_form;
    message.can_data.byte1 |= 0x10;
    message.can_data.byte2 = 0x00;
    message.can_data.byte3 = 0x06;
    if (autoclose_status) {
        message.can_data.byte3 |= 0x10;
    }
    message.can_data.byte4 = 0x02;
    if (display_trip_b) {
        message.can_data.byte4 |= 0x08;
    }
    message.can_data.byte4 |= fiat_ducato_get_lightsensor_level() << 1;
    message.can_data.byte5 = language_str[current_language].byte0;
    message.can_data.byte6 = language_str[current_language].byte1;
    message.can_data.byte6 |= 0x02;
    message.can_data.byte7 = 0;
    LOG_DEF_NORMAL("Info veicolo: distance %d current_consumption: %d temperature: %d language: %d\r\n", distance, current_consumption, temperature, current_language);
    event_can0_tx_message_emit(&message);

    redraw_last_menu();
}

/**
 * @brief Imposta la lingua richiesta sulla vettura.
 * @param lang la lingua da impostare.
 */
void fiat_ducato_change_language(enum LANGUAGE_DEFINITION lang) {
    if (current_language != lang) {
        current_language = lang;
        fiat_ducato_send_extra_informations();
    }
}

static void fiat_ducato_handle_language_changed(const union EVENT* const event) {
    if (event->as_generic.event == eEVENT_LANGUAGE_CHANGED) {
        fiat_ducato_change_language(event->as_language.language);
    }
}

/**
 * @brief Imposta il nuovo stato del tripB
 * @param enable true se bisogna abilitare la visualizzazione, false altrimenti.
 */
void fiat_ducato_change_tripb(bool enable) {
    if (display_trip_b != enable) {
        display_trip_b = enable;
        fiat_ducato_send_extra_informations();
    }
}

/**
 * @brief Imposta il formato ora utilizzato dalla vettura.
 * @param byte0 il byte contenente le informazioni relative alla visualizzazione dell'ora.
 */
void fiat_ducato_change_autoclose(bool val) {
    if (autoclose_status != val) {
        autoclose_status = val;
        fiat_ducato_send_extra_informations();
    }
}

/**
 * @brief Imposta la nuova unita' di misura delle temperature
 * @param temp la nuova unita' di misura delle temperature
 */
void fiat_ducato_change_temperature(enum FIAT_TEMPERATURE temp) {
    if (temperature != temp) {
        temperature = temp;
        fiat_ducato_send_extra_informations();
    }
}

/**
 * @brief Imposta il tipo di unità di misura dei consumi.
 * La vettura non accetta Miglia con consumi in metri e viceversa.
 * @param consumption la nuova unità di misura dei consumi.
 */
void fiat_ducato_change_measureunit(enum FIAT_CONSUMPTION consumption) {
    if (current_consumption != consumption) {
        current_consumption = consumption;
        switch (consumption) {
            case FIAT_CONSUMPTION_MIL_GAL:
                distance = FIAT_DISTANCE_MILES;
                break;
            case FIAT_CONSUMPTION_KM_L:
            case FIAT_CONSUMPTION_L_100KM:
                distance = FIAT_DISTANCE_METERS;
                break;
        }
        fiat_ducato_send_extra_informations();
    }
}

/**
 * @brief Fornisce la lingua corrente sul veicolo.
 * @return la linuga configurata
 */
enum FIAT_LANGUAGE fiat_ducato_get_language(void) {
    return current_language;
}

/**
 * @brief Fornisce l'unità di misura corrente sul veicolo.
 * @return l'unità di temperatura configurata.
 */
enum FIAT_TEMPERATURE fiat_ducato_get_temperature(void) {
    return temperature;
}

/**
 * @brief Invia le informazioni dell'ora nel momento in cui sono necessarie.
 * L'evento ricevuto dall'event loop contenente le informazioni relative alla data.
 */
static void fiat_ducato_handle_minutes_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_TIME_CHANGED) {
        LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
        if (vehicle_get_sync_clock()) {
            LOG_DEF_NORMAL("Orologio attivo.\r\n");
            uint8_t minute = msg->as_time.minute;
            uint8_t hour = msg->as_time.hour;
            uint8_t day = msg->as_time.day;
            uint8_t month = msg->as_time.month;
            uint16_t year = msg->as_time.year;
            fiat_ducato_send_changed_clock_date(year, month, day, hour, minute);
        } else {
            LOG_DEF_NORMAL("Not syncing clock. Setting disabled.\r\n");
        }
    }
}

/**
 * @brief Riceve un evento dall'event loop contenente le informazioni del formato ora
 * @param msg l'evento con le informazioni del parametro dell'ora.
 */
void fiat_ducato_timedate_format_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_TIME_FORMAT_CHANGED) {
        enum FIAT_TIME t;
        if (msg->as_time_format.time_format == 0x00) {
            t = FIAT_TIME_24H;
        } else {
            t = FIAT_TIME_AM_PM;
        }
        if (t != time_form) {
            time_form = t;
            fiat_ducato_send_extra_informations();
        }
    }
}

/**
 * @brief Inizializzazione del modulo di trasmissione dei dati.
 */
void fiat_ducato_extra_info_init(void) {
    event_connect_callback(eEVENT_LANGUAGE_CHANGED, fiat_ducato_handle_language_changed);
    event_connect_callback(eEVENT_TIME_CHANGED, fiat_ducato_handle_minutes_changed);
    event_connect_callback(eEVENT_TIME_FORMAT_CHANGED, fiat_ducato_timedate_format_changed);
}
