#include "alpine_menu.h"
#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "logger.h"
#include "vehicle.h"

static uint16_t vehicle_year = 2018;
static uint8_t vehicle_month = 1;
static uint8_t vehicle_day = 1;
static uint8_t vehicle_hour = 13;
static uint8_t vehicle_minute = 1;

uint16_t fiat_ducato_get_vehicle_year(void) {
    return vehicle_year;
}

uint8_t fiat_ducato_get_vehicle_month(void) {
    return vehicle_month;
}

uint8_t fiat_ducato_get_vehicle_day(void) {
    return vehicle_day;
}

uint8_t fiat_ducato_get_vehicle_hour(void) {
    return vehicle_hour;
}

uint8_t fiat_ducato_get_vehicle_minute(void) {
    return vehicle_minute;
}

static void fiat_ducato_read_clock(const union EVENT *const msg) {
    const uint32_t id = 0x0C214003;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                vehicle_year = year_hex_to_dec(msg->as_can_message.msg.can_data.byte4, msg->as_can_message.msg.can_data.byte5);
                vehicle_month = hex_to_dec(msg->as_can_message.msg.can_data.byte3);
                vehicle_day = hex_to_dec(msg->as_can_message.msg.can_data.byte2);
                vehicle_hour = hex_to_dec(msg->as_can_message.msg.can_data.byte0);
                vehicle_minute = hex_to_dec(msg->as_can_message.msg.can_data.byte1);
            }
        }
    }
}

/**
 * @brief Converte un numero in formato decimale in formato esadecimale.
 * il numero decimale 62 viene trasmesso su CAN con codifica BCD
 * quindi la trasmissione è 0x62.
 * @param byte0 il numero da convertire
 * @return il numero convertito
 */
static inline uint8_t dec_to_hex(uint8_t byte0) {
    uint8_t retVal;
    uint8_t tmp;
    retVal = byte0 / 10 * 0x10;
    tmp = byte0 % 10;
    retVal += tmp;
    return retVal;
}

/**
 * @brief Converte un anno in formato decimale, per esempio 2018 e lo converte
 * in un anno in formato esadecimale quindi: 0x20 0x18.
 * @param i l'anno da convertire.
 * @return il numero convertito.
 */
static inline uint16_t year_dec_to_hex(uint16_t i) {
    uint16_t retVal;
    uint16_t tmp;
    retVal = i / 1000 * 0x1000;
    tmp = i % 1000;
    retVal += tmp / 100 * 0x100;
    tmp = tmp % 100;
    retVal += tmp / 10 * 0x10;
    tmp = tmp % 10;
    retVal += tmp;
    return retVal;
}

void fiat_ducato_send_changed_clock_date(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute) {
    CAN_MESSAGE message;
    vehicle_year = year;
    vehicle_month = month;
    vehicle_day = day;
    vehicle_hour = hour;
    vehicle_minute = minute;
    LOG_DEF_NORMAL("%s year: %d month: %d day: %d hour: %d minute: %d\r\n", __PRETTY_FUNCTION__, year, month, day, hour, minute);
    message.can_id = 0xC214024;
    message.datalen = 6;
    message.interface = CAN_INTERFACE_0;
    message.can_is_ext = true;
    message.can_data.byte0 = dec_to_hex(hour);
    message.can_data.byte1 = dec_to_hex(minute);
    message.can_data.byte2 = dec_to_hex(day);
    message.can_data.byte3 = dec_to_hex(month);
    year = year_dec_to_hex(year);
    message.can_data.byte4 = year >> 8;
    message.can_data.byte5 = year;
    event_can0_tx_message_emit(&message);
}

void fiat_ducato_read_extra_informations(const union EVENT *const msg) {
    const uint32_t id = 0x0C394003;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE *message = &(msg->as_can_message.msg);
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == id) {
                bool retVal = false;
                retVal = fiat_ducato_set_extra(message->can_data.byte0);
                retVal = fiat_ducato_set_time_visualization(message->can_data.byte1);
                retVal = fiat_ducato_set_tripb(message->can_data.byte4);
                retVal = fiat_ducato_set_autoclose(message->can_data.byte3);
                retVal = fiat_ducato_set_lightsensor_level((message->can_data.byte4 >> 1) & 0x03);
                if (retVal) {
                    redraw_last_menu();
                }
            }
        }
    }
}

void fiat_ducato_extra_init(void) {
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xC394003);
    set_vehicle_change_date_func(fiat_ducato_send_changed_clock_date);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, fiat_ducato_read_extra_informations);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, fiat_ducato_read_clock);
}