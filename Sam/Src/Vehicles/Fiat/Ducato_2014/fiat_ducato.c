
#include <stdbool.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "accessory_uart.h"
#include "alpine_menu.h"
#include "board_version.h"
#include "canbus.h"
#include "controller.h"
#include "difference.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "logger.h"
#include "settings.h"
#include "speed.h"
#include "timers.h"

/**
 * @brief Gestione della retromarcia
 * Implementato gestione utilizzando il tempo o i km/h
 */
static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0x04214001;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                if ((msg->as_can_message.msg.can_data.byte7 & 0x04) == 0x04) {
                    event_reverse_emit(REVERSE_ON);
                } else {
                    event_reverse_emit(REVERSE_OFF);
                }
            }
        }
    }
}

static void handle_handbrake(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE* const message = &msg->as_can_message.msg;
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == 0x06214000) {
                if ((message->can_data.byte0 & 0x20) == 0x20) {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_VEHICLE);
                } else {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_VEHICLE);
                }
            }
        }
    }
}

static uint32_t old_speed;
static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0x4394000;
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                tmp = (((msg->as_can_message.msg.can_data.byte0 & 0x1F) << 8) + msg->as_can_message.msg.can_data.byte1);
                //0x0107 circa 20km/h 263
                //0x0257 circa 40km/h 599
                //0x03A2 circa 60km/h 930
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                tmp = tmp * 25 / 4;
                event_speed_emit(tmp);
                old_speed = tmp;
            }
        }
    }
}

static bool configured = false;
enum FIAT_DUCATO_VERSION current_config = 0;
enum FIAT_DUCATO_VERSION vers_selected;
static void handle_config_setup(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SETTINGS_READ) {
        if (msg->as_settings_read.type == SETTING_READ_VEHICLE_TYPE) {
            if (!configured) {
                vers_selected = current_config = settings_read(VEHICLE_TYPE);

                switch (current_config) {
                    case FIAT_DUCATO_OPENDASH:
                        fiat_ducato_analog_swc_init();
                        alpine_menu_disable_clock_management();
                        break;
                    case FIAT_DUCATO_VP1:
                        canbus0_enable_tx();
                        fiat_ducato_vp1_ack_init();
                        fiat_ducato_canswc_init();
                        alpine_menu_disable_clock_management();
                        break;
                    case FIAT_DUCATO_VP2:
                        canbus0_enable_tx();
                        fiat_ducato_vp2_ack_init();
                        fiat_ducato_extra_init();
                        fiat_ducato_extra_info_init();
                        fiat_ducato_canswc_init();
                        break;
                    case FIAT_DUCATO_7INCH:
                    case FIAT_DUCATO_OPEN_DASH_52WAYS:
                        fiat_ducato_canswc_init();
                        alpine_menu_disable_clock_management();
                        break;
                    default:
                        LOG_DEF_NORMAL("Unknown vehicle config %d\r\n", current_config);
                        settings_write(VEHICLE_TYPE, FIAT_DUCATO_OPENDASH);
                        fiat_ducato_analog_swc_init();
                        alpine_menu_disable_clock_management();
                        break;
                }
                configured = true;
            }
        }
    }
}

enum LED_COLOR vehicle_get_default_led_color(void) {
    return LED_COLOR_ORANGE;
}

/**
 * Inizializzazione del software relativo al veicolo Fiat Ducato con tutte le sue
 * sotto sezioni. L'inizializzazione è differente a seconda del fatto che la radio
 * preinstallata sul veicolo è una VP1 o una VP2.
 * Se è installata la VP1 non viene inizializzato il menu veicolo in quanto le configuraizioni sono gestite dal veicolo
 * Se è installata una VP2 viene inizializzato il menu veicolo in quanto siamo noi che dobbiamo fornirlo tramite la radio.
 *
 */
void fiat_ducato_init(struct CANBUS_CONFIG* can_conf) {
    canbus_init(can_conf);

    board_set_version(BOARD_VERSION_00);
    fiat_ducato_ignition_init();
    fiat_ducato_menu_init();
    fiat_ducato_laneassist_init();
    accessory_uart_init();

    event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04394000);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04214001);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06214000);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_handbrake);
}
