

#include <stdbool.h>
#include <stdio.h>

#include "analogswc.h"
#include "event_list.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "logger.h"

#define MUTE_MIN 251
#define MUTE_MAX 600
#define VOL_UP_MIN 121
#define VOL_UP_MAX 250
#define VOL_DOWN_MIN 50
#define VOL_DOWN_MAX 120
#define SOURCE_MIN 0
#define SOURCE_MAX 49

#define TEL_HANGUP_MIN 0
#define TEL_HANGUP_MAX 49
#define SEEK_UP_MIN 121
#define SEEK_UP_MAX 250
#define SEEK_DOWN_MIN 50
#define SEEK_DOWN_MAX 120
#define TEL_DIAL_MIN 251
#define TEL_DIAL_MAX 600

static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static enum RADIO_MEDIA_TYPE radio_media_type = RADIO_MEDIA_UNKNOWN;

void handle_analog_swc(const union EVENT* const event) {
    if (event->as_generic.event == eEVENT_ANALOG_VALUE) {
        uint32_t res_a = event->as_analog_value.swc_analog.channel_a;
        uint32_t res_b = event->as_analog_value.swc_analog.channel_b;
        if ((res_a >= VOL_UP_MIN) && (res_a < VOL_UP_MAX)) {
            cur_active = SWC_VOL_UP_PRESSED;
            swc_active = true;
        } else if ((res_a >= VOL_DOWN_MIN) && (res_a < VOL_DOWN_MAX)) {
            cur_active = SWC_VOL_DOWN_PRESSED;
            swc_active = true;
        } else if ((res_a >= MUTE_MIN) && (res_a < MUTE_MAX)) {
            cur_active = SWC_MUTE_PRESSED;
            swc_active = true;
        } else if (res_a < SOURCE_MAX) {
            cur_active = SWC_SPEECH_PRESSED;
            swc_active = true;
        } else if ((res_b >= SEEK_UP_MIN) && (res_b < SEEK_UP_MAX)) {
            cur_active = SWC_SEEK_UP_PRESSED;
            swc_active = true;
        } else if ((res_b >= SEEK_DOWN_MIN) && (res_b < SEEK_DOWN_MAX)) {
            cur_active = SWC_SEEK_DOWN_PRESSED;
            swc_active = true;
        } else if (res_b < TEL_HANGUP_MAX) {
            if (radio_media_type == RADIO_MEDIA_PHONE) {
                cur_active = SWC_TEL_HANGUP_PRESSED;
            } else {
                cur_active = SWC_SOURCE_PRESSED;
            }
            swc_active = true;
        } else if ((res_b >= TEL_DIAL_MIN) && (res_b < TEL_DIAL_MAX)) {
            if (radio_media_type == RADIO_MEDIA_PHONE) {
                cur_active = SWC_TEL_PICKUP_PRESSED;
            } else {
                cur_active = SWC_SPEECH_PRESSED;
            }
            swc_active = true;
        } else {
            if (swc_active) {
                cur_active++;
                swc_active = false;
            } else {
                cur_active = SWC_NO_BUTTON_PRESSED;
            }
        }
        event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_media_type = msg->as_radio_source.media_type;
    }
}

void fiat_ducato_analog_swc_init(void) {
    event_connect_callback(eEVENT_ANALOG_VALUE, handle_analog_swc);
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    analogswc_init();
}