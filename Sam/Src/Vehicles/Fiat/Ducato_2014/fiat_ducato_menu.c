#include "alpine_menu.h"
#include "alpine_task.h"
#include "fiat_definitions.h"
#include "fiat_ducato_2014.h"
#include "fiat_ducato_priv.h"
#include "ignition.h"
#include "menu_manager.h"
#include "menu_walking.h"
#include "radio_message.h"
#include "vehicle.h"

typedef enum {
    FIAT_DUCATO_MENU_MAIN = 0x80,  //!< FIAT_DUCATO_MENU_MAIN
    FIAT_DUCATO_MENU_EXTRA,
    FIAT_DUCATO_MENU_UNITS,  //!< FIAT_DUCATO_MENU_UNITS
    FIAT_DUCATO_MENU_SELECTION_ENTRY,
    FIAT_DUCATO_MENU_SELECTION,  //!< FIAT_DUCATO_MENU_SELECTION
    FIAT_DUCATO_MENU_TRIPB,
    FIAT_DUCATO_MENU_AUTOCLOSE,
    FIAT_DUCATO_MENU_MEASURE,
    FIAT_DUCATO_MENU_TEMP,
    FIAT_DUCATO_MENU_LIGHT_SENSOR,
    FIAT_DUCATO_MENU_MUTE_LANEASSIST,
} FIAT_DUCATO_MENU;

enum FIAT_DUCATO_MAIN_MENU {
    FIAT_DUCATO_MAIN_MENU_VEHICLE_SELECTION = 1,
    FIAT_DUCATO_MAIN_MENU_UNITS_SELECTION,
    FIAT_DUCATO_MAIN_MENU_TRIPB_SELECTION,
    FIAT_DUCATO_MAIN_MENU_AUTOCLOSE_SELECTION,
    FIAT_DUCATO_MAIN_MENU_LIGHTSENS_SELECTION,
    FIAT_DUCATO_MAIN_MENU_LANEASSIST_SELECTION,
};

enum FIAT_DUCATO_SEL_MENU {
    FIAT_DUCATO_SEL_MENU_OPEN_DASH = 1,
    FIAT_DUCATO_SEL_MENU_VP1,
    FIAT_DUCATO_SEL_MENU_VP2,
    FIAT_DUCATO_SEL_MENU_7INCH,
    FIAT_DUCATO_SEL_MENU_OPEN_DASH_52WAYS,
};

enum FIAT_DUCATO_SEL_ENTRY_MENU {
    FIAT_DUCATO_SEL_ENTRY_VEHICLE_CONFIRM = 1,
    FIAT_DUCATO_SEL_ENTRY_VEHICLE_REBOOT
};

enum FIAT_DUCATO_UNITS_MENU {
    FIAT_DUCATO_UNITS_MENU_MEASURE = 1,
    FIAT_DUCATO_UNITS_MENU_TEMPERATURE
};

/**
 * @brief Traduzione della stringa Vehicle settings nelle varie lingue .
 */
static const char *vehicle_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Fahrzeugeinstellungen",       //!< LANGUAGE_DEFINITION_GERMAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Réglages du véhicule",        //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni veicolo",        //!< LANGUAGE_DEFINITION_ITALIAN
    "Configuración del vehículo",  //!< LANGUAGE_DEFINITION_SPANISH
    "Configuração do veiculo",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Ustawienia Pojazdu",          //!< LANGUAGE_DEFINITION_POLISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CZECH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SWEDISH
    "Voertuig Instellingen",       //!< LANGUAGE_DEFINITION_DUTCH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_JAPANESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_RUSSIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_KOREAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_TURKISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CHINESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *oem_radio_str[LANGUAGE_DEFINITION_COUNT] = {
    "Werksradio",          //!< LANGUAGE_DEFINITION_GERMAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_ENGLISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Radio d'origine",     //!< LANGUAGE_DEFINITION_FRENCH
    "Radio Originale",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio origen OEM",    //!< LANGUAGE_DEFINITION_SPANISH
    "Rádio original OEM",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_POLISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_CZECH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_SWEDISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_DUTCH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_JAPANESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_RUSSIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_KOREAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_TURKISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_CHINESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_NORWEGIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *radio_vp1[] = {
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_GERMAN
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_ENGLISH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio VP1",  //!< LANGUAGE_DEFINITION_SPANISH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_POLISH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_CZECH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_SWEDISH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_DUTCH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_JAPANESE
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_KOREAN
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_TURKISH
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_CHINESE
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "VP1 Radio",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *radio_vp2[] = {
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_GERMAN
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_ENGLISH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio VP2",  //!< LANGUAGE_DEFINITION_SPANISH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_POLISH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_CZECH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_SWEDISH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_DUTCH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_JAPANESE
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_KOREAN
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_TURKISH
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_CHINESE
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "VP2 Radio",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *radio_7inch[] = {
    "7 Zoll 2020",     //!< LANGUAGE_DEFINITION_GERMAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_ENGLISH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "7 Pouces 2020",   //!< LANGUAGE_DEFINITION_FRENCH
    "7 Pollici 2020",  //!< LANGUAGE_DEFINITION_ITALIAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_SPANISH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_POLISH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_CZECH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_SWEDISH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_DUTCH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_JAPANESE
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_KOREAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_TURKISH
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_CHINESE
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "7 Inch 2020",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *open_dash_52ways[] = {
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_GERMAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_FRENCH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_SPANISH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_POLISH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_CZECH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_DUTCH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_KOREAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_TURKISH
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_CHINESE
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Open Dash 52Pin",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *open_dash[] = {
    "Ohne Radio",         //!< LANGUAGE_DEFINITION_GERMAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_ENGLISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Préparation radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "Open Dash",          //!< LANGUAGE_DEFINITION_ITALIAN
    "Sin unidad",         //!< LANGUAGE_DEFINITION_SPANISH
    "Sem Radio",          //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_POLISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_CZECH
    "Open Dash",          //!< LANGUAGE_DEFINITION_SWEDISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_DUTCH
    "Open Dash",          //!< LANGUAGE_DEFINITION_JAPANESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_RUSSIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_KOREAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_TURKISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_CHINESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *confirm_str[] = {
    "Bestätigen",    //!< LANGUAGE_DEFINITION_GERMAN
    "Confirm",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Confirm",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Confirmation",  //!< LANGUAGE_DEFINITION_FRENCH
    "Conferma",      //!< LANGUAGE_DEFINITION_ITALIAN
    "Confirmar",     //!< LANGUAGE_DEFINITION_SPANISH
    "Confirmar",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Confirm",       //!< LANGUAGE_DEFINITION_POLISH
    "Confirm",       //!< LANGUAGE_DEFINITION_CZECH
    "Confirm",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Confirm",       //!< LANGUAGE_DEFINITION_DUTCH
    "Confirm",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Confirm",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_KOREAN
    "Confirm",       //!< LANGUAGE_DEFINITION_TURKISH
    "Confirm",       //!< LANGUAGE_DEFINITION_CHINESE
    "Confirm",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *reboot_str[] = {
    "Neustart",    //!< LANGUAGE_DEFINITION_GERMAN
    "Reboot",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Reboot",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Redémarrer",  //!< LANGUAGE_DEFINITION_FRENCH
    "Riavvia",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Reiniciar",   //!< LANGUAGE_DEFINITION_SPANISH
    "Reiniciar",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Reboot",      //!< LANGUAGE_DEFINITION_POLISH
    "Reboot",      //!< LANGUAGE_DEFINITION_CZECH
    "Reboot",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Reboot",      //!< LANGUAGE_DEFINITION_DUTCH
    "Reboot",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Reboot",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_KOREAN
    "Reboot",      //!< LANGUAGE_DEFINITION_TURKISH
    "Reboot",      //!< LANGUAGE_DEFINITION_CHINESE
    "Reboot",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * @brief Traduzione della stringa Temperature units nelle varie lingue.
 */
static const char *temperature_units_str[] = {
    "Temperatur",   //!< LANGUAGE_DEFINITION_GERMAN
    "Temperature",  //!< LANGUAGE_DEFINITION_ENGLISH
    "Temperature",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Température",  //!< LANGUAGE_DEFINITION_FRENCH
    "Temperatura",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Temperatura",  //!< LANGUAGE_DEFINITION_SPANISH
    "Temperatura",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Temperatura",  //!< LANGUAGE_DEFINITION_POLISH
    "Temperature",  //!< LANGUAGE_DEFINITION_CZECH
    "Temperature",  //!< LANGUAGE_DEFINITION_SWEDISH
    "Temperatuur",  //!< LANGUAGE_DEFINITION_DUTCH
    "Temperature",  //!< LANGUAGE_DEFINITION_JAPANESE
    "Temperature",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_KOREAN
    "Sıcaklık",     //!< LANGUAGE_DEFINITION_TURKISH
    "Temperature",  //!< LANGUAGE_DEFINITION_CHINESE
    "Temperature",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Temperature",  //!< LANGUAGE_DEFINITION_HUNGARIAN

};

/**
 * @brief Traduzione della stringa delle unità nelle varie lingue.
 */
static const char *units_str[] = {
    "Einheiten",  //!< LANGUAGE_DEFINITION_GERMAN
    "Units",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Units",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Unit",       //!< LANGUAGE_DEFINITION_FRENCH
    "Unità",      //!< LANGUAGE_DEFINITION_ITALIAN
    "Unidades",   //!< LANGUAGE_DEFINITION_SPANISH
    "Unidades",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Units",      //!< LANGUAGE_DEFINITION_POLISH
    "Units",      //!< LANGUAGE_DEFINITION_CZECH
    "Units",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Units",      //!< LANGUAGE_DEFINITION_DUTCH
    "Units",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Units",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Units",      //!< LANGUAGE_DEFINITION_KOREAN
    "Units",      //!< LANGUAGE_DEFINITION_TURKISH
    "Units",      //!< LANGUAGE_DEFINITION_CHINESE
    "Units",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Units",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Units",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa Autoclose nelle varie lingue.
 */
static const char *autoclose_str[] = {
    "Autom. Türverriegelung",   //!< LANGUAGE_DEFINITION_GERMAN
    "Autoclose",                //!< LANGUAGE_DEFINITION_ENGLISH
    "Autoclose",                //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Autoclose",                //!< LANGUAGE_DEFINITION_FRENCH
    "Autoclose",                //!< LANGUAGE_DEFINITION_ITALIAN
    "Bloqueo de puertas aut.",  //!< LANGUAGE_DEFINITION_SPANISH
    "Fecho automático",         //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Autoclose",                //!< LANGUAGE_DEFINITION_POLISH
    "Autoclose",                //!< LANGUAGE_DEFINITION_CZECH
    "Autoclose",                //!< LANGUAGE_DEFINITION_SWEDISH
    "Autoclose",                //!< LANGUAGE_DEFINITION_DUTCH
    "Autoclose",                //!< LANGUAGE_DEFINITION_JAPANESE
    "Autoclose",                //!< LANGUAGE_DEFINITION_RUSSIAN
    "Autoclose",                //!< LANGUAGE_DEFINITION_KOREAN
    "Otomatik Kilit",           //!< LANGUAGE_DEFINITION_TURKISH
    "Autoclose",                //!< LANGUAGE_DEFINITION_CHINESE
    "Autoclose",                //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Autoclose",                //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Autoclose",                //!< LANGUAGE_DEFINITION_HUNGARIAN
};
/**
 * @brief Traduzione della stringa Units of measure nelle varie lingue.
 */
static const char *measure_units_str[] = {
    "Kraftstoffverbrauch",  //!< LANGUAGE_DEFINITION_GERMAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Consommation",         //!< LANGUAGE_DEFINITION_FRENCH
    "Consumi",              //!< LANGUAGE_DEFINITION_ITALIAN
    "Consumos",             //!< LANGUAGE_DEFINITION_SPANISH
    "Consumos",             //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Zużycie paliwa",       //!< LANGUAGE_DEFINITION_POLISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_CZECH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Brandstofverbruik",    //!< LANGUAGE_DEFINITION_DUTCH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_KOREAN
    "Tüketim",              //!< LANGUAGE_DEFINITION_TURKISH
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_CHINESE
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Fuel Consumption",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

/**
 * Traduzione della stringa Visualizza Trip B nelle varie lingue.
 */
static const char *tripb_str[] = {
    "Fahrt B anzeigen",    //!< LANGUAGE_DEFINITION_GERMAN
    "Display Trip B",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Display Trip B",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Affichage Trajet B",  //!< LANGUAGE_DEFINITION_FRENCH
    "Visualizz. Trip B",   //!< LANGUAGE_DEFINITION_ITALIAN
    "Visualizar Viaje B",  //!< LANGUAGE_DEFINITION_SPANISH
    "Mostrar viagem B",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Wizualiz. Trasy B",   //!< LANGUAGE_DEFINITION_POLISH
    "Display Trip B",      //!< LANGUAGE_DEFINITION_CZECH
    "Display Trip B",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Weergave Trip B",     //!< LANGUAGE_DEFINITION_DUTCH
    "Display Trip B",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Display Trip B",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Display Trip B",      //!< LANGUAGE_DEFINITION_KOREAN
    "Trip B Görüntüleme",  //!< LANGUAGE_DEFINITION_TURKISH
    "Display Trip B",      //!< LANGUAGE_DEFINITION_CHINESE
    "Display Trip B",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Display Trip B",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Display Trip B",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *lightsensor_level_str[] = {
    "Dimmer Sensor",        //!< LANGUAGE_DEFINITION_GERMAN
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Capteur phares",       //!< LANGUAGE_DEFINITION_FRENCH
    "Sensore fari",         //!< LANGUAGE_DEFINITION_ITALIAN
    "Sensor iluminación",   //!< LANGUAGE_DEFINITION_SPANISH
    "Sensor faróis",        //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Czujnik reflektorów",  //!< LANGUAGE_DEFINITION_POLISH
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_CZECH
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Sensor koplampen",     //!< LANGUAGE_DEFINITION_DUTCH
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_KOREAN
    "Far Sensörü",          //!< LANGUAGE_DEFINITION_TURKISH
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_CHINESE
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Twilight Sensor",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *mute_laneassist_str[] = {
    "Mute während LaneAssist Aktiv",  //!< LANGUAGE_DEFINITION_GERMAN
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Muet quand Laneassist Active",   //!< LANGUAGE_DEFINITION_FRENCH
    "Mute con Allarme LaneAssist",    //!< LANGUAGE_DEFINITION_ITALIAN
    "Mute con asist. carril activo",  //!< LANGUAGE_DEFINITION_SPANISH
    "Mute ligado",                    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_POLISH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_CZECH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_DUTCH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_KOREAN
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_TURKISH
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_CHINESE
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Mute on LaneAssist Active",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *on_str[] = {
    "Ein",   //!< LANGUAGE_DEFINITION_GERMAN
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH
    "On",    //!< LANGUAGE_DEFINITION_ENGLISH_US
    "On",    //!< LANGUAGE_DEFINITION_FRENCH
    "On",    //!< LANGUAGE_DEFINITION_ITALIAN
    "On",    //!< LANGUAGE_DEFINITION_SPANISH
    "On",    //!< LANGUAGE_DEFINITION_PORTUGUESE
    "On",    //!< LANGUAGE_DEFINITION_POLISH
    "On",    //!< LANGUAGE_DEFINITION_CZECH
    "On",    //!< LANGUAGE_DEFINITION_SWEDISH
    "On",    //!< LANGUAGE_DEFINITION_DUTCH
    "On",    //!< LANGUAGE_DEFINITION_JAPANESE
    "On",    //!< LANGUAGE_DEFINITION_RUSSIAN
    "On",    //!< LANGUAGE_DEFINITION_KOREAN
    "Açık",  //!< LANGUAGE_DEFINITION_TURKISH
    "On",    //!< LANGUAGE_DEFINITION_CHINESE
    "On",    //!< LANGUAGE_DEFINITION_NORWEGIAN
    "On",    //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "On",    //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *off_str[] = {
    "Aus",     //!< LANGUAGE_DEFINITION_GERMAN
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH
    "Off",     //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Off",     //!< LANGUAGE_DEFINITION_FRENCH
    "Off",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Off",     //!< LANGUAGE_DEFINITION_SPANISH
    "Off",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Off",     //!< LANGUAGE_DEFINITION_POLISH
    "Off",     //!< LANGUAGE_DEFINITION_CZECH
    "Off",     //!< LANGUAGE_DEFINITION_SWEDISH
    "Off",     //!< LANGUAGE_DEFINITION_DUTCH
    "Off",     //!< LANGUAGE_DEFINITION_JAPANESE
    "Off",     //!< LANGUAGE_DEFINITION_RUSSIAN
    "Off",     //!< LANGUAGE_DEFINITION_KOREAN
    "Kapalı",  //!< LANGUAGE_DEFINITION_TURKISH
    "Off",     //!< LANGUAGE_DEFINITION_CHINESE
    "Off",     //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Off",     //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Off",     //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char empty[] = "\0";
static const char fahrenheit[] = "Fahrenheit";  //!< String used in the menu.
static const char celsius[] = "Celsius";        //!< String used in the menu.
static const char km_l[] = "Km / L";            //!< String used in the menu.
static const char l_km[] = "L / 100Km";         //!< String used in the menu.
static const char miles[] = "mpg";              //!< String used in the menu.

static inline const char *oem_radio_string(enum FIAT_DUCATO_VERSION vers_req) {
    const char *col = empty;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (vers_req) {
        case FIAT_DUCATO_VP1:
            col = radio_vp1[language];
            break;
        case FIAT_DUCATO_VP2:
            col = radio_vp2[language];
            break;
        case FIAT_DUCATO_OPENDASH:
            col = open_dash[language];
            break;
        case FIAT_DUCATO_7INCH:
            col = radio_7inch[language];
            break;
        case FIAT_DUCATO_OPEN_DASH_52WAYS:
            col = open_dash_52ways[language];
            break;
        default:
            settings_write(VEHICLE_TYPE, FIAT_DUCATO_OPENDASH);
            col = open_dash[language];
            break;
    }
    return col;
}

/**
 * @brief Restituisce la stringa associata allo stato del TRIP B
 * @return On o Off.
 */
static const char *fiat_ducato_get_current_tripB_str(void) {
    bool tripb = fiat_ducato_get_tripb();
    const char *tripb_val;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (tripb) {
        tripb_val = on_str[language];
    } else {
        tripb_val = off_str[language];
    }
    return tripb_val;
}
/**
 * @brief Restituisce la stringa associata allo stato della chiusura automatica delle portiere.
 * @return On o Off.
 */
static const char *fiat_ducato_get_current_autoclose_str(void) {
    bool autoclose = fiat_ducato_get_autoclose();
    const char *autoclose_val;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (autoclose) {
        autoclose_val = on_str[language];
    } else {
        autoclose_val = off_str[language];
    }
    return autoclose_val;
}
/**
 * @brief Restituisce la stringa associata allo stato dell'attivazione del mute sul sidesense
 * @return On o Off.
 */
static const char *fiat_ducato_get_current_mute_sidesense_str(void) {
    enum FIAT_MUTE_ON_LANE_ASSIST_ALERT val = fiat_ducato_lane_assist_warning();
    const char *option;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    if (val == FIAT_MUTE_ON_LANE_ASSIST_ALERT_ON) {
        option = on_str[language];
    } else {
        option = off_str[language];
    }
    return option;
}
extern enum FIAT_DUCATO_VERSION current_config;
extern enum FIAT_DUCATO_VERSION vers_selected;
menu_manager_t fiat_ducato_selection_menu;

static bool fiat_ducato_selection_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(fiat_ducato_selection_menu), FIAT_DUCATO_MENU_SELECTION, oem_radio_str[language]);
}

static bool fiat_ducato_selection_menu_send_open_dash(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == FIAT_DUCATO_OPENDASH) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_OPENDASH, 0, open_dash[language], current_selection);
}

static bool fiat_ducato_selection_menu_send_vp1(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == FIAT_DUCATO_VP1) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_VP1, 0, radio_vp1[language], current_selection);
}

static bool fiat_ducato_selection_menu_send_vp2(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == FIAT_DUCATO_VP2) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_VP2, 0, radio_vp2[language], current_selection);
}

static bool fiat_ducato_selection_menu_send_7inch(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == FIAT_DUCATO_7INCH) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_7INCH, 0, radio_7inch[language], current_selection);
}

static bool fiat_ducato_selection_menu_send_open_dash_52_ways(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == FIAT_DUCATO_OPEN_DASH_52WAYS) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_OPEN_DASH_52WAYS, 0, open_dash_52ways[language], current_selection);
}

void fiat_ducato_selection_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, fiat_ducato_selection_menu_send_header);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_MENU_OPEN_DASH, fiat_ducato_selection_menu_send_open_dash);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_MENU_VP1, fiat_ducato_selection_menu_send_vp1);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_MENU_VP2, fiat_ducato_selection_menu_send_vp2);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_MENU_7INCH, fiat_ducato_selection_menu_send_7inch);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_MENU_OPEN_DASH_52WAYS, fiat_ducato_selection_menu_send_open_dash_52_ways);
}

menu_manager_t fiat_ducato_selection_entry_menu;

static bool fiat_ducato_selection_menu_entry_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(fiat_ducato_selection_entry_menu), FIAT_DUCATO_MENU_SELECTION_ENTRY, vehicle_settings_str[language]);
}

static bool fiat_ducato_selection_menu_entry_send_vehicle_confirmation(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *oem_radio = oem_radio_string(vers_selected);
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_SELECTION, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, oem_radio_str[language], oem_radio);
}

static bool fiat_ducato_selection_menu_entry_send_reboot(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *str = empty;
    if (vers_selected != current_config) {
        str = reboot_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_SEL_ENTRY_VEHICLE_REBOOT, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, confirm_str[language], str);
}

void fiat_ducato_selection_menu_entry_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, fiat_ducato_selection_menu_entry_send_header);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_ENTRY_VEHICLE_CONFIRM, fiat_ducato_selection_menu_entry_send_vehicle_confirmation);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_SEL_ENTRY_VEHICLE_REBOOT, fiat_ducato_selection_menu_entry_send_reboot);
}

/**
 * @brief Restituisce la stringa associata all'unita' di misura dei consumi corrente.
 * @return km/l l/100km miglia/gallone
 */
static const char *fiat_ducato_get_current_measure_str(void) {
    enum FIAT_CONSUMPTION comp_value = fiat_ducato_get_measure_unit();
    const char *comp_str;
    if (comp_value == FIAT_CONSUMPTION_KM_L) {
        comp_str = km_l;
    } else if (comp_value == FIAT_CONSUMPTION_L_100KM) {
        comp_str = l_km;
    } else {
        comp_str = miles;
    }
    return comp_str;
}

/**
 * @brief Restituisce la stringa associata all'unita' di misura della temperatura corrente.
 * @return celsius o fahrenheit.
 */
static const char *fiat_ducato_get_current_temperature_str() {
    enum FIAT_TEMPERATURE temp_value = fiat_ducato_get_temperature();
    const char *temp_str;
    if (temp_value == FIAT_TEMPERATURE_CELSIUS) {
        temp_str = celsius;
    } else {
        temp_str = fahrenheit;
    }
    return temp_str;
}

menu_manager_t fiat_ducato_units_menu;

static bool fiat_ducato_units_menu_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(fiat_ducato_units_menu), FIAT_DUCATO_MENU_UNITS, units_str[language]);
}

static bool fiat_ducato_units_menu_measure(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *comp_str = fiat_ducato_get_current_measure_str();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_MEASURE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, measure_units_str[language], comp_str);
}

static bool fiat_ducato_units_menu_temperature(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *temp_str = fiat_ducato_get_current_temperature_str();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_TEMP, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, temperature_units_str[language], temp_str);
}

void fiat_ducato_units_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, fiat_ducato_units_menu_header);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_UNITS_MENU_MEASURE, fiat_ducato_units_menu_measure);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_UNITS_MENU_TEMPERATURE, fiat_ducato_units_menu_temperature);
}

menu_manager_t fiat_ducato_main_menu;

void fiat_ducato_menu_disable_VP2_lines(void) {
    menu_manager_clear_active_bit(fiat_ducato_main_menu, FIAT_DUCATO_MAIN_MENU_UNITS_SELECTION);
    menu_manager_clear_active_bit(fiat_ducato_main_menu, FIAT_DUCATO_MAIN_MENU_TRIPB_SELECTION);
    menu_manager_clear_active_bit(fiat_ducato_main_menu, FIAT_DUCATO_MAIN_MENU_AUTOCLOSE_SELECTION);
    menu_manager_clear_active_bit(fiat_ducato_main_menu, FIAT_DUCATO_MAIN_MENU_LIGHTSENS_SELECTION);
    menu_manager_clear_active_bit(fiat_ducato_main_menu, FIAT_DUCATO_MAIN_MENU_LANEASSIST_SELECTION);
}

static bool fiat_ducato_main_menu_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(fiat_ducato_main_menu), FIAT_DUCATO_MENU_MAIN, vehicle_settings_str[language]);
}

static bool fiat_ducato_main_menu_vehicle_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *oem_radio = oem_radio_string(current_config);
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_SELECTION_ENTRY, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, oem_radio_str[language], oem_radio);
}

static bool fiat_ducato_main_menu_units_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_UNITS, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, units_str[language], "\0");
}

static bool fiat_ducato_main_menu_tripb_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *tripb_val = fiat_ducato_get_current_tripB_str();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_TRIPB, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, tripb_str[language], tripb_val);
}

static bool fiat_ducato_main_menu_autoclose_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *autoclose_val = fiat_ducato_get_current_autoclose_str();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_AUTOCLOSE, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, autoclose_str[language], autoclose_val);
}

static bool fiat_ducato_main_menu_lightsens_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *str = "1";
    enum FIAT_LIGHT_SENS_LEVEL light = fiat_ducato_get_lightsensor_level();
    switch (light) {
        case FIAT_LIGHT_SENS_LEVEL1:
            str = "1";
            break;
        case FIAT_LIGHT_SENS_LEVEL2:
            str = "2";
            break;
        case FIAT_LIGHT_SENS_LEVEL3:
            str = "3";
            break;
        default:
        case FIAT_LIGHT_SENS_LEVEL0:
            str = "1";
            break;
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_LIGHT_SENSOR, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, lightsensor_level_str[language], str);
}

static bool fiat_ducato_main_menu_laneassist_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *mute_laneassist_val = fiat_ducato_get_current_mute_sidesense_str();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, FIAT_DUCATO_MENU_MUTE_LANEASSIST, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, mute_laneassist_str[language], mute_laneassist_val);
}

void fiat_ducato_main_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, fiat_ducato_main_menu_header);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_VEHICLE_SELECTION, fiat_ducato_main_menu_vehicle_selection);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_UNITS_SELECTION, fiat_ducato_main_menu_units_selection);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_TRIPB_SELECTION, fiat_ducato_main_menu_tripb_selection);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_AUTOCLOSE_SELECTION, fiat_ducato_main_menu_autoclose_selection);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_LIGHTSENS_SELECTION, fiat_ducato_main_menu_lightsens_selection);
    menu_manager_enable_bit_func(mngr, FIAT_DUCATO_MAIN_MENU_LANEASSIST_SELECTION, fiat_ducato_main_menu_laneassist_selection);
}

static inline void handle_oem_radio_selection_save(void) {
    if (current_config != vers_selected) {
        settings_write(VEHICLE_TYPE, vers_selected);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        factory_reset_activate();
    }
}

static inline void handle_lightsens_menu(const uint8_t row_pressed,
                                         const uint8_t menu_index, const uint8_t menu_data) {
    (void)row_pressed;
    (void)menu_index;
    (void)menu_data;
    enum FIAT_LIGHT_SENS_LEVEL val = fiat_ducato_get_lightsensor_level();
    val++;
    if (val >= FIAT_LIGHT_SENS_LEVEL0) {
        val = FIAT_LIGHT_SENS_LEVEL3;
    }
    fiat_ducato_change_lightsensor_level(val);
    enable_radio_menu_refresh(fiat_ducato_main_menu);
    menu_walking_push(FIAT_DUCATO_MENU_MAIN);
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_autoclose_menu(const uint8_t row_pressed,
                                         const uint8_t menu_index, const uint8_t menu_data) {
    (void)menu_index;
    (void)row_pressed;
    (void)menu_data;
    /// Una volta verificato che il menu visualizzato sulla radio e' quello corretto...
    /// leggo il dato della riga premuta ed imposto l'opzione corretta.
    if (fiat_ducato_get_autoclose()) {
        fiat_ducato_change_autoclose(false);
    } else {
        fiat_ducato_change_autoclose(true);
    }
    ///e quindi richiedo la ritrasmissione del menu.
    enable_radio_menu_refresh(fiat_ducato_main_menu);
    menu_walking_push(FIAT_DUCATO_MENU_MAIN);
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_tripb_menu(const uint8_t row_pressed,
                                     const uint8_t menu_index, const uint8_t menu_data) {
    (void)menu_index;
    (void)row_pressed;
    (void)menu_data;
    /// Una volta verificato che il menu visualizzato sulla radio e' quello corretto...
    /// leggo il dato della riga premuta ed imposto l'opzione corretta.
    if (fiat_ducato_get_tripb()) {
        fiat_ducato_change_tripb(false);
    } else {
        fiat_ducato_change_tripb(true);
    }
    ///e quindi richiedo la ritrasmissione del menu.
    enable_radio_menu_refresh(fiat_ducato_main_menu);
    menu_walking_push(FIAT_DUCATO_MENU_MAIN);
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_main_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    if (!(row_pressed == 00 && menu_index == 00 && menu_data == 01)) {
        switch (menu_data) {
            case FIAT_DUCATO_MENU_SELECTION:
                enable_radio_menu_refresh(fiat_ducato_selection_menu);
                menu_walking_push(FIAT_DUCATO_MENU_SELECTION);
                break;
            case FIAT_DUCATO_MENU_SELECTION_ENTRY:
                enable_radio_menu_refresh(fiat_ducato_selection_entry_menu);
                menu_walking_push(FIAT_DUCATO_MENU_SELECTION_ENTRY);
                break;
            case FIAT_DUCATO_MENU_UNITS:
                enable_radio_menu_refresh(fiat_ducato_units_menu);
                menu_walking_push(FIAT_DUCATO_MENU_UNITS);
                break;
            case FIAT_DUCATO_MENU_TRIPB:
                handle_tripb_menu(row_pressed, menu_index, menu_data);
                break;
            case FIAT_DUCATO_MENU_AUTOCLOSE:
                handle_autoclose_menu(row_pressed, menu_index, menu_data);
                break;
            case FIAT_DUCATO_MENU_LIGHT_SENSOR:
                handle_lightsens_menu(row_pressed, menu_index, menu_data);
                break;
            case FIAT_DUCATO_MENU_MUTE_LANEASSIST:
                LOG_DEF_NORMAL("Da fare mute laneassist\r\n");
                if (fiat_ducato_lane_assist_warning()) {
                    fiat_ducato_set_lane_assist_warning(false);
                } else {
                    fiat_ducato_set_lane_assist_warning(true);
                }
                menu_walking_push(FIAT_DUCATO_MENU_MAIN);
                enable_radio_menu_refresh(fiat_ducato_main_menu);
                break;
            default:
                enable_radio_menu_refresh(fiat_ducato_main_menu);
                menu_walking_push(FIAT_DUCATO_MENU_MAIN);
                break;
        }
    } else {
        menu_walking_push(FIAT_DUCATO_MENU_MAIN);
        enable_radio_menu_refresh(fiat_ducato_main_menu);
    }
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_selection_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    (void)row_pressed;
    (void)menu_index;
    vers_selected = menu_data;
    menu_walking_pop();
    enable_radio_menu_refresh(fiat_ducato_selection_entry_menu);
}
static inline void handle_vehicle_menu_units(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    LOG_DEF_NORMAL("%02X %02X %02X\r\n", row_pressed, menu_index, menu_data);
    switch (menu_data) {
        case FIAT_DUCATO_MENU_MEASURE: {
            enum FIAT_CONSUMPTION meas = fiat_ducato_get_measure_unit();
            if (meas == FIAT_CONSUMPTION_L_100KM) {
                meas = FIAT_CONSUMPTION_KM_L;
            } else if (meas == FIAT_CONSUMPTION_KM_L) {
                meas = FIAT_CONSUMPTION_MIL_GAL;
            } else {
                meas = FIAT_CONSUMPTION_L_100KM;
            }
            fiat_ducato_change_measureunit(meas);
        } break;
        case FIAT_DUCATO_MENU_TEMP: {
            enum FIAT_TEMPERATURE temp = fiat_ducato_get_temperature();
            if (temp == FIAT_TEMPERATURE_CELSIUS) {
                temp = FIAT_TEMPERATURE_FAHRENHEIT;
            } else {
                temp = FIAT_TEMPERATURE_CELSIUS;
            }
            fiat_ducato_change_temperature(temp);
        }
    }
    menu_walking_push(FIAT_DUCATO_MENU_UNITS);
    enable_radio_menu_refresh(fiat_ducato_units_menu);
}
static inline void handle_vehicle_selection_menu_entry(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    if (!(row_pressed == 00 && menu_index == 00 && menu_data == 01)) {
        switch (menu_data) {
            case FIAT_DUCATO_MENU_SELECTION:
                enable_radio_menu_refresh(fiat_ducato_selection_menu);
                menu_walking_push(FIAT_DUCATO_MENU_SELECTION);
                break;
            case 0x02:
                handle_oem_radio_selection_save();
                menu_walking_push(FIAT_DUCATO_MENU_SELECTION_ENTRY);
                break;
            default:
                enable_radio_menu_refresh(fiat_ducato_selection_entry_menu);
                menu_walking_push(FIAT_DUCATO_MENU_SELECTION_ENTRY);
                break;
        }
    } else {
        menu_walking_push(FIAT_DUCATO_MENU_SELECTION_ENTRY);
        enable_radio_menu_refresh(fiat_ducato_selection_entry_menu);
    }
}

static inline void handle_menu_data_selection(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        ///Ricevuta una richiesta di trasmissione di menu
        if (msg->as_menu_request.msg_type == MENU_REQUEST_NUM_E8) {
            switch (menu_walking_current()) {
            }
        }
    }
}

static void handle_config_setup(const union EVENT *const msg) {
    (void)msg;
    static bool configured = false;
    if (!configured) {
        vers_selected = current_config = settings_read(VEHICLE_TYPE);
        if (vers_selected != FIAT_DUCATO_VP2) {
            fiat_ducato_menu_disable_VP2_lines();
        }
        configured = true;
    }
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_general_settings(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
        ///Ricevuta una richiesta di trasmissione di menu
        if (msg->as_menu_request.msg_type == MENU_REQUEST_NUM_E5) {
            const uint8_t row_pressed =
                msg->as_menu_request.request.as_e5.row_pressed_num;
            const uint8_t menu_index =
                msg->as_menu_request.request.as_e5.menu_index;
            const uint8_t menu_data =
                msg->as_menu_request.request.as_e5.menu_data;
            uint8_t current_menu = menu_walking_current();
            if ((current_menu == 0x00) && (menu_data == FIAT_DUCATO_MENU_MAIN)) {
                LOG_DEF_NORMAL("Ducato main menu\r\n");
                handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
            } else if (current_menu >= FIAT_DUCATO_MENU_MAIN) {
                switch (current_menu) {
                    case FIAT_DUCATO_MENU_MAIN:
                        handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
                        break;
                    case FIAT_DUCATO_MENU_SELECTION:
                        handle_vehicle_selection_menu(row_pressed, menu_index, menu_data);
                        break;
                    case FIAT_DUCATO_MENU_SELECTION_ENTRY:
                        handle_vehicle_selection_menu_entry(row_pressed, menu_index, menu_data);
                        break;
                    case FIAT_DUCATO_MENU_UNITS:
                        handle_vehicle_menu_units(row_pressed, menu_index, menu_data);
                        break;
                    default:
                        LOG_DEF_ERROR("Vehicle: Unknown menu %02X %02X %02X %02X\r\n", current_menu, row_pressed, menu_index, menu_data);
                        break;
                }
            }
        }
    }
}

void fiat_ducato_menu_init(void) {
    fiat_ducato_selection_menu = menu_manager_create();
    fiat_ducato_selection_menu_fill_data(fiat_ducato_selection_menu);
    fiat_ducato_selection_entry_menu = menu_manager_create();
    fiat_ducato_selection_menu_entry_fill_data(fiat_ducato_selection_entry_menu);
    fiat_ducato_units_menu = menu_manager_create();
    fiat_ducato_units_menu_fill_data(fiat_ducato_units_menu);
    fiat_ducato_main_menu = menu_manager_create();
    fiat_ducato_main_menu_fill_data(fiat_ducato_main_menu);

    alpine_menu_disable_lin_management();

    event_connect_callback(eEVENT_MENU_REQUEST, handle_vehicle_general_settings);
    event_connect_callback(eEVENT_MENU_REQUEST, handle_menu_data_selection);
    event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);
}