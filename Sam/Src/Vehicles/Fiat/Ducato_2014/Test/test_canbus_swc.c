#include <cmocka.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "fiat_ducato.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    (void)can_conf;
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS ign) {
    check_expected(ign);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void test_fiat_ducato_swc() {
    union EVENT event;

    //Invio volume+
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x6284000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_swc(&event);

    //Rilascio volume+
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Volume -
    event.as_can_message.msg.can_data.byte0 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Mute
    event.as_can_message.msg.can_data.byte0 = 0x20;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek+
    event.as_can_message.msg.can_data.byte0 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek-
    event.as_can_message.msg.can_data.byte0 = 0x08;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Source
    event.as_can_message.msg.can_data.byte0 = 0x04;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Voice
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Tel Dial
    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Tel Hangup
    event.as_can_message.msg.can_data.byte0 = 0x01;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);
}

int setup(void **state) {
    (void)state;
    last_seen_ign_on = NEVER_SAW_IGNITION;
    last_tick = 0;
    door = DOOR_UNKNOWN;
    ignition = IGNITION_OFF;
    NumLoops = 2;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_swc, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
