#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato_analog_swc.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void wisker_reset(void) {
}

void event_out_service_emit(void) {
}

void settings_write(uint8_t reg_num, uint8_t data) {
    (void)reg_num;
    (void)data;
}

void settings_factory_reset(void) {
}

// void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf){
// 	(void)can_conf;
// }

void __wrap_event_ignition_emit(enum IGNITION_STATUS ign) {
    check_expected(ign);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_fiat_ducato_ignition_init(void) {
    function_called();
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

void __wrap_analogswc_init(void) {
    function_called();
}

void test_fiat_ducato_analog_swc_init(void **state) {
    (void)state;
    expect_function_call(__wrap_analogswc_init);
    fiat_ducato_analog_swc_init();
}

void test_fiat_ducato_analog_swc(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_ANALOG_VALUE;
    event.as_analog_value.swc_analog.channel_a = -1;
    event.as_analog_value.swc_analog.channel_b = -1;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 121;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 249;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 50;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 119;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 48;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 251;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 599;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = -1;
    event.as_analog_value.swc_analog.channel_b = 121;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 248;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 50;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 119;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_analog_swc(&event);

    radio_media_type = RADIO_MEDIA_PHONE;

    event.as_analog_value.swc_analog.channel_b = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    handle_analog_swc(&event);

    radio_media_type = RADIO_MEDIA_TUNER;

    event.as_analog_value.swc_analog.channel_b = 48;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    handle_analog_swc(&event);

    radio_media_type = RADIO_MEDIA_PHONE;

    event.as_analog_value.swc_analog.channel_b = 251;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    handle_analog_swc(&event);

    radio_media_type = RADIO_MEDIA_TUNER;

    event.as_analog_value.swc_analog.channel_b = 599;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = -1;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_analog_swc(&event);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_analog_swc(&event);
}

void test_handle_source_type(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_RADIO_MEDIA_CHANGED;
    event.as_radio_source.media_type = RADIO_MEDIA_PHONE;
    handle_source_type(&event);
    assert_true(radio_media_type == RADIO_MEDIA_PHONE);

    event.as_radio_source.media_type = RADIO_MEDIA_TUNER;
    handle_source_type(&event);
    assert_true(radio_media_type == RADIO_MEDIA_TUNER);
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    radio_media_type = RADIO_MEDIA_UNKNOWN;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_analog_swc_init, setup),
        cmocka_unit_test_setup(test_fiat_ducato_analog_swc, setup),
        cmocka_unit_test_setup(test_handle_source_type, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}