#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato.c"
int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void speed_init(void) {
}

uint32_t get_hw_speed(void) {
    return 0;
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    (void)can_conf;
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS ign) {
    check_expected(ign);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_fiat_ducato_ignition_init(void) {
    function_called();
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

enum FIAT_REVERSE_WITH_DELAY __wrap_fiat_ducato_reverse_gear_with_delay() {
    return mock_type(enum FIAT_REVERSE_WITH_DELAY);
}

void __wrap_fiat_ducato_analog_swc_init(void) {
    function_called();
}

void __wrap_fiat_ducato_vp1_ack_init(void) {
    function_called();
}

void __wrap_fiat_ducato_vp2_ack_init(void) {
    function_called();
}

void __wrap_fiat_ducato_extra_init(void) {
    function_called();
}

void __wrap_fiat_ducato_menu_init(void) {
    function_called();
}

void __wrap_fiat_ducato_canswc_init(void) {
    function_called();
}

void __wrap_alpine_menu_disable_clock_management(void) {
    function_called();
}

void __wrap_fiat_ducato_laneassist_init(void) {
    function_called();
}

void test_fiat_ducato_init(void **state) {
    (void)state;
    expect_value(__wrap_canbus_add_accepted_id, num, 0x04394000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x04214001);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x06214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_function_call(__wrap_fiat_ducato_ignition_init);
    expect_function_call(__wrap_fiat_ducato_menu_init);
    expect_function_call(__wrap_fiat_ducato_laneassist_init);
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 50;
    can_conf.can0.listen_mode = false;
    fiat_ducato_init(&can_conf);
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    configured = false;
    return 0;
}

void test_handle_reverse(void **state) {
    (void)state;
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);

    CAN_MESSAGE msg1 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_revmov(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);
}

void test_handle_speedhandbrake(void **state) {
    (void)state;
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x4394000, true, {0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_in_range(__wrap_event_speed_emit, status, (0x1000 * 25 / 4) - 5, (0x1000 * 25 / 4) + 5);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte0 = 0x00;
    expect_value(__wrap_event_speed_emit, status, 0);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    handle_speed(&event);
}

void test_init_opendash(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_SETTINGS_READ;
    event.as_settings_read.type = SETTING_READ_VEHICLE_TYPE;
    will_return(__wrap_settings_read, FIAT_DUCATO_OPENDASH);
    expect_function_call(__wrap_fiat_ducato_analog_swc_init);
    expect_function_call(__wrap_alpine_menu_disable_clock_management);
    handle_config_setup(&event);
}

void test_init_not_defined(void **state) {
    (void)state;
    //Non definito, inizializza opendash
    union EVENT event;
    event.as_generic.event = eEVENT_SETTINGS_READ;
    event.as_settings_read.type = SETTING_READ_VEHICLE_TYPE;
    will_return(__wrap_settings_read, -1);
    expect_function_call(__wrap_fiat_ducato_analog_swc_init);
    expect_function_call(__wrap_alpine_menu_disable_clock_management);
    handle_config_setup(&event);
}

void test_init_vp1(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_SETTINGS_READ;
    event.as_settings_read.type = SETTING_READ_VEHICLE_TYPE;
    will_return(__wrap_settings_read, FIAT_DUCATO_VP1);
    expect_function_call(__wrap_fiat_ducato_vp1_ack_init);
    expect_function_call(__wrap_fiat_ducato_canswc_init);
    expect_function_call(__wrap_alpine_menu_disable_clock_management);
    handle_config_setup(&event);
}

void test_init_vp2(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_SETTINGS_READ;
    event.as_settings_read.type = SETTING_READ_VEHICLE_TYPE;
    will_return(__wrap_settings_read, FIAT_DUCATO_VP2);
    expect_function_call(__wrap_fiat_ducato_vp2_ack_init);
    expect_function_call(__wrap_fiat_ducato_extra_init);
    expect_function_call(__wrap_fiat_ducato_canswc_init);
    handle_config_setup(&event);
}

void test_ducato_led_color(void **state) {
    (void)state;
    assert_true(vehicle_get_default_led_color() == LED_COLOR_ORANGE);
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_init, setup),
        cmocka_unit_test_setup(test_handle_reverse, setup),
        cmocka_unit_test_setup(test_handle_speedhandbrake, setup),
        cmocka_unit_test_setup(test_init_opendash, setup),
        cmocka_unit_test_setup(test_init_not_defined, setup),
        cmocka_unit_test_setup(test_init_vp1, setup),
        cmocka_unit_test_setup(test_init_vp2, setup),
        cmocka_unit_test_setup(test_ducato_led_color, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
