
#include "fiat_definitions.h"

/**
 * @brief Converte un intero in formato BCD e lo converte nel numero decimale.
 * Prende un intero in formato 0x15 e lo converte in un decimale
 * per esempio: 0x15 => 15. Serve perchè molte vetture utilizzano
 * la scrittura dei dati su canbus in formato "numero" e non viene utilizzata
 * la conversione in esadecimale.
 */
uint8_t hex_to_dec(uint8_t byte0){
	uint8_t retVal = byte0 / 16 * 10;
	uint8_t tmp = byte0 % 16;
	retVal += tmp;
	return retVal;

}

/**
 * @brief Converte due byte in formato BCD e li converte nel numero decimale.
 * Prende due byte e li converte in un intero utilizzando lo stesso principio
 * esposto per hex_to_dec. il dato 0x20 0x15 viene convertito nell'anno 2015
 * @param byte0 la parte alta del dato.
 * @param byte1 la parte bassa del dato.
 * @return il dato corrispondente ai due byte BCD convertito in decimale.
 */
uint16_t year_hex_to_dec(uint8_t byte0, uint8_t byte1){
	return (hex_to_dec(byte0) * 100) + hex_to_dec(byte1);
}
