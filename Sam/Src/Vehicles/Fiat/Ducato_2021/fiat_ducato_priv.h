#pragma once

#include <stdbool.h>
#include "settings.h"

//enum FIAT_DUCATO_VERSION {
//    FIAT_DUCATO_OPENDASH,
//    FIAT_DUCATO_VP1,
//    FIAT_DUCATO_VP2,
//    FIAT_DUCATO_7INCH,
//    FIAT_DUCATO_OPEN_DASH_52WAYS,
//    FIAT_DUCATO_VERSION_COUNT
//};
//
//enum FIAT_SETTINGS {
//    FIAT_SETTING_REVERSE = VEHICLE_SETTINGS_START,
//    FIAT_SETTING_MUTE_LANEASSIST,
//    FIAT_SETTING_LAST  //Non mettere niente sotto a questa. Al momento disponibile per essere usato.
//};
//
////Se questa chiamata da errore, la enum sopra va in overlap con qualcos'altro.
//typedef char fiat_ducato_array_border_check[(FIAT_SETTING_LAST) < (VEHICLE_SETTINGS_START + VEHICLE_SETTINGS_SIZE) ? 1 : -1];
//
//void fiat_ducato_analog_swc_init(void);
//void fiat_ducato_menu_init(void);
//void fiat_ducato_ignition_init(void);
//void fiat_ducato_extra_init(void);
//void fiat_ducato_extra_info_init(void);
//void fiat_ducato_canswc_init(void);
//void fiat_ducato_laneassist_init(void);
//void fiat_ducato_vp1_ack_init(void);
//void fiat_ducato_vp2_ack_init(void);
//void fiat_ducato_parksens_init(void);
//
//enum FIAT_LANGUAGE fiat_ducato_get_language(void);
//enum FIAT_REVERSE_WITH_DELAY fiat_ducato_reverse_gear_with_delay(void);
//enum FIAT_DUCATO_VERSION fiat_ducato_get_version(void);
//void fiat_ducato_set_version(enum FIAT_DUCATO_VERSION version);
//enum IGNITION_STATUS fiat_ducato_can_ignition(void);
//enum IGNITION_STATUS fiat_ducato_can_ignition_raw(void);
//void fiat_ducato_send_changed_clock_date(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute);
//bool fiat_ducato_set_extra(uint8_t byte0);
//bool fiat_ducato_set_time_visualization(uint8_t byte0);
//bool fiat_ducato_set_tripb(uint8_t byte);
//bool fiat_ducato_change_lightsensor_level(enum FIAT_LIGHT_SENS_LEVEL light);
//bool fiat_ducato_set_lightsensor_level(enum FIAT_LIGHT_SENS_LEVEL light);
//enum FIAT_LIGHT_SENS_LEVEL fiat_ducato_get_lightsensor_level(void);
//bool fiat_ducato_set_language(uint8_t byte0, uint8_t byte1);
//bool fiat_ducato_set_autoclose(uint8_t byte);
//bool fiat_ducato_get_tripb(void);
//bool fiat_ducato_get_autoclose(void);
//enum FIAT_MUTE_ON_LANE_ASSIST_ALERT fiat_ducato_lane_assist_warning(void);
//enum FIAT_CONSUMPTION fiat_ducato_get_measure_unit(void);
//void fiat_ducato_set_lane_assist_warning(bool value);
//void fiat_ducato_change_autoclose(bool val);
//bool fiat_ducato_set_tripb(uint8_t byte);
//void fiat_ducato_change_tripb(bool enable);
//enum FIAT_TEMPERATURE fiat_ducato_get_temperature(void);
//void fiat_ducato_change_measureunit(enum FIAT_CONSUMPTION consumption);
//void fiat_ducato_change_temperature(enum FIAT_TEMPERATURE temp);
//

void fiat_ducato_ignition_init(void);
void fiat_ducato_analog_swc_init(void);
void fiat_ducato_can_swc_init(void);
void fiat_ducato_lin_swc_init(void);