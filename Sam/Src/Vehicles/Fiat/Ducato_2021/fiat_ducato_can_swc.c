#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2021.h"
#include "fiat_ducato_priv.h"

/**
 * @brief Comando a volante al momento gestito. Utilizzato dal parser dei comandi a volante.
 * 
 */
static bool swc_can_active = false;
/**
 * @brief Stato del parser dei comandi a volante. Attivo / Non attivo
 * 
 */
static enum SWC cur_can_active = SWC_NO_BUTTON_PRESSED;
static enum SWC old_can_active = SWC_NO_BUTTON_PRESSED;

/**
 * @brief Stato del parser dei comandi sulla mascherina. Attivo / Non attivo.
 * 
 */

static void handle_can_swc(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x2EE;
            if (msg->as_can_message.msg.can_id == id) {
                uint32_t tmp = (((msg->as_can_message.msg.can_data.byte0 << 24)) + (msg->as_can_message.msg.can_data.byte1 << 16) + (msg->as_can_message.msg.can_data.byte2 << 8) + (msg->as_can_message.msg.can_data.byte3));
                if (tmp == 0x00000001) {
                    cur_can_active = SWC_VOL_UP_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00000004) {
                    cur_can_active = SWC_VOL_DOWN_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00000010) {
                    cur_can_active = SWC_MUTE_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00000040) {
                    cur_can_active = SWC_SEEK_UP_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00000100) {
                    cur_can_active = SWC_SEEK_DOWN_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00010000) {
                    cur_can_active = SWC_SOURCE_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00001000) {
                    cur_can_active = SWC_TEL_PICKUP_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00004000) {
                    cur_can_active = SWC_TEL_HANGUP_PRESSED;
                    swc_can_active = true;
                } else if (tmp == 0x00040000) {
                    cur_can_active = SWC_SPEECH_PRESSED;
                    swc_can_active = true;
                } else {
                    if (swc_can_active) {
                        cur_can_active++;
                        swc_can_active = false;
                    } else {
                        cur_can_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                if (old_can_active != cur_can_active) {
                    event_swc_emit(cur_can_active, SWC_MODULE_VEHICLE);
                    old_can_active = cur_can_active;
                }
            }
        }
    }
}

void fiat_ducato_can_swc_init(void) {
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x2EE);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_can_swc);
}