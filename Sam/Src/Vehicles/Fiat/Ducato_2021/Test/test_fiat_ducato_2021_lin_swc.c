#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato_lin_swc.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

void __wrap_event_swc_emit(enum SWC status, enum SWC_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void test_fiat_ducato_lin_swc_init(void **state) {
    (void)state;

    expect_value(__wrap_event_connect_callback, event, eEVENT_LIN_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_lin_swc);

    fiat_ducato_lin_swc_init();
}

void test_handle_lin_swc(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_LIN_RX_MESSAGE;
    event.as_lin_message.msg.lin_break = 0x00;
    event.as_lin_message.msg.lin_sync = 0x55;
    event.as_lin_message.msg.lin_id = 0x02;
    event.as_lin_message.msg.lin_data.byte0 = 0x00;
    event.as_lin_message.msg.lin_data.byte1 = 0x00;
    event.as_lin_message.msg.lin_data.byte2 = 0x00;
    event.as_lin_message.msg.lin_data.byte3 = 0x00;
    handle_lin_swc(&event);

    event.as_lin_message.msg.lin_data.byte0 = 0x80;
    event.as_lin_message.msg.lin_data.byte1 = 0x00;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_lin_swc(&event);

    event.as_lin_message.msg.lin_data.byte0 = 0x40;
    event.as_lin_message.msg.lin_data.byte1 = 0x00;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_lin_swc(&event);

    event.as_lin_message.msg.lin_data.byte0 = 0x00;
    event.as_lin_message.msg.lin_data.byte1 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_lin_swc(&event);
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_lin_swc_init, setup),
        cmocka_unit_test_setup(test_handle_lin_swc, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
