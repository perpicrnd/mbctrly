#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>
#include <stdint.h>
#include "fiat_ducato.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops = 0;

void software_reset(void) {
}
const char interface_code[] = "TEST SOFTWARE";

void settings_write(uint8_t reg_num, uint8_t data) {
    (void)reg_num;
    (void)data;
}

void event_out_service_emit(void) {
}

void event_emit(union EVENT *event) {
    (void)event;
}

uint8_t settings_read(uint8_t reg_num) {
    return reg_num;
}

void __wrap_wisker_reset(MODULES module) {
    check_expected(module);
}

void __wrap_event_swc_emit(enum SWC status, enum SWC_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    check_expected(can_conf->can0.can_speed);
    check_expected(can_conf->can0.listen_mode);
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_alpine_menu_disable_clock_management(void) {
    function_called();
}

void __wrap_alpine_menu_disable_preflight_check(void) {
    function_called();
}

void __wrap_alpine_menu_disable_vehicle_settings(void) {
    function_called();
}

void __wrap_fiat_ducato_ignition_init(void) {
    function_called();
}

void __wrap_fiat_ducato_analog_swc_init(void) {
    function_called();
}

void __wrap_fiat_ducato_can_swc_init(void) {
    function_called();
}

void __wrap_fiat_ducato_lin_swc_init(void) {
    function_called();
}

void test_revmov(void **setup) {
    (void)setup;

    union EVENT event = {0};
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.can_id = 0x3E4;
    event.as_can_message.msg.can_is_ext = false;
    event.as_can_message.msg.datalen = 8;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_data.byte0 = 0x00;
    event.as_can_message.msg.can_data.byte1 = 0x00;
    event.as_can_message.msg.can_data.byte2 = 0x00;
    event.as_can_message.msg.can_data.byte3 = 0x00;
    event.as_can_message.msg.can_data.byte4 = 0x00;
    event.as_can_message.msg.can_data.byte5 = 0x00;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    event.as_can_message.msg.can_data.byte7 = 0x00;
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_revmov(&event);
    event.as_can_message.msg.can_data.byte7 = 0x20;
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);
}

void test_speed(void **setup) {
    (void)setup;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    event.as_can_message.msg.can_id = 0x3E2;
    event.as_can_message.msg.can_is_ext = false;
    event.as_can_message.msg.datalen = 8;
    event.as_can_message.msg.interface = CAN_INTERFACE_0;
    event.as_can_message.msg.can_data.byte0 = 0x00;
    event.as_can_message.msg.can_data.byte1 = 0x00;
    event.as_can_message.msg.can_data.byte2 = 0x00;
    event.as_can_message.msg.can_data.byte3 = 0x00;
    event.as_can_message.msg.can_data.byte4 = 0x00;
    event.as_can_message.msg.can_data.byte5 = 0x00;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    event.as_can_message.msg.can_data.byte7 = 0x00;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_speed_emit, status, 0);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte5 = 0x01;
    event.as_can_message.msg.can_data.byte6 = 0x07;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);

    expect_value(__wrap_event_speed_emit, status, (0x0107 >> 3) * SPEED_MULTIPLIER / SPEED_DIVIDER);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte5 = 0x02;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_speed_emit, status, (0x0200 >> 3) * SPEED_MULTIPLIER / SPEED_DIVIDER);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte5 = 0x20;
    event.as_can_message.msg.can_data.byte6 = 0xA2;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    expect_value(__wrap_event_speed_emit, status, (0x20A2 >> 3) * SPEED_MULTIPLIER / SPEED_DIVIDER);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte5 = 0x00;
    event.as_can_message.msg.can_data.byte6 = 0x00;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_speed_emit, status, 0);
    handle_speed(&event);
}

void test_fiat_ducato_init(void **setup) {
    (void)setup;
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 125;
    can_conf.can0.listen_mode = false;
    expect_value(__wrap_canbus_init, can_conf->can0.can_speed, 125);
    expect_value(__wrap_canbus_init, can_conf->can0.listen_mode, false);
    expect_function_call(__wrap_fiat_ducato_ignition_init);
    expect_function_call(__wrap_fiat_ducato_analog_swc_init);
    expect_function_call(__wrap_fiat_ducato_can_swc_init);
    expect_function_call(__wrap_fiat_ducato_lin_swc_init);

    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x3E4);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_revmov);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x3E2);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_speed);

    expect_function_call(__wrap_alpine_menu_disable_clock_management);
    expect_function_call(__wrap_alpine_menu_disable_vehicle_settings);

    fiat_ducato_init(&can_conf);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_revmov),
        cmocka_unit_test(test_speed),
        cmocka_unit_test(test_fiat_ducato_init),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}