#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato_ignition.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void speed_init(void) {
}

uint32_t get_hw_speed(void) {
    return 0;
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected(handler);
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

void __wrap_display_preflight_check_message(void) {
    function_called();
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    can_ignition = IGNITION_OFF;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;
    return 0;
}

void test_event_fiat_ducato_ignition_emit(void **state) {
    (void)state;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_fiat_ducato_ignition_emit();

    can_ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_fiat_ducato_ignition_emit();

    can_ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_fiat_ducato_ignition_emit();

    wired_ignition_0 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_fiat_ducato_ignition_emit();

    wired_ignition_0 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_fiat_ducato_ignition_emit();

    wired_ignition_1 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_fiat_ducato_ignition_emit();

    wired_ignition_1 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_fiat_ducato_ignition_emit();
}

void test_handle_wired_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);
}

void test_handle_can_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x356, true, {0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));

    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_KEY);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    event.as_can_message.msg.can_data.byte3 = 0x80;
    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_KEY);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    event.as_can_message.msg.can_data.byte2 = 0x00;
    event.as_can_message.msg.can_data.byte3 = 0x00;
    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_KEY);
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_can_ignition(&event);

    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_DOOR);
    event.as_can_message.msg.can_data.byte2 = 0xC0;
    event.as_can_message.msg.can_data.byte3 = 0x80;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_DOOR);
    event.as_can_message.msg.can_data.byte2 = 0x00;
    event.as_can_message.msg.can_data.byte3 = 0x00;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    will_return(__wrap_settings_read, RADIO_SETTING_IGNITION_DOOR);
    event.as_can_message.msg.can_data.byte2 = 0x00;
    event.as_can_message.msg.can_data.byte3 = 0x80;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_can_ignition(&event);
}

void test_handle_preflight_check(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x3E2, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));

    power_on_counter = 0;
    engine_running = true;

    handle_preflight_check(&event);
    assert_true(engine_running == false);

    power_on_counter = 5;
    event.as_can_message.msg.can_data.byte4 = 0x02;
    will_return(__wrap_settings_read, RADIO_PREFLIGHT_CHECK_ON);
    expect_function_call(__wrap_display_preflight_check_message);

    handle_preflight_check(&event);
    assert_true(engine_running == true);
}

void test_fiat_ducato_ignition_init(void **state) {
    (void)state;
    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_ignition);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x356);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_preflight_check);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x3E2);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_type);

    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_seconds_changed);

    expect_value(__wrap_event_connect_callback, event, eEVENT_EXIT_RADIO_MENU);
    expect_value(__wrap_event_connect_callback, handler, handle_check_preflight_check);

    fiat_ducato_ignition_init();
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_ignition_init, setup),
        cmocka_unit_test_setup(test_handle_wired_ignition, setup),
        cmocka_unit_test_setup(test_handle_can_ignition, setup),
        cmocka_unit_test_setup(test_event_fiat_ducato_ignition_emit, setup),
        cmocka_unit_test_setup(test_handle_preflight_check, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
