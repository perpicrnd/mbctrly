#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato_can_swc.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected_ptr(handler);
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_event_swc_emit(enum SWC status, enum SWC_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void test_fiat_ducato_can_swc_init(void **state) {
    (void)state;

    expect_value(__wrap_canbus_add_accepted_id, num, 0x2EE);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_swc);

    fiat_ducato_can_swc_init();
}

void test_handle_can_swc() {
    union EVENT event;

    //Invio volume+
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x2EE, false, {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio volume+
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Volume -
    event.as_can_message.msg.can_data.byte3 = 0x04;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Mute
    event.as_can_message.msg.can_data.byte3 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Seek+
    event.as_can_message.msg.can_data.byte3 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte3 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Seek-
    event.as_can_message.msg.can_data.byte2 = 0x01;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Source
    event.as_can_message.msg.can_data.byte1 = 0x01;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Voice
    event.as_can_message.msg.can_data.byte1 = 0x04;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Tel Dial
    event.as_can_message.msg.can_data.byte2 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Tel Hangup
    event.as_can_message.msg.can_data.byte2 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte2 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    handle_can_swc(&event);
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_can_swc_init, setup),
        cmocka_unit_test_setup(test_handle_can_swc, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}