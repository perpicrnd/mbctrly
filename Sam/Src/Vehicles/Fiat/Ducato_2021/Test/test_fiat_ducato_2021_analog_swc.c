#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "fiat_ducato_analog_swc.c"

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void wisker_reset(void) {
}

void event_out_service_emit(void) {
}

void settings_write(uint8_t reg_num, uint8_t data) {
    (void)reg_num;
    (void)data;
}

void settings_factory_reset(void) {
}

uint8_t __wrap_settings_read(uint8_t reg_num) {
    (void)reg_num;
    return mock_type(uint8_t);
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

void __wrap_analogswc_init(void) {
    function_called();
}

void test_fiat_ducato_analog_swc_init(void **state) {
    (void)state;
    expect_function_call(__wrap_analogswc_init);
    fiat_ducato_analog_swc_init();
}

void test_fiat_ducato_analog_swc(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_ANALOG_VALUE;
    event.as_analog_value.swc_analog.channel_a = -1;
    event.as_analog_value.swc_analog.channel_b = -1;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 1001;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 7400;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 8000;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 12999;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 800;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = 50;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_a = -1;
    event.as_analog_value.swc_analog.channel_b = 100;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 899;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 2500;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = 4900;
    handle_analog_swc(&event);

    event.as_analog_value.swc_analog.channel_b = -1;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    handle_analog_swc(&event);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_analog_swc(&event);
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_fiat_ducato_analog_swc_init, setup),
        cmocka_unit_test_setup(test_fiat_ducato_analog_swc, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
