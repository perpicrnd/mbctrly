#include <stdio.h>
#include "alpine_menu.h"
#include "analogswc.h"
#include "canbus.h"
#include "difference.h"
#include "event_list.h"
#include "event_list_provider.h"
#include "fiat_ducato_2021.h"
#include "fiat_ducato_priv.h"
#include "logger.h"
#include "serials.h"
#include "service.h"
#include "wisker.h"

static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0x3E4;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                if ((msg->as_can_message.msg.can_data.byte7 & 0x60) == 0x20) {
                    event_reverse_emit(REVERSE_ON);
                } else {
                    event_reverse_emit(REVERSE_OFF);
                }
            }
        }
    }
}

#define SPEED_MULTIPLIER 625
#define SPEED_DIVIDER 100
static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0x3E2;
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            uint32_t spd = (((msg->as_can_message.msg.can_data.byte5 << 8) | msg->as_can_message.msg.can_data.byte6) >> 3);
            spd *= SPEED_MULTIPLIER;
            spd /= SPEED_DIVIDER;
            if (spd > 400) {  //velocità > 4 km/h prima c'era spd > 0x0500
                event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
            } else {
                event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
            }
            event_speed_emit(spd);
        }
    }
}

void fiat_ducato_init(struct CANBUS_CONFIG* can_conf) {
    canbus_init(can_conf);
    serial_usart0_init_LIN(19200);
    fiat_ducato_ignition_init();
    fiat_ducato_analog_swc_init();
    fiat_ducato_can_swc_init();
    fiat_ducato_lin_swc_init();

    wisker_disable_module(MODULE_UART_LOWLEVEL);
    wisker_reset(MODULE_VEHICLE);

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x3E4);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x3E2);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);

    alpine_menu_disable_clock_management();
    alpine_menu_disable_vehicle_settings();
}
