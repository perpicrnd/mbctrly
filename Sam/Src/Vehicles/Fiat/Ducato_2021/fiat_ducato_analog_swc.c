#include <stdbool.h>
#include <stdio.h>

#include "analogswc.h"
#include "event_list.h"
#include "fiat_ducato_2021.h"
#include "fiat_ducato_priv.h"
#include "logger.h"

/// @brief Limite comando a volante.
#define MUTE_MIN 0
/// @brief Limite comando a volante.
#define MUTE_MAX 1000
/// @brief Limite comando a volante.
#define VOL_UP_MIN 1001
/// @brief Limite comando a volante.
#define VOL_UP_MAX 7500
/// @brief Limite comando a volante.
#define VOL_DOWN_MIN 7501
/// @brief Limite comando a volante.
#define VOL_DOWN_MAX 13000
/// @brief Limite comando a volante.
#define SOURCE_MIN 0
/// @brief Limite comando a volante.
#define SOURCE_MAX 900

#define SEEK_UP_MIN 901
/// @brief Limite comando a volante.
#define SEEK_UP_MAX 2000
/// @brief Limite comando a volante.
#define SEEK_DOWN_MIN 2001
/// @brief Limite comando a volante.
#define SEEK_DOWN_MAX 5000

static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static enum SWC old_active = SWC_NO_BUTTON_PRESSED;

void handle_analog_swc(const union EVENT* const event) {
    if (event->as_generic.event == eEVENT_ANALOG_VALUE) {
        uint32_t res_a = event->as_analog_value.swc_analog.channel_a;
        uint32_t res_b = event->as_analog_value.swc_analog.channel_b;

        if ((res_a >= VOL_UP_MIN) && (res_a < VOL_UP_MAX)) {
            cur_active = SWC_VOL_UP_PRESSED;
            swc_active = true;
        } else if ((res_a >= VOL_DOWN_MIN) && (res_a < VOL_DOWN_MAX)) {
            cur_active = SWC_VOL_DOWN_PRESSED;
            swc_active = true;
        } else if (res_a < MUTE_MAX) {
            cur_active = SWC_MUTE_PRESSED;
            swc_active = true;
        } else if (res_b < SOURCE_MAX) {
            cur_active = SWC_SOURCE_PRESSED;
            swc_active = true;
        } else if ((res_b >= SEEK_UP_MIN) && (res_b < SEEK_UP_MAX)) {
            cur_active = SWC_SEEK_UP_PRESSED;
            swc_active = true;
        } else if ((res_b >= SEEK_DOWN_MIN) && (res_b < SEEK_DOWN_MAX)) {
            cur_active = SWC_SEEK_DOWN_PRESSED;
            swc_active = true;
        } else {
            if (swc_active == true) {
                swc_active = false;
                cur_active++;
            } else {
                cur_active = SWC_NO_BUTTON_PRESSED;
            }
        }
        if (old_active != cur_active) {
            event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
            old_active = cur_active;
        }
    }
}

void fiat_ducato_analog_swc_init(void) {
    event_connect_callback(eEVENT_ANALOG_VALUE, handle_analog_swc);
    analogswc_init();
}