#include "alpine_menu.h"
#include "alpine_task.h"
#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2021.h"
#include "fiat_ducato_priv.h"
#include "vehicle.h"

enum DOOR {
    DOOR_UNKNOWN = 0,
    DOOR_OPEN,
    DOOR_CLOSE
};

static enum IGNITION_STATUS can_ignition_raw = IGNITION_OFF;
static enum IGNITION_STATUS can_ignition = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;

enum IGNITION_STATUS fiat_ducato_can_ignition(void) {
    return can_ignition;
}

enum IGNITION_STATUS fiat_ducato_can_ignition_raw(void) {
    return can_ignition_raw;
}

static void event_fiat_ducato_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON) ||
        (can_ignition == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
        vehicle_set_sync_clock(false);
    }
    event_ignition_emit(ignition);
}

static void handle_can_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE* const message = &msg->as_can_message.msg;
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == 0x356) {
                enum IGNITION_STATUS tmp = IGNITION_OFF;
                enum DOOR door = DOOR_CLOSE;

                if ((message->can_data.byte2 & 0xC0) != 0x00) {
                    tmp = IGNITION_ON;
                }
                if ((message->can_data.byte3 & 0x80) == 0x80) {
                    door = DOOR_OPEN;
                }

                enum RADIO_SETTING_IGNITION ign_setting = (enum RADIO_SETTING_IGNITION)settings_read(RADIO_SETTING_IGNITION_BYTE);
                if (tmp == IGNITION_ON) {
                    can_ignition_raw = IGNITION_ON;
                    can_ignition = IGNITION_ON;
                } else {
                    can_ignition_raw = IGNITION_OFF;
                    if (ign_setting == RADIO_SETTING_IGNITION_DOOR) {
                        if (door == DOOR_OPEN) {
                            can_ignition = IGNITION_OFF;
                        }
                    } else {
                        can_ignition = IGNITION_OFF;
                    }
                }
                event_fiat_ducato_ignition_emit();
            }
        }
    }
}

static void handle_wired_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_fiat_ducato_ignition_emit();
    }
}

#define POWER_ON_COUNTER_TRIG 5
static enum RADIO_SOURCE_TYPE radio_source_type = RADIO_SOURCE_OFF;
static bool engine_running = false;
static uint8_t power_on_counter = 0;

static void handle_check_preflight_check(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_EXIT_RADIO_MENU) {
        if (preflight_check_menu_was_displayed()) {
            display_preflight_check_message();
            engine_running = true;
            preflight_check_menu_displayed_reset();
        }
    }
}

//0x3E2 byte 4: EngineSts (bit 0,1) = 0x00 engine OFF, =0x02 engine ON

static void handle_preflight_check(const union EVENT* const msg) {
    const uint32_t id = 0x3E2;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                if ((msg->as_can_message.msg.can_data.byte4 & 0x02) == 0x02) {
                    if (power_on_counter >= POWER_ON_COUNTER_TRIG) {
                        if (engine_running == false) {
                            if (settings_read(RADIO_SETTING_PREFLIGHT_CHECK_BYTE) == RADIO_PREFLIGHT_CHECK_ON) {
                                display_preflight_check_message();
                                engine_running = true;
                            }
                        }
                    }
                } else {
                    engine_running = false;
                }
            }
        }
    }
}

static void handle_seconds_changed(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_SECONDS_CHANGED) {
        if (radio_source_type == RADIO_SOURCE_OFF) {
            power_on_counter = 0;
        } else {
            if (power_on_counter < POWER_ON_COUNTER_TRIG) {
                power_on_counter++;
                LOG_DEF_NORMAL("Delay\r\n");
            }
        }
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source_type = msg->as_radio_source.source_type;
        LOG_DEF_NORMAL("Current radio source type: %d\r\n", radio_source_type);
    }
}

void fiat_ducato_ignition_init(void) {
    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_can_ignition);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x356);

    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_preflight_check);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x3E2);

    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_seconds_changed);
    event_connect_callback(eEVENT_EXIT_RADIO_MENU, handle_check_preflight_check);
}