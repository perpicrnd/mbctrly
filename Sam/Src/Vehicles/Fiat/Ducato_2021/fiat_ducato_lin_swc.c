#include "canbus.h"
#include "event_list.h"
#include "fiat_ducato_2021.h"
#include "fiat_ducato_priv.h"

static enum SWC cur_lin_active = SWC_NO_BUTTON_PRESSED;
static enum SWC old_lin_active = SWC_NO_BUTTON_PRESSED;
static bool swc_lin_active = false;

void handle_lin_swc(const union EVENT* const event) {
    if (event->as_generic.event == eEVENT_LIN_RX_MESSAGE) {
        uint8_t break_B = (uint8_t)event->as_lin_message.msg.lin_break;
        uint8_t sync_B = (uint8_t)event->as_lin_message.msg.lin_sync;
        uint8_t id_B = (uint8_t)event->as_lin_message.msg.lin_id;
        //TODO: verificare checksum
        if ((break_B == 0x00) && (sync_B == 0x55) && (id_B == 0x02)) {
            uint16_t tmp = 0;
            uint8_t data = (uint8_t)event->as_lin_message.msg.lin_data.byte0;
            tmp |= data << 8;
            data = (uint8_t)event->as_lin_message.msg.lin_data.byte1;
            tmp |= data;
            // LOG_DEF_NORMAL("***LIN_MSG:%d\r\n", (int)tmp);
            if (tmp == 0x8000) {
                cur_lin_active = SWC_TEL_HANGUP_PRESSED;
                swc_lin_active = true;
            } else if (tmp == 0x4000) {
                cur_lin_active = SWC_TEL_PICKUP_PRESSED;
                swc_lin_active = true;
            } else if (tmp == 0x0002) {
                cur_lin_active = SWC_SPEECH_PRESSED;
                swc_lin_active = true;
            } else {
                if (swc_lin_active == true) {
                    swc_lin_active = false;
                    cur_lin_active++;
                } else {
                    cur_lin_active = SWC_NO_BUTTON_PRESSED;
                }
            }
            if (cur_lin_active != old_lin_active) {
                event_swc_emit(cur_lin_active, SWC_MODULE_VEHICLE);
                old_lin_active = cur_lin_active;
            }
        }
    }
}

void fiat_ducato_lin_swc_init(void) {
    event_connect_callback(eEVENT_LIN_RX_MESSAGE, handle_lin_swc);
}