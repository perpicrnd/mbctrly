#include "daily_2016_dabradio.h"
#include <stdio.h>

#include "FreeRTOS.h"
#include "canbus.h"
#include "difference.h"
#include "event_list.h"
#include "logger.h"
#include "timers.h"
#include "wisker.h"

static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;

static void event_iveco_daily_2016_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
    }
    event_ignition_emit(ignition);
}

static void handle_wired_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_iveco_daily_2016_ignition_emit();
    }
}

static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0x18FFC321;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x02) == 0x02) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}
static void handle_lights(const union EVENT* const msg) {
    const uint32_t id = 0x18FFC321;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte0 & 0x80) == 0x80) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
        }
    }
}

static uint32_t old_speed;
static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0xCF00203;
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                tmp = ((msg->as_can_message.msg.can_data.byte2) * 100);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if (DIFF(old_speed, tmp) != 0) {
                    event_speed_emit(tmp);
                    old_speed = tmp;
                }
            }
        }
    }
}

static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static void handle_swc(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x06284000;
            if (msg->as_can_message.msg.can_id == id) {
                uint16_t tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp == 0x8000) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x4000) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x2000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x1000) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0800) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0040) {
                    cur_active = SWC_SPEECH_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0200) {
                    cur_active = SWC_TEL_PICKUP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0100) {
                    cur_active = SWC_TEL_HANGUP_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
            }
        }
    }
}

void daily_2016_dabradio_init() {
    LOG_DEF_NORMAL("Daily DAB RADIO\r\n");
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 500;
    can_conf.can0.listen_mode = false;
    canbus_init(&can_conf);
    canbus0_enable_tx();

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06284000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x0A18A000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x18FFC321);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x0CF00203);

    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);
}
