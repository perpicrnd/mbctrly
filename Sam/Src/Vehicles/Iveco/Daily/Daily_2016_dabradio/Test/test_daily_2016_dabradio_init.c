#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <setjmp.h>
#include <cmocka.h>

#include "daily_2016_dabradio.c"

void vApplicationIdleHook(void){
//Make the linker happy.
}

void software_reset(void){

}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler){
	function_called();
	check_expected(event);
	check_expected(handler);
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num){
	check_expected(interface);
	check_expected(num);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf){
	(void)can_conf;
	function_called();

}

static void test_daily_2016_init(){
	expect_value(__wrap_canbus_add_accepted_id, num, 0x06284000);
	expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

	expect_value(__wrap_canbus_add_accepted_id, num, 0x0A18A000);
	expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

	expect_value(__wrap_canbus_add_accepted_id, num, 0x18FFC321);
	expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

	expect_value(__wrap_canbus_add_accepted_id, num, 0x0CF00203);
	expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

	expect_function_call(__wrap_canbus_init);

	expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
	expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);
	expect_function_call(__wrap_event_connect_callback);

	expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
	expect_value(__wrap_event_connect_callback, handler, handle_revmov);
	expect_function_call(__wrap_event_connect_callback);

	expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
	expect_value(__wrap_event_connect_callback, handler, handle_lights);
	expect_function_call(__wrap_event_connect_callback);

	expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
	expect_value(__wrap_event_connect_callback, handler, handle_speed);
	expect_function_call(__wrap_event_connect_callback);

	expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
	expect_value(__wrap_event_connect_callback, handler, handle_swc);
	expect_function_call(__wrap_event_connect_callback);



	daily_2016_dabradio_init();
}



int setup(void **state){
	(void)state;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
			cmocka_unit_test_setup(test_daily_2016_init, setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
