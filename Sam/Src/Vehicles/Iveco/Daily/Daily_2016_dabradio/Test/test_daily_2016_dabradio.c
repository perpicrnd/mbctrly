#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "daily_2016_dabradio.c"
#include "event_list.h"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

void event_out_service_emit(void) {
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

void __wrap_event_can0_tx_message_emit(const CAN_MESSAGE *const msg) {
    (void)msg;
    function_called();
}

void test_event_iveco_daily_2016_ignition_emit(void **state) {
    (void)state;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_2016_ignition_emit();

    wired_ignition_0 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_iveco_daily_2016_ignition_emit();

    wired_ignition_0 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_2016_ignition_emit();

    wired_ignition_1 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_iveco_daily_2016_ignition_emit();

    wired_ignition_1 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_2016_ignition_emit();
}

void test_handle_wired_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);
}

static void test_handle_reverse() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x18FFC321, true, {0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    event_task_loop(&event);

    CAN_MESSAGE msg1 = {0x18FFC321, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    event_task_loop(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x18FFC321, true, {0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    event_task_loop(&event);
}

static void test_handle_lights() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x18FFC321, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_ON);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    event_task_loop(&event);

    CAN_MESSAGE msg1 = {0x18FFC321, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_OFF);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    event_task_loop(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x18FFC321, true, {0x88, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_lights_emit, status, LIGHTS_ON);
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    event_task_loop(&event);
}

static void test_handle_speedhandbrake() {
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0xCF00203, true, {0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_in_range(__wrap_event_speed_emit, status, 0x0300, 0x0700);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    event_task_loop(&event);

    event.as_can_message.msg.can_data.byte2 = 0x00;
    expect_value(__wrap_event_speed_emit, status, 0);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    event_task_loop(&event);
}

void test_swc() {
    union EVENT event;

    //Invio volume+
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x06284000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    event_task_loop(&event);

    //Rilascio volume+
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Volume -
    event.as_can_message.msg.can_data.byte0 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Mute
    event.as_can_message.msg.can_data.byte0 = 0x20;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Seek+
    event.as_can_message.msg.can_data.byte0 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Seek-
    event.as_can_message.msg.can_data.byte0 = 0x08;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Source
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Voice
    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Tel Hangup
    event.as_can_message.msg.can_data.byte0 = 0x01;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    //Tel Dial
    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);

    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    event_task_loop(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    event_task_loop(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    event_task_loop(&event);
}

int setup(void **state) {
    (void)state;
    daily_2016_dabradio_init();
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_handle_reverse, setup),
        cmocka_unit_test_setup(test_handle_lights, setup),
        cmocka_unit_test_setup(test_handle_speedhandbrake, setup),
        cmocka_unit_test_setup(test_swc, setup),
        cmocka_unit_test_setup(test_handle_wired_ignition, setup)};

    return cmocka_run_group_tests(tests, NULL, NULL);
}
