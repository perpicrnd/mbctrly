
cmake_minimum_required(VERSION 3.0)

add_subdirectory(Daily_2014)
add_subdirectory(Daily_2016_dabradio)
add_subdirectory(Daily_2019)
add_subdirectory(Daily_2020_NIS_NO_NAV)

SET(LIBRARY_NAME iveco_daily)

if (CMAKE_CROSSCOMPILING)
    

else()
	add_subdirectory(Test)
endif()	


set(SOURCES
    iveco_daily.c
)

add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries (${LIBRARY_NAME} eventlist wisker settings radio daily_2014 daily_2016_dabradio daily_2019 daily_2020_nis_nonav)

target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)

add_library(${LIBRARY_NAME}_with_lin ${SOURCES})
target_link_libraries (${LIBRARY_NAME}_with_lin eventlist wisker settings radio daily_2014 daily_2016_dabradio daily_2019 daily_2020_nis_nonav)
target_compile_definitions(${LIBRARY_NAME}_with_lin PUBLIC ADD_DAILY_WITH_LIN)
  
target_include_directories(${LIBRARY_NAME}_with_lin  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)

add_library(${LIBRARY_NAME}_with_lin_no_btn ${SOURCES})
target_link_libraries (${LIBRARY_NAME}_with_lin_no_btn eventlist wisker settings radio daily_2014 daily_2016_dabradio daily_2019 daily_2020_nis_nonav)
target_compile_definitions(${LIBRARY_NAME}_with_lin_no_btn PUBLIC ADD_DAILY_WITH_LIN)
target_compile_definitions(${LIBRARY_NAME}_with_lin_no_btn PUBLIC DISABLE_BUTTONS)
  
target_include_directories(${LIBRARY_NAME}_with_lin_no_btn  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)




