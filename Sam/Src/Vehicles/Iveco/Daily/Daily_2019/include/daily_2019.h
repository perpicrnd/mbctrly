#pragma once
#include <stdbool.h>

typedef enum daily_can_speed {
    dcs_50kbps,
    dcs_500kbps
} daily_can_speed_t;

void daily_2019_init(daily_can_speed_t can_speed);
