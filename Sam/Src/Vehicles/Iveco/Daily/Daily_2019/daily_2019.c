#include <stdio.h>

#include "FreeRTOS.h"
#include "canbus.h"
#include "daily_2019.h"
#include "difference.h"
#include "event_list.h"
#include "logger.h"
#include "serials.h"
#include "timers.h"
#include "wisker.h"

static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;

static void event_iveco_daily_2019_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
    }
    event_ignition_emit(ignition);
}

static void handle_wired_ignition(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_iveco_daily_2019_ignition_emit();
    }
}

static void handle_revmov(const union EVENT *const msg) {
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        //CAN 50 kbps
        if (msg->as_can_message.msg.can_id == 0x63140C9) {
            if ((msg->as_can_message.msg.can_data.byte0 & 0x80) == 0x80) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
        //CAN 500 kbps
        if (msg->as_can_message.msg.can_id == 0xA18A001) {
            if ((msg->as_can_message.msg.can_data.byte7 & 0x04) == 0x04) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}

// static void handle_handbrake(const union EVENT *const msg) {
//     if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
//         //CAN 50 kbps
//         if (msg->as_can_message.msg.can_id == 0x6214000) {
//             if ((msg->as_can_message.msg.can_data.byte0 & 0x20) == 0x20) {
//                 event_handbrake_emit(HANDBRAKE_ON);
//             } else {
//                 event_handbrake_emit(HANDBRAKE_OFF);
//             }
//         }
//         //CAN 500 kbps
//         if (msg->as_can_message.msg.can_id == 0xA18A000) {
//             if ((msg->as_can_message.msg.can_data.byte6 & 0x20) == 0x20) {
//                 event_handbrake_emit(HANDBRAKE_ON);
//             } else {
//                 event_handbrake_emit(HANDBRAKE_OFF);
//             }
//         }
//     }
// }

static void handle_lights(const union EVENT *const msg) {
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        //CAN 50 kbps
        if (msg->as_can_message.msg.can_id == 0x2214000) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x68) == 0x68) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
        }
        //CAN 500 kbps
        if (msg->as_can_message.msg.can_id == 0xA18A000) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x20) == 0x20) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
        }
    }
}

static uint32_t old_speed;
static void handle_speed(const union EVENT *const msg) {
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            //CAN 50 kbps
            if (msg->as_can_message.msg.can_id == 0x4394000) {
                tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if (DIFF(old_speed, tmp) != 0) {
                    event_speed_emit((tmp * 25) / 4);
                    old_speed = tmp;
                }
            }
            //CAN 500 kbps
            if (msg->as_can_message.msg.can_id == 0xA28A000) {
                tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if (DIFF(old_speed, tmp) != 0) {
                    event_speed_emit((tmp * 25) / 4);
                    old_speed = tmp;
                }
            }
        }
    }
}

/**
 * @brief Flag che definisce se un comando a volante è attualmente attivo.
 * 
 */
static bool swc_active = false;
/**
 * @brief comando al volante can attualmente attivo
 * 
 */
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
/**
 * @brief comando can attualmente propagato.
 * 
 */
static enum SWC can_button = SWC_NO_BUTTON_PRESSED;

static enum PHONE_STATUS phone_status = PHONE_STATUS_INACTIVE;
static enum RADIO_SOURCE_TYPE radio_source = RADIO_SOURCE_OFF;

/**
 * @brief Parsing dei comandi a volante
 * 
 * @param EVENT l'evento ricevuto.
 */
static void handle_swc(const union EVENT *const msg) {
    bool emit_enabled = false;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x18A7FF21;
            if (msg->as_can_message.msg.can_id == id) {
                uint32_t tmp = ((msg->as_can_message.msg.can_data.byte1 << 16) + (msg->as_can_message.msg.can_data.byte2 << 8) + msg->as_can_message.msg.can_data.byte3) & (0xFFFFFF);
                if (tmp == 0x000100) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000400) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x001000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000001) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000004) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000010) {
                    cur_active = SWC_SOURCE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x040000) {
                    LOG_DEF_NORMAL("**phone\r\n");
                    if (radio_source == RADIO_SOURCE_PHONE) {
                        if (phone_status == PHONE_STATUS_RINGING_INCOMING) {
                            cur_active = SWC_TEL_PICKUP_PRESSED;
                        } else {
                            cur_active = SWC_TEL_HANGUP_PRESSED;
                        }
                        swc_active = true;
                    }
                } else if (tmp == 0x100000) {
                    cur_active = SWC_SPEECH_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                emit_enabled = true;
                can_button = cur_active;
            }
        }
    } else if (msg->as_generic.event == eEVENT_LIN_RX_MESSAGE) {
        uint8_t break_B = (uint8_t)msg->as_lin_message.msg.lin_break;
        uint8_t sync_B = (uint8_t)msg->as_lin_message.msg.lin_sync;
        uint8_t id_B = (uint8_t)msg->as_lin_message.msg.lin_id;
        //TODO: verificare checksum
        if ((break_B == 0x00) && (sync_B == 0x55) && (id_B == 0x0C)) {
            uint32_t tmp = 0;
            uint8_t data = (uint8_t)msg->as_lin_message.msg.lin_data.byte0;
            tmp |= data << 24;
            data = (uint8_t)msg->as_lin_message.msg.lin_data.byte1;
            tmp |= data << 16;
            data = (uint8_t)msg->as_lin_message.msg.lin_data.byte2;
            tmp |= data << 8;
            data = (uint8_t)msg->as_lin_message.msg.lin_data.byte3;
            tmp |= data;
            //LOG_DEF_NORMAL("***LIN_MSG:%d\r\n", (int)tmp);
            if (tmp == 0x01000200) {
                cur_active = SWC_VOL_UP_PRESSED;
                swc_active = true;
            } else if (tmp == 0x02000200) {
                cur_active = SWC_VOL_DOWN_PRESSED;
                swc_active = true;
            } else if (tmp == 0x04000200) {
                cur_active = SWC_MUTE_PRESSED;
                swc_active = true;
            } else if (tmp == 0x08000200) {
                cur_active = SWC_SEEK_UP_PRESSED;
                swc_active = true;
            } else if (tmp == 0x10000200) {
                cur_active = SWC_SEEK_DOWN_PRESSED;
                swc_active = true;
            } else if (tmp == 0x00010200) {
                cur_active = SWC_SOURCE_PRESSED;
                swc_active = true;
            } else if (tmp == 0x40000200) {
                LOG_DEF_NORMAL("**phone\r\n");
                if (radio_source == RADIO_SOURCE_PHONE) {
                    if (phone_status == PHONE_STATUS_RINGING_INCOMING) {
                        cur_active = SWC_TEL_PICKUP_PRESSED;
                    } else {
                        cur_active = SWC_TEL_HANGUP_PRESSED;
                    }
                    swc_active = true;
                }
            } else if (tmp == 0x00020200) {
                cur_active = SWC_SPEECH_PRESSED;
                swc_active = true;
            } else {
                if (swc_active) {
                    cur_active++;
                    swc_active = false;
                } else {
                    cur_active = SWC_NO_BUTTON_PRESSED;
                }
            }
            emit_enabled = true;
            can_button = cur_active;
        }
    }
    if (emit_enabled) {
        if (can_button != SWC_NO_BUTTON_PRESSED) {
            event_swc_emit(can_button, SWC_MODULE_VEHICLE);
        } else {
            event_swc_emit(SWC_NO_BUTTON_PRESSED, SWC_MODULE_VEHICLE);
        }
    }
}

static void handle_source_type(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source = msg->as_radio_source.source_type;
        LOG_DEF_NORMAL("**source type: %02X\r\n", radio_source);
    }
}

static void handle_phone_info(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_PHONE_INFO) {
        phone_status = msg->as_phone_info.bits.as_struct.phone_status;
        LOG_DEF_NORMAL("**phone status: %02X\r\n", phone_status);
    }
}

/**
 * @brief Inizializzazione del veicolo.
 */
void daily_2019_init(daily_can_speed_t can_speed) {
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.listen_mode = false;
    if (can_speed == dcs_50kbps) {
        LOG_DEF_NORMAL("Daily 2019 LOW SPEED\r\n");
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x2214000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x4394000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x6214000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x6314000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x63140C9);
        can_conf.can0.can_speed = 50;
    } else if (can_speed == dcs_500kbps) {
        LOG_DEF_NORMAL("Daily 2019 HIGH SPEED\r\n");
        canbus_add_accepted_id(CAN_INTERFACE_0, 0xA18A000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0xA18A001);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0xA28A000);
        canbus_add_accepted_id(CAN_INTERFACE_0, 0x18A7FF21);
        can_conf.can0.can_speed = 500;
    }
    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_PHONE_INFO, handle_phone_info);
    serial_usart0_init_LIN(19200);
    canbus_init(&can_conf);

    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_wired_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    // event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_handbrake);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);

    event_connect_callback(eEVENT_LIN_RX_MESSAGE, handle_swc);
    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);

    wisker_reset(MODULE_VEHICLE);
    wisker_disable_module(MODULE_UART_LOWLEVEL);
    canbus0_enable_tx();
}