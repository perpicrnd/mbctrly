
cmake_minimum_required(VERSION 3.0)

if (CMAKE_CROSSCOMPILING)
	
else()
	add_subdirectory(Test)
endif()	


SET(LIBRARY_NAME daily_2019)


set(SOURCES
	daily_2019.c
)

add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries (${LIBRARY_NAME} eventlist wisker)

        
target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)

