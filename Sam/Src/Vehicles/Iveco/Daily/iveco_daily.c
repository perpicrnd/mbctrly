#include "iveco_daily.h"
#include "alpine_menu.h"
#include "alpine_task.h"
#include "board_version.h"
#include "daily_2014.h"
#include "daily_2016_dabradio.h"
#include "daily_2019.h"
#include "daily_2020_nis_no_nav.h"
#include "event_list.h"
#include "ignition.h"
#include "logger.h"
#include "main.h"
#include "menu_manager.h"
#include "menu_walking.h"
#include "radio_message.h"
#include "settings.h"
#include "vehicle.h"

enum IVECO_DAILY_MENU {
    IVECO_DAILY_MENU_MAIN = 0x80,       //!< IVECO_DAILY_MENU_MAIN
    IVECO_DAILY_MENU_SELECTION = 0x81,  //!< IVECO_DAILY_MENU_SELECTION
    IVECO_DAILY_MENU_REBOOT,
};

enum IVECO_DAILY_MAIN_MENU {
    IVECO_DAILY_MAIN_MENU_SELECTION = 1,
    IVECO_DAILY_MAIN_MENU_REBOOT,
};

enum IVECO_DAILY_SEL_MENU {
    IVECO_DAILY_SEL_MENU_BT_RADIO = 1,
    IVECO_DAILY_SEL_MENU_DAB_RADIO,
    IVECO_DAILY_SEL_MENU_RADIO_PREPARATION,
    IVECO_DAILY_SEL_MENU_2019_DAB_RADIO,
    IVECO_DAILY_SEL_MENU_2019_RADIO_PREPARATION,
    IVECO_DAILY_SEL_MENU_2020_RADIO_NIS_NONAV,
};

static enum IVECO_DAILY_VERSION current_config = 0;
static enum IVECO_DAILY_VERSION vers_selected;
static bool configured = false;
static void handle_config_setup(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_SETTINGS_READ) {
        if (msg->as_settings_read.type == SETTING_READ_VEHICLE_TYPE) {
            current_config = settings_read(VEHICLE_TYPE);
            vers_selected = current_config;
            if (!configured) {
                configured = true;
                switch (current_config) {
                    case IVECO_DAILY_BT_RADIO:
                        daily_2014_init(true);
                        alpine_menu_disable_lin_management();
                        alpine_menu_disable_clock_management();
                        break;
                    case IVECO_DAILY_DAB_RADIO:
                        daily_2016_dabradio_init();
                        alpine_menu_disable_lin_management();
                        alpine_menu_disable_clock_management();
                        break;
                    case IVECO_DAILY_RADIO_PREPARATION:
                        daily_2014_init(false);
                        alpine_menu_disable_lin_management();
                        alpine_menu_disable_clock_management();
                        break;
                    case IVECO_DAILY_2019_RADIO_PREPARATION:
                        daily_2019_init(dcs_50kbps);
                        alpine_menu_disable_clock_management();
                        break;
                    case IVECO_DAILY_2019_DAB_RADIO:
                        daily_2019_init(dcs_500kbps);
                        alpine_menu_disable_clock_management();
                        break;
                    case IVECO_DAILY_2020_NIS_NONAV_RADIO:
                        daily_2020_nis_no_nav_init();
                        break;
                    default:
                        LOG_DEF_NORMAL("Unknown vehicle config %d\r\n", current_config);
                        settings_write(VEHICLE_TYPE, IVECO_DAILY_BT_RADIO);
                        vers_selected = IVECO_DAILY_BT_RADIO;
                        current_config = IVECO_DAILY_BT_RADIO;
                        daily_2014_init(true);
                        alpine_menu_disable_lin_management();
                        alpine_menu_disable_clock_management();
                        break;
                }
            }
        }
    }
}

/**
 * @brief Traduzione della stringa Vehicle settings nelle varie lingue .
 */
static const char *vehicle_settings_str[LANGUAGE_DEFINITION_COUNT] = {
    "Fahrzeugeinstellungen",       //!< LANGUAGE_DEFINITION_GERMAN
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH
    "Vehicle settings",            //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Réglages du véhicule",        //!< LANGUAGE_DEFINITION_FRENCH
    "Impostazioni veicolo",        //!< LANGUAGE_DEFINITION_ITALIAN
    "Configuración del vehículo",  //!< LANGUAGE_DEFINITION_SPANISH
    "Configuração do veiculo",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Ustawienia Pojazdu",          //!< LANGUAGE_DEFINITION_POLISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CZECH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SWEDISH
    "Voertuig Instellingen",       //!< LANGUAGE_DEFINITION_DUTCH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_JAPANESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_RUSSIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_KOREAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_TURKISH
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_CHINESE
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Vehicle Settings",            //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *oem_radio_str[LANGUAGE_DEFINITION_COUNT] = {
    "Werksradio",          //!< LANGUAGE_DEFINITION_GERMAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_ENGLISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Radio d'origine",     //!< LANGUAGE_DEFINITION_FRENCH
    "Radio Originale",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio origen OEM",    //!< LANGUAGE_DEFINITION_SPANISH
    "Rádio original OEM",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_POLISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_CZECH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_SWEDISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_DUTCH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_JAPANESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_RUSSIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_KOREAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_TURKISH
    "OEM Radio",           //!< LANGUAGE_DEFINITION_CHINESE
    "OEM Radio",           //!< LANGUAGE_DEFINITION_NORWEGIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "OEM Radio",           //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *bt_radio[] = {
    "BT Radio",  //!< LANGUAGE_DEFINITION_GERMAN
    "BT Radio",  //!< LANGUAGE_DEFINITION_ENGLISH
    "BT Radio",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "BT Radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "BT Radio",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio BT",  //!< LANGUAGE_DEFINITION_SPANISH
    "BT Radio",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "BT Radio",  //!< LANGUAGE_DEFINITION_POLISH
    "BT Radio",  //!< LANGUAGE_DEFINITION_CZECH
    "BT Radio",  //!< LANGUAGE_DEFINITION_SWEDISH
    "BT Radio",  //!< LANGUAGE_DEFINITION_DUTCH
    "BT Radio",  //!< LANGUAGE_DEFINITION_JAPANESE
    "BT Radio",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "BT Radio",  //!< LANGUAGE_DEFINITION_KOREAN
    "BT Radio",  //!< LANGUAGE_DEFINITION_TURKISH
    "BT Radio",  //!< LANGUAGE_DEFINITION_CHINESE
    "BT Radio",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "BT Radio",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "BT Radio",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dab_radio[] = {
    "DAB Radio",  //!< LANGUAGE_DEFINITION_GERMAN
    "DAB Radio",  //!< LANGUAGE_DEFINITION_ENGLISH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "DAB Radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "Radio DAB",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio DAB",  //!< LANGUAGE_DEFINITION_SPANISH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "DAB Radio",  //!< LANGUAGE_DEFINITION_POLISH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_CZECH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_SWEDISH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_DUTCH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_JAPANESE
    "DAB Radio",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "DAB Radio",  //!< LANGUAGE_DEFINITION_KOREAN
    "DAB Radio",  //!< LANGUAGE_DEFINITION_TURKISH
    "DAB Radio",  //!< LANGUAGE_DEFINITION_CHINESE
    "DAB Radio",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "DAB Radio",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "DAB Radio",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *open_dash[] = {
    "Ohne Radio",         //!< LANGUAGE_DEFINITION_GERMAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_ENGLISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Préparation radio",  //!< LANGUAGE_DEFINITION_FRENCH
    "Open Dash",          //!< LANGUAGE_DEFINITION_ITALIAN
    "Sin unidad",         //!< LANGUAGE_DEFINITION_SPANISH
    "Sem Radio",          //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_POLISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_CZECH
    "Open Dash",          //!< LANGUAGE_DEFINITION_SWEDISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_DUTCH
    "Open Dash",          //!< LANGUAGE_DEFINITION_JAPANESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_RUSSIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_KOREAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_TURKISH
    "Open Dash",          //!< LANGUAGE_DEFINITION_CHINESE
    "Open Dash",          //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Open Dash",          //!< LANGUAGE_DEFINITION_HUNGARIAN
};
// #ifdef ADD_DAILY_WITH_LIN
static const char *open_dash_2019[] = {
    "Ohne Radio 2019",         //!< LANGUAGE_DEFINITION_GERMAN
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_ENGLISH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Préparation radio 2019",  //!< LANGUAGE_DEFINITION_FRENCH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_ITALIAN
    "Sin unidad 2019",         //!< LANGUAGE_DEFINITION_SPANISH
    "Sem Radio 2019",          //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_POLISH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_CZECH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_SWEDISH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_DUTCH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_JAPANESE
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_RUSSIAN
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_KOREAN
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_TURKISH
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_CHINESE
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Open Dash 2019",          //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *dab_radio_2019[] = {
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_GERMAN
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_ENGLISH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_FRENCH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_ITALIAN
    "Radio DAB 2019",  //!< LANGUAGE_DEFINITION_SPANISH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_POLISH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_CZECH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_SWEDISH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_DUTCH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_JAPANESE
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_KOREAN
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_TURKISH
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_CHINESE
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "DAB Radio 2019",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *nis_radio[] = {
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_GERMAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_ENGLISH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_ENGLISH_US
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_FRENCH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_ITALIAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_SPANISH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_PORTUGUESE
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_POLISH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_CZECH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_SWEDISH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_DUTCH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_JAPANESE
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_RUSSIAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_KOREAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_TURKISH
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_CHINESE
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_NORWEGIAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "HI CONNECT 2019",  //!< LANGUAGE_DEFINITION_HUNGARIAN
};
// #endif
static const char *confirm_str[] = {
    "Bestätigen",    //!< LANGUAGE_DEFINITION_GERMAN
    "Confirm",       //!< LANGUAGE_DEFINITION_ENGLISH
    "Confirm",       //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Confirmation",  //!< LANGUAGE_DEFINITION_FRENCH
    "Conferma",      //!< LANGUAGE_DEFINITION_ITALIAN
    "Confirmar",     //!< LANGUAGE_DEFINITION_SPANISH
    "Confirmar",     //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Confirm",       //!< LANGUAGE_DEFINITION_POLISH
    "Confirm",       //!< LANGUAGE_DEFINITION_CZECH
    "Confirm",       //!< LANGUAGE_DEFINITION_SWEDISH
    "Confirm",       //!< LANGUAGE_DEFINITION_DUTCH
    "Confirm",       //!< LANGUAGE_DEFINITION_JAPANESE
    "Confirm",       //!< LANGUAGE_DEFINITION_RUSSIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_KOREAN
    "Confirm",       //!< LANGUAGE_DEFINITION_TURKISH
    "Confirm",       //!< LANGUAGE_DEFINITION_CHINESE
    "Confirm",       //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Confirm",       //!< LANGUAGE_DEFINITION_HUNGARIAN
};

static const char *reboot_str[] = {
    "Neustart",    //!< LANGUAGE_DEFINITION_GERMAN
    "Reboot",      //!< LANGUAGE_DEFINITION_ENGLISH
    "Reboot",      //!< LANGUAGE_DEFINITION_ENGLISH_US
    "Redémarrer",  //!< LANGUAGE_DEFINITION_FRENCH
    "Riavvia",     //!< LANGUAGE_DEFINITION_ITALIAN
    "Reiniciar",   //!< LANGUAGE_DEFINITION_SPANISH
    "Reiniciar",   //!< LANGUAGE_DEFINITION_PORTUGUESE
    "Reboot",      //!< LANGUAGE_DEFINITION_POLISH
    "Reboot",      //!< LANGUAGE_DEFINITION_CZECH
    "Reboot",      //!< LANGUAGE_DEFINITION_SWEDISH
    "Reboot",      //!< LANGUAGE_DEFINITION_DUTCH
    "Reboot",      //!< LANGUAGE_DEFINITION_JAPANESE
    "Reboot",      //!< LANGUAGE_DEFINITION_RUSSIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_KOREAN
    "Reboot",      //!< LANGUAGE_DEFINITION_TURKISH
    "Reboot",      //!< LANGUAGE_DEFINITION_CHINESE
    "Reboot",      //!< LANGUAGE_DEFINITION_NORWEGIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_SLOVAKIAN
    "Reboot",      //!< LANGUAGE_DEFINITION_HUNGARIAN
};

menu_manager_t iveco_daily_main_menu;
menu_manager_t iveco_daily_selection_menu;

void iveco_daily_menu_disable_2019(void) {
    menu_manager_clear_active_bit(iveco_daily_selection_menu, IVECO_DAILY_SEL_MENU_2019_DAB_RADIO);
    menu_manager_clear_active_bit(iveco_daily_selection_menu, IVECO_DAILY_SEL_MENU_2019_RADIO_PREPARATION);
}

enum LED_COLOR vehicle_get_default_led_color(void) {
    return LED_COLOR_ORANGE_DIM;
}

static const char empty[] = "\0";

static inline const char *oem_radio_string(enum IVECO_DAILY_VERSION vers_req) {
    const char *col = empty;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    switch (vers_req) {
        case IVECO_DAILY_BT_RADIO:
            col = bt_radio[language];
            break;
        case IVECO_DAILY_DAB_RADIO:
            col = dab_radio[language];
            break;
        case IVECO_DAILY_RADIO_PREPARATION:
            col = open_dash[language];
            break;
        case IVECO_DAILY_2019_RADIO_PREPARATION:
            col = open_dash_2019[language];
            break;
        case IVECO_DAILY_2019_DAB_RADIO:
            col = dab_radio_2019[language];
            break;
        case IVECO_DAILY_2020_NIS_NONAV_RADIO:
            col = nis_radio[language];
            break;
        default:
            LOG_DEF_NORMAL("No radio selected. Default.\r\n");
            settings_write(VEHICLE_TYPE, IVECO_DAILY_BT_RADIO);
            col = bt_radio[language];
            break;
    }
    return col;
}

static bool iveco_daily_main_menu_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(iveco_daily_main_menu), IVECO_DAILY_MENU_MAIN, vehicle_settings_str[language]);
}

static bool iveco_daily_main_menu_vehicle_selection(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    LOG_DEF_NORMAL("VS: %d\r\n", vers_selected);
    const char *oem_radio = oem_radio_string(vers_selected);
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_MENU_SELECTION, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, oem_radio_str[language], oem_radio);
}

static bool iveco_daily_main_menu_vehicle_reboot(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    const char *str = empty;
    if (vers_selected != current_config) {
        str = reboot_str[language];
    }
    return message_30_transmit_row_two_column(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_MENU_REBOOT, MESSAGE_30_ROW_TYPE_SUB_MENU, 0, confirm_str[language], str);
}

static inline void handle_oem_radio_selection_save(void) {
    if (current_config != vers_selected) {
        settings_write(VEHICLE_TYPE, vers_selected);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        factory_reset_activate();
    }
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_main_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    if (!(row_pressed == 00 && menu_index == 00 && menu_data == 01)) {
        switch (menu_data) {
            case IVECO_DAILY_MENU_SELECTION:
                enable_radio_menu_refresh(iveco_daily_selection_menu);
                menu_walking_push(IVECO_DAILY_MENU_SELECTION);
                break;
            case IVECO_DAILY_MENU_REBOOT:
                handle_oem_radio_selection_save();
                menu_walking_push(IVECO_DAILY_MENU_SELECTION);
                break;
            default:
                enable_radio_menu_refresh(iveco_daily_main_menu);
                menu_walking_push(IVECO_DAILY_MENU_MAIN);
                break;
        }
    } else {
        menu_walking_push(IVECO_DAILY_MENU_MAIN);
        enable_radio_menu_refresh(iveco_daily_main_menu);
    }
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_selection_menu(const uint8_t row_pressed, const uint8_t menu_index, const uint8_t menu_data) {
    (void)row_pressed;
    (void)menu_index;
    LOG_DEF_NORMAL("MD: %d\r\n", menu_data);
    vers_selected = menu_data;
    LOG_DEF_NORMAL("VS: %d\r\n", vers_selected);
    menu_walking_pop();
    enable_radio_menu_refresh(iveco_daily_main_menu);
}

/**
 * @brief gestisce una ritrasmissione da parte della radio.
 * @param row_pressed Il numero del menu visualizzato sulla radio.
 * @param menu_index non usato
 * @param menu_data il valore della riga premuta.
 */
static inline void handle_vehicle_general_settings(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_MENU_REQUEST) {
        ///Ricevuta una richiesta di trasmissione di menu
        if (msg->as_menu_request.msg_type == MENU_REQUEST_NUM_E5) {
            const uint8_t row_pressed =
                msg->as_menu_request.request.as_e5.row_pressed_num;
            const uint8_t menu_index =
                msg->as_menu_request.request.as_e5.menu_index;
            const uint8_t menu_data =
                msg->as_menu_request.request.as_e5.menu_data;
            uint8_t current_menu = menu_walking_current();
            if ((current_menu == 0x00) && (menu_data == IVECO_DAILY_MENU_MAIN)) {
                LOG_DEF_NORMAL("Daily main menu\r\n");
                handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
            } else if (current_menu >= IVECO_DAILY_MENU_MAIN) {
                switch (current_menu) {
                    case IVECO_DAILY_MENU_MAIN:
                        handle_vehicle_main_menu(row_pressed, menu_index, menu_data);
                        break;
                    case IVECO_DAILY_MENU_SELECTION:
                        handle_vehicle_selection_menu(row_pressed, menu_index, menu_data);
                        break;
                    default:
                        LOG_DEF_ERROR("Vehicle: Unknown menu %02X %02X %02X\r\n", row_pressed, menu_index, menu_data);
                        break;
                }
            }
        }
    }
}

static bool iveco_daily_selection_menu_send_header(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    (void)row_num;
    enum LANGUAGE_DEFINITION language = get_defined_language();
    return message_30_transmit_header(&msg->as_30, seq_num_send_next(), menu_manager_count_max(iveco_daily_selection_menu), IVECO_DAILY_MENU_SELECTION, oem_radio_str[language]);
}

static bool iveco_daily_selection_menu_send_bt_radio(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_BT_RADIO) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_BT_RADIO, 0, bt_radio[language], current_selection);
}

static bool iveco_daily_selection_menu_send_dab_radio(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_DAB_RADIO) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_DAB_RADIO, 0, dab_radio[language], current_selection);
}

static bool iveco_daily_selection_menu_send_radio_preparation(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_RADIO_PREPARATION) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_RADIO_PREPARATION, 0, open_dash[language], current_selection);
}

static bool iveco_daily_selection_menu_send_2019_dab_radio(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_2019_DAB_RADIO) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_2019_DAB_RADIO, 0, dab_radio_2019[language], current_selection);
}

static bool iveco_daily_selection_menu_send_2019_radio_preparation(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_2019_RADIO_PREPARATION) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_2019_RADIO_PREPARATION, 0, open_dash_2019[language], current_selection);
}

static bool iveco_daily_selection_menu_send_2020_nis_radio(union ALPINE_MESSAGE *msg, uint8_t row_num) {
    enum LANGUAGE_DEFINITION language = get_defined_language();
    uint8_t current_selection = 0;
    if (vers_selected == IVECO_DAILY_2020_NIS_NONAV_RADIO) {
        current_selection = 1;
    }
    return message_30_transmit_row_optionbox(&msg->as_30, seq_num_send(), row_num, IVECO_DAILY_2020_NIS_NONAV_RADIO, 0, nis_radio[language], current_selection);
}

void iveco_daily_selection_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, iveco_daily_selection_menu_send_header);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_BT_RADIO, iveco_daily_selection_menu_send_bt_radio);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_DAB_RADIO, iveco_daily_selection_menu_send_dab_radio);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_RADIO_PREPARATION, iveco_daily_selection_menu_send_radio_preparation);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_2019_DAB_RADIO, iveco_daily_selection_menu_send_2019_dab_radio);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_2019_RADIO_PREPARATION, iveco_daily_selection_menu_send_2019_radio_preparation);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_SEL_MENU_2020_RADIO_NIS_NONAV, iveco_daily_selection_menu_send_2020_nis_radio);
}

void iveco_daily_main_menu_fill_data(menu_manager_t mngr) {
    menu_manager_enable_bit_func(mngr, 0, iveco_daily_main_menu_header);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_MAIN_MENU_SELECTION, iveco_daily_main_menu_vehicle_selection);
    menu_manager_enable_bit_func(mngr, IVECO_DAILY_MAIN_MENU_REBOOT, iveco_daily_main_menu_vehicle_reboot);
}

void iveco_daily_init(void) {
    board_set_version(BOARD_VERSION_00);
    alpine_menu_disable_ignition_logic();
    alpine_menu_disable_preflight_check();
#ifdef DISABLE_BUTTONS
    alpine_menu_disable_buttons_lines();
#endif
    event_connect_callback(eEVENT_SETTINGS_READ, handle_config_setup);
    event_connect_callback(eEVENT_MENU_REQUEST, handle_vehicle_general_settings);
    iveco_daily_main_menu = menu_manager_create();
    iveco_daily_main_menu_fill_data(iveco_daily_main_menu);
    iveco_daily_selection_menu = menu_manager_create();
    iveco_daily_selection_menu_fill_data(iveco_daily_selection_menu);
#ifndef ADD_DAILY_WITH_LIN
    iveco_daily_menu_disable_2019();
#endif
}