#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "daily_2014.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void event_out_service_emit(void) {
}

void software_reset(void) {
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_event_swc_emit(enum SWC status) {
    check_expected(status);
}

void __wrap_event_can0_tx_message_emit(const CAN_MESSAGE *const msg) {
    (void)msg;
    function_called();
}

void test_event_iveco_daily_ignition_emit(void **state) {
    (void)state;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_ignition_emit();

    can_ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_iveco_daily_ignition_emit();

    can_ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_ignition_emit();

    wired_ignition_0 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_iveco_daily_ignition_emit();

    wired_ignition_0 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_ignition_emit();

    wired_ignition_1 = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    event_iveco_daily_ignition_emit();

    wired_ignition_1 = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    event_iveco_daily_ignition_emit();
}

void test_handle_wired_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.ignition = IGNITION_OFF;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_wired_ignition(&event);

    event.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    event.as_wired_ignition.module = 0;
    event.as_wired_ignition.ignition = IGNITION_ON;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);

    event.as_wired_ignition.module = 1;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_wired_ignition(&event);
}

void test_handle_can_ignition(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x06214000, true, {0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    event.as_can_message.msg.can_data.byte1 = 0x04;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    handle_can_ignition(&event);

    event.as_can_message.msg.can_data.byte1 = 0x00;
    event.as_can_message.msg.can_data.byte2 = 0x00;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    handle_can_ignition(&event);
}

static void test_handle_reverse(void **state) {
    (void)state;
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);

    CAN_MESSAGE msg1 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg1, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    handle_revmov(&event);

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg2 = {0x04214001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg2, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    handle_revmov(&event);
}

static void test_handle_speedhandbrake(void **state) {
    (void)state;
    union EVENT event;

    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x4394000, true, {0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_in_range(__wrap_event_speed_emit, status, (0x2000 * 6.25) - 5, (0x2000 * 6.25) + 5);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    handle_speed(&event);

    event.as_can_message.msg.can_data.byte0 = 0x00;
    expect_value(__wrap_event_speed_emit, status, 0);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    handle_speed(&event);
}

void test_swc(void **state) {
    (void)state;
    union EVENT event;

    //Invio volume+
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    CAN_MESSAGE msg = {0x06284000, true, {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
    memcpy(&event.as_can_message.msg, &msg, sizeof(CAN_MESSAGE));
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    handle_swc(&event);

    //Rilascio volume+
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Volume -
    event.as_can_message.msg.can_data.byte0 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Mute
    event.as_can_message.msg.can_data.byte0 = 0x20;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek+
    event.as_can_message.msg.can_data.byte0 = 0x10;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Seek-
    event.as_can_message.msg.can_data.byte0 = 0x08;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Source
    event.as_can_message.msg.can_data.byte1 = 0x40;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte1 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Voice
    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Tel Hangup
    event.as_can_message.msg.can_data.byte0 = 0x01;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Attivo il telefono come source sulla radio.
    union EVENT event_source;
    event_source.as_generic.event = eEVENT_RADIO_MEDIA_CHANGED;
    event_source.as_radio_source.source_type = RADIO_SOURCE_PHONE;
    handle_source_type(&event_source);
    //Simulo chiamata in arrivo
    union EVENT event_phone;
    event_phone.as_generic.event = eEVENT_PHONE_INFO;
    event_phone.as_phone_info.bits.as_struct.phone_status = PHONE_STATUS_RINGING_INCOMING;
    handle_phone_info(&event_phone);

    //Tel Dial
    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);

    //Riprovo un comando normale dopo aver resettato le flag di chiamata.
    event_phone.as_phone_info.bits.as_struct.phone_status = PHONE_STATUS_INACTIVE;
    handle_phone_info(&event_phone);
    event_source.as_radio_source.source_type = RADIO_SOURCE_TUNER;
    handle_source_type(&event_source);

    event.as_can_message.msg.can_data.byte0 = 0x02;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    handle_swc(&event);

    //Rilascio
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    handle_swc(&event);

    //Key None
    event.as_can_message.msg.can_data.byte0 = 0;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    handle_swc(&event);
}

int setup(void **state) {
    (void)state;

    can_ignition = IGNITION_OFF;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;

    enable_tx_messages = false;

    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_handle_can_ignition, setup),
        cmocka_unit_test_setup(test_handle_reverse, setup),
        cmocka_unit_test_setup(test_handle_speedhandbrake, setup),
        cmocka_unit_test_setup(test_swc, setup),
        cmocka_unit_test_setup(test_handle_wired_ignition, setup)};

    return cmocka_run_group_tests(tests, NULL, NULL);
}
