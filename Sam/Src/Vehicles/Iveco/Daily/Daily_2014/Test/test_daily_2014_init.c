#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

#include <cmocka.h>
#include "daily_2014.c"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    (void)can_conf;
    function_called();
}
void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    function_called();
    check_expected(event);
    check_expected(handler);
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

static void test_daily_2014_init() {
    expect_value(__wrap_canbus_add_accepted_id, num, 0x06214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x0e094000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04214001);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x02214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04394000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x06284000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_function_call(__wrap_canbus_init);

    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_SECONDS_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_can_tx);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_ignition);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_revmov);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_lights);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_speed);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_swc);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_type);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_PHONE_INFO);
    expect_value(__wrap_event_connect_callback, handler, handle_phone_info);
    expect_function_call(__wrap_event_connect_callback);

    daily_2014_init(true);
}

static void test_daily_2014_notx_init() {
    expect_value(__wrap_canbus_add_accepted_id, num, 0x06214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x0e094000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04214001);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x02214000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x04394000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);

    expect_value(__wrap_canbus_add_accepted_id, num, 0x06284000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_function_call(__wrap_canbus_init);

    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_ignition);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_revmov);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_lights);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_speed);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_swc);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_type);
    expect_function_call(__wrap_event_connect_callback);

    expect_value(__wrap_event_connect_callback, event, eEVENT_PHONE_INFO);
    expect_value(__wrap_event_connect_callback, handler, handle_phone_info);
    expect_function_call(__wrap_event_connect_callback);

    daily_2014_init(false);
}

int setup(void **state) {
    (void)state;
    can_ignition = IGNITION_OFF;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;

    enable_tx_messages = false;
    return 0;
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_daily_2014_init, setup),
        cmocka_unit_test_setup(test_daily_2014_notx_init, setup),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
