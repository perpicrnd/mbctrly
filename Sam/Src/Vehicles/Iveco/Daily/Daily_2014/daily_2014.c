#include <stdio.h>

#include "FreeRTOS.h"
#include "canbus.h"
#include "daily_2014.h"
#include "difference.h"
#include "event_list.h"
#include "logger.h"
#include "timers.h"
#include "wisker.h"

/**
 * @brief Attiva la trasmissione dei messaggi sul canbus.
 */
static bool enable_tx_messages = false;

static enum IGNITION_STATUS can_ignition = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;

static void event_iveco_daily_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON) ||
        (can_ignition == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
    }
    event_ignition_emit(ignition);
}

static void handle_can_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        const CAN_MESSAGE* const message = &msg->as_can_message.msg;
        if (message->interface == CAN_INTERFACE_0) {
            if (message->can_id == 0x06214000) {
                if ((message->can_data.byte2 & 0x40) == 0x40) {
                    enable_tx_messages = true;
                    can_ignition = IGNITION_ON;
                } else {
                    enable_tx_messages = false;
                    can_ignition = IGNITION_OFF;
                }
                event_iveco_daily_ignition_emit();
            }
        }
    }
}

static void handle_wired_ignition(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_iveco_daily_ignition_emit();
    }
}

static void handle_revmov(const union EVENT* const msg) {
    const uint32_t id = 0x04214001;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte7 & 0x04) == 0x04) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}

static void handle_lights(const union EVENT* const msg) {
    const uint32_t id = 0x2214000;

    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == id) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x20) == 0x20) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
        }
    }
}

static uint32_t old_speed;
static void handle_speed(const union EVENT* const msg) {
    const uint32_t id = 0x4394000;
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == id) {
                tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if (DIFF(old_speed, tmp) != 0) {
                    event_speed_emit((tmp * 25) / 4);
                    old_speed = tmp;
                }
            }
        }
    }
}

static bool swc_active = false;
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
static enum RADIO_SOURCE_TYPE radio_source_type = RADIO_SOURCE_OFF;
static enum PHONE_STATUS phone_status = PHONE_STATUS_INACTIVE;
static void handle_swc(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x06284000;
            if (msg->as_can_message.msg.can_id == id) {
                uint16_t tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp == 0x8000) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x4000) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x2000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x1000) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0800) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0040) {
                    cur_active = SWC_SOURCE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x0200) {
                    if (radio_source_type == RADIO_SOURCE_PHONE) {
                        cur_active = SWC_TEL_PICKUP_PRESSED;
                        swc_active = true;
                    } else {
                        cur_active = SWC_SPEECH_PRESSED;
                        swc_active = true;
                    }
                } else if (tmp == 0x0100) {
                    cur_active = SWC_TEL_HANGUP_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                event_swc_emit(cur_active, SWC_MODULE_VEHICLE);
            }
        }
    }
}

static void handle_source_type(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source_type = msg->as_radio_source.source_type;
    }
}

static void handle_phone_info(const union EVENT* const msg) {
    if (msg->as_generic.event == eEVENT_PHONE_INFO) {
        phone_status = msg->as_phone_info.bits.as_struct.phone_status;
    }
}

/**
 * @brief Questo array contiene i messaggi con le risposte da inviare.
 */
static const CAN_MESSAGE messages[] = {
    {0x6314005, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0},   //0x00
    {0xA094005, true, {0x00, 0x6D, 0x4A, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0},   //0x01
    {0xA114000, true, {0x00, 0x04, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x00}, 3, CAN_INTERFACE_0},   //0x02
    {0xe094005, true, {0x00, 0x3A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 2, CAN_INTERFACE_0},   //0x03
    {0x18722117, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0},  //0x04
    {0x18ff2417, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0},  //0x05
    {0x1e114005, true, {0x11, 0x11, 0x11, 0x11, 0x11, 0x10, 0x00, 0x00}, 6, CAN_INTERFACE_0},  //0x06
};

static void handle_can_tx(const union EVENT* const msg) {
    if ((msg->as_generic.event == eEVENT_SECONDS_CHANGED) && enable_tx_messages) {
        for (uint8_t i = 0; i < (sizeof(messages) / sizeof(messages[0])); i++) {
            event_can0_tx_message_emit(&messages[i]);
        }
    }
}

void daily_2014_init(bool enable_can_tx) {
    LOG_DEF_NORMAL("Daily 2014 %d\r\n", enable_can_tx);
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.can_speed = 50;
    can_conf.can0.listen_mode = false;
    canbus_init(&can_conf);
    canbus0_enable_tx();

    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06214000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x0e094000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04214001);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x02214000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x04394000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x06284000);

    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);
    if (enable_can_tx) {
        event_connect_callback(eEVENT_SECONDS_CHANGED, handle_can_tx);
    }
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_can_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);

    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_PHONE_INFO, handle_phone_info);
}
