#pragma once

enum IVECO_DAILY_VERSION {
    IVECO_DAILY_BT_RADIO,
    IVECO_DAILY_DAB_RADIO,
    IVECO_DAILY_RADIO_PREPARATION,
    IVECO_DAILY_2019_DAB_RADIO,
    IVECO_DAILY_2019_RADIO_PREPARATION,
    IVECO_DAILY_2020_NIS_NONAV_RADIO,
    IVECO_DAILY_VERSIONS_COUNT
};

void iveco_daily_init(void);