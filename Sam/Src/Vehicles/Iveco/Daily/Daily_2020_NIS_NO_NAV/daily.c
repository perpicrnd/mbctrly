#include <stdio.h>

#include "alpine_menu.h"
#include "canbus.h"
#include "daily_2020_nis_no_nav.h"
#include "difference.h"
#include "event_list.h"
#include "logger.h"
#include "serials.h"
#include "timers.h"
#include "vehicle.h"
#include "wisker.h"

static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;
static enum IGNITION_STATUS can_ignition = IGNITION_OFF;

static void event_iveco_daily_2019_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON) ||
        (can_ignition == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
        vehicle_set_sync_clock(false);
    }
    event_ignition_emit(ignition);
}

static void handle_wired_ignition(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_iveco_daily_2019_ignition_emit();
    }
}

static void handle_can_ignition(const union EVENT *const msg) {
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == 0xA18A000) {
            if ((msg->as_can_message.msg.can_data.byte2 & 0x40) == 0x40) {
                can_ignition = IGNITION_ON;
            } else {
                can_ignition = IGNITION_OFF;
            }
            event_iveco_daily_2019_ignition_emit();
        }
    }
}

static void handle_revmov(const union EVENT *const msg) {
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == 0xA18A001) {
            if ((msg->as_can_message.msg.can_data.byte7 & 0x04) == 0x04) {
                event_reverse_emit(REVERSE_ON);
            } else {
                event_reverse_emit(REVERSE_OFF);
            }
        }
    }
}

static void handle_lights(const union EVENT *const msg) {
    if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
        if (msg->as_can_message.msg.can_id == 0xA18A000) {
            if ((msg->as_can_message.msg.can_data.byte1 & 0x20) == 0x20) {
                //Abbiamo le luci spente
                event_lights_emit(LIGHTS_ON);
            } else {
                //Luci accese
                event_lights_emit(LIGHTS_OFF);
            }
        }
    }
}

static uint32_t old_speed;
static void handle_speed(const union EVENT *const msg) {
    uint32_t tmp = 0;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            if (msg->as_can_message.msg.can_id == 0xA28A000) {
                tmp = ((msg->as_can_message.msg.can_data.byte0 << 8) + msg->as_can_message.msg.can_data.byte1);
                if (tmp >= 0x40) {
                    event_handbrake_emit(HANDBRAKE_OFF, HANDBRAKE_MODULE_SPEED);
                } else {
                    event_handbrake_emit(HANDBRAKE_ON, HANDBRAKE_MODULE_SPEED);
                }
                if (DIFF(old_speed, tmp) != 0) {
                    event_speed_emit((tmp * 25) / 4);
                    old_speed = tmp;
                }
            }
        }
    }
}

/**
 * @brief Flag che definisce se un comando a volante è attualmente attivo.
 * 
 */
static bool swc_active = false;
/**
 * @brief comando al volante can attualmente attivo
 * 
 */
static enum SWC cur_active = SWC_NO_BUTTON_PRESSED;
/**
 * @brief comando can attualmente propagato.
 * 
 */
static enum SWC can_button = SWC_NO_BUTTON_PRESSED;

static enum PHONE_STATUS phone_status = PHONE_STATUS_INACTIVE;
static enum RADIO_SOURCE_TYPE radio_source = RADIO_SOURCE_OFF;

/**
 * @brief Parsing dei comandi a volante
 * 
 * @param EVENT l'evento ricevuto.
 */
static void handle_swc(const union EVENT *const msg) {
    bool emit_enabled = false;
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x18A7FF21;
            if (msg->as_can_message.msg.can_id == id) {
                uint32_t tmp = ((msg->as_can_message.msg.can_data.byte1 << 16) + (msg->as_can_message.msg.can_data.byte2 << 8) + msg->as_can_message.msg.can_data.byte3) & (0xFFFFFF);
                if (tmp == 0x000100) {
                    cur_active = SWC_VOL_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000400) {
                    cur_active = SWC_VOL_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x001000) {
                    cur_active = SWC_MUTE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000001) {
                    cur_active = SWC_SEEK_UP_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000004) {
                    cur_active = SWC_SEEK_DOWN_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x000010) {
                    cur_active = SWC_SOURCE_PRESSED;
                    swc_active = true;
                } else if (tmp == 0x040000) {
                    LOG_DEF_NORMAL("**phone\r\n");
                    if (radio_source == RADIO_SOURCE_PHONE) {
                        if (phone_status == PHONE_STATUS_RINGING_INCOMING) {
                            cur_active = SWC_TEL_PICKUP_PRESSED;
                        } else {
                            cur_active = SWC_TEL_HANGUP_PRESSED;
                        }
                        swc_active = true;
                    }
                } else if (tmp == 0x100000) {
                    cur_active = SWC_SPEECH_PRESSED;
                    swc_active = true;
                } else {
                    if (swc_active) {
                        cur_active++;
                        swc_active = false;
                    } else {
                        cur_active = SWC_NO_BUTTON_PRESSED;
                    }
                }
                emit_enabled = true;
                can_button = cur_active;
            }
        }
    }
    if (emit_enabled) {
        if (can_button != SWC_NO_BUTTON_PRESSED) {
            event_swc_emit(can_button, SWC_MODULE_VEHICLE);
        } else {
            event_swc_emit(SWC_NO_BUTTON_PRESSED, SWC_MODULE_VEHICLE);
        }
    }
}

static void handle_source_type(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_RADIO_MEDIA_CHANGED) {
        radio_source = msg->as_radio_source.source_type;
        LOG_DEF_NORMAL("**NIS source type: %02X\r\n", radio_source);
    }
}

static void handle_phone_info(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_PHONE_INFO) {
        phone_status = msg->as_phone_info.bits.as_struct.phone_status;
        LOG_DEF_NORMAL("**NIS phone status: %02X\r\n", phone_status);
    }
}

static uint16_t vehicle_year = 2020;
static uint8_t vehicle_month = 5;
static uint8_t vehicle_day = 26;
static uint8_t vehicle_hour = 13;
static uint8_t vehicle_minute = 1;
void iveco_send_changed_clock_date(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute) {
    CAN_MESSAGE message;
    vehicle_year = year;
    vehicle_month = month;
    vehicle_day = day;
    vehicle_hour = hour;
    vehicle_minute = minute;
    LOG_DEF_NORMAL("%s year: %d month: %d day: %d hour: %d minute: %d\r\n", __PRETTY_FUNCTION__, year, month, day, hour, minute);
    message.can_id = 0x18FEE641;
    message.datalen = 8;
    message.interface = CAN_INTERFACE_0;
    message.can_is_ext = true;
    message.can_data.byte0 = 0x00;
    message.can_data.byte1 = minute;
    if (hour == 0) {
        message.can_data.byte2 = 0x17;
    } else {
        message.can_data.byte2 = hour - 1;
    }
    message.can_data.byte3 = month;
    message.can_data.byte4 = day << 2;
    message.can_data.byte5 = 0x0E;
    message.can_data.byte6 = 0x7D;
    message.can_data.byte7 = 0x7E;
    event_can0_tx_message_emit(&message);
}

/**
 * @brief Invia le informazioni dell'ora nel momento in cui sono necessarie.
 * L'evento ricevuto dall'event loop contenente le informazioni relative alla data.
 */
static void iveco_handle_minutes_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_TIME_CHANGED) {
        LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
        if (vehicle_get_sync_clock()) {
            LOG_DEF_NORMAL("Orologio attivo.\r\n");
            uint8_t minute = msg->as_time.minute;
            uint8_t hour = msg->as_time.hour;
            uint8_t day = msg->as_time.day;
            uint8_t month = msg->as_time.month;
            uint16_t year = msg->as_time.year;
            iveco_send_changed_clock_date(year, month, day, hour, minute);
        } else {
            LOG_DEF_NORMAL("Not syncing clock. Setting disabled.\r\n");
        }
    }
}

static CAN_MESSAGE time_base = {0};
/**
 * @brief Riceve un evento dall'event loop contenente le informazioni del formato ora
 * @param msg l'evento con le informazioni del parametro dell'ora.
 */
void iveco_timedate_format_changed(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_TIME_FORMAT_CHANGED) {
        CAN_MESSAGE message;
        memcpy(&message, &time_base, sizeof(CAN_MESSAGE));
        message.can_id = 0x18FE0F41;
        if (msg->as_time_format.time_format == 0x00) {
            message.can_data.byte2 &= ~0x10;
        } else {
            message.can_data.byte2 |= 0x10;
        }
        event_can0_tx_message_emit(&message);
    }
}

void handle_time_base(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x18FE0F17;
            if (msg->as_can_message.msg.can_id == id) {
                memcpy(&time_base, &msg->as_can_message.msg, sizeof(CAN_MESSAGE));
            }
        }
    }
}

void handle_time_data(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_CAN0_RX_MESSAGE) {
        if (msg->as_can_message.msg.interface == CAN_INTERFACE_0) {
            const uint32_t id = 0x18FEE6EE;
            if (msg->as_can_message.msg.can_id == id) {
                uint8_t minute = msg->as_can_message.msg.can_data.byte1;
                uint8_t hour = msg->as_can_message.msg.can_data.byte2;

                uint8_t month = msg->as_can_message.msg.can_data.byte3 & 0x0F;
                uint8_t day = msg->as_can_message.msg.can_data.byte4 >> 2;
                uint16_t year = msg->as_can_message.msg.can_data.byte6;
                year <<= 4;
                year |= (msg->as_can_message.msg.can_data.byte7 >> 4) & 0x0F;
                event_vehicle_time_changed_emit(hour, minute, day, month, year);
            }
        }
    }
}

void daily_2020_nis_no_nav_init(void) {
    struct CANBUS_CONFIG can_conf = {0};
    can_conf.can0.listen_mode = false;

    LOG_DEF_NORMAL("Daily 2020 NIS NO NAV\r\n");
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xA18A000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xA18A001);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0xA28A000);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x18A7FF21);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x18FE0F17);
    canbus_add_accepted_id(CAN_INTERFACE_0, 0x18FEE6EE);
    can_conf.can0.can_speed = 500;

    event_connect_callback(eEVENT_RADIO_MEDIA_CHANGED, handle_source_type);
    event_connect_callback(eEVENT_PHONE_INFO, handle_phone_info);
    canbus_init(&can_conf);

    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_revmov);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_lights);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_can_ignition);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_speed);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_swc);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_time_base);
    event_connect_callback(eEVENT_CAN0_RX_MESSAGE, handle_time_data);
    event_connect_callback(eEVENT_TIME_CHANGED, iveco_handle_minutes_changed);
    event_connect_callback(eEVENT_TIME_FORMAT_CHANGED, iveco_timedate_format_changed);
    alpine_menu_disable_year_setup();
    alpine_menu_disable_12_24_setup();
    set_vehicle_change_date_func(iveco_send_changed_clock_date);

    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);

    wisker_reset(MODULE_VEHICLE);
    canbus0_enable_tx();
}