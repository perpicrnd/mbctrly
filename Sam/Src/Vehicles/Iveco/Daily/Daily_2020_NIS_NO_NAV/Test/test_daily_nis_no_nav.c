#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <cmocka.h>
#include "daily.c"
int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

void software_reset(void) {
}
void event_out_service_emit(void) {
}

void __wrap_canbus_add_accepted_id(CAN_INTERFACE interface, uint32_t num) {
    check_expected(interface);
    check_expected(num);
}

void __wrap_event_connect_callback(enum EVENTS event, EVENT_HANDLER handler) {
    check_expected(event);
    check_expected(handler);
}

void __wrap_canbus_init(struct CANBUS_CONFIG *can_conf) {
    (void)can_conf;
    function_called();
}

void __wrap_wisker_reset(MODULES module) {
    check_expected(module);
}

void __wrap_canbus0_enable_tx(void) {
    function_called();
}

void __wrap_event_swc_emit(enum SWC status, enum SWC_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void __wrap_event_ignition_emit(enum IGNITION_STATUS status) {
    check_expected(status);
}

void __wrap_event_reverse_emit(enum REVERSE_STATUS status) {
    check_expected(status);
}

void __wrap_event_lights_emit(enum LIGHTS_STATUS status) {
    check_expected(status);
}

void __wrap_event_speed_emit(uint32_t status) {
    check_expected(status);
}

void __wrap_event_handbrake_emit(enum HANDBRAKE_STATUS status, enum HANDBRAKE_MODULE module) {
    check_expected(status);
    check_expected(module);
}

void __wrap_set_vehicle_change_date_func(VEHICLE_CHANGE_TIME_DATE func) {
    check_expected(func);
}

void __wrap_alpine_menu_disable_year_setup(void) {
    function_called();
}

void __wrap_event_vehicle_time_changed_emit(uint8_t hour, uint8_t minute, uint8_t day, uint8_t month, uint16_t year) {
    check_expected(hour);
    check_expected(minute);
    check_expected(day);
    check_expected(month);
    check_expected(year);
}

CAN_MESSAGE last_tx = {0};
void event_can0_tx_message_emit(const CAN_MESSAGE *const msg) {
    memcpy(&last_tx, msg, sizeof(CAN_MESSAGE));
}

int setup(void **state) {
    (void)state;
    NumLoops = 2;
    radio_source = RADIO_SOURCE_OFF;
    phone_status = PHONE_STATUS_INACTIVE;
    cur_active = SWC_NO_BUTTON_PRESSED;
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;
    can_ignition = IGNITION_OFF;
    return 0;
}

static CAN_MESSAGE vol_up = {0x18A7FF21, true, {0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE vol_down = {0x18A7FF21, true, {0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE mute = {0x18A7FF21, true, {0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE seek_up = {0x18A7FF21, true, {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE seek_down = {0x18A7FF21, true, {0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE source = {0x18A7FF21, true, {0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE phone = {0x18A7FF21, true, {0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE voice = {0x18A7FF21, true, {0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE no_press = {0x18A7FF21, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE ignition_on = {0xA18A000, true, {0x00, 0x00, 0x48, 0x7C, 0x02, 0x00, 0x20, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE ignition_off = {0xA18A000, true, {0x00, 0x00, 0x28, 0x7C, 0x02, 0x00, 0x20, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE ignition_crank = {0xA18A000, true, {0x00, 0x00, 0xC8, 0x7C, 0x02, 0x00, 0x20, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE reverse_on = {0xA18A001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE reverse_off = {0xA18A001, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE lights_on = {0xA18A000, true, {0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE lights_off = {0xA18A000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE speed_0 = {0xA28A000, true, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE speed_10 = {0xA28A000, true, {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE h12 = {0x18FE0F17, true, {0x65, 0x6E, 0x5F, 0x00, 0x30, 0x3F, 0xFF, 0xFF}, 8, CAN_INTERFACE_0};
static CAN_MESSAGE h24 = {0x18FE0F17, true, {0x65, 0x6E, 0x4F, 0x00, 0x30, 0x3F, 0xFF, 0xFF}, 8, CAN_INTERFACE_0};

void test_swc_vol_up(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_VOL_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_vol_down(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &vol_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_VOL_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_mute(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &mute, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &mute, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &mute, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_MUTE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_seek_up(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_up, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_UP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_seek_down(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &seek_down, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SEEK_DOWN_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_source(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &source, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &source, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &source, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SOURCE_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_voice(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &voice, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &voice, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &voice, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_SPEECH_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_swc_phone(void **state) {
    (void)state;

    union EVENT swc;
    swc.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    phone_status = PHONE_STATUS_INACTIVE;
    radio_source = RADIO_SOURCE_TUNER;
    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &phone, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    radio_source = RADIO_SOURCE_PHONE;
    phone_status = PHONE_STATUS_RINGING_INCOMING;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &phone, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_TEL_PICKUP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    phone_status = PHONE_STATUS_CONNECTED;
    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &phone, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_TEL_HANGUP_RELEASED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);

    expect_value(__wrap_event_swc_emit, status, SWC_NO_BUTTON_PRESSED);
    expect_value(__wrap_event_swc_emit, module, SWC_MODULE_VEHICLE);
    memcpy(&swc.as_can_message.msg, &no_press, sizeof(CAN_MESSAGE));
    handle_swc(&swc);
}

void test_ignition_can(void **state) {
    (void)state;

    union EVENT ignition;
    ignition.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_on, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_crank, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    memcpy(&ignition.as_can_message.msg, &ignition_off, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    //Alzo sottochiave filare
    wired_ignition_0 = IGNITION_ON;
    ignition.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_on, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_crank, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_off, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    //Alzo sottochiave filare
    wired_ignition_0 = IGNITION_ON;
    wired_ignition_1 = IGNITION_ON;
    ignition.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_on, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_crank, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_off, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    //Alzo sottochiave filare
    wired_ignition_0 = IGNITION_OFF;
    ignition.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_on, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_crank, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_off, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    //Abbasso sottochiave filare
    wired_ignition_0 = IGNITION_OFF;
    wired_ignition_1 = IGNITION_OFF;
    ignition.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_on, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    memcpy(&ignition.as_can_message.msg, &ignition_crank, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);

    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    memcpy(&ignition.as_can_message.msg, &ignition_off, sizeof(CAN_MESSAGE));
    handle_can_ignition(&ignition);
}

void test_ignition_wired(void **state) {
    (void)state;
    union EVENT ignition;
    ignition.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    ignition.as_wired_ignition.ignition = IGNITION_ON;
    ignition.as_wired_ignition.module = 0;
    handle_wired_ignition(&ignition);

    ignition.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    ignition.as_wired_ignition.ignition = IGNITION_ON;
    ignition.as_wired_ignition.module = 1;
    handle_wired_ignition(&ignition);

    ignition.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    ignition.as_wired_ignition.ignition = IGNITION_OFF;
    ignition.as_wired_ignition.module = 0;
    handle_wired_ignition(&ignition);

    can_ignition = IGNITION_ON;
    ignition.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_ON);
    ignition.as_wired_ignition.ignition = IGNITION_OFF;
    ignition.as_wired_ignition.module = 1;
    handle_wired_ignition(&ignition);

    can_ignition = IGNITION_OFF;
    ignition.as_generic.event = eEVENT_WIRED_IGNITION_CHANGED;
    expect_value(__wrap_event_ignition_emit, status, IGNITION_OFF);
    ignition.as_wired_ignition.ignition = IGNITION_OFF;
    ignition.as_wired_ignition.module = 1;
    handle_wired_ignition(&ignition);
}

void test_reverse(void **state) {
    (void)state;

    union EVENT reverse;
    reverse.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_reverse_emit, status, REVERSE_ON);
    memcpy(&reverse.as_can_message.msg, &reverse_on, sizeof(CAN_MESSAGE));
    handle_revmov(&reverse);

    expect_value(__wrap_event_reverse_emit, status, REVERSE_OFF);
    memcpy(&reverse.as_can_message.msg, &reverse_off, sizeof(CAN_MESSAGE));
    handle_revmov(&reverse);
}

void test_lights(void **state) {
    (void)state;

    union EVENT lights;
    lights.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_lights_emit, status, REVERSE_ON);
    memcpy(&lights.as_can_message.msg, &lights_on, sizeof(CAN_MESSAGE));
    handle_lights(&lights);

    expect_value(__wrap_event_lights_emit, status, REVERSE_OFF);
    memcpy(&lights.as_can_message.msg, &lights_off, sizeof(CAN_MESSAGE));
    handle_lights(&lights);
}

void test_speed(void **state) {
    (void)state;

    union EVENT speed;
    speed.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_handbrake_emit, module, HANDBRAKE_MODULE_SPEED);
    memcpy(&speed.as_can_message.msg, &speed_0, sizeof(CAN_MESSAGE));
    old_speed = 0;
    handle_speed(&speed);
    old_speed = 30;
    expect_value(__wrap_event_speed_emit, status, 0x00);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_ON);
    expect_value(__wrap_event_handbrake_emit, module, HANDBRAKE_MODULE_SPEED);
    handle_speed(&speed);

    expect_in_range(__wrap_event_speed_emit, status, 0x01, 0x1000);
    expect_value(__wrap_event_handbrake_emit, status, HANDBRAKE_OFF);
    expect_value(__wrap_event_handbrake_emit, module, HANDBRAKE_MODULE_SPEED);
    memcpy(&speed.as_can_message.msg, &speed_10, sizeof(CAN_MESSAGE));
    handle_speed(&speed);
}

void test_time_send(void **state) {
    (void)state;

    // Data e ora: 30/11 21:33

    // 18FEE641 => xx 21 14 0B 78 0E 7D 7E
    // 18FEE6EE => xx 21 15 0B 78 0E 7D 7D
    iveco_send_changed_clock_date(2007, 11, 30, 21, 33);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x21);
    assert_true(last_tx.can_data.byte2 == 0x14);
    assert_true(last_tx.can_data.byte3 == 0x0B);
    assert_true(last_tx.can_data.byte4 == 0x78);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    // Data e ora: 30/11 22:34

    // 18FEE641 => xx 22 15 0B 78 0E 7D 7E
    // 18FEE6EE => xx 22 16 0B 78 0E 7D 7D
    iveco_send_changed_clock_date(2007, 11, 30, 22, 34);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x22);
    assert_true(last_tx.can_data.byte2 == 0x15);
    assert_true(last_tx.can_data.byte3 == 0x0B);
    assert_true(last_tx.can_data.byte4 == 0x78);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    // Data e ora: 30/11 01:56

    // 18FEE641 => xx 38 00 0B 78 0E 7D 7E
    // 18FEE6EE => xx 38 01 0B 78 0E 7D 7D
    iveco_send_changed_clock_date(2007, 11, 30, 1, 56);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x38);
    assert_true(last_tx.can_data.byte2 == 0x00);
    assert_true(last_tx.can_data.byte3 == 0x0B);
    assert_true(last_tx.can_data.byte4 == 0x78);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    //     Data e ora: 01/12 00:59

    // 18FEE641 => xx 3B 17 0B 78 0E 7D 7E
    // 18FEE6EE => xx 3B 00 0C 04 0E 7D 7D
    iveco_send_changed_clock_date(2007, 12, 1, 0, 59);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x3B);
    assert_true(last_tx.can_data.byte2 == 0x17);
    assert_true(last_tx.can_data.byte3 == 0x0C);
    assert_true(last_tx.can_data.byte4 == 0x04);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    //     Data e ora: 30/11 23:59

    // 18FEE641 => xx 3B 16 0B 78 0E 7D 7E
    // 18FEE6EE => xx 3B 17 0B 78 0E 7D 7D
    iveco_send_changed_clock_date(2007, 11, 30, 23, 59);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x3B);
    assert_true(last_tx.can_data.byte2 == 0x16);
    assert_true(last_tx.can_data.byte3 == 0x0B);
    assert_true(last_tx.can_data.byte4 == 0x78);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    //     Data e ora: 30/12 23:59

    // 18FEE641 => xx 3B 16 0C 78 0E 7D 7E
    iveco_send_changed_clock_date(2007, 12, 30, 23, 59);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x3B);
    assert_true(last_tx.can_data.byte2 == 0x16);
    assert_true(last_tx.can_data.byte3 == 0x0C);
    assert_true(last_tx.can_data.byte4 == 0x78);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    //     Data e ora: 31/01 01:04

    // 18FEE641 => xx 04 00 01 7C 0E 7D 7E
    iveco_send_changed_clock_date(2007, 1, 31, 1, 4);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x04);
    assert_true(last_tx.can_data.byte2 == 0x00);
    assert_true(last_tx.can_data.byte3 == 0x01);
    assert_true(last_tx.can_data.byte4 == 0x7C);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);

    //     Data e ora: 31/12 01:04

    // 18FEE641 => xx 04 00 0C 7C 0E 7D 7E
    iveco_send_changed_clock_date(2007, 12, 31, 1, 4);
    assert_true(last_tx.interface == CAN_INTERFACE_0);
    assert_true(last_tx.can_id == 0x18FEE641);
    assert_true(last_tx.can_is_ext);
    assert_true(last_tx.can_data.byte0 == 0x00);
    assert_true(last_tx.can_data.byte1 == 0x04);
    assert_true(last_tx.can_data.byte2 == 0x00);
    assert_true(last_tx.can_data.byte3 == 0x0C);
    assert_true(last_tx.can_data.byte4 == 0x7C);
    assert_true(last_tx.can_data.byte5 == 0x0E);
    assert_true(last_tx.can_data.byte6 == 0x7D);
    assert_true(last_tx.can_data.byte7 == 0x7E);
}

void test_time_recv(void **state) {
    (void)state;
    {
        // Data e ora: 30/11 21:33

        // 18FEE641 => xx 21 14 0B 78 0E 7D 7E
        // 18FEE6EE => xx 21 15 0B 78 0E 7D 7D
        CAN_MESSAGE canmsg = {0x18FEE6EE, true, {0x00, 0x21, 0x15, 0x0B, 0x78, 0x0E, 0x7D, 0x7D}, 8, CAN_INTERFACE_0};
        union EVENT event;
        event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
        memcpy(&event.as_can_message.msg, &canmsg, sizeof(CAN_MESSAGE));
        expect_value(__wrap_event_vehicle_time_changed_emit, hour, 21);
        expect_value(__wrap_event_vehicle_time_changed_emit, minute, 33);
        expect_value(__wrap_event_vehicle_time_changed_emit, day, 30);
        expect_value(__wrap_event_vehicle_time_changed_emit, month, 11);
        expect_value(__wrap_event_vehicle_time_changed_emit, year, 2007);
        handle_time_data(&event);
    }
    {
        // Data e ora: 30/11 22:34

        // 18FEE641 => xx 22 15 0B 78 0E 7D 7E
        // 18FEE6EE => xx 22 16 0B 78 0E 7D 7D
        CAN_MESSAGE canmsg = {0x18FEE6EE, true, {0x00, 0x22, 0x16, 0x0B, 0x78, 0x0E, 0x7D, 0x7D}, 8, CAN_INTERFACE_0};
        union EVENT event;
        event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
        memcpy(&event.as_can_message.msg, &canmsg, sizeof(CAN_MESSAGE));
        expect_value(__wrap_event_vehicle_time_changed_emit, hour, 22);
        expect_value(__wrap_event_vehicle_time_changed_emit, minute, 34);
        expect_value(__wrap_event_vehicle_time_changed_emit, day, 30);
        expect_value(__wrap_event_vehicle_time_changed_emit, month, 11);
        expect_value(__wrap_event_vehicle_time_changed_emit, year, 2007);
        handle_time_data(&event);
    }
    {
        // Data e ora: 30/11 01:56

        // 18FEE641 => xx 38 00 0B 78 0E 7D 7E
        // 18FEE6EE => xx 38 01 0B 78 0E 7D 7D
        CAN_MESSAGE canmsg = {0x18FEE6EE, true, {0x00, 0x38, 0x01, 0x0B, 0x78, 0x0E, 0x7D, 0x7D}, 8, CAN_INTERFACE_0};
        union EVENT event;
        event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
        memcpy(&event.as_can_message.msg, &canmsg, sizeof(CAN_MESSAGE));
        expect_value(__wrap_event_vehicle_time_changed_emit, hour, 1);
        expect_value(__wrap_event_vehicle_time_changed_emit, minute, 56);
        expect_value(__wrap_event_vehicle_time_changed_emit, day, 30);
        expect_value(__wrap_event_vehicle_time_changed_emit, month, 11);
        expect_value(__wrap_event_vehicle_time_changed_emit, year, 2007);
        handle_time_data(&event);
    }
    {
        // Data e ora: 01/12 00:59

        // 18FEE641 => xx 3B 17 0B 78 0E 7D 7E
        // 18FEE6EE => xx 3B 00 0C 04 0E 7D 7D
        CAN_MESSAGE canmsg = {0x18FEE6EE, true, {0x00, 0x3B, 0x00, 0x0C, 0x04, 0x0E, 0x7D, 0x7D}, 8, CAN_INTERFACE_0};
        union EVENT event;
        event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
        memcpy(&event.as_can_message.msg, &canmsg, sizeof(CAN_MESSAGE));
        expect_value(__wrap_event_vehicle_time_changed_emit, hour, 0);
        expect_value(__wrap_event_vehicle_time_changed_emit, minute, 59);
        expect_value(__wrap_event_vehicle_time_changed_emit, day, 1);
        expect_value(__wrap_event_vehicle_time_changed_emit, month, 12);
        expect_value(__wrap_event_vehicle_time_changed_emit, year, 2007);
        handle_time_data(&event);
    }
    {
        //     Data e ora: 30/11 23:59

        // 18FEE641 => xx 3B 16 0B 78 0E 7D 7E
        // 18FEE6EE => xx 3B 17 0B 78 0E 7D 7D
        CAN_MESSAGE canmsg = {0x18FEE6EE, true, {0x00, 0x3B, 0x17, 0x0B, 0x78, 0x0E, 0x7D, 0x7D}, 8, CAN_INTERFACE_0};
        union EVENT event;
        event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
        memcpy(&event.as_can_message.msg, &canmsg, sizeof(CAN_MESSAGE));
        expect_value(__wrap_event_vehicle_time_changed_emit, hour, 23);
        expect_value(__wrap_event_vehicle_time_changed_emit, minute, 59);
        expect_value(__wrap_event_vehicle_time_changed_emit, day, 30);
        expect_value(__wrap_event_vehicle_time_changed_emit, month, 11);
        expect_value(__wrap_event_vehicle_time_changed_emit, year, 2007);
        handle_time_data(&event);
    }
}

void test_time_12_24h(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    memcpy(&event.as_can_message.msg, &h12, sizeof(CAN_MESSAGE));
    //Sono impostato in 12h.
    handle_time_base(&event);

    //Arriva una conferma di mantenere a 12h
    union EVENT event_tf;
    event_tf.as_generic.event = eEVENT_TIME_FORMAT_CHANGED;
    event_tf.as_time_format.time_format = 0x01;
    iveco_timedate_format_changed(&event_tf);
    assert_true(last_tx.interface == h12.interface);
    assert_true(last_tx.can_id == 0x18FE0F41);
    assert_true(last_tx.can_is_ext == h12.can_is_ext);
    assert_true(last_tx.can_data.byte0 == h12.can_data.byte0);
    assert_true(last_tx.can_data.byte1 == h12.can_data.byte1);
    assert_true(last_tx.can_data.byte2 == h12.can_data.byte2);
    assert_true(last_tx.can_data.byte3 == h12.can_data.byte3);
    assert_true(last_tx.can_data.byte4 == h12.can_data.byte4);
    assert_true(last_tx.can_data.byte5 == h12.can_data.byte5);
    assert_true(last_tx.can_data.byte6 == h12.can_data.byte6);
    assert_true(last_tx.can_data.byte7 == h12.can_data.byte7);

    //Adesso trasmetto 24h
    event_tf.as_time_format.time_format = 0x00;
    iveco_timedate_format_changed(&event_tf);
    assert_true(last_tx.interface == h24.interface);
    assert_true(last_tx.can_id == 0x18FE0F41);
    assert_true(last_tx.can_is_ext == h24.can_is_ext);
    assert_true(last_tx.can_data.byte0 == h24.can_data.byte0);
    assert_true(last_tx.can_data.byte1 == h24.can_data.byte1);
    assert_true(last_tx.can_data.byte2 == h24.can_data.byte2);
    assert_true(last_tx.can_data.byte3 == h24.can_data.byte3);
    assert_true(last_tx.can_data.byte4 == h24.can_data.byte4);
    assert_true(last_tx.can_data.byte5 == h24.can_data.byte5);
    assert_true(last_tx.can_data.byte6 == h24.can_data.byte6);
    assert_true(last_tx.can_data.byte7 == h24.can_data.byte7);
}

void test_time_24_12h(void **state) {
    (void)state;
    union EVENT event;
    event.as_generic.event = eEVENT_CAN0_RX_MESSAGE;
    memcpy(&event.as_can_message.msg, &h24, sizeof(CAN_MESSAGE));
    //Sono impostato in 24h.
    handle_time_base(&event);

    //Arriva una conferma di mantenere a 24h
    union EVENT event_tf;
    event_tf.as_generic.event = eEVENT_TIME_FORMAT_CHANGED;
    event_tf.as_time_format.time_format = 0x00;
    iveco_timedate_format_changed(&event_tf);
    assert_true(last_tx.interface == h24.interface);
    assert_true(last_tx.can_id == 0x18FE0F41);
    assert_true(last_tx.can_is_ext == h24.can_is_ext);
    assert_true(last_tx.can_data.byte0 == h24.can_data.byte0);
    assert_true(last_tx.can_data.byte1 == h24.can_data.byte1);
    assert_true(last_tx.can_data.byte2 == h24.can_data.byte2);
    assert_true(last_tx.can_data.byte3 == h24.can_data.byte3);
    assert_true(last_tx.can_data.byte4 == h24.can_data.byte4);
    assert_true(last_tx.can_data.byte5 == h24.can_data.byte5);
    assert_true(last_tx.can_data.byte6 == h24.can_data.byte6);
    assert_true(last_tx.can_data.byte7 == h24.can_data.byte7);

    //Adesso trasmetto 12h
    event_tf.as_time_format.time_format = 0x01;
    iveco_timedate_format_changed(&event_tf);
    assert_true(last_tx.interface == h12.interface);
    assert_true(last_tx.can_id == 0x18FE0F41);
    assert_true(last_tx.can_is_ext == h12.can_is_ext);
    assert_true(last_tx.can_data.byte0 == h12.can_data.byte0);
    assert_true(last_tx.can_data.byte1 == h12.can_data.byte1);
    assert_true(last_tx.can_data.byte2 == h12.can_data.byte2);
    assert_true(last_tx.can_data.byte3 == h12.can_data.byte3);
    assert_true(last_tx.can_data.byte4 == h12.can_data.byte4);
    assert_true(last_tx.can_data.byte5 == h12.can_data.byte5);
    assert_true(last_tx.can_data.byte6 == h12.can_data.byte6);
    assert_true(last_tx.can_data.byte7 == h12.can_data.byte7);
}

void test_daily_init(void **state) {
    (void)state;
    expect_value(__wrap_canbus_add_accepted_id, num, 0xA18A000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0xA18A001);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0xA28A000);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x18A7FF21);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x18FE0F17);
    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_canbus_add_accepted_id, num, 0x18FEE6EE);

    expect_value(__wrap_canbus_add_accepted_id, interface, CAN_INTERFACE_0);
    expect_value(__wrap_event_connect_callback, event, eEVENT_RADIO_MEDIA_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_source_type);
    expect_value(__wrap_event_connect_callback, event, eEVENT_PHONE_INFO);
    expect_value(__wrap_event_connect_callback, handler, handle_phone_info);
    expect_function_call(__wrap_canbus_init);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_revmov);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_lights);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_can_ignition);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_speed);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_swc);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_time_base);
    expect_value(__wrap_event_connect_callback, event, eEVENT_CAN0_RX_MESSAGE);
    expect_value(__wrap_event_connect_callback, handler, handle_time_data);

    expect_value(__wrap_event_connect_callback, event, eEVENT_TIME_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, iveco_handle_minutes_changed);
    expect_value(__wrap_event_connect_callback, event, eEVENT_TIME_FORMAT_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, iveco_timedate_format_changed);
    expect_function_call(__wrap_alpine_menu_disable_year_setup);
    expect_value(__wrap_set_vehicle_change_date_func, func, iveco_send_changed_clock_date);

    expect_value(__wrap_event_connect_callback, event, eEVENT_WIRED_IGNITION_CHANGED);
    expect_value(__wrap_event_connect_callback, handler, handle_wired_ignition);
    expect_value(__wrap_wisker_reset, module, MODULE_VEHICLE);
    expect_function_call(__wrap_canbus0_enable_tx);
    daily_2020_nis_no_nav_init();
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup(test_daily_init, setup),
        cmocka_unit_test_setup(test_swc_vol_up, setup),
        cmocka_unit_test_setup(test_swc_vol_down, setup),
        cmocka_unit_test_setup(test_swc_mute, setup),
        cmocka_unit_test_setup(test_swc_seek_up, setup),
        cmocka_unit_test_setup(test_swc_seek_down, setup),
        cmocka_unit_test_setup(test_swc_source, setup),
        cmocka_unit_test_setup(test_swc_voice, setup),
        cmocka_unit_test_setup(test_swc_phone, setup),
        cmocka_unit_test_setup(test_ignition_can, setup),
        cmocka_unit_test_setup(test_ignition_wired, setup),
        cmocka_unit_test_setup(test_reverse, setup),
        cmocka_unit_test_setup(test_lights, setup),
        cmocka_unit_test_setup(test_speed, setup),
        cmocka_unit_test_setup(test_time_send, setup),
        cmocka_unit_test_setup(test_time_recv, setup),

        cmocka_unit_test_setup(test_time_12_24h, setup),
        cmocka_unit_test_setup(test_time_24_12h, setup),

    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}