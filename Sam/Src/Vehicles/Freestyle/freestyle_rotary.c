#include "freestyle_rotary.h"
#include "alpine_menu.h"
#include "alpine_task.h"
#include "board_version.h"
#include "event_list.h"
#include "ignition.h"
#include "logger.h"
#include "main.h"
#include "menu_manager.h"
#include "menu_walking.h"
#include "radio_message.h"
#include "settings.h"
#include "vehicle.h"
#include "wisker.h"

static enum IGNITION_STATUS wired_ignition_0 = IGNITION_OFF;
static enum IGNITION_STATUS wired_ignition_1 = IGNITION_OFF;

static void event_freestyle_rotary_ignition_emit(void) {
    enum IGNITION_STATUS ignition = IGNITION_OFF;

    if ((wired_ignition_0 == IGNITION_ON) ||
        (wired_ignition_1 == IGNITION_ON)) {
        ignition = IGNITION_ON;
    } else {
        ignition = IGNITION_OFF;
    }
    event_ignition_emit(ignition);
}

static void handle_wired_ignition(const union EVENT *const msg) {
    if (msg->as_generic.event == eEVENT_WIRED_IGNITION_CHANGED) {
        switch (msg->as_wired_ignition.module) {
            case 0:
                wired_ignition_0 = msg->as_wired_ignition.ignition;
                break;
            case 1:
                wired_ignition_1 = msg->as_wired_ignition.ignition;
                break;
        }
        event_freestyle_rotary_ignition_emit();
    }
}

void freestyle_rotary_init(void) {
    board_set_version(BOARD_VERSION_00);
    wisker_disable_module(MODULE_UART_LOWLEVEL);
    alpine_menu_disable_ignition_lines();
    alpine_menu_disable_reverse_lines();
    alpine_menu_disable_vehicle_settings();
    alpine_menu_disable_can_management();
    alpine_menu_disable_clock_management();
    alpine_menu_disable_ignition_logic();
    alpine_menu_disable_preflight_check();
    alpine_menu_disable_lin_management();
    event_connect_callback(eEVENT_WIRED_IGNITION_CHANGED, handle_wired_ignition);
}