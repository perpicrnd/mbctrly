---
size: 11pt
draft: no
head:
    address: '123 Research Ave, Science Building 2'
    city: Cambridge
    state: MA
    zip: 02114
    phone: '\(121\) 555-2234'
    fax: '\(999\) 555-6565'
    email: 'hannigan@gmail.com'
    dept: "Microbiology & Bioinformatics"
sig:
 include: yes
 sign: yes
 name: 'Geoffrey Hannigan, PhD'
 post: Bioinformatics Scientist
 secondpost: Distinguished Second Appointment
---


# MBCTRLY Ducato Development test

1. [MBCTRLY Ducato Development test](#mbctrly-ducato-development-test)
   1. [Power consumption](#power-consumption)
   2. [Interface wakeup](#interface-wakeup)
   3. [Ignition ON / OFF test](#ignition-on--off-test)
   4. [Reverse ON / OFF test](#reverse-on--off-test)
   5. [Handbrake](#handbrake)
   6. [Speed Pulse](#speed-pulse)
   7. [Steering wheel commands](#steering-wheel-commands)
   8. [Led colors](#led-colors)
   9. [Preflight Check](#preflight-check)
   10. [Factory Reset](#factory-reset)

## Power consumption

Power consumption in run mode is below 50mA.
Power consumption in sleep mode is below 5mA.

## Interface wakeup

Interface can be waken up by:

- Can activity.
- Buttons press on Alpine rotary encoder interfaces.
- IR remote button presses.

## Ignition ON / OFF test

- Ignition IN 1 wire turns ON the radio while the interface is in sleep mode.
- Ignition IN 1 wire turns ON the radio while the interface is not in sleep mode.
- MENU Button on Alpine rotary encoder interface turns ON the radio while the interface is in sleep mode.
- Menu Button on Alpine rotary encoder interface turns ON the radio while the interface is not in sleep mode.
- Power IR remote turns on the radio while the interface is in sleep mode.
- Power IR remote turns on the radio while the interface is not in sleep mode.
- CAN messages can turn the radio ON/OFF in respect to the Door settings. (Door settings is available only if CAN message provides Ignition status and Door status).
- Ignition IN 1 wire turns OFF the radio while the radio is ON.
- Menu Button on Alpine rotary encoder interface turns OFF the radio while the radio is ON.
- Power IR remote turns OFF the radio while the radio is ON.
- Verified that MENU button is intercepted by the interface and does not activate "Screen OFF".
- Verified that Power IR remote is intercapted by the interface and does not activate "Screen OFF".
- Verified Fiat Ducato configuration KEY turns ON/OFF the radio correctly.
- Verified Fiat Ducato configuration DOOR turns ON/OFF the radio correctly.
- Verified configurable HOLD time on ignition ON at 5 minutes.
- Verified manual operations on IR override Ignition HOLD minutes.
- Verified manual operations on MENU button override Ignition HOLD minutes.
- Given the fact that MENU and IR are managed to the interface only when Alpine buttons with rotary encoder are used, the Ignition functions override is only available when the Alpine buttons with rotary encoder are used.

## Reverse ON / OFF test

- Verified CAN message can turn ON / OFF the reverse gear feed to the radio.
- Verified HOLD OFF / HOLD ON configurable menu.
- Verified that a CAN speed information higher than 14km/h triggers the REVERSE OFF to the radio as soon as the speed is reached without respecting the timeout value configured.
- The reverse signal triggers the correct value on the Alpine radio.
- It was observed that the Reverse OFF wire triggers 0V as soon as the input value is received by the interface but the radio uses an internal timeout before turning the reverse OFF.

## Handbrake

Verified in INSTALLATION STATUS the correct value of PARK signal.

## Speed Pulse

Verified Speed pulse at 1 Hz / Km/h. In INSTALLATION STATUS no Speed Signal value is displayed.

## Steering wheel commands

Verified analog SWC and they are working as expected. Function of the buttons are mapped exactly as resistive Alpine ducato interface XXX-MRA-25.
From top left to bottom down as:

- Mute
- Vol+
- Vol-
- Siri

- Tel pickup during call, Siri other media
- Seek+
- Seek-
- Tel hangup during call, Source other media

## Led colors

Verified the LED changing color functionality. When the interface goes in sleep mode the new setting is saved.

## Preflight Check

Verified the Pre-Flight Check setting using a custom message. It is not yet tested in vehicle.
The message text:

>Before driving
>
>Don't use the smartphone while driving and lock the safety belt.

Is a placeholder for the real text that will be provided by the Alpine.

## Factory Reset

Factory reset was tested changing some value and verifying that when the radio turns on again the values are changed to the original values.
During this stage the saved memory area is changed and the original data are used.
