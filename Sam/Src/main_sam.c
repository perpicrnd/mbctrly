#include "asf.h"
#include "event_list_provider.h"
#include "logger.h"
#include "main.h"
#include "watchdog.h"
/**
 * @brief variabile inizializzata in runtime contenente la dimensione della flash.
 */
uint32_t iflash0_size;

typedef enum {
    ALARM_SOURCE = 0x08,
    ENABSENS_SOURCE = 0x80,
    CAN_0_R_SOURCE = 0x02,
    CAN_1_R_SOURCE = 0x8000,
} INTERRUPT_SOURCE;

/**
 * @brief Flag che indica la presenza di un interrupt e quindi che devo riavviare in running mode.
 * 
 */
static volatile bool running_mode_requested = false;
/**
 * @brief Flag per differenziare la modalità di funzionamento.
 * 
 */
static volatile bool board_running = false;

#define INT_PORT_A_MASK (Ign_In_1_ISR | Can_0_ISR | Rxd1_ISR)
#define INT_PORT_B_MASK (Ign_In_2_ISR | Can_1_ISR)

void PIOA_Handler(void) {
    int32_t temp = pio_get_interrupt_status(PIOA);
    if ((temp & Can_0_ISR) == Can_0_ISR) {
        pio_disable_interrupt(PIOA, Can_0_ISR);
        running_mode_requested = true;
    }
    if ((temp & Ign_In_1_ISR) == Ign_In_1_ISR) {
        pio_disable_interrupt(PIOA, Ign_In_1_ISR);
        running_mode_requested = true;
    }
    if ((temp & Rxd1_ISR) == Rxd1_ISR) {
        pio_disable_interrupt(PIOA, Rxd1_ISR);
        SWO_PrintStringDef("WAKE UP\r\n");
        running_mode_requested = true;
    }
}

void PIOB_Handler(void) {
    int32_t temp = pio_get_interrupt_status(PIOB);  //cancella i flag di interrupt
    if ((temp & Can_1_ISR) == Can_1_ISR) {          //verifica l'interrupt di CAN_1_R
        pio_disable_interrupt(PIOB, Can_1_ISR);
        running_mode_requested = true;
    }
    if ((temp & Ign_In_2_ISR) == Ign_In_2_ISR) {
        pio_disable_interrupt(PIOB, Ign_In_2_ISR);
        running_mode_requested = true;
    }
}

void init_interrupt_for_sleep(void) {
    ioport_init();
    // ioport_set_pin_dir(Alarm_Notify, IOPORT_DIR_INPUT);			//i pin di interrupt tutti in INPUT
    ioport_set_pin_dir(Can_0_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Can_1_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(Rxd_1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Rxd_1, IOPORT_MODE_PULLUP);

    pio_configure_interrupt(PIOA, INT_PORT_A_MASK, PIO_IT_LOW_LEVEL);  //interrupt generato sul LOW LEVEL
    pio_configure_interrupt(PIOB, INT_PORT_B_MASK, PIO_IT_LOW_LEVEL);  //interrupt generato sul LOW LEVEL

    NVIC_DisableIRQ(PIOA_IRQn);
    NVIC_ClearPendingIRQ(PIOA_IRQn);
    NVIC_SetPriority(PIOA_IRQn, 0);  //Ok 0 perchè no RTOS in questa modalità
    NVIC_EnableIRQ(PIOA_IRQn);

    NVIC_DisableIRQ(PIOB_IRQn);
    NVIC_ClearPendingIRQ(PIOB_IRQn);
    NVIC_SetPriority(PIOB_IRQn, 0);  //Ok 0 perchè no RTOS in questa modalità
    NVIC_EnableIRQ(PIOB_IRQn);
}

void enable_interrupt_for_sleep(void) {
    pio_enable_interrupt(PIOA, INT_PORT_A_MASK);  //abilita i pin a generare l'interrupt
    pio_enable_interrupt(PIOB, INT_PORT_B_MASK);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT0);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT1);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT3);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT7);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT12);
    pmc_set_fast_startup_input(PMC_FSMR_FSTT13);
}

static inline void init_board_sleep_mode() {
    //Inizializzazione
    init_interrupt_for_sleep();
    enable_interrupt_for_sleep();

    osc_enable(OSC_SLCK_32K_RC);
    osc_wait_ready(OSC_SLCK_32K_RC);
    pmc_switch_mck_to_sclk(CONFIG_SYSCLK_PRES);
    pmc_osc_disable_main_xtal();
    pmc_osc_disable_fastrc();

    system_init_flash(sysclk_get_cpu_hz());
    SystemCoreClockUpdate();
    ioport_init();
    board_init_sleep();

    for (;;) {
        //Verifico se fino ad adesso ho ricevuto una richiesta di reboot
        SWO_PrintStringDef("Wait Sleep\r\n");
        if (running_mode_requested == false) {
            ioport_set_pin_level(En_Services, false);
            ioport_set_pin_level(Ign_Out, false);
            enable_interrupt_for_sleep();
            watchdog_reset();

        } else {
            SWO_PrintStringDef("Awaking\r\n");
            break;
        }
    }
}

static uint32_t unique_can_id = 0;
uint32_t maybe_unique_can_id(void) {
    unique_can_id &= 0x1FFFFFFF;
    return unique_can_id;
}

static inline void init_board_running() {
    ///Verifica della reset cause del micro.
    uint8_t reset_cause = (rstc_get_reset_cause(RSTC) & RSTC_SR_RSTTYP_Msk) >> RSTC_SR_RSTTYP_Pos;
    osc_enable(OSC_MAINCK_4M_RC);
    osc_wait_ready(OSC_MAINCK_4M_RC);
    pmc_switch_mck_to_mainck(CONFIG_SYSCLK_PRES);
    sysclk_init();
    ioport_init();

    board_init();
    LOG_DEF_NORMAL("Enable watchdog with %d microseconds period\n\r", (int)wdt_get_us_timeout_period(WDT, BOARD_FREQ_SLCK_XTAL));
    LOG_DEF_NORMAL("%X %lX %lX\r\n", reset_cause, SUPC->SUPC_SR, gpbr_read(1));
    gpbr_write(1, 0);
    ///Verifica di quale banco di flash viene utilizzato per eseguire il sistema.
    if ((uint32_t)init_board < 0xA0000u) {
        SWO_PrintStringDef("Bank 1\r\n");
    } else {
        SWO_PrintStringDef("Bank 2\r\n");
    }
    ///Verifica del microprocessore utilizzato, serve per l'updater.
    if ((((CHIPID->CHIPID_CIDR) >> 8) & 0x0F) == 0x09) {
        iflash0_size = 0x20000u;
    } else {
        iflash0_size = 0x40000u;
    }

    uint32_t ul_rc;
    uint32_t unique_id[4];
    ul_rc = flash_init(FLASH_ACCESS_MODE_128, 4);
    if (ul_rc != FLASH_RC_OK) {
        SWO_PrintStringDef("-F- Initialization error\n\r");
    }
    ul_rc = flash_read_unique_id(unique_id, 4);
    if (ul_rc != FLASH_RC_OK) {
        SWO_PrintStringDef("-F- Read the Unique Identifier error\n\r");
    }

    unique_can_id = unique_id[3];
}

/**
 * Viene inizializzata la periferia della scheda, con tutte le parti hardware necessarie al funzionamento
 * della scheda.
 */
void init_board(void) {
    ///Attivazione della modalità di rilevamento della caduta di tensione.
    supc_set_monitor_sampling_period(SUPC, SUPC_SMMR_SMSMPL_CSM);
    supc_set_monitor_threshold(SUPC, SUPC_SMMR_SMTH_2_7V);
    supc_enable_monitor_reset(SUPC);

    // SWO_PrintStringDef("init_board sleep\r\n");
    board_running = false;
    //Entro in sleep mode.
    init_board_sleep_mode();
    init_board_running();
    SWO_PrintStringDef("init_board running\r\n");
}

void software_reset(void) {
    rstc_start_software_reset(RSTC);
}
