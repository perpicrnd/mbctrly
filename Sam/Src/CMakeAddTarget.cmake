
MACRO(ADD_POST_BUILD_COMMAND target_name)
	ADD_CUSTOM_COMMAND(TARGET ${target_name}
		POST_BUILD
		COMMAND ${ARM_OBJ_COPY} -O ihex -R .eeprom -R .fuse -R .lock -R .signature  ${target_name} ${target_name}.hex
		COMMAND ${ARM_OBJ_COPY} -O binary ${target_name} ${target_name}.bin
		COMMAND ${ARM_OBJ_DUMP} -h -S ${target_name} > ${target_name}.lss
		COMMAND ${ARM_OBJ_COPY} -O srec -R .eeprom -R .fuse -R .lock -R .signature  ${target_name} ${target_name}.srec
		COMMAND ${ARM_OBJ_SIZE} ${target_name}
	)
ENDMACRO()

MACRO(ADD_TARGET target_name car_number hardware_code hardware_release opt)
    SET(CAR_NUMBER ${car_number})
    SET(HARDWARE_CODE ${hardware_code})
    SET(HARDWARE_RELEASE ${hardware_release})
	SET(OPT ${opt})
    if (${OPT} STREQUAL "NO")
	    execute_process(COMMAND python3 ./format_code.py --boardcode MBY --firmwarecode ${CAR_NUMBER} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE car_fmt)
    else()
        execute_process(COMMAND python3 ./format_code.py --boardcode MBY --firmwarecode ${CAR_NUMBER} --firmwaremodificator ${OPT} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE car_fmt)
    endif()
	string(STRIP ${car_fmt} car_fmt)
	set(software_code B${car_fmt})
	set(final_target_name ${car_fmt}_${target_name})
	
	configure_file(version_in.c ${CMAKE_BINARY_DIR}/version_${target_name}.c)
	set(VERLIBSOURCES
		${CMAKE_BINARY_DIR}/version_${target_name}.c
	)
	
	set(SOURCES main.c ${CMAKE_BINARY_DIR}/version_${target_name}.c)
	if(CMAKE_CROSSCOMPILING)
		#Per sam questi file devono essere con il main.
		SET(SOURCES ${SOURCES}
			Libraries/ThirdParty/ASF/sam/utils/syscalls/gcc/syscalls.c
			Libraries/ThirdParty/ASF/sam/utils/cmsis/sam3x/source/templates/gcc/startup_sam3x.c
			Libraries/ThirdParty/ASF/common/utils/stdio/read.c
			Libraries/ThirdParty/ASF/common/utils/stdio/write.c
			main_sam.c
		)
		SET(ARM_OBJ_COPY arm-none-eabi-objcopy )
		SET(ARM_OBJ_DUMP arm-none-eabi-objdump )
		SET(ARM_OBJ_SIZE arm-none-eabi-size )
	else()
		SET(SOURCES ${SOURCES} main_host.c )
	endif()

	#generazione primo eseguibile
	set(final_target_name_8 ${final_target_name}_8)
    add_executable(${final_target_name_8} ${SOURCES})
    foreach(loop_var ${ARGN})
        target_link_libraries(${final_target_name_8} ${loop_var})
    endforeach()
	if (CMAKE_CROSSCOMPILING)
		SET_TARGET_PROPERTIES(${final_target_name_8} PROPERTIES LINK_FLAGS ${CMAKE_EXE_LINKER_FLAGS_8} )
		ADD_POST_BUILD_COMMAND(${final_target_name_8})
	else()
		install(TARGETS ${final_target_name_8} RUNTIME DESTINATION bin)
	endif()
	target_link_libraries(${final_target_name_8} board_version freertos updater canbus eventlist watchdog_lib freertos_cli service_lib serial_task)
	SET(inst_file ${final_target_name_8})
	SET(ADDRESS 0x00080000)
	SET(GPVN2_STATUS "clear")
	SET(TARGET at91sam3ax_4x)
	configure_file(openocd.conf.in openocd_${final_target_name_8}.conf)
	ADD_CUSTOM_TARGET(
		${final_target_name_8}_install
		COMMAND openocd -f openocd_${final_target_name_8}.conf&> ${final_target_name_8}.install
		DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${final_target_name_8}.bin
	)
	
	#generazione secondo eseguibile
	set(final_target_name_A ${final_target_name}_A)
	add_executable(${final_target_name_A} ${SOURCES})
	foreach(loop_var ${ARGN})
        target_link_libraries(${final_target_name_A} ${loop_var})
    endforeach()
	if (CMAKE_CROSSCOMPILING)
		SET_TARGET_PROPERTIES(${final_target_name_A} PROPERTIES LINK_FLAGS ${CMAKE_EXE_LINKER_FLAGS_A} )
		ADD_POST_BUILD_COMMAND(${final_target_name_A})
	else()
		install(TARGETS ${final_target_name_A} RUNTIME DESTINATION bin)
	endif()
	target_link_libraries(${final_target_name_A} board_version freertos updater canbus eventlist watchdog_lib freertos_cli service_lib serial_task)
	SET(inst_file ${final_target_name_A})
	SET(ADDRESS 0x000A0000)
	SET(TARGET at91sam3ax_4x)
	SET(GPVN2_STATUS "set")
	configure_file(openocd.conf.in openocd_${final_target_name_A}.conf)
	ADD_CUSTOM_TARGET(
		${final_target_name_A}_install
		COMMAND openocd -f openocd_${final_target_name_A}.conf&> ${final_target_name_A}.install
		DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${final_target_name_A}.bin
	)
	
	#generazione terzo eseguibile
	set(final_target_name_C ${final_target_name}_C)
	add_executable(${final_target_name_C} ${SOURCES})
	foreach(loop_var ${ARGN})
        target_link_libraries(${final_target_name_C} ${loop_var})
    endforeach()
	if (CMAKE_CROSSCOMPILING)
		SET_TARGET_PROPERTIES(${final_target_name_C} PROPERTIES LINK_FLAGS ${CMAKE_EXE_LINKER_FLAGS_C} )
		ADD_POST_BUILD_COMMAND(${final_target_name_C})
	else()
		install(TARGETS ${final_target_name_C} RUNTIME DESTINATION bin)
	endif()
	target_link_libraries(${final_target_name_C} board_version freertos updater canbus eventlist watchdog_lib freertos_cli service_lib serial_task)
	SET(inst_file ${final_target_name_C})
	SET(ADDRESS 0x000C0000)
	SET(TARGET at91sam3ax_8x)
	SET(GPVN2_STATUS "set")
	configure_file(openocd.conf.in openocd_${final_target_name_C}.conf)
	ADD_CUSTOM_TARGET(
		${final_target_name_C}_install
		COMMAND openocd -f openocd_${final_target_name_C}.conf&> ${final_target_name_C}.install
		DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${final_target_name_C}.bin
	)
	
ENDMACRO()