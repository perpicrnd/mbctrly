#include "version.h"

const char interface_code[] = "${software_code}";

uint8_t firmware_car_number(void) {
    return ${CAR_NUMBER};
}

uint8_t hardware_version() {
    return ${HARDWARE_CODE};
}

uint8_t hardware_release() {
    return ${HARDWARE_RELEASE};
}
