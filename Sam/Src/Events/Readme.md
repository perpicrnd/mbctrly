Descrizione del funzionamento degli eventi
==========================================

Questo modulo non porta del codice con se. Bensì lo genera:
- La descrizione degli eventi
- la definizione dei tipi dei dati
- La definizione delle funzioni necessarie a collegare gli eventi

All'interno del sistema, esiste un modulo (directory) che è responsabile per la definizione dell'evento, altri moduli che, invece, lo utilizzeranno.

Giusto per fare un esempio:

    fornisce un servizio  
    utilizza il servizio

    messaggio can ricevuto  
    B Modulo veicolo che vuole leggere dal can

Partendo da questo semplice esempio bisogna decidere dove verranno inseriti i codici relativi alla ricezione di un messaggio CAN.
In questo caso la scelta è abbastanza semplice. Il modulo CAN è univoco, il veicolo può cambiare. Quindi il codice di ricezione degli eventi can deve essere scritto nel modulo CAN.

I file che bisogna predisporre sono 3.
- canbus.events
- canbus.struct
- canbus.evdef.h

canbus.events
-------------

All'interno del file canbus.events deve essere inserito il nome dell'evento (o degli eventi, possono essere più di uno), che il modulo fornisce.

Esempio:

    eEVENT_CAN0_RX_MESSAGE, //!< EVENT_CAN0_RX_MESSAGE
	eEVENT_CAN1_RX_MESSAGE, //!< EVENT_CAN1_RX_MESSAGE

	eEVENT_CAN0_TX_MESSAGE, //!< EVENT_CAN0_TX_MESSAGE
	eEVENT_CAN1_TX_MESSAGE, //!< EVENT_CAN1_TX_MESSAGE

canbus.struct
-------------

All'interno del file canbus.struct bisogna inserire il nome della struttura che verrà inserita all'interno dell'evento.
Una volta inserito all'interno di tutto il codice sarà possibile accedere ai dati dell'evento utilizzando l'accesso a dato della union EVENT.

    event->as_(module).dato

Esempio:

    struct EVENT_CAN_MESSAGE as_can_message;

canbus.evdef.h
--------------

Il file canbus.evdef.h contiene tutte le strutture dati necessarie per far funzionare il sistema ad eventi.
Per esempio se un evento (Quale il modulo canbus), deve trasferire dei dati da un modulo all'altro (il messaggio can in questo caso) la struttura dati del messaggio can deve essere definita in questo file.
All'interno di questo file verranno inserite anche le funzioni base che permettono la generazione di un evento in modo che altri metodi possano generare in modo autonomo questi eventi.

Sempre prendendo spunto dal codice attuale del canbus questo è il suo contenuto:

    #pragma once
    #include <stdint.h>
    #include "events.h"
    /**
    * @brief Elenco delle interfacce canbus presenti sull'interfaccia.
    */
    typedef enum{
        CAN_INTERFACE_UNKNOWN = (uint8_t)-1,//!< CAN_INTERFACE_UNKNOWN interfaccia sconosciuta
        CAN_INTERFACE_0 = 0,       //!< CAN_INTERFACE_0 interfaccia CAN0
        CAN_INTERFACE_1,           //!< CAN_INTERFACE_1 interfaccia CAN1
        CAN_INTERFACE_COUNT        //!< CAN_INTERFACE_COUNT Numero delle interfacce
    }CAN_INTERFACE;


    /**
    * @brief struttura contenente i dati del messaggio canbus
    * I dati sono salvati in una struttura e si può accedere ai singoli byte.
    */
    typedef struct{
        uint32_t byte0: 8;
        uint32_t byte1: 8;
        uint32_t byte2: 8;
        uint32_t byte3: 8;
        uint32_t byte4: 8;
        uint32_t byte5: 8;
        uint32_t byte6: 8;
        uint32_t byte7: 8;
    }CAN_DATA;

    /**
    * @brief Struttura contenente le informazioni relative ad un messaggio canbus
    * Un messaggio canbus contiene tutte le informazioni necessarie per la sua gestione sia in
    * ricezione che tramissione.
    */
    typedef struct{
        /**
        * Id del messaggio can.
        */
        uint32_t can_id;
        /**
        * Flag che indica se il messaggio è standard o esteso.
        */
        bool can_is_ext;
        /**
        * Dati contenuti nel messaggio can.
        */
        CAN_DATA can_data;
        /**
        * Numero di byte contenuti in \ref CAN_DATA.
        */
        uint8_t datalen;
        /**
        * Interfaccia da cui è stato ricevuto il messaggio o su cui verrà trasmesso.
        */
        CAN_INTERFACE interface;
    }CAN_MESSAGE;

    /**
    * @brief evento di tipo messaggio canbus ricevuto dal veicolo
    */
    struct EVENT_CAN_MESSAGE{
        /**
        * Evento generico.
        */
        struct EVENT_GENERIC generic;
        /**
        * Messaggio canbus ricevuto.
        */
        CAN_MESSAGE msg;
    };


    /**
    * @brief emette un messaggio contenente un messaggio can ricevuto sul can0
    * @param msg il messaggio canbus ricevuto.
    */
    void event_can0_rx_message_emit(const CAN_MESSAGE * const msg);

    /**
    * @brief emette un messaggio contenente un messaggio can ricevuto sul can1
    * @param msg il messaggio canbus ricevuto.
    */
    void event_can1_rx_message_emit(const CAN_MESSAGE * const msg);

    /**
    * @brief emette un messaggio con la richiesta di trasmettere un messaggio sul can0
    * @param msg il messaggio canbus da trasmettere.
    */
    void event_can0_tx_message_emit(const CAN_MESSAGE * const msg);

    /**
    * @brief emette un messaggio con la richiesta di trasmettere un messaggio sul can1
    * @param msg il messaggio canbus da trasmettere.
    */
    void event_can1_tx_message_emit(const CAN_MESSAGE * const msg);


Cose da tenere a mente durante l'utilizzo pratico del modulo eventi:
-------------------------------------------------------------------

- ogni modulo di evento viene copiato e appeso ad un file autogenerato. I file vengono accodati in ordine alfabetico, potrebbe quindi essere necessario agire manualmente sul nome del file nel caso in cui ci siano delle dipendenze tra un modulo ed un altro. Comunicazione con l'attiny board_controller sono effettuate utilizzando il twi, se il modulo board_controller deve generare degli eventi dovrà avere un nome *più grande* rispetto a quello del modulo TWI.
Questo potrà essere fatto semplicemente nominando il file zcontroller.events e via dicendo per gli altri.

- il modulo ad eventi essendo generato da cmake e non durante la fase di compilazione viene rigenerato solo dopo una modifica ad un file CMakeLists.txt: Dopo aver fatto una modifica al file principale, bisogna salvare il file CMakeLists.txt per far si che il nuovo codice appaia nel file generato.
- All'interno di ogni modulo è sufficiente includere il file event_list.h per usufruire di tutte le funzionalità del modulo.