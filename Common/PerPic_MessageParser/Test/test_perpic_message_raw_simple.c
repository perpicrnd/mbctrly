#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "string.h"

#include <cmocka.h>
#include "perpic_message_raw_simple.h"

void vApplicationIdleHook(void) {
    //Make the linker happy.
}

int NumLoops;
const char interface_code[] = "TEST SOFTWARE";

static void CheckReset(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    assert_true(msg->dle_active == false);
    assert_true(msg->is_acknack == false);
    assert_true(msg->crc == PERPIC_CRC_SIMPLE_UNKNOWN);
    assert_true(msg->completed == false);
    assert_true(msg->pos == PERPIC_MESSAGE_EMPTY_VALUE);
    assert_true(msg->data_priv[0] == 0x9F);
    assert_true(msg->data_priv[1] == 0x02);

    for (uint16_t i = 2; i < PERPIC_MESSAGE_LEN; i++) {
        assert_true(msg->data_priv[i] == 0xFF);
    }
}

static inline void test_perpic_message_raw_simple_reset(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);

    CheckReset(&mymsg);
    assert_true(mymsg.start_seen == false);
}

static inline void test_perpic_message_raw_simple_type(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);

    mymsg.data[0] = 8;

    assert_true(perpic_message_raw_simple_type(&mymsg) == 8);
}

static inline void test_perpic_message_raw_simple_is_completed(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);

    assert_true(perpic_message_raw_simple_is_completed(&mymsg) == false);
}

static inline void test_perpic_message_raw_simple_add_uint8(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);
    perpic_message_raw_simple_add_uint8(&mymsg, 10);

    assert_true(mymsg.pos == 0);
    assert_true(mymsg.data[mymsg.pos] == 10);
}

static inline void test_perpic_message_raw_simple_get_id(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);
    assert_true(perpic_message_raw_simple_get_id(&mymsg) == 0xFF);

    perpic_message_raw_simple_add_uint8(&mymsg, 10);
    mymsg.completed = true;

    assert_true(perpic_message_raw_simple_get_id(&mymsg) == 10);
}

static inline void test_perpic_message_raw_simple_add_crc(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);
    perpic_message_raw_simple_add_uint8(&mymsg, 0x0A);
    perpic_message_raw_simple_add_uint8(&mymsg, 0x0B);
    perpic_message_raw_simple_add_crc(&mymsg);

    assert_true(mymsg.pos == 2);

    assert_true(mymsg.data[mymsg.pos] == 0x03);
}

static uint8_t fake_buffer[400];
static uint8_t fake_buffer_pos = 0;
static struct SERIAL *serial_to_check = NULL;
void fake_putstr(struct SERIAL *serial, uint8_t *buff, int len) {
    serial_to_check = serial;
    for (uint8_t i = 0; i < len; i++) {
        fake_buffer[fake_buffer_pos++] = buff[i];
    }
}

static inline void test_perpic_message_raw_simple_crc_check(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    perpic_message_raw_simple_reset(&mymsg);
    assert_true(perpic_message_raw_simple_crc_check(&mymsg) == PERPIC_CRC_SIMPLE_UNKNOWN);

    perpic_message_raw_simple_add_uint8(&mymsg, 0x0A);
    perpic_message_raw_simple_add_crc(&mymsg);
    mymsg.completed = true;

    assert_true(perpic_message_raw_simple_crc_check(&mymsg) == PERPIC_CRC_SIMPLE_OK);

    mymsg.data[1] = 0;

    assert_true(perpic_message_raw_simple_crc_check(&mymsg) == PERPIC_CRC_SIMPLE_KO);
}

static inline void test_perpic_message_raw_simple_add_uint8_from_extern(void **state) {
    (void)state;
    struct PERPIC_RAW_MESSAGE_SIMPLE mymsg;

    //Not giving start of frame
    perpic_message_raw_simple_reset(&mymsg);
    perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x01);
    assert_true(mymsg.pos == 0);
    assert_true(mymsg.data[mymsg.pos] == 1);
    assert_true(mymsg.dle_active == false);

    perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
    assert_true(mymsg.pos == 0);
    assert_true(mymsg.data[mymsg.pos] == 1);
    assert_true(mymsg.dle_active == true);

    for (uint16_t i = 0; i < 2; i++) {
        //Receiving dummy data
        for (uint16_t i = 0; i <= PERPIC_MESSAGE_LEN; i++) {
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x01);
        }
        assert_true(mymsg.pos == (PERPIC_MESSAGE_LEN - 2));

        //Receiving a good message
        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
        assert_true(mymsg.pos == (PERPIC_MESSAGE_LEN - 2));
        assert_true(mymsg.dle_active == true);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x02);
        assert_true(mymsg.dle_active == false);

        CheckReset(&mymsg);
        assert_true(mymsg.start_seen == true);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x02);
        assert_true(mymsg.dle_active == false);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x05);
        assert_true(mymsg.pos == 0);
        assert_true(mymsg.data[mymsg.pos] == 0x05);
        assert_true(mymsg.dle_active == false);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x03);
        assert_true(mymsg.pos == 1);
        assert_true(mymsg.data[mymsg.pos] == 0x03);
        assert_true(mymsg.dle_active == false);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
        assert_true(mymsg.pos == 1);
        assert_true(mymsg.data[mymsg.pos] == 0x03);
        assert_true(mymsg.dle_active == true);

        perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
        assert_true(mymsg.pos == 2);
        assert_true(mymsg.data[mymsg.pos] == 0x9F);
        assert_true(mymsg.dle_active == false);

        if (i == 0) {
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x99);  //correct crc
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x03);

            assert_true(mymsg.completed == true);
            assert_true(mymsg.dle_active == false);
        } else {
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x98);  //wrong crc
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
            perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x03);
            assert_true(perpic_message_raw_simple_crc_check(&mymsg) == PERPIC_CRC_SIMPLE_KO);
            perpic_message_raw_simple_reset(&mymsg);
            CheckReset(&mymsg);
        }
    }

    perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x9F);
    assert_true(mymsg.pos == PERPIC_MESSAGE_EMPTY_VALUE);
    assert_true(mymsg.dle_active == true);

    perpic_message_raw_simple_add_uint8_from_extern(&mymsg, 0x07);
    assert_true(mymsg.dle_active == false);

    CheckReset(&mymsg);
}

int main(void) {
    //Make coverage happy
    vApplicationIdleHook();
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_perpic_message_raw_simple_reset),
        cmocka_unit_test(test_perpic_message_raw_simple_type),
        cmocka_unit_test(test_perpic_message_raw_simple_is_completed),
        cmocka_unit_test(test_perpic_message_raw_simple_add_uint8),
        cmocka_unit_test(test_perpic_message_raw_simple_get_id),
        cmocka_unit_test(test_perpic_message_raw_simple_add_crc),
        cmocka_unit_test(test_perpic_message_raw_simple_crc_check),
        cmocka_unit_test(test_perpic_message_raw_simple_add_uint8_from_extern),

    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
