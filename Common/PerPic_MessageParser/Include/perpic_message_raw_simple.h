#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "logger.h"
#include "serials.h"

#define PERPIC_MESSAGE_LEN 64
#define PERPIC_MESSAGE_EMPTY_VALUE 0xFF

typedef enum {
    PERPIC_CRC_SIMPLE_UNKNOWN,  //!< CRC_UNKNOWN
    PERPIC_CRC_SIMPLE_OK,       //!< CRC_OK
    PERPIC_CRC_SIMPLE_KO,       //!< CRC_KO
} PERPIC_CRC_SIMPLE_CHECK;

/**
 * Struttura base relativa ad un messaggio trasmetto/ricevuto
 */
struct PERPIC_RAW_MESSAGE_SIMPLE {
    uint8_t data_priv[PERPIC_MESSAGE_LEN];  //!<buffer di ricezione/trasmissione
    uint8_t *data;
    uint8_t pos;  //!<posizione attuale all'interno del buffer / lunghezza complessiva del messaggio
    uint8_t len;
    bool dle_active;  //!<i dati che vengono ricevuti subiscono un byte stuffing, data[] contiene i dati senza byte stuffing, durante al ricezione viene normalizzato
    bool is_acknack;  //!<flag che indica se il messaggio ricevuto è di tipo ack/nack o messaggio normale
    bool completed;   //!<flag che indica che il messaggio è completo. Non possono essere aggiunti altri caratteri.
    bool start_seen;
    bool ack;
    bool nack;
    PERPIC_CRC_SIMPLE_CHECK crc;  //!<crc calcolato sul messaggio
};

/**
 * Reset the message to the initial status
 * @param msg the message to reset.
 */
static inline void perpic_message_raw_simple_reset(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    msg->pos = 0;
    msg->dle_active = false;
    msg->data = &msg->data_priv[2];
    for (uint16_t i = 2; i < PERPIC_MESSAGE_LEN; i++) {
        msg->data_priv[i] = 0xFF;
    }
    msg->start_seen = false;
    msg->data_priv[0] = 0x9F;
    msg->data_priv[1] = 0x02;
    msg->is_acknack = false;
    msg->crc = PERPIC_CRC_SIMPLE_UNKNOWN;
    msg->completed = false;
    msg->ack = false;
    msg->nack = false;
    msg->pos = PERPIC_MESSAGE_EMPTY_VALUE;
    msg->len = 0;
}

static inline uint8_t perpic_message_raw_simple_type(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    return msg->data[0];
}

static inline bool perpic_message_raw_simple_is_completed(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    return msg->completed;
}

/**
 * Add a character to the message
 * @param msg the message  where to add the character
 * @param c the char to add
 */
static inline void perpic_message_raw_simple_add_uint8(struct PERPIC_RAW_MESSAGE_SIMPLE *msg, unsigned char c) {
    if ((msg->pos < (PERPIC_MESSAGE_LEN - 3)) || (msg->pos == PERPIC_MESSAGE_EMPTY_VALUE)) {
        ++msg->pos;
        msg->data[msg->pos] = c;
        msg->len++;
    }
}

static inline void perpic_message_raw_simple_add_uint16(struct PERPIC_RAW_MESSAGE_SIMPLE *msg, uint16_t c) {
    perpic_message_raw_simple_add_uint8(msg, c);
    perpic_message_raw_simple_add_uint8(msg, c >> 8);
}

static inline void perpic_message_raw_simple_add_uint32(struct PERPIC_RAW_MESSAGE_SIMPLE *msg, uint32_t c) {
    perpic_message_raw_simple_add_uint8(msg, c);
    perpic_message_raw_simple_add_uint8(msg, c >> 8);
    perpic_message_raw_simple_add_uint8(msg, c >> 16);
    perpic_message_raw_simple_add_uint8(msg, c >> 24);
}

static inline uint8_t perpic_message_raw_simple_get_id(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    if (perpic_message_raw_simple_is_completed(msg)) {
        return msg->data[0];
    } else {
        return 0xFF;
    }
}

static inline void perpic_message_raw_simple_add_crc(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    msg->data[msg->pos + 1] = msg->len;
    for (uint8_t i = 0; i <= msg->pos; i++) {
        msg->data[msg->pos + 1] ^= msg->data[i];
    }
    msg->pos++;
}

static inline bool perpic_message_raw_simple_is_ack(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    return msg->ack;
}

static inline bool perpic_message_raw_simple_is_nack(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    return msg->nack;
}

static inline bool perpic_message_raw_simple_is_acknack(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    return msg->is_acknack;
}

static inline void perpic_message_raw_simple_print(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    uint8_t pos = msg->len;
    if (pos == 0xFF) {
        pos = 0;
    }
    LOG_DEF_NORMAL("len %d comp %d:", pos, msg->completed);
    for (uint8_t i = 0; i < pos + 4; i++) {
        LOG_DEF_NORMAL("%02X ", msg->data_priv[i]);
    }
    LOG_DEF_NORMAL("\r\n");
}

static inline PERPIC_CRC_SIMPLE_CHECK perpic_message_raw_simple_crc_check(struct PERPIC_RAW_MESSAGE_SIMPLE *msg) {
    PERPIC_CRC_SIMPLE_CHECK retVal = PERPIC_CRC_SIMPLE_UNKNOWN;
    uint8_t checksum = 0x00;
    if (msg->completed) {
        checksum ^= msg->len;
        for (uint16_t i = 0; i <= msg->pos; i++) {
            checksum ^= msg->data[i];
            if (i >= PERPIC_MESSAGE_LEN) {
                break;
            }
        }
        if (msg->pos != msg->len) {
            retVal = PERPIC_CRC_SIMPLE_KO;
        } else if (checksum == 0) {
            retVal = PERPIC_CRC_SIMPLE_OK;
        } else {
            retVal = PERPIC_CRC_SIMPLE_KO;
        }
    }
    return retVal;
}

/**
 * Function that check for byte stuffing, start/stop and id.
 * This is an extended version of the alpark4_message_raw_add that is intended for parse receiving messages.
 * @param msg the message where to add the char.
 * @param c the char to add.
 */
static inline void perpic_message_raw_simple_add_uint8_from_extern(struct PERPIC_RAW_MESSAGE_SIMPLE *msg, unsigned char c) {
    do {
        if (msg->dle_active) {
            msg->dle_active = false;
            if (c == 0x02) {
                //Inizio del frame, non può essere a metà frame, è un errore.
                perpic_message_raw_simple_reset(msg);
                msg->start_seen = true;
                break;
            } else if (c == 0x03) {
                if (!msg->start_seen) {
                    break;
                }
                msg->completed = true;
                break;
            } else if (c == 0x06) {
                msg->ack = true;
                msg->is_acknack = true;
                break;
            } else if (c == 0x15) {
                msg->nack = true;
                msg->is_acknack = true;
                break;
            } else if (c == 0x9F) {
            } else {
                perpic_message_raw_simple_reset(msg);
                break;
            }
        } else if (c == 0x9F) {
            msg->dle_active = true;
            break;
        }
        if ((msg->pos > PERPIC_MESSAGE_LEN - 3) && (msg->pos != PERPIC_MESSAGE_EMPTY_VALUE)) {
            break;
        }
        if ((msg->start_seen == true) && (msg->len == 0)) {
            msg->len = c;
        } else {
            msg->data[++msg->pos] = c;
        }
    } while (0);
}
