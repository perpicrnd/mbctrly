SET(CMAKE_SYSTEM_NAME Generic)

#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_SYSTEM_PROCESSOR avr)
# where is the target environment 
#SET(CMAKE_FIND_ROOT_PATH /usr/lib/gcc/avr )


SET(CMAKE_C_COMPILER avr-gcc)
SET(CMAKE_CXX_COMPILER avr-g++)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

SET(CMCU "-mmcu=atxmega32e5")
SET(CSTANDARD "-std=gnu99")
#SET(CDEBUG "-gstabs -g")
SET(CWARN "-Wall -Wstrict-prototypes -Winvalid-pch -Wno-long-long")
SET(CTUNING "-mcall-prologues  -fdata-sections -ffunction-sections -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Wl,--gc-sections -Wl,--relax")
SET(ASM_OPTIONS "-x assembler-with-cpp")
SET(COPT "-Os")
SET(CDEFS "-DF_CPU=32000000")

SET(CFLAGS "${CMCU} ${CTUNING} ${CDEFS} ${CINCS} ${COPT} ${CWARN} ${CSTANDARD} ${CEXTRA}" )

SET(CMAKE_C_FLAGS  ${CFLAGS} CACHE STRING "" FORCE)
SET(CMAKE_ASM_FLAGS "${CFLAGS} ${ASM_OPTIONS}" CACHE STRING "" FORCE)

add_definitions(-D__AVR_ATxmega32E5__=1)
add_definitions(-DUSE_TC45_TIMER=1)
add_definitions(-DPORT_INTLVL=1)


