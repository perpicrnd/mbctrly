#!/bin/bash
cd "$(dirname "$0")"
CUR_DIR=`pwd`
echo "Working in ${CUR_DIR}"

PROJECT="MBCTRLY_XMEGA_host"

CMAKE=cmake
MAKEFILE="Eclipse CDT4 - Ninja"
ECLIPSE_OPTION="-DCMAKE_ECLIPSE_GENERATE_SOURCE_PROJECT=TRUE"
SOURCE_DIR=${CUR_DIR}/../Src/

CUR_DIR=`pwd`
DIRECTORY=${PROJECT}

if [ ! -d "$DIRECTORY" ]; then
    echo "Directory ${DIRECTORY} not present... creating it."
    mkdir -p ${DIRECTORY}
    cd ${DIRECTORY}

    cmake -G"${MAKEFILE}" -DCMAKE_BUILD_TYPE=Debug ${ECLIPSE_OPTION} ${CUR_DIR}/../Src/
    rc=$?
    if [[ $rc != 0 ]] ; then
        echo "Error during cmake"
        exit $rc
    fi
    cd ${CUR_DIR}
else
    echo "Directory ${DIRECTORY} already present."
fi

cd ${DIRECTORY}
ninja
rc=$?
if [[ $rc != 0 ]] ; then
    echo "Error during make project."
    exit $rc
fi
ninja coverage
rc=$?
if [[ $rc != 0 ]] ; then
    echo "Error during tests"
    exit $rc
fi

