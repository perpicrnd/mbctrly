#!/bin/bash
cd "$(dirname "$0")"
CUR_DIR=`pwd`
echo "Working in ${CUR_DIR}"

PROJECT="MBCTRLY_XMEGA"

CMAKE=cmake
MAKEFILE="Eclipse CDT4 - Ninja"
ECLIPSE_OPTION="-DCMAKE_ECLIPSE_GENERATE_SOURCE_PROJECT=TRUE"
TOOLCHAIN="-DCMAKE_TOOLCHAIN_FILE=${CUR_DIR}/../Toolchain-avr.gcc.cmake"
SOURCE_DIR=${CUR_DIR}/../Src

CUR_DIR=`pwd`
DIRECTORY=${PROJECT}
cmake --version
if [ ! -d "$DIRECTORY" ]; then
    echo "Directory ${DIRECTORY} not present... creating it."
    mkdir -p ${DIRECTORY}
    cd ${DIRECTORY}

    cmake -G"${MAKEFILE}" ${ECLIPSE_OPTION} ${TOOLCHAIN} ${SOURCE_DIR}
    rc=$?
    if [[ $rc != 0 ]] ; then
        echo "Error during cmake ${PROJECT} car: ${CAR} radio: ${RADIO}"
        exit $rc
    fi
    cd ${CUR_DIR}
else
    echo "Directory ${DIRECTORY} already present."
fi

cd ${DIRECTORY}
ninja
rc=$?
if [[ $rc != 0 ]] ; then
    echo "Error during make project: ${PROJECT} car: ${CAR} radio: ${RADIO}"
    exit $rc
fi

