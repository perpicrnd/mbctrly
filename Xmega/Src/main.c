#ifndef __amd64__
#include "asf.h"
#endif
#include "watchdog.h"
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "event_list.h"
#include "logger.h"
#include "watchdog.h"
#include "adc_backend.h"
#include "pwm_in.h"
#include "pwm_out.h"
#include "eeprom.h"
#include "serial_parser.h"
#include "alive_controller.h"
#include "ir.h"
#include "updater_parser.h"

uint32_t idle_counter = 0;
uint32_t counter = 0;
extern uint8_t isr_counter;
extern uint8_t reset_status;

static void secondly_operation(const union EVENT * const event){
    if (event->as_generic.event == eEVENT_SECONDS_CHANGED){
        idle_counter = 0;
        counter++;
        transmit_active_led_setting();
    }
}
bool system_running = false;

int main(void){
    for(uint8_t i=0;i<0xFE;i++){
        asm("nop");
    }
    init_board();
    watchdog_init();
    watchdog_reset();
    logger_init();
    event_init();
    event_connect_callback(eEVENT_SECONDS_CHANGED, secondly_operation);
#if defined(PRINT_RAM_USAGE)
    event_connect_callback(eEVENT_SECONDS_CHANGED, timer_ram_usage);
#endif
    serial_parser_init();
    adc_backend_init();
    eeprom_init();
    check_default_button_configuration();
    pwm_in_init();
    pwm_out_init();
#ifndef __amd64__
    set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
#endif
    ir_init();
    alive_controller_init();
    LOG_DEF_NORMAL_P(PSTR("run %02X\b\r\n"), reset_status);
    system_running = true;
    vTaskStartScheduler();
}


// uint16_t counter = 0;
void vApplicationIdleHook( void )
{
    idle_counter++;
#ifndef __amd64__
    asm("nop");
    sleep_cpu();
#endif
}