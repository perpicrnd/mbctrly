#pragma once

#include "event_list.h"

void init_board(void);
void request_sleep_mode(void);
void timer_ram_usage(const union EVENT * const event);
void check_default_button_configuration(void);