#!/usr/bin/env python

'''
    Passando come parametri in ingresso al programma i dati relativi ad un firmware viene
    generato il nome dell'eseguibile stampandolo su stdout.
    
    esempio:
    ./format_code.py --boardcode AVC --firmwarecode 15
    ./format_code.py --boardcode AVC --firmwarecode 15 --year 2016 --month 1 --day 1
    ./format_code.py --boardcode AVC --firmwarecode 15 --year 2016 --month 2 --day 29
    
'''


import os
import sys
import datetime
import argparse


def format_year(time):
    base = time.year
    base -= 2010
    if base >= 8:
        base += 1
    return chr(ord('A')+base)


def format_month(time):
    base = time.month
    retVal = 0

    if base < 10:
        return chr(ord('0')+base)
    elif base == 10:
        return 'A'
    elif base == 11:
        return 'B'
    elif base == 12:
        return 'C'
    else:
        return '0'


def format_day(time):
    base = time.day
    if base < 10:
        return chr(ord('0')+base)
    elif base < 18:
        return chr(ord('A')+base-10)
    elif base < 23:
        return chr(ord('B')+base-10)
    elif base <= 31:
        return chr(ord('C')+base-10)
    else:
        return 0


def format_string(t, n, date, par=None, ):
    year = format_year(date)
    month = format_month(date)
    day = format_day(date)
    st = "%c%c%c-%s-%02X" % (year, month, day, t, int(n))
    if par is not None:
        st += par
    print(st),


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        add_help=True, description='Generate a firmware code with the given parameters.')
    parser.add_argument('--boardcode',  required=True,
                        action='store', help='The board code')
    parser.add_argument('--firmwarecode',  type=int, required=True,
                        action='store', help="The progressive firmware number")
    parser.add_argument('--firmwaremodificator',
                        action='store', help="The firmware modificator")
    group = parser.add_argument_group()
    group.add_argument('--year', action='store',
                       type=int, help="The year to use.")
    group.add_argument('--month', action='store',
                       type=int, help="The month to use.")
    group.add_argument('--day', action='store',
                       type=int, help="The day to use.")
    args = parser.parse_args()
    date = None
    if (args.year is not None):
        date = datetime.date(args.year, args.month, args.day)
    else:
        date = datetime.datetime.now()
    format_string(args.boardcode, args.firmwarecode,
                  date, args.firmwaremodificator)

