#include <stdint.h>
#ifdef __amd64__
const uint8_t car_name = ${car_name};
const uint8_t can_name_inv = ${car_name} ^ 0xFF;
const char interface_code[] = "B{software_code}";
#else
#include <avr/pgmspace.h>

const uint8_t car_name  __attribute__ ((section (".andrea"))) = ${car_name};
const uint8_t car_name_inv __attribute__ ((section (".andrea"))) = (${car_name} ^ 0xFF);

const char interface_code[] PROGMEM = "B${software_code}";
#endif