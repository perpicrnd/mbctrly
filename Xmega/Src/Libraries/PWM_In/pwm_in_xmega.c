#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "asf.h"
#include "pwm_in.h"
#include "event_list.h"
#include "logger.h"



/**
 * @brief Ultimo valore trasmesso sul pwm.
 * 
 */
 
volatile uint8_t last_pwm_pct;

extern volatile bool overflow_off;
extern volatile bool overflow_on;

ISR(TCD5_OVF_vect){
    if ((PORTD.IN & PIN2_bm) != PIN2_bm){
        overflow_off = false;
        overflow_on = true;
    }else{
        overflow_off = true;
        overflow_on = false;
    }
    tc45_clear_overflow(&TCD5);
}



void pwm_in_init(void){
    ioport_set_pin_mode(Pwm_In, IOPORT_DIR_INPUT | IOPORT_MODE_PULLUP | IOPORT_SENSE_BOTHEDGES);
    PORTD.PIN2CTRL &= ~PORT_ISC_INPUT_DISABLE_gc;
    PORTD.PIN2CTRL |= PORT_ISC_BOTHEDGES_gc;
	tc45_enable(&TCD5);
	tc45_write_period(&TCD5, 0xffff);
	tc45_set_resolution(&TCD5, 1000000);
    tc45_set_overflow_interrupt_level(&TCD5, TC45_INT_LVL_LO);

	PORTD.INTMASK = PIN2_bm;
	PORTD.INTCTRL = PORT_INTLVL_LO_gc;
	cpu_irq_enable();   
}