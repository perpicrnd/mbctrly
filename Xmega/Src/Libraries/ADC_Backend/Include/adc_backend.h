#pragma once

#include <stdbool.h>
#include <stdint.h>

void adc_backend_init(void);
void adc_init_sleep_mode(void);
void adc_start_convertion(void);
bool adc_xmega_rot_val(uint8_t num);