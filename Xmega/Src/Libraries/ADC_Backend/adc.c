#include <string.h>
#include "event_list.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "adc_backend.h"
#include "logger.h"
#include "eeprom.h"

#define TIMER_TICK 10
#define OLD_READS 10
#define COUNTER_TICK 2000 / TIMER_TICK
#define COUNTER_TICK_INCREASE 5

static TimerHandle_t timer;

#define MVOLT_MAGIC_NUMBER 0x949
#define PROTECTION_RES 1500

void check_default_button_configuration(void);

volatile uint16_t adc_res[8] = {0};

static inline uint32_t calc_res(uint16_t adc_val, uint16_t vref){
    uint32_t retVal = -1;
    
    retVal = (uint32_t)adc_val * vref * 2 / MVOLT_MAGIC_NUMBER;
    if (retVal >= vref){
        retVal = vref - 1;
    }
    retVal = ((uint32_t) retVal * 24000) / (vref - (uint32_t)retVal);
    if (retVal < PROTECTION_RES){
        retVal = 0;
    }else{
        retVal -= PROTECTION_RES;
    }
    return retVal;
}

static uint32_t cur_vref;

#define MAX_VAL 10000
#define RANGE_1 5000
#define RANGE_2 3000
#define RANGE_3 300

#define STARTUP_RAMP_VAL 0x3FF

static uint8_t delayer = 0;
static uint8_t repeater = 0;
static uint8_t old_reads = 0;
static uint8_t old_reads_count = 0;
static uint16_t startup_ramp = 0;
volatile bool rot_x_changed; //Viene cambiato dall'interrupt

#define BTN_MUTE 0x40
#define BTN_CAM 0x30
#define BTN_MUSIC 0x20
#define BTN_MIC 0x10

#define BTN_EXTRA 0x04
#define BTN_MENU 0x03
#define BTN_NAVI 0x02
#define BTN_TEL 0x01

#define BTN_SEEK_UP 0x80
#define BTN_SEEK_DOWN 0x08

static uint8_t convert_raw_to_mapped_button_vr(uint8_t val){
    //Rotori non sono da spostare, gli altri bit sono specchiati nei due nibble.
    //I rotori nel nibble sono su 8
    uint8_t tmp = (val >> 4)&0x07;
    tmp |= (val << 4) & 0x70;
    tmp |= (val & 0x88);
    return tmp;
}

static uint8_t convert_raw_to_mapped_button_hl(uint8_t val){
    //Rotori non sono da spostare
    uint8_t retVal = 0;
    uint8_t tmp = (val & 0x07);
    switch(tmp){
        case 0x01:
        retVal |= BTN_MIC;
        break;
        case 0x02:
        retVal |= BTN_CAM;
        break;
        case 0x03:
        retVal |= BTN_MUSIC;
        break;
        case 0x04:
        retVal |= BTN_MUTE;
        break;
    }
    tmp = (val >> 4)&0x07;
    switch(tmp){
        case 0x01:
        retVal |= BTN_TEL;
        break;
        case 0x02:
        retVal |= BTN_MENU;
        break;
        case 0x03:
        retVal |= BTN_NAVI;
        break;
        case 0x04:
        retVal |= BTN_EXTRA;
        break;
    }
    retVal |= (val & 0x88);
    return retVal;
}

static uint8_t convert_raw_to_mapped_button_hr(uint8_t val){
        uint8_t retVal = 0;
    uint8_t tmp = (val >> 4)&0x07;
    switch(tmp){
        case 0x01:
        retVal |= BTN_MIC;
        break;
        case 0x02:
        retVal |= BTN_CAM;
        break;
        case 0x03:
        retVal |= BTN_MUSIC;
        break;
        case 0x04:
        retVal |= BTN_MUTE;
        break;
    }
    tmp = (val & 0x07);
    switch(tmp){
        case 0x01:
        retVal |= BTN_TEL;
        break;
        case 0x02:
        retVal |= BTN_MENU;
        break;
        case 0x03:
        retVal |= BTN_NAVI;
        break;
        case 0x04:
        retVal |= BTN_EXTRA;
        break;
    }
    retVal |= (val & 0x88);
    return retVal;
}



static uint8_t convert_raw_to_mapped_button(uint8_t val){
    uint8_t result = 0;
    switch(eeprom_safe_read_byte_cached(BTN_CONFIGURATION)){
        case BUTTON_CONFIGURATION_VERTICAL_LEFT: 
        result = val; //Stessa configurazione
        break;
        case BUTTON_CONFIGURATION_VERTICAL_RIGHT: 
        result = convert_raw_to_mapped_button_vr(val);
        break;
        case BUTTON_CONFIGURATION_HORIZONTAL_LEFT: 
        result = convert_raw_to_mapped_button_hl(val);
        break;
        case BUTTON_CONFIGURATION_HORIZONTAL_RIGHT: 
        result = convert_raw_to_mapped_button_hr(val);
        break;
        default:
        result = val;
        check_default_button_configuration();
    }
    return result;
}

static uint8_t range_detector(uint32_t value){
    uint8_t retVal = 0x00;
    if (value < RANGE_3){
        retVal = 0x01;
    }else if(value < RANGE_2){
        retVal = 0x02;
    }else if (value < RANGE_1){
        retVal = 0x03;          
    }else if (value < MAX_VAL){
        retVal = 0x04;
    }
    return retVal;
}

void timer_swc_res_conversions(TimerHandle_t timer){
    (void)timer;
    uint8_t channel_convertions[3];
    {
        struct SWC_ANALOG_CONVERSION event;

        cur_vref = (1100.0 * MVOLT_MAGIC_NUMBER / adc_res[5] / 2);
        event.channel_a = calc_res(adc_res[1], cur_vref);
        event.channel_b = calc_res(adc_res[2], cur_vref);
        event.channel_c = calc_res(adc_res[3], cur_vref);

        
        channel_convertions[0] = range_detector(event.channel_b);
        channel_convertions[1] = range_detector(event.channel_b);
        if (adc_xmega_rot_val(1)){
            channel_convertions[1] |= 0x08;    
        }
        channel_convertions[2] = range_detector(event.channel_c);
        if (adc_xmega_rot_val(0)){
            channel_convertions[2] |= 0x08;    
        }
        if (startup_ramp < STARTUP_RAMP_VAL){
            startup_ramp++;
        }
        if ((channel_convertions[2] != 0)||(channel_convertions[1] != 0)){
            startup_ramp = STARTUP_RAMP_VAL;
        }
    }
    do{
        if (startup_ramp < STARTUP_RAMP_VAL){
            break;
        }
        uint8_t tmp = channel_convertions[1] | (channel_convertions[2]<<4);

        if ((tmp != old_reads)||(rot_x_changed)){
            old_reads = tmp;
            old_reads_count = 0;
            delayer = 0;
            repeater = 0;
            rot_x_changed = false;
            //Valore differente, di sicuro non trasmetto nulla.
            if (tmp){
                break;//nel caso di no button pressed (tmp = 0) non eseguire il filtro (prosegui quindi dai il comando)
            }
        }else{
            if (old_reads_count < OLD_READS){
                old_reads_count++;
                //Non trasmetto, aspetto di avere una lettura costante.
                if (tmp){
                    break;//nel caso di no button pressed (tmp = 0) non eseguire il filtro (prosegui quindi dai il comando)
                }
            }
        }
        
        delayer++;
        if (delayer >= repeater){
            delayer = 0;
            if (repeater < COUNTER_TICK){
                repeater+=COUNTER_TICK_INCREASE;
            }
            struct CONTROLLER_DATA data;
            data.command = CONTROLLER_RO_BUTTONS;
            data.data[0] = convert_raw_to_mapped_button(tmp);
            event_controller_emit(&data);
        }
    }while(0);
    adc_start_convertion();
}

void adc_backend_init(void){
    adc_init_sleep_mode();
    timer = xTimerCreate("swc_tim", TIMER_TICK/portTICK_PERIOD_MS, pdTRUE, NULL, timer_swc_res_conversions);
    xTimerStart(timer, TIMER_TICK);
}
