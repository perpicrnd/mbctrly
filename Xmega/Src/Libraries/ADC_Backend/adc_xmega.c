 #include <asf.h>
#include <string.h>
#include "logger.h"




#define MY_ADC    ADCA		//ADC utilizzata
#define MY_ADC_CH ADC_CH0	//Canale adc utilizzato

bool adc_xmega_rot_val(uint8_t num){
    switch(num){
        case 0:
        return !ioport_get_pin_level(ROT_X_A);
        break;
        case 1:
        return !ioport_get_pin_level(ROT_X_B);
        break;
    }
    return false;
}

void adc_init_sleep_mode(void){
    ADCA.CAL = adc_get_calibration_data(ADC_CAL_ADCA);

	ioport_set_pin_dir(AREF0_PIN, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(AREF0_PIN, IOPORT_MODE_WIREDOR);
	
    ioport_set_pin_dir(AREF_NEG, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(AREF_NEG, IOPORT_MODE_WIREDOR);

    ioport_set_pin_dir(An_In_1, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(An_In_1, IOPORT_MODE_WIREDOR);

    ioport_set_pin_dir(An_In_2, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(An_In_2, IOPORT_MODE_WIREDOR);

    ioport_set_pin_dir(An_In_3, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(An_In_3, IOPORT_MODE_WIREDOR);

    ioport_set_pin_dir(ROT_X_A, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(ROT_X_A, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(ROT_X_B, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(ROT_X_B, IOPORT_MODE_PULLUP);

    struct adc_config adc_conf;
	adc_read_configuration(&MY_ADC, &adc_conf);
	
	adc_enable_internal_input(&adc_conf, ADC_INT_BANDGAP);
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_AREFA);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_FREERUN, 1, 0);
	adc_set_clock_rate(&adc_conf, 30000);
    adc_write_configuration(&MY_ADC, &adc_conf);
    
    struct adc_channel_config adcch_conf;
    adcch_read_configuration(&MY_ADC, ADC_CH0, &adcch_conf);
	adcch_enable_interrupt(&adcch_conf);
    adcch_set_interrupt_mode(&adcch_conf, ADCCH_MODE_COMPLETE);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN2, ADCCH_NEG_PIN1, 0);
    adcch_set_pin_scan(&adcch_conf, 0, 2);
	adcch_enable_averaging(&adcch_conf, ADC_SAMPNUM_16X);
	adcch_write_configuration(&MY_ADC, ADC_CH0, &adcch_conf);

    adc_enable(&MY_ADC);
}

void adc_start_convertion(void){
    adc_enable(&ADCA);
}