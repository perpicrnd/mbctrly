#pragma once

#include <stdint.h>

struct SWC_ANALOG_CONVERSION{
    uint32_t channel_a;
    uint32_t channel_b;
    uint32_t channel_c;
    uint32_t bandgap;
};

struct EVENT_ANALOG_VALUE{
	/**
	 * Evento generico.
	 */
	struct EVENT_GENERIC generic;
	/**
	 * Messaggio da trasmettere.
	 */
	struct SWC_ANALOG_CONVERSION swc_analog;
};

void event_analog_value_emit(const struct SWC_ANALOG_CONVERSION * const ev);



