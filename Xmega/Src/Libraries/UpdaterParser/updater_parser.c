#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "timers.h"

#include "logger.h"
#include "serials.h"
#include "eeprom.h"
#include "watchdog.h"

#include "updater_parser.h"
#include "updater_priv.h"

#ifdef __amd64__
#define sprintf_P sprintf
#define pgm_read_byte(x) (x)
#endif

extern const uint8_t car_name  __attribute__ ((section (".andrea")));
extern const char interface_code[];
#define BUFF_RX_SIZE 32	//Dimensione buffer di ricezione da PC
#define MASK_PAGE_SIZE 0xFF
#define MAX_BYTES_RX 30	//Dimesione del comando ricevuto (in hex)
#define MAX_BYTES_TX 45	//Dimesione dei dati (in ASCII) da inviare al PC


volatile uint16_t pos = 0;
volatile char recvBuffer[BUFF_RX_SIZE];			//Buffer di ricezione da PC

static uint16_t numBytesRX;			//Num byte utili del comando
static char strHex[6]; 				//Substringa estratta (da convertire)
static char **endPtr;
static uint8_t bytesRX[MAX_BYTES_RX];//Record (in hex) ricevuto dal PC (senza STX - ETX - LF)
static char strTX[MAX_BYTES_TX];		//Recod (parte dati) da inviare al PC
static bool go_on = false;

static bool waiting_reboot = false;
extern struct SERIAL uartD;
extern struct SERIAL uartC;
struct SERIAL *updater_uart = &uartC;

void Eval_Param_Blocco (void);
void leggiPaginaFlash(void);


void elaboraComando(void);
//Formato record trasmesso: XON Num D0..Dn Ceck XOFF LF
// XON = 0x02
// Num = 3 bytes (H-M-L) Numero Dati da D0 a Dn
// Check = 2 bytes (H-L) Somma + 1 dei bytes da Num a Dn (somma del byte ASCII e non del valore che rappresenta - esclusi XON XOFF LF e Check)
// XOFF = 0x03
// LF = 0x0A - Inserito per poter utilizzare in ricezione ReadLine()
// Tutti i dati sono in HEX e rappresentati con caratteri ASCII (esluso XON XOFF e LF)
void trasmeRisposta (void){
	uint8_t lenRecordTX;
	char bytesOut[100];
	char locStr[10];
	uint32_t checkSum;
	lenRecordTX = strlen(strTX);
	bytesOut[0] = 0x02;															// STX
	sprintf_P(locStr,PSTR("%03X"),lenRecordTX);
	bytesOut[1] = locStr[0]; bytesOut[2] = locStr[1]; bytesOut[3] = locStr[2];	// Lunghezza dati record
	checkSum = 1 + bytesOut[1] + bytesOut[2] + bytesOut[3];	//Init check
	uint16_t j=4;
	for (uint8_t i = 0; i < lenRecordTX; i++,j++){
		bytesOut[j] = strTX[i];												// Corpo dati
		checkSum += bytesOut[j];
	}
	sprintf_P(locStr,PSTR("%04X"),(uint16_t) (checkSum & 0xFFFF));						// Checksum
	for (uint8_t i = 0; i < 4;i++,j++) {
		bytesOut[j] = locStr[i];
	}
	bytesOut[j++] = 0x03;
	bytesOut[j++] = 0x0A;								// ETX
    uint8_t tmp = (lenRecordTX+10)>>1;
    updater_uart->put_string(updater_uart, (uint8_t *)bytesOut, tmp);
    vTaskDelay(5/portTICK_PERIOD_MS);
    updater_uart->put_string(updater_uart, (uint8_t *)&bytesOut[tmp], (lenRecordTX+10)-tmp);
}

static inline void getSubstringHex(volatile char *orig, char *dest, uint16_t begin,uint8_t Len) {
	uint16_t i;
	for (i = 0; i < Len; i++){
		dest[i] = orig[begin+i];
	}
	dest[i] = 0;
}

static inline void clear_buffer(void){
	recvBuffer[0] = 0;
	go_on = false;
	return;
}


static bool get_uart_char(char *c){
    bool retVal = false;
    if (updater_uart->recv_isr_pos != updater_uart->recv_usr_pos){
		*c = updater_uart->bufferRxUartMessage[updater_uart->recv_usr_pos];
		updater_uart->recv_usr_pos++;
		if (updater_uart->recv_usr_pos >= UART_RX_BUFF_LEN){
			updater_uart->recv_usr_pos = 0;
		}
		retVal = true;
	}
    return retVal;
}

static char c;
static uint16_t hand_pos = 0;
static bool msg_completed = false;

SemaphoreHandle_t recv_semaphore;
void firmware_task(void) {
    while (get_uart_char(&c)){
        recvBuffer[hand_pos] = c;
        if (hand_pos >= BUFF_RX_SIZE){
            hand_pos = 0;
        }
        if (recvBuffer[0] != 0x02){
            hand_pos = 0;
        }
        if (recvBuffer[hand_pos++]==0x0A){
            msg_completed = true;
            pos = hand_pos;
            hand_pos = 0;

        }
    }
    
    if (msg_completed){
        if (recvBuffer[0]==0x02){
            if (recvBuffer[pos-1]==0x0A){
                msg_completed = false;
                //Ho un messaggio completo nel buffer comprensivo di 0x02 iniziale e 0x10 finale.
                uint16_t i, j;
                uint32_t Check_Sum;
                Check_Sum = 1 + recvBuffer[1] + recvBuffer[2] + recvBuffer[3];
                getSubstringHex(recvBuffer, strHex, 1, 3);
                numBytesRX = (uint16_t) strtol(strHex, endPtr, 16);
                if (numBytesRX == pos - 10) {
                    for (j = 0, i = 4; j < numBytesRX; i++, j++) {
                        Check_Sum += recvBuffer[i];
                    }
                    if (recvBuffer[i + 4] == 0x03) {
                        // Verifica il check dei dati ricevuti
                        getSubstringHex(recvBuffer, strHex, i, 4);
                        if ((strtol(strHex, endPtr, 16) & 0xFFFF) == (Check_Sum & 0xFFFF)) {
                            elaboraComando();
                            if (waiting_reboot){
                                vTaskDelay(100/portTICK_PERIOD_MS);
                                eeprom_safe_factory_settings();
                            }
                        }
                    } else {
                        clear_buffer();
                        // Errore -- Non riconosciuto XOFF -- Non inviare risposta
                    }
                } else {
                    clear_buffer();
                    // Errore -- Lunghezza dati errata -- Non inviare risposta
                }
            }
        }
    }
}


uint8_t convSubstring2Hex(volatile char *orig, uint16_t begin);
uint8_t convSubstring2Hex(volatile char *orig, uint16_t begin) {

	uint8_t i;
	char park[3];
	for (i = 0; i < 2; i++) {
		park[i] = orig[begin++];
	}
	park[i] = 0;
	return (uint8_t) (strtol(park, endPtr, 16));
}
typedef union{
	uint32_t as_32;
	uint8_t as_8;
	char as_c[4];
}DWordUnion;

void elaboraComandoNonCriptatoNumerico(uint16_t i, uint16_t j) {
	char loc_str[40];
	for (; (i << 1) < numBytesRX; i++, j += 2) {
		bytesRX[i] = convSubstring2Hex(recvBuffer, j);
	}

	switch (bytesRX[1]) {
	    case 8:
        {
            waiting_reboot = true;
            writeRequeUpdater();
        }
    case 1:						// Richiesta Info
		// Determina il banco da cui si effettua il boot e calcola l'offset per l'indirizzo di accesso alla flash
		{
                
				loc_str[0] = '6';
				loc_str[1] = '0';
				loc_str[2] = ':';
				loc_str[3] = '\0';
				strcat(strTX, loc_str);
				sprintf_P(loc_str, PSTR("%c%c%c:"), pgm_read_byte(&interface_code[1]), pgm_read_byte(&interface_code[2]), pgm_read_byte(&interface_code[3]));
				strcat(strTX, loc_str);
				sprintf_P(loc_str, PSTR("%X:"), pgm_read_byte(&car_name));
				strcat(strTX, loc_str);
				sprintf_P(loc_str,PSTR("%X:"), 10);
				strcat(strTX, loc_str);
				sprintf_P(loc_str,PSTR("%X:"), 1);
				strcat(strTX, loc_str);
				// Estrai dal Chip ID Register l'identifier del micro (SAM3AxC = 0x83 -- SAM3XxC = 0x84)
				// Determina dimensione flash (0x09 = 128K -- 0x0A = 256K)
                // sprintf_P(loc_str, PSTR("%X:%X:%lX:") ,0x96, 32, 1);
                loc_str[0] = '9';
                loc_str[1] = '6';
				loc_str[2] = ':';
                loc_str[3] = '2';
                loc_str[4] = '0';
				loc_str[5] = ':';
                loc_str[6] = '1';
                loc_str[7] = ':';
				loc_str[8] = '\0';
				strcat(strTX, loc_str);
		}
		break;

	default:					// Comando sconosciuto
		strcat(strTX, "01");
		break;
	}

}

void elaboraComando() {
	uint16_t i;
	uint16_t j;
	for (i = 0, j = 4; i < 2; i++, j+=2) {
		bytesRX[i] = convSubstring2Hex(recvBuffer, j);
	}
	getSubstringHex(recvBuffer, strTX, 4, 4);//Inizializza risposta da trasmettere
	switch (bytesRX[0]) {
	case 0x20:				// Comandi non criptati con records solo numerici
		elaboraComandoNonCriptatoNumerico(i, j);
		break;
	default:
		strcat(strTX, "01");		//comando non gestito;
		break;
	}
	trasmeRisposta ();
}


