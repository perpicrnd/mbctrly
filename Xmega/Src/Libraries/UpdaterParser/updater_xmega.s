
#include <assembler.h>

//============================================================
// ---- Scrive nei primi 6 bytes della e2p di pagina 30 ------
// ---- la sequenza 0xBD 0x2C 0xBD 0x2C 0xBD 0x2C       ------
//============================================================ 

// La scrittura della flag di richiesta e relativo check
// avviene effettuando la chiamata:
//	void writeRequeUpdater(void);

// Non viene usata alcuna cella di memoria ma solo registri

// I riferimenti ai registri/bit/indirizzi del micro si trovano nel file ATxmega32e5def.inc
// Quelli che servono sono definiti in questo file. 
// Se il compilatore dovesse dare errore, provare a definirli con .EQU nome = valore

#define NVM_BASE  0x01C0		// Non Volatile Memory Controller
#define NVM_NVMBUSY_bp  7 // Non-volatile Memory Busy bit position
#define NVM_CMD_ERASE_WRITE_EEPROM_PAGE_gc  0x35 ; Erase-and-write EEPROM page
#define CCP_IOREG_gc  0xD8 ; IO Register Protection

PUBLIC_FUNCTION(writeRequeUpdater)
	push	R0					;Salva tutti i registri utilizzati
	push	R1					;per l'esecuzione della funzione
	push	R16
	push	R17
	push	R18
	push	R21
	push	R24
	push	R25
	push	ZH
	push	ZL
	push	XH
	push	XL		
	ldi		R16,30			;Numero pagina in cui si scrive
	ldi		R18,3				;Numero coppi 0xBD 0x2C
	ldi		R17,32			;Calcola l'indirizzo di start della pagina
	mul		R16,R17
	movw	R24,R0
	clr		R21					;Counter coppie scritte
lpx:	
	ldi		R16,0xBD		;Valore da scrivere
	rcall	EEPROM_WriteByte
	adiw	R24, 1
	ldi		R16,0x2C		;Checksum
	rcall	EEPROM_WriteByte
	adiw	R24, 1
	inc		R21
	cp		R21,R18			;Scritte tutte le coppie di valori ?
	brne	lpx					;NO
	pop 	XL
	pop 	XH
	pop 	ZL
	pop 	ZH					;Ripristina il valore dei registri che
	pop		R25					;si aveva prima di effettuare la chiamata
	pop		R24					;a questa funzione
	pop		R21
	pop		R18
	pop		R17
	pop		R16
	pop	  R1
	pop   R0
	ret
END_FUNC(writeRequeUpdater)

PUBLIC_FUNCTION(EEPROM_WriteByte)
	ldi		ZL, 0xC0; low(NVM_BASE)
	ldi		ZH, 0x01; (NVM_BASE)
qbabc:	ldd		R17, Z + NVM_STATUS - NVM_BASE
	sbrc	R17, NVM_NVMBUSY_bp
	rjmp	qbabc
	clr		R1
	std		Z + NVM_ADDR0 - NVM_BASE, R24
	std		Z + NVM_ADDR1 - NVM_BASE, R25
	std		Z + NVM_ADDR2 - NVM_BASE, R1
  movw 	XL, R24
	subi	XL, 0x00; low(-MAPPED_EEPROM_START)
	sbci	XH, 0xF0; high(-MAPPED_EEPROM_START)
	st		X, R16
  ; Issue EEPROM Erase & Write command.
	ldi		R16, NVM_CMD_ERASE_WRITE_EEPROM_PAGE_gc
	std		Z + NVM_CMD - NVM_BASE, R16
	ldi		R16, CCP_IOREG_gc
	ldi		R17, NVM_CMDEX_bm
	out		CPU_CCP, R16
	std		Z + NVM_CTRLA - NVM_BASE, R17
	ret
END_FUNC(EEPROM_WriteByte)
END_FILE()