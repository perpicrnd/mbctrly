#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "stream_buffer.h"

struct SERIAL; 

typedef void (* RX_ISR_ENA)(void);
typedef void (* TX_ISR_ENA)(void);
typedef int (* TX_CHAR)(struct SERIAL *serial, char c);
typedef void (* PUTSTRING)(struct SERIAL *serial, uint8_t *buff, int len);
typedef int  (* PRINTF)(struct SERIAL *serial, const char *__fmt, ...);
typedef void (* SERIAL_INIT)(struct SERIAL *, uint32_t, const char, bool);
typedef int ( *VFPRINTF)(struct SERIAL *serial, const char *__restrict __fmt, va_list __arg);

#define UART_RX_BUFF_LEN 30

struct SERIAL{
    RX_ISR_ENA rx_isr_ena;
    TX_ISR_ENA tx_isr_ena;
    TX_CHAR tx_char;
    PUTSTRING put_string;
    PRINTF printf;
    SERIAL_INIT init;
    uint8_t bufferRxUartMessage[UART_RX_BUFF_LEN];
    uint8_t recv_isr_pos;
    uint8_t recv_usr_pos;
    StreamBufferHandle_t xBufferTxUartMessage;
};

void serial_init(void);


