#pragma once

#include "serials.h"
/**
 * @brief Inizializzazione dell'hardware responsabile della lettura dei dati da seriale.
 *
 * Questa funzione è chiamata da \ref serial_init.
 */
void serial_uartC_init(struct SERIAL * self, uint32_t uart_speed, const char c, bool swap_uart);
void serial_uartD_init(struct SERIAL * self, uint32_t uart_speed, const char c, bool swap_uart);

void serial_tx_buff(char *buff);
int serial_printf(struct SERIAL *serial, const char *__fmt, ...);
void serial_putstr(struct SERIAL *serial, uint8_t *buff, int len);
int serial_tx_char(struct SERIAL *serial, char c);

void serial_uartC_enable_rx_interrupt(void);
void serial_uartC_enable_tx_interrupt(void);
void serial_uartD_enable_rx_interrupt(void);
void serial_uartD_enable_tx_interrupt(void);