#ifdef __amd64__

#else
#include <avr/pgmspace.h>
#endif
#include <string.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include "stream_buffer.h"
#include <semphr.h>

#include "serials.h"
#include "serial_priv.h"
#include "logger.h"

static SemaphoreHandle_t tx_mutex = NULL;
struct SERIAL uartC;
struct SERIAL uartD;

int serial_tx_char(struct SERIAL *serial, char c){
	int retVal = -1;
	if (serial->xBufferTxUartMessage != NULL){
		xStreamBufferSend(serial->xBufferTxUartMessage, &c, 1, 10/portTICK_PERIOD_MS);		
		if (serial->tx_isr_ena != NULL){
			serial->tx_isr_ena();
		}
		retVal = c;
	}
	return retVal;
}

#define ENABLE_UART_LOGGING 0

inline void serial_putstr(struct SERIAL *serial, uint8_t *buff, int len){
#if ENABLE_UART_LOGGING
    if (serial == &uartD){
        LOG_DEF_NORMAL_P(PSTR("\r\n"));
        for(uint8_t i=0; i<len; i++){
            LOG_DEF_NORMAL_P(PSTR("%02X "), buff[i]);
        }
        LOG_DEF_NORMAL_P(PSTR("\r\n"));
    }
#endif
	if (!xStreamBufferSend(serial->xBufferTxUartMessage, buff, len, 10/portTICK_PERIOD_MS)){
        // LOG_DEF_NORMAL("Coda di trasmissione piena.\r\n");
    }
	if (serial->tx_isr_ena != NULL){
		serial->tx_isr_ena();
	}
}
#define BUFFER_SIZE 60
static char tx_buffer[BUFFER_SIZE];
int serial_printf(struct SERIAL *serial, const char *__fmt, ...){
	int retVal = 0;
	if (tx_mutex != NULL){
		if(xSemaphoreTake(tx_mutex, 100)){
			va_list ap;
			va_start(ap, __fmt);
			vsnprintf(tx_buffer, BUFFER_SIZE, (char *)__fmt, ap);
			va_end(ap);
			serial_putstr(serial, (uint8_t *)tx_buffer, strlen(tx_buffer));
			xSemaphoreGive(tx_mutex);
		}
	}
	return retVal;
}

void serial_init(void){
	if (tx_mutex == NULL){
		tx_mutex = xSemaphoreCreateMutex();
	}
}



struct SERIAL uartC = {
	.init = serial_uartC_init,
	.rx_isr_ena = serial_uartC_enable_rx_interrupt,
	.tx_isr_ena = serial_uartC_enable_tx_interrupt,
	.tx_char = serial_tx_char,
	.put_string = serial_putstr,
	.printf = serial_printf,
	.bufferRxUartMessage = {0},
    .recv_isr_pos = 0,
    .recv_usr_pos = 0,
	.xBufferTxUartMessage = NULL,
};

struct SERIAL uartD = {
	.init = serial_uartD_init,
	.rx_isr_ena = serial_uartD_enable_rx_interrupt,
	.tx_isr_ena = serial_uartD_enable_tx_interrupt,
	.tx_char = serial_tx_char,
	.put_string = serial_putstr,
	.printf = serial_printf,
	.bufferRxUartMessage = {0},
    .recv_isr_pos = 0,
    .recv_usr_pos = 0,
	.xBufferTxUartMessage = NULL,
};

