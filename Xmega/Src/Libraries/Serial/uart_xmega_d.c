#include "asf.h"
#include "serials.h"
#include "serial_priv.h"
#include "event_list.h"
#include "logger.h"

extern struct SERIAL uartD;

static void uartD_error(const union EVENT * const event){
	(void)event;
	LOG_DEF_NORMAL("%s multiple configuration\r\n", __PRETTY_FUNCTION__);
}

static bool configured = false;
void serial_uartD_init(struct SERIAL * self, uint32_t uart_speed, const char c, bool swap_uart) {
    if (configured){
		event_connect_callback(eEVENT_SECONDS_CHANGED, uartD_error);
	}else{
        configured = true;
        if(!swap_uart){
			ioport_set_pin_mode(IOPORT_CREATE_PIN(PORTD, 2), IOPORT_MODE_PULLUP);
			ioport_set_pin_sense_mode(IOPORT_CREATE_PIN(PORTD, 2), IOPORT_SENSE_BOTHEDGES);
			ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 2), IOPORT_DIR_INPUT);

			ioport_set_pin_mode(IOPORT_CREATE_PIN(PORTD, 3), IOPORT_MODE_TOTEM);
			ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 3), IOPORT_DIR_OUTPUT);
		}else{
			PORTD.REMAP |= _BV(PORT_USART0_bp);
			ioport_set_pin_mode(IOPORT_CREATE_PIN(PORTD, 6), IOPORT_MODE_PULLUP);
			ioport_set_pin_sense_mode(IOPORT_CREATE_PIN(PORTD, 6), IOPORT_SENSE_BOTHEDGES);
			ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 6), IOPORT_DIR_INPUT);

			ioport_set_pin_mode(IOPORT_CREATE_PIN(PORTD, 7), IOPORT_MODE_TOTEM);
			ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 7), IOPORT_DIR_OUTPUT);
		}
        self->recv_isr_pos = 0;
        self->recv_usr_pos = 0;
        
        if (self->xBufferTxUartMessage == NULL) {
            self->xBufferTxUartMessage = xStreamBufferCreate(32, 1);
        }
        static usart_serial_options_t usart_D0_options = {
            .baudrate = 0,
            .charlength = USART_CHSIZE_8BIT_gc,
            .paritytype = USART_PMODE_DISABLED_gc,
            .stopbits = false
        };
        usart_D0_options.baudrate = uart_speed;

        usart_serial_init(&USARTD0, &usart_D0_options);
        usart_rx_enable(&USARTD0);
		usart_tx_enable(&USARTD0);
        usart_set_rx_interrupt_level(&USARTD0, USART_DREINTLVL_HI_gc);
    }
}


void serial_uartD_enable_rx_interrupt(void){
    usart_set_rx_interrupt_level(&USARTD0, USART_DREINTLVL_HI_gc);
}

void serial_uartD_enable_tx_interrupt(void){
    usart_set_dre_interrupt_level(&USARTD0, USART_DREINTLVL_LO_gc);
}

ISR(USARTD0_RXC_vect){
    uartD.bufferRxUartMessage[uartD.recv_isr_pos] = USARTD0.DATA;
	uartD.recv_isr_pos++;
	if (uartD.recv_isr_pos >= UART_RX_BUFF_LEN){
		uartD.recv_isr_pos = 0;
	}
}

ISR(USARTD0_DRE_vect){
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    uint8_t tx_byte;
    if (xStreamBufferReceiveFromISR(uartD.xBufferTxUartMessage, &tx_byte, 1, &xHigherPriorityTaskWoken)){
        if (xHigherPriorityTaskWoken) {
            taskYIELD();
        }
        USARTD0.DATA = tx_byte;
	}else{
		usart_set_dre_interrupt_level(&USARTD0, USART_DREINTLVL_OFF_gc);
	}
}
