#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include "FreeRTOS.h"
#include "serial_priv.h"
#include "serials.h"
#include "timers.h"

extern struct SERIAL uartC;
extern struct SERIAL uartD;

static FILE* handle_uartC;
static FILE* handle_uartD;

static bool uartC_recv_9F = false;
static bool uartD_recv_9F = false;

static TimerHandle_t timer = NULL;

int _kbhit() {
    static const int STDIN = 0;
    static bool initialized = false;

    if (!initialized) {
        // Use termios to turn off line buffering
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

#define BUFF_LEN 40

static void timer_printout(TimerHandle_t timer) {
    (void)timer;
    uint8_t c;
    char buff[BUFF_LEN];
    if (xStreamBufferReceive(uartC.xBufferTxUartMessage, &c, 1, 0)) {
        //write(handle_uart0, &c, 1);
        snprintf(buff, BUFF_LEN, "%02X ", c);
        if (uartC_recv_9F) {
            if (c == 0x02) {
                snprintf(buff, BUFF_LEN, "%lu 9F %02X ", (unsigned long)time(NULL), c);
            }
            if (c == 0x03) {
                snprintf(buff, BUFF_LEN, "9F %02X\r\n", c);
            }
            if (c == 0x9F) {
                snprintf(buff, BUFF_LEN, "9F %02X ", c);
            }
            uartC_recv_9F = false;
        } else {
            if (c == 0x9F) {
                uartC_recv_9F = true;
            }
        }
        if (!uartC_recv_9F) {
            fwrite(buff, 1, strlen(buff), handle_uartC);
            fflush(handle_uartC);
        }
    }
    if (xStreamBufferReceive(uartD.xBufferTxUartMessage, &c, 1, 0)) {
        // handle_uart0 =  open("/tmp/uart.txt", O_WRONLY | O_APPEND | O_CREAT, 0644);
        // write(handle_usart0, &c, 1);
        // close(handle_uart0);
        snprintf(buff, BUFF_LEN, "%02X ", c);
        if (uartD_recv_9F) {
            if (c == 0x02) {
                snprintf(buff, BUFF_LEN, "%lu 9F %02X ", (unsigned long)time(NULL), c);
            }
            if (c == 0x03) {
                snprintf(buff, BUFF_LEN, "9F %02X\r\n", c);
            }
            if (c == 0x9F) {
                snprintf(buff, BUFF_LEN, "9F %02X ", c);
            }
            uartD_recv_9F = false;
        } else {
            if (c == 0x9F) {
                uartD_recv_9F = true;
            }
        }
        if (!uartD_recv_9F) {
            fwrite(buff, 1, strlen(buff), handle_uartD);
            fflush(handle_uartD);
        }
    }
}

void serial_uartC_init(struct SERIAL* self, uint32_t uart_speed, const char c, bool swap_uart) {
    (void)self;
    (void)uart_speed;
    (void)swap_uart;
    (void)c;
    self->recv_isr_pos = 0;
    self->recv_usr_pos = 0;

    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        printf("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_uartC = fopen("/tmp/uartC.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_uartD_init(struct SERIAL* self, uint32_t uart_speed, const char c, bool swap_uart) {
    (void)self;
    (void)uart_speed;
    (void)swap_uart;
    (void)c;
    self->recv_isr_pos = 0;
    self->recv_usr_pos = 0;

    if (self->xBufferTxUartMessage == NULL) {
        self->xBufferTxUartMessage = xStreamBufferCreate(256, 2);
        printf("%s Apro il file.\r\n", __PRETTY_FUNCTION__);
        handle_uartD = fopen("/tmp/uartD.txt", "a");
    }
    if (timer == NULL) {
        timer = xTimerCreate("swc_tim", 5 / portTICK_PERIOD_MS, pdTRUE, NULL, timer_printout);
        xTimerStart(timer, 2);
    }
}

void serial_uartC_enable_rx_interrupt(void) {
}
void serial_uartC_enable_tx_interrupt(void) {
}
void serial_uartD_enable_rx_interrupt(void) {
}
void serial_uartD_enable_tx_interrupt(void) {
}
