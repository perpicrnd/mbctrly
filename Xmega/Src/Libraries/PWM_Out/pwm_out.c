#include <string.h>
#include "FreeRTOS.h"
#include "timers.h"
#include "logger.h"
#include "event_list.h"
#include "pwm_out.h"
#include "pwm_out_priv.h"
#include "eeprom.h"

#define TIMER_TICK 50
static TimerHandle_t timer;

enum LED_CFG{
    LED_RED = 0,
    LED_GREEN,
    LED_BLUE,
    LED_WHITE,
    LED_COUNT,
};

static uint8_t leds[LED_COUNT] = {0};
static uint8_t luma = 0;


/**
 * @brief Variabile di appoggio per il passaggio dati da interrupt a userspace.
 */
volatile uint16_t cca = 0;
/**
 * @brief Variabile di appoggio per il passaggio dati da interrupt a userspace.
 */
volatile uint16_t ccb = 0;

volatile bool overflow_off = false;
volatile bool overflow_on = false;

static void timer_pwm_out(TimerHandle_t timer){
    (void)timer;
    bool changed = false;
    {
        uint8_t luma_tmp;
        if (overflow_off){
            luma_tmp = 0;
        }else if(overflow_on){
            luma_tmp = 100;
        }else{
            portENTER_CRITICAL();
            luma_tmp = cca * (uint32_t)100 / ccb;
            portEXIT_CRITICAL();
        }
        
        if (luma != luma_tmp){
            if (luma_tmp > luma){
                luma++;
                changed = true;
            }else if (luma_tmp < luma){
                luma--;
                changed = true;
            }
        }
    }
    {
        uint8_t leds_tmp[LED_COUNT];
        eeprom_safe_get_leds(eeprom_safe_get_active_led_cfg(), &leds_tmp[LED_RED], &leds_tmp[LED_GREEN], &leds_tmp[LED_BLUE], &leds_tmp[LED_WHITE]);    
        if (memcmp(leds, leds_tmp, sizeof(leds)) != 0){
            memcpy(leds, leds_tmp, sizeof(leds));
            changed = true;
        }
    }
    if (changed){
        uint16_t leds16[LED_COUNT] = {0};
        for(uint8_t i=0; i<LED_COUNT; i++){
            double res =  (((double)PWM_TIMER_PERIOD) * luma) / 100;
            double res2 = ((res * leds[i]) / 0xFF);
            leds16[i] = (uint16_t)res2;
        }
        // LOG_DEF_NORMAL("%d %d %d %d\r\n", leds16[LED_RED], leds16[LED_GREEN], leds16[LED_BLUE], leds16[LED_WHITE]);
        update_leds_value(leds16[LED_RED], leds16[LED_GREEN], leds16[LED_BLUE], leds16[LED_WHITE]);
    }
}

void pwm_out_init(void){
    pwm_out_hw_init();
    timer = xTimerCreate("led_tim", TIMER_TICK/portTICK_PERIOD_MS, pdTRUE, NULL, timer_pwm_out);
    xTimerStart(timer, TIMER_TICK);

}