#include "asf.h"
#include "pwm_out.h"
#include "pwm_out_priv.h"
#include "logger.h"



/**
 * @brief valore salvato durante l'ultimo giro del task
 */
static volatile uint16_t old_cca = 0;
/**
 * @brief valore salvato durante l'ultimo giro del task
 */
static volatile uint16_t old_ccb = 0;
/**
 * @brief valore salvato durante l'ultimo giro del task
 */
static volatile uint16_t old_ccc = 0;
/**
 * @brief valore salvato durante l'ultimo giro del task
 */
static volatile uint16_t old_ccd = 0;

ISR(TCC4_OVF_vect)
{
	tc45_write_cc(&TCC4, TC45_CCA, old_cca);
	tc45_write_cc(&TCC4, TC45_CCB, old_ccb);
	tc45_write_cc(&TCC4, TC45_CCC, old_ccc);
    tc45_write_cc(&TCC4, TC45_CCD, old_ccd);
	tc45_set_overflow_interrupt_level(&TCC4, TC45_INT_LVL_OFF);
}

void update_leds_value(uint16_t red, uint16_t green, uint16_t blue, uint16_t white){
    old_cca = red;
    old_ccb = green;
    old_ccc = blue;
    old_ccd = white;
    // bool off = false;
    // if (old_cca == 0){
    //     if (old_ccb == 0){
    //         if (old_ccc == 0){
    //             if (old_ccd == 0){
    //                off = true; 
    //             }
    //         }
    //     }
    // }

    // if (off){
    //     tc45_disable(&TCC4);
    //     tc45_disable_cc_channels(&TCC4, TC45_CCACOMP);
    //     tc45_disable_cc_channels(&TCC4, TC45_CCBCOMP);
    //     tc45_disable_cc_channels(&TCC4, TC45_CCCCOMP);
    //     tc45_disable_cc_channels(&TCC4, TC45_CCDCOMP);
    //     ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 4), false);
    //     ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 5), false);
    //     ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 6), false);
    //     ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 7), false);
    // }else{
    //     tc45_enable_cc_channels(&TCC4, TC45_CCACOMP);
    //     tc45_enable_cc_channels(&TCC4, TC45_CCBCOMP);
    //     tc45_enable_cc_channels(&TCC4, TC45_CCCCOMP);
    //     tc45_enable_cc_channels(&TCC4, TC45_CCDCOMP);
    //     tc45_enable(&TCC4);
    // }
    tc45_set_overflow_interrupt_level(&TCC4, TC45_INT_LVL_LO);
}

void pwm_out_hw_init(void){
    ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 4), true);
    ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 5), true);
    ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 6), true);
    ioport_set_pin_level(IOPORT_CREATE_PIN(PORTC, 7), true);
    ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 4), IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 5), IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 6), IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 7), IOPORT_DIR_OUTPUT);
    
    tc45_enable(&TCC4);

    tc45_set_wgm(&TCC4, TC45_WG_SS);
    PORTC.REMAP |= 0x0F;
    tc45_write_period(&TCC4, PWM_TIMER_PERIOD);
    tc45_write_cc(&TCC4, TC45_CCA, 0); 
    tc45_write_cc(&TCC4, TC45_CCB, 0); 
    tc45_write_cc(&TCC4, TC45_CCC, 0); 
    tc45_write_cc(&TCC4, TC45_CCD, 0); 
    tc45_enable_cc_channels(&TCC4, TC45_CCACOMP);
    tc45_enable_cc_channels(&TCC4, TC45_CCBCOMP);
    tc45_enable_cc_channels(&TCC4, TC45_CCCCOMP);
    tc45_enable_cc_channels(&TCC4, TC45_CCDCOMP);

    tc45_write_clock_source(&TCC4, TC45_CLKSEL_DIV1_gc);
}