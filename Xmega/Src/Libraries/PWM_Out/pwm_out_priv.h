#pragma once

#include <stdint.h>

/**
 * Periodo del timer
 */
#define PWM_TIMER_PERIOD 0xFFFF

void pwm_out_hw_init(void);
void update_leds_value(uint16_t red, uint16_t green, uint16_t blue, uint16_t white);