#include <avr/io.h>
#include "asf.h"
#include "alive_controller.h"
#include "serials.h"



extern struct SERIAL *controller_uart;

void force_sam_off(void){

    ioport_set_pin_dir(Not_Enable_Sam, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Not_Enable_Sam, true);

    ioport_set_pin_level(Vcc_33_Disch, true);
    ioport_set_pin_dir(Vcc_33_Disch, IOPORT_DIR_OUTPUT);
    usart_tx_disable(&USARTD0);
    usart_rx_disable(&USARTD0);


}

void turn_sam_on(void){
    ioport_set_pin_level(Vcc_33_Disch, false);
    ioport_set_pin_dir(Vcc_33_Disch, IOPORT_DIR_OUTPUT);

    ioport_set_pin_dir(Not_Enable_Sam, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Not_Enable_Sam, false);
}