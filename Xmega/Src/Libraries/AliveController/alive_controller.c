#include "event_list.h"
#include "alive_controller.h"
#include "watchdog.h"
#include "FreeRTOS.h"
#include "logger.h"
#include "timers.h"
#include "eeprom.h"

#define TIMER_TICK 100

enum ALIVE_CONTROLLER_STATE{
    ALIVE_CONTROLLER_STATE_UNKNOWN = 0,
    ALIVE_CONTROLLER_STATE_RUNNING_NO_ACK,
    ALIVE_CONTROLLER_STATE_RUNNING,
    ALIVE_CONTROLLER_STATE_RESET
};

static TimerHandle_t timer;
static uint8_t counter = 0;
static uint8_t timer_counter = 0;
static uint8_t alive_seconds = 0;
static uint16_t off_counter = 0;
static uint8_t stuff_counter = 0;
static uint8_t old_stuff_counter = 0;
static uint8_t error_counter = 0;
static uint8_t sleep_counter = 0;
static enum ALIVE_CONTROLLER_STATE controller_state = ALIVE_CONTROLLER_STATE_UNKNOWN;

void alive_controller_got_stuff(void){
    error_counter = 0;
}

void alive_controller_got_tick(uint8_t sec){
    alive_seconds = sec;
    counter++;
    if (counter == 0){
        counter = 1;
    }
    
    alive_controller_got_stuff();
}

static inline void alive_controller_state_unknown(void){
    off_counter = 0;
    controller_state = ALIVE_CONTROLLER_STATE_RUNNING_NO_ACK;
    watchdog_reset();
}

static inline void alive_controller_state_running_no_ack(void){
    watchdog_reset();
    if (counter != 0){
        controller_state = ALIVE_CONTROLLER_STATE_RUNNING;
    }
    off_counter++;
    if (off_counter >= 30){
        controller_state = ALIVE_CONTROLLER_STATE_RESET;
    }
}

static inline void alive_controller_state_running(void){
    if (counter == timer_counter){
        if ((alive_seconds >= 2)&&(alive_seconds != 0xFF)){
            watchdog_reset();
            uint8_t stf = stuff_counter;
            off_counter++;
            if (off_counter > 15){
                LOG_DEF_NORMAL_P(PSTR("Off Cnt %d\r\n"), off_counter);
            }
            if (stf != old_stuff_counter){
                //C'è comunicazione, resetto il counter
                off_counter = 0;
                old_stuff_counter = stf;
            }
            if (off_counter >= 30){
                //Tre secondi senza comunicazione, vado in failure
                controller_state = ALIVE_CONTROLLER_STATE_RESET;
            }
        }else{
            LOG_DEF_NORMAL_P(PSTR("Slp %d\r\n"), sleep_counter);
            if (sleep_counter++ > 10){
                software_reset();
            }
        }
    }else{
        //Qui tutto è buono.
        sleep_counter = 0;
        watchdog_reset();
        off_counter = 0;
    }
    timer_counter = counter;
}


static inline void alive_controller_state_reset(void){
    watchdog_reset();
    if (off_counter <= 35){
        force_sam_off();
        LOG_DEF_NORMAL_P(PSTR("Force SAM OFF\r\n"));
    }else{
        turn_sam_on();
        LOG_DEF_NORMAL_P(PSTR("Reset\r\n"));
        software_reset();
    }
    off_counter++;
}

void timer_alive_controller(TimerHandle_t xTimer){
    (void)xTimer;
    if (counter != timer_counter){
        //Attività sulla seriale, il Sam sta comunicando dati.
        error_counter++;
        if (error_counter > 10){
            LOG_DEF_NORMAL_P(PSTR("Error counter %d\r\n"), error_counter);
        }
        if (error_counter < 20){
            watchdog_reset();
        }
        if (alive_seconds != 0){
            watchdog_reset();
        }
    }
    switch(controller_state){
        case ALIVE_CONTROLLER_STATE_UNKNOWN:
            alive_controller_state_unknown();
        break;
        case ALIVE_CONTROLLER_STATE_RUNNING_NO_ACK:
            alive_controller_state_running_no_ack();
        break;
        case ALIVE_CONTROLLER_STATE_RUNNING:
            alive_controller_state_running();
        break;
        case ALIVE_CONTROLLER_STATE_RESET:
            alive_controller_state_reset();
        break;
    }
}



void alive_controller_init(void){
    timer = xTimerCreate("swc_tim", TIMER_TICK/portTICK_PERIOD_MS, pdTRUE, NULL, timer_alive_controller);
    xTimerStart(timer, TIMER_TICK);
    watchdog_reset();
}