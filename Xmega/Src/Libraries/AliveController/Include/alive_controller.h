#pragma once

#include <stdint.h>

void alive_controller_got_tick(uint8_t alive_seconds);
void alive_controller_got_stuff(void);
void alive_controller_init(void);
void force_sam_off(void);
void turn_sam_on(void);