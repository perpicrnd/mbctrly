cmake_minimum_required(VERSION 3.0)

SET(LIBRARY_NAME freertos)
PROJECT(${LIBRARY_NAME} VERSION 0.1 LANGUAGES C)

if (CMAKE_CROSSCOMPILING)
    MESSAGE("CROSS BUILD FREERTOS")
	set(SOURCES ${SOURCES} portable/GCC/ATXmega/port.c
		portable/MemMang/heap_1.c
	)
else()
    MESSAGE("NORMAL BUILD FREERTOS")
	find_package (Threads)
	set(SOURCES ${SOURCES} portable/GCC/Posix/port.c
		portable/MemMang/heap_3.c
	)
endif()	

set(SOURCES ${SOURCES}
	tasks.c
	croutine.c
    event_groups.c
    stream_buffer.c
	list.c
	queue.c
	timers.c
)

add_library(${LIBRARY_NAME} ${SOURCES})

if( CMAKE_CROSSCOMPILING)
	target_link_libraries(${LIBRARY_NAME} asf)
else()
	target_link_libraries (${LIBRARY_NAME} ${CMAKE_THREAD_LIBS_INIT})
endif()

        
if (CMAKE_CROSSCOMPILING)
        target_include_directories(${LIBRARY_NAME}  PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
            $<INSTALL_INTERFACE:include/>
            PUBLIC 
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/portable/GCC/ATXmega>
            $<INSTALL_INTERFACE:/portable/GCC/ATXmega/>
            PRIVATE .)
else()
        target_include_directories(${LIBRARY_NAME}  PUBLIC
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
            $<INSTALL_INTERFACE:include/>
            PUBLIC 
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/portable/GCC/Posix>
            $<INSTALL_INTERFACE:/portable/GCC/Posix/>
            PRIVATE .)
endif()
