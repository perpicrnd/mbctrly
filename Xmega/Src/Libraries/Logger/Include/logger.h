#pragma once

#include <stdarg.h>

#ifdef __amd64__
#define PSTR(x) (x)
#define vsnprintf_P vsnprintf
#endif

enum LOGGING{
	LOGGING_DEFAULT = 0,
	LOGGING_COUNT
};

typedef enum{
	LOGGING_SEVERITY_ERROR,
	LOGGING_SEVERITY_NORMAL,
	LOGGING_SEVERITY_INFO,
	LOGGING_SEVERITY_WARNING,
	LOGGING_SEVERITY_DEBUG,
}LOGGING_SEVERITY;

void logger_init(void);
void logging_register(enum LOGGING log, LOGGING_SEVERITY severity);
int logging_notification(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...)__attribute__ ((format (gnu_printf, 3, 4))); 
int logging_notification_p(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...)__attribute__ ((format (gnu_printf, 3, 4))); 
void SWO_PrintStringDef(const char *s);
void SWO_PrintStringFmt(const char *format, ...);


#define LOG_DEF_DEBUG(...) 		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_DEBUG, __VA_ARGS__)
#define LOG_DEF_WARNING(...)	logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_WARNING, __VA_ARGS__)
#define LOG_DEF_ERROR(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_ERROR, __VA_ARGS__)
#define LOG_DEF_INFO(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_INFO, __VA_ARGS__)
#define LOG_DEF_NORMAL(...)		logging_notification(LOGGING_DEFAULT, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)
#define LOG_DEF_NORMAL_P(...)		logging_notification_p(LOGGING_DEFAULT, LOGGING_SEVERITY_NORMAL, __VA_ARGS__)