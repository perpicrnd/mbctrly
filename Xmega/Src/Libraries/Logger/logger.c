#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "logger.h"
#include "serials.h"

#ifndef __amd64__
#include "asf.h"
#endif

extern struct SERIAL uartC;
extern uint8_t stop_printout; //Si trova in freertos_clitask.c

struct SERIAL *logger = NULL;
typedef struct{
	LOGGING_SEVERITY severity[LOGGING_COUNT];
}LOGGING_pvt;

LOGGING_pvt logging;

void logging_register(enum LOGGING log, LOGGING_SEVERITY severity){
	if (log >= LOGGING_COUNT){
		return;
	}
	logging.severity[log] = severity;
}

static uint8_t pcWriteBuffer[100];
int logging_notification(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...){
	int retVal = 0;
        
	if (logging.severity[log] >= severity){
		va_list ap;
		va_start(ap, format);
        
		vsnprintf( (char *)pcWriteBuffer, 100, format, ap);
		va_end(ap);
        
        uartC.put_string(&uartC, pcWriteBuffer, strlen((char *)pcWriteBuffer));
	}
	return retVal;
}

int logging_notification_p(enum LOGGING log, LOGGING_SEVERITY severity, const char *format, ...){
	int retVal = 0;
        
	if (logging.severity[log] >= severity){
		va_list ap;
		va_start(ap, format);
        
		vsnprintf_P( (char *)pcWriteBuffer, 100, format, ap);
		va_end(ap);
        
        uartC.put_string(&uartC, pcWriteBuffer, strlen((char *)pcWriteBuffer));
	}
	return retVal;
}

void logger_init(void){
	logging_register(LOGGING_DEFAULT, LOGGING_SEVERITY_INFO);
    uartC.init(&uartC, 115200, 'N', false);
	// LOG_DEF_NORMAL("%s\r\n", __PRETTY_FUNCTION__);
};
