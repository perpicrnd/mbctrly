#ifndef _ARRAYLEN_H_
#define _ARRAYLEN_H_

/**
 * This macro uses a simple but effective way for giving the length of an array
 * it is just total size of the array / size of a single element.
 */
#define ARRAYLEN( x )   (sizeof(x)/sizeof(x[0]))


#endif
