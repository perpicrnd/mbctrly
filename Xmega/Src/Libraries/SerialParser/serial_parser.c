

#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "timers.h"
#include "event_list.h"
#include "serial_parser.h"
#include "serial_parser_priv.h"
#include "logger.h"
#include "watchdog.h"
#include "eeprom.h"
#include "alive_controller.h"
#include "updater_parser.h"

#define CONTROLLER_QUEUE_NUM 12
#define SHARED_BUFFER_LEN 60

extern struct SERIAL uartD;
struct SERIAL *controller_uart;

union PERPIC_CONTROLLER_MESSAGE tx_message;
union PERPIC_CONTROLLER_MESSAGE rx_message;
static QueueHandle_t task_queue;
static TimerHandle_t msec160;
static uint8_t timeout_counter = 0;
bool ack_received = true;
uint8_t nack_counter = 0;
static bool enter_updater = false;

static inline void reset_counters(void){
    taskENTER_CRITICAL();
    timeout_counter = 0;
    nack_counter = 0;
    ack_received = true;
    taskEXIT_CRITICAL();
}
static uint8_t tx_shared_buffer[SHARED_BUFFER_LEN];
static uint8_t tx_shared_buffer_pos = 0;
static inline void reset_buffer(void){
    for(uint8_t i=0; i<SHARED_BUFFER_LEN; i++){
        tx_shared_buffer[i] = 0x00;
    }
    tx_shared_buffer_pos = 0;
}


static void controller_tx_message(union PERPIC_CONTROLLER_MESSAGE *msg){
    reset_buffer();
    tx_shared_buffer[0] = 0x9F;
    tx_shared_buffer[1] = 0x02;
    tx_shared_buffer[2] = msg->as_raw.pos;
    uint8_t j = 3;
    for (uint8_t i=0; i<=msg->as_raw.pos; i++, j++){
        switch(msg->as_raw.data[i]){
            case 0x9F:
            tx_shared_buffer[j] = 0x9F;
            j++;
            break;
        }
        tx_shared_buffer[j] = msg->as_raw.data[i];
        
    }
    tx_shared_buffer[j++] = 0x9F;
    tx_shared_buffer[j++] = 0x03;
    xTimerStart(msec160, 0);
    tx_shared_buffer_pos = j;
    controller_uart->put_string(controller_uart, tx_shared_buffer, j);
    vTaskDelay(20/portTICK_PERIOD_MS);
}


#define ACKNACK_LEN 8
static inline void controller_tx_message_ack(void){   
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x06\x07\x9F\x03"};
    controller_uart->put_string(controller_uart, tx_buffer, ACKNACK_LEN);
}

static inline void controller_tx_message_nack(void){
    uint8_t tx_buffer[ACKNACK_LEN] = {"\x9F\x02\x01\x9F\x15\x14\x9F\x03"};
    controller_uart->put_string(controller_uart, tx_buffer, ACKNACK_LEN);
}


static void controller_tx(struct CONTROLLER_DATA *controller){
    perpic_message_raw_simple_reset(&tx_message.as_raw);
    perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->command);
    switch(controller->command){
        case CONTROLLER_WO_ALIVE_SECONDS:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->alive_seconds);
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        case CONTROLLER_RO_BUTTONS:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        case CONTROLLER_RO_EEPROM:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[1]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[2]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[3]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[4]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[5]);
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        case CONTROLLER_RW_ACTIVE_CONF:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        case CONTROLLER_RO_EEPROM_PAGE:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[1]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[2]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[3]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[4]);
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[5]);
       
        for(uint8_t i=0; i<controller->data[5]; i++){
            perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[6+i]);
        }
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        case CONTROLLER_WO_POWER:
        perpic_message_raw_simple_add_uint8(&tx_message.as_raw, controller->data[0]);
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);
        break;
        default: 
        perpic_message_raw_simple_add_crc(&tx_message.as_raw);
        controller_tx_message(&tx_message);    
        break;
    }
    
}

static inline void handle_controller_alive_seconds(union PERPIC_CONTROLLER_MESSAGE *msg){
    if (msg->as_raw.data[1] != 0){
        alive_controller_got_tick(msg->as_raw.data[1]);
    }
}

static inline void handle_controller_eeprom_write(union PERPIC_CONTROLLER_MESSAGE *msg){
    eeprom_safe_update_byte(msg->as_raw.data[1], msg->as_raw.data[2]);
}

static inline void handle_controller_eeprom_read(union PERPIC_CONTROLLER_MESSAGE *msg){
    uint32_t func_pointer = ((uint32_t)msg->as_raw.data[1]) | ((uint32_t)msg->as_raw.data[2] << 8) | ((uint32_t)msg->as_raw.data[3]<<16) | ((uint32_t)msg->as_raw.data[4] << 24);
    eeprom_safe_get_byte(func_pointer, msg->as_raw.data[5]);
}

static inline void handle_controller_leds_conf(union PERPIC_CONTROLLER_MESSAGE *msg){
    eeprom_safe_update_leds(msg->as_raw.data[1], msg->as_raw.data[2], msg->as_raw.data[3], msg->as_raw.data[4], msg->as_raw.data[5]);
}

static inline void handle_controller_active_led(union PERPIC_CONTROLLER_MESSAGE *msg){
    eeprom_safe_update_active_led(msg->as_raw.data[1]);
}

static inline void handle_controller_hardware_selection(union PERPIC_CONTROLLER_MESSAGE *msg){
    if (msg->as_raw.data[1] == 0x07){
        enter_updater = true;
    }
}

static inline void handle_controller_eeprom_read_page(union PERPIC_CONTROLLER_MESSAGE *msg){
    uint32_t func_pointer = ((uint32_t)msg->as_raw.data[1]) | ((uint32_t)msg->as_raw.data[2] << 8) | ((uint32_t)msg->as_raw.data[3]<<16) | ((uint32_t)msg->as_raw.data[4] << 24);
    eeprom_safe_get_page(func_pointer, msg->as_raw.data[5]);
}

static bool get_uart_char(char *c){
    bool retVal = false;
    if (controller_uart->recv_isr_pos != controller_uart->recv_usr_pos){
		*c = controller_uart->bufferRxUartMessage[controller_uart->recv_usr_pos];
		controller_uart->recv_usr_pos++;
		if (controller_uart->recv_usr_pos >= UART_RX_BUFF_LEN){
			controller_uart->recv_usr_pos = 0;
		}
		retVal = true;
	}
    return retVal;
}


static bool clear_buffer = false;
static SemaphoreHandle_t acknack_sem;
static void serial_parser_task(void* parameter)
{
    (void)parameter;
    controller_uart->init(controller_uart, 115200, 'N', true);
    for (;;) {
        if (enter_updater){
            firmware_task();
        }
        do{
            if (xSemaphoreTake(acknack_sem, 1)){
                controller_tx_message(&tx_message);
                break;
            }

            if (ack_received){
                struct CONTROLLER_DATA msg;
                if (xQueueReceive(task_queue, &msg, 10/portTICK_PERIOD_MS)){
                    controller_tx(&msg);
                    ack_received = false;
                }
            }
        }while(0);

        char c;
        bool ch_recv;
        do{
            ch_recv = false;
            if (get_uart_char(&c)) {
                ch_recv = true;
                perpic_message_raw_simple_add_uint8_from_extern(&rx_message.as_raw, c);
                if (perpic_message_raw_simple_is_completed(&rx_message.as_raw)) {
                    if (perpic_message_raw_simple_is_acknack(&rx_message.as_raw)){
                        xTimerStop(msec160, 10);
                        alive_controller_got_stuff();
                        if (perpic_message_raw_simple_is_nack(&rx_message.as_raw)){
                            if (nack_counter <= 3){
                                perpic_message_raw_simple_print(&rx_message.as_raw);
                                xSemaphoreGive(acknack_sem);
                                nack_counter++;
                            }else{
                                reset_counters();
                            }
                        }else{
                            reset_counters();
                        }

                    }else{
                        if (perpic_message_raw_simple_crc_check(&rx_message.as_raw) == PERPIC_CRC_SIMPLE_OK){
                            controller_tx_message_ack();
                            alive_controller_got_stuff();
                            switch (perpic_message_raw_simple_get_id(&rx_message.as_raw)) {
                            case CONTROLLER_WO_ALIVE_SECONDS:
                                handle_controller_alive_seconds(&rx_message);
                                break;
                            case CONTROLLER_WO_EEPROM:
                                handle_controller_eeprom_write(&rx_message);
                                break;
                            case CONTROLLER_RW_LEDS:
                                handle_controller_leds_conf(&rx_message);
                                break;
                            case CONTROLLER_RW_ACTIVE_CONF:
                                handle_controller_active_led(&rx_message);
                                break;
                            case CONTROLLER_RO_EEPROM:  
                                handle_controller_eeprom_read(&rx_message);
                                break;
                            case CONTROLLER_FACTORY_SETTING:
                                eeprom_safe_factory_settings();
                            break;
                            case CONTROLLER_WO_SELECTOR:
                                handle_controller_hardware_selection(&rx_message);
                            break;
                            case CONTROLLER_RO_EEPROM_PAGE:
                                handle_controller_eeprom_read_page(&rx_message);
                                break;
                            default:
                                break;
                            }
                        }else{
                            controller_tx_message_nack();
                        }
                    }
                    perpic_message_raw_simple_reset(&rx_message.as_raw);
                }
            }else{
                vTaskDelay(2/portTICK_PERIOD_MS);
            }
        }while(ch_recv);
    }
}


static void timer_msec160(TimerHandle_t pxTimer){
	(void)pxTimer;
    clear_buffer = true;
	///Release the semaphore for unlock the radio task.
	//Non ho un sequence number sul timeout.
    if (timeout_counter < 3){
	    timeout_counter++;
        xSemaphoreGive(acknack_sem);
    }else{
        reset_counters();
    }
}

static bool configured = false;
void serial_parser_init(void){
    if (!configured){
        configured = true;
       controller_uart = &uartD;
        xTaskCreate(serial_parser_task, (char*)"ctrl", configMINIMAL_STACK_SIZE+180, NULL, configMAX_PRIORITIES, NULL);
        task_queue = xQueueCreate(CONTROLLER_QUEUE_NUM, sizeof(struct CONTROLLER_DATA));
        msec160 = xTimerCreate((char *)"t_160" , 160/portTICK_PERIOD_MS, 	pdFALSE, 0, timer_msec160);
        acknack_sem = xSemaphoreCreateBinary();
    }

}

void event_controller_emit(struct CONTROLLER_DATA *controller){
    if (uxQueueSpacesAvailable(task_queue) < 2){
        return;
    }
    xQueueSendToBack(task_queue, controller, 2);
}