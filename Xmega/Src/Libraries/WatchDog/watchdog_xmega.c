#include "asf.h"
#include "watchdog.h"
#include "logger.h"

void watchdog_reset(void){
    wdt_reset();
}

void watchdog_init(void){
    wdt_enable();
    wdt_set_timeout_period(WDT_TIMEOUT_PERIOD_1KCLK);
}