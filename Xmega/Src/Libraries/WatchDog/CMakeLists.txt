
if (CMAKE_CROSSCOMPILING)
    add_library(watchdog watchdog_xmega.c)
    target_link_libraries(watchdog asf)
else()
    add_library(watchdog watchdog_host.c)
endif()

target_link_libraries(watchdog logger)

target_include_directories(watchdog PUBLIC
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
$<INSTALL_INTERFACE:include/>
PRIVATE .)
