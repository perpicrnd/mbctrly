#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "eeprom.h"

enum EEPROM_BANK{
    EEPROM_BANK_0 = 0,
    EEPROM_BANK_1,
    EEPROM_BANK_2,
    EEPROM_BANK_COUNT
};

struct  __attribute__((packed)) EEPROM_MEMORY_PVT { 
    uint32_t empty1;
    struct EEPROM_MEMORY memory;
    bool dirty;
    uint32_t empty2;
};

struct __attribute__((packed)) EEPROM_MEMORY_STR {
    uint8_t values[sizeof(struct EEPROM_MEMORY_PVT)];
};

union  __attribute__((packed)) EEPROM_UNION {
    struct EEPROM_MEMORY_PVT as_struct;
    struct EEPROM_MEMORY_STR as_str;
};

struct __attribute__((packed)) EEPROM_MEMORY_INITIALIZED{
	union EEPROM_UNION eeprom;	//! I dati da salvare in eeprom
	bool initialized;		//! Flag che indica se i dati sono stati inizializzati
	uint16_t checksum;		//! Checksum calcolato dei dati salvati
} ;

void eeprom_hw_init(void);
uint16_t eeprom_bank_checksum(enum EEPROM_BANK bank);
bool eeprom_bank_initialized(enum EEPROM_BANK bank);

uint16_t eeprom_safe_calculate_crc(void);

uint8_t eeprom_safe_read_byte (const uint8_t *p); 
uint16_t eeprom_safe_read_word (const uint16_t *p);
uint32_t eeprom_safe_read_dword (const uint32_t *p);
void eeprom_safe_write_byte(uint8_t *p, uint8_t value);
void eeprom_safe_write_dword(uint32_t *p, uint32_t value);

uint8_t eeprom_safe_read_flash(const uint16_t *p);
void eeprom_safe_write_block (const void *src, void *dst, size_t n);
void eeprom_safe_update_block(const void *src, void *dst, size_t n);
void eeprom_safe_read_block(void *dst, const void *src, size_t n);