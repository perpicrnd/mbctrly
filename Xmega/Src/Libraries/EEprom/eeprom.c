#include <stdio.h>
#include "eeprom.h"
#include "FreeRTOS.h"
#include "eeprom_priv.h"
#include "event_list.h"
#include "logger.h"
#include "task.h"

#ifdef __amd64__
#include <string.h>
#define EEMEM
#define PROGMEM
#define memcpy_P memcpy
#define _delay_ms(x) \
    do {             \
    } while (0);
#else
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#endif

static enum EEPROM_BANK active_eeprom_bank = EEPROM_BANK_COUNT;

static const union EEPROM_UNION eeprom_in_flash PROGMEM = {
    .as_struct = {
        .empty1 = 0x01234567,
        .memory = {
            .led_setting = {
                {0xFF, 0x00, 0x00, 0x00},  //Red
                {0x7F, 0x00, 0x00, 0x00},  //Red mid
                {0x00, 0xFF, 0x00, 0x00},  //Green
                {0x00, 0x7F, 0x00, 0x00},  //Green mid
                {0x00, 0x00, 0xFF, 0x00},  //Blue
                {0x00, 0x00, 0x7F, 0x00},  //Blue mid
                {0x00, 0x00, 0x00, 0xFF},  //White
                {0x00, 0x00, 0x00, 0x7F},  //White mid
                {0xFF, 0x20, 0x00, 0x00},  //Orange
                {0x7F, 0x10, 0x00, 0x00},  //Orange mid
                {0xFF, 0x68, 0x00, 0x00},  //Yellow
                {0x7F, 0x34, 0x00, 0x00},  //Yellow mid
                {0x00, 0x00, 0x00, 0x00},  //LED OFF
            },
            .active_led_cfg = 0x0C,
            .memory = {
                .eeprom_array = {
                    [0 ...(E2ARRAY - 1)] = 0xFF,
                },
            },
        },
        .empty2 = 0x89ABCDEF}};

static struct EEPROM_MEMORY_INITIALIZED EEMEM myeeprom[EEPROM_BANK_COUNT] = {};

struct EEPROM_MEMORY_INITIALIZED ram_data;

void eeprom_memory_flush(void) {
    ram_data.checksum = eeprom_safe_calculate_crc();
    eeprom_safe_write_block(&ram_data, &myeeprom[EEPROM_BANK_0], sizeof(struct EEPROM_MEMORY_INITIALIZED));
    eeprom_safe_write_block(&ram_data, &myeeprom[EEPROM_BANK_1], sizeof(struct EEPROM_MEMORY_INITIALIZED));
    eeprom_safe_write_block(&ram_data, &myeeprom[EEPROM_BANK_2], sizeof(struct EEPROM_MEMORY_INITIALIZED));
}

uint16_t eeprom_bank_checksum(enum EEPROM_BANK bank) {
    return eeprom_safe_read_word(&myeeprom[bank].checksum);
}

bool eeprom_bank_version(enum EEPROM_BANK bank) {
    bool retVal = (eeprom_safe_read_byte((uint8_t *)&myeeprom[bank].eeprom.as_struct.memory.memory.eeprom_array[EEPROM_VERSION]) == EEPROM_VERSION_LOCAL);
    return retVal;
}

bool eeprom_bank_initialized(enum EEPROM_BANK bank) {
    bool retVal = eeprom_safe_read_byte((uint8_t *)&myeeprom[bank].initialized);
    return retVal;
}

uint16_t eeprom_safe_checksum(enum EEPROM_BANK bank) {
    int sz = sizeof(struct EEPROM_MEMORY_PVT);
    uint16_t checksum = 0;
    for (int i = 0; i < sz; i++) {
        uint8_t val = eeprom_safe_read_byte(&myeeprom[bank].eeprom.as_str.values[i]);
        checksum += val;
    }
    return checksum;
}

uint16_t eeprom_safe_checksum_flash(void) {
    int sz = sizeof(struct EEPROM_MEMORY_PVT);
    uint16_t checksum = 0;
    for (int i = 0; i < sz; i++) {
        uint8_t val = ram_data.eeprom.as_str.values[i];
        checksum += val;
    }
    return checksum;
}

bool eeprom_safe_check(enum EEPROM_BANK bank) {
    bool retVal = false;
    if ((eeprom_bank_initialized(bank) != 0) && eeprom_bank_version(bank)) {
        if (eeprom_safe_checksum(bank) == eeprom_bank_checksum(bank)) {
            eeprom_safe_read_block(&ram_data, &myeeprom[bank], sizeof(struct EEPROM_MEMORY_INITIALIZED));
            if (eeprom_safe_checksum_flash() == ram_data.checksum) {
                retVal = true;
            }
        } else {
        }
    } else {
    }
    return retVal;
}

uint16_t eeprom_safe_calculate_crc(void) {
    uint16_t crc = 0;

    for (uint8_t i = 0; i < sizeof(struct EEPROM_MEMORY_PVT); i++) {
        crc += ram_data.eeprom.as_str.values[i];
    }
    return crc;
}

void eeprom_safe_flash_to_eeprom(void) {
    memcpy_P(&ram_data.eeprom, &eeprom_in_flash, sizeof(union EEPROM_UNION));
    ram_data.eeprom.as_struct.memory.memory.eeprom_array[EEPROM_VERSION] = EEPROM_VERSION_LOCAL;
    ram_data.initialized = true;
    eeprom_memory_flush();
}

void eeprom_safe_factory_settings(void) {
    LOG_DEF_NORMAL_P(PSTR("Factory\r\n"));
    ram_data.initialized = false;
   uint8_t selected_veichle = eeprom_safe_read_byte_cached(VEICHLE_SELECTED);
    eeprom_safe_flash_to_eeprom();
    ram_data.eeprom.as_struct.memory.memory.eeprom_array[VEICHLE_SELECTED] = selected_veichle;
    eeprom_memory_flush();
    vTaskDelay(300 / portTICK_PERIOD_MS);
    software_reset();
}

uint8_t eeprom_safe_read_byte_cached(uint8_t pos) {
    return ram_data.eeprom.as_struct.memory.memory.eeprom_array[pos];
}

void eeprom_init(void) {
    eeprom_hw_init();
    bool eeprom_ok = false;
    do {
        eeprom_ok = eeprom_safe_check(EEPROM_BANK_0);
        if (eeprom_ok) {
            _delay_ms(100);
            active_eeprom_bank = EEPROM_BANK_0;
            LOG_DEF_NORMAL_P(PSTR("Bank 0 OK\r\n"));
        } else {
            _delay_ms(100);
            eeprom_ok = eeprom_safe_check(EEPROM_BANK_1);
            if (eeprom_ok) {
                active_eeprom_bank = EEPROM_BANK_1;
                LOG_DEF_NORMAL_P(PSTR("Bank 1 OK\r\n"));
            } else {
                _delay_ms(100);
                eeprom_ok = eeprom_safe_check(EEPROM_BANK_2);
                if (eeprom_ok) {
                    active_eeprom_bank = EEPROM_BANK_2;
                    LOG_DEF_NORMAL_P(PSTR("Bank 2 OK\r\n"));
                } else {
                    _delay_ms(100);
                    LOG_DEF_NORMAL_P(PSTR("No EEPROM BANK USABLE\r\n"));
                    eeprom_safe_flash_to_eeprom();
                }
            }
        }
    } while (!eeprom_ok);
}