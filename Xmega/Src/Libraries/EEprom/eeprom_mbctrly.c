#include <string.h>
#include "eeprom.h"
#include "eeprom_priv.h"
#include "logger.h"
#include "event_list.h"

extern struct EEPROM_MEMORY_INITIALIZED ram_data;

void eeprom_safe_update_leds(uint8_t index, uint8_t red, uint8_t green, uint8_t blue, uint8_t white){
    if (index < LED_SETTING_NUM){
        ram_data.eeprom.as_struct.memory.led_setting[index].red = red;
        ram_data.eeprom.as_struct.memory.led_setting[index].green = green;
        ram_data.eeprom.as_struct.memory.led_setting[index].blue = blue;
        ram_data.eeprom.as_struct.memory.led_setting[index].white = white;
        eeprom_memory_flush();
    }
}

void transmit_active_led_setting(void){
    struct CONTROLLER_DATA data;
    data.command = CONTROLLER_RW_ACTIVE_CONF;
    data.data[0] = ram_data.eeprom.as_struct.memory.active_led_cfg;
    event_controller_emit(&data);
}

void eeprom_safe_update_active_led(uint8_t active_led){
    ram_data.eeprom.as_struct.memory.active_led_cfg = active_led;
    eeprom_memory_flush();
    transmit_active_led_setting();
}

bool eeprom_safe_get_leds(uint8_t index, uint8_t *red, uint8_t *green, uint8_t *blue, uint8_t *white){
    bool retVal = false;
    if (index < LED_SETTING_NUM){
        retVal = true;
        *red = ram_data.eeprom.as_struct.memory.led_setting[index].red;
        *green = ram_data.eeprom.as_struct.memory.led_setting[index].green;
        *blue = ram_data.eeprom.as_struct.memory.led_setting[index].blue;
        *white = ram_data.eeprom.as_struct.memory.led_setting[index].white;
    }
    return retVal;
}

uint8_t eeprom_safe_get_active_led_cfg(void){
    return ram_data.eeprom.as_struct.memory.active_led_cfg;
}

void eeprom_safe_update_byte(uint8_t pos, uint8_t val){
    if (pos < E2ARRAY){
        LOG_DEF_NORMAL_P(PSTR("Write to %d %02X\r\n"), pos, val);
        ram_data.eeprom.as_struct.memory.memory.eeprom_array[pos] = val;
        eeprom_memory_flush();
    }
}

void eeprom_safe_get_byte(uint32_t func_pointer, uint8_t pos){
    struct CONTROLLER_DATA data;
    LOG_DEF_NORMAL_P(PSTR("Rec e2 %d\r\n"), pos);
    data.command = CONTROLLER_RO_EEPROM;
    if (pos < E2ARRAY){
        data.data[0] = func_pointer;
        data.data[1] = func_pointer>>8;
        data.data[2] = func_pointer>>16;
        data.data[3] = func_pointer>>24;
        data.data[4] = pos;
        data.data[5] = ram_data.eeprom.as_struct.memory.memory.eeprom_array[pos];
        event_controller_emit(&data);
    }else{
        LOG_DEF_NORMAL_P(PSTR("Out of bounds\r\n"));
    }
}

void eeprom_safe_get_page(uint32_t func_pointer, uint8_t pos){
    struct CONTROLLER_DATA data;
    LOG_DEF_NORMAL_P(PSTR("Pag e2 %d\r\n"), pos);
    data.command = CONTROLLER_RO_EEPROM_PAGE;
    if (pos < E2ARRAY_PART){
        data.data[0] = func_pointer;
        data.data[1] = func_pointer>>8;
        data.data[2] = func_pointer>>16;
        data.data[3] = func_pointer>>24;
        data.data[4] = pos;
        data.data[5] = E2ARRAY_PART_LEN;
        memcpy(&data.data[6], ram_data.eeprom.as_struct.memory.memory.eeprom_data_bank[pos], sizeof(uint8_t)*E2ARRAY_PART_LEN);
        event_controller_emit(&data);
    }else{
        LOG_DEF_NORMAL_P(PSTR("Out of bounds\r\n"));
    }
}

