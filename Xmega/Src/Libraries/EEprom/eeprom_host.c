#include <string.h>
#include "eeprom.h"
#include "eeprom_priv.h"


void eeprom_hw_init(void){
}

uint8_t eeprom_safe_read_byte (const uint8_t *__p){
      return *__p;
}

uint16_t eeprom_safe_read_word (const uint16_t *__p){
    return *__p;
}

uint32_t eeprom_safe_read_dword (const uint32_t *__p){
    return *__p;
}

void eeprom_safe_write_byte(uint8_t *__p, uint8_t __value){
    *__p = __value;
}

void eeprom_safe_write_word(uint16_t *__p, uint16_t __value){
    *__p = __value;
}

void eeprom_safe_write_dword(uint32_t *__p, uint32_t __value){
    *__p = __value;
}

uint8_t eeprom_safe_read_flash(const uint16_t *__p){
    return *__p;
}

void eeprom_safe_write_block (const void *src, void *dst, size_t n){
    memcpy(dst, src, sizeof(n));
}

void eeprom_safe_update_block(const void *src, void *dst, size_t n){
    memcpy(dst, src, n);
}

void eeprom_safe_read_block(void *dst, const void *src, size_t n){
    memcpy(dst, src, n);
}


void software_reset(void){
}