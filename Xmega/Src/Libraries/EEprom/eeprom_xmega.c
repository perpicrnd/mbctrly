#include <avr/io.h>
#include "asf.h"
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "eeprom.h"
#include "eeprom_priv.h"




uint8_t eeprom_safe_read_byte (const uint8_t *p){
    return eeprom_read_byte(p);
}
uint16_t eeprom_safe_read_word (const uint16_t *p){
    return eeprom_read_word(p);
}
uint32_t eeprom_safe_read_dword (const uint32_t *p){
    return eeprom_read_dword(p);
}

void eeprom_safe_write_byte(uint8_t *p, uint8_t value){
    uint8_t sreg = SREG;
    cli();
    eeprom_write_byte(p, value);
    SREG = sreg;
}

void eeprom_safe_write_word(uint16_t *p, uint16_t value){
    uint8_t sreg = SREG;
    cli();
    eeprom_write_word(p, value);
    SREG = sreg;
}

void eeprom_safe_write_dword(uint32_t *p, uint32_t value){
    uint8_t sreg = SREG;
    cli();
    eeprom_write_dword(p, value);
    SREG = sreg;
}

uint8_t eeprom_safe_read_flash(const uint16_t *p){
    return pgm_read_byte(p);
}

void eeprom_safe_write_block (const void *src, void *dst, size_t n){
    uint8_t sreg = SREG;
    cli();
    eeprom_write_block(src, dst, n);
    SREG = sreg;
}

void eeprom_safe_update_block(const void *src, void *dst, size_t n){
    uint8_t sreg = SREG;
    cli();
    eeprom_update_block(src, dst, n);
    SREG = sreg;
}

void eeprom_safe_read_block(void *dst, const void *src, size_t n){
    eeprom_read_block(dst, src, n);
}

void eeprom_hw_init(void){
    

}

void software_reset(void){
    reset_do_soft_reset();
}