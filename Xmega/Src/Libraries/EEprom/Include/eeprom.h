#pragma once

#include <stdint.h>
#include <stdbool.h>

#define EEPROM_VERSION_LOCAL 0x01

#define LED_SETTING_NUM 13
#define E2ARRAY_PART 6
#define E2ARRAY_PART_LEN 8

#define BTN_CONFIGURATION 0
#define VEICHLE_SELECTED 1
#define EEPROM_VERSION 2

#define E2ARRAY (E2ARRAY_PART*E2ARRAY_PART_LEN)

enum BUTTON_CONFIGURATION{
    BUTTON_CONFIGURATION_CLEAR = -1,
    BUTTON_CONFIGURATION_EMPTY = 0,
    BUTTON_CONFIGURATION_VERTICAL_LEFT,
    BUTTON_CONFIGURATION_VERTICAL_RIGHT,
    BUTTON_CONFIGURATION_HORIZONTAL_LEFT,
    BUTTON_CONFIGURATION_HORIZONTAL_RIGHT,
};

struct LED_SETTING{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t white;
};

struct BUTTON_SETTING{
    uint8_t ton;
    uint8_t toff;
};

union EEPROM_MEMORY_ARRAY{
        uint8_t eeprom_data_bank[E2ARRAY_PART][E2ARRAY_PART_LEN];
        uint8_t eeprom_array[E2ARRAY];
    };

struct EEPROM_MEMORY{
    struct LED_SETTING led_setting[LED_SETTING_NUM];
    union EEPROM_MEMORY_ARRAY memory;
    uint8_t active_led_cfg;
};

void eeprom_init(void);
void eeprom_memory_flush(void);

void eeprom_safe_update_leds(uint8_t index, uint8_t red, uint8_t green, uint8_t blue, uint8_t white);
void eeprom_safe_update_active_led(uint8_t active_led);
bool eeprom_safe_get_leds(uint8_t index, uint8_t *red, uint8_t *green, uint8_t *blue, uint8_t *white);
uint8_t eeprom_safe_get_active_led_cfg(void);
uint8_t eeprom_safe_read_byte_cached(uint8_t pos);

void eeprom_safe_update_byte(uint8_t pos, uint8_t data);
void eeprom_safe_update_bank(uint8_t start, uint8_t len, uint8_t *buff);
void eeprom_safe_get_byte(uint32_t func_addr, uint8_t bank);
void eeprom_safe_get_page(uint32_t func_pointer, uint8_t pos);

void transmit_active_led_setting(void);
void eeprom_safe_factory_settings(void);
void software_reset(void);
void eeprom_safe_write_word(uint16_t *p, uint16_t value);