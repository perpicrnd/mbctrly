cmake_minimum_required(VERSION 3.0)

SET(LIBRARY_NAME eeprom)
PROJECT(${LIBRARY_NAME} VERSION 0.1 LANGUAGES C)

if (CMAKE_CROSSCOMPILING)
	set(SOURCES ${SOURCES} eeprom_xmega.c
	)
else()
    set(SOURCES ${SOURCES} eeprom_host.c
	)
    add_subdirectory(Test)
endif()	

set(SOURCES ${SOURCES}
    eeprom.c
    eeprom_mbctrly.c
)

add_library(${LIBRARY_NAME} ${SOURCES})
target_link_libraries(${LIBRARY_NAME} logger serial_parser)

if( CMAKE_CROSSCOMPILING)
	target_link_libraries(${LIBRARY_NAME} asf)
else()
    target_link_libraries (${LIBRARY_NAME})
endif()

        
target_include_directories(${LIBRARY_NAME}  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Include>
    $<INSTALL_INTERFACE:include/>
    PRIVATE .)
