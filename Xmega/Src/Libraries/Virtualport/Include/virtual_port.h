
#include <stdint.h>

typedef uint8_t ioport_mode_t;
typedef uint8_t ioport_pin_t;
typedef uint8_t ioport_port_t;
typedef uint8_t ioport_port_mask_t;

#if __amd64__
enum ioport_direction{
    IOPORT_DIR_INPUT,
    IOPORT_DIR_OUTPUT,
};
#else
#include "asf.h"
#endif

typedef struct{
	ioport_pin_t a;
	ioport_pin_t b;
	ioport_pin_t c;
	ioport_pin_t d;
	ioport_pin_t e;
	ioport_pin_t f;
	ioport_pin_t g;
	ioport_pin_t h;
}VIRTUALPORT;

void virtual_ioport_set_port_mode(VIRTUALPORT *port, ioport_port_mask_t mask, ioport_mode_t mode);
void virtual_ioport_set_port_dir(VIRTUALPORT *port, ioport_port_mask_t mask, enum ioport_direction dir);
ioport_port_mask_t virtual_ioport_get_port_level(VIRTUALPORT *port, ioport_port_mask_t mask);


