
#include "asf.h"
#include "virtual_port.h"

void virtual_ioport_set_port_mode(VIRTUALPORT *port, ioport_port_mask_t mask, ioport_mode_t mode){
	if ((mask & 0x80) == 0x80){
		ioport_set_pin_mode(port->h, mode);
	}
	if ((mask & 0x40) == 0x40){
		ioport_set_pin_mode(port->g, mode);
	}
	if ((mask & 0x20) == 0x20){
		ioport_set_pin_mode(port->f, mode);
	}
	if ((mask & 0x10) == 0x10){
		ioport_set_pin_mode(port->e, mode);
	}
	if ((mask & 0x08) == 0x08){
		ioport_set_pin_mode(port->d, mode);
	}
	if ((mask & 0x04) == 0x04){
		ioport_set_pin_mode(port->c, mode);
	}
	if ((mask & 0x02) == 0x02){
		ioport_set_pin_mode(port->b, mode);
	}
	if ((mask & 0x01) == 0x01){
		ioport_set_pin_mode(port->a, mode);
	}
}

void virtual_ioport_set_port_dir(VIRTUALPORT *port, ioport_port_mask_t mask, enum ioport_direction dir){
	if ((mask & 0x80) == 0x80){
		ioport_set_pin_dir(port->h, dir);
	}
	if ((mask & 0x40) == 0x40){
		ioport_set_pin_dir(port->g, dir);
	}
	if ((mask & 0x20) == 0x20){
		ioport_set_pin_dir(port->f, dir);
	}
	if ((mask & 0x10) == 0x10){
		ioport_set_pin_dir(port->e, dir);
	}
	if ((mask & 0x08) == 0x08){
		ioport_set_pin_dir(port->d, dir);
	}
	if ((mask & 0x04) == 0x04){
		ioport_set_pin_dir(port->c, dir);
	}
	if ((mask & 0x02) == 0x02){
		ioport_set_pin_dir(port->b, dir);
	}
	if ((mask & 0x01) == 0x01){
		ioport_set_pin_dir(port->a, dir);
	}
}


ioport_port_mask_t virtual_ioport_get_port_level(VIRTUALPORT *port, ioport_port_mask_t mask){
	ioport_port_mask_t retVal = 0;
	if ((mask & 0x80) == 0x80){
		retVal |= ioport_get_pin_level(port->h);
	}
	retVal = retVal<< 1;
	if ((mask & 0x40) == 0x40){
		retVal |= ioport_get_pin_level(port->g);
	}
	retVal = retVal<< 1;
	if ((mask & 0x20) == 0x20){
		retVal |= ioport_get_pin_level(port->f);
	}
	retVal = retVal<< 1;
	if ((mask & 0x10) == 0x10){
		retVal |= ioport_get_pin_level(port->e);
	}
	retVal = retVal<< 1;
	if ((mask & 0x08) == 0x08){
		retVal |= ioport_get_pin_level(port->d);
	}
	retVal = retVal<< 1;
	if ((mask & 0x04) == 0x04){
		retVal |= ioport_get_pin_level(port->c);
	}
	retVal = retVal<< 1;
	if ((mask & 0x02) == 0x02){
		retVal |= ioport_get_pin_level(port->b);
	}
	retVal = retVal<< 1;
	if ((mask & 0x01) == 0x01){
		retVal |= ioport_get_pin_level(port->a);
	}
	return retVal & mask;
}


