#pragma once

#include <stdint.h>
#include <stdbool.h>

struct IR_TIMING_STRUCT{
	/// \ref IR_STATUS della transizione sul pin.
	bool status;
	/// tempo passato dall'ultima transizione.
	uint16_t counter;
    uint16_t bigcounter;
};

struct EVENT_IR_IN_VALUE{
    struct EVENT_GENERIC generic;
    struct IR_TIMING_STRUCT ir_timing;
};

