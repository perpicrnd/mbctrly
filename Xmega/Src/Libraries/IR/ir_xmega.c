#include <avr/pgmspace.h>
#include "asf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "event_list.h"
#include "ir.h"
#include "ir_priv.h"
#include "logger.h"

extern volatile bool lock_interrupt;
bool ir_running = false;
static int8_t pos = 0;

uint64_t ir_timing = 0;
uint64_t old_ir_timing = 0;
static const char power_off[] PROGMEM = "10011110101100010110111110010000";

static inline __attribute__((always_inline)) char get_ir_code_n(uint8_t pos){
    char retVal = 0xFF;
    if (pos < 32){
        retVal = pgm_read_byte(&power_off[pos]);
    }
    return retVal;
}
static uint8_t second_counter = 0;
static void handle_transmission(const union EVENT * const event){
    if (event->as_generic.event == eEVENT_SECONDS_CHANGED){
        ir_running = true;
        if (second_counter++ >= 2){
            if (lock_interrupt == true){
                LOG_DEF_NORMAL_P(PSTR("Invio\r\n"));
                struct CONTROLLER_DATA data;
                data.command = CONTROLLER_WO_POWER;
                lock_interrupt = false;
                data.data[0] = lock_interrupt;
                event_controller_emit(&data);
            }
            lock_interrupt = false;
        }
    }        
}

static void handle_ir(const union EVENT * const event){
    ir_timing = ((uint64_t)event->as_ir_in_value.ir_timing.bigcounter) * (configCPU_CLOCK_HZ / configTICK_RATE_HZ / 64-1);
    ir_timing += event->as_ir_in_value.ir_timing.counter;
    uint32_t delta = ir_timing - old_ir_timing;
    if (delta > 10000){
        pos = -1;
        lock_interrupt = false;
    }else if (delta > 4000){
        pos = 0;
        lock_interrupt = false;
    }else if(delta > 800 ){
        if (get_ir_code_n(pos) == '0'){
            pos++;
        }else{
            pos = -1;
        }
    }else{
        if (get_ir_code_n(pos) == '1'){
            pos++;
        }else{
            pos = -1;
        }
    }
    old_ir_timing = ir_timing;
    if (pos > 24){
        if (lock_interrupt == false){
            lock_interrupt = true;
            second_counter = 0;
            struct CONTROLLER_DATA data;
            data.command = CONTROLLER_WO_POWER;
            data.data[0] = lock_interrupt;
            event_controller_emit(&data);
        }
    }
}


void ir_hw_init(void){
    ioport_set_pin_dir(REMOCON_OUT, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(Remo_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Remo_In, IOPORT_MODE_PULLUP);

    PORTD.INTMASK |= PIN0_bm;
    PORTD.INTCTRL = (PORTD.INTCTRL & ~PORT_INTLVL_gm ) | PORT_INTLVL_MED_gc;
    PORTD.PIN0CTRL |= PORT_ISC_BOTHEDGES_gc;
    event_connect_callback(eEVENT_IR_IN_VALUE, handle_ir);
    event_connect_callback(eEVENT_SECONDS_CHANGED, handle_transmission);
}