#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include "FreeRTOS.h"
#include "task.h"
#include "adc_backend.h"
#include "main.h"
#include "asf.h"
#include "logger.h"
#include "event_list.h"

#define WDT_RESET_CONT_DETECT 0x03
#define EEPROM_MAX_WRITES 90000L

#if !defined(PRINT_RAM_USAGE)
static enum CPU_STATUS sleep_status __attribute__ ((section (".noinit")));
#endif
static uint16_t wdt_reset_continuous __attribute__ ((section (".noinit")));
static uint32_t reset_cause_list[8] EEMEM = {0};
static uint32_t reset_wdt_continuous EEMEM = {0};

uint8_t reset_status = 0;

void save_reset_cause(void){
    reset_status = RST.STATUS;
	if ((RST.STATUS & RST_SRF_bm) != 0){
		uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_SRF_bp])+1;
		if (cur < EEPROM_MAX_WRITES){
			eeprom_write_dword(&reset_cause_list[RST_SRF_bp], cur);
		}
		RST.STATUS |= RST_SRF_bm;
		wdt_reset_continuous = 0;
	}
	if ((RST.STATUS & RST_PDIRF_bm) != 0){
		uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_PDIRF_bp])+1;
		if (cur < EEPROM_MAX_WRITES){
			eeprom_write_dword(&reset_cause_list[RST_PDIRF_bp], cur);
		}
		RST.STATUS |= RST_PDIRF_bm;
		wdt_reset_continuous = 0;
	}
	if ((RST.STATUS & RST_WDRF_bm) != 0){
		if (wdt_reset_continuous == 0){
			uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_WDRF_bp])+1;
			if (cur < EEPROM_MAX_WRITES){
				eeprom_write_dword(&reset_cause_list[RST_WDRF_bp], cur);
			}
			RST.STATUS |= RST_WDRF_bm;
		}else if(wdt_reset_continuous == WDT_RESET_CONT_DETECT){
			uint32_t cur = eeprom_read_dword(&reset_wdt_continuous)+1;
			if (cur < EEPROM_MAX_WRITES){
				eeprom_write_dword(&reset_wdt_continuous, cur);
			}
		}
		if (wdt_reset_continuous <= WDT_RESET_CONT_DETECT){
			wdt_reset_continuous++;
		}
	}
	if ((RST.STATUS & RST_BORF_bm) != 0){
		uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_BORF_bp])+1;
		if (cur < EEPROM_MAX_WRITES){
			eeprom_write_dword(&reset_cause_list[RST_BORF_bp], cur);
		}
		RST.STATUS |= RST_BORF_bm;
		wdt_reset_continuous = 0;
	}
	if ((RST.STATUS & RST_EXTRF_bm) != 0){
		uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_EXTRF_bp])+1;
		if (cur < EEPROM_MAX_WRITES){
			eeprom_write_dword(&reset_cause_list[RST_EXTRF_bp], cur);
		}
		RST.STATUS |= RST_EXTRF_bm;
		wdt_reset_continuous = 0;
	}
	if ((RST.STATUS & RST_PORF_bm) != 0){
		uint32_t cur = eeprom_read_dword(&reset_cause_list[RST_PORF_bp])+1;
		if (cur < EEPROM_MAX_WRITES){
			eeprom_write_dword(&reset_cause_list[RST_PORF_bp], cur);
		}
		RST.STATUS |= RST_PORF_bm;
		wdt_reset_continuous = 0;
	}

}

#if defined(PRINT_RAM_USAGE)
__attribute__((naked,section(".init3"))) void flood_RAM(void) {
	register uint16_t i;
	for (i=RAMSTART; i <= RAMEND; i++) {
		if ((uint16_t *)i == &wdt_reset_continuous){
			//Don't write on the noinit flag.
			continue;
		}
		*(uint8_t *)i = 0x33;
	}
}

void timer_ram_usage(const union EVENT * const event)
{
	(void)event;
	uint16_t uA5 = 0;
	uint16_t u33 = 0;
	for(uint16_t i=RAMSTART; i<= RAMEND; i++){
		if (*(uint8_t *)i == 0xA5){
			uA5++;
		}
		if (*(uint8_t *)i == 0x33){
			u33++;
		}
	}
	LOG_DEF_NORMAL_P(PSTR("Rtos: %d ram %d heap %d\r\n"), uA5, u33, xPortGetFreeHeapSize());
}
#warning "Sleep mode disabled."
void sleep_if_needed(void){
    return;
}
#else
__attribute__((naked,section(".init3"))) void early_wake_sam(void) {
    PORTD.OUTCLR |= 0x20;
    PORTD.DIRSET |= 0x20;
    PORTD.OUTCLR |= 0x10;
    PORTD.DIRSET |= 0x10;
}

enum CPU_STATUS{
    CPU_STATUS_UNKNOWN = 0,
    CPU_STATUS_SLEEPING = 0xA5A5,
    CPU_STATUS_RUNNING = 0x5A5A,
};


void sleep_if_needed(void){
    if (sleep_status == CPU_STATUS_SLEEPING){
        //Enter low power state.

    }
    return;
}

void request_sleep_mode(void){
    sleep_status = CPU_STATUS_SLEEPING;
    ccp_write_io((void *)&RST.CTRL, RST_SWRST_bm);
}
#endif

/**
 * \internal
 * \brief ISR for channel 0 on ADC A
 *
 * Calls the callback function that has been set for the ADC when the channel's
 * interrupt flag is set, if its interrupt has been enabled.
 */
extern volatile uint16_t adc_res[8];
ISR(ADCA_CH0_vect)
{
    uint16_t result = adc_get_result(&ADCA, ADC_CH0);
	if (result > 0x7FF){
		result = 0;
	}    
    if ((ADCA.CH0.CTRL & 0x03)==0){
        //Sto leggendo il bandgap
        adc_res[5] = result;
        ADCA.CH0.SCAN = 0x03;
        ADCA.CH0.CTRL |= 0x1E;
        ADCA.CH0.MUXCTRL &= ~0x78;
        ADCA.CH0.MUXCTRL |= 0x10;
        adc_disable(&ADCA);
    }else{
        adc_res[(ADCA.CH0.SCAN >> 4)] = result;
        if ((ADCA.CH0.SCAN >> 4)== (ADCA.CH0.SCAN & 0x0F)){
            //Letto i 3 canali.Passo a leggere la tensione di alimentazione.
            ADCA.CH0.SCAN = 0;
            ADCA.CH0.MUXCTRL &= ~0x78;
            ADCA.CH0.MUXCTRL |= 0x08;
            ADCA.CH0.CTRL &= 0xFC;
        }
    }
#if !defined(PRINT_RAM_USAGE)
    if (sleep_status == CPU_STATUS_SLEEPING){

    }else{
#else
    {
#endif
        
    }
}

extern volatile uint16_t cca;
extern volatile uint16_t ccb;
static uint32_t tmp_cca;
extern volatile bool overflow_off;
extern volatile bool overflow_on;
extern volatile bool rot_x_changed;

extern bool system_running;
extern bool ir_running;
static volatile bool wake_up_request = false;
uint8_t isr_counter = 0;
union EVENT ir_event;
            
volatile bool lock_interrupt = false;

ISR(PORTC_INT_vect)
{
    PORTC.INTFLAGS |= 0xFF;
}

ISR(PORTA_INT_vect)
{
    uint8_t flags = PORTA.INTFLAGS;
    if ((flags & PIN3_bm) == PIN3_bm){
        PORTA.PIN3CTRL = (PORTA.PIN3CTRL & PORT_ISC_INPUT_DISABLE_gc);
        PORTA.INTFLAGS |= PIN3_bm;
        wake_up_request = true;
    }
    if ((flags & PIN4_bm) == PIN4_bm){
        PORTA.PIN4CTRL = (PORTA.PIN4CTRL & PORT_ISC_INPUT_DISABLE_gc);
        PORTA.INTFLAGS |= PIN4_bm;
        wake_up_request = true;
    }
    if ((flags & PIN5_bm) == PIN5_bm){
        PORTA.INTFLAGS |= PIN5_bm;
        rot_x_changed = true;
    }
    if ((flags & PIN6_bm) == PIN6_bm){
        PORTA.INTFLAGS |= PIN6_bm;
        rot_x_changed = true;
    }
}
extern volatile uint32_t user_systick;
ISR(PORTD_INT_vect)
{
    uint8_t flags = PORTD.INTFLAGS;
    if ((flags & PIN0_bm) == PIN0_bm){
        if (ir_running){
            bool pin_status = ioport_get_pin_level(Remo_In);
            if (!lock_interrupt){
                ioport_set_pin_level(REMOCON_OUT, pin_status);
            }
            if (!pin_status){
                ir_event.as_generic.event = eEVENT_IR_IN_VALUE;    
                ir_event.as_ir_in_value.ir_timing.bigcounter = user_systick; 
                ir_event.as_ir_in_value.ir_timing.counter = TCC5.CNT;
                BaseType_t xHigherPriorityTaskWoken = pdFALSE;
                event_emit_fromISR(&ir_event, &xHigherPriorityTaskWoken);
                if (xHigherPriorityTaskWoken){
                    taskYIELD();
                }
            }
        }
        PORTD.INTFLAGS |= PIN0_bm;
        wake_up_request = true;
    }
	if ((flags & PIN2_bm) == PIN2_bm){
        if (system_running){
            if ((PORTD.IN & PIN2_bm) != PIN2_bm){
                ccb = tc45_read_count(&TCD5);
                cca = tmp_cca;
                TCD5.CTRLGSET = TC45_CMD_RESTART_gc;
                overflow_off = false;
                overflow_on = false;
            }else{
                tmp_cca = tc45_read_count(&TCD5);
            }
        }
        PORTD.INTFLAGS |= PIN2_bm;
        wake_up_request = true;
	}
    if ((flags & PIN3_bm) == PIN3_bm ){
        PORTD.PIN3CTRL = (PORTD.PIN3CTRL & PORT_ISC_INPUT_DISABLE_gc);
        PORTD.INTFLAGS |= PIN3_bm;
        wake_up_request = true;
    }
   
}

void init_board_low_power(void){
    ioport_set_pin_dir(Remo_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Remo_In, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Ign_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ign_In, IOPORT_MODE_PULLUP);

    ioport_set_pin_level(Vcc_33_Disch, false);
    ioport_set_pin_dir(Vcc_33_Disch, IOPORT_DIR_OUTPUT);

    ioport_set_pin_dir(Not_Enable_Sam, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(Not_Enable_Sam, false);

    ioport_set_pin_dir(Ext_Alarm, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Ext_Alarm, IOPORT_MODE_PULLUP);

    ioport_set_pin_level(Pwm_R, false);
    ioport_set_pin_dir(Pwm_R, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(Pwm_G, false);
    ioport_set_pin_dir(Pwm_G, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(Pwm_B, false);
    ioport_set_pin_dir(Pwm_B, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(Pwm_W, false);
    ioport_set_pin_dir(Pwm_W, IOPORT_DIR_OUTPUT);

    ioport_set_pin_level(REMOCON_OUT, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(REMOCON_OUT, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(Pwm_In, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Pwm_In, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(ROT_X_A, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(ROT_X_A, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(ROT_X_B, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(ROT_X_B, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(An_In_1, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(An_In_2, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(An_In_3, IOPORT_DIR_INPUT);
    
    ioport_set_pin_dir(AREF0_PIN, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(AREF_NEG, IOPORT_DIR_INPUT);
    ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTA, 7), IOPORT_DIR_INPUT);

    ioport_set_pin_dir(Rxd1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Rxd1, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(Txd1, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(Txd1, IOPORT_MODE_PULLUP);

    ioport_set_pin_dir(M_Tx, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(M_Tx, IOPORT_MODE_PULLUP);
    ioport_set_pin_dir(M_Rx, IOPORT_DIR_INPUT);
    ioport_set_pin_mode(M_Rx, IOPORT_MODE_PULLUP);

    _delay_ms(10);
    sleep_enable();


    wake_up_request = false;
    PORTD.INTMASK |= PIN0_bm | PIN2_bm | PIN3_bm; // if we have more pins , OR them in here
    PORTD.INTCTRL = (PORTD.INTCTRL & ~PORT_INTLVL_gm ) | PORT_INTLVL_LO_gc;
    PORTD.PIN0CTRL |= PORT_ISC_BOTHEDGES_gc;
    PORTD.PIN2CTRL |= PORT_ISC_FALLING_gc;
    PORTD.PIN3CTRL |= PORT_ISC_BOTHEDGES_gc;

    PORTA.INTMASK |= PIN3_bm | PIN4_bm;
    PORTA.INTCTRL = (PORTA.INTCTRL & ~PORT_INTLVL_gm) | PORT_INTLVL_LO_gc;
    PORTA.PIN3CTRL |= PORT_ISC_BOTHEDGES_gc;
    PORTA.PIN4CTRL |= PORT_ISC_BOTHEDGES_gc;
    
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    do{
        if (ioport_get_pin_level(Ign_In) == false){
            wake_up_request = true;
        }
        sei();
        if (wake_up_request){
            break;
        }else{
            sleep_cpu();
        }
    }while(wake_up_request == false);
    cli();
    
    ioport_set_pin_dir(Rxd1, IOPORT_DIR_OUTPUT);
    for(uint8_t i=0; i<40; i++){
        ioport_toggle_pin_level(Rxd1);
        _delay_us(20);
    }
    ioport_set_pin_level(Rxd1, true);
}

void init_board(void){
	pmic_init();
    init_board_low_power();
    sleep_if_needed();
	cpu_irq_enable();
	ioport_init();
	sysclk_init();
    save_reset_cause();
    PORTD.INTMASK &= ~(PIN0_bm | PIN2_bm | PIN3_bm); // if we have more pins , OR them in here
    PORTD.INTCTRL = (PORTD.INTCTRL & ~PORT_INTLVL_gm ) | PORT_INTLVL_LO_gc;
    PORTD.PIN0CTRL |= PORT_ISC_BOTHEDGES_gc;
    PORTD.PIN2CTRL |= PORT_ISC_FALLING_gc;
    PORTD.PIN3CTRL |= PORT_ISC_BOTHEDGES_gc;

    PORTA.INTMASK &= ~(PIN3_bm | PIN4_bm);
    PORTA.INTMASK |= PIN5_bm | PIN6_bm;
    PORTA.INTCTRL = (PORTA.INTCTRL & ~PORT_INTLVL_gm) | PORT_INTLVL_LO_gc;
    PORTA.PIN5CTRL |= PORT_ISC_BOTHEDGES_gc;
    PORTA.PIN6CTRL |= PORT_ISC_BOTHEDGES_gc;

}

void init_board_running(void){
    
}