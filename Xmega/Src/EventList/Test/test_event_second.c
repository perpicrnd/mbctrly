#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "event_list.c"

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

void vApplicationIdleHook(void){
//Make the linker happy.
}

UBaseType_t __wrap_uxQueueSpacesAvailable( const QueueHandle_t xQueue ){
    (void)xQueue;
    return 10;
}

bool function_xQueueGenericSend_called = false;
BaseType_t __wrap_xQueueGenericSend( QueueHandle_t xQueue, const void * const pvItemToQueue, TickType_t xTicksToWait, const BaseType_t xCopyPosition ){
	(void)xCopyPosition;
	(void)xTicksToWait;
	(void)pvItemToQueue;
	assert_ptr_equal(xQueue, task_queue);
	function_xQueueGenericSend_called = true;
	return pdTRUE;
}

static void test_vTimerSecond(){
	//vTimerSecond genera un evento sul sistema. Lo vedo dal wrap assert sopra.
	vTimerSecond(NULL);
	assert_true(function_xQueueGenericSend_called);

}



static int test_setup(void **state){
	(void)state;
	function_xQueueGenericSend_called = false;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();

	const struct CMUnitTest tests[] = {
	  cmocka_unit_test_setup(test_vTimerSecond, test_setup),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
