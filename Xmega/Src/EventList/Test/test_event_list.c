#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "event_list.c"

void vApplicationIdleHook(void){
//Make the linker happy.
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

static bool function_dummy_handler_called = false;
static bool function_dummy_handler2_called = false;
static void dummy_handler(const union EVENT * const msg){
	(void)msg;
	function_dummy_handler_called = true;
}

static void dummy_handler2(const union EVENT * const msg){
	(void)msg;
	function_dummy_handler2_called = true;
}

void __wrap_event_emit(union EVENT *event){
	event_task_loop(event);
}

void test_event_handler_item_next()
{
	//Due elementi per poterli accodare l'uno all'altro.
	EVENT_HANDLER_ITEM *item = event_handler_item_new();
	EVENT_HANDLER_ITEM *next = event_handler_item_new();

	//Gli elementi non sono collegati
	assert_false(event_handler_item_has_next(item));
	assert_false(event_handler_item_has_next(next));
	assert_false(event_handler_item_is_valid(item));
	assert_false(event_handler_item_is_valid(next));
	assert_null(event_handler_item_next(item));
	assert_null(event_handler_item_next(next));



	//Collego manualmente gli elementi
	item->next = next;
	assert_true(event_handler_item_has_next(item));
	assert_ptr_equal(event_handler_item_next(item), next);

	//Non ho ancora messo un handler, verifichiamo che venga aggiunto correttamente
	assert_false(event_handler_item_is_valid(item));
	event_add_handler(item, dummy_handler);
	assert_true(event_handler_item_is_valid(item));


	free(item);
	free(next);

}
void test_event_add_handler()
{
	//Un elemento su cui operare
	EVENT_HANDLER_ITEM *item = event_handler_item_new();

	//Non è inizializzato
	assert_false(event_handler_item_has_next(item));
	assert_null(event_handler_item_next(item));

	//Aggiungo l'elemento
	assert_true(event_add_handler(item, dummy_handler));
	//deve essere valido
	assert_true(event_handler_item_is_valid(item));
	//ma non avere nulla dopo.
	assert_null(event_handler_item_next(item));

	//Chiamo l'handler e verifico che sia chiamato.
	event_handler(item, NULL);
	assert_true(function_dummy_handler_called);
	assert_false(function_dummy_handler2_called);

	//Se lo riaggiungo
	assert_true(event_add_handler(item, dummy_handler));
	//rimane valido
	assert_true(event_handler_item_is_valid(item));
	//ma non è stato aggiunto.
	assert_null(event_handler_item_next(item));

	//Se aggiungo un handler differente
	assert_true(event_add_handler(item, dummy_handler2));
	//rimane valido
	assert_true(event_handler_item_is_valid(item));
	//ed ha un successivo.
	assert_non_null(event_handler_item_next(item));

	//Richiamo l'handler e verifico che siano entrambi chiamati.
	event_handler(item, NULL);
	assert_true(function_dummy_handler_called);
	assert_true(function_dummy_handler2_called);
}


int setup_test(void **state){
	(void)state;
	function_dummy_handler_called = false;
	function_dummy_handler2_called = false;
	return 0;
}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
	  cmocka_unit_test_setup(test_event_handler_item_next, setup_test),
	  cmocka_unit_test_setup(test_event_add_handler, setup_test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
