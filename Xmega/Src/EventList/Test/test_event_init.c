#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "event_list.c"

void vApplicationIdleHook(void){
//Make the linker happy.
}

int NumLoops = 50;
const char interface_code[] = "TEST SOFTWARE";

uint8_t accessory_init_called = 0;
void __wrap_accessory_init(void){
	function_called();
}


BaseType_t __wrap_xTaskCreate(	TaskFunction_t pxTaskCode,
							const char * const pcName,
							const uint16_t usStackDepth,
							void * const pvParameters,
							UBaseType_t uxPriority,
							TaskHandle_t * const pxCreatedTask ){
	(void) pxTaskCode;
	(void) pcName;
	(void) usStackDepth;
	(void) pvParameters;
	(void) uxPriority;
	(void) pxCreatedTask;
	function_called();
	return pdTRUE;
}

void test_event_init(){
	assert_int_equal(accessory_init_called, 0);
	expect_function_call(__wrap_xTaskCreate);
	event_init();
	//Must execute initialization once.
	event_init();

}

int main(void)
{
	//Make coverage happy
	vApplicationIdleHook();
	const struct CMUnitTest tests[] = {
	  cmocka_unit_test(test_event_init),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
