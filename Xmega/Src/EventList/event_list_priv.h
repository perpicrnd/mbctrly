#pragma once

/**
 * @brief effettua un loop dell'event task per processare eventi pendenti.
 * @param event l'evento da processare
 */
void event_task_loop(const union EVENT * const event);