
#include "eeprom.h"

void check_default_button_configuration(void){
    if (eeprom_safe_read_byte_cached(0) == 0xFF){
        eeprom_safe_update_byte(0, BUTTON_CONFIGURATION_HORIZONTAL_RIGHT);
    }
    if (eeprom_safe_read_byte_cached(EEPROM_VERSION) != EEPROM_VERSION_LOCAL){
        eeprom_safe_update_byte(EEPROM_VERSION, EEPROM_VERSION_LOCAL);
    }
}