
MACRO(ADD_UNIT_TEST TEST_NAME)
	add_executable(${TEST_NAME} ${TEST_NAME}.c)
	add_dependencies(${TEST_NAME} ${TEST_NAME})
	add_test(${TEST_NAME} ${TEST_NAME})
	foreach(loop_var ${ARGN})
		target_link_libraries(${TEST_NAME} ${loop_var})
	endforeach()
	target_link_libraries(${TEST_NAME} ${CMOCKA_LIBRARIES})
	target_include_directories(${TEST_NAME} PUBLIC "/usr/include")
	
ENDMACRO() 
