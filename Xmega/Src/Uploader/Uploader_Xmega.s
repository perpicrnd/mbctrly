/*
 * AssemblerApplication1.asm
 *
 *  Created: 17/06/2016 19.32.40
 *   Author: Buzzanca
 */ 
# include "gas.h"
# include <avr/io.h>
#include "Mem_Def.inc"

;==================================================
;==  Vectors interrupt boot prog                 ==
;==================================================
		DATA_SECTION(.code)
	.ORG	RESET_VECT_BOOT
BootVect:	jmp	Start_Prog_Boot	 ;Reset
	jmp Start_Prog_Boot  ; Oscillator Failure Interrupt (NMI)
	jmp Start_Prog_Boot  ; PORTR interrupt vectors - External Interrupt
	jmp Start_Prog_Boot  ; EDMA Channel 0 Interrupt
	jmp Start_Prog_Boot  ; EDMA Channel 1 Interrupt
	jmp Start_Prog_Boot  ; EDMA Channel 2 Interrupt
	jmp Start_Prog_Boot  ; EDMA Channel 3 Interrupt
	jmp Start_Prog_Boot  ; Overflow Interrupt
	jmp Start_Prog_Boot  ; Compare Interrupt
	jmp Start_Prog_Boot  ; External Interrupt
	jmp Start_Prog_Boot  ; TWI Slave Interrupt
	jmp Start_Prog_Boot  ; TWI Master Interrupt						
		jmp	TCC4_Overflow		; Overflow TCC4 interrupt vectors
	jmp Start_Prog_Boot  ; Error Interrupt
	jmp Start_Prog_Boot  ; Channel A Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Channel B Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Channel C Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Channel D Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Overflow Interrupt
	jmp Start_Prog_Boot  ; Error Interrupt
	jmp Start_Prog_Boot  ; Channel A Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Channel B Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; SPI Interrupt
		jmp	USARTC0_RXC_ISR ; USARTC0 - Reception Complete Interrupt
	jmp Start_Prog_Boot  ; Data Register Empty Interrupt
	jmp Start_Prog_Boot  ; Transmission Complete Interrupt
	jmp Start_Prog_Boot  ; EE Interrupt
	jmp Start_Prog_Boot  ; SPM Interrupt
	jmp Start_Prog_Boot  ; Timer/Counter Underflow Interrupt
	jmp Start_Prog_Boot  ; Timer/Counter Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; External Interrupt
	jmp Start_Prog_Boot  ; AC0 Interrupt
	jmp Start_Prog_Boot  ; AC1 Interrupt
	jmp Start_Prog_Boot  ; ACW Window Mode Interrupt
	jmp Start_Prog_Boot  ; ADC Channel Interrupt
	jmp Start_Prog_Boot  ; External Interrupt
	jmp Start_Prog_Boot  ; Overflow Interrupt
	jmp Start_Prog_Boot  ; Error Interrupt
	jmp Start_Prog_Boot  ; Channel A Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Channel B Compare or Capture Interrupt
	jmp Start_Prog_Boot  ; Reception Complete Interrupt
	jmp Start_Prog_Boot  ; Data Register Empty Interrupt
	jmp Start_Prog_Boot  ; Transmission Complete Interrupt

;==================================================
;==  Reset => Entry programma boot               ==
;==================================================
	.ORG	RESET_VECT_BOOT + INT_VECTORS_SIZE
Start_Prog_Boot:		
	ldi		Acc,0x80
	sts		PORTC_DIRSET,Acc
	ldi		Acc,PORT_OPC_PULLUP_gc
	sts		PORTC_PIN1CTRL,Acc
	sts		PORTC_PIN2CTRL,Acc
	sts		PORTC_PIN3CTRL,Acc
	sts		PORTC_PIN4CTRL,Acc
	sts		PORTC_PIN5CTRL,Acc
	ldi		Acc,0x0C				;pa3 e pa2 in output 
	sts		PORTA_DIRSET,Acc
	ldi		Acc,0x0C
	sts		PORTA_OUTCLR,Acc
	ldi		Acc,OSC_RC32MEN_bm		; Internal 32 MHz RC Oscillator Enable bit mask
	sts		OSC_CTRL,Acc
lp_osc_ready:
	lds		Acc,OSC_STATUS
	andi	Acc,OSC_RC32MEN_bm		; Internal 32 MHz RC Oscillator Ready
	breq	lp_osc_ready
	clr		Acc
	out     CPU_RAMPZ, Acc          // Reset bits 23:16 of Z	                					
	ldi		ZL,LOW (CLK_CTRL)		// Load addr into Z
	ldi		ZH,HIGH (CLK_CTRL)
	ldi     Acc, CCP_IOREG          // Load magic CCP value
	out     CPU_CCP, Acc            // Start CCP handshake
	ldi		Acc,CLK_SCLKSEL_RC32M_gc
	st      Z, Acc                  // Write value to I/O register
;------ Imposta vettori di interrupt nell'area boot  ------
	ldi		Par_1,PMIC_IVSEL_bm
	rcall	write_PMIC	
;------ Inizializzazione memoria  ------------------
	ldi		XH,0x20
	clr		XL
	clr		Acc
	clr		Fl_Stato
	ldi		Rstop,3
	clr		Rcount
lp_azz_mem:
	st		X+,Acc							;Azzera i primi 256 bytes della ram
	inc		Rcount
	brne	lp_azz_mem
	dec		Rstop
	brne	lp_azz_mem
	sts		statEvolRX,Acc	
	ldi		Acc,USER_MODE					;SI => Seleziona modalit� di funzionamento normale
	sts		modeBoot,Acc				
	rcall	testConnectPC
	sbrs	Fl_Stato,B_CONNECT_PC			;Dispositivo connesso a PC ?				
	rjmp	userProg						;No => vai alla gestione del programma principale
	ldi		Acc,BOOT_MODE					;Di default si entra con la modalit� BOOT MODE
	sts		modeBoot,Acc
;==================================================
;==  Gestione modalit� BOOT						 ==
;==================================================
gestBootMode:
	call	ena_timer_usart					;Abilita timer e usart
wait_event:
	rcall	trasmeRisposta					;Trasmissione su linea seriale
	lds		Acc,tickTim
	ori		Acc,0							;Scattato tick di timer ?
	breq	wait_event						;NO
	clr		Acc
	sts		tickTim,Acc						;SI => Azzera tick
	wdr										; Watchdog reset
	lds		Acc,tmoRxChar					;Il imer di controllo timeout della linea seriale � attivo ?
	ori		Acc,0
	breq	exiTestTmo						;NO
	dec		Acc
	sts		tmoRxChar,Acc					;Timeout spirato ?
	brne	exiTestTmo						;NO
	sts		statEvolRX,Acc					;SI => Annulla ricezione
exiTestTmo:	
	sbrs	Fl_Stato,B_COM_SER_PC			;Ricevuto un comando da PC ?
	rjmp	noMessPc						;NO						
	sbr		Fl_Stato,MK_ELAB_MES			;SI => Segnala che � in corso l'elaborazione del messaggio
	cbr		Fl_Stato,MK_COM_SER_PC			;Libera la linea seriale
	clr		ErrCode							;Inizializza codice di errore in fase di elaborazione
	ldi		Acc,0
	sts		flRecData,Acc
	lds		Acc,bytesRX
	cpi		Acc,0x26						;Comando cryptato ?
	brne	noComCrypt						;NO

;----  Elaborazione comando Criptato  ----
	rcall	elaboraComandoCriptato
	rjmp	gestRisul

noComCrypt:	
	cpi		Acc,0x20						;Comando non criptato ?
	brne	ErrComNonGest					;NO => Rispondi col codice di comando sconosciuto
	rcall	elaboraComandoNonCriptato		;SI => Elabora comando
gestRisul:	
	cpi		ErrCode,0						;Rilevato errore ?
	breq	rispoACK						;NO => Rispondi con ACK
	cpi		ErrCode,0xFF					;Richiesta la trasmissione di un messaggio specifico gi� composto ?
	breq	startTX							;SI => Attina la trasmissione
	rcall	RispoErr						;NO => Trasmetti codice di errore
	rjmp	wait_event						;Ritorna ad attesa eventi	
rispoACK:		
	ldi		Par_1,'6'						;(ACK + 0x30)
	rcall	AddrBuffTX						;Calcola l'indirizzo di deposito	
	st		X+,Par_1
	inc		RidxTX
	lds		Acc,flRecData
	cpi		Acc,0
	breq	noInsRecNum
	lds		Rpark,numRecord
	rcall   conv_4_byte						;Converti in Ascii i 4 bytes low di numRecord
	lds		Par_1,numRecord+1
	rcall   convInsAscii
	rjmp	startTX							;Attiva trasmissione e ritorna ad attesa eventi

noInsRecNum:	
	st		X,Par_2							;Codice da associare a (ACK + 0x30)
	inc		RidxTX
	rjmp	startTX


 
RispoErr:									;---- Attiva trasmissione con il codice di errore
	mov		Par_1,ErrCode
	rjmp	txErrSys

ErrComNonGest:
	ldi		Par_1,'1'
txErrSys:	
	rcall	AddrBuffTX						;Calcola l'indirizzo di deposito
	ldi		Acc,'0'
	st		X+,Acc
	inc		RidxTX
	st		X,Par_1
	inc		RidxTX
startTX:	
	ldi		Acc,1							;Attiva trasmissione
	sts		requestTX,Acc
noMessPc:								
	rjmp	wait_event						;SI => Ritorna ad attesa eventi

;---- Commuta su esecuzione programma utente
userProg:
	cli
;------ Imposta vettori di interrupt nell'area user  ------
	clr		Par_1
	rcall	write_PMIC	
	jmp		RESET_VECT_USER



;==================================================
;==  Esecuzione comando non cryptato			 ==
;==================================================
elaboraComandoNonCriptato:		
	lds		Acc,bytesRX+1
;-----------------------------------------------------------------------	
	cpi		Acc,1							;Codice 2001 (Richiesta configurazione dispositivo)?	
	brne	tstComNc_2						;NO
	ldi		ZH,HIGH(0x6FFC)
	ldi		ZL,LOW(0x6FFC)
	lpm
	ldi		Acc,0xFF
	eor		Acc,R0
	adiw	Z,1
	lpm
	mov		Par_1,R0
	cp		Acc,R0
	breq	info_0
	ldi		Par_1,0xFF
info_0: 
	ldi		Rcount,0						;Utilizzzato come flag per indicare l'inserzione dell'ID vettura							
	ldi		ZH,HIGH(RispoInfo * 2)
	ldi		ZL,LOW(RispoInfo * 2)			;Indirizza l'area flash "RispoInfo"
lp_info:
	lpm
	mov		Rpark,R0
	cpi		Rpark,0
	brne	datoInfo
	cpi		Rcount,0
	brne	end_lp_info
	ldi		Rcount,0xFF						;segnala ID inserito
	rcall   convInsAscii					;Converti in Ascii il byte in Par_1
	rjmp	info_1
end_lp_info:	
	ldi		ErrCode,0xFF					;Attiva la trasmissione trasmissione
	ret
datoInfo:	
	rcall	AddrBuffTX						;Calcola l'indirizzo di deposito
	st		X,Rpark							;Trasferisci dato
info_1:	
	adiw	Z,1							;Incrementa indirizzo di lettura
	rjmp	lp_info
	
							
RispoInfo: .db "60:", "2:", 0x00, ":",ID_HARDWARE, ":", REL_HARDWARE, ":", "95:", "9:", '0', ':',0x00,0x00
//					  Val_Envir->Act_Prod
//							xx - vettura
//								  Val_Envir->ID_Hardware
//										Val_Envir->Rel_HW
//												Val_Envir->ID_Micro
//													 Val_Envir->ID_Size
//																bank_write
convInsAscii:	
	mov		Rpark,Par_1
	swap	Rpark
	andi	Rpark,0x0F
	rcall	conv_ins_Ascii
	mov		Rpark,Par_1
conv_4_byte:
	andi	Rpark,0x0F
conv_ins_Ascii:	
	ori		Rpark,0x30
	cpi		Rpark,0x3A
	brlo	ins_09
	ldi		Acc,0x07
	add		Rpark,Acc
ins_09:	
	rcall	AddrBuffTX						;Calcola l'indirizzo di deposito
	st		X,Rpark							;Trasferisci dato
	ret
	
;-----------------------------------------------------------------------
tstComNc_2:		
	cpi		Acc,2							;Codice 2002 (Start programmazione flash)?
	breq	Com_2002
	rjmp	tstComNc_3						;NO
Com_2002:
	lds		Acc,bytesRX+2					;Ricava chiave di crypy 0
	ldi		Rpark,0x59
	eor		Acc,Rpark
	sts		rndVal,Acc
	lds		Acc,bytesRX+4					;Ricava chiave di crypy 1
	ldi		Rpark,0x1A
	eor		Acc,Rpark
	sts		rndVal+1,Acc
	lds		Acc,bytesRX+3					;Decrypta e salva parte alta
	lds		Rpark,rndVal+1					;del check del firmware da aggiornare
	eor		Acc,Rpark						;come (bytesRX+3) ^ (rndVal+1)
	sts		checkProg,Acc					
	lds		Acc,bytesRX+5					;Decrypta e salva parte bassa
	lds		Rpark,rndVal					;del check del firmware da aggiornare
	eor		Acc,Rpark						;come (bytesRX+5) ^ (rndVal)
	sts		checkProg+1,Acc			
	lds		Acc,0xD7						;Ricava altra chiave di decrypt (16 bit)
	eor		Acc,Rpark						;parte alta key di decodifica
	sts		keyCrypt,Acc					;come (rndVal) ^ 0xD7
	lds		Acc,rndVal+1					
	ldi		Rpark,0x6C
	eor		Acc,Rpark						;parte bassa key di decodifica
	sts		keyCrypt+1,Acc					;come (rndVal+1) ^ 0x6C
	ldi		Acc,0xFF						
	sts		cntRXRecord,Acc					;Inizializza il counter di controllo seguenza record				
	sts		cntRXRecord+1,Acc				;con il valore -1
	clr		Acc								;Azzera indice del blocco attualmente in uso
	sts		Idx_Act_Blocco,Acc
	sts		mirE2P+CHECK_E2P,Acc			;Invalida checksum della sys area
	sts		mirE2P+CHECK_E2P+1,Acc
	rcall	writeSysArea					;Scrivi system area	
	rcall	Eval_Param_Blocco				;Calcola i parametri del blocco e init index buffer di transito
	cbr		Fl_Stato,MK_END_RX_DATA			;Azzera flag di fine trasferimento dati di aggiornamento
	sbr		Fl_Stato,MK_FIRST_RECORD		;Segnala che si aspetta il primo record
	clr		checkParziale_L					;Azzera checksum parziale
	clr		checkParziale_H
											;L'identifier del micro ed il size della flash sono stati gi� determinati
	ldi		Par_2,'2'						;Risposta:"2002 62"
	ret 

;-----------------------------------------------------------------------
tstComNc_3:
	cpi		Acc,3							;2003 (scrittura mappa dei blocchi di memoria programma)
	brne	tstComNc_5						;NO	
	lds		Acc,bytesRX+3						
	sts		numBlocchi,Acc					;I blocchi sono al massimo 5 -- si pu� ignorare il byte alto
	clr		Acc
	clr		Rcount							;Contatore blocchi processati
	ldi		XH,HIGH (bytesRX+6)				;Salta il valore del segmento che � sempre 0x0000
	ldi		XL,LOW (bytesRX+6)
	ldi		YH,HIGH (addrBlocco_1)			;Y punta allo start del blocco
	ldi		YL,LOW (addrBlocco_1)
	ldi		ZH,HIGH (addrBlocco_2)			;Z punta alla lunghezza -1 in bytes del blocco
	ldi		ZL,LOW (addrBlocco_2)
lp_load_blocchi:							;Trasferisci i dati dei blocchi	
	ld		Acc,X+					
	st		Y+,Acc
	ld		Acc,X+							;byte alto start blocco
	ldi		Par_2,0x28						;Key di cript start
	eor		Acc,Par_2	
	st		Y+,Acc							;byte basso
	ld		Acc,X+
	st		Z+,Acc							;byte alto lunghezza -1
	ldi		Par_2,0xB3						;Key di cript lunghezza
	ld		Acc,X+
	eor		Acc,Par_2
	st		Z+,Acc							;byte basso	
	inc		Rcount
	adiw	X,3								;Salta il tipo (1 bytes) ed il segmento ( 2 bytes)
	lds		Rpark,numBlocchi	
	cp		Rcount,Rpark					;Trasferiti tutti iblocchi ?
	brne	lp_load_blocchi					;NO
	ldi		Par_2,'3'						;Risposta 200263
	ret

;-----------------------------------------------------------------------
tstComNc_5:
	cpi		Acc,5							;Codice 2005 (fine scrittura flash)?
	breq	Com2005
	rjmp	ErrComNonGest					;NO

;----  Verifica corretta scrittura flash ed eventualmente imposta modalit� user  -----
Com2005:
	sbrs	Fl_Stato,B_END_RX_DATA			;Si � acquisito l'intero corpo dati ?
	rjmp	errore_0B						;NO => Errore
	rcall	VerificaFlashProg
	brcs	err_verf						;Flash KO
	lds		Par_1,numBlocchi
	lsl		Par_1							;Moltiplica per 2 (punta a bytes)
	clr		checkSum_H						;Inizializza checkSum = 1 (16 bit - High - Low)
	ldi		Acc,1
	mov		checkSum_L,Acc
	clr		Rcount							;Counter numero blocchi
	ldi		YH,HIGH (addrBlocco_1)			;Memorizza vettori di start
	ldi		YL,LOW (addrBlocco_1)
	rcall	trasfBlocco
	clr		Rcount
	ldi		YH,HIGH (addrBlocco_2)			;Memorizza lunghezze (-1 per compatibilit� prog SAM) 
	ldi		YL,LOW (addrBlocco_2)
	rcall	trasfBlocco						;X punta all'area delle lunghezze da trasferire in e2p						
	sts		mirE2P+CHECK_E2P,checkSum_H		;Inserisci checksum della e2p
	sts		mirE2P+CHECK_E2P+1,checkSum_L
	rcall	writeSysArea					;Scrivi system area	
	ldi		Par_2,'6'						;ErrCode = 0
	ret										;Scrittura flash avvenuta correttamente;


errore_0B:
	ldi		ErrCode,'B'						;Ricevuto "Fine aggiornamento" senza aver completato il trasferimento di tutti i blocchi	
	ret
err_verf:
	ldi		ErrCode,'5'						;Errore in fase di verifica flash (Check errato)
	ret

trasfBlocco:								;Calcola checksum dei parametri dei blocchi realmente presenti
	ld		Acc,Y+
	rcall	newValCheck
	inc		Rcount
	cp		Rcount,Par_1
	brne	trasfBlocco
	ser		Acc
lp_eras_blocco:								;e imposta a 0xFF tutte le celle non utilizzate
	cpi		Rcount,MAX_BLOCCHI_PROG * 2
	breq	exiTrasfBlocco
	st		Y+,Acc
	rcall	newValCheck
	inc		Rcount
	rjmp	lp_eras_blocco
exiTrasfBlocco:
	ret	

;------------------------------------------------------------------------------------------------
;----  Verifica corretta scrittura flash tramite controllo del checksum globale			    -----
;----  Return: Carry = 0 per flash Ok -- Carry = 1 per flash Ko e in Par_1 codice di errore  ----
;------------------------------------------------------------------------------------------------
VerificaFlashProg:
	clr		checkSum_L							;Inizializza checksum = 0
	clr		checkSum_H
	sts		Idx_Act_Blocco,checkSum_H			;Idx_Act_Blocco = 0
lp_ver_blocchi:	
	rcall	Eval_Param_Blocco					;"addrMicro" contiene l'indirizzo di start
page_blocc:
	rcall	leggiPaginaFlash					;Leggi pagina flash che contiene l'indirizzo "addrMicro:addrMicro+1"
	lds		Acc,addrMicro+1
	andi	Acc,MASK_PAGE_SIZE
	mov		Par_1,Acc							;Imposta indice di scansione lettura pagina	
	ldi		YH,HIGH (PageFlashBuffer)
	ldi		YL,LOW (PageFlashBuffer)
	rcall	addr_Y								;Y punta alla prima cella da leggere		
lp_check_bloc:
	ld		Acc,Y+
	add		checkSum_L,Acc
	clr		R1
	adc		checkSum_H,R1
	lds		XH,addrMicro						;Incrementa "addrMicro:addrMicro+1"
	lds		XL,addrMicro+1
	adiw	X,1
	sts		addrMicro+1,XL
	sts		addrMicro,XH
	lds		Acc,addrMicro
	lds		Rpark,End_Blocco					;Raggiunto il fine blocco?
	cp		Acc,Rpark
	brne	no_end_bloc							;NO									
	lds		Acc,addrMicro+1
	lds		Rpark,End_Blocco+1
	cp		Acc,Rpark
	breq	end_byte_blocc						;SI => passa al blocco successivo
no_end_bloc:									
	inc		Par_1								;Incrementa indice di scan. pagina
	cpi		Par_1,FLASH_PAGE_SIZE				;Raggiunta la fine della pagina			
	breq	page_blocc							;SI
	rjmp	lp_check_bloc						;NO => processa il prossimo dato 
				
end_byte_blocc:									
	lds		Acc,Idx_Act_Blocco					;SI => Processati tutti i blocchi ?
	inc		Acc
	lds		Rpark,numBlocchi
	cp		Acc,Rpark
	breq	ver_ck								;SI => Verifica checksum
	sts		Idx_Act_Blocco,Acc
	rjmp	lp_ver_blocchi						;NO => processa il nuovo blocco

ver_ck:
	lds		Rpark,checkProg						;Verifica check calcolato con quello
	cp		Rpark,checkSum_H					;ricevuto nel record con codice 2002
	brne	errore_05		
	lds		Rpark,checkProg+1
	cp		Rpark,checkSum_L
	brne	errore_05
	clc											;Carry = 0 per flash ok
	ret
errore_05:
	sec
	ret

;==================================================================================
;==  Calcola i parametri del blocco e init parametri di gestione	             ==
;--  Act_Addr_Free = Primo indirizzo libero alla fine del blocco in elaborazione ==
;--  lenBlocco = numero di bytes che compongono il blocco						 ==
;--  End_Blocco = indirizzo di fine blocco										 ==
;--  Index_Mem = 0 e setta flag di primo record da ricevere						 ==
;--  YH:YL = indirizzo di start blocco											 ==	
;==================================================================================
Eval_Param_Blocco:
	rcall	addrStartBlocco					;Calcola indirizzo di start del blocco "Idx_Act_Blocco" (memo in Y)
	ld		Act_Addr_Free_H,Y+
	sts		addrMicro,Act_Addr_Free_H		;Imposta indirizzo della prima cella libera
	ld		Act_Addr_Free_L,Y
	sts		addrMicro+1,Act_Addr_Free_L
	rcall	addrLenBlocco					;Punta all'array delle lunghezze
	ld		Acc,Y+
	ld		Rpark,Y
	sec										;Somma 1 al risultato (lunghezza � -1 per compatibilit�)
	adc		Rpark,Act_Addr_Free_L
	adc		Acc,Act_Addr_Free_H
	sts		End_Blocco,Acc					;Calcola indirizzo di fine blocco (prima cella libera dopo il blocco)
	sts		End_Blocco+1,Rpark
	sbr		Fl_Stato,MK_FIRST_RECORD		;Si aspetta di ricevere il primo record del blocco
	clr		Index_Mem						;Inizializza indice buffer di transito
	ret

;==================================================
;==  Esecuzione comando cryptato				 ==
;==================================================
elaboraComandoCriptato:
	rcall	decryptRecord					;Decrypta record dati
	cpi		ErrCode,0						;Si sono riscontrati errori?
	breq	elabComCry						;NO
	ret	
elabComCry:	
	lds		Acc,bytesRX+1					;Leggi subcodice operativo
;-----------------------------------------------------------------------
	cpi		Acc,0x05						;Codice 2805 (scrittura e2p)?
	brne	tstCom_07						;NO
	lds		Rpark,bytesRX+2					;Ricava indirizzo di start dati del record - MSB
	lds		Acc,bytesRX+3					;LSB
	rcall	loadPageE2p						;Leggi la pagina che contiene l'indirizzo
	ldi		YH,HIGH	(buffE2P)
	ldi		YL,LOW (buffE2P)
	lds		Par_1,bytesRX+3
	andi	Par_1,MASK_PAGE_E2P
	rcall	addr_Y							;Pointer destinazione dati
	ldi		Rcount,4
	ldi		XH,HIGH (bytesRX+4)				;Pointer origine dati
	ldi		XL,LOW (bytesRX+4)
lp_buff_e2p:
	ld		Acc,X+
	st		Y+,Acc
	inc		Rcount
	cp		Rcount,idxBuffRX
	brne	lp_buff_e2p
	mov		Acc,Par_2						;Imposta numero pagina
	ldi		Par_1,E2P_PAGE_SIZE
	ldi		YL,LOW (buffE2P)
	ldi		YH,HIGH (buffE2P)
; ---- Scrive il buffer puntato da Y nella pagina "Acc" della E2P per una lunghezza pari a "Par_1" ------
	rcall	EEPROM_WriteBlock
	ldi		Par_2,'5'						;Rispondi che � stata scritto il record di dati per e2p
	ret
;-----------------------------------------------------------------------
tstCom_07:	
	cpi		Acc,0x07						;Codice 2807 (cambio segmento) ?
	brne	tstCom_08						;NO
	sbrc	Fl_Stato,B_FIRST_RECORD			;Si aspetta il primo record ?
	rjmp	rispo_2607						;SI
	or		Index_Mem,Index_Mem				;Vi sono dati ancora non scritti in flash ?
	breq	IndexMem_0						;NO
	rcall	fillPageBuff					;SI => Fill restante pagina della flash
	rcall	scriviFlashAddrMicro			;Scrivi pagina flash e azzera indice del buffer di transito
	brcc	IndexMem_0						;Scrittura effettuata correttamente ?
	rjmp	errWriteFlash					;NO => Segnala errore in fase di scrittura
IndexMem_0:
	rcall	valutaBlocchi					;Se non sono stati elaborati tutti i blocchi, inizializza parametri del nuovo blocco		
rispo_2607:
	ldi		Par_2,'7'						;Rispondi che � stata impostata parte alta indirizzo"
	ret

;-----------------------------------------------------------------------
tstCom_08:	
	cpi		Acc,0x08						;Codice 2808 (acquisizione corpo dati)?
	breq	exe_Com_08
	rjmp	tstCom_09						;NO
exe_Com_08:	
	ldi		Acc,1							;Segnala elaborazione dati per flash
	sts		flRecData,Acc
	lds		Acc,bytesRX+4					;Ricava indirizzo di start dati del record
	sts		addrMicro,Acc					;MSB
	lds		Acc,bytesRX+5
	sts		addrMicro+1,Acc					;LSB
	lds		Acc,bytesRX+2					;Num. record MSB
	sts		numRecord,Acc
	lds		Acc,bytesRX+3					;Num. record LSB
	sts		numRecord+1,Acc
	subi	idxBuffRX,8						;tolgo 2 bytes x 2608 - 2 x num record - 2 x addr - 2 x ck locale
	sbrs	Fl_Stato,B_FIRST_RECORD			;Si aspetta il primo record ?
	rjmp	Trasf_Byte_Rec					;NO
	cbr		Fl_Stato,MK_FIRST_RECORD		;Segnala che si � processato il primo record		
	rcall	addrStartBlocco					;Inizializza parametri per l'acquisizione di un nuovo blocco
	ld		Acc,Y+							;Msb
	sts		addrMicro,Acc
	ld		Acc,Y							;Lsb
	sts		addrMicro+1,Acc				
	andi	Acc,0x7F						;L'indirizzo del primo record � allineato alla pagina ?
	breq	Trasf_Byte_Rec					;SI
	rcall	leggiPaginaFlash				;NO ==> Leggi pagina dalla flash	
	lds		Acc,addrMicro+1					;Imposta indice "Index_Mem" del buffer della flash 	
	andi	Acc,0x7F						;per puntare all'indirizzo ricevuto nel record
	mov		Index_Mem,Acc							
Trasf_Byte_Rec:
 	clr		Rcount
	ldi		YH,HIGH (bytesRX+6)				;Inizio dati ricevuti
	ldi		YL,LOW (bytesRX+6)
lp_check_parz:	
	ld		Acc,Y+
	add		checkParziale_L,Acc				;Controllo del checksum parziale
	clr		R1
	adc		checkParziale_H,R1
	inc		Rcount
	cp		Rcount,idxBuffRX
	brne	lp_check_parz
	ld		Acc,Y+
	ld		Rpark,Y
	cp		Acc,checkParziale_H
	brne	errCheckParz
	cp		Rpark,checkParziale_L
	brne	errCheckParz
	clr		Rcount
	ldi		YH,HIGH (bytesRX+6)				;Inizio dati ricevuti
	ldi		YL,LOW (bytesRX+6)
	ldi		XH,HIGH (PageFlashBuffer)		;Calcola l'indirizzo del buffer della flash in cui
	ldi		XL,LOW (PageFlashBuffer)		;trasferire il primo byte del record
	add		XL,Index_Mem
	clr		Acc
	adc		XH,Acc
	movw	ZH:ZL,Act_Addr_Free_H:Act_Addr_Free_L
lp_trasf_byte:
	ld		Acc,Y+							;Trasferisci dati dal buffer di Rx a quello della flash
	st		X+,Acc	
	adiw	Z,1								;Aggiorna indirizzo in cui si scriver� il prossimo byte nella flash 
	movw	Act_Addr_Free_H:Act_Addr_Free_L,ZH:ZL	;(serve per controllare quando si raggiunge il fine blocco)
	inc		Rcount
	inc		Index_Mem						;Incrementa indice del buffer della flash
	mov		Acc,Index_Mem
	cpi		Acc,FLASH_PAGE_SIZE				;La pagina della flash � completa ?
	brne	lp_trasf_byte_1					;NO
	rcall	scriviFlashAddrMicro			;SI => Scrivi pagina flash e azzera indice "Index_Mem" del buffer della flash
	brcs	errWriteFlash					;Carry = 1 => Rilevato errore in scrittura/verifica flash
lp_trasf_byte_1:
	cp		Rcount,idxBuffRX				;Effettuato il trasferimento di tutti i dati ricevuti ?
	brne	lp_trasf_byte					;NO
	cpi		idxBuffRX,DATI_PER_RECORD		;SI => Verifica se la lunghezza del record ricevuto � minore della dimensione del buffer?
	breq	rec_completo					;Lunghezza del record uguale alla dimensione del buffer 
	rcall	testEndBlocco					;Si � raggiunto un fine blocco ?
	brcc	rec_no_fine_blocco				;NO => errore
	or		Index_Mem,Index_Mem				;Si � gi� effettuata una scrittura ed il buffer � vuoto ?
	breq	rec_completo					;SI
	rcall	fillPageBuff					;NO => Fill con 0xFF sino alla fine della pagina
	rcall	scriviFlashAddrMicro			;Scrivi pagina flash e azzera indice "Index_Mem" del buffer della flash
	brcs	errWriteFlash					;Carry = 1 => Rilevato errore in scrittura/verifica flash
rec_completo:
	rcall	testEndBlocco					;Si � raggiunto un fine blocco ?
	brcc	exi_2608						;NO
	rjmp	valutaBlocchi					;SI => Verifica se sono stati trasferiti tutti i blocchi

rec_no_fine_blocco:
	ldi		ErrCode,'8'						;Errore -> Ad un record incompleto non corrisponde un fine blocco
exi_2608:
	ret

errCheckParz:
	ldi		ErrCode,'4'						;Errore -> Check parziale errato
	ret
errWriteFlash:
	ldi		ErrCode,'C'						;Segnala Errore in scrittura flash
	ret

;-------- Valuta se sono stati trattati tutti i blocchi
valutaBlocchi:
	lds		Rpark,Idx_Act_Blocco
	inc		Rpark
	sts		Idx_Act_Blocco,Rpark
	lds		Acc,numBlocchi
	cp		Acc,Rpark
	breq	segna_fine_dati
	rjmp	Eval_Param_Blocco					;Inizializza parametri per gestire un nuovo blocco
segna_fine_dati:
	sbr		Fl_Stato,MK_END_RX_DATA				;Segnala che non vi sono altri blocchi da gestire
	ret

;-------- Fill pagina buffer da Rcount sino a FLASH_PAGE_SIZE 
fillPageBuff:	
	ldi		YH,HIGH (PageFlashBuffer)
	mov		YL,Index_Mem
	ldi		Acc,0xFF
lp_fill_page:
	st		Y+,Acc
	cpi		YL,FLASH_PAGE_SIZE
	brne	lp_fill_page
	ret

;---- Test se raggiunto il fine blocco
;---- Return: Carry = 1 se raggiunta la fine
;---- Carry = 0 in caso contrario  
testEndBlocco:	
	lds		Acc,End_Blocco
	cp		Acc,Act_Addr_Free_H
	brne	no_fine_blocco
	lds		Acc,End_Blocco+1
	cp		Acc,Act_Addr_Free_L
	brne	no_fine_blocco
	sec
	ret
no_fine_blocco:
	clc
	ret
			
//			case 0x09:
tstCom_09:	
	cpi		Acc,0x09
	breq	ela_09
	rjmp	ErrComNonGest
ela_09:
	or		Index_Mem,Index_Mem
	breq	rispo_69
	rcall	scriviFlashAddrMicro		// Micro: Scrivi pagina flash e azzera indice del buffer di transito		
rispo_69:
	ldi		Par_2,'9'					//" Impostati i registri CS e IP di start programma";
	ret

;==================================================
;==  Decrypt record								 ==
;==================================================
 decryptRecord:
	// uint16_t n = 0;
	clr		Nreg
	clr		ErrCode
	ldi		YH,HIGH (bytesRX)
	// if (bytesRX[1] == 8) {
	lds		Acc,bytesRX+1
	cpi		Acc,0x08
	brne	 decryptRecord_1
		//micro_numByteDati = (numBytesRX - 7) / 2;
	mov		Rstop,idxBuffRX
	clc
	subi	Rstop,4				;numByteDati = numBytesRX - 4 (2 bytes 2608 - 2 bytes num. record)
	;(++valEnvir.cntRXRecord)
	lds		ZH,cntRXRecord
	lds		ZL,cntRXRecord+1
	adiw	ZH:ZL,1
	sts		cntRXRecord,ZH			;Incrementa numero record
	sts		cntRXRecord+1,ZL

//		if ((++valEnvir.cntRXRecord)== ((uint16_t) mstrtolhex((char *)&recvBuffer[position], 3) & 0xFFF)) {
	lds		Acc,bytesRX+2
	cp		Acc,ZH
	brne	nonConsecut
	lds		Acc,bytesRX+3
	cp		Acc,ZL
	brne	nonConsecut
//			if (micro_Fl_End_RX_Data == true) {	
	ldi		YL,LOW (bytesRX+4)
	sbrs	Fl_Stato,B_END_RX_DATA
	rjmp	lp_decrypt		;Tutto ok				
//				cod_err = 0x0A;
	ldi		ErrCode,'A'			// "0A" - Ricevuto record dopo il fine trasferimento dati
	ret

nonConsecut:
//			rispoCodErr(0x33);	// "03" - "Record non consecutivo"
//			cod_err = 0x03;
	ldi		ErrCode,'3'
	ret

decryptRecord_1:		
		;micro_numByteDati = (numBytesRX - 4) / 2;
	mov		Rstop,idxBuffRX
	clc
	subi	Rstop,2
//		for (i = 0; i < micro_numByteDati; i++, position += 2) {
	ldi		YL,LOW (bytesRX+2)
lp_decrypt:	
//			B_Crypt_0 = ((uint8_t) mstrtolhex((char *)&recvBuffer[position], 2) ^ valEnvir.rndVal[n & 1])^ n;
	lds		Rpark,rndVal		;valEnvir.rndVal[n & 1]
	sbrc	Nreg,0
	lds		Rpark,rndVal+1
	ld		B_Crypt_0,Y
	eor		B_Crypt_0,Rpark
	eor		B_Crypt_0,Nreg
//			B_Crypt_1 = (B_Crypt_0 & 0xF0) >> 4;
	mov		B_Crypt_1,B_Crypt_0
	swap	B_Crypt_1
	ldi		Acc,0x0F
	and		B_Crypt_1,Acc
	
//			B_Crypt_0 = B_Crypt_0 & 0x0F;
	ldi		Acc,0x0F
	and		B_Crypt_0,Acc
//			bytesRX[i+2] = (decryptTab[B_Crypt_0 & 0x0F ] << 4) + decryptTab[B_Crypt_1];
	ldi		ZH,HIGH(decryptTab << 1)
	ldi		ZL,LOW(decryptTab << 1)
	clr		Acc
	add		ZL,B_Crypt_0	
	adc		ZH,Acc
	lpm		Rpark,Z					
	swap	Rpark
	andi	Rpark,0xF0
	ldi		ZH,HIGH(decryptTab << 1)
	ldi		ZL,LOW(decryptTab << 1)
	clr		Acc
	add		ZL,B_Crypt_1	
	adc		ZH,Acc
	lpm		Acc,Z					
	or		Rpark,Acc
	st		Y+,Rpark				
//			n++;
	inc		Nreg
	cp		Nreg,Rstop
	brne	lp_decrypt
//		}	
	ret

decryptTab: .db 0x03, 0x0D, 0x07, 0x0E, 0x02, 0x0F,	0x08, 0x0B, 0x05, 0x00, 0x04, 0x06, 0x09, 0x0C, 0x0A, 0x01

;==================================================
;==  Test connessione dispositivo al PC			 ==
;==================================================
testConnectPC:
	sbr		Fl_Stato,MK_CONNECT_PC			;Predisponi per dispositivo connesso	
	lds		Acc,PORTC_IN
	andi	Acc,MK_SEL_CLQ_PC					;Si tratta della connessione a PC ?
	cpi		Acc,SEL_CLQ_PC
	breq	connect_pc						;SI
	cbr		Fl_Stato,MK_CONNECT_PC			;NO => Segnala che il dispositivo non � connesso
connect_pc:
	ret

	.include "GesDev.asm"
	.EXIT					
