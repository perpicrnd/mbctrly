/*
 * GesDev.asm
 *
 *  Created: 03/07/2016 22:04:35
 *   Author: Buzzanca
 */
# include "gas.h"
# include <avr/io.h>
;===========================================================
;==  Scrivi "Par_1" in PIMIC_CTRL che � in area protetta  ==
;===========================================================
#include "Mem_Def.inc"

write_PMIC:	
	clr		Acc
	out     CPU_RAMPZ, Acc          // Reset bits 23:16 of Z	                					
	ldi		ZL,pm_lo8 (PMIC_CTRL)		// Load addr into Z
	ldi		ZH,pm_hi8 (PMIC_CTRL)
	ldi     Acc, CCP_IOREG 
	out     CPU_CCP, Acc            // Start CCP handshake
	st      Z, Par_1                  // Write value to I/O register
	ret

;===================================================================
;==  Calcola l'indirizzo della lunghezza del blocco attuale       ==
;===================================================================
addrLenBlocco:
	ldi		YH,hi8(addrBlocco_2)
	ldi		YL,pm_lo8 (addrBlocco_2)
	rjmp	addrEval
;===================================================================
;==  Calcola l'indirizzo di entry dello start del blocco attuale  ==
;===================================================================
addrStartBlocco:	
	ldi		YH,pm_hi8 (addrBlocco_1)
	ldi		YL,pm_lo8 (addrBlocco_1)
addrEval:	
	lds		Par_1,Idx_Act_Blocco
	lsl		Par_1							;Moltiplica per 2	
;---  Somma Par_1 al registro Y
 addr_Y:
	add		YL,Par_1
	clr		R1
	adc		YH,R1
	ret
		 
;==================================================
;==  Trasmissione su linea seriale				 ==
;==================================================
trasmeRisposta:
	lds		Acc,USARTC0_STATUS				;Trasmissione possibile ?
	andi	Acc,USART_DREIF_bm
	brne	tst_req_0
	ret										;NO => Esci
tst_req_0:	
	lds		Acc,requestTX					;Richiesta in corso ?
	ori		Acc,0
	brne	tst_req_1
	ret										;NO => Esci
tst_req_1:	
	cpi		Acc,1
	brne	tst_req_2
	clr		Acc
	sts		cntBytesTX,Acc					;Counter dei bytes trasmessi
	mov		Rpark,RidxTX
	rcall	convAscii
	clr		checkSum_H						;checkSum = '1' + bytesOut[0] + bytesOut[1];
	ldi		Acc,'0' + 1
	mov		checkSum_L,Acc
	lds		Acc,bytesOut	
	rcall	newValCheck
	lds		Acc,bytesOut+1
	rcall	newValCheck
	ldi		Rpark,STX						;Invio STX
	rjmp	txAndInc						;Trasmetti e passa allo stato successivo

tst_req_2:
	cpi		Acc,2
	brne	tst_req_3
	ldi		Rpark,'0'						;Invio lunghezza dati record H		
	rjmp	txAndInc

tst_req_3:
	cpi		Acc,3
	brne	tst_req_4
	lds		Rpark,bytesOut					;Invio lunghezza dati record M	
	rjmp	txAndInc

tst_req_4:
	cpi		Acc,4
	brne	tst_req_5
	clr		Acc
	sts		idxTX,Acc						;Azzera indice del buffer dati da trasmetter
	lds		Rpark,bytesOut+1				;Invio lunghezza dati record L	
	rjmp	txAndInc		

tst_req_5:
	cpi		Acc,5
	brne	tst_req_6	
	ldi		XH,pm_hi8 (strTX)					;Invio corpo dati					
	ldi		XL,pm_lo8 (strTX)					;X = Pointer al buffer dati da trasmetter
	lds		Acc,idxTX
	add		XL,Acc							;Calcola indirizzo
	ld		Acc,X
	rcall	newValCheck						;Non modifica Acc
	sts		USARTC0_DATA,Acc				;Trasmetti dato
	lds		Acc,idxTX
	inc		Acc								;Incrementa indice buffer dati da trasmettere
	sts		idxTX,Acc
	lds		Acc,cntBytesTX
	inc		Acc								;Incrementa numero dati trasmessi
	sts		cntBytesTX,Acc
	cp		Acc,RidxTX						;Trasmessi tutto il corpo dati?
	breq	increqTX						;SI => Passa allo stato successivo
	rjmp	exiDataTX						;NO => Esci senza cambiare stato

tst_req_6:
	cpi		Acc,6
	brne	tst_req_7
	mov		Rpark,checkSum_H
	rcall	convAscii
	lds		Rpark,bytesOut
	rjmp	txAndInc						;Trasmetti checksum H (digit alto)

tst_req_7:
	cpi		Acc,7
	brne	tst_req_8
	lds		Rpark,bytesOut+1
	rjmp	txAndInc						;Trasmetti checksum H (digit basso)	

tst_req_8:
	cpi		Acc,8
	brne	tst_req_9
	mov		Rpark,checkSum_L
	rcall	convAscii
	lds		Rpark,bytesOut
	rjmp	txAndInc						;Trasmetti checksum L (digit alto)			

tst_req_9:
	cpi		Acc,9
	brne	tst_req_10
	lds		Rpark,bytesOut+1
	rjmp	txAndInc						;Trasmetti checksum H (digit basso)

tst_req_10:
	cpi		Acc,10
	brne	req_11
	ldi		Rpark,ETX
	rjmp	txAndInc						;Trasmetti ETX

req_11:
	ldi		Rpark,LF
	sts		USARTC0_DATA,Rpark				;Trasmetti LF
	clr		Acc
	sts		requestTX,Acc					;Azzera stato evolutivo (riportalo a riposo)			
	cbr		Fl_Stato,MK_ELAB_MES			;Segnala che l'elaborazione del comando � finita
	ret
txAndInc:
	sts		USARTC0_DATA,Rpark				;Trasmetti dato in "Rpark"
increqTX:	
	lds		Acc,requestTX					;Passa allo stato successivo
	inc		Acc
	sts		requestTX,Acc	
exiDataTX:	
	ret

;---- Aggiorna valore del checksum
newValCheck:	
	add		checkSum_L,Acc					;Calcola il checksum su 16 bit
	clr		R1
	adc		checkSum_H,R1
	ret

;----  Converi un hex (Rpark) in 2 byte ascii (bytesOut e bytesOut+1)
convAscii:
	mov		Acc,Rpark
	andi	Acc,0x0F						;4 bit bassi
	ldi		Rcount,0x30
	cpi		Acc,0x0A
	brlo	min10
	ldi		Rcount,0x37
min10:
	add		Acc,Rcount
	sts		bytesOut+1,Acc
	swap	Rpark
	andi	Rpark,0x0F						;4 bit alti
	ldi		Rcount,0x30
	cpi		Rpark,0x0A
	brlo	min0A
	ldi		Rcount,0x37
min0A:
	add		Rpark,Rcount
	sts		bytesOut,Rpark
	ret

;==================================================
;==  Gestione interrupt da timer 4				 ==
;==================================================	
TCC4_Overflow:
	push	Acc								;Salva reg. Acc
	lds		Acc,CPU_SREG					;Salva Status
	push	Acc
	lds		Acc,TCC4_INTFLAGS
	ori		Acc,TC4_OVFIF_bm
	sts		TCC4_INTFLAGS,Acc
	ldi		Acc,1
	sts		tickTim,Acc						;Segnala passaggio per timer spirato
	pop		Acc
	sts		CPU_SREG,Acc					;Ripristina status
	pop		Acc								;Ripristina reg. Acc
	reti

;==================================================
;==  Gestione interrupt da Usart				 ==
;==================================================
USARTC0_RXC_ISR:
	push	Rpark							;Salva reg. Acc e Rpark
	push	Acc
	lds		Acc,CPU_SREG					;Salva Status
	push	Acc
	push	R26								;Salva registro X
	push	R27	
	lds		Acc,USARTC0_STATUS				;USARTC0.STATUS |= 0x80;
	ori		Acc,0x80
	sts		USARTC0_STATUS,Acc
	ldi		Acc,MAX_DELAY_CHAR				;Retrig timeout rx
	sts		tmoRxChar,Acc
	lds		Rpark,USARTC0_DATA				;Leggi carattere ricevuto
	sbrs	Fl_Stato,B_COM_SER_PC
	sbrc	Fl_Stato,B_ELAB_MES
	rjmp	abort_mes						; Con attivit� in corso, scarta carattere
	lds		Acc,statEvolRX
	cpi		Acc,0							;statEvolRX == 0 ?
	brne	tst_evolRX_1					;NO	
; --- statEvolRX = 0 -- Wait STX 	
	cpi		Rpark,STX						;SI => Ricevuto STX ?
	breq	evol_RX_0
	rjmp	abort_mes						;NO => scarta carattere ricevuto 
evol_RX_0:	
	ldi		Acc,1							;statEvolRX = 1
	sts		statEvolRX,Acc		
	sts		checkRX,Acc						;checkRX = 1
	clr		idxBuffRX						;idxBuffRX = 0
	sts		checkRx+1,idxBuffRX
	sts		numCharRX,idxBuffRX				;numCharRX = 0	
	rjmp	exit_int_usart

tst_evolRX_1:
	cpi		Acc,1							;statEvolRX == 1 ?
	brne	tst_evolRX_2					;NO
; --- statEvolRX = 1 -- Acquisizione lunghezza H	
	rcall	Check_e_Conv					;Calcola check - converti in binario 
	brcc	evol_RX_1
j_abort_mes:	
	rjmp	abort_mes						;Non digit hex => scarta
evol_RX_1:	
	ori		Rpark,0							;La lunghezza deve essere inferiore a 0xFF
	brne	j_abort_mes
	rjmp	next_stat_RX

tst_evolRX_2:
	cpi		Acc,2							;statEvolRX == 2 ?
	brne	tst_evolRX_3					;NO
; --- statEvolRX = 2 -- Acquisizione lunghezza M	
	rcall	Check_e_Conv					;Calcola check - converti in binario 
	brcc	evol_RX_2
	rjmp	abort_mes						;Non digit hex => scarta
evol_rx_2:	
	swap	Rpark
	sts		lenDati,Rpark
	rjmp	next_stat_RX

tst_evolRX_3:
	cpi		Acc,3							;statEvolRX == 3 ?
	brne	tst_evolRX_4					;NO
; --- statEvolRX = 3 -- Acquisizione lunghezza L
	rcall	Check_e_Conv					;Calcola check - converti in binario
	brcc	evol_RX_3
jj_abort_mes:	
	rjmp	abort_mes						;Non digit hex => scarta
evol_RX_3:	
	lds		Acc,lenDati						;Memorizza la lunghezza del corpo dati da ricevere
	or		Rpark,Acc
	sts		lenDati,Rpark
	clr		RidxTX							;Indice prima cella libera nel buffer di TX
	rjmp	next_stat_RX

tst_evolRX_4:
	cpi		Acc,4							;statEvolRX == 4 ?
	brne	tst_evolRX_5					;NO		
; --- statEvolRX = 4 -- Acquisizione corpo dati
	cpi		idxBuffRX,BUFF_RX_SIZE			;Buffer pieno ?
	breq	jj_abort_mes					;SI => abort messaggio -- statEvolRX = 0;
	mov		Acc,RidxTX
	cpi		Acc,4							;Copia i primi 4 caratteri nel buffer TX (echo del comando)
	brsh	no_ini_tx
	rcall	AddrBuffTX						;Calcola l'indirizzo di deposito
	st		X,Rpark
no_ini_tx:	
	rcall	Memo_Data_RX					;Memorizza dato convertito in binario nel buffer di Rx
	lds		Acc,numCharRX
	inc		Acc								;Incrementa numCharRX
	sts		numCharRX,Acc
	lds		Rpark,lenDati
	cp		Acc,Rpark						;Ricevuti tutti i bytes ?
	breq	j_next_stat_RX					;SI => Vai a ricevere il checksum
	rjmp	exit_int_usart

tst_evolRX_5:
	cpi		Acc,5							;statEvolRX == 5 ?
	brne	tst_evolRX_6					;NO
; --- statEvolRX = 5 -- Check digit 1	
	rcall	Conv_Ascii_Bin					;Converti in binario 
	brcs	abort_mes						;Non digit hex => scarta
	swap	Rpark
	sts		checkRec+1,Rpark
j_next_stat_RX:	
	rjmp	next_stat_RX

tst_evolRX_6:
	cpi		Acc,6							;statEvolRX == 6 ?
	brne	tst_evolRX_7					;NO
; --- statEvolRX = 6 -- Check digit 2
	rcall	Conv_Ascii_Bin					;Converti in binario
	brcs	abort_mes						;Non digit hex => scarta
	lds		Acc,checkRec+1
	or		Rpark,Acc
	sts		checkRec+1,Rpark
	rjmp	next_stat_RX

tst_evolRX_7:
	cpi		Acc,7							;statEvolRX == 7 ?
	brne	tst_evolRX_8					;NO
; --- statEvolRX = 7 -- Check digit 3	
	rcall	Conv_Ascii_Bin					;Converti in binario 
	brcs	abort_mes						;Non digit hex => scarta
	swap	Rpark
	sts		checkRec,Rpark
	rjmp	next_stat_RX

tst_evolRX_8:
	cpi		Acc,8							;statEvolRX == 8 ?
	brne	tst_evolRX_9					;NO
; --- statEvolRX = 8 -- Check digit 4
	rcall	Conv_Ascii_Bin					;Converti in binario
	brcs	abort_mes						;Non digit hex => scarta
	lds		Acc,checkRec
	or		Rpark,Acc
	sts		checkRec,Rpark
	rjmp	next_stat_RX

tst_evolRX_9:
	cpi		Acc,9							;statEvolRX == 9 ?
	brne	tst_evolRX_A					;NO
; --- statEvolRX = 9 -- Atteso ETX
	cpi		Rpark,ETX						;Ricevuto ETX ?
	brne	abort_mes						;NO => scarta carattere ricevuto
	rjmp	next_stat_RX

tst_evolRX_A:
	cpi		Acc,0x0A						;statEvolRX == 10 ?
	brne	abort_mes						;NO
; --- statEvolRX = 10 -- Atteso LF
	cpi		Rpark,LF						;Ricevuto LF ?
	brne	abort_mes						;NO
	lds		Acc,checkRX+1
	lds		Rpark,checkRec+1
	cp		Acc,Rpark
	brne	abort_mes
	lds		Acc,checkRX
	lds		Rpark,checkRec
	cp		Acc,Rpark
	brne	abort_mes
	sbr		Fl_Stato,MK_COM_SER_PC			;Segnala al sistema che � stato ricevuto un messaggio da PC
abort_mes:
	clr		Acc								;statEvolRX = 0;
	sts		statEvolRX,Acc
	sts		tmoRxChar,Acc					;Ferma counter timeout
	rjmp	exit_int_usart

next_stat_RX:								;Passa allo stati successivo
	lds		Acc,statEvolRX
	inc		Acc
	sts		statEvolRX,Acc
exit_int_usart:
	pop		R27								;Ripristina registro X
	pop		R26
	pop		Acc
	sts		CPU_SREG,Acc					;Ripristina status
	pop		Acc								;Ripristina reg. Acc e Rpark
	pop		Rpark
	reti

;---- Aggiorna valore del checksum
;---- Memorizza Rpark convertito nel buffer di Rx
Memo_Data_RX:
	rcall	Check_e_Conv
	ldi		XH,pm_hi8 (bytesRX)				;Pointer al buffer di RX
	ldi		XL,pm_lo8 (bytesRX)
	add		XL,idxBuffRX					;Calcola indirizzo effettivo della prima locazione libera
	lds		Acc,numCharRX					;Verifica se trattasi del digit alto
	andi	Acc,0x01						;del byte
	brne	lowByte
	swap	Rpark
	st		X,Rpark							;Memorizza parte alta del digit
	ret
lowByte:
	ld		Acc,X
	or		Acc,Rpark						;Aggiungi parte bassa
	st		X,Acc	
	inc		idxBuffRX						;Incrementa counter idxBuffRX
	ret

;---- Calcola indirizzo effettivo della prima locazione libera nel buffer di TX
AddrBuffTX:	
	ldi		XH,pm_hi8 (strTX)					;Pointer al buffer di TX
	ldi		XL,pm_lo8 (strTX)
	add		XL,RidxTX							
	inc		RidxTX
	ret
 
;---- Converti valore in binario e Aggiorna valore del checksum
Check_e_Conv:	
	push	Rpark							;Salva per la conversione successiva
	lds		Acc,checkRX						;Aggiorna valore del checksum dei dati ricevuti
	add		Acc,Rpark
	sts		checkRX,Acc
	clr		Acc
	lds		Rpark,checkRX+1
	adc		Acc,Rpark
	sts		checkRX+1,Acc
	pop		Rpark							;Ripristina valore da convertire
;---- Verifica che il dato in Rpark sia un digit hex rappresentato in Ascii
;---- Se la verifica da esito negativo, si ha il Carry = 1
;---- Se � un digit hex si ha il Carry = 0 ed il dato convertito in binario
Conv_Ascii_Bin:		
	subi	Rpark,0x30
	cpi		Rpark,0x17
	brsh	abort_ch
	cpi		Rpark,0x0A
	brlo	exi_conv
	cpi		Rpark,0x11
	brlo	abort_ch
	subi	Rpark,7
exi_conv:
	clc				;OK => C = 0
	ret	
abort_ch:
	sec				;Errore => C = 1
	ret
				
;==================================================
;==  Abilita timer e usart e relativi interrupt  ==
;==================================================
ena_timer_usart:	
	clr		Acc							;Azzera flag di timer spirato
	sts		tickTim,Acc	
	lds		Acc,PR_PRPC					;PR.PRPC &= PR_TC4_bm;
	andi	Acc,PR_TC4_bm
	sts		PR_PRPC,Acc
	lds		Acc,PR_PRPC					;PR.PRPC &= PR_HIRES_bm;
	andi	Acc,PR_HIRES_bm
	sts		PR_PRPC,Acc
	ldi		Acc,TC4_OVFINTLVL0_bm		; Timer Overflow/Underflow Interrupt  Low Level group
	sts		TCC4_INTCTRLA,Acc
	ldi		Acc,TC45_WGMODE_NORMAL_gc	; Configure TC in normal mode
	sts		TCC4_CTRLB,Acc				; using 500kHz (2us) resolution clock (5ms = 2500 * 2us)
	ldi		Acc,pm_lo8(2500)				; Imposta counter per generare int. ogni 5 ms
	sts		TCC4_PER,Acc
	ldi		Acc,pm_hi8(2500)
	sts		TCC4_PER+1,Acc	
	ldi		Acc,TC45_CLKSEL_DIV64_gc	; Clock source for the timer/counter = System Clock / 64
	sts		TCC4_CTRLA,Acc
	ldi		Acc,PR_HIRES_bm
	sts		PR_PRPC,Acc	

	// Initialize usart driver in RS232 mode
	lds		Acc,PORTC_REMAP
	ori		Acc,PR_USART0_bm
	sts		PORTC_REMAP,Acc
	ldi		Acc,0x80
	sts		PORTC_DIRSET, Acc
	ldi		Acc,0x40
	sts		PORTC_DIRCLR, Acc
	;USARTC0.CTRLC = (USARTC0.CTRLC & (~USART_CMODE_gm)) | USART_CMODE_ASYNCHRONOUS_gc;
	lds		Acc,USARTC0_CTRLC
	andi	Acc,(~USART_CMODE_gm)
	ori		Acc,USART_CMODE_ASYNCHRONOUS_gc
	sts		USARTC0_CTRLC,Acc
	;USARTC0.CTRLC = USART_CHSIZE_8BIT_gc | USART_PMODE_DISABLED_gc;
	ldi		Acc,USART_CHSIZE_8BIT_gc | USART_PMODE_DISABLED_gc
	sts		USARTC0_CTRLC,Acc
	//BSCALE 0 BSEL 16
	;USARTC0.BAUDCTRLB = 0x0A4;	//(uint8_t)((~USART_BSCALE_gm) & (bsel_value >> 8));
	ldi		Acc,0x0A4
	sts		USARTC0_BAUDCTRLB,Acc
	;USARTC0.BAUDCTRLA = 0x20;	//(uint8_t)(bsel_value);
	ldi		Acc,0x20
	sts		USARTC0_BAUDCTRLA,Acc
	;USARTC0.CTRLB |= USART_TXEN_bm | USART_RXEN_bm;
	lds		Acc,USARTC0_CTRLB
	ori		Acc,USART_TXEN_bm | USART_RXEN_bm
	sts		USARTC0_CTRLB,Acc
	;USARTC0.CTRLA |= USART_RXSIE_bm | USART_RXCINTLVL0_bm;
	lds		Acc,USARTC0_CTRLA
	ori		Acc,USART_RXSIE_bm | USART_RXCINTLVL0_bm
	sts		USARTC0_CTRLA,Acc
	;PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
	ldi		Acc,PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm
	sts		PMIC_CTRL,Acc	
	sei		;sei();
	ret

;==================================================
;==  Gestione E2P                                ==
;==================================================
; ---- Carica la pagina della Sys Area dalla E2P in mirE2P  -----
loadSysArea:
	ldi		Acc,SYS_PAGE_E2P
	ldi		YL,pm_lo8 (mirE2P)
	ldi		YH,pm_hi8 (mirE2P)
	rjmp	Load_Page
; ---- Carica la pagina che contiene l'indirizzo "Rpark:Acc" della E2P in buffE2P  -----
; ---- e salva il numero di pagina in Par_2
loadPageE2p:
	clr		Rcount
lp_sh_addr:
	clc		
	ror		Rpark
	ror		Acc
	inc		Rcount
	cpi		Rcount,5
	brne	lp_sh_addr
	mov		Par_2,Acc
	ldi		YL,pm_lo8 (buffE2P)
	ldi		YH,pm_hi8 (buffE2P)
Load_Page:	
	ldi		Rpark,E2P_PAGE_SIZE			;Calcola l'indirizzo di start della pagina
	mul		Acc,Rpark
	movw	XH:XL,R1:R0
	ori		XH,0x10	
	clr		Rcount
lp_rd_page_e2p:	
	rcall	EEPROM_ReadByte		;Legge il byte dalla E2P di indirizzo in "XH:XL"
	st		Y+,Acc				;Trasferisci nel buffer il dato letto da e2p
	adiw	X,1					;Punta al prossimo indirizzo e2p
	inc		Rcount
	cpi		Rcount,E2P_PAGE_SIZE
	brne	lp_rd_page_e2p
	ret

; ---- Legge un byte dalla E2P di indirizzo in "XH:XL" ------
; ---- Il dato � restituito in "Acc" ------------------------------ 
EEPROM_ReadByte:				
	lds		Acc,NVM_STATUS		; Wait until NVM is not busy.
	sbrc	Acc,NVM_NVMBUSY_bp
	rjmp	EEPROM_ReadByte	
	ld		Acc,X	
	ret

; ---- Scrive il buffer puntato da Y nella pagina "Acc" della E2P ------
EEPROM_WritePage:
	ldi		Par_1,E2P_PAGE_SIZE
	rjmp	EEPROM_WriteBlock

; ---- Scrive il buffer "mirE2P" nella System area della E2P ------
writeSysArea:	
	ldi		Acc,SYS_PAGE_E2P
	ldi		Par_1,LEN_SYS_AREA
	ldi		YL,pm_lo8 (mirE2P)
	ldi		YH,pm_hi8 (mirE2P)
; ---- Scrive il buffer puntato da Y nella pagina "Acc" della E2P per una lunghezza pari a "Par_1" ------
EEPROM_WriteBlock:
	push	R24
	push	R25
	ldi		Rpark,E2P_PAGE_SIZE			;Calcola l'indirizzo di start della pagina
	mul		Acc,Rpark
	movw	R25:R24,R1:R0
	clr		Rcount	
lpx:	
	ld		Acc,Y+
	rcall	EEPROM_WriteByte
	adiw	R24, 1
	inc		Rcount
	cp		Rcount,Par_1
	brne	lpx
	pop		R25
	pop		R24
	ret
		
EEPROM_WriteByte:	
	ldi		ZL, low(NVM_BASE)
	ldi		ZH, high(NVM_BASE)
qb:	ldd		Rpark, Z + NVM_STATUS - NVM_BASE
	sbrc	Rpark, NVM_NVMBUSY_bp
	rjmp	qb
	clr		R1
	std		Z + NVM_ADDR0 - NVM_BASE, R24
	std		Z + NVM_ADDR1 - NVM_BASE, R25
	std		Z + NVM_ADDR2 - NVM_BASE, R1
  	movw 	XL, R24
	subi	XL, low(-MAPPED_EEPROM_START)
	sbci	XH, high(-MAPPED_EEPROM_START)
	st		X, Acc
  ; Issue EEPROM Erase & Write command.
	ldi		Acc, NVM_CMD_ERASE_WRITE_EEPROM_PAGE_gc
	std		Z + NVM_CMD - NVM_BASE, Acc
	ldi		Acc, CCP_IOREG_gc
	ldi		Rpark, NVM_CMDEX_bm
	out		CPU_CCP, Acc
	std		Z + NVM_CTRLA - NVM_BASE, Rpark
	ret

;==========================================================
;==  Lettura della pagina flash che contiene l'indirizzo ==
;==	 "addrMicro" e trasferiscila in PageFlashBuffer		 ==
;==========================================================
leggiPaginaFlash:
	lds		ZH,addrMicro
	lds		ZL,addrMicro+1
	andi	ZL,0x80
	ldi		XL,pm_lo8 (PageFlashBuffer)
	ldi		XH,pm_hi8 (PageFlashBuffer)		
	ldi		Rcount,FLASH_PAGE_SIZE
lp_read_flash:
	lpm		Acc,Z+
	st		X+,Acc
	dec		Rcount
	brne	lp_read_flash				
	ret

;==================================================
;==  Scrittura della pagina flash che contiene	 ==
;==	 l'indirizzo "addrMicro" e aggiornalo		 ==
;==  all'inizio della pagina successiva			 ==
;==================================================
scriviFlashAddrMicro:
	lds		Rpark,addrMicro
	lds		Acc,addrMicro+1
	rol		Acc								;Dividi per 128 e
	rol		Rpark							;metti il risultato in Rpark
	rcall	scriviPaginaFlash
	brcs	exiScriviErr
	ldi		Acc,FLASH_PAGE_SIZE
	mov		R0,Acc
	lds		Rpark,addrMicro
	lds		Acc,addrMicro+1
	andi	Acc,0x80
	clr		R1
	add		Acc,R0
	adc		Rpark,R1
	sts		addrMicro,Rpark
	sts		addrMicro+1,Acc
	ldi		XH,pm_hi8 (PageFlashBuffer)		;Imposta il pointer all'indirizzo del buffer della flash
	ldi		XL,pm_lo8 (PageFlashBuffer)
	clr		Index_Mem						;Azzera indice del buffer di transito
	clc
exiScriviErr:
	ret

;==========================================================
;==  Scrittura della pagina flash indicata da Rpark		 ==
;==========================================================
scriviPaginaFlash:
	push	Rcount							; Salva valore utilizzato nel chiamante		
	cli
	clr		ZL								; Clear low byte of Z, to indicate start of page.
	clr		ZH								; Clear high byte of Z, to indicate start of page.
	clr		Acc
	out		CPU_RAMPX, Acc					; Clear RAMPX pointer.	
	ldi		XL,pm_lo8 (PageFlashBuffer)
	ldi		XH,pm_hi8 (PageFlashBuffer)		; Load X with data buffer address.
	ldi 	Acc, NVM_CMD_LOAD_FLASH_BUFFER_gc
	sts		NVM_CMD, Acc						; Load NVM command register.
	ldi		Rcount, ((FLASH_PAGE_SIZE/2)&0xFF)  ; Counter word
	ldi		Acc, CCP_SPM_gc                    ; Prepare Protect SPM signature in R16.
lp_LoadFlashPage:
	ld		r0, X+							; Load low byte from buffer into R0.
	ld		r1, X+							; Load high byte from buffer into R1.
	sts		CPU_CCP, Acc					; Enable SPM operation (this disables interrupts for 4 cycles).
	spm     Z+								; Self-program and move Z to next Flash word.
	dec		Rcount							; Decrement word count.
	brne	lp_LoadFlashPage				; Repeat until word cont is zero.

EraseWriteApplicationPage:                                                   
	ldi		Acc,FLASH_PAGE_SIZE
	mul		Acc,Rpark
	clr		Acc
	out		CPU_RAMPZ, Acc
	movw	ZL,R0
	ldi		Acc, NVM_CMD_ERASE_WRITE_APP_PAGE_gc  ; Prepare NVM command
	sts		NVM_CMD, Acc					; Load prepared command into NVM Command register.
	ldi		Acc, CCP_SPM_gc					; Prepare Protect SPM signature in R18
	sts		CPU_CCP, Acc					; Enable SPM operation (this disables interrupts for 4 cycles).
	spm										; Self-program.
WaitForSPM:
	lds		Acc, NVM_STATUS					; Load the NVM Status register.
	sbrc	Acc, NVM_NVMBUSY_bp				; Check if bit is cleared.
	rjmp	WaitForSPM						; Repeat check if bit is not cleared.
	clr		Acc
	sts		NVM_CMD,Acc						; Clear up command register to NO_OPERATION.
	sei
;---- Verifica che la pagina appena scritta sia uguale a quanto contenuto in "PageFlashBuffer"
	clr		Acc
	ror		Rpark							;Ricava indirizzo pagina
	ror		Acc
	mov		ZH,Rpark
	mov		ZL,Acc
	ldi		XL,pm_lo8 (PageFlashBuffer)
	ldi		XH,pm_hi8 (PageFlashBuffer)		
	ldi		Rcount,FLASH_PAGE_SIZE
lp_verf_flash:
	lpm		Acc,Z+
	ld		Par_1,X+
	cp		Par_1,Acc
	brne	err_verf_write
	dec		Rcount
	brne	lp_verf_flash
	pop		Rcount							;Ripristina valore utilizzato nel chiamante
	clc										;Segnala con Carry = 0 che la scrittura � uguale alla lettura
	ret

err_verf_write:
	pop		Rcount							;Ripristina valore utilizzato nel chiamante	
	sec										;Segnala con Carry = 1 la presenza dell'errore
	ret
