/*
 * Mem_Def.inc
 *
 *  Created: 17/06/2016 21.46.29
 *   Author: Buzzanca
 */
 #define ID_HARDWARE  '4'			;Identificativo Hardware dispositivo
 #define REL_HARDWARE  'M'		;Release hardware
 //#define ID_VETTURA  0xFF			;Identificativo vettura supportata

#define	checkParziale_L   R6		;Counter bytes da processare nel blocco in esame
#define	checkParziale_H   R7
#define	checkSum_L   R8			;Check calcolato in fase di lettura/elaborazione
#define	checkSum_H   R9
#define	Act_Addr_Free_L   R10	;Primo indirizzo libero nella flash
#define	Act_Addr_Free_H	  R11
#define	Index_Mem   R12		;Indice del buffer di transito per programmazione flash
#define	B_Crypt_0   R13
#define	B_Crypt_1   R14
#define	Nreg	  R15		;Counter in fase di decrypt
#define	Acc		  R16		;Utilizzato come accumulatore
#define	Rpark	  R17		;Registro temporaneo
#define	Par_1	  R18		;Passaggio parametro 1 a subroutine
#define	Par_2	  R19		;Passaggio parametro 2 a subroutine
#define	Rstop	  R20		;Registro temporaneo (controllo loop)
#define	Rcount	  R21		;Counter generico
#define	RidxTX    R22		;Indice prima cella libera nel buffer di TX
#define	ErrCode	  R23		;Codice di errore restituito
#define	idxBuffRX   R24		;Indice dati binari nel buffer
#define	Fl_Stato   R25		;Flags di stato

#define	MK_END_RX_DATA  1
#define	MK_FIRST_RECORD  2
#define	MK_COM_SER_PC  4
#define	MK_CONNECT_PC  8
#define	MK_ELAB_MES  0x10

#define	B_END_RX_DATA  0
#define	B_FIRST_RECORD  1
#define	B_COM_SER_PC  2		;Ricevuto un messaggio da PC 
#define	B_CONNECT_PC  3		;Il dispositivo � connesso al PC
#define	B_ELAB_MES  4			;E' in corso l'elaborazione di un messaggio

;==================================
;==  Valori limiti				 ==
;==================================
#define	MAXBLOCCHI  5			;Max numero+1 di blocchi PROGRAMMA
#define	MAX_DELAY_CHAR  10		;50 ms - Ritardo max tra due caratteri del messaggio da PC
#define	BUFF_RX_SIZE  170		;Dimensione buffer di RX da seriale
#define	BUFF_TX_SIZE 	80		;Buffer di trasmissione 
#define	FLASH_PAGE_SIZE  128	;Pagina flash (bytes)
#define	MASK_PAGE_SIZE  0x7F
#define	DATI_PER_RECORD  64	;Dati contenuti in un record
#define	E2P_PAGE_SIZE  32		;Pagina e2p (bytes)
#define	MASK_PAGE_E2P  0x1F
#define	MAX_BLOCCHI_PROG  5	;Numero segmenti di memoria programma

;==================================
;==  Costanti   				 ==
;==================================
#define	STX  0x02
#define	ETX  0x03
#define	LF   0x0A
#define	MK_SEL_CLQ_PC  0x3E	;maschera per isolare bit di selezione 	
#define	SEL_CLQ_PC  0x00		;Selezione che indica la connessione al PC => tutti i loop collegati = 0x00

;==================================
;==  Organizzazione dati in E2P  ==
;==================================
#define	SYS_PAGE_E2P = 31	; Pagina di inizio dei valori di sistema in e2p
// Byte/offset
//	0	= numBlocchi
//	1	= free
//	2-3	= checkSum dei parametri in e2p
//	4-5 = checkSum dei dati che compongono il programma in flash
//	6-7 = Indirizzo di inizio del primo blocco
//	8-9 = Lunghezza in byte dei dati del primo blocco
//	x(0-1) = Indirizzo di inizio blocco
//	y(0-1) = Lunghezza in byte del blocco
#define	NUM_SEG_PROG  0x00	; Numero blocchi che compongono il programma
#define	CHECK_E2P	  0x02	; CheckSum dei parametri in e2p
#define	CHECK_PROG	  0x04	; CheckSum dei dati che compongono il programma in flash
#define	START_BLOCCHI  0x06 ; Indirizzo di inizio del primo blocco
#define	LEN_SYS_AREA  START_BLOCCHI + MAX_BLOCCHI_PROG * 4

;==================================
;==  Assegnazione memoria RAM	 ==
;==================================	
	DATA_SECTION(.data) ; Start data segment
    
	.ORG 0x2000
modeBoot:	.BYTE 1		;Flag che indica la modalit� di funzionamento attiva
#define	 USER_MODE  0
#define	 BOOT_MODE  1
tickTim:	.BYTE 1		;Flag che indica che il timer � spirato
mirE2P:		.BYTE E2P_PAGE_SIZE					;Specchio dei valori in E2P (System area)
#define	numBlocchi  mirE2P + NUM_SEG_PROG		;Numero di blocchi/segmenti che compongono il programma
#define	checkProg  mirE2P + CHECK_PROG			;Check sum dell'intero firmware
#define	addrBlocco_1  (mirE2P + START_BLOCCHI)	;high:low
#define	addrBlocco_2  (addrBlocco_1 + (MAX_BLOCCHI_PROG * 2))	;high:low	ATTENZIONE: La lunghezza � -1 per compatibilit� con SAM
buffE2P:	.BYTE E2P_PAGE_SIZE					;Buffer di transito per scrittura dati in e2p 
;----- Ricezione da linea seriale
checkRX:	.BYTE 2		;Check calcolato del messaggio ricevuto da PC
lenDati:	.BYTE 1		;Lunghezza del messaggio ricevuto da PC
tmoRxChar:	.BYTE 1		;Timeout RX tra due caratteri del messaggio
statEvolRX:	.BYTE 1		;Stato evolutivo ricezione
numCharRX:	.BYTE 1		;Numero caratteri ricevuti
checkRec:	.BYTE 2		;Checksum del record ricevuto
;----- Trasmissione su linea seriale
requestTX:	.BYTE 1		;Richiesta e stato evolutivo trasmissione
bytesOut:	.BYTE 2		;Buffer (locale) in trasmissione
cntBytesTX: .BYTE 1		;Caratteri del messaggio trasmessi
idxTX:		.BYTE 1		;Indice dati da  trasmettere
;----- Supporto interprete comando
numRecord:	.BYTE 2		;Num. record dati
flRecData:	.BYTE 1		;Elaborato un record dati flash
numByteDati: .BYTE 1
cntRXRecord: .BYTE 2	;Counter sequenza record
rndVal:		.BYTE 2		;Chiave variabile
keyCrypt:	.BYTE 2
addrMicro:	.BYTE 2		;Indirizzo di inizio dati del record
End_Blocco:	.BYTE 2
Idx_Act_Blocco: .BYTE 1
	.ORG 0x2100
strTX:		.BYTE BUFF_TX_SIZE ;Buffer di trasmissione -- ;ATTENZIONE !! l'indirizzo basso deve essere 0x00
bytesRX:	.BYTE BUFF_RX_SIZE ;Buffer di ricezione
	.ORG 0x2200
PageFlashBuffer: .BYTE FLASH_PAGE_SIZE		;ATTENZIONE !! l'indirizzo basso deve essere 0x00

;============================================================
;==  Riferimeti microprocessore (compatibilit� prog. in C)  ==
;=============================================================
#define	RESET_VECT_USER  0x0000
#define	RESET_VECT_BOOT  0x4000
#define	NVM_CMD_READ_EEPROM_gc  0x06	; EEPROM Read command
#define	NVM_CMD_LOAD_EEPROM_BUFFER_gc  0x33 ;
#define	CCP_IOREG   0xD8


